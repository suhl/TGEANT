#include "T4SensitiveRich.hh"

T4SensitiveRich::T4SensitiveRich(T4RichPCB richPCB) :
    G4VSensitiveDetector(setSDName(richPCB.name, richPCB.id, TGEANT::RICH))
{
  G4SDManager* sdManager = G4SDManager::GetSDMpointer();
  sdManager->AddNewDetector(this);

  richData.detectorName = richPCB.name;
  richData.detectorId = richPCB.id;
  padPosition = richPCB.position;

  eventManager = T4EventManager::getInstance();
}

G4bool T4SensitiveRich::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  // only optical photons (PDG-ID 0)
  if (aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding() != 0)
    return false;

  // double hits are not allowed
  if (find(trackList.begin(), trackList.end(), aStep->GetTrack()->GetTrackID())
      != trackList.end())
    return false;

  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4ThreeVector hit = preStepPoint->GetPosition();
  setThreeVector(richData.photonHitPosition, hit.x(), hit.y(), hit.z());
  G4ThreeVector pos = aStep->GetTrack()->GetVertexPosition();
  setThreeVector(richData.photonProductionPosition, pos.x(), pos.y(), pos.z());
  richData.photonEnergy = preStepPoint->GetTotalEnergy();
  richData.time = preStepPoint->GetGlobalTime();
  richData.xPadPosition = richData.photonHitPosition[0] - padPosition.x();
  G4double yDist = sqrt(
      pow(richData.photonHitPosition[1] - padPosition.y(), 2)
          + pow(richData.photonHitPosition[2] - padPosition.z(), 2));
  (richData.photonHitPosition[1] - padPosition.y() > 0) ?
      richData.yPadPosition = yDist : richData.yPadPosition = -yDist;
  richData.parentTrackId = aStep->GetTrack()->GetParentID();

  T4TrajectoryHandler* g4Trajectory = getParentTrajectory(
      richData.parentTrackId);
  if (g4Trajectory != 0) {
    G4ThreeVector mom = g4Trajectory->GetInitialMomentum();
    setThreeVector<double>(richData.momentumMotherParticle, mom.x(), mom.y(), mom.z());
    G4ThreeVector photonMom = aStep->GetTrack()->GetVertexMomentumDirection();
    richData.cerenkovAngle = photonMom.angle(mom);
  } else {
    setThreeVector<double>(richData.momentumMotherParticle, 0, 0, 0);
    richData.cerenkovAngle = 0;
  }

  eventManager->getEvent()->rich.push_back(richData);

  trackList.push_back(aStep->GetTrack()->GetTrackID());

  return true;
}

void T4SensitiveRich::EndOfEvent(G4HCofThisEvent*)
{
  trackList.clear();
}

T4TrajectoryHandler* T4SensitiveRich::getParentTrajectory(G4int parentId)
{
  G4TrajectoryContainer* trajectoryContainer = G4RunManager::GetRunManager()
      ->GetCurrentEvent()->GetTrajectoryContainer();
  if (trajectoryContainer == 0)
    return 0;
  for (size_t i = 0; i < trajectoryContainer->entries(); i++) {
    T4TrajectoryHandler* g4Trajectory =
        (T4TrajectoryHandler*) ((*trajectoryContainer)[i]);
    if (g4Trajectory->GetTrackID() == parentId)
      return g4Trajectory;
  }
  return 0;
}
