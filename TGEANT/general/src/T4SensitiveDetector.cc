#include "T4SensitiveDetector.hh"

G4String setSDName(G4String detectorName, G4int channelNo,
    TGEANT::ReadoutType readoutType)
{
  G4String readoutName;

  switch (readoutType) {
    case TGEANT::HIT:
      readoutName = "HIT";
      break;
    case TGEANT::TRIGGER:
      readoutName = "TRIGGER";
      break;
    case TGEANT::CALO:
      readoutName = "CALO";
      break;
    case TGEANT::RICH:
      readoutName = "RICH";
      break;
    case TGEANT::PMT:
      readoutName = "PMT";
      break;
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__, "setSDName: Unknown readout type.");
      readoutName = "UNDEFINED";
      break;
  }

  return readoutName + "/" + detectorName + "/" + intToStr(channelNo);
}

T4SensitiveDetector::T4SensitiveDetector(G4String detectorName, G4int channelNo,
    TGEANT::ReadoutType readoutType, G4int detectorId, TGEANT::PmtType pmtType,
    G4int second_detectorId) :
    G4VSensitiveDetector(setSDName(detectorName, channelNo, readoutType))
{
  G4SDManager* sdManager = G4SDManager::GetSDMpointer();
  sdManager->AddNewDetector(this);

  hitCollection.detectorName = detectorName;
  hitCollection.detectorId = detectorId;
  hitCollection.second_detectorId = second_detectorId;
  hitCollection.channelNo = channelNo;
  hitCollection.readoutType = readoutType;
  hitCollection.pmtType = pmtType;
  hitCollection.sdIndex = 0;

  hitCollection.sdInformation.resize(10);

  eventManager = T4EventManager::getInstance();
  colour = T4Colour::getInstance();
  deadZoneRadius = 0;
  deadZoneX = 0;
  deadZoneY = 0;
}

G4bool T4SensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
  G4double totalEnergyDeposit = aStep->GetTotalEnergyDeposit();
  G4int pdgEncoding =
      aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

  // non-zero energy deposit for HIT and CALO
  if (hitCollection.readoutType != TGEANT::PMT && totalEnergyDeposit == 0.0 && hitCollection.detectorName != "DUMMY")
    return false;

  // only optical photons (PDG-ID 0) for PMT
  if (hitCollection.readoutType == TGEANT::PMT && pdgEncoding != 0)
    return false;

  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();

  if (isInDeadZone(preStepPoint->GetPosition()))
    return false;

  if (hitCollection.sdInformation.size() == hitCollection.sdIndex)
    hitCollection.sdInformation.resize(hitCollection.sdIndex + 50);

  //get new space from memory manager
  SdInformation* writeTo = T4MemoryManager::getInstance()->getNextIndex();
  //remember the address we write to
  hitCollection.sdInformation[hitCollection.sdIndex] = writeTo;

  writeTo->particleId = pdgEncoding;
  writeTo->trackId = aStep->GetTrack()->GetTrackID();
  writeTo->parentId = aStep->GetTrack()->GetParentID();
  writeTo->particleEnergy = preStepPoint->GetTotalEnergy();
  writeTo->time = preStepPoint->GetGlobalTime();
  writeTo->energyDeposit = totalEnergyDeposit;
  writeTo->beta = preStepPoint->GetBeta();
  writeTo->hitPosition = preStepPoint->GetPosition();
  writeTo->lastPosition = aStep->GetPostStepPoint()->GetPosition();
  writeTo->particleMomentum = preStepPoint->GetMomentum();

  hitCollection.sdIndex++;

  if (pdgEncoding != 0)
    Draw(writeTo->hitPosition);
  return true;
}

void T4SensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
  if (hitCollection.sdIndex > 0) {
    eventManager->analyse(&hitCollection);
    hitCollection.sdIndex = 0;
    if (hitCollection.sdIndex > 50)
      hitCollection.sdInformation.resize(50);
  }
}

void T4SensitiveDetector::Draw(G4ThreeVector hitPosition)
{
  G4VVisManager* vVisManager = G4VVisManager::GetConcreteInstance();
  if (vVisManager) {
    G4Circle circle(hitPosition);
    circle.SetScreenSize(5.0);
    circle.SetFillStyle(G4Circle::filled);
    circle.SetVisAttributes(colour->red);
    vVisManager->Draw(circle);
  }
}

void T4SensitiveDetector::addDeadZoneTubs(G4ThreeVector position, G4double radius)
{
  deadZonePositionTubs = position;
  deadZoneRadius = radius;
}

void T4SensitiveDetector::addDeadZoneBox(G4ThreeVector position, G4double xHalfSize, G4double yHalfSize)
{
  deadZonePositionBox = position;
  deadZoneX = xHalfSize;
  deadZoneY = yHalfSize;
}

G4bool T4SensitiveDetector::isInDeadZone(const G4ThreeVector& hitPos)
{
  if (deadZoneRadius != 0) {
    if ((hitPos - deadZonePositionTubs).perp() < deadZoneRadius)
      return true;
  }

  if (deadZoneX != 0 && deadZoneY != 0) {
    if (hitPos.x() > deadZonePositionBox.x() - deadZoneX
        && hitPos.x() < deadZonePositionBox.x() + deadZoneX
        && hitPos.y() > deadZonePositionBox.y() - deadZoneY
        && hitPos.y() < deadZonePositionBox.y() + deadZoneY)
      return true;
  }

  return false;
}
