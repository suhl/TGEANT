#include "T4RunManager.hh"

T4RunManager::T4RunManager(T4PhysicsList* physicsList)
{
  g4RunManager = new G4RunManager();
  settingsFile = T4SettingsFile::getInstance();

  g4RunManager->SetUserInitialization(physicsList);

  eventManager = NULL;
  primaryGenerator = NULL;
  worldConstruction = NULL;
}

void T4RunManager::startRun(G4int numEvents)
{
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4RunManager::startRun: Shooting " + intToStr(numEvents)
          + " particles.");

  
    g4RunManager->BeamOn(numEvents);
  
    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
        "Run finished.");
}

void T4RunManager::setEventManager(T4EventManager* eventManager_)
{
  if (eventManager == NULL) {
    eventManager = eventManager_;
    g4RunManager->SetUserAction(eventManager);
  } else
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__,
        "T4RunManager::setEventManager: EventManager has already been set.");
}

void T4RunManager::setBeamGun(T4PrimaryGenerator* primaryGenerator_)
{
  if (primaryGenerator == 0) {
    primaryGenerator = primaryGenerator_;
    g4RunManager->SetUserAction(primaryGenerator);
  } else
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__, "T4RunManager::setBeamGun: Beam gun has already been set.");
}

void T4RunManager::setWorldGeometry(T4WorldConstruction* worldConstruction_)
{
  if (worldConstruction == 0) {
    worldConstruction = worldConstruction_;
    g4RunManager->SetUserInitialization(worldConstruction);
  } else
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__,
        "T4RunManager::setWorldGeometry: World geometry has already been set.");
}

void T4RunManager::initialize(void)
{
  if (worldConstruction != NULL && primaryGenerator != NULL
      && eventManager != NULL) {
    // T4TrackingAction will be deleted by the G4RunManager
    g4RunManager->SetUserAction(new T4TrackingAction());
    g4RunManager->Initialize();
  } else
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4RunManager::initialize: World geometry or beam gun has not been set up correctly. RunManager couldn't be initialized.");
}
