#include "T4TrajectoryHandler.hh"

G4Allocator<T4TrajectoryHandler> t4TrajectoryAllocator;

T4TrajectoryHandler::T4TrajectoryHandler(const G4Track* aTrack)
{
  G4ParticleDefinition * particleDefinition = aTrack->GetDefinition();
  particleName = particleDefinition->GetParticleName();
  particleCharge = particleDefinition->GetPDGCharge();

  t4Trajectory.trackId = aTrack->GetTrackID();
  t4Trajectory.parentId = aTrack->GetParentID();
  t4Trajectory.particleId = particleDefinition->GetPDGEncoding();
  t4Trajectory.position[0] = aTrack->GetPosition().x();
  t4Trajectory.position[1] = aTrack->GetPosition().y();
  t4Trajectory.position[2] = aTrack->GetPosition().z();
  t4Trajectory.time = aTrack->GetGlobalTime();
  t4Trajectory.momentum[0] = aTrack->GetMomentum().x();
  t4Trajectory.momentum[1] = aTrack->GetMomentum().y();
  t4Trajectory.momentum[2] = aTrack->GetMomentum().z();

  positionRecord = new TrajectoryPointContainer();
  // Following is for the first trajectory point
  positionRecord->push_back(new G4TrajectoryPoint(aTrack->GetPosition()));
}

T4TrajectoryHandler::~T4TrajectoryHandler()
{
  if (positionRecord) {
    for (size_t i = 0; i < positionRecord->size(); i++)
      delete positionRecord->at(i);
    positionRecord->clear();
    delete positionRecord;
  }
}

void T4TrajectoryHandler::AppendStep(const G4Step* aStep)
{
  positionRecord->push_back(
      new G4TrajectoryPoint(aStep->GetPostStepPoint()->GetPosition()));
}

void T4TrajectoryHandler::MergeTrajectory(G4VTrajectory* secondTrajectory)
{
  if (!secondTrajectory)
    return;

  T4TrajectoryHandler* seco = (T4TrajectoryHandler*) secondTrajectory;
  G4int ent = seco->GetPointEntries();
  // initial point of the second trajectory should not be merged
  for (G4int i = 1; i < ent; i++) {
    positionRecord->push_back((*(seco->positionRecord))[i]);
  }
  delete (*seco->positionRecord)[0];

  seco->positionRecord->clear();
}
