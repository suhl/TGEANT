#include "T4SettingsFile.hh"

T4SettingsFile* T4SettingsFile::settingsFile = NULL;

T4SettingsFile* T4SettingsFile::getInstance(void)
{
  if (settingsFile == NULL) {
    settingsFile = new T4SettingsFile();
  }
  return settingsFile;
}

T4SettingsFile::T4SettingsFile(void)
{
  if (getenv("TGEANT") == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4SettingsFile: $TGEANT not set! Please set $TGEANT to the TGEANT installation directory.");
  }

  std::string pathToTGEANT = getenv("TGEANT");
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Found $TGEANT as " + pathToTGEANT);

  settingsFileName = pathToTGEANT + "/resources/settings.xml";
  startSeed = 0;
  randMan = NULL;

  structM = new T4SStructManager();
  fileBackEnd = NULL;
  detectorId = 1;
  t4SCalorimeter = NULL;
}

T4SettingsFile::~T4SettingsFile(void)
{
  delete structM;
  settingsFile = NULL;
  if (t4SCalorimeter != NULL)
    delete t4SCalorimeter;
}

void T4SettingsFile::loadFile(std::string input)
{
  settingsFileName = input;
  loadFile();
}

void T4SettingsFile::loadFile(void)
{
  if (fileBackEnd == NULL)
    fileBackEnd = new T4SSettingsFileXML(structM);

  fileBackEnd->load(settingsFileName);

  T4SMessenger::getInstance()->setColorTheme(structM->getGeneral()->colorTheme);
  T4SMessenger::getInstance()->setVerboseLevel(
      structM->getGeneral()->verboseLevel);

  delete fileBackEnd;
  fileBackEnd = NULL;

  if (settingsFile->getStructManager()->getGeneral()->useSeed) {
    startSeed = settingsFile->getStructManager()->getGeneral()->seed;
  } else {
    T4SMessenger::getReference().setupStream(T4SNotice, __LINE__, __FILE__);
    T4SMessenger::getReference() << "Calling a random seed from /dev/random...";
    FILE *random;
    random = fopen("/dev/random", "r");
    if (random == NULL) {
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
      __FILE__,
          "T4SettingsFile::loadFile: Cannot open /dev/random! Select seed by hand and restart TGEANT.");
    }
    fread(&startSeed, sizeof(startSeed), 1, random);
    startSeed = fabs(startSeed);
    T4SMessenger::getReference() << " finished.";
    T4SMessenger::getReference().endStream();
  }

  G4Random::setTheSeed(startSeed);
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4SettingsFile::loadFile: Seed set to: " + longintToStr(startSeed));

  double randSeed = G4UniformRand();
  randSeed *= 1000000;

  randMan = new CLHEP::HepRandom;
  CLHEP::HepRandomEngine* tmp = new CLHEP::RanluxEngine;
  tmp->setSeed(randSeed,3);
  randMan->setTheEngine(tmp);
  randMan->setTheSeed(randSeed,3);
  randEng = tmp;
}

TGEANT::PhysicsList T4SettingsFile::getPhysicsList(void)
{
  string pl = structM->getGeneral()->physicsList;

  if (pl == "EM_ONLY")
    return TGEANT::EM_ONLY;
  else if (pl == "STANDARD")
    return TGEANT::STANDARD;
  else if (pl == "FULL")
    return TGEANT::FULL;
  else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "T4SettingsFile::getPhysicsList: Unknown TGEANT::PhysicsList. Set to TGEANT::STANDARD as default.");
    return TGEANT::STANDARD;
  }
}

TGEANT::T4BeamFileType T4SettingsFile::getBeamFileType(std::string input)
{
  if (input == "Beam")
    return TGEANT::Normal;
  else if (input == "Halo")
    return TGEANT::Halo;
  else if (input == "Both")
    return TGEANT::Both;
  else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "T4SettingsFile::getBeamFileType: Unknown TGEANT::T4BeamFileType. Set to TGEANT::Normal as default.");
    return TGEANT::Normal;
  }
}

bool T4SettingsFile::isOpticalPhysicsActivated(void)
{
  return getPhysicsList() == TGEANT::FULL;
}

bool T4SettingsFile::geometryModeActivated(void)
{
  if (structM->getGeneral()->saveDetDat || structM->getGeneral()->exportGDML) {
    // disable some not wanted features
    structM->getBeam()->numParticles = 1;
    structM->getGeneral()->saveASCII = false;
    structM->getGeneral()->saveBinary = false;
    structM->getGeneral()->useGflash = false;
    return true;
  } else
    return false;
}

int T4SettingsFile::getDetectorId(int n)
{
  detectorId += n;
  return detectorId - n;
}

const vector<T4SCAL>* T4SettingsFile::getCMTXList(void)
{
  if (t4SCalorimeter == NULL) {
    t4SCalorimeter = new T4SCalorimeter(structM);
    t4SCalorimeter->readXML(structM->getExternal()->calorimeterInfo);
  }
  return &t4SCalorimeter->getCalorimeter();
}
