#include "T4SignalManager.hh"

bool T4SignalManager::instanceFlag = false;
T4SignalManager* T4SignalManager::single = NULL;

T4SignalManager* T4SignalManager::getInstance()
{
  if (!instanceFlag) {
    single = new T4SignalManager();
    instanceFlag = true;
    return single;
  } else {
    return single;
  }
}

void interruptHandler(int sig)
{
  T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
      "CTRL+C found");
  if (T4SignalManager::getInstance()->interruptedOnce)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "CTRL+C found twice, killing with force!");
  else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "Trying graceful shutdown! Please wait a few seconds!.....");
    G4RunManager::GetRunManager()->AbortRun(true);

    T4SignalManager::getInstance()->interruptedOnce = true;
  }
}

void terminationHandler(int sig)
{
  if (T4SignalManager::getInstance()->terminatedOnce)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "Termination requested twice!");
  else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Termination has been requested! Stopping run and trying to save some data!");
    G4RunManager::GetRunManager()->AbortRun();
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Lets hope it works! Kill again if this does not show effect in 20-30 seconds");
    T4SignalManager::getInstance()->terminatedOnce = true;
  }
}

T4SignalManager::T4SignalManager(void)
{
  //register the signals
  signal(SIGINT, interruptHandler);
  signal(SIGTERM, terminationHandler);
  interruptedOnce = false;
  terminatedOnce = false;
}
