#include <T4RegionManager.hh>

T4RegionManager* T4RegionManager::regionManager = NULL;

T4RegionManager* T4RegionManager::getInstance(void)
{
  if (regionManager == NULL) {
    regionManager = new T4RegionManager();
  }
  return regionManager;
}

T4RegionManager::T4RegionManager(void)
{
  // default region
  defaultRegion = (*(G4RegionStore::GetInstance()))[0];
  // 1eV cut for optical photons
  defaultUserLimits = new G4UserLimits(DBL_MAX, DBL_MAX, DBL_MAX, 0.5 * CLHEP::MeV);
  defaultRegion->SetUserLimits(defaultUserLimits);
 
  // target region
  G4double targetStepLimit;
  if (T4SettingsFile::getInstance()->getStructManager()->getBeam()->useTargetExtrap)
    targetStepLimit = T4SettingsFile::getInstance()->getStructManager()->getBeam()->targetStepLimit;
  else
    targetStepLimit = DBL_MAX;

  targetRegion = new G4Region("targetRegion");
  targetRegion->SetProductionCuts(G4ProductionCutsTable::GetProductionCutsTable()->GetDefaultProductionCuts());
  targetUserLimits = new G4UserLimits(targetStepLimit);
  targetRegion->SetUserLimits(targetUserLimits);

  // camera region
  opticalRegion = new G4Region("opticalRegion");
  opticalRegion->SetProductionCuts(G4ProductionCutsTable::GetProductionCutsTable()->GetDefaultProductionCuts());
//  opticalUserLimits = new G4UserLimits(DBL_MAX, maxTrackLength, maxTrackTime);
//  opticalRegion->SetUserLimits(opticalUserLimits);

  // absorber region
  absorberRegion = new G4Region("absorberRegion");
  initCuts(absorberCuts, 50. * CLHEP::cm);
  absorberRegion->SetProductionCuts(absorberCuts);
//  absorberUserLimits = new G4UserLimits(DBL_MAX, maxTrackLength, maxTrackTime, 1.0 * CLHEP::GeV);
//  absorberRegion->SetUserLimits(absorberUserLimits);

  // calo region
  T4SGeneral* general = T4SettingsFile::getInstance()->getStructManager()->getGeneral();
  initCuts(gamsCuts, general->productionCutsGams * CLHEP::mm);
  initCuts(rhgamsCuts, general->productionCutsRHGams * CLHEP::mm);
  initCuts(mainzCuts, general->productionCutsMainz * CLHEP::mm);
  initCuts(olgaCuts, general->productionCutsOlga * CLHEP::mm);
  initCuts(shashlikCuts, general->productionCutsShashlik * CLHEP::mm);
  initCuts(hcalCuts, general->productionCutsHcal * CLHEP::mm);
  
  ecal1_GamsRegion = NULL;
  ecal2_GamsRegion = NULL;
  ecal2_GamsRhRegion = NULL;
  ecal1_OlgaRegion = NULL;
  ecal1_MainzRegion = NULL;
  ecal0Region = NULL;
  hcal1Region = NULL;
  hcal2Region = NULL;
  ecal1_ShashlikRegion = NULL;
  ecal2_ShashlikRegion = NULL;
}

T4RegionManager::~T4RegionManager(void)
{
  delete defaultUserLimits;

  delete targetRegion;
  delete targetUserLimits;

  delete opticalRegion;
//  delete opticalUserLimits;

  delete absorberRegion;
  delete absorberCuts;
//  delete absorberUserLimits;
  
  if (ecal1_GamsRegion != NULL)
    delete ecal1_GamsRegion;
  if (ecal2_GamsRegion != NULL)
    delete ecal2_GamsRegion;
  if (ecal2_GamsRhRegion != NULL)
    delete ecal2_GamsRhRegion;
  if (ecal1_OlgaRegion != NULL)
    delete ecal1_OlgaRegion;
  if (ecal1_MainzRegion != NULL)
    delete ecal1_MainzRegion;
  if (ecal0Region != NULL)
    delete ecal0Region;
  if (hcal1Region != NULL)
    delete hcal1Region;
  if (hcal2Region != NULL)
    delete hcal2Region;
  if (ecal1_ShashlikRegion != NULL)
    delete ecal1_ShashlikRegion;
  if (ecal2_ShashlikRegion != NULL)
    delete ecal2_ShashlikRegion;

  delete gamsCuts;
  delete rhgamsCuts;
  delete mainzCuts;
  delete olgaCuts;
  delete shashlikCuts;
  delete hcalCuts;
}

void T4RegionManager::addToRegion(G4LogicalVolume* log, G4Region* reg)
{
  log->SetRegion(reg);
  reg->AddRootLogicalVolume(log);
}

void T4RegionManager::checkRegion(G4Region*& reg, G4String name,
    T4RegionInformation* info, G4ProductionCuts* cuts)
{
  if (reg == NULL && info != NULL) {
    reg = new G4Region(name);
    reg->SetProductionCuts(cuts);
    reg->SetUserInformation(info);
  }
}

void T4RegionManager::initCuts(G4ProductionCuts*& cut, double val)
{
  cut = new G4ProductionCuts();
  cut->SetProductionCut(val, "gamma");
  cut->SetProductionCut(val, "e-");
  cut->SetProductionCut(val, "e+");
}

G4Region* T4RegionManager::getECAL1GamsRegion(T4RegionInformation* info)
{
  checkRegion(ecal1_GamsRegion, "ecal1_GamsRegion", info, gamsCuts);
  return ecal1_GamsRegion;
}

G4Region* T4RegionManager::getECAL2GamsRegion(T4RegionInformation* info)
{
  checkRegion(ecal2_GamsRegion, "ecal2_GamsRegion", info, gamsCuts);
  return ecal2_GamsRegion;
}

G4Region* T4RegionManager::getECAL2GamsRhRegion(T4RegionInformation* info)
{
  checkRegion(ecal2_GamsRhRegion, "ecal2_GamsRhRegion", info, rhgamsCuts);
  return ecal2_GamsRhRegion;
}

G4Region* T4RegionManager::getECAL1OlgaRegion(T4RegionInformation* info)
{
  checkRegion(ecal1_OlgaRegion, "ecal1_OlgaRegion", info, olgaCuts);
  return ecal1_OlgaRegion;
}

G4Region* T4RegionManager::getECAL1MainzRegion(T4RegionInformation* info)
{
  checkRegion(ecal1_MainzRegion, "ecal1_MainzRegion", info, mainzCuts);
  return ecal1_MainzRegion;
}

G4Region* T4RegionManager::getEcal0Region(T4RegionInformation* info)
{
  checkRegion(ecal0Region, "ecal0Region", info, shashlikCuts);
  return ecal0Region;
}

G4Region* T4RegionManager::getHcal1Region(T4RegionInformation* info)
{
  checkRegion(hcal1Region, "hcal1Region", info, hcalCuts);
  return hcal1Region;
}

G4Region* T4RegionManager::getHcal2Region(T4RegionInformation* info)
{
  checkRegion(hcal2Region, "hcal2Region", info, hcalCuts);
  return hcal2Region;
}

G4Region* T4RegionManager::getECAL1ShashlikRegion(T4RegionInformation* info)
{
  checkRegion(ecal1_ShashlikRegion, "ecal1_ShashlikRegion", info, shashlikCuts);
  return ecal1_ShashlikRegion;
}

G4Region* T4RegionManager::getECAL2ShashlikRegion(T4RegionInformation* info)
{
  checkRegion(ecal2_ShashlikRegion, "ecal2_ShashlikRegion", info, shashlikCuts);
  return ecal2_ShashlikRegion;
}
