#include "T4OutputDetDat.hh"
#include "T4WorldConstruction.hh"

void T4OutputDetDat::close(void)
{
  T4SettingsFile* settingsFile = T4SettingsFile::getInstance();

  T4WorldConstruction* t4World =
      (T4WorldConstruction*) G4RunManager::GetRunManager()
          ->GetUserDetectorConstruction();
  const std::vector<T4BaseDetector*>& baseDetector = t4World->getBaseDetector();

  // add detectors.dat lines for all TGEANT detectors
  for (unsigned int i = 0; i < baseDetector.size(); i++)
    baseDetector.at(i)->getWireDetDat(t4SDetectorsDat->getWireDetector(),
        t4SDetectorsDat->getDeadZone());

  // add target information
  if (T4ActiveTarget::getInstance()->getTarget() != NULL)
    T4ActiveTarget::getInstance()->getTarget()->getTargetDetDat(
        t4SDetectorsDat->getTargetInformation());

  // add all other information loaded from a detectors.dat file
  t4SDetectorsDat->addWireInformationFromEffic(settingsFile->getStructManager()->getExternal()->detectorEfficiency);

  // add positions of all ...
  // calorimeters:
  t4SDetectorsDat->addCaloPosition(*settingsFile->getStructManager()->getCalorimeter());
  t4SDetectorsDat->addCaloCMTXLines(settingsFile->getCMTXList());
  // magnets:
  t4SDetectorsDat->addMagnetPosition(*settingsFile->getStructManager()->getMagnet());
  // rich:
  t4SDetectorsDat->addRichPosition(*settingsFile->getStructManager()->getRICH());

  // save detectors.dat file
  checkFile();
  t4SDetectorsDat->save(saveFileName);
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4OutputDetDat::close: Output saved as " + saveFileName);
}
