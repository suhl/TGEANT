#include "T4Globals.hh"
using namespace std;


T4DetIdent detIdent(G4String tbName, G4String detName, G4int unit)
{
  T4DetIdent tmp = {tbName, detName, unit};
  return tmp;
}
