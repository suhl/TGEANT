#include "T4PerformanceMonitor.hh"

T4PerformanceMonitor* T4PerformanceMonitor::performanceMonitor = 0;

T4PerformanceMonitor* T4PerformanceMonitor::getInstance(void)
{
  if (performanceMonitor == 0) {
    performanceMonitor = new T4PerformanceMonitor();
  }
  return performanceMonitor;
}

T4PerformanceMonitor::T4PerformanceMonitor(void)
{
  useMonitor = T4SettingsFile::getInstance()->getStructManager()->getGeneral()
      ->usePerformanceMonitor;

  timeForRun = 0;
  timeForLoad = 0;
  timeForEvent = 0;
  avgTimeForEvent = 0;
  numEvents = 0;
  numSavedEvents = 0;

  isStopped = false;

  //start run timer
  gettimeofday(&completeRunStart, 0);
  if (useMonitor)
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__, "T4PerformanceMonitor is active.");
}

T4PerformanceMonitor::~T4PerformanceMonitor(void)
{
  if (useMonitor) {
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"-------------------------------------------------------------");
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Loading time for startup         [s]: "+doubleToStrFP(timeForLoad / CLHEP::s));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of events                    : "+intToStr(numEvents));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Average time per event           [s]: "+doubleToStrFP(avgTimeForEvent / CLHEP::s / numEvents));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"-------------------------------------------------------------");
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of saved events              : "+intToStr(numSavedEvents));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Ratio of saved events            [%]: "+doubleToStrFP((double) numSavedEvents / numEvents * 100.));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Average time per saved event     [s]: "+doubleToStrFP((double) avgTimeForEvent / CLHEP::s / numSavedEvents));
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"-------------------------------------------------------------");
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Time for complete run            [s]: "+doubleToStrFP((double) timeForRun / CLHEP::s));
    T4SMessenger::getInstance()->printCurrentTime("Date: %Y-%m-%d Time: %X");
  }
}

void T4PerformanceMonitor::eventTimer(T4PerformanceMonitor::Monitor input)
{
  timeForEvent = 0;
  if (input == T4PerformanceMonitor::start) {
    gettimeofday(&eventStart, 0);
  } else if (input == T4PerformanceMonitor::stop) {
    gettimeofday(&eventStop, 0);
    timeForEvent = (eventStop.tv_sec - eventStart.tv_sec
        + (float) (eventStop.tv_usec - eventStart.tv_usec) / 1000000)
        * CLHEP::s;
    avgTimeForEvent += timeForEvent;
    numEvents++;
  }
}
void T4PerformanceMonitor::stopLoadTimer(void)
{
  if (!isStopped) {
    gettimeofday(&loadStop, 0);
    isStopped = true;
  }
}

G4double T4PerformanceMonitor::getLoadTime(void)
{
  timeForLoad = (loadStop.tv_sec - completeRunStart.tv_sec
      + (float) (loadStop.tv_usec - completeRunStart.tv_usec) / 1000000)
      * CLHEP::s;
  return timeForLoad / CLHEP::s;
}

G4double T4PerformanceMonitor::getTotalRunTime(void)
{
  // end run timer
  gettimeofday(&completeRunStop, 0);
  timeForRun = (completeRunStop.tv_sec - completeRunStart.tv_sec
      + (float) (completeRunStop.tv_usec - completeRunStart.tv_usec) / 1000000)
      * CLHEP::s;
  return timeForRun / CLHEP::s;
}
