#include "T4OutputManager.hh"
#include "T4EventManager.hh"
#include "T4SMessenger.hh"

T4OutputManager::T4OutputManager(T4Event* t4Event)
{
  settingsFile = T4SettingsFile::getInstance();

  findRunName();

  if (settingsFile->getStructManager()->getGeneral()->saveASCII)
    backEnd.push_back(new T4OutputASCII(outputFileName));
  
  if (settingsFile->getStructManager()->getGeneral()->saveBinary)
    backEnd.push_back(new T4OutputBinary(outputFileName));

  if (settingsFile->getStructManager()->getGeneral()->saveDetDat)
    backEnd.push_back(new T4OutputDetDat(outputFileName));

  for (unsigned int i = 0; i < backEnd.size(); i++)
  {
    backEnd.at(i)->setEvent(t4Event);
    backEnd.at(i)->setChunkSplitting(settingsFile->getStructManager()->getGeneral()->useSplitting,settingsFile->getStructManager()->getGeneral()->eventsPerChunk,settingsFile->getStructManager()->getGeneral()->useSplitPiping);
  }
}

T4OutputManager::~T4OutputManager(void)
{
  // no delete because of problems with ROOT
}

void T4OutputManager::closeFiles(void)
{
  T4PerformanceMonitor* performanceMonitor =
      T4PerformanceMonitor::getInstance();
  double loadTime = performanceMonitor->getLoadTime();
  double totalRunTime = performanceMonitor->getTotalRunTime();

  for (unsigned int i = 0; i < backEnd.size(); i++) {
    backEnd.at(i)->savePerformance(loadTime, totalRunTime, settingsFile->getStartSeed());
    backEnd.at(i)->close();
  }

  if (settingsFile->getStructManager()->getGeneral()->exportGDML)
    saveROOTGeometry();
}

void T4OutputManager::saveEvent(void)
{
  for (unsigned int i = 0; i < backEnd.size(); i++)
    backEnd.at(i)->save();
}

void T4OutputManager::saveROOTGeometry()
{
  while (true) {
    if (!fileExists(outputFileName + ".gdml"))
      break;
    outputFileName += "_";
  }

  G4VPhysicalVolume* exportWorld =
      G4TransportationManager::GetTransportationManager()
          ->GetNavigatorForTracking()->GetWorldVolume();
  G4GDMLParser outputParser;
  outputParser.Write((outputFileName + ".gdml").c_str(), exportWorld);
}

void T4OutputManager::findRunName(void)
{
  T4SGeneral* t4SGeneral = settingsFile->getStructManager()->getGeneral();
  std::string outFile = t4SGeneral->outputPath + t4SGeneral->runName;

  if (t4SGeneral->namingWithSeed) {
    bool printWarning(false);
    outputFileName = outFile + "_seed" + longintToStr(settingsFile->getStartSeed());
    while (true) {
      if (!fileExists(outputFileName + ".tgeant")
          && !fileExists(outputFileName + ".root")
          && !fileExists(outputFileName + "_histos.root")
          && !fileExists(outputFileName + "_detectors.dat")
          && !fileExists(outputFileName + ".gdml")) {
        break;
      }
      outputFileName += "_";
      printWarning = true;
    }
    if (printWarning) {
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
          "T4OutputManager::findRunName: The output file '" + outFile
              + "' with the given seed "
              + longintToStr(settingsFile->getStartSeed())
              + " already exists. Added '_' to the output file name.");
    }
  } else {
    outFile += "_run";
    unsigned int runNo = 0;
    while (true) {
      std::string number = getRunNoString(runNo);
      if (!fileExists(outFile + number + ".tgeant")
          && !fileExists(outFile + number + ".root")
          && !fileExists(outFile + number + "_histos.root")
          && !fileExists(outFile + number + "_detectors.dat")
          && !fileExists(outFile + number + ".gdml")) {
        outputFileName = outFile + number;
        break;
      }
      runNo++;
    }
  }
}

std::string T4OutputManager::getRunNoString(unsigned int runNo)
{
  std::ostringstream runNo_str;
  if (runNo < 10) {
    runNo_str << runNo;
    return "00" + runNo_str.str();
  } else if (10 <= runNo && runNo < 100) {
    runNo_str << runNo;
    return "0" + runNo_str.str();
  } else {
    runNo_str << runNo;
    return runNo_str.str();
  }
}
