#include "T4EventManager.hh"
#include "T4TargetTracking.hh"
#include "T4BeamBackend.hh"

T4EventManager* T4EventManager::eventManager = NULL;

T4EventManager* T4EventManager::getInstance(void)
{
  if (eventManager == NULL) {
    eventManager = new T4EventManager();
  }
  return eventManager;
}

T4EventManager::T4EventManager(void)
{
  t4Event = new T4Event();
  t4Daq = new T4Daq(&trackIds);
  t4Daq->synchronizePMT();
  t4OutputManager = new T4OutputManager(t4Event);
  settingsFile = T4SettingsFile::getInstance();

  if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "DVCS2012")
    triggerPlugin = new T4TriggerDVCS2012();
  else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "Hadron2008")
    triggerPlugin = new T4TriggerHadron2008();
  else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "SIDIS2011")
    triggerPlugin = new T4TriggerSIDIS2011();
  else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "DY2014")
    triggerPlugin = new T4TriggerDY2014();
  else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "DY2015")
    triggerPlugin = new T4TriggerDY2015();
  else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "Primakoff2012") {
    triggerPlugin = new T4TriggerPrimakoff2012(settingsFile->getStructManager()->getPrimTrig()->threshPrim1,
                                               settingsFile->getStructManager()->getPrimTrig()->threshPrim2,
                                               settingsFile->getStructManager()->getPrimTrig()->threshPrim2Veto,
                                               settingsFile->getStructManager()->getPrimTrig()->threshPrim3);
  } else if (settingsFile->getStructManager()->getGeneral()->triggerPlugin == "DVCS2016")
      triggerPlugin = new T4TriggerDVCS2016();
  else
    T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"Triggerplugin '%s' not found! Please use a valid string for triggerPlugin!\n",settingsFile->getStructManager()->getGeneral()->triggerPlugin.c_str());

  if (settingsFile->getStructManager()->getGeneral()->useTrigger){
    triggerPlugin->setEventPointer(t4Event);
    T4SExternalFiles* files = settingsFile->getStructManager()->getExternal();
    triggerPlugin->loadInnerX(files->triggerMatrixInnerX);
    triggerPlugin->loadLadderX(files->triggerMatrixLadderX);
    triggerPlugin->loadMiddleX(files->triggerMatrixMiddleX);
    triggerPlugin->loadMiddleY(files->triggerMatrixMiddleY);
    triggerPlugin->loadOuterY(files->triggerMatrixOuterY);
    triggerPlugin->loadLast(files->triggerMatrixLAST);
  }

  // usedGenerator
  // check if plugin exists
  map<string, T4BeamBackEnd*>* beamPluginMap = &T4BeamPluginList::getInstance()->beamPlugins;
  string beamPlugin = settingsFile->getStructManager()->getBeam()->beamPlugin;
  if (beamPluginMap->find(beamPlugin) == beamPluginMap->end())
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
          "T4EventManager: Name of beam plugin '" + beamPlugin + "' not found!");

  usedGenerator = ((*beamPluginMap)[beamPlugin]->getEventGeneratorProcess() != NULL);

  eventNo = 0;
  nSavedEvents = 0;
  verboseLevel = settingsFile->getStructManager()->getGeneral()->verboseLevel;
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4EventManager loaded.");
}

T4EventManager::~T4EventManager(void)
{
  delete t4Daq;
  delete triggerPlugin;
  
  delete t4Event;
  T4PerformanceMonitor::getInstance()->setSavedEventNo(nSavedEvents);

  eventManager = NULL;

  T4SMessenger::getReference() << "T4EventManager finished.";
  T4SMessenger::getReference().endStream();
}

void T4EventManager::EndOfEventAction(const G4Event* anEvent)
{
  G4TrajectoryContainer* trajectoryContainer =
      anEvent->GetTrajectoryContainer();
  if (trajectoryContainer) {

    G4int badCoralParticleId = 1000000000;
    std::vector<G4int>::iterator it;
    for (it = trackIds.begin(); it != trackIds.end();) {
      if (*it >= badCoralParticleId)
        it = trackIds.erase(it);
      else
        ++it;
    }

    // create map
    for (G4int i = 0; i < trajectoryContainer->entries(); i++) {
      T4TrajectoryHandler* g4Trajectory =
          (T4TrajectoryHandler*) trajectoryContainer->GetVector()->at(i);
      const T4Trajectory* t4Trajectory = g4Trajectory->getTrajectory();
      // do not save trajectories from optical photons (id=0)
      // anyway, they are only saved with enabled visualization
      if (t4Trajectory->particleId == 0)
        continue;
      // create map
      trackMap[t4Trajectory->trackId] = t4Trajectory;
      // save at least all particles with parent id <= 1 (primary vertex)
      if (t4Trajectory->parentId <= 1
          && t4Trajectory->particleId < badCoralParticleId)
        trackIds.push_back(t4Trajectory->trackId);
    }

    // add parent tracks
    int parentId = 0;
    for (unsigned int i = 0; i < trackIds.size(); i++) {
      G4int id = trackIds.at(i);
      while (true) {
        if (trackMap.find(id) == trackMap.end())
          break;
        parentId = trackMap[id]->parentId;
        if (parentId == 0 || parentId >= badCoralParticleId)
          break;
        else {
          trackIds.push_back(parentId);
          id = parentId;
        }
      }
    }

    // remove multi tracks
    std::sort(trackIds.begin(), trackIds.end());
    trackIds.erase(std::unique(trackIds.begin(), trackIds.end()),
        trackIds.end());

    // Geant4 tracking order follows 'last in first out' rule:
    // 1) incoming muon
    // 2) all pile up muons
    // 3) secondary particles from pile up muons
    // 4) delta electrons
    // 5) our primary vertex particles
    // 6) all tertiary particles from primary vertex particles

    // save trajectories in the following order: 1-5-6-2-3-4 (move pile up at the end)
    vector<G4int> skipList;
    G4bool primVertPartWasThere = false;
    for (unsigned int i = 0; i < trackIds.size(); i++) {
      if (trackMap.find(trackIds.at(i)) != trackMap.end()) {
        if (!primVertPartWasThere) {
          if (trackMap[trackIds.at(i)]->parentId == 1
              && (float) trackMap[trackIds.at(i)]->position[0]
                  == (float) t4Event->beamData.vertexPosition[0]
              && (float) trackMap[trackIds.at(i)]->position[1]
                  == (float) t4Event->beamData.vertexPosition[1]
              && (float) trackMap[trackIds.at(i)]->position[2]
                  == (float) t4Event->beamData.vertexPosition[2])
            primVertPartWasThere = true;
        }
        if (i == 0 /* for incoming muon */|| primVertPartWasThere)
          t4Event->beamData.trajectories.push_back(*trackMap[trackIds.at(i)]);
        else
          skipList.push_back(i);
      }
    }

    // save all pile up trajectories with secondaries
    for (unsigned int i = 0; i < skipList.size(); i++) {
      t4Event->beamData.trajectories.push_back(*trackMap[trackIds.at(skipList.at(i))]);
    }

    t4Event->beamData.nTrajectories = t4Event->beamData.trajectories.size();
  }

  // performance monitor
  if (settingsFile->getStructManager()->getGeneral()->usePerformanceMonitor) {
    T4PerformanceMonitor::getInstance()->eventTimer(T4PerformanceMonitor::stop);
    t4Event->processingTime =
        T4PerformanceMonitor::getInstance()->getEventTime();
  } else
    t4Event->processingTime = 0;

  // trigger logic
  G4int trigMask = 0;
  if (settingsFile->getStructManager()->getGeneral()->useTrigger)
    trigMask = triggerPlugin->getTriggerMask();
  t4Event->trigMask = trigMask;

  T4TargetTracking* targetTracking = T4TargetTracking::getInstance();

  // save output if no visualization mode
  if ((!usedGenerator || targetTracking->usedProcessInThisEvent())) {
    t4OutputManager->saveEvent();
    nSavedEvents++;
  }

  if (verboseLevel > 2)
    t4Event->printEventInfo();

  if (settingsFile->getStructManager()->getGeneral()->usePerformanceMonitor)
    T4SMessenger::getInstance()->printMessage(T4SVerbose, __LINE__, __FILE__,
	"End of event #" + intToStr(eventNo) + ". Processing time: " + doubleToStrFP(t4Event->processingTime)+"s.");
  else
    T4SMessenger::getInstance()->printMessage(T4SVerbose, __LINE__, __FILE__,
	"End of event #" + intToStr(eventNo) + ".");    

  eventNo++;

  // clean up
  t4Event->clear();
  t4Daq->synchronizePMT();

  targetTracking->reset();
  trackIds.clear();
  trackMap.clear();
  T4MemoryManager::getInstance()->resetIndex();
  
  //now we check for memory consumption, we do this all 50 events
  if (eventNo % 50 == 0 && !settingsFile->getStructManager()->getGeneral()->noMemoryLimitation) {
    double vm(0.0),real(0.0);
    T4MemoryManager::process_mem_usage(vm,real);
    T4SMessenger::getInstance()->setupStream(T4SVerboseMore,__LINE__,__FILE__);
    T4SMessenger::getReference() << "Checking for memory consumption! virt: " << vm << " resident_set: " << real;
    T4SMessenger::getInstance()->endStream();
    if (vm > 2048*1024) {
      T4SMessenger::getInstance()->printMessage(T4SWarning,__LINE__,__FILE__,"Memory of 2GB exceeded! Aborting Run to keep up system stability!");
      G4RunManager::GetRunManager()->AbortRun(true);
    }
  }

//  if (T4PerformanceMonitor::getInstance()->getTotalRunTime() > 45*60) {
//    T4SMessenger::getInstance()->printMessage(T4SWarning,__LINE__,__FILE__,"Total run time of 45 minutes exceeded! Stop the run...");
//    G4RunManager::GetRunManager()->AbortRun();
//  }
}

void T4EventManager::analyse(T4HitCollection* input)
{
  switch (input->readoutType) {
    case TGEANT::HIT:
      t4Daq->analyseHit(&t4Event->tracking, input, 0.5 * CLHEP::MeV);
      break;
    case TGEANT::TRIGGER:
      t4Daq->analyseHit(&t4Event->trigger, input, 1.0 * CLHEP::MeV);
      break;
    case TGEANT::CALO:
      t4Daq->analyseCalorimeter(&t4Event->calorimeter, input, 1. * CLHEP::MeV);
      break;
    case TGEANT::PMT:
      t4Daq->analysePmt(&t4Event->pmt, input);
      break;
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__, "T4EventManager::analyse: Unknown readout type.");
      break;
  }
}

void T4EventManager::setBeamData(T4BeamData* _in)
{
  T4BeamData* beamData = getBeamData();
  for (int i = 0; i < 3; i++)
    beamData->vertexPosition[i] = _in->vertexPosition[i];
  beamData->vertexTime = _in->vertexTime;
  beamData->generator = _in->generator;
  beamData->aux = _in->aux;
  beamData->x_bj = _in->x_bj;
  beamData->y = _in->y;
  beamData->w2 = _in->w2;
  beamData->q2 = _in->q2;
  beamData->nu = _in->nu;

  for (unsigned int i = 0; i < 20; i++)
    beamData->uservar[i] = _in->uservar[i];
  for (unsigned int i = 0; i < 40; i++)
    beamData->lst[i] = _in->lst[i];
  for (unsigned int i = 0; i < 30; i++)
    beamData->parl[i] = _in->parl[i];
  for (unsigned int i = 0; i < 14; i++)
    beamData->cut[i] = _in->cut[i];

  beamData->nBeamParticle = _in->nBeamParticle;
  beamData->beamParticles.clear();
  for (unsigned int i = 0; i < beamData->nBeamParticle; i++)
    beamData->beamParticles.push_back(_in->beamParticles.at(i));

  beamData->nTrajectories = _in->nTrajectories;
  beamData->trajectories.clear();
  for (unsigned int i = 0; i < beamData->nTrajectories; i++)
    beamData->trajectories.push_back(_in->trajectories.at(i));
}
