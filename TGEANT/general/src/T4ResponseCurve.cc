#include "T4ResponseCurve.hh"

T4ResponseCurve::T4ResponseCurve(void)
{
  T4SettingsFile* settingsFile = T4SettingsFile::getInstance();
  randMan = settingsFile->getRandom();
  setBialkali();

}

void T4ResponseCurve::setBialkali(void)
{
  responsePoints.clear();

  // energy , probability
  addPoint(1.772 * CLHEP::eV, 0.0004); // 700nm
  addPoint(1.825 * CLHEP::eV, 0.002); // 680nm
  addPoint(2.001 * CLHEP::eV, 0.01); // 620nm
  addPoint(2.068 * CLHEP::eV, 0.03); // 600nm
  addPoint(2.256 * CLHEP::eV, 0.07); // 550nm
  addPoint(2.386 * CLHEP::eV, 0.1); // 520nm
  addPoint(2.481 * CLHEP::eV, 0.15); // 500nm
  addPoint(2.585 * CLHEP::eV, 0.2); // 480nm
  addPoint(3.026 * CLHEP::eV, 0.25); // 410nm
  addPoint(3.265 * CLHEP::eV, 0.25); // 380nm
  addPoint(3.545 * CLHEP::eV, 0.2); // 350nm
  addPoint(3.877 * CLHEP::eV, 0.1); // 320nm
  addPoint(4.002 * CLHEP::eV, 0.07); // 310nm
  addPoint(4.136 * CLHEP::eV, 0.03); // 300nm
  addPoint(4.206 * CLHEP::eV, 0.009); // 295nm
}

G4bool T4ResponseCurve::getResponse(G4double energy)
{
  if (responsePoints.size() == 0) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "T4ResponseCurve::getResponse: ResponseCurve not set! Return no response.");
    return false;
  }

  if (energy <= responsePoints[0].first
      || responsePoints[responsePoints.size() - 1].first <= energy) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4ResponseCurve::getResponse: Particle energy out of range! Return no response. Energy: "
            + doubleToStrFP(energy) + ".");
    return false;
  }

  G4int position = 0;
  for (G4int i = 0; i < (G4int) responsePoints.size() - 1; i++) {
    if (responsePoints[i].first <= energy
        && energy <= responsePoints[i + 1].first) {
      position = i;
      break;
    }
  }

  G4double x1 = responsePoints[position].first;
  G4double x2 = responsePoints[position + 1].first;
  G4double y1 = responsePoints[position].second;
  G4double y2 = responsePoints[position + 1].second;

  G4double result = y1 + (y1 - y2) / (x1 - x2) * (energy - x1);
  G4double randomNo = randMan->flat();
  return (result > randomNo);
}

void T4ResponseCurve::addPoint(G4double x, G4double y)
{
  responsePoints.push_back(std::make_pair(x / CLHEP::eV, y));
  if (responsePoints.size() > 1
      && responsePoints.at(responsePoints.size() - 2).first
          > responsePoints.back().first) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "Error in T4ResponseCurve: ResponseCurve not continuous.");
  }
}
