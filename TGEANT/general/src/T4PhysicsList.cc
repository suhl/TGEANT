#include "T4PhysicsList.hh"

T4PhysicsList::T4PhysicsList(TGEANT::PhysicsList _physicsList)
{
  settingsFile = T4SettingsFile::getInstance();
  T4SMessenger::getReference().printMessage(T4SNotice, __LINE__, __FILE__,
      "Loading T4PhysicsList. Mode: "
          + settingsFile->getStructManager()->getGeneral()->physicsList);

  physicsList = _physicsList;
  tgeantProcess = NULL;
  inelasticProcess = NULL;

  decay = NULL;
  stepLimiter = NULL;

  // optical
  scintillation = NULL;
  cerenkov = NULL;
  absorption = NULL;
  rayleigh = NULL;
  boundary = NULL;

  gflash = NULL;

  verboseLevel = 0;
  SetVerboseLevel(verboseLevel);
}

T4PhysicsList::~T4PhysicsList(void)
{
  if (tgeantProcess != NULL)
    delete tgeantProcess;
  if (inelasticProcess != NULL)
    delete inelasticProcess;

  if (decay != NULL)
    delete decay;

  if (stepLimiter != NULL)
    delete stepLimiter;

  for (unsigned int i = 0; i < userSpecialCuts.size(); i++)
    delete userSpecialCuts.at(i);
  userSpecialCuts.clear();

  for (unsigned int i = 0; i < g4VProcess.size(); i++)
    delete g4VProcess.at(i).first;
  g4VProcess.clear();

  for (unsigned int i = 0; i < hadronPhysics.size(); i++)
    delete hadronPhysics.at(i);
  hadronPhysics.clear();

  // optical
  if (scintillation != NULL)
    delete scintillation;
  if (cerenkov != NULL)
    delete cerenkov;
  if (absorption != NULL)
    delete absorption;
  if (rayleigh != NULL)
    delete rayleigh;
  if (boundary != NULL)
    delete boundary;

  if (gflash != NULL)
    delete gflash;

  T4SMessenger::getReference().setupStream(T4SNotice, __LINE__, __FILE__);
  T4SMessenger::getReference() << "Deleting T4PhysicsList, ";
}

void T4PhysicsList::ConstructParticle(void)
{
  G4LeptonConstructor leptonConstructor;
  leptonConstructor.ConstructParticle();
  G4BosonConstructor bosonConstructor;
  bosonConstructor.ConstructParticle();
  G4MesonConstructor mesonConstructor;
  mesonConstructor.ConstructParticle();
  G4BaryonConstructor baryonConstructor;
  baryonConstructor.ConstructParticle();
  G4IonConstructor ionConstructor;
  ionConstructor.ConstructParticle();
  G4ShortLivedConstructor shortLivedConstructor;
  shortLivedConstructor.ConstructParticle();
}

void T4PhysicsList::ConstructProcess(void)
{
  AddTransportation();

  if (settingsFile->getStructManager()->getBeam()->beamPlugin == "VisualizationMode"
      || settingsFile->geometryModeActivated())
    return;

  constructEM();
  constructDecay();

  if (physicsList != TGEANT::EM_ONLY) {
    addHadronic();
    for (unsigned int i = 0; i < hadronPhysics.size(); i++)
      hadronPhysics[i]->ConstructProcess();
  }

  string beamPlugin = settingsFile->getStructManager()->getBeam()->beamPlugin;
  T4BeamBackEnd* beamBackEnd = T4BeamPluginList::getInstance()->beamPlugins[beamPlugin];
  tgeantProcess = beamBackEnd->getEventGeneratorProcess();

  if (tgeantProcess == NULL
      && (settingsFile->getStructManager()->getBeam()->useTargetExtrap
          || settingsFile->getStructManager()->getBeam()
              ->useHadronicInteractionEGCall)) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4PhysicsList::ConstructProcess: Event generator missing!");
  }

  if (tgeantProcess != NULL) {
    theParticleIterator->reset();
    while ((*theParticleIterator)()) {
      G4ParticleDefinition* particle = theParticleIterator->value();
      if (particle->GetPDGEncoding()
          == settingsFile->getStructManager()->getBeam()->beamParticle) {
        particle->GetProcessManager()->AddDiscreteProcess(tgeantProcess);

        if (settingsFile->getStructManager()->getBeam()->useHadronicInteractionEGCall) {
          int i;
          for (i = 0; i < particle->GetProcessManager()->GetProcessList()->size(); i++) {
            if (dynamic_cast<G4HadronInelasticProcess*>(particle->GetProcessManager()->GetProcessList()->operator ()(i)))
              break;
          }

          G4VDiscreteProcess* geantProcess = (G4VDiscreteProcess*) particle ->GetProcessManager()->RemoveProcess(i);
          inelasticProcess = new T4PrimaryInelasticProcess(tgeantProcess, geantProcess);
          particle->GetProcessManager()->AddDiscreteProcess(inelasticProcess);
        }
      }
    }
  }

  if (settingsFile->getStructManager()->getBeam()->useHadronicInteractionEGCall
      && inelasticProcess == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4PhysicsList::ConstructProcess: The useHadronicInteractionEGCall option was enabled but the override of the Geant4 hadronic inelastic process for the beam particle was not successful!");
  }

  if (settingsFile->getStructManager()->getGeneral()->useGflash)
    addGflash();

  if (physicsList == TGEANT::FULL)
    constructOptical();

  stepLimiter = new G4StepLimiter();
  theParticleIterator->reset();
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    particle->GetProcessManager()->AddDiscreteProcess(stepLimiter);
    userSpecialCuts.push_back(new G4UserSpecialCuts());
    particle->GetProcessManager()->AddProcess(userSpecialCuts.back(), -1, -1,  1);
  }
}

void T4PhysicsList::constructEM(void)
{
  // from example N03
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  theParticleIterator->reset();
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4String particleName = particle->GetParticleName();

    if (particleName == "gamma") {
      // gamma
      g4VProcess.push_back(std::make_pair(new G4PhotoElectricEffect, particle));
      g4VProcess.push_back(std::make_pair(new G4ComptonScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4GammaConversion, particle));

    } else if (particleName == "e-") {
      //electron
      g4VProcess.push_back(std::make_pair(new G4eMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4eIonisation, particle));
      g4VProcess.push_back(std::make_pair(new G4eBremsstrahlung, particle));

    } else if (particleName == "e+") {
      //positron
      g4VProcess.push_back(std::make_pair(new G4eMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4eIonisation, particle));
      g4VProcess.push_back(std::make_pair(new G4eBremsstrahlung, particle));
      g4VProcess.push_back(std::make_pair(new G4eplusAnnihilation, particle));

    } else if (particleName == "mu+" || particleName == "mu-") {
      //muon
      g4VProcess.push_back(
          std::make_pair(new G4MuMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4MuIonisation, particle));
      g4VProcess.push_back(std::make_pair(new G4MuBremsstrahlung, particle));
      g4VProcess.push_back(std::make_pair(new G4MuPairProduction, particle));

    } else if (particleName == "proton" || particleName == "pi-"
        || particleName == "pi+") {
      //proton
      g4VProcess.push_back(std::make_pair(new G4hMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4hIonisation, particle));
      g4VProcess.push_back(std::make_pair(new G4hBremsstrahlung, particle));
      g4VProcess.push_back(std::make_pair(new G4hPairProduction, particle));

    } else if (particleName == "alpha" || particleName == "He3") {
      //alpha
      g4VProcess.push_back(std::make_pair(new G4hMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4ionIonisation, particle));

    } else if (particleName == "GenericIon") {
      //Ions
      g4VProcess.push_back(std::make_pair(new G4hMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4ionIonisation, particle));

    } else if ((!particle->IsShortLived()) && (particle->GetPDGCharge() != 0.0)
        && (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
      g4VProcess.push_back(std::make_pair(new G4hMultipleScattering, particle));
      g4VProcess.push_back(std::make_pair(new G4hIonisation, particle));
    }
  }

  for (unsigned int i = 0; i < g4VProcess.size(); i++)
    ph->RegisterProcess(g4VProcess.at(i).first, g4VProcess.at(i).second);
}

void T4PhysicsList::constructDecay(void)
{
  // from example N03
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  // Add Decay Process
  decay = new G4Decay();
  theParticleIterator->reset();
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    if (decay->IsApplicable(*particle)) {
      ph->RegisterProcess(decay, particle);
    }
  }
}

void T4PhysicsList::addHadronic(void)
{
  hadronPhysics.push_back(new G4HadronPhysicsFTFP_BERT(verboseLevel));
  G4NeutronTrackingCut* input = new G4NeutronTrackingCut(verboseLevel);
  input->SetKineticEnergyLimit(10.0 * CLHEP::MeV);
  input->SetTimeLimit(0.1 * CLHEP::ms);
  hadronPhysics.push_back(input);
  hadronPhysics.push_back(new G4StoppingPhysics(verboseLevel));

  hadronPhysics.push_back(new G4EmExtraPhysics(verboseLevel));
  hadronPhysics.push_back(new G4HadronElasticPhysics(verboseLevel));
}

void T4PhysicsList::addGflash(void)
{
  gflash = new G4FastSimulationManagerProcess();

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    pmanager->AddDiscreteProcess(gflash);
  }
}

void T4PhysicsList::constructOptical(void)
{
  scintillation = new G4Scintillation();
  cerenkov = new G4Cerenkov();
  absorption = new G4OpAbsorption();
  rayleigh = new G4OpRayleigh();
  boundary = new G4OpBoundaryProcess();

  scintillation->AddSaturation(G4LossTableManager::Instance()->EmSaturation());

  theParticleIterator->reset();
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    if (scintillation->IsApplicable(*particle))
      this->RegisterProcess(scintillation, particle);
    if (cerenkov->IsApplicable(*particle))
      this->RegisterProcess(cerenkov, particle);

    if (particle == G4OpticalPhoton::OpticalPhotonDefinition()) {
      this->RegisterProcess(absorption, particle);
      this->RegisterProcess(rayleigh, particle);
      this->RegisterProcess(boundary, particle);
    }
  }
}

void T4PhysicsList::SetCuts(void)
{
  if (settingsFile->getStructManager()->getGeneral()->noSecondaries) {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "T4PhysicsList::SetCuts: The option 'noSecondaries' is enabled. \nNo secondary particles from electromagnetic processes will be generated. \nThe energy conservation will be violated!");
    defaultCutValue = DBL_MAX;
  } else {
    defaultCutValue = settingsFile->getStructManager()->getGeneral()
        ->productionCutsGlobal * CLHEP::mm;

    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
        "T4PhysicsList::SetCuts: Default cut value set to "
            + doubleToStrFP(defaultCutValue) + "mm, for ECALS (Gams, RHGams, Mainz, Olga, Shashlik) to "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsGams * CLHEP::mm) + "mm, "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsRHGams * CLHEP::mm) + "mm, "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsMainz * CLHEP::mm) + "mm, "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsOlga * CLHEP::mm) + "mm, "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsShashlik * CLHEP::mm)
            + "mm and HCAL to "
            + doubleToStrFP(settingsFile->getStructManager()->getGeneral()->productionCutsHcal * CLHEP::mm) + "mm.");
  }

  SetCutsWithDefault();
}

G4ParticleDefinition* T4PhysicsList::getParticleByID(G4int id)
{
  theParticleIterator->reset();
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    if (particle->GetPDGEncoding() == id) {
      return particle;
    }
  }

  T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
  __FILE__,
      "T4PhysicsList::getParticleByID: Unknown particle ID. Return G4Geantino.");
  return G4Geantino::GeantinoDefinition();
}

G4VParticleChange* T4PrimaryInelasticProcess::PostStepDoIt(
    const G4Track& aTrack, const G4Step& aStep)
{
  G4VParticleChange* particleChange = geantProcess->PostStepDoIt(aTrack, aStep);

  if (aTrack.GetTrackID() == 1) {
    if (particleChange->GetTrackStatus() == fStopAndKill) {
      particleChange->Clear();
      T4TargetTracking::getInstance()->activateProcessNow();
      particleChange = tgeantProcess->PostStepDoIt(aTrack, aStep);
    }
  }

  return particleChange;
}

G4double T4PrimaryInelasticProcess::PostStepGetPhysicalInteractionLength(
    const G4Track& a, G4double b , G4ForceCondition* c)
{
  return geantProcess->PostStepGetPhysicalInteractionLength(a, b, c);
}

G4double T4PrimaryInelasticProcess::GetMeanFreePath(const G4Track&, G4double,
    G4ForceCondition*)
{
  cout<<"T4PrimaryInelasticProcess::GetMeanFreePath -- We have to check if this function gets ever called!"<<endl;
  return DBL_MAX;
}
