#include "T4MemoryManager.hh"
#include "T4SMessenger.hh"

T4MemoryManager* T4MemoryManager::instance = 0;
char memBuffer[400];

void T4MemoryManager::addBlock(void)
{
  blockStruct newBlock;
  newBlock.dataArray = new SdInformation[blockSize];
  //allocate in big blocks
  newBlock.index = 0;
  memoryBank.push_back(newBlock);
  T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__, __FILE__,
      "T4MemoryManager -- adding a new block!");
}

void T4MemoryManager::printFill()
{
  T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__,
      __FILE__, "T4MemoryManager -- printing Debug!");
  for (int i = 0; i < memoryBank.size(); i++) {
    sprintf(memBuffer, "Block %i: Filling at: %i of %i Ratio: %f", i,
        memoryBank.at(i).index, blockSize,
        (float) memoryBank.at(i).index / blockSize);
    T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__,
        __FILE__, memBuffer);
  }
}

SdInformation* T4MemoryManager::getNextIndex(void)
{
  if (memoryBank[index].index == blockSize - 1) {
    if (memoryBank.size() - 1 == index)
      addBlock();
    index++;
  }
  //disabled for performance
  //sprintf("Index in Bank: %i, Index Overall: %i \n",memoryBank[index].index,index);

  return &(memoryBank[index].dataArray[memoryBank[index].index++]);
}

T4MemoryManager::T4MemoryManager()
{
  blockSize = 100000;
  cleanUpCount = 50;
  addBlock();
  index = 0;
}

T4MemoryManager::~T4MemoryManager()
{
  for (int i = 0; i < memoryBank.size(); i++)
    delete[] memoryBank.at(i).dataArray;
  memoryBank.clear();
}

void T4MemoryManager::resetIndex(void)
{
  bool foundUsage = false;
  for (int i = 0; i < memoryBank.size(); i++) {
    if (memoryBank.at(i).index == 0 && !foundUsage) {
      usageMonitor.push_back(i);
      foundUsage = true;
    }
    memoryBank.at(i).index = 0;
  }

  if (usageMonitor.size() == cleanUpCount)
    cleanUp();
  index = 0;
}

void T4MemoryManager::cleanUp(void)
{

  int usage = 0;
  for (int i = 0; i < usageMonitor.size(); i++)
    usage += usageMonitor.at(i);
  usage /= usageMonitor.size();
  usage++;
  usageMonitor.clear();
  sprintf(memBuffer,
      "--- T4MemoryManager cleanup: Active Blocks: %i, Mean used Blocks+1: %i, deleting the rest\n");
  T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__,
      __FILE__, memBuffer);

  for (int i = usage; i < memoryBank.size(); i++) {
    delete[] memoryBank.at(i).dataArray;
    memoryBank.at(i).index = 0;
  }
  memoryBank.resize(usage);
}

void T4MemoryManager::process_mem_usage(double& vm_usage, double& resident_set)
{
  using std::ios_base;
  using std::ifstream;
  using std::string;

  vm_usage = 0.0;
  resident_set = 0.0;

  // 'file' stat seems to give the most reliable results
  //
  ifstream stat_stream("/proc/self/stat", ios_base::in);

  // dummy vars for leading entries in stat that we don't care about
  //
  string pid, comm, state, ppid, pgrp, session, tty_nr;
  string tpgid, flags, minflt, cminflt, majflt, cmajflt;
  string utime, stime, cutime, cstime, priority, nice;
  string O, itrealvalue, starttime;

  // the two fields we want
  //
  unsigned long vsize;
  long rss;

  stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
      >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt >> utime
      >> stime >> cutime >> cstime >> priority >> nice >> O >> itrealvalue
      >> starttime >> vsize >> rss; // don't care about the rest

  stat_stream.close();

  long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
  vm_usage = vsize / 1024.0;
  resident_set = rss * page_size_kb;
}
