#include "T4TrackingAction.hh"

void T4TrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
  // We do not save optical photons (id=0).
  // [ Possible option for comgeant style: Do not allow electron (id=11) trajectories. ]
  // [ In this case one has to patch T4Daq (save parentId instead of trackId)          ]
  // [ and T4EventManager::EndOfEventAction (in the same way like optical photons)     ]

  if (aTrack->GetParticleDefinition()->GetPDGEncoding() != 0 /*&& aTrack->GetParticleDefinition()->GetPDGEncoding() != 11*/) {
    fpTrackingManager->SetStoreTrajectory(true);
    fpTrackingManager->SetTrajectory(new T4TrajectoryHandler(aTrack));
  } else {
    fpTrackingManager->SetStoreTrajectory(useVisualization);
  }
}
