#include "T4SensitiveCalorimeter.hh"

G4bool T4SensitiveCalorimeter::ProcessHits(G4GFlashSpot* aSpot,
    G4TouchableHistory*)
{
  G4double totalEnergyDeposit = aSpot->GetEnergySpot()->GetEnergy();

  // non-zero energy deposit
  if (totalEnergyDeposit == 0.0)
    return false;

  const G4Track* track = aSpot->GetOriginatorTrack()->GetPrimaryTrack();
  G4StepPoint* preStepPoint = track->GetStep()->GetPreStepPoint();

  if (hitCollection.sdInformation.size() == hitCollection.sdIndex)
    hitCollection.sdInformation.resize(hitCollection.sdIndex + 50);

 //get new space from memory manager
  SdInformation* writeTo = T4MemoryManager::getInstance()->getNextIndex();
  //remember the address we write to
  hitCollection.sdInformation[hitCollection.sdIndex] = writeTo;
  

  writeTo->particleId = track->GetParticleDefinition()->GetPDGEncoding();
  writeTo->trackId = track->GetTrackID();
  writeTo->parentId = track->GetParentID();
  writeTo->particleEnergy = preStepPoint->GetTotalEnergy();
  writeTo->time = preStepPoint->GetGlobalTime();
  writeTo->energyDeposit = totalEnergyDeposit * energyScale;
  writeTo->beta = preStepPoint->GetBeta();
  writeTo->hitPosition = preStepPoint->GetPosition();
  writeTo->lastPosition = cellPosition;
  writeTo->particleMomentum = preStepPoint->GetMomentum();

  hitCollection.sdIndex++;

  Draw(writeTo->hitPosition);
  return true;
}

