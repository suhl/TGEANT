#include "T4Daq.hh"
#include "RichConstruction.hh"
#include "T4PhotoMultiplier.hh"

T4Daq::T4Daq(std::vector<G4int>* _trackIds)
{
  trackIds = _trackIds;
  T4SettingsFile* settingsFile = T4SettingsFile::getInstance();

  randMan = settingsFile->getRandom();
  responseCurve = NULL;
  useOptical = settingsFile->isOpticalPhysicsActivated();

  if (useOptical) {
    windowSize = settingsFile->getStructManager()->getSampling()->windowSize; // in ns
    nsPerBin = settingsFile->getStructManager()->getSampling()
        ->binsPerNanoSecond;
    useBaseLine = settingsFile->getStructManager()->getSampling()->useBaseline;
    baseLine = settingsFile->getStructManager()->getSampling()->baseline;

    responseCurve = new T4ResponseCurve();
    nDigits = windowSize / nsPerBin;
    TT = 0.0;
    TTS = 0.0;
    randomNo = 0;
    randomTime = 0.0;
  }
}

T4Daq::~T4Daq(void)
{
  if (responseCurve != NULL)
    delete responseCurve;
}

void T4Daq::synchronizePMT(void)
{
  if (useOptical) {
    // synchronize all GANDLF modules for each event
    randomNo = (G4int) floor(randMan->flat() * 1000 * nsPerBin); // ps
    randomTime = (G4double) randomNo / 1000 * CLHEP::ns; // ps->ns
  }
}

G4double T4Daq::getEnergy(T4HitElement* element)
{
  G4double energy = 0;
  for (unsigned int i = 0; i < element->sdInfo.size(); i++)
    energy += element->sdInfo.at(i)->energyDeposit;

  if (element->children.size() == 0)
    return energy;
  else {
    for (unsigned int i = 0; i < element->children.size(); i++)
      energy += getEnergy(element->children.at(i));

    return energy;
  }
}

void T4Daq::analyseHit(std::vector<T4HitData>* output, T4HitCollection* input,
    G4double minParticleEnergy)
{
  if (input->sdIndex == 0)
    return;

  std::map<G4int, T4HitElement> hits;
  std::vector<T4HitElement*> realHits;

  // create map for all trackIds and fill T4HitElement with sdInfo
  vector<SdInformation*>::iterator sdIterator;
  unsigned int myIndex = 0;
  
  for (sdIterator = input->sdInformation.begin();
      sdIterator != input->sdInformation.end(); sdIterator++) {
    if (myIndex++ == input->sdIndex)
      break;
    hits[(*sdIterator)->trackId].sdInfo.push_back((*sdIterator));
    hits[(*sdIterator)->trackId].parent = NULL;
  }

  // loop all trackIds and search for parentIds
  // no parent id found: real hit
  // else: link parent and children
  std::map<G4int, T4HitElement>::iterator mapIterator;
  for (mapIterator = hits.begin(); mapIterator != hits.end(); mapIterator++) {
    std::map<G4int, T4HitElement>::iterator parent = hits.find(
        mapIterator->second.sdInfo.at(0)->parentId);
    if (parent == hits.end()) {
      //real hit
      realHits.push_back(&(mapIterator->second));
    } else {
      mapIterator->second.parent = &(parent->second);
      parent->second.children.push_back(&(mapIterator->second));
    }
  }

  // loop over real hits
  std::vector<T4HitElement*>::iterator hIterator;
  for (hIterator = realHits.begin(); hIterator != realHits.end(); hIterator++) {
    T4HitData hitData;
    hitData.detectorName = input->detectorName;
    hitData.detectorId = input->detectorId;
    hitData.channelNo = input->channelNo;
    SdInformation* firstHit = (*hIterator)->sdInfo.at(0);
    hitData.particleEnergy = firstHit->particleEnergy;
    if (hitData.particleEnergy < minParticleEnergy)
      continue;
    hitData.particleId = firstHit->particleId;
    hitData.trackId = firstHit->trackId;
    hitData.time = firstHit->time;
    hitData.beta = firstHit->beta;
    setThreeVector<double>(hitData.primaryHitPosition,
        (double)firstHit->hitPosition.getX(), (double)firstHit->hitPosition.getY(),
        (double)firstHit->hitPosition.getZ());
    setThreeVector<double>(hitData.momentum, firstHit->particleMomentum.getX(),
        firstHit->particleMomentum.getY(), firstHit->particleMomentum.getZ());
    hitData.energyDeposit = getEnergy(*hIterator);

    G4ThreeVector lastHit = (*hIterator)->sdInfo.at(
        (*hIterator)->sdInfo.size() - 1)->lastPosition;
    setThreeVector<double>(hitData.lastHitPosition, lastHit.getX(), lastHit.getY(),
        lastHit.getZ());
    setThreeVector<double>(hitData.hitPosition,
        (hitData.primaryHitPosition[0] + hitData.lastHitPosition[0]) / 2,
        (hitData.primaryHitPosition[1] + hitData.lastHitPosition[1]) / 2,
        (hitData.primaryHitPosition[2] + hitData.lastHitPosition[2]) / 2);

    output->push_back(hitData);
    if (input->second_detectorId != 0) {
      hitData.detectorId = input->second_detectorId;
      output->push_back(hitData);
    }

    trackIds->push_back(hitData.trackId);
  }
}

void T4Daq::analyseCalorimeter(std::vector<T4HitData>* output,
    T4HitCollection* input, G4double minEnergyDeposit)
{
  if (input->sdIndex == 0)
    return;

  G4double energyDeposit = 0;
  G4int minTrackNo = 0;

  for (unsigned int i = 0; i < input->sdIndex; i++) {
    if (input->sdInformation.at(i)->trackId
        < input->sdInformation.at(minTrackNo)->trackId)
      minTrackNo = i;

    energyDeposit += input->sdInformation.at(i)->energyDeposit;
  }
  if (energyDeposit < minEnergyDeposit)
    return;

  T4HitData hitData;
  hitData.detectorName = input->detectorName;
  hitData.detectorId = input->detectorId;
  hitData.channelNo = input->channelNo;
  SdInformation* firstHit = input->sdInformation.at(minTrackNo);
  hitData.particleId = firstHit->particleId;
  hitData.trackId = firstHit->trackId;
  hitData.particleEnergy = firstHit->particleEnergy;
  hitData.time = firstHit->time;
  hitData.beta = firstHit->beta;

  setThreeVector<double>(hitData.hitPosition, firstHit->hitPosition.getX(),
      firstHit->hitPosition.getY(), firstHit->hitPosition.getZ());
  setThreeVector<double>(hitData.momentum, firstHit->particleMomentum.getX(),
      firstHit->particleMomentum.getY(), firstHit->particleMomentum.getZ());
  hitData.energyDeposit = energyDeposit;

  for (int i = 0; i < 3; i++)
    hitData.primaryHitPosition[i] = hitData.hitPosition[i];

  // this is the cell position
  setThreeVector<double>(hitData.lastHitPosition, firstHit->lastPosition.getX(),
      firstHit->lastPosition.getY(), firstHit->lastPosition.getZ());

  output->push_back(hitData);
  trackIds->push_back(hitData.trackId);

  T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__,
  __FILE__,
      "T4Daq::analyseCalorimeter: Hit in "
          + (string) hitData.detectorName + ", channel number "
          + intToStr(hitData.channelNo) + " with Energy of "
          + doubleToStrFP(hitData.energyDeposit / CLHEP::GeV) + " GeV.");
}

void T4Daq::analysePmt(std::vector<T4PmtData>* output, T4HitCollection* input,
    G4bool useResponseCurve)
{
  if (!useOptical) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "T4Daq::analysePmt: Physics List isn't in mode FULL. PMT readout not supported.");
    return;
  }

  if (input->sdIndex == 0)
    return;

  T4PmtData pmtData;
  pmtData.detectorName = input->detectorName;
  pmtData.detectorId = input->detectorId;
  pmtData.channelNo = input->channelNo;
  pmtData.firstEventTime = input->sdInformation.at(0)->time;
  pmtData.randomTime = 0;
  pmtData.nsPerBin = 1;
  pmtData.windowSize = 1;
  
  int photonCounter = 0;
  for (unsigned int i = 0; i < input->sdIndex; i++) { // all hits
    if (input->sdInformation.at(i)->particleId != 0)
      continue; // check opticalphoton (PDG-ID 0)
    bool doubleId = false;
    for (unsigned int j = 0; j < i; j++) { // check for multi trackId
      if (input->sdInformation.at(i)->trackId
          == input->sdInformation.at(j)->trackId) {
        doubleId = true;
        break;
      }
    }
    if (doubleId == true)
      continue;  
    
    photonCounter++;
  }
  
  pmtData.digits.push_back(photonCounter);
  output->push_back(pmtData);

  // OLD CODE BELOW (depending on ROOT functions, written in 2012)
  /*
  T4PhotoMultiplier photoMultiplier;
  
  if (!useResponseCurve)
    T4SMessenger::getInstance()->printMessage(T4SVerboseMoreMore, __LINE__,
        __FILE__,
        "T4Daq::analysePmt: " + intToStr(input->sdInformation.size())
            + " photons in detector " + input->detectorName + ".");

  T4PmtData pmtData;
  pmtData.detectorName = input->detectorName;
  pmtData.detectorId = input->detectorId;
  pmtData.channelNo = input->channelNo;

  TT = photoMultiplier.getTT(input->pmtType);
  TTS = photoMultiplier.getTTS(input->pmtType);

  G4double firstEventTime = 0.0 * CLHEP::ns; // default if no event in PMT

  // 1 sample == 1ps, range: 0 - windowSize_ /ns
  int nBins = windowSize + nsPerBin; // +nsPerBin_ if randomTime = nsPerBin_
   TH1D* myHist = new TH1D("myHist", "", nBins * 1000, 0, nBins); // ps resolution

  for (unsigned int i = 0; i < input->sdIndex; i++) { // all hits
    if (input->sdInformation.at(i)->particleId != 0)
      continue; // check opticalphoton (PDG-ID 0)
    G4bool doubleId = false;
    for (unsigned int j = 0; j < i; j++) { // check for multi trackId
      if (input->sdInformation.at(i)->trackId
          == input->sdInformation.at(j)->trackId) {
        doubleId = true;
        break;
      }
    }
    if (doubleId == true)
      continue;

    if (useResponseCurve) {
      responseCurve->setBialkali();
      G4bool response = responseCurve->getResponse(
          input->sdInformation.at(i)->particleEnergy / CLHEP::eV);
      if (!response)
        continue; // check response
    }
    T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"CANNOT USE PMT READOUT RIGHT NOW!\n");

    G4double randomTransitTime = 0.0;
     CLHEP::RandLandau::shoot(randMan->getTheEngine(),TT, TTS);
    
     myHist->Fill(
         input->sdInformation.at(i)->time / CLHEP::ns + randomTransitTime);
  }

   for (int i = 0; i < nBins * 1000; i++) { // find firstEventTime
     if (myHist->GetBinContent(i) > 0) {
       firstEventTime = (G4double) i / 1000;
       break;
     }
   }

  pmtData.randomTime = randomTime;
  pmtData.firstEventTime = firstEventTime;
  pmtData.nsPerBin = nsPerBin;
  pmtData.windowSize = windowSize;

   for (G4int i = 0; i < nDigits; i++) { // digits
     G4int value = 0;
     for (G4int j = 0; j < 1000 * nsPerBin; j++) { // nsPerBin_ in ps
       value += (G4int) myHist->GetBinContent(
           randomNo + i * 1000 * nsPerBin + j); //value = #gamma
     }
     if (useBaseLine) {
       value += (G4int) (baseLine + floor(CLHEP::RandGauss::shoot(randMan->getTheEngine(),0,2)+0.5));
     }
     pmtData.digits.push_back(value);
   }
   delete myHist;

  output->push_back(pmtData);
  */
}
