#ifndef T4SIGNALMANAGER_HH
#define T4SIGNALMANAGER_HH

#include <iostream>
#include <string>

#include <csignal>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>

#include "T4SMessenger.hh"
#include "G4RunManager.hh"

class T4SignalManager
{
  public:
    ~T4SignalManager(void) {instanceFlag = false;}
    static T4SignalManager* getInstance();

    friend void interruptHandler(int sig);
    friend void terminationHandler(int sig);

  private:
    bool interruptedOnce;
    bool terminatedOnce;
    static bool instanceFlag;
    static T4SignalManager* single;
    T4SignalManager(void);
};

#endif
