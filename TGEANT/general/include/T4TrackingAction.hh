#ifndef T4TRACKINGACTION_HH_
#define T4TRACKINGACTION_HH_

#include "G4UserTrackingAction.hh"
#include "G4TrackingManager.hh"

#include "T4SettingsFile.hh"
#include "T4Event.hh"
#include "T4TrajectoryHandler.hh"
#include "T4SMessenger.hh"

class T4TrackingAction : public G4UserTrackingAction
{
  public:
    T4TrackingAction(void) {useVisualization = T4SettingsFile::getInstance()->getStructManager()->getGeneral()->useVisualization;}
    virtual ~T4TrackingAction(void) {T4SMessenger::getReference() << "T4TrackingAction, ";}

    void PreUserTrackingAction(const G4Track*);

  private:
    G4bool useVisualization;
};

#endif /* T4TRACKINGACTION_HH_ */
