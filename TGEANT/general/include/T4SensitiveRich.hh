#ifndef T4SENSITIVERICH_HH_
#define T4SENSITIVERICH_HH_

#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"

#include "T4EventManager.hh"
#include "T4SensitiveDetector.hh"

struct T4RichPCB
{
    G4ThreeVector position;
    G4String name;
    G4int id;
};

class T4SensitiveRich : public G4VSensitiveDetector
{
  public:
    T4SensitiveRich(T4RichPCB);
    virtual ~T4SensitiveRich(void) {};

    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

  private:
    T4EventManager* eventManager;
    T4RichData richData;
    G4ThreeVector padPosition;
    vector<G4int> trackList;

    T4TrajectoryHandler* getParentTrajectory(G4int parentId);
};

#endif /* T4SENSITIVERICH_HH_ */
