#ifndef T4DAQ_HH_
#define T4DAQ_HH_

#include "T4SettingsFile.hh"
#include "T4Globals.hh"
#include "T4ResponseCurve.hh"
#include "T4Event.hh"


#include "CLHEP/Random/RandomEngine.h"
#include "CLHEP/Random/RanluxEngine.h"
#include "CLHEP/Random/Random.h"


#include "T4PerformanceMonitor.hh"

class T4Daq
{
  public:
    T4Daq(std::vector<G4int>* trackIds);
    virtual ~T4Daq(void);

    void analyseHit(std::vector<T4HitData>*, T4HitCollection*, G4double minParticleEnergy);
    void analyseCalorimeter(std::vector<T4HitData>*, T4HitCollection*, G4double minEnergyDeposit);

    void analysePmt(std::vector<T4PmtData>*, T4HitCollection*, G4bool useResponseCurve = true);
    void synchronizePMT(void);

  private:
    G4double getEnergy(T4HitElement*);

    G4bool useOptical;
    
    CLHEP::HepRandom* randMan;
    
    T4ResponseCurve* responseCurve;
    std::vector<G4int>* trackIds;
    G4int nDigits;
    G4int windowSize;
    G4int nsPerBin;
    G4double TT;
    G4double TTS;
    G4bool useBaseLine;
    G4double baseLine;
    G4int randomNo;
    G4double randomTime;
};

#endif /* T4DAQ_HH_ */
