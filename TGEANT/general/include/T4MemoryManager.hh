#ifndef T4MEMORYMANAGER_HH_
#define T4MEMORYMANAGER_HH_

#include <cstdio>
#include <string>
#include <vector>
#include <ios>
#include <iostream>
#include <fstream>

#include <unistd.h>

#include <T4Globals.hh>

using namespace std;

/*! \brief This struct holds sdInformation structs in the heap in blocks and an index to the currently used element
 */
typedef struct
{
    SdInformation* dataArray;
    int index;
} blockStruct;

/*! \class T4MemoryManager
 *  \brief This class implements a simple heap memory management for the sdInformation structs
 *  These Structs get filled by every detector themself during the event and are read out at the end.
 *  By keeping them in blocks we save performance and stop the dynamic allocating during the events for every detector
 */
class T4MemoryManager
{
  public:
    /*! \brief the getInstance function returns the currently active instance for this singleton class */
    static T4MemoryManager* getInstance()
    {
      if (!instance)
        instance = new T4MemoryManager();
      return instance;
    }

    /*! \brief this function returns a pointer to the next free index */
    SdInformation* getNextIndex(void);
    /*! \brief this function resets the indizes of every block allocated and the index of the active block to zero */
    void resetIndex(void);

    /*! \brief this function prints a debug information about blocks in use and their  filling */
    void printFill();
    /*! \brief get and return the mem usage of this program */
    static void process_mem_usage(double& vm_usage, double& resident_set);

  private:
    T4MemoryManager();
    ~T4MemoryManager();
    /*! \brief The instance in use */
    static T4MemoryManager* instance;

    /*! \brief size of the blocks to allocate at once, is set in the constructor */
    int blockSize;
    /*! \brief allocates a new block of blockSize sdInformation structs */
    void addBlock(void);
    /*! \brief the Index of the block that is currently filled */
    int index;
    /*! \brief the number of events that must pass before we clean up unused blocks */
    int cleanUpCount;
    /*! \brief this function checks the number of blocks that were filled during the last cleanUpCount events.
     *  It then frees the rest of the memory */
    void cleanUp(void);
    /*! \brief the container for the blocksStructs*/
    vector<blockStruct> memoryBank;
    /*! \brief keeps for each event the number of filled blocks */
    vector<int> usageMonitor;
};

#endif
