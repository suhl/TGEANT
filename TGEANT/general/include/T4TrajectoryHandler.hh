#ifndef T4TRAJECTORYHANDLER_HH_
#define T4TRAJECTORYHANDLER_HH_

#include "G4VTrajectory.hh"
#include "G4Allocator.hh"
#include "G4TrajectoryPoint.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4AttDefStore.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
#include "G4UIcommand.hh"
#include "G4UnitsTable.hh"

#include "T4Event.hh"

typedef std::vector<G4VTrajectoryPoint*> TrajectoryPointContainer;

class T4TrajectoryHandler : public G4VTrajectory
{
  public:
    T4TrajectoryHandler(const G4Track* aTrack);
    virtual ~T4TrajectoryHandler();

   inline void* operator new(size_t);
   inline void  operator delete(void*);
   inline int operator == (const T4TrajectoryHandler& right) const
   {return (this==&right);}

   G4int GetTrackID() const {return t4Trajectory.trackId;}
   G4int GetParentID() const {return t4Trajectory.parentId;}
   G4String GetParticleName() const {return particleName;}
   G4double GetCharge() const {return particleCharge;}
   G4int GetPDGEncoding() const {return t4Trajectory.particleId;}
   G4ThreeVector GetInitialMomentum() const {
     return G4ThreeVector(t4Trajectory.momentum[0], t4Trajectory.momentum[1], t4Trajectory.momentum[2]);}

   int GetPointEntries() const {return positionRecord->size();}
   G4VTrajectoryPoint* GetPoint(G4int i) const {return positionRecord->at(i);}

   void AppendStep(const G4Step* aStep);
   void MergeTrajectory(G4VTrajectory* secondTrajectory);

   const T4Trajectory* getTrajectory(void) const {return &t4Trajectory;}
    
  private:
    TrajectoryPointContainer* positionRecord;
    T4Trajectory t4Trajectory;
    G4double particleCharge;
    G4String particleName;
};

#if defined G4TRACKING_ALLOC_EXPORT
extern G4DLLEXPORT G4Allocator<T4TrajectoryHandler> t4TrajectoryAllocator;
#else
extern G4DLLIMPORT G4Allocator<T4TrajectoryHandler> t4TrajectoryAllocator;
#endif

inline void* T4TrajectoryHandler::operator new(size_t)
{
  void* aTrajectory;
  aTrajectory = (void*) t4TrajectoryAllocator.MallocSingle();
  return aTrajectory;
}

inline void T4TrajectoryHandler::operator delete(void* aTrajectory)
{
  t4TrajectoryAllocator.FreeSingle((T4TrajectoryHandler*) aTrajectory);
}

#endif /* T4TRAJECTORYHANDLER_HH_ */
