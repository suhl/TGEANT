#ifndef T4GLOBALS_HH_
#define T4GLOBALS_HH_

#include <config.hh>
#include <vector>
#include <sstream>
#include <iomanip>

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Vector/ThreeVector.h"

namespace TGEANT
{
  enum Alignment
  {
    UP, DOWN
  };

  enum PhysicsList
  {
    EM_ONLY = 0, STANDARD = 1, FULL = 2
  };

  enum ReadoutType
  {
    HIT = 0, // full TGEANT output for tracker
    TRIGGER = 1, // full TGEANT output for hodoscopes (same logic as for HIT)
    CALO = 2, // full TGEANT output for calorimeter
    PMT = 3, // specific PMT readout
    RICH = 4 // RICH readout
  };

  enum PmtType
  {
    ET9823B, R10533, XP2050, EMI9236KB, PMTDUMMY
  };

  enum RoationMatrix
  {
    ROT_0DEG = 1, ROT_10pDEG = 3, ROT_10nDEG = 4, ROT_90pDEG = 5,
    ROT_45pDEG = 6, ROT_30pDEG = 7, ROT_025pDEG = 9, ROT_45nDEG = 10, ROT_15nDEG = 11, ROT_XtoZ = 16
  };

  enum T4BeamFileType
  {
    Normal = 1, Halo = 2, Both = 3
  };

  enum T4CaloModule
  {
    SHASHLIK_ECAL0 = 0, SHASHLIK = 1, GAMS = 2, MAINZ = 3, OLGA = 4, GAMSRH = 5,
    HCAL1 = 6, HCAL2_36 = 7, HCAL2_40 = 8
  };
}

// DAQ objects

struct SdInformation
{
    G4int particleId;
    G4int trackId;
    G4int parentId;
    G4double particleEnergy;
    G4double time;
    G4double energyDeposit;
    G4double beta;
    G4ThreeVector hitPosition;
    G4ThreeVector lastPosition;
    G4ThreeVector particleMomentum;
};

struct T4HitCollection
{
    G4String detectorName;
    G4int detectorId;
    G4int second_detectorId;
    G4int channelNo;
    TGEANT::ReadoutType readoutType;
    TGEANT::PmtType pmtType;
    unsigned int sdIndex;
    std::vector<SdInformation*> sdInformation;
};

struct T4HitElement;
struct T4HitElement
{
    std::vector<SdInformation*> sdInfo;
    T4HitElement* parent;
    std::vector<T4HitElement*> children;
};

struct T4BeamFileParticle
{
    G4double posX;
    G4double posY;
    G4double posZ;
    G4int particleId;
    G4double energy;
    G4ThreeVector momentumDirection;
    TGEANT::T4BeamFileType beamFileType;
};

// helper for detector.dat identification
struct T4DetIdent
{
    G4String tbName;
    G4String detName;
    G4int unit;
};
T4DetIdent detIdent(G4String tbName, G4String detName, G4int unit);

#endif /* T4GLOBALS_HH_ */
