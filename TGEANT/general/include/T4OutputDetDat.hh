#ifndef T4OUTPUTDETDAT_HH_
#define T4OUTPUTDETDAT_HH_

#include "T4OutputBackEnd.hh"
#include "T4Globals.hh"
#include "T4SettingsFile.hh"

#include "T4SDetectorsDat.hh"

#include "G4RunManager.hh"

class T4OutputDetDat : public T4OutputBackEnd
{
  public:
    T4OutputDetDat(std::string _fileName)
      {saveFileName = _fileName + "_detectors.dat"; t4SDetectorsDat = new T4SDetectorsDat();}
    virtual ~T4OutputDetDat(void) {delete t4SDetectorsDat;}

    void close(void);

  private:
    T4SDetectorsDat* t4SDetectorsDat;
};

#endif /* T4OUTPUTDETDAT_HH_ */
