#ifndef T4PHYSICSLIST_HH_
#define T4PHYSICSLIST_HH_

#include "T4SettingsFile.hh"
#include "T4Globals.hh"
#include "T4BeamBackend.hh"
#include "T4ProcessBackend.hh"
#include "T4SMessenger.hh"

#include "G4VUserPhysicsList.hh"
#include "G4VProcess.hh"
#include "G4ProcessManager.hh"
#include "G4PhysicsListHelper.hh"
#include "G4StepLimiter.hh"
#include "G4UserSpecialCuts.hh"

#include "G4VPhysicsConstructor.hh"
#include "G4HadronElasticPhysics.hh"
#include "G4EmExtraPhysics.hh"
#include "G4StoppingPhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"

#include "G4Decay.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4eMultipleScattering.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"
#include "G4MuMultipleScattering.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#include "G4hMultipleScattering.hh"
#include "G4hIonisation.hh"
#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"
#include "G4ionIonisation.hh"

// optical
#include "G4Scintillation.hh"
#include "G4Cerenkov.hh"
#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4LossTableManager.hh"

// particles
#include "G4LeptonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"

// gflash
#include "G4FastSimulationManagerProcess.hh"

// dy
#include "G4PionMinusInelasticProcess.hh"


#include <assert.h>
#include <config.hh>

// This macro will be missing in Geant4.10.3 (for multi-threaded mode, which is not used in TGEANT), so we define it here
#define theParticleIterator ((this->subInstanceManager.offset[this->g4vuplInstanceID])._theParticleIterator)

class T4PrimaryInelasticProcess;

class T4PhysicsList : public G4VUserPhysicsList
{
  public:
    T4PhysicsList(TGEANT::PhysicsList);
    ~T4PhysicsList(void);

    void ConstructParticle(void);
    void ConstructProcess(void);

    void SetCuts(void);

    G4ParticleDefinition* getParticleByID(G4int id);

  private:
    TGEANT::PhysicsList physicsList;
    T4ProcessBackEnd* tgeantProcess;
    T4PrimaryInelasticProcess* inelasticProcess;
    T4SettingsFile* settingsFile;

    void constructEM(void);
    void constructDecay(void);
    void addHadronic(void);
    void addGflash(void);
    void constructOptical(void);

    G4Decay* decay;
    std::vector<G4VPhysicsConstructor*>  hadronPhysics;

    // optical
    G4Scintillation* scintillation;
    G4Cerenkov* cerenkov;
    G4OpAbsorption* absorption;
    G4OpRayleigh* rayleigh;
    G4OpBoundaryProcess* boundary;

    G4FastSimulationManagerProcess* gflash;

    G4StepLimiter* stepLimiter;
    std::vector<G4UserSpecialCuts*> userSpecialCuts;
    std::vector<std::pair<G4VProcess*, G4ParticleDefinition*> > g4VProcess;
};

class T4PrimaryInelasticProcess : public G4VDiscreteProcess
{
  public:
    T4PrimaryInelasticProcess(T4ProcessBackEnd* _tgeantProcess, G4VDiscreteProcess* _geantProcess) :
          G4VDiscreteProcess("T4PrimaryInelasticProcess", fUserDefined) {
      tgeantProcess = _tgeantProcess;
      geantProcess = _geantProcess;}

    ~T4PrimaryInelasticProcess(void) {}

    virtual G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);
    G4double PostStepGetPhysicalInteractionLength(const G4Track&, G4double,
        G4ForceCondition*);
    G4double GetMeanFreePath(const G4Track&, G4double, G4ForceCondition*);

  private:
    T4ProcessBackEnd* tgeantProcess;
    G4VDiscreteProcess* geantProcess;
};


#endif /* T4PHYSICSLIST_HH_ */
