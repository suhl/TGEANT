#ifndef T4SENSITIVEDETECTOR_HH_
#define T4SENSITIVEDETECTOR_HH_

#include <iostream>

#include "T4EventManager.hh"
#include "T4Globals.hh"
#include "T4Colour.hh"
#include "T4MemoryManager.hh"


#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"

G4String setSDName(G4String detectorName, G4int channelNo,
    TGEANT::ReadoutType readoutType);

class T4SensitiveDetector : public G4VSensitiveDetector
{
  public:
    T4SensitiveDetector(G4String detectorName, G4int channelNo,
        TGEANT::ReadoutType readoutType, G4int detectorId = 0,
        TGEANT::PmtType pmtType = TGEANT::PMTDUMMY, G4int second_detectorId = 0);
    ~T4SensitiveDetector(void) {}

    G4bool ProcessHits(G4Step*, G4TouchableHistory*);
    void EndOfEvent(G4HCofThisEvent*);

    void addDeadZoneTubs(G4ThreeVector position, G4double radius);
    void addDeadZoneBox(G4ThreeVector position, G4double xHalfSize, G4double yHalfSize);

  protected:
    void Draw(G4ThreeVector);
    
    T4EventManager* eventManager;
    T4HitCollection hitCollection;
    T4Colour* colour;

  private:
    G4bool isInDeadZone(const G4ThreeVector&);
    G4ThreeVector deadZonePositionTubs;
    G4double deadZoneRadius;
    G4ThreeVector deadZonePositionBox;
    G4double deadZoneX;
    G4double deadZoneY;
};

#endif /* T4SENSITIVEDETECTOR_HH_ */
