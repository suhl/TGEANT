#ifndef T4PERFORMANCEMONITOR_HH_
#define T4PERFORMANCEMONITOR_HH_

#include "globals.hh"
#include "sys/time.h"
#include "T4SettingsFile.hh"
#include "CLHEP/Units/SystemOfUnits.h"

class T4PerformanceMonitor
{
  public:
    static T4PerformanceMonitor* getInstance();
    enum Monitor
    {
      start, stop
    };
    virtual ~T4PerformanceMonitor(void);

    void stopLoadTimer(void);
    void eventTimer(Monitor);

    G4double getLoadTime(void);
    G4double getEventTime(void)
      {return timeForEvent / CLHEP::s;}
    G4double getTotalRunTime(void);

    void setSavedEventNo(G4int _numSavedEvents)
      {numSavedEvents = _numSavedEvents;}

  private:
    T4PerformanceMonitor(void);
    static T4PerformanceMonitor* performanceMonitor;
    G4bool useMonitor;
    timeval completeRunStart;
    timeval loadStop;
    timeval eventStart;
    timeval eventStop;
    timeval completeRunStop;

    G4bool isStopped;
    G4double timeForRun;
    G4double timeForLoad;
    G4double timeForEvent;
    G4double avgTimeForEvent;
    G4int numEvents;
    G4int numSavedEvents;
};

#endif /* T4PERFORMANCEMONITOR_HH_ */
