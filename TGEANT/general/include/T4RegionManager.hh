#ifndef T4REGIONMANAGER_HH_
#define T4REGIONMANAGER_HH_

#include <stdlib.h>

#include "G4UserLimits.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCutsTable.hh"
#include "G4LogicalVolume.hh"
#include "G4VUserRegionInformation.hh"

#include "T4SettingsFile.hh"

class T4RegionInformation;

class T4RegionManager
{
  public:
    static T4RegionManager* getInstance(void);
    virtual ~T4RegionManager(void);

    void addToRegion(G4LogicalVolume*, G4Region*);

    void addToTargetRegion(G4LogicalVolume* log) {addToRegion(log, targetRegion);}
    void addToOpticalRegion(G4LogicalVolume* log) {addToRegion(log, opticalRegion);}
    void addToAbsorberRegion(G4LogicalVolume* log) {addToRegion(log, absorberRegion);}

    G4Region* getECAL1GamsRegion(T4RegionInformation* info = NULL);
    G4Region* getECAL2GamsRegion(T4RegionInformation* info = NULL);
    G4Region* getECAL2GamsRhRegion(T4RegionInformation* info = NULL);
    G4Region* getECAL1OlgaRegion(T4RegionInformation* info = NULL);
    G4Region* getECAL1MainzRegion(T4RegionInformation* info = NULL);
    G4Region* getEcal0Region(T4RegionInformation* info = NULL);
    G4Region* getHcal1Region(T4RegionInformation* info = NULL);
    G4Region* getHcal2Region(T4RegionInformation* info = NULL);
    G4Region* getECAL1ShashlikRegion(T4RegionInformation* info = NULL);
    G4Region* getECAL2ShashlikRegion(T4RegionInformation* info = NULL);

    /*! \brief Reset and destruct the instance.*/
    static void resetInstance(void) {delete regionManager; regionManager = NULL;}

  private:
    T4RegionManager(void);
    static T4RegionManager* regionManager;

    G4Region* defaultRegion;
    G4UserLimits* defaultUserLimits;

    G4Region* targetRegion;
    G4UserLimits* targetUserLimits;

    G4Region* opticalRegion;
//    G4UserLimits* opticalUserLimits;

    G4Region* absorberRegion;
    G4ProductionCuts* absorberCuts;
//    G4UserLimits* absorberUserLimits;

    void checkRegion(G4Region*&, G4String, T4RegionInformation*, G4ProductionCuts*);
    G4Region* ecal1_GamsRegion;
    G4Region* ecal2_GamsRegion;
    G4Region* ecal2_GamsRhRegion;
    G4Region* ecal1_OlgaRegion;
    G4Region* ecal1_MainzRegion;
    G4Region* ecal0Region;
    G4Region* hcal1Region;
    G4Region* hcal2Region;
    G4Region* ecal1_ShashlikRegion;
    G4Region* ecal2_ShashlikRegion;

    G4ProductionCuts* gamsCuts;
    G4ProductionCuts* rhgamsCuts;
    G4ProductionCuts* mainzCuts;
    G4ProductionCuts* olgaCuts;
    G4ProductionCuts* shashlikCuts;
    G4ProductionCuts* hcalCuts;
    void initCuts(G4ProductionCuts*&, double);
};

class T4RegionInformation : public G4VUserRegionInformation
{
  public:
    T4RegionInformation(G4Material* _mat1, G4double _d1 = 0, G4Material* _mat2 =
        NULL, G4double _d2 = 0) :
        G4VUserRegionInformation(), mat1(_mat1), mat2(_mat2), d1(_d1), d2(_d2) {}
    virtual ~T4RegionInformation(void) {}
    virtual void Print(void) const {}

    G4Material* getMat1(void) {return mat1;}
    G4Material* getMat2(void) {return mat2;}
    G4double getD1(void) {return d1;}
    G4double getD2(void) {return d2;}

  private:
    G4Material* mat1;
    G4Material* mat2;
    G4double d1;
    G4double d2;
};

#endif /* T4REGIONMANAGER_HH_ */
