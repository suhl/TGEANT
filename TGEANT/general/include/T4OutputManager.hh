#ifndef T4OUTPUTMANAGER_HH_
#define T4OUTPUTMANAGER_HH_

#include "T4Event.hh"
#include "T4SettingsFile.hh"
#include "T4PerformanceMonitor.hh"

#include "T4OutputBackEnd.hh"
#include "T4OutputASCII.hh"
#include "T4OutputBinary.hh"
#include "T4OutputDetDat.hh"

#include "G4GDMLParser.hh"

class T4OutputManager
{
  public:
    T4OutputManager(T4Event*);
    virtual ~T4OutputManager(void);

    void saveEvent(void);
    void closeFiles(void);

  private:
    std::vector<T4OutputBackEnd*> backEnd;
    T4SettingsFile* settingsFile;

    std::string outputFileName;
    
    void saveROOTGeometry();

    void findRunName(void);
    std::string getRunNoString(unsigned int runNo);
};

#endif /* T4OUTPUTMANAGER_HH_ */
