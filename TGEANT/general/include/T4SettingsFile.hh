#ifndef T4SETTINGSFILE_HH_
#define T4SETTINGSFILE_HH_

#include <fstream>
#include <cstdlib>

#include "globals.hh"
#include "G4ThreeVector.hh"

#include "Randomize.hh"

#include "CLHEP/Random/RandomEngine.h"
#include "CLHEP/Random/RanluxEngine.h"
#include "CLHEP/Random/Random.h"

#include "T4Globals.hh"

#include "T4SStructManager.hh"
#include "T4SGlobals.hh"
#include "T4SFileBackEnd.hh"
#include "T4SDetectorsDat.hh"
#include "T4SCalorimeter.hh"
#include "T4SSettingsFileXML.hh"
#include "T4SMessenger.hh"

class T4SettingsFile
{
	public:
    static T4SettingsFile* getInstance(void);
    ~T4SettingsFile(void);

    void loadFile(void);
    void loadFile(std::string);

    long int getStartSeed(void) {return startSeed;}

    std::string getConfigFile(void) {return settingsFileName;}

    T4SStructManager* getStructManager(void) {return structM;}
    CLHEP::HepRandom* getRandom(void) {return randMan;}

    TGEANT::PhysicsList getPhysicsList(void);
    TGEANT::T4BeamFileType getBeamFileType(std::string);

    bool isOpticalPhysicsActivated(void);
    bool geometryModeActivated(void);

    int getDetectorId(int n = 1);

    const vector<T4SCAL>* getCMTXList(void);

  private:
    T4SettingsFile(void);
    static T4SettingsFile* settingsFile;

    T4SStructManager* structM;
    T4SFileBackEnd* fileBackEnd;
    
    CLHEP::HepRandom* randMan;
    CLHEP::HepRandomEngine* randEng;
    
    
    std::string filenameInput;
    std::string settingsFileName;
    long int startSeed;

    int detectorId;

    // calorimeter cmtx lines
    T4SCalorimeter* t4SCalorimeter;
};

#endif /* SETTINGSFILE_HH_ */
