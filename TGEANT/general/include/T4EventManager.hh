#ifndef T4EVENTMANAGER_HH_
#define T4EVENTMANAGER_HH_

#include "globals.hh"
#include "G4UserEventAction.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"

#include "T4Globals.hh"
#include "T4Event.hh"
#include "T4Daq.hh"
#include "T4OutputManager.hh"
#include "T4TrajectoryHandler.hh"
#include "T4MemoryManager.hh"

#include "T4TriggerPlugin.hh"
#include "T4TriggerHadron2008.hh"
#include "T4TriggerSIDIS2011.hh"
#include "T4TriggerDVCS2012.hh"
#include "T4TriggerPrimakoff2012.hh"
#include "T4TriggerDY2014.hh"
#include "T4TriggerDY2015.hh"
#include "T4TriggerDVCS2016.hh"

class T4EventManager : public G4UserEventAction
{
  public:
    static T4EventManager* getInstance(void);
    ~T4EventManager(void);

    void analyse(T4HitCollection*);

    G4int getEventNo(void) {return eventNo;}
    T4Event* getEvent(void) {return t4Event;}
    T4BeamData* getBeamData(void) {return &t4Event->beamData;}
    void setBeamData(T4BeamData*);
    void endOfRunAction(void) {t4OutputManager->closeFiles();}

    T4TriggerPlugin* getTriggerPlugin(void) {return triggerPlugin;}

  private:
    T4EventManager(void);
    static T4EventManager* eventManager;

    void EndOfEventAction(const G4Event* anEvent);

    T4SettingsFile* settingsFile;
    T4Event* t4Event;
    T4Daq* t4Daq;
    T4OutputManager* t4OutputManager;
    T4TriggerPlugin* triggerPlugin;

    std::vector<G4int> trackIds;
    std::map<G4int, const T4Trajectory*> trackMap;
    G4int verboseLevel;
    G4int eventNo;
    G4int nSavedEvents;
    G4bool usedGenerator;
};

#endif /* T4EVENTMANAGER_HH_ */
