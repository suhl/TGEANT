#ifndef T4RUNMANAGER_HH_
#define T4RUNMANAGER_HH_

#include "G4RunManager.hh"
#include "G4UserLimits.hh"

#include "T4SettingsFile.hh"
#include "T4PhysicsList.hh"
#include "T4WorldConstruction.hh"
#include "T4PrimaryGenerator.hh"
#include "T4TrackingAction.hh"

class T4RunManager
{
  public:
    T4RunManager(T4PhysicsList* physicsList);
    ~T4RunManager(void) {delete g4RunManager;}

    void setEventManager(T4EventManager*);
    void setBeamGun(T4PrimaryGenerator*);
    void setWorldGeometry(T4WorldConstruction*);
    void initialize(void);

    void startRun(G4int numEvents);

  private:
    G4RunManager* g4RunManager;
    T4EventManager* eventManager;
    T4PrimaryGenerator* primaryGenerator;
    T4WorldConstruction* worldConstruction;
    T4SettingsFile* settingsFile;
};

#endif /* T4RUNMANAGER_HH_ */
