#ifndef T4SENSITIVECALORIMETER_HH_
#define T4SENSITIVECALORIMETER_HH_

#include "T4SensitiveDetector.hh"
#include "G4VGFlashSensitiveDetector.hh"

class T4SensitiveCalorimeter : public T4SensitiveDetector,
    public G4VGFlashSensitiveDetector
{
  public:
    T4SensitiveCalorimeter(G4String detectorName, G4int channelNo, G4ThreeVector _cellPosition, double _energyScale) :
        T4SensitiveDetector(detectorName, channelNo, TGEANT::CALO, channelNo),
            G4VGFlashSensitiveDetector() {cellPosition = _cellPosition; energyScale = _energyScale;}
    virtual ~T4SensitiveCalorimeter() {}

    G4bool ProcessHits(G4GFlashSpot* aSpot, G4TouchableHistory*);

  private:
    G4ThreeVector cellPosition;
    double energyScale;
};

#endif /* T4SENSITIVECALORIMETER_HH_ */
