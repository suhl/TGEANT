#ifndef T4RESPONSECURVE_HH_
#define T4RESPONSECURVE_HH_

#include "T4SettingsFile.hh"

#include "globals.hh"
#include <vector>

#include "CLHEP/Random/RandomEngine.h"
#include "CLHEP/Random/RanluxEngine.h"
#include "CLHEP/Random/Random.h"


class T4ResponseCurve
{
  public:
    T4ResponseCurve();
    virtual ~T4ResponseCurve(void) {};

    G4bool getResponse(G4double energy);
    void setBialkali(void);

  private:
    std::vector<std::pair<G4double, G4double> > responsePoints;
    void addPoint(G4double energy, G4double probability);

    CLHEP::HepRandom* randMan;
    
  
};

#endif /* T4RESPONSECURVE_HH_ */
