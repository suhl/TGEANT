#ifndef T4ECALCALIB_HH_
#define T4ECALCALIB_HH_

#include "T4BeamBackend.hh"

class T4EcalCalib : public T4BeamBackEnd
{
  public:
    T4EcalCalib(void);
    virtual ~T4EcalCalib(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4int id;
};

#endif /* T4ECALCALIB_HH_ */
