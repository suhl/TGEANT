#ifndef T4PHOTONBEAM_HH_
#define T4PHOTONBEAM_HH_

#include "T4BeamBackend.hh"

class T4PhotonBeam : public T4BeamBackEnd
{
  public:
    T4PhotonBeam(bool _generateBeam);
    virtual ~T4PhotonBeam(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4int id;
    G4double energy;
    G4bool generateBeam;
};

#endif /* T4PHOTONBEAM_HH_ */
