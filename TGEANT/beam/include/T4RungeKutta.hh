#ifndef BEAM_INCLUDE_T4RUNGEKUTTA_HH_
#define BEAM_INCLUDE_T4RUNGEKUTTA_HH_

#include "T4DeathMagnetic.hh"
#include "T4Globals.hh"

using namespace std;

class T4RungeKutta
{
  public:
    T4RungeKutta(void);
    virtual ~T4RungeKutta(void);

    void init(const T4BeamFileParticle&);
    void step();

//    inline TMatrixD getTrackParameter(void) {return *p;}

  private:
//    TMatrixD derive(TMatrixD);
//
//    inline void setZero(TMatrixD* in) {
//      double* const array = in->GetMatrixArray();
//      for (int i = 0; i < in->GetNoElements(); i++)
//        array[i] = 0;
//    }

    inline void setZero(double* in, unsigned int size) {
      for (unsigned int i = 0; i < size; i++)
        in[i] = 0;
    }

//    TMatrixD* p; // track parameter
    double stepWidth; // step width
    double BField[3]; // B-field
//    TMatrixD* k; // k-vector
//    TMatrixD* b; // Cash-Karp parameter b
//    TMatrixD* c; // Cash-Karp parameter c & c*
    T4DeathMagnetic* magneticField; // b-field
};

#endif /* BEAM_INCLUDE_T4RUNGEKUTTA_HH_ */
