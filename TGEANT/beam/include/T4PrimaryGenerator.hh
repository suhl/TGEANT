#ifndef T4PRIMARYGENERATOR_HH_
#define T4PRIMARYGENERATOR_HH_

#include <T4BeamOnly.hh>

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"

#include "T4SettingsFile.hh"
#include "T4PerformanceMonitor.hh"
#include "T4BeamBackend.hh"
#include "T4TargetTracking.hh"

#include "T4Lepto.hh"
#include "T4User.hh"
#include "T4Cosmic.hh"
#include "T4VisualizationMode.hh"
#include "T4ElectronBeam.hh"
#include "T4PhotonBeam.hh"
#include "T4EcalCalib.hh"
#include "T4EventGen.hh"

class T4PrimaryGenerator : public G4VUserPrimaryGeneratorAction
{
  public:
    T4PrimaryGenerator(void);
    virtual ~T4PrimaryGenerator(void);

  private:
    void GeneratePrimaries(G4Event* event);

    G4ParticleGun* particleGun;
    T4BeamBackEnd* backEnd;
    T4SettingsFile* settingsFile;
};

#endif /* T4PRIMARYGENERATOR_HH_ */
