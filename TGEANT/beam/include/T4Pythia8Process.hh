#ifndef T4PYTHIA8PROCESS_HH_
#define T4PYTHIA8PROCESS_HH_

#include "config.hh"
#ifdef USE_PYTHIA8

#include "T4ProcessBackend.hh"
#include "Pythia8/Pythia.h"

#include "fstream"
using namespace Pythia8;

class T4Pythia8Process : public T4ProcessBackEnd
{
  public:
    T4Pythia8Process(void);
    virtual ~T4Pythia8Process(void);

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);
    
    void initPythia(void);
    
  private:
    Pythia* Pythia8;

    std::string beamParticle;
};


#endif

#endif /* T4PYTHIAPROCESS_HH_ */
