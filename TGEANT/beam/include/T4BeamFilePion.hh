#ifndef T4BEAMFILEPION_HH_
#define T4BEAMFILEPION_HH_

#include "T4BeamFileBackend.hh"

class T4BeamFilePion : public T4BeamFileBackend
{
  public:
    T4BeamFilePion(void);
    virtual ~T4BeamFilePion(void) {}

    /*! \brief This function loads a beamfile. */
    void loadFile(std::string fileName);
};

#endif /* T4BEAMFILEPION_HH_ */
