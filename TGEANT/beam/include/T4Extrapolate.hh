#ifndef T4EXTRAPOLATE_HH_
#define T4EXTRAPOLATE_HH_

#include "T4SettingsFile.hh"
#include "T4WorldConstruction.hh"
#include "T4TargetTracking.hh"
#include "T4TargetBackend.hh"

#include "T4RungeKutta.hh"

#include "G4ParticleDefinition.hh"

class T4Extrapolate
{
  public:
    T4Extrapolate(G4ParticleDefinition* _beamParticle);
    virtual ~T4Extrapolate(void) {}

    double extrap(T4BeamFileParticle& particle);
    bool extrapAndCalcTargetDist(T4BeamFileParticle& particle);

    G4double getAdditionalKineticEnergy(void);

  private:
    double extrap(T4BeamFileParticle& particle, G4double newZPosition,
        bool wantTargetDist = false);

    G4ParticleDefinition* beamParticle;
    T4TargetBackend* targetBackend;
    bool useEventGenerator;

    double zFirst, zLast;
    double finalDistance;
    double wantedZPosition;
};

#endif /* T4EXTRAPOLATE_HH_ */
