#ifndef T4PILEUP_HH
#define T4PILEUP_HH

#include "T4SettingsFile.hh"

class T4PileUp
{
  public:
    T4PileUp(double _beamFlux, double _timeGate, double _beamFlux2);
    ~T4PileUp() {}

    int getPileUpTracksNumber(bool std = true);
    double getPileUpTrackTimeOffset(void);

  private:
    double beamFlux;
    double beamFlux2;
    double timeGate;
    CLHEP::HepRandom* randMan;
};

#endif
