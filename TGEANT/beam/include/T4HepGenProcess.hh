#ifndef T4HEPGENPROCESS_HH_
#define T4HEPGENPROCESS_HH_

#include "T4ProcessBackend.hh"
#include "G4Proton.hh"
#include "config.hh"

#ifdef USE_HEPGEN
// hepgen-ng
#include "hparammanager.h"
#include "hgenmanager.h"
#include "hevent.h"

class T4HepGenProcess : public T4ProcessBackEnd
{
  public:
    T4HepGenProcess(void);
    virtual ~T4HepGenProcess(void);

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);

  private:
    void doubleToFloats(double _in, float& high, float& low);
    void doubleToFloats(double _in, float* out);
    HGenManager* hGenManager;
    HParamManager* hParamManager;
    HParams* hParams;
};

#endif
#endif /* T4HEPGENPROCESS_HH_ */
