#ifndef T4LEPTOPROCESS_HH_
#define T4LEPTOPROCESS_HH_

#include "T4ProcessBackend.hh"
#include "T4LeptoFile.hh"

class T4LeptoProcess : public T4ProcessBackEnd
{
  public:
    T4LeptoProcess(void) {leptoFile = T4LeptoFile::getInstance();}
    virtual ~T4LeptoProcess(void) {};

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);

  private:
    T4LeptoFile* leptoFile;
};

#endif /* T4LEPTOPROCESS_HH_ */
