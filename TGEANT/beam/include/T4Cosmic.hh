#ifndef T4COSMIC_HH_
#define T4COSMIC_HH_

#include "T4BeamBackend.hh"

class T4Cosmic : public T4BeamBackEnd
{
  public:
    T4Cosmic(void);
    virtual ~T4Cosmic(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4double energy;
};

#endif /* T4COSMIC_HH_ */
