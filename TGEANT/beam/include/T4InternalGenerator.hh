#ifndef T4INTERNALGENERATOR_HH_
#define T4INTERNALGENERATOR_HH_

#include "T4BeamBackend.hh"
#include "T4HepGenProcess.hh"
#include "T4PythiaProcess.hh"
#include "T4PrimakoffProcess.hh"
#include "T4Pythia8Process.hh"
#include "T4asciiProcess.hh"

class T4HEPGen : public T4BeamBackEnd
{
  public:
    T4HEPGen(void);
    virtual ~T4HEPGen(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};

class T4Pythia8 : public T4BeamBackEnd
{
  public:
    T4Pythia8(void);
    virtual ~T4Pythia8(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};


class T4Pythia : public T4BeamBackEnd
{
  public:
    T4Pythia(void);
    virtual ~T4Pythia(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};

class T4Primakoff : public T4BeamBackEnd
{
  public:
    T4Primakoff(void);
    virtual ~T4Primakoff(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};

class T4ascii : public T4BeamBackEnd
{
  public:
    T4ascii(void);
    virtual ~T4ascii(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};



#endif /* T4INTERNALGENERATOR_HH_ */
