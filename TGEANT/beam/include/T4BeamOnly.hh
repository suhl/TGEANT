#ifndef T4BEAMONLY_HH_
#define T4BEAMONLY_HH_

#include <T4BeamFileMuon.hh>
#include "T4BeamBackend.hh"

class T4BeamOnly : public T4BeamBackEnd
{
  public:
    T4BeamOnly(void);
    virtual ~T4BeamOnly(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4bool useBeamFile;
    G4int id;
    G4double energy;
};

#endif /* T4BEAMONLY_HH_ */
