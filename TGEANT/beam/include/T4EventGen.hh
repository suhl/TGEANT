#ifndef T4EVENTGEN_HH_
#define T4EVENTGEN_HH_

#include "T4BeamBackend.hh"
#include "T4HepGenProcess.hh"

class T4EventGen : public T4BeamBackEnd
{
  public:
    T4EventGen(void);
    virtual ~T4EventGen(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

    T4ProcessBackEnd* getEventGeneratorProcess(void);

  private:
    G4ThreeVector targetPos;
    G4double radius;
    G4double length;
    G4int id;
};

#endif /* T4BEAMBACKEND_HH_ */
