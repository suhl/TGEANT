#ifndef T4PRIMAKOFFPROCESS_HH_
#define T4PRIMAKOFFPROCESS_HH_

#include "T4ProcessBackend.hh"
#include "config.hh"

#ifdef USE_PRIMAKOFF
#include "primGen.h"

class T4PrimakoffProcess : public T4ProcessBackEnd
{
  public:
    T4PrimakoffProcess(void);
    virtual ~T4PrimakoffProcess(void);

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);
  private:
    primGen* primGenerator;
};

#endif
#endif /* T4PRIMAKOFFPROCESS_HH_ */
