#ifndef T4PYTHIAPROCESS_HH_
#define T4PYTHIAPROCESS_HH_

#include "config.hh"
#ifdef USE_PYTHIA

#include "T4ProcessBackend.hh"
#include "TPythia6.h"


class T4PythiaProcess : public T4ProcessBackEnd
{
  public:
    T4PythiaProcess(void);
    virtual ~T4PythiaProcess(void);

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);
    
    void initPythia(void);
    
  private:
    std::string getParticleString(int);
    TPythia6* tPythia6;
    T4SPythia* pythia;

    std::string beamParticle;
};


#endif

#endif /* T4PYTHIAPROCESS_HH_ */
