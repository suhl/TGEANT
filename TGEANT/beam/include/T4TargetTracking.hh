#ifndef T4TARGETTRACKING_HH_
#define T4TARGETTRACKING_HH_

#include "G4UserSteppingAction.hh"
#include "G4Step.hh"

#include "T4SettingsFile.hh"
#include "T4TargetBackend.hh"

class T4TargetTracking : public G4UserSteppingAction
{
  public:
    static T4TargetTracking* getInstance(void);
    virtual ~T4TargetTracking(void) {};

    void reset(void);

    G4bool isProcessActiveNow(void) {return callEventGenNow;}

    void activateProcessNow(void) {callEventGenNow = true; usedProcess = true;}
    void processCalled(void) {callEventGenNow = false;}

    G4bool usedProcessInThisEvent(void) {return usedProcess;}
    void dontSaveThisEvent(void) {usedProcess = false;}

    void setDistanceForTargetStepping(G4double dist) {finalDistance = dist;}
    G4double getDistanceForTargetStepping(void) {return finalDistance;}
    
    void activateTargetStepping(void);

  private:
    T4TargetTracking(void);
    static T4TargetTracking* targetTracking;
    T4SettingsFile* settingsFile;

    void UserSteppingAction(const G4Step* aStep);

    T4TargetBackend* targetBackend;

    G4int beamParticleId;
    G4double coveredDistance;
    G4double finalDistance;
    G4bool callEventGenNow;
    G4bool usedProcess;
    G4bool isSteppingActive;
    G4double zPosition;

    G4bool noSecondary;
};

#endif /* T4TARGETTRACKING_HH_ */
