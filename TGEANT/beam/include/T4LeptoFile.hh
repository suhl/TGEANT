#ifndef T4LEPTOFILE_HH_
#define T4LEPTOFILE_HH_

#include "T4SettingsFile.hh"
#include "T4Event.hh"

#include "G4RunManager.hh"

class T4LeptoFile
{
  public:
    static T4LeptoFile* getInstance(void);
    ~T4LeptoFile(void);

    T4BeamData* getNextLeptoEvent(void);
    T4BeamData* getCurrentLeptoEvent(void) {return &leptoCollection.at(leptoEventNo-1);}

  private:
    T4LeptoFile(void);
    static T4LeptoFile* leptoFile;

    T4SettingsFile* settingsFile;

    bool readFile(std::ifstream& file, streamsize headerSize, bool firstCall);

    unsigned int leptoEventNo;
    std::vector<T4BeamData> leptoCollection;
};

#endif /* T4LEPTOFILE_HH_ */
