#ifndef T4LEPTO_HH_
#define T4LEPTO_HH_

#include "T4BeamBackend.hh"
#include "T4LeptoFile.hh"
#include "T4LeptoProcess.hh"

class T4Lepto : public T4BeamBackEnd
{
  public:
    T4Lepto(void);
    virtual ~T4Lepto(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

    T4ProcessBackEnd* getEventGeneratorProcess(void);

  private:
    T4LeptoFile* leptoFile;
};

#endif /* T4LEPTO_HH_ */
