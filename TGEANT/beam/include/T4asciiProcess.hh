// Copyright [2016] Misha Mikhasenko
// ------------------------------------------
// Process pluggin, which generates event from simple ascii discription
// see example of input file in
//  $TGEANT/resources/hadron_2008/ascii_events
// ------------------------------------------

#ifndef T4ASCIIPROCESS_HH_
#define T4ASCIIPROCESS_HH_

#include <fstream>
#include <vector>
#include <utility>

#include "T4ProcessBackend.hh"

class T4asciiProcess : public T4ProcessBackEnd {
 public:
  T4asciiProcess(void);
  virtual ~T4asciiProcess(void);

  G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);

  void initFile(void);
  void nextEvent();

 private:
  std::ifstream fin;
  std::vector<pair<int, std::vector<double> > > particles;
};


#endif /* T4ASCIIPROCESS_HH_ */
