#ifndef T4BEAMFILEMUON_HH_
#define T4BEAMFILEMUON_HH_

#include "T4BeamFileBackend.hh"

class T4BeamFileMuon : public T4BeamFileBackend
{
  public:
    T4BeamFileMuon(void);
    virtual ~T4BeamFileMuon(void) {}

    /*! \brief This function loads a beamfile. */
    void loadFile(std::string fileName);
};

#endif /* T4BEAMFILEMUON_HH_ */
