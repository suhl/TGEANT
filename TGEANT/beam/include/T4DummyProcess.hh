#ifndef BEAM_SRC_T4DUMMYPROCESS_HH_
#define BEAM_SRC_T4DUMMYPROCESS_HH_

#include "T4BeamBackend.hh"
#include "T4ProcessBackend.hh"

class T4DummyBeamPlugin : public T4BeamBackEnd
{
  public:
    T4DummyBeamPlugin(void);
    virtual ~T4DummyBeamPlugin(void) {}

    T4ProcessBackEnd* getEventGeneratorProcess(void);
};

class T4DummyProcess : public T4ProcessBackEnd
{
  public:
    T4DummyProcess(void) {}
    virtual ~T4DummyProcess(void) {}

    G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&);
};

#endif /* BEAM_SRC_T4DUMMYPROCESS_HH_ */
