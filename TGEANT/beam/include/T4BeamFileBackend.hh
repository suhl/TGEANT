#ifndef T4BEAMFILEBACKEND_HH
#define T4BEAMFILEBACKEND_HH

#include "T4SettingsFile.hh"
#include <iostream>
#include <assert.h>

class T4BeamFileBackend;

class T4BeamFileList
{
  public:
    static T4BeamFileList* getInstance(void);
    virtual ~T4BeamFileList(void);

    map<string, T4BeamFileBackend*> beamFiles;
    T4BeamFileBackend* getBeamFile(string);

    /*! \brief Reset and destruct the instance.*/
    static void resetInstance(void) {delete instance; instance = NULL;}

  private:
    T4BeamFileList(void) {}
    static T4BeamFileList* instance;
};

class T4BeamFileBackend
{
  public:
    T4BeamFileBackend(void) {
      is4BitFile = false;
      tmpDouble = 0;
      tmpInt = 0;
      tmpInt64 = 0;
      particleId = -13;
      zPosConvention = 0.;
      beamFileName = "NA";}
    virtual ~T4BeamFileBackend() {}

    /*! \brief Gets the next beam-entry from the file. */
    virtual const T4BeamFileParticle& getNextEntry(TGEANT::T4BeamFileType);

    /*! \brief This function loads a beamfile.
     *  Make sure this function afterwards randomizes the index!
     *  Also this HAS to be implemented by each derived class
     */
    virtual void loadFile(std::string fileName)=0;

    void init(void);

    /*! \brief This function sets the beamFileZConvention.
     *  You only need to call this function for the additional pile up beamfile if the
     *  zPosConvention differs from the standard convention for the primary beamfile.
     */
    void setBeamFileZConvention(double _zConv) {
      zPosConvention = _zConv;
    }

  protected:
    string beamFileName;
    void registerPlugin(void);

    ifstream inFile;
    G4bool isValidHeader(void);

    double zPosConvention;
    int particleId;

    vector<T4BeamFileParticle>::iterator index;
    vector<T4BeamFileParticle> beamList;
    const T4BeamFileParticle& rollOver(TGEANT::T4BeamFileType);

    G4bool is4BitFile;
    float tmpDouble;
    int tmpInt;
    int64_t tmpInt64;

    G4int getLong(void) {
        inFile.read((char*) &tmpInt64, sizeof(int64_t));
        return tmpInt64;
    }

    G4int getShort(void) {
        inFile.read((char*) &tmpInt, sizeof(int));
        return tmpInt;
    }

    G4int getInt(void) {
        if (is4BitFile) return getShort();
        else return getLong();
    }

    G4double getDouble(void) {
        inFile.read((char*) &tmpDouble, sizeof(float));
        return tmpDouble;
    }
};

#endif
