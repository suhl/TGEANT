#ifndef T4USER_HH_
#define T4USER_HH_

#include "T4BeamBackend.hh"

class T4User : public T4BeamBackEnd
{
  public:
    T4User(void);
    virtual ~T4User(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4int id;
    G4double energy;
    G4ThreeVector position;
    G4ThreeVector momentum;

    T4SUser* user;
};

#endif /* T4USER_HH_ */
