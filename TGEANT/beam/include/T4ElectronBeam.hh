#ifndef T4ELECTRONBEAM_HH_
#define T4ELECTRONBEAM_HH_

#include "T4BeamBackend.hh"

class T4ElectronBeam : public T4BeamBackEnd
{
  public:
    T4ElectronBeam(void);
    virtual ~T4ElectronBeam(void) {}

    void generateEvent(G4Event*);
    void initialize(void);

  private:
    G4int id;
    G4double energy;
    G4double electronMass;
};

#endif /* T4ELECTRONBEAM_HH_ */
