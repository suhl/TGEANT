#ifndef T4BEAMBACKEND_HH_
#define T4BEAMBACKEND_HH_

#include <T4BeamFileMuon.hh>
#include <T4BeamFilePion.hh>

#include "G4ParticleGun.hh"
#include "G4Event.hh"
#include "G4VProcess.hh"
#include "globals.hh"

#include "T4SettingsFile.hh"
#include "T4EventManager.hh"
#include "T4PhysicsList.hh"
#include "T4TargetTracking.hh"
#include "T4PileUp.hh"
#include "T4Extrapolate.hh"
#include "T4ProcessBackend.hh"

#include "T4SMessenger.hh"

class T4BeamBackEnd;

class T4BeamPluginList
{
  public:
    static T4BeamPluginList* getInstance(void);
    virtual ~T4BeamPluginList(void);

    map<string, T4BeamBackEnd*> beamPlugins;
    vector<unsigned int> generatorId;

    /*! \brief Reset and destruct the instance.*/
    static void resetInstance(void) {delete instance; instance = NULL;}

  private:
    T4BeamPluginList(void) {}
    static T4BeamPluginList* instance;
};

class T4BeamBackEnd
{
  public:
    T4BeamBackEnd(void) {
      particleGun = NULL;
      beamFile = NULL;
      beamFileForAdditionalPileUp = NULL;
      beamFileType = TGEANT::Normal;
      pileUp = NULL;
      extrapolate = NULL;
      settingsFile = T4SettingsFile::getInstance();
      beamPluginList = T4BeamPluginList::getInstance();
      pluginName = "NA";
      eventGenId = 0;
      process = NULL;}

    virtual ~T4BeamBackEnd(void) = 0;

    virtual void generateEvent(G4Event*); /* default for event generators: HEPGen, Pythia6 and LEPTO */
    virtual void initialize(void); /* default for event generators: HEPGen, Pythia6 and LEPTO */

    void setParticleGun(G4ParticleGun* _particleGun) {
      particleGun = _particleGun;}

    virtual T4ProcessBackEnd* getEventGeneratorProcess(void) {return NULL;}

  protected:
    string pluginName;
    unsigned int eventGenId;
    void registerPlugin(void);

    G4ParticleGun* particleGun;
    T4SettingsFile* settingsFile;
    T4BeamFileBackend* beamFile;
    T4BeamFileBackend* beamFileForAdditionalPileUp;
    TGEANT::T4BeamFileType beamFileType;
    T4PileUp* pileUp;
    T4Extrapolate* extrapolate;
    T4ProcessBackEnd* process;

    void generatePileUp(G4Event* event);
    void setupPileUp(T4BeamFileParticle&);

    G4ParticleDefinition* getParticleByID(G4int id);
    G4double getParticleMass(G4int id);

    void setOrigin(T4BeamFileParticle&);

  private:
    T4BeamPluginList* beamPluginList;
};

#endif /* T4BEAMBACKEND_HH_ */
