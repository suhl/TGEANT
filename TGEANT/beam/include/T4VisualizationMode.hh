#ifndef T4VISUALIZATIONMODE_HH_
#define T4VISUALIZATIONMODE_HH_

#include "T4BeamBackend.hh"

#include "G4Geantino.hh"

class T4VisualizationMode : public T4BeamBackEnd
{
  public:
    T4VisualizationMode(void);
    virtual ~T4VisualizationMode(void) {};

    void generateEvent(G4Event*);
    void initialize(void);
};

#endif /* T4VISUALIZATIONMODE_HH_ */
