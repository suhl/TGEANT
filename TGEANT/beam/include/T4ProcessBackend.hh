#ifndef T4PROCESSBACKEND_HH_
#define T4PROCESSBACKEND_HH_

#include "G4VDiscreteProcess.hh"
#include "G4ParticleDefinition.hh"

#include "T4SettingsFile.hh"
#include "T4TargetTracking.hh"
#include "T4EventManager.hh"

class T4ProcessBackEnd : public G4VDiscreteProcess
{
  public:
    T4ProcessBackEnd(void) :
      G4VDiscreteProcess("T4EventGenProcess", fUserDefined) {
      targetTracking = T4TargetTracking::getInstance();
      settingsFile = T4SettingsFile::getInstance();
      eventGenId = 0;}
    virtual ~T4ProcessBackEnd(void) {}

    virtual G4VParticleChange* PostStepDoIt(const G4Track&, const G4Step&) = 0;

    G4double PostStepGetPhysicalInteractionLength(const G4Track&, G4double,
        G4ForceCondition*) {
      if (targetTracking->isProcessActiveNow()) return 0;
      else return DBL_MAX;}

    G4double GetMeanFreePath(const G4Track&, G4double, G4ForceCondition*) {
      return DBL_MAX;}

    inline std::pair<G4double, G4ThreeVector> getRotation(G4ThreeVector input,
        G4ThreeVector axis = G4ThreeVector(0, 0, 1)) {
      input.setMag(1.);
      axis.setMag(1.);
      G4ThreeVector cross = axis.cross(input);
      G4double arg = input.dot(axis);
      G4double angle = 0;
      if (arg < 1. && cross != G4ThreeVector(0,0,0))
        angle = acos(input.dot(axis));
      return std::make_pair(angle, cross);
    }

    void setEventGenId(unsigned int id) {eventGenId = id;}

  protected:
    T4TargetTracking* targetTracking;
    T4SettingsFile* settingsFile;
    unsigned int eventGenId;

    G4ParticleDefinition* getParticleByID(G4int id);
};

#endif /* T4PROCESSBACKEND_HH_ */
