#include <T4RungeKutta.hh>

T4RungeKutta::T4RungeKutta(void)
{
//  p = new TMatrixD(6, 1); // track parameter
//  k = new TMatrixD(6, 6); // k-vector
//  b = new TMatrixD(6, 5); // Cash-Karp parameter b
//  c = new TMatrixD(6, 2); // Cash-Karp parameter c & c*

  magneticField = T4DeathMagnetic::getInstance(); // b-field
  stepWidth = 0.01;
}

T4RungeKutta::~T4RungeKutta(void)
{
//  delete p;
//  delete k;
//  delete b;
//  delete c;
}

void T4RungeKutta::init(const T4BeamFileParticle& input)
{
//  (*p)[0][0] = 0.; // z
//  (*p)[1][0] = input.posX; // x
//  (*p)[2][0] = input.posY; // y
//  (*p)[3][0] = input.momentumDirection.x()/input.momentumDirection.z(); // x'
//  (*p)[4][0] = input.momentumDirection.y()/input.momentumDirection.z(); // y'
//  (*p)[5][0] = -1. / (input.energy/CLHEP::GeV); // rho tilde= e*c/p
//
//  stepWidth = 0.01; //cm
//
//  setZero(BField, 3);
//  setZero(k);
//
//  (*b)[0][0] = 0.;            (*b)[0][1] = 0.;         (*b)[0][2] = 0.;           (*b)[0][3] = 0.;              (*b)[0][4] = 0.;
//  (*b)[1][0] = 1./5.;         (*b)[1][1] = 0.;         (*b)[1][2] = 0.;           (*b)[1][3] = 0.;              (*b)[1][4] = 0.;
//  (*b)[2][0] = 3./40.;        (*b)[2][1] = 9./40.;     (*b)[2][2] = 0.;           (*b)[2][3] = 0.;              (*b)[2][4] = 0.;
//  (*b)[3][0] = 3./10.;        (*b)[3][1] = -9./10.;    (*b)[3][2] = 6./5.;        (*b)[3][3] = 0.;              (*b)[3][4] = 0.;
//  (*b)[4][0] = -11./54.;      (*b)[4][1] = 5./2.;      (*b)[4][2] = -70./27.;     (*b)[4][3] = 35./27.;         (*b)[4][4] = 0.;
//  (*b)[5][0] = 1631./55296.;  (*b)[5][1] = 175./512.;  (*b)[5][2] = 575./13824.;  (*b)[5][3] = 44275./110592.;  (*b)[5][4] = 253./4096.;
//
//  (*c)[0][0] = 37./378.;      (*c)[0][1] = 2825./27648.;
//  (*c)[1][0] = 0.;            (*c)[1][1] = 0.;
//  (*c)[2][0] = 250./621.;     (*c)[2][1] = 18575./48384.;
//  (*c)[3][0] = 125./594.;     (*c)[3][1] = 13525./55296.;
//  (*c)[4][0] = 0.;            (*c)[4][1] = 277./14336.;
//  (*c)[5][0] = 512./1771.;    (*c)[5][1] = 1./4.;
}

//TMatrixD T4RungeKutta::derive(TMatrixD input)
//{
//  // vars
//  TMatrixD output(6, 1);
//
//  output[0][0] = -1.;
//  output[1][0] = input[3][0];
//  output[2][0] = input[4][0];
//  output[3][0] = input[5][0]
//      * sqrt(1 + pow(input[3][0], 2.) + pow(input[4][0], 2.))
//      * (input[3][0] * input[4][0] * BField[0]
//          - (1 + pow(input[3][0], 2.)) * BField[1] + input[4][0] * BField[2]);
//  output[4][0] = input[5][0]
//      * sqrt(1 + pow(input[3][0], 2.) + pow(input[4][0], 2.))
//      * ((1 + pow(input[4][0], 2.)) * BField[0]
//          - input[3][0] * input[4][0] * BField[1] - input[3][0] * BField[2]);
//  output[5][0] = 0.;
//
//  return output;
//}

void T4RungeKutta::step(void)
{
//  cout<<"i'm stepping: " <<   (*p)[1][0]<<"  "<<(*p)[2][0]<<"  "<<(*p)[0][0]<<"   step="<<stepWidth<<endl;
  // vars
//  TMatrixD input(6, 1);
//  TMatrixD input_derived(6, 1);
//  TMatrixD k_tmp(6, 6);
//  TMatrixD error(6, 1);
//  bool check;
//  double B_pos[3];
//  double error_req = 0.0001;
//  double z_req = 50.;
//
//  do {
//    // read b-field
//    B_pos[0] = (*p)[1][0] / CLHEP::cm; //x     // in cm
//    B_pos[1] = (*p)[2][0] / CLHEP::cm; //y
//    B_pos[2] = (*p)[0][0] / CLHEP::cm; //z
//    magneticField->GetFieldValue(B_pos, BField);
//    // T -> kG
//    BField[0] /= 1000.*CLHEP::gauss; //x
//    BField[1] /= 1000.*CLHEP::gauss; //y
//    BField[2] /= 1000.*CLHEP::gauss; //z
//
//    setZero(&k_tmp);
//
//    // calc k_tmp
//    for (int i = 0; i < 6; i++) {
//      input[i][0] = (*p)[i][0];
//    }
//
//    input_derived = derive(input);
//    for (int i = 0; i < 6; i++) {
//      k_tmp[i][0] = stepWidth * input_derived[i][0];
//    }
//
//    for (int l = 1; l < 6; l++) {
//      for (int i = 0; i < 6; i++) {
//        input[i][0] = (*p)[i][0];
//      }
//      for (int j = 0; j < 5; j++) {
//        for (int i = 0; i < 6; i++) {
//          input[i][0] += (*b)[l][j] * k_tmp[i][j];
//        }
//      }
//
//      input_derived = derive(input);
//      for (int i = 0; i < 6; i++) {
//        k_tmp[i][l] = stepWidth * input_derived[i][0];
//      }
//    }
//
//    // calc error
//    for (int i = 0; i < 6; i++) {
//      for (int j = 0; j < 6; j++) {
//        error[i][0] += ((*c)[j][0] - (*c)[j][1]) * k_tmp[i][j];
//      }
//    }
//
//    // check
//    check = true;
////    for (int i = 0; i < 6; i++) {
////      if (error[i][0] > error_req) {
////        check = false;
////        break;
////      }
////    }
////
////    // modify h
////    if (!check) {
////      // new step size
////      double minimum;
////      double tmp;
////      for (int i = 0; i < 6; i++) {
////        tmp = pow(abs(error_req / error[i][0]), 0.25);
////        if (i==0 || tmp < minimum)
////          minimum = tmp;
////      }
////
////      stepWidth *= 0.99 * minimum;
//////      while (((*p)[0][0] + stepWidth) > z_req && stepWidth > error_req)
//////        stepWidth /= 2.;
////    }
//  } while (check == false);
//
//  // k_tmp -> *k
//  for (int i = 0; i < 6; i++) {
//    for (int j = 0; j < 6; j++) {
//      (*k)[i][j] = k_tmp[i][j];
//    }
//  }
//
//  // calc p
//  for (int i = 0; i < 6; i++) {
//    for (int j = 0; j < 6; j++) {
//      (*p)[i][0] += (*c)[j][0] * (*k)[i][j];
//    }
//  }
//
//  // error matrix propagation
//  // error_prop();
//
//  // new step size
////  double minimum;
////  double tmp;
////  for (int i = 0; i < 6; i++) {
////    tmp = pow(abs(error_req / error[i][0]), 0.25);
////    if (i==0 || tmp < minimum)
////      minimum = tmp;
////  }
////
////  stepWidth *= 0.99 * minimum;
////  while (((*p)[0][0] + stepWidth) > z_req && stepWidth > error_req)
////    stepWidth /= 2.;
////  cout<<"i'm stepping end: " <<   (*p)[1][0]<<"  "<<(*p)[2][0]<<"  "<<(*p)[0][0]<<"   step="<<stepWidth<<endl;
}
