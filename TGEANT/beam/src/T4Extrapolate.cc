#include "T4Extrapolate.hh"
#include "T4BeamBackend.hh"

T4Extrapolate::T4Extrapolate(G4ParticleDefinition* _beamParticle)
{
  T4SBeam* t4sBeam = T4SettingsFile::getInstance()->getStructManager()->getBeam();

  beamParticle = _beamParticle;
  targetBackend = T4ActiveTarget::getInstance()->getTarget();
  useEventGenerator =
      (T4BeamPluginList::getInstance()->beamPlugins[t4sBeam->beamPlugin]->getEventGeneratorProcess()
          != NULL);
  wantedZPosition = t4sBeam->beamZStart;
  finalDistance = 0;

  if (targetBackend != NULL) {
    zFirst = targetBackend->getZFirst();
    zLast = targetBackend->getZLast();

    if (t4sBeam->beamZStart > zFirst && t4sBeam->useTargetExtrap)
      T4SMessenger::getInstance()->printfMessage(T4SFatalError, __LINE__,
          __FILE__,
          "Your beam wants to start at %0.3e but the target begins at %0.3e! Start your beam upstream of your target if you use the option 'useTargetExtrap'!\n",
          t4sBeam->beamZStart, zFirst);
  } else {
    zFirst = 0;
    zLast = 0;
  }
}

double T4Extrapolate::extrap(T4BeamFileParticle& particle,
    G4double newZPosition, bool wantTargetDist)
{
  static bool firstEvent = true;
  if (firstEvent) {
    firstEvent = false;
    if (targetBackend != NULL) {
      zFirst = targetBackend->getZFirst();
      zLast = targetBackend->getZLast();
    }
  }
  
  if (particle.posZ == newZPosition)
    return 0.;

  bool forwardExtrap = (particle.posZ < newZPosition);

  //these are used to determine the distance in target and validate the beam particle
  int zeroSteps = 0;
  bool wasInField = false;

  double stepsize = 1. * CLHEP::mm;
  const double conversionFactor = -299.792458;
  
  //if there is no magnetic field in the middle of the target, we only step till the sm1 field is over
  static bool hasField = true;
  static bool first = true; // the setup never changes
  if (!wantTargetDist && first) {
    first = false;
    G4ThreeVector testField;
    G4ThreeVector testPos = G4ThreeVector(0., 0., (zFirst + zLast) / 2.);
    T4DeathMagnetic::getInstance()->GetFieldValue(&testPos[0], &testField[0]);
    if (testField.mag() == 0.0)
      hasField = false;
  }

  // for the forward extrapolation:
  // we can do a linear extrap to zFirst, if there is no magnetic field
  // -- we know that there is no second field outside the target
  // -- and we know that there is more optimization possible
  if (forwardExtrap && !hasField) {
    double newZ;
    if (newZPosition < zFirst )
      newZ = newZPosition;
    else
      newZ = zFirst;
    G4double factor = (newZ - particle.posZ)
        / particle.momentumDirection.getZ();
    particle.posX += factor * particle.momentumDirection.getX();
    particle.posY += factor * particle.momentumDirection.getY();
    particle.posZ = newZ;
  }

  //these vectors will be iterated
  G4ThreeVector actualPosition = G4ThreeVector(particle.posX, particle.posY,
      particle.posZ);
  // we actually thought about static_cast<double>((static_cast<int>(forwardExtrap)*2 - 1)) ;)
  G4ThreeVector actualMomentum = (forwardExtrap ? 1.0 : -1.0) * particle.momentumDirection;
  const double magnitude = actualMomentum.mag();

  G4Navigator* navi = G4TransportationManager::GetTransportationManager()
      ->GetNavigatorForTracking();
  G4ThreeVector magField;
  G4ThreeVector deltaP;

  // we use this to measure the length of the track in the target
  double lengthInTarget = 0.;

  while ((forwardExtrap && (actualPosition[2] < newZPosition))
      || (!forwardExtrap && (actualPosition[2] > newZPosition))) {
    // Geant4 magnetic field value in 10e3 tesla
    T4DeathMagnetic::getInstance()->GetFieldValue(&actualPosition[0],
        &magField[0]);

    // no field and outside of target
    if (magField.mag() == 0.0 && !forwardExtrap && actualPosition.z() < zFirst)
      // we don't need the forwardExtrap check because we will stop anyway if we reach zLast
      zeroSteps++;

    // we want the extrap to be done stepwise in the target region - no linear allowed here
    // if we are behind the target already, then break this loop if the magnetic field is 0
    if (!forwardExtrap && zeroSteps > 3 && (actualPosition.z()  < zFirst || !hasField))
      break;

    deltaP = actualMomentum.cross(magField);
    deltaP /= magnitude;
    deltaP *= conversionFactor * beamParticle->GetPDGCharge();
    deltaP *= stepsize;
    actualMomentum += deltaP;
    actualMomentum.setMag(stepsize);
    actualPosition += actualMomentum;
    actualMomentum.setMag(magnitude);

    if ((!forwardExtrap && actualMomentum.z() > 0.) || (forwardExtrap && actualMomentum.z() < 0.)) {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__,
          "T4Extrapolate::extrap: z-component of the momentum of extrapolated particle changed sign!");
      return 0.;
    }

    if (wantTargetDist) {
      if (targetBackend->isVolumeInTarget(navi->LocateGlobalPointAndSetup(actualPosition)))
        lengthInTarget++;
    }
  }

  particle.posX = actualPosition[0];
  particle.posY = actualPosition[1];
  particle.posZ = actualPosition[2];
  particle.momentumDirection = -actualMomentum;

  //do a linear extrap once we had three steps without B-Field -- we know that there is no second field outside the target
  if (!forwardExtrap && zeroSteps > 3 && actualPosition.z() > newZPosition) {
    G4double factor = (newZPosition - actualPosition.z())
        / particle.momentumDirection.getZ();
    particle.posX += factor * particle.momentumDirection.getX();
    particle.posY += factor * particle.momentumDirection.getY();
    particle.posZ = newZPosition;
  }

  return lengthInTarget;
}

double T4Extrapolate::extrap(T4BeamFileParticle& particle)
{
  return extrap(particle, wantedZPosition);
}

bool T4Extrapolate::extrapAndCalcTargetDist(T4BeamFileParticle& particle)
{
  T4BeamFileParticle oldParticle = particle;
  double lengthinTarget = extrap(particle, wantedZPosition, true);

  // now if we were downstream of the target we are finished
  // else we need to go forward until the end of the target and add this part of the distance
  double lengthForward = 0.0;
  if (oldParticle.posZ < zLast && zLast != zFirst)
    lengthForward = extrap(oldParticle, zLast, true);
  lengthinTarget += lengthForward;
  
  finalDistance = targetBackend->getRndmTargetDist(lengthinTarget);
  // the target backend decides what happens for lengthinTarget == 0.0:
  // for the thin primakoff target we'll get 0.0 for valid beam tracks...
  if (finalDistance < 0) {
    T4SMessenger::getInstance()->printMessage(T4SVerbose, __LINE__, __FILE__,
        "Beam particle missed target - Taking next one!");
    return false;
  } else {
    T4TargetTracking::getInstance()->setDistanceForTargetStepping(finalDistance);
    return true;
  }
}

G4double T4Extrapolate::getAdditionalKineticEnergy(void)
{
  if (targetBackend != NULL)
    return targetBackend->getEConst() + finalDistance * targetBackend->getESlope();
  else
    return 0;
}
