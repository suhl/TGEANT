#include "T4EventGen.hh"

static T4EventGen* t4EventGen = new T4EventGen();

T4EventGen::T4EventGen(void)
{
  pluginName = "EventGen";
  eventGenId = 5;
  registerPlugin();

  radius = 0;
  length = 0;
  id = 0;
}

void T4EventGen::initialize(void)
{
  if (settingsFile->getStructManager()->getPolGPD()->general.useDetector) {
    double* pos = settingsFile->getStructManager()->getPolGPD()->general.position;
    targetPos = G4ThreeVector(pos[0], pos[1], pos[2]);
    radius = settingsFile->getStructManager()->getPolGPD()->target_nh3_radius;
    length = settingsFile->getStructManager()->getPolGPD()->target_length;
  } else {
    targetPos = G4ThreeVector(0, 0, 0);
    radius = 0.;
    length = 0.;

    T4SDetector* t4SDetector;
    for (unsigned int i = 0;
        i < settingsFile->getStructManager()->getDetector()->size(); i++) {
      t4SDetector = &settingsFile->getStructManager()->getDetector()->at(i);
      if (t4SDetector->useDetector) {
        string name = t4SDetector->name;
        if (name == "LH2") {
          targetPos = G4ThreeVector(t4SDetector->position[0], t4SDetector->position[1], t4SDetector->position[2]);
          radius = 20. * CLHEP::mm;
          length = 2615. * CLHEP::mm;
          break;
        }
      }
      //to do: implement dy target
    }
  }

  T4SBeam* beam = settingsFile->getStructManager()->getBeam();

  if (beam->useBeamfile) {
    beamFile = T4BeamFileList::getInstance()->getBeamFile(beam->beamFileBackend);
    beamFileType = settingsFile->getBeamFileType(beam->beamFileType);
    beamFile->loadFile(
        settingsFile->getStructManager()->getExternal()->beamFile);
  }

  id = settingsFile->getStructManager()->getBeam()->beamParticle;
  particleGun->SetParticleDefinition(getParticleByID(id));
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
}

void T4EventGen::generateEvent(G4Event* event)
{
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();

  T4BeamFileParticle beamFileParticle;
  while (true) {
    if (beam->useBeamfile) {
      beamFileParticle = beamFile->getNextEntry(beamFileType);
      T4EventManager::getInstance()->getBeamData()->uservar[0] =
          beamFileParticle.posX;
      T4EventManager::getInstance()->getBeamData()->uservar[1] =
          beamFileParticle.posY;
    } else {
      setOrigin(beamFileParticle);
      beamFileParticle.posX = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-radius, radius);
      beamFileParticle.posY = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-radius, radius);
    }

    if (sqrt(
        pow(beamFileParticle.posX - targetPos.x(), 2)
            + pow(beamFileParticle.posY - targetPos.y(), 2)) < radius)
      break;
  }

  particleGun->SetParticleEnergy(beamFileParticle.energy);

  double startPositionZ = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),targetPos.z() - length/2.,
      targetPos.z() + length/2.);

  G4ThreeVector position, momentum;
  position = G4ThreeVector(beamFileParticle.posX, beamFileParticle.posY, startPositionZ);

  momentum = beamFileParticle.momentumDirection;
  particleGun->SetParticlePosition(position);
  particleGun->SetParticleMomentumDirection(momentum);
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
  particleGun->GeneratePrimaryVertex(event);

  T4TargetTracking::getInstance()->activateProcessNow();
}

T4ProcessBackEnd* T4EventGen::getEventGeneratorProcess(void)
{
#ifdef USE_HEPGEN
  if (process == NULL) {
    process = new T4HepGenProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
#endif

  // not activated:
  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4HEPGen: This version of TGEANT does not support HEPGen++! Choose another SettingsFile or enable HEPGen++.");
}
