#include "T4VisualizationMode.hh"

static T4VisualizationMode* t4VisualizationMode = new T4VisualizationMode();

T4VisualizationMode::T4VisualizationMode(void)
{
  pluginName = "VisualizationMode";
  registerPlugin();
}

void T4VisualizationMode::initialize(void)
{
  particleGun->SetParticleDefinition(G4Geantino::GeantinoDefinition());
  particleGun->SetNumberOfParticles(0);
  particleGun->SetParticleEnergy(0);
  particleGun->SetParticlePosition(G4ThreeVector(0,0,0));

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
}

void T4VisualizationMode::generateEvent(G4Event* event)
{
  particleGun->GeneratePrimaryVertex(event);
}
