#include "T4Cosmic.hh"

static T4Cosmic* t4Cosmic = new T4Cosmic();

T4Cosmic::T4Cosmic(void)
{
  pluginName = "Cosmics";
  registerPlugin();

  energy = 0;
}

void T4Cosmic::initialize(void)
{
  energy = settingsFile->getStructManager()->getBeam()->beamEnergy;
  G4double kinEnergy = energy - getParticleMass(13);
  if (kinEnergy <= 0)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
        "T4Cosmic::initialize: Total energy of particle is smaller than muon mass!");
  particleGun->SetParticleEnergy(kinEnergy);
}

void T4Cosmic::generateEvent(G4Event* event)
{
//  G4int id;
//  G4double kinEnergy;
//  if (settingsFile->getRandom()->flat() > 0.5) {
//    id = -13; //muon
//    energy = settingsFile->getStructManager()->getBeam()->beamEnergy;
//    kinEnergy = energy - getParticleMass(-13);
//  } else {
//    id = 11; //electron (Sr 90 38)
//    kinEnergy = 196. * CLHEP::keV;
//    kinEnergy = 934. * CLHEP::keV; // maximum 2280keV
//    energy = kinEnergy + getParticleMass(-11);
//  }
//  particleGun->SetParticleEnergy(kinEnergy);

  G4int id = 13;
  if (settingsFile->getRandom()->flat() > 0.55)
    id = -13;
  particleGun->SetParticleDefinition(getParticleByID(id));

  T4SCosmic* cosmic = settingsFile->getStructManager()->getCosmic();

//  double zPos[7] = {-90, -60, -30, 0, 30, 60, 90};
//  int rnd = (int) floor(settingsFile->getRandom()->flat() * 7.);
//
//  G4ThreeVector position = G4ThreeVector(
//      cosmic->positionX - cosmic->variationX / 2
//          + settingsFile->getRandom()->flat() * cosmic->variationX, 30.0 * CLHEP::cm,
//          zPos[rnd] * CLHEP::cm - cosmic->variationZ / 2
//          + settingsFile->getRandom()->flat() * cosmic->variationZ);

  G4ThreeVector position = G4ThreeVector(
      cosmic->positionX - cosmic->variationX / 2
          + settingsFile->getRandom()->flat() * cosmic->variationX, 5.0 * CLHEP::m,
      cosmic->positionZ - cosmic->variationZ / 2
          + settingsFile->getRandom()->flat() * cosmic->variationZ);
  particleGun->SetParticlePosition(position);

  G4ThreeVector momentum;
  if (cosmic->angleVariation != 0) {
    G4double phi = settingsFile->getRandom()->flat() * 360.0 * CLHEP::deg;
    G4double theta = 0 * CLHEP::deg;
    while (true) {
      G4double x = settingsFile->getRandom()->flat();
      if (settingsFile->getRandom()->flat() < pow(cos(x), 2)) {
        phi = x / 3.141592653589 * 360.0 * CLHEP::deg;
        if (phi < cosmic->angleVariation)
          break;
      }
    }
    momentum.setRThetaPhi(-1, theta, phi);
  } else {
    momentum.set(0, -1, 0);
  }
  particleGun->SetParticleMomentumDirection(momentum);

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  G4double kinEnergy = energy - getParticleMass(13);
  momentum.setMag(kinEnergy / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = (energy) / CLHEP::GeV;
  particle.p[4] = getParticleMass(id) / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(), position.z());

  particleGun->GeneratePrimaryVertex(event);
}
