#include "T4Pythia8Process.hh"

#ifdef USE_PYTHIA8

void T4Pythia8Process::initPythia ( void ) {
  if ( Pythia8 != NULL )
    delete Pythia8;

  printf("Initing pythia! \n");
  Pythia8 = new Pythia();


  string filename = settingsFile->getStructManager()->getExternal()->localGeneratorFile;
  replacePathEnvTGEANT(filename);
  ifstream myFile ( filename );
  if ( !myFile.good() ) {
    T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Generator file not good: %s \n", settingsFile->getStructManager()->getExternal()->localGeneratorFile.c_str() );
  }
  string line;
  
  while ( !myFile.eof() ) {
    getline ( myFile, line );
    if (line.length() > 0)
      if (line[0] != '#')
        Pythia8->readString ( line.c_str() );
  }
  
  Pythia8->settings.parm ( "Beams:idA", settingsFile->getStructManager()->getBeam()->beamParticle );
  Pythia8->init();
}

T4Pythia8Process::T4Pythia8Process ( void ) {
  Pythia8 = NULL;
  initPythia();
}

T4Pythia8Process::~T4Pythia8Process ( void ) {
  delete Pythia8;
}

G4VParticleChange* T4Pythia8Process::PostStepDoIt ( const G4Track& aTrack,
    const G4Step& aStep ) {
  aParticleChange.Initialize ( aTrack );

  G4ThreeVector vertexPosition = aStep.GetPostStepPoint()->GetPosition();
  G4double vertexTime = aStep.GetPostStepPoint()->GetGlobalTime();


  G4ThreeVector momentum = aStep.GetPostStepPoint()->GetMomentum();

  Pythia8->settings.parm ( "Beams:pxA", momentum.getX() / CLHEP::GeV );
  Pythia8->settings.parm ( "Beams:pyA", momentum.getY() / CLHEP::GeV );
  Pythia8->settings.parm ( "Beams:pzA", momentum.getZ() / CLHEP::GeV );
//   Pythia8->settings.parm ( "Print:quiet", 1);
  Pythia8->init();

  while( !Pythia8->next() ) continue; //Bug fixing by Yu-Shiang

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  beamData->nBeamParticle = 0;
  
  int realParticlesCount = 0;
  for ( int i = 0; i < Pythia8->event.size(); ++i )
    if (Pythia8->event[i].isFinal())
      realParticlesCount++;
    
  aParticleChange.SetNumberOfSecondaries(realParticlesCount);
     

  for ( int i = 0; i < Pythia8->event.size(); ++i ) {
    T4BeamParticle newParticle;
    newParticle.p[0] = Pythia8->event[i].px() * CLHEP::GeV;
    newParticle.p[1] = Pythia8->event[i].py() * CLHEP::GeV;
    newParticle.p[2] = Pythia8->event[i].pz() * CLHEP::GeV;
    newParticle.p[3] = Pythia8->event[i].e() * CLHEP::GeV;
    newParticle.p[4] = Pythia8->event[i].m() * CLHEP::GeV;
    newParticle.k[0] = Pythia8->event[i].status();
    newParticle.k[1] = Pythia8->event[i].id();
    newParticle.k[2] = Pythia8->event[i].mother1();
    newParticle.k[3] = Pythia8->event[i].daughter1();
    newParticle.k[4] = Pythia8->event[i].daughter2();
    if ( Pythia8->event[i].isFinal() ) {
      G4ThreeVector momentumDirection ( newParticle.p[0], newParticle.p[1], newParticle.p[2] );
      G4DynamicParticle* aParticle = new G4DynamicParticle ( getParticleByID ( Pythia8->event[i].id() ),
          momentumDirection );
      G4Track* aSecondaryTrack = new G4Track ( aParticle, vertexTime,
          vertexPosition );
      aSecondaryTrack->SetTouchableHandle ( aTrack.GetTouchableHandle() );
      aSecondaryTrack->SetParentID ( aTrack.GetTrackID() );
      aParticleChange.AddSecondary ( aSecondaryTrack );
    }
    newParticle.p[0] /= CLHEP::GeV;
    newParticle.p[1] /= CLHEP::GeV;
    newParticle.p[2] /= CLHEP::GeV;
    newParticle.p[3] /= CLHEP::GeV;
    newParticle.p[4] /= CLHEP::GeV;
    beamData->beamParticles.push_back ( newParticle );
    beamData->nBeamParticle++;
  }

  //this line spams so much shit, i cant stand it! 
//   Pythia8->event.list(false,true,3);
  beamData->generator = eventGenId;
  setThreeVector<double> ( beamData->vertexPosition, vertexPosition.x(),
                           vertexPosition.y(), vertexPosition.z() );
  beamData->vertexTime = vertexTime;

  aParticleChange.ProposeTrackStatus ( fStopAndKill );
  targetTracking->processCalled();

  T4SMessenger::getInstance()->printMessage ( T4SVerboseMore, __LINE__, __FILE__,
      "T4Pythia8Process::PostStepDoIt: T4Pythia8Process was executed." );

  return G4VDiscreteProcess::PostStepDoIt ( aTrack, aStep );
}






#endif
