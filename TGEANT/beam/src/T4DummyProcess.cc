#include "T4DummyProcess.hh"

static T4DummyBeamPlugin* t4DummyBeamPlugin = new T4DummyBeamPlugin();

T4DummyBeamPlugin::T4DummyBeamPlugin(void)
{
  eventGenId = 4;

  pluginName = "DummyGen";
  registerPlugin();
}

T4ProcessBackEnd* T4DummyBeamPlugin::getEventGeneratorProcess(void)
{
  if (process == NULL) {
    process = new T4DummyProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
}

G4VParticleChange* T4DummyProcess::PostStepDoIt(const G4Track& aTrack,
    const G4Step& aStep)
{
  aParticleChange.Initialize(aTrack);

  aParticleChange.SetNumberOfSecondaries(0);
  aParticleChange.ProposeTrackStatus(fStopAndKill);
  targetTracking->processCalled();

  T4SMessenger::getInstance()->printMessage(T4SVerboseMore, __LINE__, __FILE__,
      "T4DummyProcess::PostStepDoIt: T4DummyProcess was executed.");

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();

  G4ThreeVector vertexPosition = aStep.GetPostStepPoint()->GetPosition();
  setThreeVector<double>(beamData->vertexPosition, vertexPosition.x(),
      vertexPosition.y(), vertexPosition.z());
  beamData->vertexTime = aStep.GetPostStepPoint()->GetGlobalTime();
  beamData->generator = eventGenId;

  T4BeamParticle particle;
  particle.k[0] = 21;
  particle.k[1] = aTrack.GetParticleDefinition()->GetPDGEncoding();
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
//   particle.k[4] = 0; // no daughter
  particle.k[4] = T4TargetTracking::getInstance()->getDistanceForTargetStepping()*0.028 + 7.0; // no daughter

  particle.p[0] = aStep.GetPostStepPoint()->GetMomentum().x();
  particle.p[1] = aStep.GetPostStepPoint()->GetMomentum().y();
  particle.p[2] = aStep.GetPostStepPoint()->GetMomentum().z();
  particle.p[3] = aStep.GetPostStepPoint()->GetTotalEnergy() / CLHEP::GeV;
  particle.p[4] = particle.p[3] - aStep.GetPostStepPoint()->GetKineticEnergy() / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);
  beamData->nBeamParticle = 1;

  return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
}
