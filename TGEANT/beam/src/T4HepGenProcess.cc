#include "T4HepGenProcess.hh"
#ifdef USE_HEPGEN

T4HepGenProcess::T4HepGenProcess(void)
{
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();

  hGenManager = HGenManager::getInstance();

  hParams = new HParams();
  HParamManager::resetParamStruct(hParams);
  T4SHEPGen* t4HEPGen = settingsFile->getStructManager()->getHEPGen();

  hParams->IPROC = 5;
  hParams->LST.at(19) = t4HEPGen->LST;
  hParams->CUT.at(0) = t4HEPGen->CUT[0];
  hParams->CUT.at(1) = t4HEPGen->CUT[1];
  hParams->CUT.at(2) = t4HEPGen->CUT[2];
  hParams->CUT.at(3) = t4HEPGen->CUT[3];
  hParams->CUT.at(4) = t4HEPGen->CUT[4];
  hParams->CUT.at(5) = t4HEPGen->CUT[5];
  hParams->CUT.at(6) = t4HEPGen->CUT[6];
  hParams->CUT.at(7) = t4HEPGen->CUT[7];
  hParams->CUT.at(8) = t4HEPGen->CUT[8];
  hParams->CUT.at(9) = t4HEPGen->CUT[9];
  hParams->CUT.at(10) = t4HEPGen->CUT[10];
  hParams->CUT.at(11) = t4HEPGen->CUT[11];
  hParams->CUT.at(12) = t4HEPGen->CUT[12];
  hParams->CUT.at(13) = t4HEPGen->CUT[13];

  hParams->THMAX = t4HEPGen->THMAX;
  hParams->alf = t4HEPGen->alf;
  hParams->atomas = t4HEPGen->atomas;
  hParams->probc = t4HEPGen->probc;
  hParams->bcoh = t4HEPGen->bcoh;
  hParams->bin = t4HEPGen->bin;
  
  //do this directly
  hParams->clept = getParticleByID(settingsFile->getStructManager()->getBeam()->beamParticle)->GetPDGCharge();
  hParams->slept = -t4HEPGen->clept;
/*
  hParams->clept = t4HEPGen->clept;
  hParams->slept = t4HEPGen->slept;*/

  hParams->B0 = t4HEPGen->B0;
  hParams->xbj0 = t4HEPGen->xbj0;
  hParams->alphap = t4HEPGen->alphap;

  hParams->ELEPT = beam->beamEnergy;
  hParams->IPART = beam->beamParticle;

  hParams->IVECM.at(0) = t4HEPGen->IVECM;

  hParams->aux1.push_back("aux1");

  switch (hParams->IVECM.at(0)) {
    case 0:
      hParams->IVECM.at(1) = 0;
      break; // DVCS
    case 1:
      hParams->IVECM.at(1) = 22;
      hParams->aux2.push_back("aux2");
      if (settingsFile->getStructManager()->getHEPGen()->usePi0_transv_table) {
        hParams->aux1.push_back(settingsFile->getStructManager()->getExternal()->libHEPGen_Pi0_transv);
        hParams->aux2.push_back("1");
      } else {
        hParams->aux1.push_back(settingsFile->getStructManager()->getExternal()->libHEPGen_Pi0);
        hParams->aux2.push_back("0");
      }
      break; // pi0 -> yy
    case 2:
      hParams->IVECM.at(1) = 211;
      break; // rho0 -> pi+ pi-
    case 3:
      hParams->IVECM.at(1) = 321;
      break; // phi -> K+ K-
    case 4:
      hParams->IVECM.at(1) = 11;
      break; // J/Phi -> e+ e- and mu+ mu- (approx 50:50)
    case 6:
      hParams->IVECM.at(1) = 211;
      break; // omega -> pi+ pi- pi0
    case 7:
      hParams->IVECM.at(1) = 111;
      hParams->aux1.push_back(settingsFile->getStructManager()->getExternal()->libHEPGen_Rho);
      break; // rho+ -> pi0 pi+
    case 8:
      hParams->IVECM.at(1) = 22;
      break; // omega -> gamma pi0
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
          __FILE__,
          "T4HepGenProcess: Unknown IVECM.at(0) = " + intToStr(hParams->IVECM.at(0))
              + ". Set it to 0 (DVCS).");
      break;
  }

  hParams->I_BEAMREAD = 0; // not implemented in hepgen_c

  // target
  if (t4HEPGen->target == "p") {
    hParams->EHADR = G4Proton::ProtonDefinition()->GetPDGMass() / CLHEP::GeV;
    hParams->PARL.at(0) = 1; // AHADR
    hParams->PARL.at(1) = 1; // ZHADR
  }

  // fill PARL vector
  hParams->PARL.push_back(hParams->ELEPT); // 2
  hParams->PARL.push_back(hParams->EHADR); // 3
  hParams->PARL.push_back(hParams->IPART); // 4
  hParams->PARL.push_back(hParams->IPROC); // 5

  // to be filled by kfvm, kfdp1, kfdp2 codes for vector-meson, and the 2 decay particles
  hParams->PARL.push_back(0.0); // 6
  hParams->PARL.push_back(0.0); // 7
  hParams->PARL.push_back(0.0); // 8

  hParams->PARL.push_back(hParams->THMAX); // 9
  hParams->PARL.push_back(hParams->atomas); // 10
  hParams->PARL.push_back(hParams->probc); // 11
  hParams->PARL.push_back(hParams->bcoh); // 12
  hParams->PARL.push_back(hParams->bin); // 13
  hParams->PARL.push_back(hParams->alf); // 14
  hParams->PARL.push_back(t4HEPGen->TLIM[0]); // 15
  hParams->PARL.push_back(t4HEPGen->TLIM[1]); // 16
  hParams->PARL.push_back(hParams->clept); // 17
  hParams->PARL.push_back(hParams->slept); // 18

  hParamManager = new HParamManager(hParams);
  hGenManager->setParamManager(hParamManager);
  hGenManager->setupGeneratorOneShot();
  hGenManager->setSeed((int) T4SettingsFile::getInstance()->getStartSeed() + 1);

  if (T4SettingsFile::getInstance()->getStructManager()->getGeneral()->verboseLevel > 2)
    hGenManager->enableDebug();
}

T4HepGenProcess::~T4HepGenProcess(void)
{
  delete hParamManager;
  delete hGenManager;
}

G4VParticleChange* T4HepGenProcess::PostStepDoIt(const G4Track& aTrack,
    const G4Step& aStep)
{
  aParticleChange.Initialize(aTrack);

  G4double energy = aStep.GetPostStepPoint()->GetTotalEnergy() / CLHEP::GeV;
  hParamManager->getStruct()->ELEPT = energy; // needs GeV
  hParamManager->getStruct()->PARL.at(2) = energy;
  HEvent* hEvent = NULL;

  hGenManager->oneShot();
  //  For the purpose of beeing able to reweight at phast-lvl with HEPGen-in-PHAST
  //  We do not check for good weights as before. 
  //   while (hGenManager->getEvent()->getStruct()->USERVAR.at(2) == 0)
  //     hGenManager->oneShot();
  hEvent = hGenManager->getEvent();

  std::pair<double, G4ThreeVector> rotation = getRotation(
      aStep.GetPostStepPoint()->GetMomentumDirection());

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();

  std::vector<HParticle*> listOfParticles = hEvent->getStruct()->listOfParticles;

  G4int nSecondaries = 0;
  for (unsigned int i = 0; i < listOfParticles.size(); i++)
    if (listOfParticles.at(i)->getParticleAuxFlag() == 1)
      nSecondaries++;

  aParticleChange.SetNumberOfSecondaries(nSecondaries);

  G4ThreeVector vertexPosition = aStep.GetPostStepPoint()->GetPosition();
  G4double vertexTime = aStep.GetPostStepPoint()->GetGlobalTime();

  vector<T4BeamParticle> lowList;

  for (unsigned int i = 0; i < listOfParticles.size(); i++) {
    HParticle* secondary = listOfParticles.at(i);

    G4int id = secondary->getParticleType();
    HVector3 hvector3 = secondary->getTVector();
    G4ThreeVector helper = G4ThreeVector(hvector3.X(), hvector3.Y(), hvector3.Z());
    if (rotation.first != 0)
      helper.rotate(rotation.first, rotation.second);

    G4double totalEnergy = secondary->getEnergy() * CLHEP::GeV; // now in MeV
    G4double particleMass = secondary->getMass() * CLHEP::GeV; // now in MeV

    T4BeamParticle particle,particle2;

    if (secondary->getParticleAuxFlag() == 1) {
      G4ThreeVector momentumDirection = helper.unit();
      G4double kinEnergy = totalEnergy - particleMass;
      G4DynamicParticle* aParticle = new G4DynamicParticle(getParticleByID(id),
          momentumDirection, kinEnergy);
      G4Track* aSecondaryTrack = new G4Track(aParticle, vertexTime,
          vertexPosition);
      aSecondaryTrack->SetTouchableHandle(aTrack.GetTouchableHandle());
      aSecondaryTrack->SetParentID(aTrack.GetTrackID());
      aParticleChange.AddSecondary(aSecondaryTrack);
    }
    
    particle2.k[0] = particle.k[0] = secondary->getParticleAuxFlag();
    particle2.k[1] = particle.k[1] = id;
    particle2.k[2] = particle.k[2] = secondary->getParticleOrigin();
    particle2.k[3] = particle.k[3] = secondary->getParticleDaughter1();
    particle2.k[4] = particle.k[4] = secondary->getParticleDaughter2();

    //invert the low-particle
    particle2.k[0] *= -1;
    
    float x[2],y[2],z[2],e[2],m[2];
    
    //split the kinematics from double to 2 times float precision
    doubleToFloats(helper.x(),x);
    doubleToFloats(helper.y(),y);
    doubleToFloats(helper.z(),z);
    doubleToFloats(totalEnergy / CLHEP::GeV,e);
    doubleToFloats(particleMass / CLHEP::GeV,m);
    
    
    particle.p[0] = x[0];
    particle.p[1] = y[0];
    particle.p[2] = z[0];
    particle.p[3] = e[0];
    particle.p[4] = m[0];
    
    particle2.p[0] = x[1];
    particle2.p[1] = y[1];
    particle2.p[2] = z[1];
    particle2.p[3] = e[1];
    particle2.p[4] = m[1];
    
    //push the lower digits to their own vector
    lowList.push_back(particle2);
    beamData->beamParticles.push_back(particle);
  }
  //add the virtual particles containing the lower digits to the back with inverted k-code
  beamData->beamParticles.insert(beamData->beamParticles.end(),lowList.begin(),lowList.end());
  

  beamData->nBeamParticle = beamData->beamParticles.size();
  beamData->generator = eventGenId;
  setThreeVector<double>(beamData->vertexPosition, vertexPosition.x(),
      vertexPosition.y(), vertexPosition.z());
  beamData->vertexTime = vertexTime;
  beamData->x_bj = hEvent->getXbj();
  beamData->y = hEvent->getY();
  beamData->q2 = hEvent->getQsq();
  beamData->w2 = hEvent->getWsq();
  beamData->nu = hEvent->getNu();

  HEventData* hEventStruct = hEvent->getStruct();
  std::copy(hEventStruct->USERVAR.begin() + 2, hEventStruct->USERVAR.end(),
      &beamData->uservar[2]);
  std::copy(hParams->LST.begin(), hParams->LST.end(), beamData->lst);
  std::copy(hEventStruct->PARL.begin(), hEventStruct->PARL.end(), beamData->parl);
  std::copy(hParams->CUT.begin(), hParams->CUT.end(), beamData->cut);

  aParticleChange.ProposeTrackStatus(fStopAndKill);
  targetTracking->processCalled();

  T4SMessenger::getInstance()->printMessage(T4SVerboseMore, __LINE__, __FILE__,
      "T4HepGenProcess::PostStepDoIt: T4HepGenProcess was executed.");

  return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
}


void T4HepGenProcess::doubleToFloats(double _in, float* out)
{
  doubleToFloats(_in,out[0],out[1]);
}


void T4HepGenProcess::doubleToFloats(double _in, float& high, float& low)
{
  high = static_cast<float>(_in);
  double tmp = _in - high;
  low = static_cast<float>(tmp);
}



#endif
