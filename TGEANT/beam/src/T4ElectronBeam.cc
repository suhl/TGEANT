#include "T4ElectronBeam.hh"

static T4ElectronBeam* t4ElectronBeam = new T4ElectronBeam();

T4ElectronBeam::T4ElectronBeam(void)
{
  pluginName = "ElectronBeam";
  registerPlugin();

  id = 0;
  energy = 0;
  electronMass = 0;
}

void T4ElectronBeam::initialize(void)
{
  id = 11;
  energy = 40.0 * CLHEP::GeV;
  particleGun->SetParticleDefinition(getParticleByID(id));
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
  electronMass = getParticleMass(id);
}

void T4ElectronBeam::generateEvent(G4Event* event)
{
  G4double x, y, dx, dy, mom;
  G4double kinEnergy;

  double caloPosX = 0;
  double caloPosY = 0;

  // --------------------------------------------------------------------------
  // ECAL2:
  // we shoot the electron beam at 200 different positions
  // -> two horizontal lines
//    int caloPosNumber = (int) floor(settingsFile->getRandom()->flat() * 200.);
//    caloPosX = 1.28 + 2.56 * ((caloPosNumber%100)-50) * CLHEP::cm;
//    caloPosY = (caloPosNumber>=100)? 75. * CLHEP::cm : -30 * CLHEP::cm;
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // ECAL1:
  // we shoot at 10 different positions (extracted from rd run)
      double posX[10] = {-42, -16, 4, 27, 45, 53, 57, 57, 57, 57};
      double posY[10] = {22, 26, 26, 26, 26, 0, -10, -20, -30, -40};

      int caloPosNumber = (int) floor(settingsFile->getRandom()->flat() * 10.);
      caloPosX = -posX[caloPosNumber] * CLHEP::cm;
      caloPosY = -posY[caloPosNumber] * CLHEP::cm;
  // --------------------------------------------------------------------------


  while (true) {
    x = CLHEP::RandGauss::shoot(settingsFile->getRandom()->getTheEngine(), -0.2, 0.5);
    y = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(), -2., 2.5);
    dx = CLHEP::RandGauss::shoot(settingsFile->getRandom()->getTheEngine(),0.00065, 0.0004);
    dy = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-0.0015, 0.0015);
    mom = CLHEP::RandGauss::shoot(settingsFile->getRandom()->getTheEngine(),energy, 0.5);

    G4double x670 = x + 130. * dx;
    G4double y670 = y + 130. * dy;
    if ((x670 * x670 + (y670 - 0.4) * (y670 - 0.4)) <= 1.4 * 1.4) {
      kinEnergy = mom - electronMass;
      if (kinEnergy > 0)
        break;
    }
  }

  particleGun->SetParticleEnergy(kinEnergy);

  G4ThreeVector position = G4ThreeVector(x * CLHEP::cm - caloPosX,
      y * CLHEP::cm - caloPosY, -800.0 * CLHEP::cm);
  particleGun->SetParticlePosition(position);

  G4ThreeVector momentum = G4ThreeVector(mom * dx, mom * dy,
      mom * sqrt(1 - dx * dx - dy * dy));
  particleGun->SetParticleMomentumDirection(momentum);


  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  momentum.setMag(kinEnergy / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = mom / CLHEP::GeV;
  particle.p[4] = electronMass / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(),
      position.z());

  particleGun->GeneratePrimaryVertex(event);
}

  // --------------------------------------------------------------------------
  // ECAL1 MODULE TUNING

  // OLGA
//    double moduleSize = 14.314;
//    double posX = -148.568 * CLHEP::cm;
//    double posY = 7.29 * CLHEP::cm;

  // MAINZ
//    double moduleSize = 7.5;
//    double posX = -42.85 * CLHEP::cm;
//    double posY = 94.71 * CLHEP::cm;

  // GAMS
//    double moduleSize = 3.82;
//    double posX = -67.065 * CLHEP::cm;
//    double posY = -1.965 * CLHEP::cm;

  // SHASHLIK
//    double moduleSize = 3.82;
//    double posX = -2.025 * CLHEP::cm;
//    double posY = 32.535 * CLHEP::cm;

  // --------------------------------------------------------------------------
  // ECAL0 MODULE TUNING
//    double moduleSize = 3.96;
//    double posX = -56 * CLHEP::cm;
//    double posY = 0 * CLHEP::cm;


  // --------------------------------------------------------------------------
  // ECAL2 MODULE TUNING
  // SHASHLIK
//    double moduleSize = 3.82;
//    double posX = -21.065 * CLHEP::cm;
//    double posY = -1.915 * CLHEP::cm;

  // GAMS
//    double moduleSize = 3.82;
//    double posX = 78.515 * CLHEP::cm;
//    double posY = -1.915 * CLHEP::cm;

  // GAMSRH
//    double moduleSize = 3.82;
//    double posX = -101.495 * CLHEP::cm;
//    double posY = 1.915 * CLHEP::cm;

//      caloPosX = -posX;
//      caloPosY = -posY;
//
//      x = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(), -moduleSize/2., moduleSize/2.);
//      y = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(), -moduleSize/2., moduleSize/2.);
//      kinEnergy = energy - electronMass;
//      G4ThreeVector momentum = G4ThreeVector(0,0,1);
  // --------------------------------------------------------------------------

//  G4ThreeVector position = G4ThreeVector(x * CLHEP::cm - caloPosX,
//      y * CLHEP::cm - caloPosY, 800.0 * CLHEP::cm);
