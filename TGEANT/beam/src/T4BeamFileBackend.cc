#include "T4BeamFileBackend.hh"

T4BeamFileList* T4BeamFileList::instance = NULL;

T4BeamFileList* T4BeamFileList::getInstance(void)
{
  if (instance == NULL)
    instance = new T4BeamFileList();
  return instance;
}

T4BeamFileBackend* T4BeamFileList::getBeamFile(string name)
{
  // check if plugin exists
  if (beamFiles.find(name) == beamFiles.end())
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamFileList::getBeamFile: Name of beam file '" + name + "' not found!");

  beamFiles[name]->init();
  return beamFiles[name];
}

T4BeamFileList::~T4BeamFileList(void)
{
  map<string, T4BeamFileBackend*>::iterator it;
  for (it = beamFiles.begin(); it != beamFiles.end(); it++)
    delete it->second;
}

void T4BeamFileBackend::registerPlugin(void)
{
  if (beamFileName == "NA")
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamFileBackend: Name of beam file is unset! You need to set 'beamFileName' before calling 'registerPlugin()'.");

  // check if the plugin name is already registered
  if (T4BeamFileList::getInstance()->beamFiles.find(beamFileName)
      != T4BeamFileList::getInstance()->beamFiles.end())
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamFileBackend: Name of beam file '" + beamFileName
            + "' used twice!");

  T4BeamFileList::getInstance()->beamFiles[beamFileName] = this;
}

void T4BeamFileBackend::init(void)
{
  T4SBeam* beam = T4SettingsFile::getInstance()->getStructManager()->getBeam();
  particleId = beam->beamParticle;
  zPosConvention = beam->beamFileZConvention;
}

const T4BeamFileParticle& T4BeamFileBackend::getNextEntry(
    TGEANT::T4BeamFileType beamFileType)
{
  if (index != beamList.end()) {
    while (((*index).beamFileType != beamFileType)
        && (beamFileType != TGEANT::Both))
      if (index + 1 != beamList.end())
        index++;
      else
        return rollOver(beamFileType);
    return (*index++);
  } else
    return rollOver(beamFileType);
}

const T4BeamFileParticle& T4BeamFileBackend::rollOver(
    TGEANT::T4BeamFileType beamFileType)
{
  index = beamList.begin();
  return getNextEntry(beamFileType);
}

// This function checks for f77 header-size (4bit). If it fails, it should be 8 bit otherwise we exit the program.
G4bool T4BeamFileBackend::isValidHeader(void)
{
  // first we try to find a 4bit int with 24 content
  int counter = 0;
  while (!inFile.eof() || counter > 100) {
    counter++;
    if (getShort() != 24)
      continue;
    // expect we have one here
    // next we expect a type - it has to be 1 or 2, else our header length assumption was wrong!
    G4int type = getShort();
    if (type == 1 || type == 2)
      is4BitFile = true;
    return true;
  }

  // try 8 bit
  inFile.seekg(0);
  counter = 0;
  while (!inFile.eof() || counter > 100) {
    counter++;
    if (getLong() != 24)
      continue;
    // expect we have one here
    // next we expect a type - it has to be 1 or 2, else our header length assumption was wrong!
    G4int type = getShort();
    if (type == 1 || type == 2)
      is4BitFile = false;
    return true;
  }

  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4BeamFile::isValidHeader: Neither 4 Bit-Headers nor 8 Bit-Headers found! Beamfile is corrupted!");

  return false;
}
