#include "T4BeamBackend.hh"

T4BeamPluginList* T4BeamPluginList::instance = NULL;

T4BeamPluginList* T4BeamPluginList::getInstance(void)
{
  if (instance == NULL)
    instance = new T4BeamPluginList();
  return instance;
}

T4BeamPluginList::~T4BeamPluginList(void)
{
  map<string, T4BeamBackEnd*>::iterator it;
  for (it = beamPlugins.begin(); it != beamPlugins.end(); it++)
    delete it->second;
}

T4BeamBackEnd::~T4BeamBackEnd(void)
{
  T4BeamFileList::resetInstance();
  if (pileUp != NULL)
    delete pileUp;
  if (extrapolate != NULL)
    delete extrapolate;
}

void T4BeamBackEnd::initialize(void)
{
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();
  if (beam->useBeamfile) {
    beamFile = T4BeamFileList::getInstance()->getBeamFile(beam->beamFileBackend);
    beamFileType = settingsFile->getBeamFileType(beam->beamFileType);
    beamFile->loadFile(settingsFile->getStructManager()->getExternal()->beamFile);

    if (beam->usePileUp)
      pileUp = new T4PileUp(beam->beamFlux, beam->timeGate, beam->additionalPileUpFlux);
  }
  extrapolate = new T4Extrapolate(getParticleByID(beam->beamParticle));
  T4SettingsFile::getInstance()->getStructManager()->getBeam()->useNewBeamParticle = true;
}

void T4BeamBackEnd::generateEvent(G4Event* event)
{
  // This function is in use for: HEPGEN++ and PYTHIA6
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();

  // incoming beam particle
  G4int incomingId = beam->beamParticle;
  particleGun->SetParticleDefinition(getParticleByID(incomingId));

  G4ThreeVector position, momentum;
  static T4BeamFileParticle beamFileParticle;

  if (beam->useTargetExtrap) {
    if (beam->useBeamfile) {
      while (true) {
        if(T4SettingsFile::getInstance()->getStructManager()->getBeam()->useNewBeamParticle)
          beamFileParticle = beamFile->getNextEntry(beamFileType);
        T4EventManager::getInstance()->getBeamData()->uservar[0] =
            beamFileParticle.posX;
        T4EventManager::getInstance()->getBeamData()->uservar[1] =
            beamFileParticle.posY;

        if (extrapolate->extrapAndCalcTargetDist(beamFileParticle))
          break;
      }
    } else {
      setOrigin(beamFileParticle);
      if (!extrapolate->extrapAndCalcTargetDist(beamFileParticle)) {
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
            "T4BeamBackEnd::generateEvent: Target was found but beamfile is not activated. The backwards extrapolation of the default beam particle with momentum=(0,0,e) and position=(0,0,0) does not hit the target.");
      }
    }
  } else {
    if (beam->useBeamfile) {
      if(T4SettingsFile::getInstance()->getStructManager()->getBeam()->useNewBeamParticle)
        beamFileParticle = beamFile->getNextEntry(beamFileType);
      T4EventManager::getInstance()->getBeamData()->uservar[0] =
          beamFileParticle.posX;
      T4EventManager::getInstance()->getBeamData()->uservar[1] =
          beamFileParticle.posY;
    } else {
      setOrigin(beamFileParticle);
    }
    extrapolate->extrap(beamFileParticle);
  }

  position = G4ThreeVector(beamFileParticle.posX, beamFileParticle.posY, beamFileParticle.posZ);
  momentum = beamFileParticle.momentumDirection;
//   particleGun->SetParticleEnergy(beamFileParticle.energy);
    particleGun->SetParticleEnergy(
      beamFileParticle.energy + extrapolate->getAdditionalKineticEnergy());

  particleGun->SetParticlePosition(position);
  particleGun->SetParticleMomentumDirection(momentum);
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
  particleGun->GeneratePrimaryVertex(event);

  // The pile up code must stay below the code for the "real" primary particle
  if (beam->useBeamfile && beam->usePileUp)
    generatePileUp(event);

  if (!beam->useTargetExtrap && !beam->useHadronicInteractionEGCall)
    T4TargetTracking::getInstance()->activateProcessNow();
}

void T4BeamBackEnd::generatePileUp(G4Event* event)
{
  for (int i = 0; i < pileUp->getPileUpTracksNumber(); i++) {
    T4BeamFileParticle beamFileParticle;
    beamFileParticle = beamFile->getNextEntry(TGEANT::Both);
    setupPileUp(beamFileParticle);
    particleGun->GeneratePrimaryVertex(event);
  }

  if (settingsFile->getStructManager()->getBeam()->useAdditionalPileUp &&
      settingsFile->getStructManager()->getBeam()->beamFileBackend == "Pion") {
    if (beamFileForAdditionalPileUp == NULL) {
      beamFileForAdditionalPileUp = T4BeamFileList::getInstance()->getBeamFile(
          "Muon");
      beamFileForAdditionalPileUp->setBeamFileZConvention(
          settingsFile->getStructManager()->getBeam()
              ->beamFileZConventionForAdditionalPileUp);
      beamFileForAdditionalPileUp->loadFile(
          settingsFile->getStructManager()->getExternal()->beamFileForAdditionalPileUp);
    }

    int muonId;
    switch (settingsFile->getStructManager()->getBeam()->beamParticle) {
      case -211:
        muonId = -13;
        break;
      case 211:
        muonId = 13;
        break;
      default:
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
            "T4BeamBackEnd::generatePileUp: Expecting pi+ (id=211) or pi- (id=-211) beam and not :'"
                + intToStr(settingsFile->getStructManager()->getBeam()->beamParticle)
                + "'. Can't create the additional muon pileup from pion decay.");
        return;
        break;
    }

    particleGun->SetParticleDefinition(getParticleByID(muonId));

    for (int i = 0; i < pileUp->getPileUpTracksNumber(false); i++) {
      T4BeamFileParticle beamFileParticle;
      beamFileParticle = beamFileForAdditionalPileUp->getNextEntry(TGEANT::Both);
      setupPileUp(beamFileParticle);
      particleGun->GeneratePrimaryVertex(event);
    }
  }
}

void T4BeamBackEnd::setupPileUp(T4BeamFileParticle& beamFileParticle)
{
  extrapolate->extrap(beamFileParticle);
  particleGun->SetParticleEnergy(beamFileParticle.energy);
  particleGun->SetParticlePosition(G4ThreeVector(beamFileParticle.posX, beamFileParticle.posY, beamFileParticle.posZ));
  particleGun->SetParticleMomentumDirection(beamFileParticle.momentumDirection);
  particleGun->SetParticleTime(pileUp->getPileUpTrackTimeOffset() * CLHEP::ns);
}

G4ParticleDefinition* T4BeamBackEnd::getParticleByID(G4int id)
{
  T4PhysicsList* t4PhysicsList = (T4PhysicsList*) G4RunManager::GetRunManager()
      ->GetUserPhysicsList();
  return t4PhysicsList->getParticleByID(id);
}

G4double T4BeamBackEnd::getParticleMass(G4int id)
{
  return getParticleByID(id)->GetPDGMass();
}

void T4BeamBackEnd::setOrigin(T4BeamFileParticle& beamFileParticle)
{
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();
  beamFileParticle.posX = 0;
  beamFileParticle.posY = 0;
  beamFileParticle.posZ = 0;
  beamFileParticle.energy = beam->beamEnergy - getParticleMass(beam->beamParticle);
  beamFileParticle.momentumDirection = G4ThreeVector(0, 0, beamFileParticle.energy);

  if (beamFileParticle.energy <= 0)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4BeamBackEnd::generateEvent: Total energy of particle is smaller than its mass!");

  T4EventManager::getInstance()->getBeamData()->uservar[0] = 0;
  T4EventManager::getInstance()->getBeamData()->uservar[1] = 0;
}

void T4BeamBackEnd::registerPlugin(void)
{
  if (pluginName == "NA")
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamBackEnd: Name of beam plugin is unset! You need to set 'pluginName' before calling 'registerPlugin()'.");

  // check if the plugin name is already registered
  if (beamPluginList->beamPlugins.find(pluginName)
      != beamPluginList->beamPlugins.end())
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamBackEnd: Name of beam plugin '" + pluginName + "' used twice!");

  // check if we have a unique event generator id
  if (eventGenId != 0) {
    if (find(beamPluginList->generatorId.begin(), beamPluginList->generatorId.end(), eventGenId) != beamPluginList->generatorId.end())
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
          "T4BeamBackEnd: Event generator id '" + intToStr(eventGenId) + "' is not unique!");
    else
      beamPluginList->generatorId.push_back(eventGenId);
  }

  beamPluginList->beamPlugins[pluginName] = this;
}
