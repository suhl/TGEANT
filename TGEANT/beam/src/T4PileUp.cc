#include "T4PileUp.hh"

T4PileUp::T4PileUp(double _beamFlux, double _timeGate, double _beamFlux2)
{
  beamFlux = _beamFlux;
  beamFlux2 = _beamFlux2;
  timeGate = _timeGate;
  randMan = T4SettingsFile::getInstance()->getRandom();
}

int T4PileUp::getPileUpTracksNumber(bool std)
{
  double pileM = 2.0 * timeGate; // beamflux in 1/ns, timeGate in ns
  if (std)
    pileM *= beamFlux;
  else
    pileM *= beamFlux2;

  if (pileM > 0.001) {
    int nPileUp = CLHEP::RandPoisson::shoot(randMan->getTheEngine(),  pileM) - 1.; // subtract the beam particle
    if (nPileUp > 0)
      return nPileUp;
  }
  // in all other cases:
  return 0;
}

double T4PileUp::getPileUpTrackTimeOffset(void)
{
  return timeGate * (-1 + randMan->flat() * 2);
}
