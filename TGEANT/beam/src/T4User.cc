#include "T4User.hh"

static T4User* t4User = new T4User();

T4User::T4User(void)
{
  pluginName = "User";
  registerPlugin();

  id = 0;
  energy = 0;
  user = NULL;
}

void T4User::initialize(void)
{
  id = settingsFile->getStructManager()->getBeam()->beamParticle;
  particleGun->SetParticleDefinition(getParticleByID(id));

  user = settingsFile->getStructManager()->getUser();

  position.set(user->position[0], user->position[1], user->position[2]);
  particleGun->SetParticlePosition(position);

  momentum.set(user->momentum[0], user->momentum[1], user->momentum[2]);
  particleGun->SetParticleMomentumDirection(momentum);

  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
}

void T4User::generateEvent(G4Event* event)
{
  double particleMass = getParticleMass(id);
  double particleMomentum;

  if (user->useRandomEnergy) {
    particleMomentum = settingsFile->getRandom()->flat() * (user->maxEnergy - user->minEnergy) + user->minEnergy;
        
//     double beta = settingsFile->getRandom()->flat();
//     particleMomentum = particleMass * beta / sqrt(1-pow(beta,2));
    
    energy = sqrt(pow(particleMomentum, 2) + pow(particleMass, 2));
  } else {
    energy = settingsFile->getStructManager()->getBeam()->beamEnergy;
    particleMomentum = sqrt(pow(energy, 2) - pow(particleMass, 2));
  }

  if (energy - particleMass <= 0)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
        "T4User::generateEvent: Total energy of particle is smaller than its mass!");

  double kinEnergy = energy - particleMass;
  particleGun->SetParticleEnergy(kinEnergy);

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  momentum.setMag(particleMomentum / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = energy / CLHEP::GeV;
  particle.p[4] = particleMass / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(),
      position.z());

  particleGun->GeneratePrimaryVertex(event);
}
