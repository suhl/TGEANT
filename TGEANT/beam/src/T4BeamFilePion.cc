#include "T4BeamFilePion.hh"

static T4BeamFilePion* t4BeamFilePion = new T4BeamFilePion();

T4BeamFilePion::T4BeamFilePion(void)
{
  beamFileName = "Pion";
  registerPlugin();
}

void T4BeamFilePion::loadFile(std::string fileName)
{
  inFile.open(fileName.c_str(), ios::in | ios::binary);
  if (!inFile.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4BeamFilePion::loadFile: Loading of file '" + fileName
            + "' not possible! Beamfile is corrupted!");
  }

  // now we check if this is a f77 or gfortran binary fileName
  // difference is the different size in start and stop-flag
  // to check this, we read the header in 4 bit and see if the second (first typeflag) is valid (i.e. int 1 or int 2)
  // else if it does not validate - retry with 8 bit int length
  // if its neither 4 nor 8 bit we exit the program
  isValidHeader();

  //reset the filepointer
  inFile.seekg(0);

  while (!inFile.eof()) {
    // wait for startflag
    if (getInt() != 24)
      continue;

    T4BeamFileParticle newEntry;
    switch (getShort()) {
      case 1:
        newEntry.beamFileType = TGEANT::Normal;
        break;
      case 2:
        newEntry.beamFileType = TGEANT::Halo;
        break;
      default:
        continue;
    }

    newEntry.posX = getDouble() * CLHEP::mm;
    newEntry.posY = getDouble() * CLHEP::mm;
    newEntry.posZ = zPosConvention;
    newEntry.particleId = particleId;

    //these are mrad here
    G4double slopeX = getDouble();
    G4double slopeY = getDouble();

    newEntry.energy = getDouble() * CLHEP::GeV; // aKineticEnergy

    // endflag here or event corrupted
    if (getInt() != 24)
      continue;

    // now do the kinematics of the beam and make the beam ready
    G4double sinx, siny, pz;
    sinx = sin(slopeX * 1e-3);
    siny = sin(slopeY * 1e-3);

    // here called energy is just absolute momentum
    pz = newEntry.energy / (sqrt(1. + pow(sinx, 2) + pow(siny, 2)));
    newEntry.momentumDirection = G4ThreeVector(pz * sinx, pz * siny, pz);

    beamList.push_back(newEntry);
  }

  index = beamList.begin();
  G4int start = (G4int) beamList.size()
      * T4SettingsFile::getInstance()->getRandom()->flat();
  index += start;

  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4BeamFilePion::loadFile: File '" + fileName
          + "' has been loaded with " + intToStr(beamList.size())
          + " entries.");
}
