#include "T4PrimakoffProcess.hh"
#ifdef USE_PRIMAKOFF

G4VParticleChange* T4PrimakoffProcess::PostStepDoIt(const G4Track& aTrack,
    const G4Step& aStep)
{
  aParticleChange.Initialize(aTrack);

  // we need this to rotate the complete event w.r.t. the momentum direction of the incident beam particle
  std::pair<double, G4ThreeVector> rotation = getRotation(
      aStep.GetPostStepPoint()->GetMomentumDirection());

  // prepare beamdata output
  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;

  G4ThreeVector vertexPosition = aStep.GetPostStepPoint()->GetPosition();
  beamData->vertexTime = aStep.GetPostStepPoint()->GetGlobalTime();
  beamData->generator = eventGenId;
  setThreeVector<double>(beamData->vertexPosition, vertexPosition.x(),
      vertexPosition.y(), vertexPosition.z());

  // call primakoff generator
  G4double beamMomentum = aStep.GetPostStepPoint()->GetMomentum().mag() / CLHEP::GeV; // momentum/GeV
  bool newBeam = primGenerator->generate(beamMomentum, 28);
  T4SettingsFile::getInstance()->getStructManager()->getBeam()->useNewBeamParticle = newBeam;

  aParticleChange.SetNumberOfSecondaries(primGenerator->getParticles()->size());

  // add the incoming beam particle to the beamData vector at first place
  particle.k[0] = 21;
  particle.k[1] = aTrack.GetParticleDefinition()->GetPDGEncoding();
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  // unit for beamData: GeV
  particle.p[0] = aStep.GetPostStepPoint()->GetMomentum().x() / CLHEP::GeV;
  particle.p[1] = aStep.GetPostStepPoint()->GetMomentum().y() / CLHEP::GeV;
  particle.p[2] = aStep.GetPostStepPoint()->GetMomentum().z() / CLHEP::GeV;
  particle.p[3] = beamMomentum;
  particle.p[4] = aTrack.GetParticleDefinition()->GetPDGMass() / CLHEP::GeV;
  beamData->beamParticles.push_back(particle);


  for (unsigned int i = 0; i < primGenerator->getParticles()->size(); i++) {
    PrimParticle secondary = primGenerator->getParticles()->at(i);

    int id_geant3 = secondary.getParticleType();
    G4ThreeVector helper = G4ThreeVector(secondary.getPx(), secondary.getPy(), secondary.getPz());
    if (rotation.first != 0)
      helper.rotate(rotation.first, rotation.second);

    G4double totalEnergy = secondary.getE() * CLHEP::GeV; // unit correct?
    G4double particleMass = secondary.getM() * CLHEP::GeV; // unit correct?

    G4ThreeVector momentumDirection = helper.unit();
    G4double kinEnergy = totalEnergy - particleMass;
    G4DynamicParticle* aParticle = new G4DynamicParticle(getParticleByID(id_geant3),
        momentumDirection, kinEnergy);
    G4Track* aSecondaryTrack = new G4Track(aParticle, beamData->vertexTime,
        vertexPosition);
    aSecondaryTrack->SetTouchableHandle(aTrack.GetTouchableHandle());
    aSecondaryTrack->SetParentID(aTrack.GetTrackID());
    aParticleChange.AddSecondary(aSecondaryTrack);

    // beamData output:
    particle.k[0] = 1;
    particle.k[1] = id_geant3;
    particle.k[2] = 0; // primary particle
    particle.k[3] = 0; // no daughter
    particle.k[4] = 0; // no daughter

    // unit for beamData: GeV
    particle.p[0] = helper.x();
    particle.p[1] = helper.y();
    particle.p[2] = helper.z();
    particle.p[3] = secondary.getE();
    particle.p[4] = secondary.getM();
    beamData->beamParticles.push_back(particle);
  }

  beamData->nBeamParticle = beamData->beamParticles.size();

  aParticleChange.ProposeTrackStatus(fStopAndKill);
  targetTracking->processCalled();

  T4SMessenger::getInstance()->printMessage(T4SVerboseMore, __LINE__, __FILE__,
      "T4PrimakoffProcess::PostStepDoIt: T4PrimakoffProcess was executed.");

  return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
}

T4PrimakoffProcess::T4PrimakoffProcess() : primGenerator(NULL)
{
  primGen::process_type process = primGen::PrimakoffCompton;
  primGenerator = new primGen(process);
  T4SPrimGen* t4PrimPGen = settingsFile->getStructManager()->getPrimGen();

  int icontrib = t4PrimPGen->born || (t4PrimPGen->rhoContrib<<1) ||
                 (t4PrimPGen->chiralLoops<<2) || (t4PrimPGen->polarizabilities<<3);

  primGenerator->setPrimComptonParams(t4PrimPGen->Z, t4PrimPGen->particle, 1,
                                      t4PrimPGen->alpha_1, t4PrimPGen->beta_1,
                                      t4PrimPGen->alpha_2, t4PrimPGen->beta_2,
                                      t4PrimPGen->s_min, t4PrimPGen->s_max,
                                      t4PrimPGen->Q2_max, t4PrimPGen->Egamma_min,
                                      t4PrimPGen->pbeam_min, t4PrimPGen->pbeam_max,
                                      t4PrimPGen->born, t4PrimPGen->radcorr, icontrib);

  primGenerator->setSeed((int) T4SettingsFile::getInstance()->getStartSeed() + 1);
  primGenerator->init();
}

T4PrimakoffProcess::~T4PrimakoffProcess()
{
  if(primGenerator!=NULL)
    delete primGenerator;
}

#endif
