#include "T4EcalCalib.hh"

static T4EcalCalib* t4EcalCalib = new T4EcalCalib();

T4EcalCalib::T4EcalCalib(void)
{
  pluginName = "EcalCalib";
  registerPlugin();

  id = 0;
}

void T4EcalCalib::initialize(void)
{
  id = settingsFile->getStructManager()->getBeam()->beamParticle;
  particleGun->SetParticleDefinition(getParticleByID(id));
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
}

void T4EcalCalib::generateEvent(G4Event* event)
{
  T4SEcalCalib* ecalCalib = settingsFile->getStructManager()->getEcalCalib();
  G4double energy = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),ecalCalib->energyMin, ecalCalib->energyMax);
  particleGun->SetParticleEnergy(energy);

  G4double x, y, z;
  x = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),ecalCalib->positionXMin,ecalCalib->positionXMax);
  y = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),ecalCalib->positionYMin, ecalCalib->positionYMax);
  z = ecalCalib->positionZ;
  G4ThreeVector position = G4ThreeVector(x, y, z);
  particleGun->SetParticlePosition(position);

  G4ThreeVector momentum = G4ThreeVector(0, 0, 1);
  particleGun->SetParticleMomentumDirection(momentum);

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  G4double kinEnergy = energy - getParticleMass(id);
  momentum.setMag(energy / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = (energy + getParticleMass(id)) / CLHEP::GeV;
  particle.p[4] = getParticleMass(id) / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(), position.z());

  particleGun->GeneratePrimaryVertex(event);
}
