#include "T4ProcessBackend.hh"
#include "T4PhysicsList.hh"

G4ParticleDefinition* T4ProcessBackEnd::getParticleByID(G4int id)
{
  T4PhysicsList* t4PhysicsList = (T4PhysicsList*) G4RunManager::GetRunManager()
      ->GetUserPhysicsList();
  return t4PhysicsList->getParticleByID(id);
}
