#include "T4PhotonBeam.hh"

static T4PhotonBeam* t4PhotonBeam = new T4PhotonBeam(true);
static T4PhotonBeam* t4PhotonCone = new T4PhotonBeam(false);

T4PhotonBeam::T4PhotonBeam(bool _generateBeam)
{
  generateBeam = _generateBeam;

  if (generateBeam)
    pluginName = "PhotonBeam";
  else
    pluginName = "PhotonCone";

  registerPlugin();

  id = 0;
  energy = 0;
}

void T4PhotonBeam::initialize(void)
{
  id = 22;
  energy = 2.0 * CLHEP::GeV;
  particleGun->SetParticleDefinition(getParticleByID(id));
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);
}

void T4PhotonBeam::generateEvent(G4Event* event)
{
  G4double x, y, z;
  G4ThreeVector momentum;

  if (generateBeam) { // normal
    z = -500.0 * CLHEP::cm;
    momentum = G4ThreeVector(0, 0, 1);

//    G4double radius = 2.5 * CLHEP::cm;
    G4double radiusX = 135. * CLHEP::cm;
    G4double radiusY = 70. * CLHEP::cm;


    while (true) {
      x = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-radiusX, radiusX);
      y = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-radiusY, radiusY);

//      if ((x*x + y*y) <= radius*radius)
        break;
    }
  } else { //cone
    x = 0;
    y = 0;
    z = -190.0 * CLHEP::cm;
    G4double angle = 200 * CLHEP::mrad;

    G4double costheta = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),cos(angle),1.0);
    G4double sintheta = sin(cos(costheta));
    G4double phi = CLHEP::RandFlat::shoot(settingsFile->getRandom()->getTheEngine(),-M_PI,M_PI);
    G4double pz = 2.0*costheta;
    G4double px = 2.0*sintheta*cos(phi);
    G4double py = 2.0*sintheta*sin(phi);
    momentum = G4ThreeVector(px, py, pz);
  }

  particleGun->SetParticleEnergy(energy);

  G4ThreeVector position = G4ThreeVector(x, y, z);
  particleGun->SetParticlePosition(position);
  particleGun->SetParticleMomentumDirection(momentum);

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  momentum.setMag(energy / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = energy / CLHEP::GeV;
  particle.p[4] = 0;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(),
      position.z());

  particleGun->GeneratePrimaryVertex(event);
}
