#include "T4TargetTracking.hh"

T4TargetTracking* T4TargetTracking::targetTracking = NULL;

T4TargetTracking* T4TargetTracking::getInstance(void)
{
  if (targetTracking == NULL)
    targetTracking = new T4TargetTracking();
  return targetTracking;
}

T4TargetTracking::T4TargetTracking(void)
{
  callEventGenNow = false;
  settingsFile = T4SettingsFile::getInstance();

  beamParticleId = settingsFile->getStructManager()->getBeam()->beamParticle;
  isSteppingActive = false;
  reset();

  noSecondary = settingsFile->getStructManager()->getGeneral()->noSecondaries;
}

void T4TargetTracking::reset(void)
{
  usedProcess = false;
  callEventGenNow = false;
  coveredDistance = 0;
}

void T4TargetTracking::UserSteppingAction(const G4Step* aStep)
{
  // This block kills all secondaries.
  // Warning: If this option is enabled, the energy conservation will be violated!
  if (noSecondary) {
    if (aStep->GetTrack()->GetParentID() > 1 || aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding() == 11)
//    if (aStep->GetTrack()->GetParentID() > 0) // harder cut, only incoming beam particle now!
      aStep->GetTrack()->SetTrackStatus(fStopAndKill);
  }

  // activate process in target
  if (!usedProcess && isSteppingActive) {
    if (aStep->GetTrack()->GetTrackID() == 1) {
      if (targetBackend->isVolumeInTarget(aStep->GetPreStepPoint()->GetPhysicalVolume())) {
        coveredDistance += aStep->GetStepLength();
        if (coveredDistance > finalDistance) {
          callEventGenNow = true;
          usedProcess = true;
        }
      }
    }
  }
}

void T4TargetTracking::activateTargetStepping(void)
{
  targetBackend = T4ActiveTarget::getInstance()->getTarget();
  if (targetBackend == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4TargetTracking::activateTargetStepping: The 'Use Target Extrap.' option is enabled but no target found. This makes no sense!");
  } else {
    isSteppingActive = true;
  }
}
