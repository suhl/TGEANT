#include "T4BeamOnly.hh"

static T4BeamOnly* t4BeamOnly = new T4BeamOnly();

T4BeamOnly::T4BeamOnly(void)
{
  pluginName = "BeamOnly";
  registerPlugin();

  beamFile = NULL;
  useBeamFile = false;
  id = 0;
  energy = 0;
}

void T4BeamOnly::initialize(void)
{
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();
  id = beam->beamParticle;
  particleGun->SetParticleDefinition(getParticleByID(id));
  energy = beam->beamEnergy;
  G4double kinEnergy = energy - getParticleMass(id);
  if (kinEnergy <= 0)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
        "T4Geant::initialize: Total energy of particle is smaller than its mass!");

  particleGun->SetParticleEnergy(kinEnergy);
  particleGun->SetNumberOfParticles(1);
  particleGun->SetParticleTime(0);

  useBeamFile = beam->useBeamfile;
  if (useBeamFile) {
    beamFile = T4BeamFileList::getInstance()->getBeamFile(beam->beamFileBackend);
    beamFileType = settingsFile->getBeamFileType(beam->beamFileType);
    beamFile->loadFile(settingsFile->getStructManager()->getExternal()->beamFile);

    if (beam->usePileUp)
      pileUp = new T4PileUp(beam->beamFlux, beam->timeGate, beam->additionalPileUpFlux);
    extrapolate = new T4Extrapolate(getParticleByID(beam->beamParticle));
  }
}

void T4BeamOnly::generateEvent(G4Event* event)
{
  G4ThreeVector position, momentum;
  if (useBeamFile) {
    T4BeamFileParticle beamFileParticle;
    beamFileParticle = beamFile->getNextEntry(beamFileType);
    extrapolate->extrap(beamFileParticle);
    position = G4ThreeVector(beamFileParticle.posX, beamFileParticle.posY, beamFileParticle.posZ);
    momentum = beamFileParticle.momentumDirection;
    energy = beamFileParticle.energy;
    particleGun->SetParticleEnergy(energy);
  } else {
    position = G4ThreeVector(0, 0, settingsFile->getStructManager()->getBeam()->beamZStart);
    momentum = G4ThreeVector(0, 0, 1);
  }
  particleGun->SetParticlePosition(position);
  particleGun->SetParticleMomentumDirection(momentum);

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  T4BeamParticle particle;
  particle.k[0] = 1; // undecayed final state
  particle.k[1] = id;
  particle.k[2] = 0; // primary particle
  particle.k[3] = 0; // no daughter
  particle.k[4] = 0; // no daughter

  G4double kinEnergy = energy - getParticleMass(id);
  momentum.setMag(kinEnergy / CLHEP::GeV);
  particle.p[0] = momentum.x();
  particle.p[1] = momentum.y();
  particle.p[2] = momentum.z();
  particle.p[3] = energy / CLHEP::GeV;
  particle.p[4] = getParticleMass(id) / CLHEP::GeV;

  beamData->beamParticles.push_back(particle);

  beamData->nBeamParticle = 1;
  setThreeVector<double>(beamData->vertexPosition, position.x(), position.y(),
      position.z());

  particleGun->GeneratePrimaryVertex(event);

  // The pile up code must stay below the code for the "real" primary particle
  T4SBeam* beam = settingsFile->getStructManager()->getBeam();
  if (beam->useBeamfile && beam->usePileUp)
    generatePileUp(event);
}
