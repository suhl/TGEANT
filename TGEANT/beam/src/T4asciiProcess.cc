// Copyright [2016] Misha Mikhasenko

#include "T4asciiProcess.hh"
#include "G4RunManager.hh"

T4asciiProcess::T4asciiProcess() {
  /* Open File */
  string filename = settingsFile->getStructManager()->getExternal()->localGeneratorFile;
  replacePathEnvTGEANT(filename);

  fin.open(filename.c_str(), std::ifstream::in);
  if (!fin) {
    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Error: Cannot open in file! You gen dummy generator.");
  }
  T4SMessenger::getInstance()->printMessage(T4SVerboseMore, __LINE__, __FILE__,
      "Acsii file was opened.");
}

/* Close file File */
T4asciiProcess::~T4asciiProcess() {
  fin.close();
}

/* fill vector of particles from code */
void T4asciiProcess::nextEvent() {
  particles.clear();
  while (fin) {
    std::string line;
    std::getline(fin, line);
    if (line.find("-----") != std::string::npos) {
      break;
    }
    std::stringstream ssline(line);
    int pdg_id;     ssline >> pdg_id;
    double momentum[3]; for(int i=0; i < 3; i++) ssline >> momentum[i];
    if (ssline.fail()) {
      T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4asciiProcess::nextEnent(): Problems with infile while getting particle.");
      break;
    }
    vector<double> p(momentum, momentum + 3);
    particles.push_back(std::make_pair(pdg_id,p));
  }

  if (!fin) {
    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4asciiProcess::nextEvent. Warning: End of file!");
  }
}

#define POW2(a) (a)*(a)

/* Suggest changes: kill beam particle, 
   replace to final state particles */
G4VParticleChange* T4asciiProcess::PostStepDoIt( const G4Track& aTrack,
                                                 const G4Step& aStep ) {
  aParticleChange.Initialize(aTrack);

  G4ThreeVector vertexPosition = aStep.GetPostStepPoint()->GetPosition();
  G4double vertexTime = aStep.GetPostStepPoint()->GetGlobalTime();

  G4ThreeVector momentum = aStep.GetPostStepPoint()->GetMomentum();

  T4BeamData* beamData = T4EventManager::getInstance()->getBeamData();
  beamData->setDefault();
  beamData->nBeamParticle = 0;

  nextEvent(); /* fill particles */

  aParticleChange.SetNumberOfSecondaries(particles.size());

  G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
  for ( uint i = 0; i < particles.size(); i++ ) {
    G4ParticleDefinition *pDef = particleTable->FindParticle(particles[i].first);
    // wront id -> replace it to geantino
    if (!pDef) {
      T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
         "Warning: particle for given PDGid not found, replaced to geantino!");
      pDef = particleTable->FindParticle("geantino");
    }
    // fill data for beamData
    const double *p = particles[i].second.data();
    std::cout << "Add " << pDef->GetParticleName() << "(" << particles[i].first <<  "), ("
              << p[0] << "," << p[1] << "," << p[2] << ")" << std::endl;
    T4BeamParticle newParticle;
    memcpy(newParticle.p, p, 3*sizeof(double));
    newParticle.p[4] = pDef->GetPDGMass();  // m();
    newParticle.p[3] += sqrt(POW2(p[0])+POW2(p[1])+POW2(p[2])+POW2(newParticle.p[4]));  // e();
    newParticle.k[0] = 0.0;  // status();
    newParticle.k[1] = particles[i].first;  // id();
    newParticle.k[2] = 0.0;  // mother1();
    newParticle.k[3] = 0.0;  // daughter1();
    newParticle.k[4] = 0.0;  // daughter2();
    G4DynamicParticle* aParticle = new G4DynamicParticle(pDef, G4ThreeVector(p[0]*CLHEP::GeV,
                                                                             p[1]*CLHEP::GeV,
                                                                             p[2]*CLHEP::GeV));
    G4Track* aSecondaryTrack = new G4Track ( aParticle, vertexTime, vertexPosition );

    aSecondaryTrack->SetTouchableHandle(aTrack.GetTouchableHandle());
    aSecondaryTrack->SetParentID(aTrack.GetTrackID());
    aParticleChange.AddSecondary(aSecondaryTrack);

    beamData->beamParticles.push_back(newParticle);
    beamData->nBeamParticle++;
  }

  beamData->generator = eventGenId;
  setThreeVector<double>(beamData->vertexPosition, vertexPosition.x(), vertexPosition.y(), vertexPosition.z());
  beamData->vertexTime = vertexTime;

  aParticleChange.ProposeTrackStatus(fStopAndKill);
  targetTracking->processCalled();

  T4SMessenger::getInstance()->printMessage(T4SVerboseMore, __LINE__, __FILE__,
                                            "T4asciiProcess was executed.");

  return G4VDiscreteProcess::PostStepDoIt ( aTrack, aStep );
}
