#include "T4InternalGenerator.hh"

static T4Pythia* t4Pythia = new T4Pythia();
static T4HEPGen* t4HEPGen = new T4HEPGen();
static T4Primakoff* t4Primakoff = new T4Primakoff();
static T4Pythia8* t4Pythia8 = new T4Pythia8();
static T4ascii* t4ascii = new T4ascii();

T4Pythia::T4Pythia(void)
{
  eventGenId = 1;

  pluginName = "PYTHIA";
  registerPlugin();
}

T4Pythia8::T4Pythia8(void)
{
  eventGenId = 42;

  pluginName = "PYTHIA8";
  registerPlugin();
}

T4HEPGen::T4HEPGen(void)
{
  eventGenId = 3;

  pluginName = "HEPGEN";
  registerPlugin();
}

T4Primakoff::T4Primakoff(void)
{
  eventGenId = 6;

  pluginName = "Primakoff";
  registerPlugin();
}

T4ascii::T4ascii(void)
{
  eventGenId = 8;

  pluginName = "ascii";
  registerPlugin();
}

T4ProcessBackEnd* T4Pythia::getEventGeneratorProcess(void)
{
#ifdef USE_PYTHIA
  if (process == NULL) {
    process = new T4PythiaProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
#endif

  // not activated:
  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4Pythia: This version of TGEANT does not support Pythia6! Choose another SettingsFile or enable PYTHIA6.");
  return 0;
}

T4ProcessBackEnd* T4Pythia8::getEventGeneratorProcess(void)
{
#ifdef USE_PYTHIA8
  if (process == NULL) {
    process = new T4Pythia8Process();
    process->setEventGenId(eventGenId);
  }
  return process;
#endif

  // not activated:
  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4Pythia8: This version of TGEANT does not support Pythia8! Choose another SettingsFile or enable PYTHIA8.");
  return 0;
}


T4ProcessBackEnd* T4HEPGen::getEventGeneratorProcess(void)
{
#ifdef USE_HEPGEN
  if (process == NULL) {
    process = new T4HepGenProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
#endif

  // not activated:
  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4HEPGen: This version of TGEANT does not support HEPGen++! Choose another SettingsFile or enable HEPGen++.");
  return 0;
}

T4ProcessBackEnd* T4Primakoff::getEventGeneratorProcess(void)
{
#ifdef USE_PRIMAKOFF
  if (process == NULL) {
    process = new T4PrimakoffProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
#endif

  // not activated:
  T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
      "T4Primakoff: This version of TGEANT does not support the Primakoff event generator! Choose another SettingsFile or enable PRIMAKOFF.");
  return 0;
}

T4ProcessBackEnd* T4ascii::getEventGeneratorProcess(void)
{
  if (process == NULL) {
    process = new T4asciiProcess();
    process->setEventGenId(eventGenId);
  }
  return process;
}
