#include "T4PrimaryGenerator.hh"

T4PrimaryGenerator::T4PrimaryGenerator(void)
{
  particleGun = new G4ParticleGun();
  settingsFile = T4SettingsFile::getInstance();

  string beamPlugin = settingsFile->getStructManager()->getBeam()->beamPlugin;

  if (settingsFile->geometryModeActivated())
    beamPlugin = "VisualizationMode";

  map<string, T4BeamBackEnd*>* beamPluginMap = &T4BeamPluginList::getInstance()->beamPlugins;

  // check if plugin exists
  if (beamPluginMap->find(beamPlugin) == beamPluginMap->end())
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
          "T4PrimaryGenerator: Name of beam plugin '" + beamPlugin + "' not found!");

  backEnd = (*beamPluginMap)[beamPlugin];
  backEnd->setParticleGun(particleGun);

  T4TargetTracking* targetTracking = T4TargetTracking::getInstance();
  G4RunManager* g4RunManager = G4RunManager::GetRunManager();
  g4RunManager->SetUserAction(targetTracking);
  if (settingsFile->getStructManager()->getBeam()->useTargetExtrap)
    targetTracking->activateTargetStepping();

  backEnd->initialize();
}

T4PrimaryGenerator::~T4PrimaryGenerator(void)
{
  T4BeamPluginList::resetInstance();
  delete particleGun;
  T4SMessenger::getReference() << "T4PrimaryGenerator, ";
}

void T4PrimaryGenerator::GeneratePrimaries(G4Event* event)
{
  if (settingsFile->getStructManager()->getGeneral()->usePerformanceMonitor) {
    T4PerformanceMonitor::getInstance()->stopLoadTimer();
    T4PerformanceMonitor::getInstance()->eventTimer(
        T4PerformanceMonitor::start);
  }

  backEnd->generateEvent(event);
}
