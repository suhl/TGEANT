#include "T4SettingsFile.hh"
#include "T4EventManager.hh"
#include "T4SignalManager.hh"
#include "T4Messenger.hh"
#include "T4SMessenger.hh"
#include "T4PerformanceMonitor.hh"

#include "T4RunManager.hh"
#include "T4PhysicsList.hh"
#include "T4WorldConstruction.hh"
#include "T4PrimaryGenerator.hh"

#include "G4UImanager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

int main(int argc, char** argv)
{
  //make sure we have this first, so the signals get caught correctly
  T4SignalManager* mySigMan = T4SignalManager::getInstance();
  printStartTGEANT();
  T4SettingsFile* settingsFile = T4SettingsFile::getInstance();
  T4SMessenger* messenger = T4SMessenger::getInstance();

  switch (argc) {
    case 1: // local
      messenger->printMessage(T4SNotice, __LINE__, __FILE__,
          "Using TGEANT with the local settings file: $TGEANT/resources/settings.xml");
      settingsFile->loadFile();
      break;
    case 2: // extern settings file
      settingsFile->loadFile(std::string(argv[1]));
      break;
    default:
      messenger->printMessage(T4SNotice, __LINE__, __FILE__,
          "Usage: TGEANT (The settings file will be loaded from $TGEANT/resources/settings.xml)");
      messenger->printMessage(T4SNotice, __LINE__, __FILE__,
          "Usage: TGEANT [/path/to/settings.xml or .conf]");
      messenger->printMessage(T4SFatalError, __LINE__,
      __FILE__, "Exit.");
      break;
  }

  if (settingsFile->geometryModeActivated())
    messenger->printMessage(T4SNotice, __LINE__, __FILE__,
        "GeometryMode enabled. Generating GDML geometry and/or detectors.dat for current setup. Beam gun, event generator and event output (.tgeant and .root) disabled.");

  // initialize
  T4PerformanceMonitor* performanceMonitor =
      T4PerformanceMonitor::getInstance();

  T4PhysicsList* physicsList = new T4PhysicsList(
      settingsFile->getPhysicsList());
  messenger->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4PhysicsList loaded. Initialize T4EventManager...");

  T4RunManager* runManager = new T4RunManager(physicsList);
  runManager->setEventManager(T4EventManager::getInstance());
  runManager->setWorldGeometry(new T4WorldConstruction());
  runManager->setBeamGun(new T4PrimaryGenerator());
  runManager->initialize();

  // visualization
  if (settingsFile->getStructManager()->getGeneral()->useVisualization) {
    messenger->printMessage(T4SNotice, __LINE__, __FILE__,
        "Visualization enabled. This may take some time for a larger setup...");

    G4VisManager* visManager = new G4VisExecutive("quiet");
    visManager->SetVerboseLevel(0);
    visManager->Initialize();
    G4UImanager* uiManager = G4UImanager::GetUIpointer();
    G4UIExecutive* ui = new G4UIExecutive(argc, argv);
    uiManager->ApplyCommand(
        "/control/execute "
            + settingsFile->getStructManager()->getExternal()
                ->visualizationMacro);
    ui->SessionStart();
    delete ui;
  } else {
    messenger->printMessage(T4SNotice, __LINE__, __FILE__,
        "Visualization disabled. Run started...");
    runManager->startRun(
        settingsFile->getStructManager()->getBeam()->numParticles);
  }

  // end of run
  T4EventManager::getInstance()->endOfRunAction();
  // T4WorldConstruction, T4PhysicsList, T4PrimaryGenerator and T4EventManager
  // are owned and deleted by the T4RunManager, so they are not deleted here!
  delete runManager;

  delete settingsFile;
  delete performanceMonitor;
  printEndTGEANT();

  return 0;
}
