#include "T4DeathMagnetic.hh"

T4DeathMagnetic* T4DeathMagnetic::deathMagnetic = NULL;

void T4DeathMagnetic::registerMeAtGeant()
{
  if (isRegistered)
    return;

  G4FieldManager* fieldManager =
      G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fieldManager->SetDetectorField((G4MagneticField*) getInstance());
  fieldManager->CreateChordFinder((G4MagneticField*) getInstance());
  isRegistered = true;
}

T4DeathMagnetic* T4DeathMagnetic::getInstance(void)
{
  if (!deathMagnetic)
    deathMagnetic = new T4DeathMagnetic();

  return deathMagnetic;
}

void T4DeathMagnetic::GetFieldValue(const double Point[4], double* Bfield) const
{
  double tx, ty, tz;
  double x = 0.;
  double y = 0.;
  double z = 0.;

  // sum up all partial fieds for this point
  for (unsigned int i = 0; i < partialFields.size(); i++) {
    tx = ty = tz = 0.;
    partialFields.at(i)->getField(Point[0], Point[1], Point[2], tx, ty, tz);
    x += tx;
    y += ty;
    z += tz;
  }
  
  Bfield[0] = x * CLHEP::tesla;
  Bfield[1] = y * CLHEP::tesla;
  Bfield[2] = z * CLHEP::tesla;
}

T4DeathMagnetic::~T4DeathMagnetic(void)
{
  for (unsigned int i = 0; i < partialFields.size(); i++)
    delete partialFields.at(i);
  partialFields.clear();
}

// THESE FUNCTIONS ARE COMPLETELY UNCHANGED AND COPIED FROM CORAL!
// (except float to double)
double T4PartialField::fint2(double X[2], double A[4], double F[2][2]) 
{
  double t,u,g1,g2;
  double x,y;
  
  x = X[0];
  y = X[1];

  t = (x-A[0])/(A[1]-A[0]);
  u = (y-A[2])/(A[3]-A[2]);

  g1 = (1-t)*F[0][0] + t*F[1][0];
  g2 = (1-t)*F[0][1] + t*F[1][1];

  return (1-u)*g1+u*g2;
}


double T4PartialField::fint3(double X[3], double A[6], double F[2][2][2])
{
  double t,u,v,g1,g2,f1,f2,h1,h2;
  double x,y,z;

  x = X[0];
  y = X[1];
  z = X[2];

  t = (x-A[0])/(A[1]-A[0]);
  u = (y-A[2])/(A[3]-A[2]);
  v = (z-A[4])/(A[5]-A[4]);

  g1 = (1-t)*F[0][0][0]+t*F[1][0][0];
  g2 = (1-t)*F[0][1][0]+t*F[1][1][0];
  h1 = (1-u)*g1+u*g2;

  f1 = (1-t)*F[0][0][1]+t*F[1][0][1];
  f2 = (1-t)*F[0][1][1]+t*F[1][1][1];
  h2 = (1-u)*f1+u*f2;

  return (1-v)*h1+v*h2;
}
