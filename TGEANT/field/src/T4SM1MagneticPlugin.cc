#include "T4SM1MagneticPlugin.hh"

T4SM1MagneticPlugin::T4SM1MagneticPlugin(T4SMagnet* _sm1)
{
  sm1 = _sm1;
  loadFieldMap();
  setScale(_sm1->scaleFactor);
}

void T4SM1MagneticPlugin::loadFieldMap(void)
{
  std::ifstream file;
  std::string filename = sm1->fieldmapPath;

  file.open(filename.c_str(), std::ios::in);
  if (!file.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
        "T4SM1MagneticPlugin::loadFieldMap: File " + filename
            + " does not exist.");
    return;
  }

  double x, y, z;
  double bx, by, bz;
  double gap;

  file >> gap;

  for (int i = 0; i < 41; i++) {
    for (int j = 0; j < 41; j++) {
      for (int k = 0; k < 108; k++) {
        file >> x >> y >> z >> bx >> by >> bz;
        xSM1_[i] = x; // cm
        ySM1_[j] = y; // cm
        zSM1_[k] = z; // cm note that map offset = 350
        bxSM1_[i][j][k] = bx; // T
        bySM1_[i][j][k] = by; // T
        bzSM1_[i][j][k] = bz; // T
      }
    }
  }

  file.close();
}


void T4SM1MagneticPlugin::getField(double pos_x, double pos_y, double pos_z,
    double& field_x, double& field_y, double& field_z)
{
  // IN THIS FUNCTION:
  // ALL UNITS IN GLOBAL TGEANT SYSTEM!

  // go to magnet system
  pos_x -= sm1->general.position[0];
  pos_y -= sm1->general.position[1];
  pos_z -= sm1->general.position[2];

  // mm -> cm
  pos_x /= 10.;
  pos_y /= 10.;
  pos_z /= 10.;

  field_x = 0.;
  field_y = 0.;
  field_z = 0.;

  getFieldSM1(pos_x, pos_y, pos_z, field_x, field_y, field_z);
  field_x *= scale;
  field_y *= scale;
  field_z *= scale;
}

// THIS FUNCTION IS COMPLETELY UNCHANGED AND COPIED FROM CORAL!
// (except float to double)
void T4SM1MagneticPlugin::getFieldSM1(double pos_x, double pos_y, double pos_z, double& field_x,
    double& field_y, double& field_z)
{
  int ii,jj,kk,ip,jp,kp;
  float eps;

  double XFINT[3];
  double AFINT[6];
  double FFINT[2][2][2];

  if( fabs(pos_x) < 160 && fabs(pos_y) < 160 &&
      pos_z > -416 && pos_z < 440){

    ii = (int)floor(fabs(pos_x)/4.);
    jj = (int)floor(pos_y/8.+20);
    kk = (int)floor(pos_z/8.+52);

    //--- Interpolation on a cube (8 points)
    ip = ii + 1;
    jp = jj + 1;
    kp = kk + 1;

    XFINT[0] = fabs(pos_x);
    XFINT[1] = pos_y;
    XFINT[2] = pos_z;

    AFINT[0] = xSM1_[ii];
    AFINT[1] = xSM1_[ip];
    AFINT[2] = ySM1_[jj];
    AFINT[3] = ySM1_[jp];
    AFINT[4] = zSM1_[kk];
    AFINT[5] = zSM1_[kp];

    FFINT[0][0][0] = bxSM1_[ii][jj][kk];
    FFINT[1][0][0] = bxSM1_[ip][jj][kk];
    FFINT[0][1][0] = bxSM1_[ii][jp][kk];
    FFINT[1][1][0] = bxSM1_[ip][jp][kk];
    FFINT[0][0][1] = bxSM1_[ii][jj][kp];
    FFINT[1][0][1] = bxSM1_[ip][jj][kp];
    FFINT[0][1][1] = bxSM1_[ii][jp][kp];
    FFINT[1][1][1] = bxSM1_[ip][jp][kp];
    field_x = fint3( XFINT, AFINT, FFINT);
    FFINT[0][0][0] = bySM1_[ii][jj][kk];
    FFINT[1][0][0] = bySM1_[ip][jj][kk];
    FFINT[0][1][0] = bySM1_[ii][jp][kk];
    FFINT[1][1][0] = bySM1_[ip][jp][kk];
    FFINT[0][0][1] = bySM1_[ii][jj][kp];
    FFINT[1][0][1] = bySM1_[ip][jj][kp];
    FFINT[0][1][1] = bySM1_[ii][jp][kp];
    FFINT[1][1][1] = bySM1_[ip][jp][kp];
    field_y = fint3( XFINT, AFINT, FFINT);
    FFINT[0][0][0] = bzSM1_[ii][jj][kk];
    FFINT[1][0][0] = bzSM1_[ip][jj][kk];
    FFINT[0][1][0] = bzSM1_[ii][jp][kk];
    FFINT[1][1][0] = bzSM1_[ip][jp][kk];
    FFINT[0][0][1] = bzSM1_[ii][jj][kp];
    FFINT[1][0][1] = bzSM1_[ip][jj][kp];
    FFINT[0][1][1] = bzSM1_[ii][jp][kp];
    FFINT[1][1][1] = bzSM1_[ip][jp][kp];
    field_z = fint3( XFINT, AFINT, FFINT);
  } else {
    field_x = 0.;
    field_y = 0.;
    field_z = 0.;
  }
  //--  decreasing ----------
  /*
  if( pos_z < 0 ){
    eps = exp( -pos_z * pos_z/2500. );
    bmx = bmx * eps;
    bmy = bmy * eps;
    bmz = bmz * eps;
  }
  */
  //---------------------------

  //bmx = bmx * sign(pos_y) * sign(pos_x);
  //bmz = bmz * sign(pos_y);

  field_x = field_x  * sign(pos_x);
}
