#include "T4SM2MagneticPlugin.hh"

T4SM2MagneticPlugin::T4SM2MagneticPlugin(T4SMagnet* _sm2)
{
  sm2 = _sm2;
  loadFieldMap();
  setScale(_sm2->scaleFactor);
}

void T4SM2MagneticPlugin::loadFieldMap(void)
{
  std::ifstream file;
  std::string filename = sm2->fieldmapPath;

  file.open(filename.c_str(), std::ios::in);
  if (!file.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
        "T4SM2MagneticPlugin::loadFieldMap: File " + filename
            + " does not exist.");
    return;
  }

  file >> IFSMC1;
  for (G4int i = 0; i < 54; i++)
    file >> title[i];
  file >> FSMA01;
  file >> FSMA11;
  for (G4int i = 0; i < 1770; i++)
    file >> FSMAX1[i];
  for (G4int i = 0; i < 472; i++)
    file >> FSMBX1[i];
  for (G4int i = 0; i < 2360; i++)
    file >> FSMAY1[i];
  for (G4int i = 0; i < 300; i++)
    file >> FSMBY1[i];
  for (G4int i = 0; i < 3540; i++)
    file >> FSMAZ1[i];
  for (G4int i = 0; i < 84; i++)
    file >> FSMCX1[i];
  for (G4int i = 0; i < 126; i++)
    file >> FSMCY1[i];
  for (G4int i = 0; i < 168; i++)
    file >> FSMCZ1[i];
  for (G4int i = 0; i < 312; i++)
    file >> FSMDX1[i];
  for (G4int i = 0; i < 390; i++)
    file >> FSMDY1[i];
  for (G4int i = 0; i < 468; i++)
    file >> FSMDZ1[i];
  file.close();
}

void T4SM2MagneticPlugin::getField(double pos_x, double pos_y, double pos_z,
    double& field_x, double& field_y, double& field_z)
{
  // IN THIS FUNCTION:
  // ALL UNITS IN GLOBAL TGEANT SYSTEM!

  // go to magnet system
  pos_x -= sm2->general.position[0];
  pos_y -= sm2->general.position[1];
  pos_z -= sm2->general.position[2];

  // mm -> cm
  pos_x /= 10.;
  pos_y /= 10.;
  pos_z /= 10.;

  field_x = 0.;
  field_y = 0.;
  field_z = 0.;

  if (!extrapolateField(pos_x, pos_y, pos_z, field_x, field_y, field_z)) {
    field_x = 0.0;
    field_y = 0.0;
    field_z = 0.0;
  }
  field_x *= scale;
  field_y *= scale;
  field_z *= scale;
}

bool T4SM2MagneticPlugin::extrapolateField(double pos_x, double pos_y,
    double pos_z, double& field_x, double& field_y, double& field_z)
{
  // NOW WE CHANGE FROM TGEANT MAGNET SYSTEM
  // TO COMGEANT SYSTEM

  int IRET = 0;
  int IERR = 2;

  double dist;
  double pos2_x = 0, pos2_y = 0, pos2_z = 0;
  double field1_x, field1_y, field1_z, field2_x, field2_y, field2_z;

  // CORAL -> COMGeant
  double pos1_x = pos_z;
  double pos1_y = pos_x;
  double pos1_z = pos_y;

  if (!getFieldSM2(pos1_x, pos1_y, pos1_z, field1_x, field1_y, field1_z, IRET))
    return false;

  if (IRET == 2) {

    //---        Extrapolate

    pos1_x = 200.;
    pos1_y = 102.;
    pos1_z = 52.;

    if (fabs(pos_z) < pos1_x) {
      pos2_x = pos_z;
      while (IERR == 2) {
        pos1_y = pos1_y - 0.5;
        pos1_z = pos1_z - 0.5;
        pos2_y = ((fabs(pos_x) < pos1_y) ? fabs(pos_x) : pos1_y) * sign(pos_x);
        pos2_z = ((fabs(pos_y) < pos1_z) ? fabs(pos_y) : pos1_z) * sign(pos_y);

        if (!getFieldSM2(pos2_x, pos2_y, pos2_z, field2_x, field2_y, field2_z,
            IERR))
          return false;

      }
      dist = sqrt(
          (pos2_x - pos_z) * (pos2_x - pos_z)
              + (pos2_y - pos_x) * (pos2_y - pos_x)
              + (pos2_z - pos_y) * (pos2_z - pos_y));
      if (dist < 150.) {
        field1_x = field2_x * exp(-dist * 0.03);
        field1_y = field2_y * exp(-dist * 0.03);
        field1_z = field2_z * exp(-dist * 0.03);
      }
    }
  }

  field_x = field1_y;
  field_y = field1_z;
  field_z = field1_x;

  return true;
}


// THIS FUNCTION IS COMPLETELY UNCHANGED AND COPIED FROM CORAL!
// except comment in line 493 (unit of magnetic field: tesla)
bool T4SM2MagneticPlugin::getFieldSM2(double pos_x, double pos_y, double pos_z,
    double& field_x, double& field_y, double& field_z, int& IRET)
{
  // *** ALL INPUT/OUTPUT VARIABLES IN THE MAGNET COORDINATE SYSTEM
  // *** THE TRANSLATION TO EMC IS MADE IN THE CALLING SEQUENCE
  // *** INPUT/OUTPUT VARIABLE UNITS ARE METRES,GAUSS
  // *** INTERNAL UNITS ARE CMS,GAUSS
  // *** FOR A CALL ....   CALL MAGB(X,Y,Z,BX,BY,BZ)
  // *** NOTE THAT X (MAGNET) = Y (EMC)
  // *** NOTE THAT Y (MAGNET) = X (EMC)
  // *** NOTE THAT Z (MAGNET) = -Z (EMC)

  float BXX[3+1][3+1],BYY[3+1][3+1],BZZ[3+1][3+1],BXXX[3+1],BYYY[3+1],
    BZZZ[3+1];
  double Z2,Z3,Z4,Z5,Z6,Z7,Z8,Z9,Z10;

  float XX,YY,ZZ,ZP,FC,X,Y,Z,BX,BY,BZ,XS,YS,ZS,DELY,
    DX,DY,XR,YR,VX,VY,VZ,A,B;
  int IFLAG,IX,JX,LX,MX,JX1,MX1,NX1,NY1,IY,JY,LY,JZ;

  // ------------------------------------------------------------------

  IRET=0;

  //---   Move to the magnet reference frame

  XX = pos_y;
  YY = pos_x;
  ZP = pos_z;

  //---   Here the old code is plugged in

  // *** CHANGE Z FOR EMC/MAGNET DIFFERENCE

  ZZ=-ZP;

  // *** FIELD CURRENT SIGN

  FC=-1.0;
  X=fabs(XX);
  Y=fabs(YY);
  Z=fabs(ZZ);


  //     *** HIT THE MAGNET ??

  if( Y <= 200  &&
      ( X > 101.5   ||  ( Y < 120.0 && Z > 50.0 )   ||
  ( Y >= 120.0 && Z > 52.0 ) )    ) {
    IRET=2;
    field_x = 0;
    field_y = 0;
    field_z = 0;
    return true;
  }

  //     *** IN MAGNETIC FIELD REGION ??

  if( Y > 340.0  ||  Z > 69.0  ||  X > 144.0 ){
    IRET=3;
    field_x = 0;
    field_y = 0;
    field_z = 0;
    return true;
   }


  // *** PROTECT AGAINST UNDERFLOW (GENUINE ONES !!)

  if(Z < 1.0E-5) Z=0.0;

  IRET=0;
  field_x = 0.;
  field_y = 0.;
  field_z = 0.;


  //     *** COMPONENT SIGNS

  XS=sign(XX);
  ZS=sign(ZZ);

  //     *** IN MAIN BOX ??

  if( (X <= 101.5 || Y <= 200.)  &&
      (Z <= 50.0 || Y <= 200.0)  &&
      (Y < 222.0 && Z <= 52.0) ){
    //     *** MAIN FIELD - FIRST WORK OUT Y SHIFT AS FUNCTION OF X

    //     *** SET EXTRAPOLATION FLAG IF NECESSARY

    if(X > 96.0) IFLAG=1;

    //     *** LIMIT X IF NECESSARY

    if(X > 96.0) X=96.0;

    //     *** LIMIT Z IF NECESSARY

    if(Y > 120.0 && Z > 50.0){
      IFLAG=1;
      Z=50.0;
    }else{
      if(Y <= 120.0 && Z > 48.0){
  IFLAG=1;
  Z=48.0;
      }
    }
    DELY = FSMA01+FSMA11*(16.0-X/6.0)*(16.0-X/6.0);
    Y = -fabs(YY+DELY);

    //     *** Y COMPONENT SIGN

    YS = -sign(YY+DELY);

    //     *** WORK OUT X RANGE AND ARRAY NUMBER

    if( X <= 36. ){
      NX1=-1;
      DX=18.;
      XR=X;
    }else{
      if(X <= 72.){
  XR=X-36.0;
  NX1=(int)(XR/12.0);
  if(NX1 < 1) NX1=1;
  if(NX1 > 2) NX1=2;
  DX=12.0;
  XR=XR-(NX1-1)*12.0;
      }else{
  XR=X-54.0;
  NX1=(int)(XR/6.0);
  if(NX1 < 4) NX1=4;
  if(NX1 > 6) NX1=6;
  DX=6.0;
  XR=XR-(NX1-1)*6.0;
      }
    }

    //     *** WORK OUT Y RANGE AND ARRAY NUMBER

    if( Y <= -212. ){
      YR=Y+224.0;
      NY1=(int)(YR/6.0);
      if(NY1 < -1) NY1=-1;
      if(NY1 > 1) NY1=1;
      DY=6.0;
      YR=YR-(NY1-1)*6.0;
    }else{
      if(Y <= -80.){
  YR=Y+218.0;
  NY1=(int)(YR/3.0);
  if(NY1 < 3) NY1=3;
  if(NY1 > 45) NY1=45;
  DY=3.0;
  YR=YR-(NY1-1)*3.0;
      }else{
  if(Y <= -50.){
    YR=Y+356.0;
    NY1=(int)(YR/6.0);
    if(NY1 < 47) NY1=47;
    if(NY1 > 50) NY1=50;
    DY=6.0;
    YR=YR-(NY1-1)*6.0;
  }else{
    YR=Y+662.0;
    NY1=(int)(YR/12.0);
    if(NY1 < 52) NY1=52;
    if(NY1 > 55) NY1=55;
    DY=12.0;
    YR=YR-(NY1-1)*12.0;
  }
      }
    }

    //     *** FORM POWERS OF Z FOR LOCAL Z FITS

    Z2=Z*Z;
    Z3=Z2*Z;
    Z4=Z2*Z2;
    Z5=Z4*Z;
    Z6=Z5*Z;
    Z7=Z6*Z;
    Z8=Z7*Z;
    Z9=Z8*Z;
    Z10=Z9*Z;
    MX=NY1*30+NX1*3;
    MX1=NY1*8+2*NX1-12;
    LY=NY1+1;
    for( IY=0+1; IY<3+1; IY++){
      LY=LY+1;
      MX=MX+30;
      MX1=MX1+8;
      JX=MX;
      JX1=MX1;
      LX=NX1+1;
      for( IX=0+1; IX<3+1; IX++){
  LX=LX+1;
  JX=JX+3;
  JX1=JX1+2;
  JY=JX+JX/3;
  JZ=JX+JX;
  VX=FSMAX1[JX]*Z+FSMAX1[JX+1]*Z3+FSMAX1[JX+2]*Z5;
  VY=FSMAY1[JY]*Z+FSMAY1[JY+1]*Z3+FSMAY1[JY+2]*Z5+
    FSMAY1[JY+3]*Z7;
  BZZ[IY][IX]=FSMAZ1[JZ]+FSMAZ1[JZ+1]*Z2+FSMAZ1[JZ+2]*Z4+
    FSMAZ1[JZ+3]*Z6+FSMAZ1[JZ+4]*Z8+FSMAZ1[JZ+5]*Z10;

  //     *** EXTRA X TERMS

  if(LX > 6) VX=VX+FSMBX1[JX1]*Z7+FSMBX1[JX1+1]*Z9;

  //     *** EXTRA Y TERMS

  if(LY > 15 && LY < 46){
   VY=VY+FSMBY1[10*(LY-16)+LX-1]*Z9;
  }
  BXX[IY][IX]=VX;
  BYY[IY][IX]=VY;
      }
    }
  }else{
    //     *** FRINGE FIELD EVALUATION

    //     *** Y COMPONENT SIGN

    YS=-sign(YY);

    //     *** MAKE NECESSARY Y SHIFT

    if(YY > 0.0) Y=-YY;
    if(YY < 0.0) Y=YY+2.0*(FSMA01+FSMA11*(16.0-X/6.0)*(16.0-X/6.0));

    //     *** ADJUST Y TO NEAREST FRINGE FIELD MEASUREMENT IF NECESSARY

    if(Y >= -220.0) IRET=1;
    if(Y > -220.0) Y=-220.0;

    //     *** FORM LOCAL Z POLYNOMIALS

    Z2=Z*Z;
    Z3=Z2*Z;
    Z4=Z2*Z2;
    Z5=Z3*Z2;
    Z6=Z4*Z2;
    if(Y <= -250.0){
      NX1=(int)(X/24.0-2.0);
      if(NX1 < -1) NX1=-1;
      if(NX1 > 3) NX1=3;
      YR=Y+340.0;
      NY1=(int)(YR/18.0-2.0);
      if(NY1 < -1) NY1=-1;
      if(NY1 > 2) NY1=2;
      MX=14*NY1+2*NX1;
      for( IY=0+1; IY<3+1; IY++){
  MX=MX+14;
  JX=MX;
  for( IX=0+1; IX<3+1; IX++){
    JX=JX+2;
    JY=JX+JX/2;
    JZ=JX+JX;
    BXX[IY][IX]=FSMCX1[JX]*Z+FSMCX1[JX+1]*Z3;
    BYY[IY][IX]=FSMCY1[JY]*Z+FSMCY1[JY+1]*Z3+FSMCY1[JY+2]*Z5;
    BZZ[IY][IX]=FSMCZ1[JZ]+FSMCZ1[JZ+1]*Z2+
      FSMCZ1[JZ+2]*Z4+FSMCZ1[JZ+3]*Z6;
  }
      }
      DX=24.0;
      DY=18.0;
      YR=YR-(NY1+1)*18.0;
      XR=X-(NX1+1)*24.0;
    }else{
      NX1=(int)(X/12.0-2.0);
      if(NX1 < -1) NX1=-1;
      if(NX1 > 9) NX1=9;
      DX=12.0;
      YR=Y+250.0;
      NY1=(int)(YR/6.0-2.0);
      if(NY1 < -1) NY1=-1;
      if(NY1 > 2) NY1=2;
      DY=6.0;
      Z7=Z5*Z2;
      Z8=Z4*Z4;
      Z9=Z7*Z2;
      Z10=Z9*Z;
      MX=NY1*52+NX1*4;
      for( IY=0+1; IY<3+1; IY++){
  MX=MX+52;
  JX=MX;
  for( IX=0+1; IX<3+1; IX++){
    JX=JX+4;
    JY=JX+JX/4;
    JZ=JX+JX/2;
    BXX[IY][IX]=FSMDX1[JX]*Z+FSMDX1[JX+1]*Z3+
      FSMDX1[JX+2]*Z5+FSMDX1[JX+3]*Z7;
    BYY[IY][IX]=FSMDY1[JY]*Z+FSMDY1[JY+1]*Z3+
      FSMDY1[JY+2]*Z5+FSMDY1[ JY+3]*Z7+FSMDY1[JY+4]*Z9;
    BZZ[IY][IX]=FSMDZ1[JZ]+FSMDZ1[JZ+1]*Z2+
      FSMDZ1[JZ+2]*Z4+FSMDZ1[JZ+3]*Z6+FSMDZ1[JZ+4]*Z8+
      FSMDZ1[JZ+5]*Z10;
  }
      }
      YR=YR-(NY1+1)*6.0;
      XR=X-(NX1+1)*12.0;

      //     *** DO Y INTERPOLATION

    }
  }

  YR=YR/DY;
  for(IX=0+1;IX<3+1;IX++){   //IX=1,3
    A=(BXX[2+1][IX]+BXX[0+1][IX]-2.0*BXX[1+1][IX])/2.0;
    B=BXX[1+1][IX]-BXX[0+1][IX]-A;
    BXXX[IX]=(A*YR+B)*YR+BXX[0+1][IX];
    A=(BYY[2+1][IX]+BYY[0+1][IX]-2.0*BYY[1+1][IX])/2.0;
    B=BYY[1+1][IX]-BYY[0+1][IX]-A;
    BYYY[IX]=(A*YR+B)*YR+BYY[0+1][IX];
    A=(BZZ[2+1][IX]+BZZ[0+1][IX]-2.0*BZZ[1+1][IX])/2.0;
    B=BZZ[1+1][IX]-BZZ[0+1][IX]-A;
    BZZZ[IX]=(A*YR+B)*YR+BZZ[0+1][IX];
  }

  // *** DO X INTERPOLATION

  XR=XR/DX;
  A=(BXXX[2+1]+BXXX[0+1]-2.0*BXXX[1+1])/2.0;
  B=BXXX[1+1]-BXXX[0+1]-A;
  VX=(A*XR+B)*XR+BXXX[0+1];
  A=(BYYY[2+1]+BYYY[0+1]-2.0*BYYY[1+1])/2.0;
  B=BYYY[1+1]-BYYY[0+1]-A;
  VY=(A*XR+B)*XR+BYYY[0+1];
  A=(BZZZ[2+1]+BZZZ[0+1]-2.0*BZZZ[1+1])/2.0;
  B=BZZZ[1+1]-BZZZ[0+1]-A;
  VZ=(A*XR+B)*XR+BZZZ[0+1];
  // ***
  // *** BX,BY,BZ IN EMC COORDINATE SYSTEM
  // ***
  BX=VX*XS*ZS*FC;
  BY=VY*YS*ZS*FC;
  BZ=-VZ*FC;

  //---     Move to the MRS (master reference frame)
  //---     Convert to tesla

  field_x = -BY*0.0001;
  field_y = -BX*0.0001;
  field_z = -BZ*0.0001;

  return true;
}
