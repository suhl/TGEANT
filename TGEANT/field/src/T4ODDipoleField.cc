#include <T4ODDipoleField.hh>

T4ODDipoleField::T4ODDipoleField(T4SMagnet* magnet)
{
  position[0] = magnet->general.position[0];
  position[1] = magnet->general.position[1];
  position[2] = magnet->general.position[2];

  loadFieldMap(magnet->fieldmapPath);
  setScale(magnet->scaleFactor);
}

T4ODDipoleField::T4ODDipoleField(double _position[3])
{
  position[0] = _position[0];
  position[1] = _position[1];
  position[2] = _position[2];
  std::string pathToTGEANT = getenv("TGEANT");
  loadFieldMap(pathToTGEANT + "/resources/fieldmaps/OD_dipole.fieldmap");
}

void T4ODDipoleField::loadFieldMap(string filePath)
{
  ifstream file;
  file.open(filePath.c_str(), std::ios::in);
  if (!file.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4ODDipoleField::loadFieldMap: File " + filePath
            + " does not exist.");
    return;
  }

  string line;
  getline(file, line);
  if (line != "Map for the dipole configuration of the Oxford magnet.")
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4ODDipoleField::loadFieldMap: The first line of the file " + filePath
            + " does not match the expected sentence.");
  getline(file, line);
  getline(file, line);

  for (int i = 0; i < 27; i++) {
    for (int j = 0; j < 27; j++) {
      for (int k = 0; k < 62; k++) {
        file >> xoxd_[i] >> yoxd_[j] >> zoxd_[k];
        file >> bxoxd_[i][j][k] >> byoxd_[i][j][k] >> bzoxd_[i][j][k];
      }
    }
  }

  file.close();
}


void T4ODDipoleField::getField(double pos_x, double pos_y, double pos_z,
    double& field_x, double& field_y, double& field_z)
{
  // IN THIS FUNCTION:
  // ALL UNITS IN GLOBAL TGEANT SYSTEM!

  // go to magnet system
  pos_x -= position[0];
  pos_y -= position[1];
  pos_z -= position[2];

  // mm -> cm
  pos_x /= 10.;
  pos_y /= 10.;
  pos_z /= 10.;

  field_x = 0.;
  field_y = 0.;
  field_z = 0.;

  getFieldDipOx(pos_x, pos_y, pos_z, field_x, field_y, field_z);
  field_x *= scale;
  field_y *= scale;
  field_z *= scale;
}

// THIS FUNCTION IS COMPLETELY UNCHANGED AND COPIED FROM CORAL!
// (except the T4SMessenger stuff, and float to double)
void T4ODDipoleField::getFieldDipOx(double pos_x, double pos_y,
    double pos_z, double& bmx, double& bmy, double& bmz)
{
  int NX_OXD = 27;
  int NY_OXD = 27;
  int NZ_OXD = 62;

  if (fabs(pos_x) < xoxd_[NX_OXD - 1] && fabs(pos_y) < yoxd_[NY_OXD - 1]
      && fabs(pos_z) < zoxd_[NZ_OXD - 1]) {

    int ii = 0;
    while (fabs(pos_x) >= xoxd_[ii] && ii < NX_OXD)
      ii++;
    ii = ii - 1;

    int jj = 0;
    while (fabs(pos_y) >= yoxd_[jj] && jj < NY_OXD)
      jj++;
    jj = jj - 1;

    int kk = 0;
    while (fabs(pos_z) >= zoxd_[kk] && kk < NZ_OXD)
      kk++;
    kk = kk - 1;

    if (ii < 0 || jj < 0 || kk < 0) {
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
      __FILE__,
          "T4ODDipoleField::getFieldDipOx: This should never happen.");
    }

    //--- Interpolation on a cube (8 points)
    int ip = ii + 1;
    int jp = jj + 1;
    int kp = kk + 1;

    double XFINT[3];
    XFINT[0] = fabs(pos_x);
    XFINT[1] = fabs(pos_y);
    XFINT[2] = fabs(pos_z);
    //if( pos_z < 0 ) XFINT[2] = 0;
    //else            XFINT[2] = pos_z;

    double AFINT[6];
    AFINT[0] = xoxd_[ii];
    AFINT[1] = xoxd_[ip];
    AFINT[2] = yoxd_[jj];
    AFINT[3] = yoxd_[jp];
    AFINT[4] = zoxd_[kk];
    AFINT[5] = zoxd_[kp];

    double FFINT[2][2][2];
    FFINT[0][0][0] = bxoxd_[ii][jj][kk];
    FFINT[1][0][0] = bxoxd_[ip][jj][kk];
    FFINT[0][1][0] = bxoxd_[ii][jp][kk];
    FFINT[1][1][0] = bxoxd_[ip][jp][kk];
    FFINT[0][0][1] = bxoxd_[ii][jj][kp];
    FFINT[1][0][1] = bxoxd_[ip][jj][kp];
    FFINT[0][1][1] = bxoxd_[ii][jp][kp];
    FFINT[1][1][1] = bxoxd_[ip][jp][kp];
    bmx = fint3(XFINT, AFINT, FFINT);
    FFINT[0][0][0] = byoxd_[ii][jj][kk];
    FFINT[1][0][0] = byoxd_[ip][jj][kk];
    FFINT[0][1][0] = byoxd_[ii][jp][kk];
    FFINT[1][1][0] = byoxd_[ip][jp][kk];
    FFINT[0][0][1] = byoxd_[ii][jj][kp];
    FFINT[1][0][1] = byoxd_[ip][jj][kp];
    FFINT[0][1][1] = byoxd_[ii][jp][kp];
    FFINT[1][1][1] = byoxd_[ip][jp][kp];
    bmy = fint3(XFINT, AFINT, FFINT);
    FFINT[0][0][0] = bzoxd_[ii][jj][kk];
    FFINT[1][0][0] = bzoxd_[ip][jj][kk];
    FFINT[0][1][0] = bzoxd_[ii][jp][kk];
    FFINT[1][1][0] = bzoxd_[ip][jp][kk];
    FFINT[0][0][1] = bzoxd_[ii][jj][kp];
    FFINT[1][0][1] = bzoxd_[ip][jj][kp];
    FFINT[0][1][1] = bzoxd_[ii][jp][kp];
    FFINT[1][1][1] = bzoxd_[ip][jp][kp];
    bmz = fint3(XFINT, AFINT, FFINT);
  } else {
    bmx = 0.;
    bmy = 0.;
    bmz = 0.;
  }

  //---------------------------

  bmx *= (sign(pos_y) * sign(pos_x));
  bmz *= (sign(pos_y) * sign(pos_z));
}
