#include <T4TransvTargetField.hh>

T4TransvTargetField::T4TransvTargetField(T4SMagnet* magnet)
{
  position[0] = magnet->general.position[0];
  position[1] = magnet->general.position[1];
  position[2] = magnet->general.position[2];

  loadFieldMap(magnet->fieldmapPath);
  setScale(magnet->scaleFactor);
}

void T4TransvTargetField::loadFieldMap(string filePath)
{
  ifstream file;
  file.open(filePath.c_str(), std::ios::in);
  if (!file.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4TransvTargetField::loadFieldMap: File " + filePath
            + " does not exist.");
    return;
  }
  
  string line;
  int safetyCounter = 0;
  while(true) {
    getline(file, line);

    //     Z         R        Bz         Br       Btotal    Brelative    Homogeneity
    if (line.size()>11)
      if (line.substr(line.size()-12, 11) == "Homogeneity")
	break;
    safetyCounter++;
    if (safetyCounter > 20) {
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4TransvTargetField::loadFieldMap: Uncorrect file: " + filePath);
    }
  }
  
  double z,r,bz,br,tmp;
  for (int j = 0; j<22-1; j++) {
    for (int i = 0; i<62-1; i++) {
        file >> zsol_[i] >> rsol_[j] >> bzsol_[i][j] >> brsol_ [i][j]
        >> tmp >> tmp >> tmp;
    }
  }

  file.close();
}


void T4TransvTargetField::getField(double pos_x, double pos_y, double pos_z,
    double& field_x, double& field_y, double& field_z)
{
  // IN THIS FUNCTION:
  // ALL UNITS IN GLOBAL TGEANT SYSTEM!

  // go to magnet system
  pos_x -= position[0];
  pos_y -= position[1];
  pos_z -= position[2];

  // mm -> cm
  pos_x /= 10.;
  pos_y /= 10.;
  pos_z /= 10.;

  field_x = 0.;
  field_y = 0.;
  field_z = 0.;

  getFieldSol(pos_x, pos_y, pos_z, field_x, field_y, field_z);
  field_x *= scale;
  field_y *= scale;
  field_z *= scale;
}

// THIS FUNCTION IS COMPLETELY UNCHANGED AND COPIED FROM CORAL!
// (except the T4SMessenger stuff, and float to double)
void T4TransvTargetField::getFieldSol(double pos_x, double pos_y,
    double pos_z, double& bmx, double& bmy, double& bmz)
{
   int ii,jj,ip,jp;

  double XFINT[2];
  double AFINT[4];
  double GFINT[2][2];

  double r,bmr,eps;

  r = sqrt( pos_x*pos_x + pos_y*pos_y );
  
  if( fabs(pos_z) < 300. )
    ii = (int)floor(fabs(pos_z)/5.);
  else
    ii = 59;
    
  if( r < 100. )
    jj = (int)floor(r/5.);
  else
    jj = 19;
    
  ip = ii + 1;
  jp = jj + 1;

  XFINT[0] = fabs(pos_z);
  XFINT[1] = r;

  AFINT[0] = zsol_[ii];
  AFINT[1] = zsol_[ip];
  AFINT[2] = rsol_[jj];
  AFINT[3] = rsol_[jp];

  GFINT[0][0] = bzsol_[ii][jj];
  GFINT[1][0] = bzsol_[ip][jj];
  GFINT[0][1] = bzsol_[ii][jp];
  GFINT[1][1] = bzsol_[ip][jp];

  bmz = fint2( XFINT, AFINT, GFINT);
  GFINT[0][0] = brsol_[ii][jj];
  GFINT[1][0] = brsol_[ip][jj];
  GFINT[0][1] = brsol_[ii][jp];
  GFINT[1][1] = brsol_[ip][jp];

  bmr = fint2( XFINT, AFINT, GFINT);
  // ---- decreasing --------------

  if( fabs(pos_z) > 300. ){
    eps = exp( -(fabs(pos_z)-300.)/60 );
    bmr = bmr * eps;
    bmz = bmz * eps;
  }
  if( r > 100 ){
    eps = exp( -(r-100.)/30 );
    bmr = bmr * eps;
    bmz = bmz * eps;
  }
  // -------------

  if( r > 0 ) {
    bmx = pos_x/r * bmr * sign(pos_z);
    bmy = pos_y/r * bmr * sign(pos_z);
  }else{
    bmx = 0;
    bmy = 0;
  }
}
