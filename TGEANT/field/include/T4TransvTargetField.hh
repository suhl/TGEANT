#ifndef T4TRANSVTARGETFIELD_HH_
#define T4TRANSVTARGETFIELD_HH_

#include "T4DeathMagnetic.hh"
#include "T4SettingsFile.hh"

#include <fstream>

/**
 * \brief This class implements the polarized target field loaded from SOL_map_fabrice.dat.
 *
 * This includes the reading of the magnetic field map and returning the magnetic field value.
 * There is also the possibility to scale the field strength. The given value refers to the
 * origin of the magnet reference system.
 */
class T4TransvTargetField : public T4PartialField
{
  public:
    T4TransvTargetField(T4SMagnet*);
    virtual ~T4TransvTargetField(void) {}

    void getField(double _xin, double _yin, double _zin, double& _xout,
        double& _yout, double& _zout);

  private:
    double position[3];

    void loadFieldMap(string filePath);

    void getFieldSol(double pos_x, double pos_y, double pos_z,
        double& field_x, double& field_y, double& field_z);

    float zsol_[62];
    float rsol_[22];
    float bzsol_[62][22];
    float brsol_[62][22];
};

#endif /* T4TRANSVTARGETFIELD_HH_ */
