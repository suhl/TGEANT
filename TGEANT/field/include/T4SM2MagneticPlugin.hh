#ifndef T4SM2MAG_HH_
#define T4SM2MAG_HH_

#include "T4SettingsFile.hh"
#include "T4DeathMagnetic.hh"

#include <fstream>
#include <cmath>
#include <math.h>
/**
 * \brief This class implements the partial magnetic field for the sm2
 *
 *
 *
 * This includes the reading of the magnetic field map and returning the magnetic field value
 *
 */

class T4SM2MagneticPlugin : public T4PartialField
{
  public:
    T4SM2MagneticPlugin(T4SMagnet*);
    virtual ~T4SM2MagneticPlugin(void){};

    void getField(double pos_x, double pos_y, double pos_z,
        double& field_x, double& field_y, double& field_z);

  private:
    void loadFieldMap(void);

    /** \brief black-magic for adding ultra-fast signum-function to c++ - templated for your pleasure
     */
    template<typename T> inline int sign(T val)
      {return (T(0) < val) - (val < T(0));}


    bool extrapolateField(double pos_x, double pos_y, double pos_z,
        double& field_x, double& field_y, double& field_z);

    bool getFieldSM2(double pos_x, double pos_y, double pos_z,
        double& field_x, double& field_y, double& field_z, int& IRET);

    G4double IFSMC1;
    G4double FSMA01;
    G4double FSMA11;
    char title[54];
    G4double FSMAX1[1770];
    G4double FSMBX1[472];
    G4double FSMAY1[2360];
    G4double FSMBY1[300];
    G4double FSMAZ1[3540];
    G4double FSMCX1[84];
    G4double FSMCY1[126];
    G4double FSMCZ1[168];
    G4double FSMDX1[312];
    G4double FSMDY1[390];
    G4double FSMDZ1[468];

    T4SMagnet* sm2;
};

#endif
