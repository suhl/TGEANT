#ifndef T4ODDIPOLEFIELD_HH_
#define T4ODDIPOLEFIELD_HH_

#include "T4DeathMagnetic.hh"
#include "T4SettingsFile.hh"

#include <fstream>

/**
 * \brief This class implements the magnetic dipole field loaded from the OD_dipole.fieldmap.
 *
 * This includes the reading of the magnetic field map and returning the magnetic field value.
 * There is also the possibility to scale the field strength. The given value refers to the
 * origin of the magnet reference system.
 */
class T4ODDipoleField : public T4PartialField
{
  public:
    T4ODDipoleField(T4SMagnet*);
    T4ODDipoleField(double position[3]);
    virtual ~T4ODDipoleField(void) {}

    void getField(double _xin, double _yin, double _zin, double& _xout,
        double& _yout, double& _zout);

    void setFieldStrength(double fieldStrength) {scale *= fieldStrength / (-0.62300 * CLHEP::tesla);}

  private:
    double position[3];

    void loadFieldMap(string filePath);

    void getFieldDipOx(double pos_x, double pos_y, double pos_z,
        double& field_x, double& field_y, double& field_z);

    float xoxd_[27]; //!< X coordinate for the field of Oxford's dipole
    float yoxd_[27]; //!< Y coordinate for the field of Oxford's dipole
    float zoxd_[62]; //!< Z coordinate for the field of Oxford's dipole
    float bxoxd_[27][27][62];
    float byoxd_[27][27][62];
    float bzoxd_[27][27][62];
};

#endif /* T4ODDIPOLEFIELD_HH_ */
