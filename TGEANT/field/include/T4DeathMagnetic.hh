#ifndef T4DEATHMAG_HH_
#define T4DEATHMAG_HH_

#include <vector>

#include "G4ChordFinder.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4MagneticField.hh"

/**
 * \brief This class is the base-class for magnetic fields in the spectrometer. 
 * Given a complete MRS point - time and coordinates it returns the magnetic partial field it represents.
 * The complete logic of the field is handled in these classes
 * 
 */
class T4PartialField
{
  public:
    T4PartialField(double _scale = 1.0) {scale = _scale;}
    virtual ~T4PartialField(void) {}

    virtual void getField(double _xin, double _yin, double _zin,
        double& _xout, double& _yout, double& _zout) = 0;

    void changePolarization(void) {scale *= -1.;}
    void setScale(double _scale) {scale = _scale;}

  protected:
    /*! \fn float fint2(float X[2],float A[4],float F[2][2]);
    \brief That function uses linear interpolaition method
    to evaluate a function  f( \a z, \a r) of 2 variables which has been
    tabulated at nodes of an 2-dimensional rectangular grid.
    \param X   contains the coordinates \a z, \a r of the point at which 
	      the interpolation is to be performed.
    \param A   \e A[0] and \e A[1] contain tabulated values of \a z , 
		\e A[2] and \e A[3] contain tabulated values of \a r.
    \param F   contains values of the function f at the nodes of 
		the rectangular grid.
    */
    double fint2(double X[2], double A[4], double F[2][2]);
    
    /*! \fn float fint3(float X[3],float A[6],float F[2][2][2]);
       \brief That function uses linear interpolaition method
       to evaluate a function f( \a x, \a y, \a z) of 3 variables which has been
       tabulated at nodes of an 3-dimensional rectangular grid.
       \param X   contains the coordinates \a x, \a y, \a z of the point at which
                  the interpolation is to be performed.
       \param A   \e A[0] and \e A[1] contain tabulated values of \a x ,
                   \e A[2] and \e A[3] contain tabulated values of \a y,
       \e A[4] and \e A[5] contain tabulated values of \a z.
       \param F   contains values of the function f at the nodes of
                   the rectangular grid.
    */
    double fint3(double X[3], double A[6], double F[2][2][2]);

    template<typename T> inline int sign(T val)
      {return (T(0) < val) - (val < T(0));}

    double scale;
};

/**
 * \brief Next-Gen magnetic-field manager for TGEANT
 * 
 * This class manages the magnetic fields by sm1 and sm2 - and possibly other fields to be included into the simulation.
 * Every Detector generates its Partialfield and registers it at the DeathMagnetic-class that should only exist once.
 */
class T4DeathMagnetic : public G4MagneticField
{
  public:
    virtual ~T4DeathMagnetic(void);
    static T4DeathMagnetic* getInstance(void);

    void registerPartialField(T4PartialField* _fieldIn)
      {registerMeAtGeant(); partialFields.push_back(_fieldIn);};

    // this is important for the geant4-part
    void GetFieldValue(const double Point[4], double* Bfield) const;

    static void resetInstance(void) {delete deathMagnetic; deathMagnetic = NULL;}

  private:
    T4DeathMagnetic(void)
      {isRegistered = false;};

    void registerMeAtGeant(void);
    std::vector<T4PartialField*> partialFields;
    bool isRegistered;
    static T4DeathMagnetic* deathMagnetic;
};

#endif
