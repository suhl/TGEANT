#ifndef T4SM1MAG_HH_
#define T4SM1MAG_HH_

#include "T4DeathMagnetic.hh"
#include "T4SettingsFile.hh"

#include <fstream>

/**
 * \brief This class implements the partial magnetic field for the sm1
 *
 *
 *
 * This includes the reading of the magnetic field map and returning the magnetic field value
 *
 */

class T4SM1MagneticPlugin : public T4PartialField
{
  public:
    T4SM1MagneticPlugin(T4SMagnet*);
    virtual ~T4SM1MagneticPlugin(void){};

    void getField(double _xin, double _yin, double _zin, double& _xout,
        double& _yout, double& _zout);

  private:
    void loadFieldMap(void);
    T4SMagnet* sm1;

    float xSM1_[41];           //!< X coordinate of points where field of SM1 was measured.
    float ySM1_[41];           //!< Y coordinate of points where field of SM1 was measured.
    float zSM1_[108];          //!< Z coordinate of points where field of SM1 was measured.
    float bxSM1_[41][41][108]; //!< X components of the SM1 measured magnetic field.
    float bySM1_[41][41][108]; //!< Y components of the SM1 measured magnetic field.
    float bzSM1_[41][41][108]; //!< Z components of the SM1 measured magnetic field.

    void getFieldSM1(double pos_x, double pos_y, double pos_z, double& field_x,
        double& field_y, double& field_z);
};

#endif
