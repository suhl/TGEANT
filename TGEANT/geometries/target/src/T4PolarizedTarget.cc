#include "T4PolarizedTarget.hh"

T4PolarizedTarget::T4PolarizedTarget(T4SDetector* det)
{
  setPosition(det->position);
  useMechanicalStructure = det->useMechanicalStructure;

  //Solenoid dimensions
  zSol[0] = 251. / 2. * CLHEP::cm;
  zSol[1] = 200. / 2. * CLHEP::cm;
  zSol[2] = 170. / 2. * CLHEP::cm;
  zSol[3] = 146. / 2. * CLHEP::cm;
  SolRay[0] = 120. / 2. * CLHEP::cm;
  SolRay[1] = 80. / 2. * CLHEP::cm;
  SolRay[2] = 64. / 2. * CLHEP::cm;
  SolRay[3] = 54. / 2. * CLHEP::cm;
  
  cavityRadius = 26.5 * CLHEP::cm;
  cavityThickness = 0.1 * CLHEP::cm;
  targetRadius = 2.0 * CLHEP::cm;
  
  // additional parameters for target
  setDimension(positionVector.z()-100. * CLHEP::cm, positionVector.z() + 100. * CLHEP::cm);
  setELossParams(0., 0.);
}

void T4PolarizedTarget::construct(G4LogicalVolume* world_log)
{
  //Implementation "COMGEANT" based
   
  //TS02
  unsigned int ts02Index = log.size();
  tubs.push_back(new G4Tubs("tso2",0,SolRay[2],zSol[2],0.,2.* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TS02"));
  new G4PVPlacement(0, positionVector - G4ThreeVector(0,0,2*CLHEP::cm), log.back(), "polT_iron2", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray_50);
  regionManager->addToTargetRegion(log.back());

  //PTSX
  tubs.push_back(new G4Tubs("ptsx",0.,SolRay[3],zSol[2],0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->vacuum_noOptical, "PTSX"));
  new G4PVPlacement(0, G4ThreeVector(0.,0.,0.), log.back(), "polT_vac", log.at(ts02Index),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);
  vacuum_log = log.back();
  
  //TA99
  tubs.push_back(new G4Tubs("TA99",cavityRadius, cavityRadius + cavityThickness,77. * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "TA99"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "polT_cavityCu", vacuum_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);
  
  //DB16
  unsigned int db16index = log.size();
  tubs.push_back(new G4Tubs("DB16",0., targetRadius + 1.62* CLHEP::cm, 70. * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->lhe, "DB16"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,2 * CLHEP::cm), log.back(), "polT_lhe", vacuum_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkcyan);

  //TA50
  unsigned int ta50index = log.size();
  tubs.push_back(new G4Tubs("TA50",0., targetRadius + 0.06 * CLHEP::cm, 15.03 * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->pctfe, "TA50"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,-50. * CLHEP::cm), log.back(), "polT_pctfe50", log.at(db16index),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkred);
  //TA52
  unsigned int ta52index = log.size();
  tubs.push_back(new G4Tubs("TA52",0., targetRadius + 0.06 * CLHEP::cm, 15.03 * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->pctfe, "TA52"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,50. * CLHEP::cm), log.back(), "polT_pctfe52", log.at(db16index),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkred);
  //TA51
  unsigned int ta51index = log.size();
  tubs.push_back(new G4Tubs("TA51",0., targetRadius + 0.06 * CLHEP::cm, 30.03 * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->pctfe, "TA51"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "polT_pctfe51", log.at(db16index),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkred);

  //UPST
  tubs.push_back(new G4Tubs("UPST",0., targetRadius, 15. * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he, "UPST"));
  addToTarget(new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "UPST", log.at(ta50index),0,0, checkOverlap));
  log.back()->SetVisAttributes(colour->blue);
  //MDDL
  tubs.push_back(new G4Tubs("MDDL",0., targetRadius, 30. * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he, "MDDL"));
  addToTarget(new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "MDDL", log.at(ta51index),0,0, checkOverlap));
  log.back()->SetVisAttributes(colour->blue);
  //DNST
  tubs.push_back(new G4Tubs("DNST",0., targetRadius, 15. * CLHEP::cm,0.,2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he, "DNST"));
  addToTarget(new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "DNST", log.at(ta52index),0,0, checkOverlap));
  log.back()->SetVisAttributes(colour->blue);

  
  if (useMechanicalStructure) {
    //Solenoid
    tubs.push_back(new G4Tubs("tsol",SolRay[1],SolRay[0],zSol[0],0.,2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TSol"));
    new G4PVPlacement(0, positionVector - G4ThreeVector(0,0,-2*CLHEP::cm), log.back(), "polT_solenoid",world_log,0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray);
    regionManager->addToTargetRegion(log.back());

    //TS01
    tubs.push_back(new G4Tubs("tso1",SolRay[2],SolRay[1],zSol[1],0.,2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TS01"));
    new G4PVPlacement(0, positionVector - G4ThreeVector(0,0,-2*CLHEP::cm), log.back(), "polT_iron1", world_log,0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray);
    regionManager->addToTargetRegion(log.back());
    
    //FBE2
    tubs.push_back(new G4Tubs("FBE2",0., 10.4* CLHEP::cm, 0.01 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "FBE2"));
    new G4PVPlacement(0,positionVector - G4ThreeVector(0.,0.,130.49 * CLHEP::cm) - G4ThreeVector(0,0,-2*CLHEP::cm), log.back(), "FBE2", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    regionManager->addToTargetRegion(log.back());
    
    //WI99
    tubs.push_back(new G4Tubs("WI99",0. * CLHEP::cm,26.6 * CLHEP::cm,0.015 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "WI99"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,77.015 * CLHEP::cm), log.back(), "WI99", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    //DI16
    unsigned int di16index = log.size();
    tubs.push_back(new G4Tubs("DI16",targetRadius + 1.62 * CLHEP::cm,targetRadius + 1.68 * CLHEP::cm, 70. * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->epoxy, "DI16"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,2. * CLHEP::cm), log.back(), "polT_epoxy", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    //WI16
    unsigned int wi16index = log.size();
    tubs.push_back(new G4Tubs("WI16",0. * CLHEP::cm,targetRadius + 1.68 * CLHEP::cm, 0.03 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->epoxy, "WI99"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,72.03 * CLHEP::cm), log.back(), "WI16", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    //ST16
    tubs.push_back(new G4Tubs("ST16",3.670 * CLHEP::cm, 9.* CLHEP::cm, 0.05 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "ST16"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-68.2 * CLHEP::cm), log.back(), "ST16", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    
    //FLAV
    tubs.push_back(new G4Tubs("FLAV",0.,SolRay[3],0.15 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "FLAV"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,84.85 * CLHEP::cm), log.back(), "FLAV", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    //FLAR
    tubs.push_back(new G4Tubs("FLAR",0.,SolRay[3],0.02 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "FLAR"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,80.5 * CLHEP::cm), log.back(), "FLAR", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkcyan);
    //THS1
    tubs.push_back(new G4Tubs("THS1",3.35 * CLHEP::cm, 3.575 * CLHEP::cm, 3. * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "THS1"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-72.3 * CLHEP::cm), log.back(), "THS1", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);
    //THS2
    tubs.push_back(new G4Tubs("THS2",3.576 * CLHEP::cm, 5.7 * CLHEP::cm, 0.3 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "THS2"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-72.7 * CLHEP::cm), log.back(), "THS2", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //MST1
    tubs.push_back(new G4Tubs("MST1",3.68 * CLHEP::cm, cavityRadius, 0.015 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "MST1"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-30.5 * CLHEP::cm), log.back(), "MST1", vacuum_log,0,0, checkOverlap);
    //MST3
    new G4PVPlacement(0,G4ThreeVector(0.,0.,34.5 * CLHEP::cm), log.back(), "MST3", vacuum_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);
  
    //MST2-4 //Commented because of overlaps
    //tubs.push_back(new G4Tubs("MST2",3.655 * CLHEP::cm, 3.665* CLHEP::cm, 2.35 * CLHEP::cm,0.,2.*M_PI));
    //log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "MST2"));
    //new G4PVPlacement(0,G4ThreeVector(0.,0.,-32.5 * CLHEP::cm), log.back(), "MST2", log.at(di16index),0,0, checkOverlap);
    //new G4PVPlacement(0,G4ThreeVector(0.,0.,32.5 * CLHEP::cm), log.back(), "MST2", log.at(di16index),0,0, checkOverlap);
    //log.back()->SetVisAttributes(colour->orange);
    //T380
    tubs.push_back(new G4Tubs("T380",targetRadius + 1.48 * CLHEP::cm, targetRadius + 1.565* CLHEP::cm, 69. * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->kevlar, "T380"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,0.), log.back(), "polT_kevlar", log.at(db16index),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->lightgreen);

    //T441
    tubs.push_back(new G4Tubs("T380",0., targetRadius + 1.47 * CLHEP::cm, 0.1 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->kevlar, "T441"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,68.9 * CLHEP::cm), log.back(), "T441", log.at(db16index),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->lightgreen);
    
    //FF50
    tubs.push_back(new G4Tubs("FF50",0., targetRadius + 1.47 * CLHEP::cm, 0.01 * CLHEP::cm,0.,2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "FF50"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-32.5 * CLHEP::cm), log.back(), "FF50", log.at(db16index),0,0, checkOverlap);
    new G4PVPlacement(0,G4ThreeVector(0.,0.,32.5 * CLHEP::cm), log.back(), "FF51", log.at(db16index),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);
  }
  
    /*
  * C
C
C   This is schematic representation of the COMPASS PTarget.
C   Partly taken from : COMGEANT geom_muon_066
C   ... 2007 NH3 target
C                                                               Version 28.06.2007
C
C
C
C         name   med  mother    X     Y     Z  rot  shape npar  par.....
C
C COMPASS Magnet (preliminary, based on material budget provided by Jaakko 25.04.06)
C
  PARVOL52 'TCMP'    1  'HALL'     0.    0.    0.   0  'BOX '  3   135.   60.  60.          Air
C
  PARVOL53 'TSOL'  295  'TCMP'     0.    0.    0.   1  'TUBE'  3    40.   60.  130.5        Fe
  PARVOL54 'TSO1'  295  'TCMP'     0.    0.    0.   1  'TUBE'  3    32.   40.  100.         Fe
  PARVOL55 'TSO2'  295  'TCMP'     0.    0.    0.   1  'TUBE'  3     0.   32.   85.         Fe
  PARVOL56 'PTSX'  200  'TSO2'     0.    0.    0.   0  'TUBE'  3     0.   27.0  85.         Vacu
C vacuum window
  PARVOL57 'FLAV'  235  'PTSX'     0.    0.   84.85  0  'TUBE'  3     0.   27.0    0.15      Al
C radiation shield
  PARVOL58 'FLAR'  235  'PTSX'     0.    0.   80.5   0  'TUBE'  3     0.   27.0    0.02      Al
C cavity, cyllindrical one
  PARVOL59 'TA99'  231  'PTSX'     0.    0.    0.    0  'TUBE'  3    26.5 26.6   77.        Cu
  PARVOL60 'WI99'  231  'PTSX'     0.    0.   77.015 0  'TUBE'  3     0.  26.6    0.015     Cu
C mixing chamber - long fiberglass tube, fiberglass flance at right, steel flance at left
  PARVOL61 'DI16'  266  'PTSX'     0.    0.    2.   0  'TUBE'  3     3.605 3.665  70.    Fbg
  PARVOL62 'WI16'  266  'PTSX'     0.    0.   72.03 0  'TUBE'  3     0.    3.665   0.03  Fbg
  PARVOL63 'ST16'  295  'PTSX'     0.    0.  -68.2  0  'TUBE'  3     3.670 9.000   0.05  Fe
C some foils at the beam entrance
  PARVOL65 'FBE2'  295  'TCMP'  -130.49  0.    0.   1  'TUBE'  3     0.   10.400   0.01 Fe
C liquid helium fills it  ( maybe more He3 at the top ...)
  PARVOL66 'DB16'  263  'PTSX'     0.    0.    2.   0  'TUBE'  3     0.    3.605  70.    LHe
C mwave stopper upsteram
  PARVOL67 'MST1'  231  'PTSX'     0.    0. -30.5   0  'TUBE'  3    3.665 26.500   0.015 Cu
  PARVOL68 'MST2'  231  'DI16'     0.    0. -32.5   0  'TUBE'  3    3.655  3.665   2.35  Cu
C mwave stopper downstream
  PARVOL69 'MST3'  231  'PTSX'     0.    0.  34.5   0  'TUBE'  3    3.665 26.500   0.015 Cu
  PARVOL70 'MST4'  231  'DI16'     0.    0.  32.5   0  'TUBE'  3    3.655  3.665   2.35  Cu
C target holder - Kevlar plus right/left flange
  PARVOL71 'T380'  244  'DB16'     0.    0.    0.   0  'TUBE'  3    3.47   3.555   69.00   Kvlr
  PARVOL72 'THS1'  296  'PTSX'     0.    0.  -70.   0  'TUBE'  3    3.35   3.575   3.     Fe
  PARVOL73 'THS2'  296  'PTSX'     0.    0.  -72.7  0  'TUBE'  3    3.575  5.7     0.3    Fe
  PARVOL74 'T441'  244  'DB16'     0.    0.   68.9  0  'TUBE'  3    0.   3.47     0.1   Kvlr
C LiD holders. Polyester (Mylar) net (60% open) 300um???
  PARVOL75 'TA50'  250  'DB16'     0.    0.  -50.   0  'TUBE'  3     0.   2.015   15.03    Mlr
  PARVOL76 'TA51'  250  'DB16'     0.    0.    0.   0  'TUBE'  3     0.   2.015   30.03    Mlr
  PARVOL77 'TA52'  250  'DB16'     0.    0.   50.   0  'TUBE'  3     0.   2.015   15.03    Mlr
C thin mashes in between the three cells
  PARVOL78 'FF50'  231  'DB16'     0.    0.  -32.5  0. 'TUBE'  3     0.    3.47     0.01  Cu
  PARVOL79 'FF51'  231  'DB16'     0.    0.   32.5  0. 'TUBE'  3     0.    3.47     0.01  Cu
C The NH3 for upstream and middle and downstream cells but also 0.4 liquid helium - Packing Factor
  PARVOL80 'UPST'  251  'TA50'     0.    0.   0.   0  'TUBE'  3     0.0   2.0    15.    LiD
  PARVOL81 'MDDL'  251  'TA51'     0.    0.   0.   0  'TUBE'  3     0.0   2.0    30.    LiD
  PARVOL82 'DNST'  251  'TA52'     0.    0.   0.   0  'TUBE'  3     0.0   2.0    15.    LiD
C
C
  PARVOL83 'TABS'   97  'HALL'     0.    0.    0.   1  'TUBE'  3   249.9  250.   250.    Absorber
C
C the coils 5 pieces for every half  about 0.5 g of a Cu maybe more - not in target material any more
C should be "dissolved" in He.
C
C ------ Define the target volumes and the target area
C       ( to simulate the interaction point )
C
TRGVOL    'UPST'   'MDDL' 'DNST'
TRGPROB    0.25     0.5   0.25
TRGLIM     -800. 100.  -3. 3.   -3. 3.
C
C   This can be improved ...
C
C
  */
}

T4PolarizedTarget::~T4PolarizedTarget(void)
{
 for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}

void T4PolarizedTarget::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.name = "UPST";
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = 20.0;
  target.ySize = 300.;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();
  target.zCen = positionVector.getZ() - 500.;
  targetInformation.push_back(target);

  target.name = "MDDL";
  target.ySize = 600.;
  target.zCen = positionVector.getZ();
  targetInformation.push_back(target);
  
    target.name = "DNST";
  target.ySize = 300.;
  target.zCen = positionVector.getZ() + 500.;
  targetInformation.push_back(target);
}
