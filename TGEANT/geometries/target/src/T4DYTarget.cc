#include "T4DYTarget.hh"

T4DYTarget::T4DYTarget(T4SDetector* det)
{
  setPosition(det->position);
  useMechanicalStructure = det->useMechanicalStructure;

  //Solenoid dimensions
  zSol[0] = 251. / 2. * CLHEP::cm;
  zSol[1] = 200. / 2. * CLHEP::cm;
  zSol[2] = 170. / 2. * CLHEP::cm;
  zSol[3] = 146. / 2. * CLHEP::cm;
  SolRay[0] = 120. / 2. * CLHEP::cm;
  SolRay[1] = 80. / 2. * CLHEP::cm;
  SolRay[2] = 64. / 2. * CLHEP::cm;
  SolRay[3] = 54. / 2. * CLHEP::cm;
  //Windows downstream spherical plate dimensions
  sphRay[0] = 116.2 / 2. * CLHEP::cm;
  sphRay[1] = 116.8 / 2. * CLHEP::cm;
  sphAng[0] = 2.642;
  //Aluminized Mylar foils parameters
  AlMlRay[0] = 12.0 / 2. * CLHEP::cm;
  AlMlRay[1] = 10.0 / 2. * CLHEP::cm;
  AlMlRay[2] = 9.8 / 2. * CLHEP::cm;
  zAlMl = 0.01 / 2. * CLHEP::cm;
  //Cavity
  cavityPar[0] = 40. / 2. * CLHEP::cm;
  cavityPar[1] = 40.2 / 2. * CLHEP::cm;
  cavityPar[2] = 140.2 / 2. * CLHEP::cm;
  //Cavity spherical window
  cavityWin[0] = 200. / 2. * CLHEP::cm;
  cavityWin[1] = 200.03 / 2. *CLHEP::cm;
  cavityWin[2] = (14.744 / 360.)*2 * M_PI;

  //Mixing chamber parameters
  mixPar[0] = 7.15 / 2. * CLHEP::cm;
  mixPar[1] = 7.27 / 2. * CLHEP::cm;
  mixPar[2] = 130.8 / 2. * CLHEP::cm;
  wi16Par[0] = 7.32 / 2. * CLHEP::cm;
  wi16Par[1] = 7.44 / 2. * CLHEP::cm;
  //Foils parameters
  foilPar[0] = 8.2 / 2. * CLHEP::cm;
  foilPar[1] = 6. / 2. * CLHEP::cm;
  foilPar[2] = 0.01 / 2. * CLHEP::cm;
  //Kevlar tube parameters
  kevlarPar[0] = 7.08 / 2. * CLHEP::cm;
  kevlarPar[1] = 7.10 / 2. * CLHEP::cm;
  kevlarPar[2] = 134.2 / 2. * CLHEP::cm;
  //Liquid helium parameters
  lHePar[0] = 6.94 / 2. * CLHEP::cm;
  lHePar[1] = 138. / 2. * CLHEP::cm;
  lHePar[2] = 7.24 / 2. * CLHEP::cm;
  //Copper parameters
  copPar[0] = 7.630 / 2. * CLHEP::cm;
  copPar[1] = 7.690 / 2. * CLHEP::cm;
  copPar[2] = 16. / 2. * CLHEP::cm;
  copPar[3] = 7.70 / 2. * CLHEP::cm;
  copPar[4] = 39.0 / 2. * CLHEP::cm;
  copPar[5] = 0.03 / 2. * CLHEP::cm;
  //Micro wave stopper parameters
  mstPar[0] = 6.908 / 2. * CLHEP::cm;
  mstPar[1] = 6.910 / 2. * CLHEP::cm;
  mstPar[2] = 4. / 2. * CLHEP::cm;
  //Meshes parameters
  meshPar[0] = 7. / 2. * CLHEP::cm;
  meshPar[1] = 6.9 / 2. * CLHEP::cm;
  meshPar[2] = 0.02 / 2. * CLHEP::cm;
  //Radiation shields parameters
  shieldPar[0] = 63.9 / 2. * CLHEP::cm;
  shieldPar[1] = 0.05 / 2. * CLHEP::cm;
  //NH3 holder parameters
  nh3hold[0] = 4.03 / 2. *CLHEP::cm;
  nh3hold[1] = 55. / 2. *CLHEP::cm;

  lheLength = 137.9 / 2. * CLHEP::cm;
  lheRadius = 6.9 / 2. * CLHEP::cm;
  // lhePositionShift is the difference between the center of the LHe tube
  // and the position of T4DYTarget (the center of both nh3 tubs)
  lhePositionShift = -3.95 * CLHEP::cm; //-3.95 cm

  nh3Length = 55. / 2. * CLHEP::cm; // To be changed!
  nh3Radius = 4. / 2. * CLHEP::cm;
  cellGap = 20. / 2. * CLHEP::cm;
  
  // additional parameters for target
  // in principle unused because we don't want to extrapolate in this case
  // Anyway, let's do it correct. The downstream end of the target gets updated by the absorber class.
  setDimension(positionVector.z() - 100. * CLHEP::cm, positionVector.z() + 100. * CLHEP::cm);
}

void T4DYTarget::construct(G4LogicalVolume* world_log)
{
  // New implementantion of the target (A LA COMGEANT)
  // Some modifications: changed length of vacuum chamber to solve overlaps!

  //PTSX
  unsigned int onIndex = log.size();
  G4ThreeVector helper = G4ThreeVector(0,0,3.3 / 2. * CLHEP::cm);
  tubs.push_back(new G4Tubs("ptsx",0.,SolRay[3],zSol[3]+ 3.3 / 2. * CLHEP::cm,0.,2.* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->vacuum_noOptical, "PTSX"));
  new G4PVPlacement(0, positionVector + helper, log.back(), "PTSX", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  //Liquid helium in the kevlar
  unsigned int lHeIndex = log.size();
  tubs.push_back(new G4Tubs("lHe_tube",0.,lHePar[0],lHePar[1],0.,2* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->lhe, "lHe_targ"));
  new G4PVPlacement(0, G4ThreeVector(0.,0.,-4*CLHEP::cm)-helper, log.back(), "lHe_targ", log.at(onIndex),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->blue_50);

  //NH3 holders
  unsigned int fstCellIndex = log.size();
  tubs.push_back(new G4Tubs("ta50",0., nh3hold[0], nh3hold[1], 0.,2* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->mylar, "ta50"));
  new G4PVPlacement(0, G4ThreeVector(0.,0.,-33.5*CLHEP::cm), log.back(), "ta50", log.at(lHeIndex),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->magenta);

  unsigned int sndCellIndex = log.size();
  tubs.push_back(new G4Tubs("ta51",0., nh3hold[0], nh3hold[1], 0.,2* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->mylar, "ta51"));
  new G4PVPlacement(0, G4ThreeVector(0.,0.,41.5*CLHEP::cm), log.back(), "ta51", log.at(lHeIndex),0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->magenta);

  //NH3 cells
  tubs.push_back(new G4Tubs("Upst_cell",0., nh3Radius, nh3Length, 0.,2* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he_1st, "upst_cell"));
  addToTarget(new G4PVPlacement(0, G4ThreeVector(0.,0.,0.), log.back(), "upst_cell", log.at(fstCellIndex),0,0, checkOverlap));
  log.back()->SetVisAttributes(colour->darkred);

  tubs.push_back(new G4Tubs("Down_cell",0., nh3Radius, nh3Length, 0.,2* M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he_2nd, "down_cell"));
  addToTarget(new G4PVPlacement(0, G4ThreeVector(0.,0.,0.), log.back(), "down_cell", log.at(sndCellIndex),0,0, checkOverlap));
  log.back()->SetVisAttributes(colour->darkred);

  if (useMechanicalStructure) {
    //Solenoid
    tubs.push_back(new G4Tubs("tsol",SolRay[1],SolRay[0],zSol[0],0.,2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TSol"));
    new G4PVPlacement(0, positionVector, log.back(), "Solenoid",world_log,0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->silver_20);

    //TS01
    tubs.push_back(new G4Tubs("tso1",SolRay[2],SolRay[1],zSol[1],0.,2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TS01"));
    new G4PVPlacement(0, positionVector, log.back(), "TS01", world_log,0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->silver_20);
    
    sphere.push_back(new G4Sphere("db17", 0., lHePar[2],0.,2* M_PI,0., M_PI/2.));
    log.push_back(new G4LogicalVolume(sphere.back(), materials->lhe, "DB17"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,65*CLHEP::cm)-helper, log.back(),"DB17", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->blue_50);

    //TS02
    unsigned int ts02Index = log.size();
    tubs.push_back(new G4Tubs("tso2",SolRay[3],SolRay[2],zSol[2],0.,2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "TS02"));
    new G4PVPlacement(0, positionVector, log.back(), "TS02", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->silver_20);
  
    //Window downstream spherical plate
    sphere.push_back(new G4Sphere("flav",sphRay[0],sphRay[1], 0., 2* M_PI, 2.6415, M_PI));
    log.push_back(new G4LogicalVolume(sphere.back(), materials->aluminium_noOptical, "FLAV"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,140.4*CLHEP::cm), log.back(), "FLAV", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Radiation shields
    tubs.push_back(new G4Tubs("flr1",0.,AlMlRay[0],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR1"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-391.1*CLHEP::cm), log.back(), "FLR1", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    tubs.push_back(new G4Tubs("flr2",0.,AlMlRay[0],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR2"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-382.1*CLHEP::cm), log.back(), "FLR2", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    tubs.push_back(new G4Tubs("flr3",0.,AlMlRay[1],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR3"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-370.6*CLHEP::cm), log.back(), "FLR3", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    tubs.push_back(new G4Tubs("flr4",0.,AlMlRay[2],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR4"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-124.1*CLHEP::cm), log.back(), "FLR4", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    tubs.push_back(new G4Tubs("flr5",0.,AlMlRay[2],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR5"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-107.6*CLHEP::cm), log.back(), "FLR5", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    tubs.push_back(new G4Tubs("flr6",0.,AlMlRay[2],zAlMl,0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "FLR6"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-91.1*CLHEP::cm), log.back(), "FLR6", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkyellow);

    //Copper cavity
    tubs.push_back(new G4Tubs("cavity_cilinder",cavityPar[0],cavityPar[1],cavityPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "CAVITY_CIL"));
    new G4PVPlacement(0, -helper, log.back(), "CAVITY_CIL", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);

    //Cavity window
    sphere.push_back(new G4Sphere("spherical_window", cavityWin[0],cavityWin[1],0.,2* M_PI,0.,cavityWin[2]));
    log.push_back(new G4LogicalVolume(sphere.back(), materials->Cu, "CAVITY_WIN"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-26.9 *CLHEP::cm)-helper, log.back(),"CAVITY_WIN", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Mixing chamber - long fiberglass tube, fiberglass round cover downstream
    tubs.push_back(new G4Tubs("di16",mixPar[0],mixPar[1],mixPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->g10, "DI16"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-0.4*CLHEP::cm)-helper, log.back(), "DI16", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    // V
    sphere.push_back(new G4Sphere("wi16", wi16Par[0], wi16Par[1],0.,2* M_PI,0., M_PI/2.));
    log.push_back(new G4LogicalVolume(sphere.back(), materials->g10, "WI16"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,65*CLHEP::cm)-helper, log.back(),"WI16", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Foils at the beam entrance
    tubs.push_back(new G4Tubs("fbe1",0.,foilPar[0],foilPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "FBE1"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-412.5*CLHEP::cm), log.back(), "FBE1", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray);

    tubs.push_back(new G4Tubs("fbe2",0.,foilPar[1],foilPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->iron, "FBE2"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-73.05*CLHEP::cm), log.back(), "FBE2", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray);

    //Kevlar tube
    tubs.push_back(new G4Tubs("kevlar_tube",kevlarPar[0],kevlarPar[1],kevlarPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->kevlar, "kevlar_tube"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-2.1*CLHEP::cm)-helper, log.back(), "kevlar_tube", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepyellow);
    
      //Copper tube and rings
    tubs.push_back(new G4Tubs("mst0",copPar[0],copPar[1],copPar[2],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "mst0"));
    new G4PVPlacement(0, -helper, log.back(), "mst0", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    tubs.push_back(new G4Tubs("mst1",copPar[3],copPar[4],copPar[5],0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "mst1"));
    new G4PVPlacement(0, -helper, log.back(), "mst1", log.at(onIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Micro wave stoppers
    tubs.push_back(new G4Tubs("mst2",mstPar[0], mstPar[1], mstPar[2], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "mst2"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-2*CLHEP::cm), log.back(), "mst2", log.at(lHeIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    tubs.push_back(new G4Tubs("mst3",mstPar[0], mstPar[1], mstPar[2], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "mst3"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,10*CLHEP::cm), log.back(), "mst3", log.at(lHeIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Copper meshes todo
    tubs.push_back(new G4Tubs("ff50",0., /*meshPar in onIndex*/lHePar[0], meshPar[2], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "ff50"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-65.9*CLHEP::cm/*-69.9 in onIndex*/)-helper, log.back(), "ff50", log.at(lHeIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    tubs.push_back(new G4Tubs("ff51",0., meshPar[1], meshPar[2], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "ff51"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-4.*CLHEP::cm), log.back(), "ff51", log.at(lHeIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    tubs.push_back(new G4Tubs("ff52",0., meshPar[1], meshPar[2], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "ff52"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,12.*CLHEP::cm), log.back(), "ff52", log.at(lHeIndex),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);
  
    //Downstream radiation shields
    //First shield -> Segmented into two parts to avoid overlaps - inner part
    tubs.push_back(new G4Tubs("flr7inMoth",0.,SolRay[3], shieldPar[1], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "flr7inMoth"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,76.6*CLHEP::cm), log.back(), "flr7inMoth", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);
    //First shield - outer part
    tubs.push_back(new G4Tubs("flr7inT02",SolRay[3],shieldPar[0], shieldPar[1], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "flr7inT02"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,76.6*CLHEP::cm), log.back(), "flr7inT02", log.at(ts02Index),0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);

    //Second shield
    tubs.push_back(new G4Tubs("flr8",0.,SolRay[3], shieldPar[1], 0.,2* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->AlMylar, "flr8"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,76.6*CLHEP::cm), log.back(), "flr8", world_log,0,0, checkOverlap);
    log.back()->SetVisAttributes(colour->deepcyan);
  }
}

T4DYTarget::~T4DYTarget(void)
{
 for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < sphere.size();i++)
    delete sphere.at(i);
  sphere.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}

void T4DYTarget::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.name = "UPST";
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = nh3Radius;
  target.ySize = nh3Length * 2.;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();
  target.zCen = positionVector.getZ() - cellGap - nh3Length;
  targetInformation.push_back(target);

  target.name = "DNST";
  target.zCen = positionVector.getZ() + cellGap + nh3Length;
  targetInformation.push_back(target);
}
