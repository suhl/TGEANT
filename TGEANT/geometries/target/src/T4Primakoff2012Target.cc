#include "T4Primakoff2012Target.hh"

T4Primakoff2012Target::T4Primakoff2012Target(T4SDetector* target)
{
  setPosition(target->position);

  lengthNickel = 4.2 * CLHEP::mm;
  lengthTungsten = 50. * CLHEP::micrometer;

  zPosNickel = (78. + 4.2 / 2.) * CLHEP::mm;
  zPosTungsten = 420. * CLHEP::mm;

  theta0 = 280.9 * CLHEP::deg;
  //additional parameters for target
  setDimension(positionVector.z(), positionVector.z()/* + 435.5 * CLHEP::mm*/);
}

T4Primakoff2012Target::~T4Primakoff2012Target()
{

}

void T4Primakoff2012Target::construct(G4LogicalVolume* world_log)
{
  // blue line as origin for the moment
  placeTargetHolder(world_log, positionVector + G4ThreeVector(0, 0, (-497.5 - 10. / 2.) * CLHEP::mm),
      positionVector + G4ThreeVector(0, 0, -5.1 / 2. * CLHEP::mm));
  placeCarbonRods(world_log, positionVector + G4ThreeVector(0, 0, -500 * CLHEP::mm));
  placeSmallRing(world_log, positionVector + G4ThreeVector(0, 0, 5. / 2. * CLHEP::mm));
  placeSmallCarbonRods(world_log, positionVector + G4ThreeVector(0, 0, 380. / 2. * CLHEP::mm));

  placeNickelTarget(world_log, positionVector + G4ThreeVector(0, 0, zPosNickel));
  placeTungstenTarget(world_log, positionVector + G4ThreeVector(0, 0, zPosTungsten));
}

void T4Primakoff2012Target::placeTargetHolder(G4LogicalVolume* world_log, G4ThreeVector posU, G4ThreeVector posD)
{
  // picture of this element:
  // http://wwwcompass.cern.ch/twiki/pub/HadronAnalysis/NuclearTargets2012/flange1_measurement.pdf

  // 1) prepare all elements:
  int innerRing = solid.size();
  solid.push_back(new G4Tubs("targetHolder_innerRing_tubs", 49.8 / 2. * CLHEP::mm, (49.8 / 2. + 22.1) * CLHEP::mm, 5. * CLHEP::mm, 0., 2.* M_PI));
  int outerRing = solid.size();
  solid.push_back(new G4Tubs("targetHolder_outerRing_tubs", (244. / 2. - 22.1) * CLHEP::mm, 244. / 2. * CLHEP::mm, 5. * CLHEP::mm, 0., 2.* M_PI));
  int connectorBox = solid.size();
  solid.push_back(new G4Box("targetHolder_connector_box", 37.5 * CLHEP::mm, 30.2 / 2. * CLHEP::mm, 5. * CLHEP::mm));
  int carbonHoles = solid.size();
  solid.push_back(new G4Tubs("targetHolder_carbonHoles_tubs", 0, 8. / 2. * CLHEP::mm, 5.11 * CLHEP::mm, 0., 2.* M_PI));

  // 2) connect all parts:
  // two rings
  solid.push_back(new G4UnionSolid("targetHolder_union", solid.at(innerRing), solid.at(outerRing)));

  // the 4 box connectors
  double angle = theta0 - 60. * CLHEP::deg;
  for (int i = 0; i < 4; i++) {
    rot.push_back(new CLHEP::HepRotation);
    rot.back()->rotateZ(-angle);

    double r = 73.45 * CLHEP::mm;
    solid.push_back(new G4UnionSolid("targetHolder_union", solid.back(), solid.at(connectorBox), rot.back(), G4ThreeVector(r * cos(angle),r * sin(angle),0)));

    angle += 90. * CLHEP::deg;
  }

  // subtract the 3 holes for the carbon rods
  angle = theta0 - 60. * CLHEP::deg;
  double r = (49.8 + 22.1) / 2. * CLHEP::mm;
  for (int i = 0; i < 3; i++) {
    solid.push_back(new G4SubtractionSolid("targetHolder_sub", solid.back(), solid.at(carbonHoles), 0, G4ThreeVector(r * cos(angle),r * sin(angle),0)));

    angle += 120. * CLHEP::deg;
  }

  // 3) set material and place it:
  log.push_back(new G4LogicalVolume(solid.back(), materials->aluminium_noOptical, "targetHolder_log"));
  log.back()->SetVisAttributes(colour->silver);
  new G4PVPlacement(0, posU, log.back(), "targetHolder", world_log, false, 0, checkOverlap);

  // 4) now the other ring:
  // right picture: http://wwwcompass.cern.ch/twiki/pub/HadronAnalysis/NuclearTargets2012/flange2_measurement.pdf
  solid.push_back(new G4Tubs("secondRing_tubs", 48. / 2. * CLHEP::mm, 84. / 2. * CLHEP::mm, 5.1 / 2. * CLHEP::mm, 0., 2.* M_PI));

  // subtract the 3 holes for the carbon rods
  angle = theta0 - 60. * CLHEP::deg;
  for (int i = 0; i < 3; i++) {
    solid.push_back(new G4SubtractionSolid("secondRing_sub", solid.back(), solid.at(carbonHoles), 0, G4ThreeVector(r * cos(angle),r * sin(angle),0)));
    angle += 120. * CLHEP::deg;
  }

  log.push_back(new G4LogicalVolume(solid.back(), materials->aluminium_noOptical, "secondRing_log"));
  log.back()->SetVisAttributes(colour->silver);
  new G4PVPlacement(0, posD, log.back(), "secondRing", world_log, false, 0, checkOverlap);
}

void T4Primakoff2012Target::placeCarbonRods(G4LogicalVolume* world_log, G4ThreeVector pos)
{
  solid.push_back(new G4Tubs("carbonRods_tubs", 3. * CLHEP::mm, 4. * CLHEP::mm, 0.5 * CLHEP::m, 0., 2.* M_PI));
  log.push_back(new G4LogicalVolume(solid.back(), materials->carbonFibre, "carbonRods_log"));
  log.back()->SetVisAttributes(colour->black);

  double r = (49.8 + 22.1) / 2. * CLHEP::mm;
  double angle = theta0 - 60. * CLHEP::deg;
  for (int i = 0; i < 3; i++) {
    new G4PVPlacement(0, pos + G4ThreeVector(r * cos(angle),r * sin(angle),0), log.back(), "carbonRods", world_log, false, 0, checkOverlap);
    angle += 120. * CLHEP::deg;
  }
}

void T4Primakoff2012Target::placeSmallRing(G4LogicalVolume* world_log, G4ThreeVector pos)
{
  // left picture: http://wwwcompass.cern.ch/twiki/pub/HadronAnalysis/NuclearTargets2012/flange2_measurement.pdf

  int carbonHoles = solid.size();
  solid.push_back(new G4Tubs("smallRing_carbonHoles_tubs", 0, 4. / 2. * CLHEP::mm, 5.01 * CLHEP::mm, 0., 2.* M_PI));

  solid.push_back(new G4Tubs("smallRing_tubs", 48. / 2. * CLHEP::mm, 66. / 2. * CLHEP::mm, 5. / 2. * CLHEP::mm, 0., 2.* M_PI));

  // subtract the 3 holes for the carbon rods
  double r = 27. * CLHEP::mm;
  double angle = theta0 - 0. * CLHEP::deg;
  for (int i = 0; i < 3; i++) {
   solid.push_back(new G4SubtractionSolid("smallRing_sub", solid.back(), solid.at(carbonHoles), 0, G4ThreeVector(r * cos(angle),r * sin(angle),0)));
   angle += 120. * CLHEP::deg;
  }

  log.push_back(new G4LogicalVolume(solid.back(), materials->aluminium_noOptical, "smallRing_log"));
  log.back()->SetVisAttributes(colour->silver);
  new G4PVPlacement(0, pos, log.back(), "smallRing", world_log, false, 0, checkOverlap);
}

void T4Primakoff2012Target::placeSmallCarbonRods(G4LogicalVolume* world_log, G4ThreeVector pos)
{
  double lengthPart1 = 380. / 2. * CLHEP::mm;
  solid.push_back(new G4Tubs("smallCarbonRods_tubs", 1.25 * CLHEP::mm, 2. * CLHEP::mm, lengthPart1, 0., 2.* M_PI));
  int index1 = log.size();
  log.push_back(new G4LogicalVolume(solid.back(), materials->carbonFibre, "smallCarbonRods_log"));
  log.back()->SetVisAttributes(colour->black);

  // lets add 33.5 on both sides ... there is a '?' on http://wwwcompass.cern.ch/twiki/pub/HadronAnalysis/NuclearTargets2012/tungsten_target_measurement.pdf
  solid.push_back(new G4Tubs("metalRods_tubs", 0.75 * CLHEP::mm, 1.25 * CLHEP::mm, (22. / 2. + 33.5) * CLHEP::mm, 0., 2.* M_PI));
  int index2 = log.size();
  log.push_back(new G4LogicalVolume(solid.back(), materials->stainlessSteel, "metalRods_log"));
  log.back()->SetVisAttributes(colour->silver);

  double lengthPart3 = 33.5 / 2. * CLHEP::mm;
  solid.push_back(new G4Tubs("endCarbonRods_tubs", 1.25 * CLHEP::mm, 2. * CLHEP::mm, lengthPart3, 0., 2.* M_PI));
  int index3 = log.size();
  log.push_back(new G4LogicalVolume(solid.back(), materials->carbonFibre, "endCarbonRods_log"));
  log.back()->SetVisAttributes(colour->black);

  double r = 27. * CLHEP::mm;
  double angle = theta0 - 0. * CLHEP::deg;
  for (int i = 0; i < 3; i++) {
    double x = r * cos(angle);
    double y = r * sin(angle);
    new G4PVPlacement(0, pos + G4ThreeVector(x, y, 0), log.at(index1), "smallCarbonRods", world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, pos + G4ThreeVector(x, y, lengthPart1 + 11. * CLHEP::mm), log.at(index2), "metalRods", world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, pos + G4ThreeVector(x, y, lengthPart1 + 22. * CLHEP::mm + lengthPart3), log.at(index3), "endCarbonRods", world_log, false, 0, checkOverlap);

    angle += 120. * CLHEP::deg;
  }
}

void T4Primakoff2012Target::placeNickelTarget(G4LogicalVolume* world_log, G4ThreeVector pos)
{
  solid.push_back(new G4Tubs("nickel", 0, 50. / 2. * CLHEP::mm, lengthNickel / 2., 0., 2. * M_PI));
  log.push_back(new G4LogicalVolume(solid.back(), materials->nickel, "nickel_log"));
  log.back()->SetVisAttributes(colour->red);
  addToTarget(new G4PVPlacement(0, pos, log.back(), "nickel", world_log, false, 0, checkOverlap));
  regionManager->addToTargetRegion(log.back());
}

void T4Primakoff2012Target::placeTungstenTarget(G4LogicalVolume* world_log, G4ThreeVector pos)
{
  double angle = 19.22 * CLHEP::deg;
  solid.push_back(new G4Tubs("tungsten_50", 0, 50. / 2. * CLHEP::mm, lengthTungsten / 2., 0. + angle, M_PI));
  log.push_back(new G4LogicalVolume(solid.back(), materials->tungsten, "tungsten_50_log"));
  log.back()->SetVisAttributes(colour->green);
  addToTarget(new G4PVPlacement(0, pos, log.back(), "tungsten_50", world_log, false, 0, checkOverlap));
  regionManager->addToTargetRegion(log.back());

  solid.push_back(new G4Tubs("tungsten_25", 0, 50. / 2. * CLHEP::mm, lengthTungsten / 4., M_PI + angle, M_PI));
  log.push_back(new G4LogicalVolume(solid.back(), materials->tungsten, "tungsten_25_log"));
  log.back()->SetVisAttributes(colour->red);
  addToTarget(new G4PVPlacement(0, pos, log.back(), "tungsten_25", world_log, false, 0, checkOverlap));
  regionManager->addToTargetRegion(log.back());
}

double T4Primakoff2012Target::getRndmTargetDist(double maxTargetLength)
{
  double rndm = T4SettingsFile::getInstance()->getRandom()->flat();
  double scalingNickel =  8.90 / 58.6934 * 28 * 28; //rho/A*Z*Z

  double scalingTungsten = 19.3 / 183.84 * 74 * 74 /** 2./3*/; //rho/A*Z*Z; (as an assumption for the split disk)

  double rndmLength =  (lengthNickel * scalingNickel + lengthTungsten * scalingTungsten) * rndm;
  if(rndmLength <= lengthNickel * scalingNickel)
    return rndmLength / scalingNickel;
  else
    return lengthNickel + (rndmLength - lengthNickel * scalingNickel) / scalingTungsten;
}

void T4Primakoff2012Target::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = 50 * CLHEP::mm;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();

  target.name = "NICK";
  target.ySize = lengthNickel;
  target.zCen = positionVector.getZ() + zPosNickel;
  targetInformation.push_back(target);

  target.name = "WF01";
  target.ySize = lengthTungsten;
  target.zCen = positionVector.getZ() + zPosTungsten;
  targetInformation.push_back(target);
}
