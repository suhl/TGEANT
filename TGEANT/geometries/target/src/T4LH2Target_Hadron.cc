#include "T4LH2Target_Hadron.hh"

T4LH2Target_Hadron::T4LH2Target_Hadron(T4SDetector* lh2)
{
  setPosition(lh2->position);
  useMechanicalStructure = lh2->useMechanicalStructure;

  lh2Length = 40.0 / 2. * CLHEP::cm;
  lh2Radius = 3.5 * CLHEP::cm;
  
  //additional parameters for target
  setDimension(positionVector.z() - lh2Length, positionVector.z() + lh2Length);
  setELossParams(0., 0.);
}

// positioVector = middle of lh2 tube
void T4LH2Target_Hadron::construct(G4LogicalVolume* world_log)
{
  // a simple tube for the LH2 target
  tubs.push_back(new G4Tubs("lh2_tubs", 0., lh2Radius, lh2Length, 0., 2.* M_PI));
  
  // the logical volume to assign the lh2 material
  log.push_back(new G4LogicalVolume(tubs.back(), materials->lh2, "lh2_log"));
  
  // we place the volume and we define it as target (otherwise the vertex generation will not work!)
  addToTarget(new G4PVPlacement(0, positionVector, log.back(), "LH2Hadron", world_log, 0, 0, checkOverlap));
  
  // add a colour
  log.back()->SetVisAttributes(colour->red);
  
  // add the logical volume to the target region 
  // This is important for the step limitation and therefore important for the vertex generation!
  regionManager->addToTargetRegion(log.back());

  if (useMechanicalStructure) {
    // here one has to add the mechanical structure
    // this is very important for the energy loss of all the recoiled particles 
    // TODO!
  }
}

void T4LH2Target_Hadron::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.name = "T001";
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = lh2Radius;
  target.ySize = lh2Length * 2.;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();
  target.zCen = positionVector.getZ();

  targetInformation.push_back(target);
}

T4LH2Target_Hadron::~T4LH2Target_Hadron(void)
{
  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}
