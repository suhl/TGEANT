#include "T4LH2Target.hh"

T4LH2Target::T4LH2Target(T4SDetector* lh2)
{
  setPosition(lh2->position);
  useMechanicalStructure = lh2->useMechanicalStructure;
  
  yearSetup = strToDouble(T4EventManager::getInstance()->getTriggerPlugin()->getPluginName().substr(0,4));

  downstreamHole = 55.0 * CLHEP::mm;

  vacuumOuterRadius = 78.0 * CLHEP::mm / 2;
  vacuumLenght = (2710.0 + 458.0) * CLHEP::mm / 2;

  carbonThickness = 0.566 * CLHEP::mm;
  carbonLength = (2610.0 + 54.0) * CLHEP::mm / 2;
  epoxyThickness = 0.434 * CLHEP::mm;

  silverThickness = 0.032 * CLHEP::mm;

  aluminiumInnerRadius = 47.0 * CLHEP::mm / 2; // 45=>47 to avoid overlap introduced with the rotated lh2
  aluminiumThickness = 30 * 0.011 * CLHEP::mm;
  aluminiumLength = carbonLength;

  rohacellThickness = 11.0 * CLHEP::mm;
  rohacellLength = 11.0 * CLHEP::mm / 2;

  carbonDownInnerRadius = 76.0 * CLHEP::mm / 2;
  carbonDownThickness = 2.0 * CLHEP::mm;
  carbonDownLength = 35.0 * CLHEP::mm / 2;

  mylarWindowLength = 0.35 * CLHEP::mm / 2;

  lh2Radius = 40.0 * CLHEP::mm / 2;
  kaptonThickness = 0.125 * CLHEP::mm;
  kaptonLength = 2535.0 * CLHEP::mm / 2; //-5mm because of mylar sphere with radius 20mm

  inoxSuperPosUpDownInnerRadius = lh2Radius + kaptonThickness;
  inoxSuperPosUpDownLength = 5.0 * CLHEP::mm / 2;
  inoxThickness = 0.25 * CLHEP::mm;

  inoxSuperPosMidInnerRadius = lh2Radius + kaptonThickness;
  inoxSuperPosMidLength = 10.0 * CLHEP::mm / 2;

  inoxPieceInnerRadius = lh2Radius;
  inoxPieceLength = 80.0 * CLHEP::mm / 2;
  inoxPieceMidLength = 22.0 * CLHEP::mm / 2;
  inoxPieceMidThickness = 1.5 * CLHEP::mm;

  mylarEndCapRadius = 20.0 * CLHEP::mm;
  mylarEndCapThickness = 0.125 * CLHEP::mm;

  lh2Length = kaptonLength + inoxPieceLength;

  inoxMechSmallThickness = 5.9 * CLHEP::mm;
  inoxMechSmallLength = 40.0 * CLHEP::mm / 2;
  inoxMechLargeThickness = 100.0 * CLHEP::mm;
  inoxMechLargeLength = 32.0 * CLHEP::mm / 2;

  inoxPipeOuterRadius = 8.0 * CLHEP::mm / 2;
  inoxPipeTopLength = 458.0 * CLHEP::mm / 2;
  inoxPipeBottomLength = 486.0 * CLHEP::mm / 2;

  targetCellRotation = NULL;
  vacuum_tubs = NULL;
  vacuum_log = NULL;
  vacuumRot_tubs = NULL;
  vacuumRot_log = NULL;
  carbon_tubs = NULL;
  carbon_log = NULL;
  epoxy_tubs = NULL;
  epoxy_log = NULL;
  aluminium_tubs = NULL;
  aluminium_log = NULL;
  silver_tubs = NULL;
  silver_log = NULL;
  rohacell_tubs = NULL;
  rohacell_log = NULL;
  carbonDown_tubs = NULL;
  carbonDown_log = NULL;
  carbonDownEpoxy_tubs = NULL;
  carbonDownEpoxy_log = NULL;
  carbonDownVaccum_tubs = NULL;
  carbonDownVaccum_log = NULL;
  mylarWindow_tubs = NULL;
  mylarWindow_log = NULL;
  kapton_tubs = NULL;
  kapton_log = NULL;
  lh2_tubs = NULL;
  lh2_log = NULL;
  inoxSuperPosUpDown_tubs = NULL;
  inoxSuperPosUpDown_log = NULL;
  inoxSuperPosMid_tubs = NULL;
  inoxSuperPosMid_log = NULL;
  inoxPiece_tubs = NULL;
  inoxPiece_log = NULL;
  inoxPieceMid_tubs = NULL;
  inoxPieceMid_log = NULL;
  rotateDown = NULL;
  rotateUp = NULL;
  mylarEndCap_sphere = NULL;
  mylarEndCap_Up_log = NULL;
  mylarEndCap_Dn_log = NULL;
  mylarEndCapLH2_sphere = NULL;
  mylarEndCapLH2_Up_log = NULL;
  mylarEndCapLH2_Dn_log = NULL;
  inoxMechSmall_tubs = NULL;
  inoxMechSmall_log = NULL;
  inoxMechLarge_tubs = NULL;
  inoxMechLarge_log = NULL;
  inoxPipeTop_tubs = NULL;
  inoxPipeTop_log = NULL;
  inoxPipeBottom_tubs = NULL;
  inoxPipeBottom_log = NULL;

  //additional parameters for target
  setDimension(positionVector.z() - 1.1* lh2Length, positionVector.z() + 1.1* lh2Length);
//   setELossParams(8.0 * CLHEP::MeV, 0.03176 * CLHEP::MeV / CLHEP::mm);
  setELossParams(7.0 * CLHEP::MeV, 0.028 * CLHEP::MeV / CLHEP::mm);
}

// positioVector = middle of lh2 tube
void T4LH2Target::construct(G4LogicalVolume* world_log)
{
  G4double posToEnd = lh2Length + mylarEndCapRadius + downstreamHole;

  vacuum_tubs = new G4Tubs("vacuum_tubs", 0, vacuumOuterRadius, vacuumLenght, 0,
      360 * CLHEP::deg);
  vacuum_log = new G4LogicalVolume(vacuum_tubs, materials->vacuum_noOptical,
      "vacuum_log");
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, 0, posToEnd - vacuumLenght), vacuum_log,
      "vacuum_phys", world_log, 0, 0, checkOverlap);
  vacuum_log->SetVisAttributes(colour->invisible);

  targetCellRotation = new G4RotationMatrix();
  if (yearSetup == 2012) {
    targetCellRotation->rotateX(-0.063 * CLHEP::deg);
  }
  // extrap: y=-0.46cm @ z=-300cm and y=-0.68cm @ z=-100cm
  // position: y=-0.575cm and x=0.022ch

  vacuumRot_tubs = new G4Tubs("vacuumRot_tubs", 0,
      inoxSuperPosUpDownInnerRadius + inoxPieceMidThickness,
      lh2Length + mylarEndCapRadius, 0, 360 * CLHEP::deg);
  vacuumRot_log = new G4LogicalVolume(vacuumRot_tubs,
      materials->vacuum_noOptical, "vacuumRot_log");
  new G4PVPlacement(targetCellRotation,
      G4ThreeVector(0, 0, vacuumLenght - posToEnd), vacuumRot_log,
      "vacuumRot_phys", vacuum_log, 0, 0, checkOverlap);
  vacuumRot_log->SetVisAttributes(colour->invisible);

  if (useMechanicalStructure) {
    carbon_tubs = new G4Tubs("carbon_tubs", vacuumOuterRadius + epoxyThickness,
        vacuumOuterRadius + epoxyThickness + carbonThickness, carbonLength, 0,
        360 * CLHEP::deg);
    carbon_log = new G4LogicalVolume(carbon_tubs, materials->carbonLH2,
        "carbon_log");
    new G4PVPlacement(0,
        positionVector + G4ThreeVector(0, 0, posToEnd - carbonLength),
        carbon_log, "carbon_phys", world_log, 0, 0, checkOverlap);
    carbon_log->SetVisAttributes(colour->blue);

    epoxy_tubs = new G4Tubs("epoxy_tubs", vacuumOuterRadius,
        vacuumOuterRadius + epoxyThickness, carbonLength, 0, 360 * CLHEP::deg);
    epoxy_log = new G4LogicalVolume(epoxy_tubs, materials->epoxy, "epoxy_log");
    new G4PVPlacement(0,
        positionVector + G4ThreeVector(0, 0, posToEnd - carbonLength),
        epoxy_log, "epoxy_phys", world_log, 0, 0, checkOverlap);
    epoxy_log->SetVisAttributes(colour->darkblue);

    carbonDown_tubs = new G4Tubs("carbonDown_tubs", 0,
        carbonDownInnerRadius + carbonDownThickness, carbonDownLength, 0,
        360 * CLHEP::deg);
    carbonDown_log = new G4LogicalVolume(carbonDown_tubs, materials->carbonLH2,
        "carbonDown_log");
    new G4PVPlacement(0,
        positionVector + G4ThreeVector(0, 0, posToEnd + carbonDownLength),
        carbonDown_log, "carbonDown_phys", world_log, 0, 0, checkOverlap);
    carbonDown_log->SetVisAttributes(colour->blue);

    carbonDownEpoxy_tubs = new G4Tubs("carbonDownEpoxy_tubs", 0,
        carbonDownInnerRadius + 2. * epoxyThickness, carbonDownLength, 0,
        360 * CLHEP::deg);
    carbonDownEpoxy_log = new G4LogicalVolume(carbonDownEpoxy_tubs,
        materials->epoxy, "carbonDownEpoxy_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), carbonDownEpoxy_log,
        "carbonDownEpoxy_phys", carbonDown_log, 0, 0, checkOverlap);
    carbonDownEpoxy_log->SetVisAttributes(colour->invisible);

    carbonDownVaccum_tubs = new G4Tubs("carbonDownVaccum_tubs", 0,
        carbonDownInnerRadius, carbonDownLength, 0, 360 * CLHEP::deg);
    carbonDownVaccum_log = new G4LogicalVolume(carbonDownVaccum_tubs,
        materials->vacuum_noOptical, "carbonDownVaccum_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), carbonDownVaccum_log,
        "carbonDownVaccum_phys", carbonDownEpoxy_log, 0, 0, checkOverlap);
    carbonDownVaccum_log->SetVisAttributes(colour->invisible);

    mylarWindow_tubs = new G4Tubs("mylarWindow_tubs", 0, carbonDownInnerRadius,
        mylarWindowLength, 0, 360 * CLHEP::deg);
    mylarWindow_log = new G4LogicalVolume(mylarWindow_tubs, materials->mylar,
        "mylarWindow_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), mylarWindow_log,
        "mylarWindow_phys", carbonDownVaccum_log, 0, 0, checkOverlap);
    mylarWindow_log->SetVisAttributes(colour->invisible);

    silver_tubs = new G4Tubs("silver_tubs", vacuumOuterRadius - silverThickness,
        vacuumOuterRadius, carbonLength, 0, 360 * CLHEP::deg);
    silver_log = new G4LogicalVolume(silver_tubs, materials->silver,
        "silver_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, vacuumLenght - carbonLength),
        silver_log, "silver_phys", vacuum_log, 0, 0, checkOverlap);
    silver_log->SetVisAttributes(colour->silver);

    aluminium_tubs = new G4Tubs("aluminium_tubs", aluminiumInnerRadius,
        aluminiumInnerRadius + aluminiumThickness, aluminiumLength, 0,
        360 * CLHEP::deg);
    aluminium_log = new G4LogicalVolume(aluminium_tubs,
        materials->aluminium_noOptical, "aluminium_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, vacuumLenght - aluminiumLength),
        aluminium_log, "aluminium_phys", vacuum_log, 0, 0, checkOverlap);
    aluminium_log->SetVisAttributes(colour->silver);

    rohacell_tubs = new G4Tubs("rohacell_tubs",
        vacuumOuterRadius - silverThickness - rohacellThickness,
        vacuumOuterRadius - silverThickness, rohacellLength, 0,
        360 * CLHEP::deg);
    rohacell_log = new G4LogicalVolume(rohacell_tubs, materials->rohacell,
        "rohacell_log");

    // I think these aren't the exact positions!
    G4double vacPosToInnoxBegin = vacuumLenght - 2. * carbonLength
        + 54. * CLHEP::mm;
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, vacPosToInnoxBegin + 1. * 499 * CLHEP::mm),
        rohacell_log, "rohacell1_phys", vacuum_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, vacPosToInnoxBegin + 2. * 499 * CLHEP::mm),
        rohacell_log, "rohacell2_phys", vacuum_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, vacPosToInnoxBegin + 3. * 499 * CLHEP::mm),
        rohacell_log, "rohacell3_phys", vacuum_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, vacPosToInnoxBegin + 4. * 499 * CLHEP::mm),
        rohacell_log, "rohacell4_phys", vacuum_log, 0, 0, checkOverlap);
    rohacell_log->SetVisAttributes(colour->red);

    kapton_tubs = new G4Tubs("kapton_tubs", lh2Radius,
        lh2Radius + kaptonThickness, kaptonLength, 0, 360 * CLHEP::deg);
    kapton_log = new G4LogicalVolume(kapton_tubs, materials->kapton,
        "kapton_log");
    new G4PVPlacement(0, G4ThreeVector(0, 0, lh2Length - kaptonLength),
        kapton_log, "kapton_phys", vacuumRot_log, 0, 0, checkOverlap);
    kapton_log->SetVisAttributes(colour->blue);

    inoxSuperPosUpDown_tubs = new G4Tubs("inoxSuperPosUpDown_tubs",
        inoxSuperPosUpDownInnerRadius,
        inoxSuperPosUpDownInnerRadius + inoxThickness, inoxSuperPosUpDownLength,
        0, 360 * CLHEP::deg);
    inoxSuperPosUpDown_log = new G4LogicalVolume(inoxSuperPosUpDown_tubs,
        materials->inox, "inoxSuperPosUpDown_log");
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, lh2Length + inoxSuperPosUpDownLength),
        inoxSuperPosUpDown_log, "inoxSuperPosDown_phys", vacuumRot_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, -lh2Length - inoxSuperPosUpDownLength),
        inoxSuperPosUpDown_log, "inoxSuperPosUp_phys", vacuumRot_log, 0, 0,
        checkOverlap);
    inoxSuperPosUpDown_log->SetVisAttributes(colour->black);

    inoxSuperPosMid_tubs = new G4Tubs("inoxSuperPosMid_tubs",
        inoxSuperPosMidInnerRadius, inoxSuperPosMidInnerRadius + inoxThickness,
        inoxSuperPosMidLength, 0, 360 * CLHEP::deg);
    inoxSuperPosMid_log = new G4LogicalVolume(inoxSuperPosMid_tubs,
        materials->inox, "inoxSuperPosMid_log");
    new G4PVPlacement(0,
        G4ThreeVector(0, 0,
            lh2Length - 2. * kaptonLength + inoxSuperPosMidLength),
        inoxSuperPosMid_log, "inoxSuperPosMid_phys", vacuumRot_log, 0, 0,
        checkOverlap);
    inoxSuperPosMid_log->SetVisAttributes(colour->black);

    inoxPiece_tubs = new G4Tubs("inoxPiece_tubs", inoxPieceInnerRadius,
        inoxPieceInnerRadius + inoxThickness, inoxPieceLength, 0,
        360 * CLHEP::deg);
    inoxPiece_log = new G4LogicalVolume(inoxPiece_tubs, materials->inox,
        "inoxPiece_log");
    new G4PVPlacement(0,
        G4ThreeVector(0, 0, lh2Length - 2. * kaptonLength - inoxPieceLength),
        inoxPiece_log, "inoxPiece_phys", vacuumRot_log, 0, 0, checkOverlap);
    inoxPiece_log->SetVisAttributes(colour->black);

    inoxPieceMid_tubs = new G4Tubs("inoxPieceMid_tubs",
        inoxPieceInnerRadius + inoxThickness,
        inoxPieceInnerRadius + inoxPieceMidThickness, inoxPieceMidLength, 0,
        360 * CLHEP::deg);
    inoxPieceMid_log = new G4LogicalVolume(inoxPieceMid_tubs, materials->inox,
        "inoxPieceMid_log");
    new G4PVPlacement(0,
        G4ThreeVector(0, 0,
            lh2Length - 2. * kaptonLength - 54.0 * CLHEP::mm
                - inoxPieceMidLength), inoxPieceMid_log, "inoxPieceMid_phys",
        vacuumRot_log, 0, 0, checkOverlap);
    inoxPieceMid_log->SetVisAttributes(colour->black);

    mylarEndCap_sphere = new G4Sphere("mylarEndCap_sphere", 0,
        mylarEndCapRadius, 0 * CLHEP::deg, 180 * CLHEP::deg, 0 * CLHEP::deg,
        180 * CLHEP::deg);
    mylarEndCapLH2_sphere = new G4Sphere("mylarEndCapLH2_sphere", 0,
        mylarEndCapRadius - mylarEndCapThickness, 0 * CLHEP::deg,
        180 * CLHEP::deg, 0 * CLHEP::deg, 180 * CLHEP::deg);

    mylarEndCap_Dn_log = new G4LogicalVolume(mylarEndCap_sphere,
        materials->mylar, "mylarEndCap_Dn_log");
    rotateDown = new G4RotationMatrix();
    rotateDown->rotateX(-90 * CLHEP::deg);
    new G4PVPlacement(rotateDown, G4ThreeVector(0, 0, lh2Length),
        mylarEndCap_Dn_log, "mylarEndCapDown_phys", vacuumRot_log, 0, 0,
        checkOverlap);
    mylarEndCap_Dn_log->SetVisAttributes(colour->red);

    mylarEndCapLH2_Dn_log = new G4LogicalVolume(mylarEndCapLH2_sphere,
        materials->lh2, "mylarEndCapLH2_Dn_log");
    addToTarget(
        new G4PVPlacement(0, G4ThreeVector(0, 0, 0), mylarEndCapLH2_Dn_log,
            "TARGET_p", mylarEndCap_Dn_log, 0, 0, checkOverlap));
    mylarEndCapLH2_Dn_log->SetVisAttributes(colour->red);

    mylarEndCap_Up_log = new G4LogicalVolume(mylarEndCap_sphere,
        materials->mylar, "mylarEndCap_Up_log");
    rotateUp = new G4RotationMatrix();
    rotateUp->rotateX(90 * CLHEP::deg);
    new G4PVPlacement(rotateUp, G4ThreeVector(0, 0, -lh2Length),
        mylarEndCap_Up_log, "mylarEndCap_Up_phys", vacuumRot_log, 0, 0,
        checkOverlap);
    mylarEndCap_Up_log->SetVisAttributes(colour->red);

    mylarEndCapLH2_Up_log = new G4LogicalVolume(mylarEndCapLH2_sphere,
        materials->lh2, "mylarEndCapLH2_Up_log");
    addToTarget(
        new G4PVPlacement(0, G4ThreeVector(0, 0, 0), mylarEndCapLH2_Up_log,
            "TARGET_p", mylarEndCap_Up_log, 0, 0, checkOverlap));
    mylarEndCapLH2_Up_log->SetVisAttributes(colour->red);

    inoxMechSmall_tubs = new G4Tubs("inoxMechSmall_tubs",
        vacuumOuterRadius + carbonThickness + epoxyThickness,
        vacuumOuterRadius + carbonThickness + epoxyThickness
            + inoxMechSmallThickness, inoxMechSmallLength, 0, 360 * CLHEP::deg);
    inoxMechSmall_log = new G4LogicalVolume(inoxMechSmall_tubs, materials->inox,
        "inoxMechSmall_log");
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(0, 0,
                posToEnd - 2. * carbonLength + 54. * CLHEP::mm
                    - inoxMechSmallLength), inoxMechSmall_log,
        "inoxMechSmall_phys", world_log, 0, 0, checkOverlap);
    inoxMechSmall_log->SetVisAttributes(colour->black);

    inoxMechLarge_tubs = new G4Tubs("inoxMechLarge_tubs",
        vacuumOuterRadius + carbonThickness + epoxyThickness,
        vacuumOuterRadius + carbonThickness + epoxyThickness
            + inoxMechLargeThickness, inoxMechLargeLength, 0, 360 * CLHEP::deg);
    inoxMechLarge_log = new G4LogicalVolume(inoxMechLarge_tubs, materials->inox,
        "inoxMechLarge_log");
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(0, 0,
                posToEnd - 2. * carbonLength + 54. * CLHEP::mm
                    - 2. * inoxMechSmallLength - inoxMechLargeLength),
        inoxMechLarge_log, "inoxMechLarge_phys", world_log, 0, 0, checkOverlap);
    inoxMechLarge_log->SetVisAttributes(colour->black);

    inoxPipeTop_tubs = new G4Tubs("inoxPipeTop_tubs",
        inoxPipeOuterRadius - inoxThickness, inoxPipeOuterRadius,
        inoxPipeTopLength, 0, 360 * CLHEP::deg);
    inoxPipeTop_log = new G4LogicalVolume(inoxPipeTop_tubs, materials->inox,
        "inoxPipeTop_log");
    new G4PVPlacement(0,
        G4ThreeVector(0,
            inoxPieceInnerRadius + inoxPieceMidThickness + 10. * CLHEP::mm
                - inoxPipeOuterRadius,
            vacPosToInnoxBegin - 54.0 * CLHEP::mm - inoxPieceMidLength
                - inoxPipeTopLength), inoxPipeTop_log, "inoxPipeTop_phys",
        vacuum_log, 0, 0, checkOverlap);
    inoxPipeTop_log->SetVisAttributes(colour->black);

    inoxPipeBottom_tubs = new G4Tubs("inoxPipeBottom_tubs",
        inoxPipeOuterRadius - inoxThickness, inoxPipeOuterRadius,
        inoxPipeBottomLength, 0, 360 * CLHEP::deg);
    inoxPipeBottom_log = new G4LogicalVolume(inoxPipeBottom_tubs,
        materials->inox, "inoxPipeBottom_log");
    new G4PVPlacement(0,
        G4ThreeVector(0,
            -inoxPieceInnerRadius - inoxPieceMidThickness - 10. * CLHEP::mm
                + inoxPipeOuterRadius,
            vacPosToInnoxBegin - 54.0 * CLHEP::mm - inoxPieceMidLength
                - inoxPipeBottomLength), inoxPipeBottom_log,
        "inoxPipeBottom_phys", vacuum_log, 0, 0, checkOverlap);
    inoxPipeBottom_log->SetVisAttributes(colour->black);
  }

  lh2_tubs = new G4Tubs("lh2_tubs", 0, lh2Radius, lh2Length, 0,
      360 * CLHEP::deg);
  lh2_log = new G4LogicalVolume(lh2_tubs, materials->lh2, "lh2_log");
  addToTarget(
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), lh2_log, "TARGET_p",
          vacuumRot_log, 0, 0, checkOverlap));
  lh2_log->SetVisAttributes(colour->red);

  setUserLimits();
}

void T4LH2Target::setUserLimits(void)
{
  regionManager->addToTargetRegion(vacuum_log);
  if (useMechanicalStructure) {
    regionManager->addToTargetRegion(carbon_log);
    regionManager->addToTargetRegion(epoxy_log);
    regionManager->addToTargetRegion(carbonDown_log);
    regionManager->addToTargetRegion(mylarWindow_log);
    regionManager->addToTargetRegion(inoxMechSmall_log);
    regionManager->addToTargetRegion(inoxMechLarge_log);
  }
}

void T4LH2Target::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.name = "T001";
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = lh2Radius;
  target.ySize = lh2Length * 2.;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();
  target.zCen = positionVector.getZ();

  targetInformation.push_back(target);
}


double T4LH2Target::getRndmTargetDist(double maxTargetLength) 
{
  if (maxTargetLength == 0)
    return -1;
  
  if (yearSetup == 2012)
    return maxTargetLength * T4SettingsFile::getInstance()->getRandom()->flat();
  else
    return (lh2Length + mylarEndCapRadius) * 2. * T4SettingsFile::getInstance()->getRandom()->flat();
}

T4LH2Target::~T4LH2Target(void)
{
  if (targetCellRotation != NULL)
    delete targetCellRotation;
  if (vacuum_tubs != NULL)
    delete vacuum_tubs;
  if (vacuum_log != NULL)
    delete vacuum_log;
  if (vacuumRot_tubs != NULL)
    delete vacuumRot_tubs;
  if (vacuumRot_log != NULL)
    delete vacuumRot_log;
  if (carbon_tubs != NULL)
    delete carbon_tubs;
  if (carbon_log != NULL)
    delete carbon_log;
  if (epoxy_tubs != NULL)
    delete epoxy_tubs;
  if (epoxy_log != NULL)
    delete epoxy_log;
  if (aluminium_tubs != NULL)
    delete aluminium_tubs;
  if (aluminium_log != NULL)
    delete aluminium_log;
  if (silver_tubs != NULL)
    delete silver_tubs;
  if (silver_log != NULL)
    delete silver_log;
  if (rohacell_tubs != NULL)
    delete rohacell_tubs;
  if (rohacell_log != NULL)
    delete rohacell_log;
  if (carbonDown_tubs != NULL)
    delete carbonDown_tubs;
  if (carbonDown_log != NULL)
    delete carbonDown_log;
  if (carbonDownEpoxy_tubs != NULL)
    delete carbonDownEpoxy_tubs;
  if (carbonDownEpoxy_log != NULL)
    delete carbonDownEpoxy_log;
  if (carbonDownVaccum_tubs != NULL)
    delete carbonDownVaccum_tubs;
  if (carbonDownVaccum_log != NULL)
    delete carbonDownVaccum_log;
  if (mylarWindow_tubs != NULL)
    delete mylarWindow_tubs;
  if (mylarWindow_log != NULL)
    delete mylarWindow_log;
  if (kapton_tubs != NULL)
    delete kapton_tubs;
  if (kapton_log != NULL)
    delete kapton_log;
  if (lh2_tubs != NULL)
    delete lh2_tubs;
  if (lh2_log != NULL)
    delete lh2_log;
  if (inoxSuperPosUpDown_tubs != NULL)
    delete inoxSuperPosUpDown_tubs;
  if (inoxSuperPosUpDown_log != NULL)
    delete inoxSuperPosUpDown_log;
  if (inoxSuperPosMid_tubs != NULL)
    delete inoxSuperPosMid_tubs;
  if (inoxSuperPosMid_log != NULL)
    delete inoxSuperPosMid_log;
  if (inoxPiece_tubs != NULL)
    delete inoxPiece_tubs;
  if (inoxPiece_log != NULL)
    delete inoxPiece_log;
  if (inoxPieceMid_tubs != NULL)
    delete inoxPieceMid_tubs;
  if (inoxPieceMid_log != NULL)
    delete inoxPieceMid_log;
  if (rotateDown != NULL)
    delete rotateDown;
  if (rotateUp != NULL)
    delete rotateUp;
  if (mylarEndCap_sphere != NULL)
    delete mylarEndCap_sphere;
  if (mylarEndCap_Up_log != NULL)
    delete mylarEndCap_Up_log;
  if (mylarEndCap_Dn_log != NULL)
    delete mylarEndCap_Dn_log;
  if (mylarEndCapLH2_sphere != NULL)
    delete mylarEndCapLH2_sphere;
  if (mylarEndCapLH2_Up_log != NULL)
    delete mylarEndCapLH2_Up_log;
  if (mylarEndCapLH2_Dn_log != NULL)
    delete mylarEndCapLH2_Dn_log;
  if (inoxMechSmall_tubs != NULL)
    delete inoxMechSmall_tubs;
  if (inoxMechSmall_log != NULL)
    delete inoxMechSmall_log;
  if (inoxMechLarge_tubs != NULL)
    delete inoxMechLarge_tubs;
  if (inoxMechLarge_log != NULL)
    delete inoxMechLarge_log;
  if (inoxPipeTop_tubs != NULL)
    delete inoxPipeTop_tubs;
  if (inoxPipeTop_log != NULL)
    delete inoxPipeTop_log;
  if (inoxPipeBottom_tubs != NULL)
    delete inoxPipeBottom_tubs;
  if (inoxPipeBottom_log != NULL)
    delete inoxPipeBottom_log;
}
