#ifndef T4DYTARGET_HH_
#define T4DYTARGET_HH_

#include "T4BaseDetector.hh"
#include "T4ODDipoleField.hh"
#include "T4TargetBackend.hh"
#include "G4Tubs.hh"

class T4DYTarget : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4DYTarget(T4SDetector*);
    virtual ~T4DYTarget(void);

    void construct(G4LogicalVolume*);

    void getTargetDetDat(std::vector<T4STargetInformation>&);
    
  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    vector<G4Sphere*> sphere;

    G4double zSol[4];
    G4double SolRay[4];
    G4double sphRay[2];
    G4double sphAng[4];
    G4double AlMlRay[3];
    G4double zAlMl;
    G4double cavityPar[3];
    G4double cavityWin[3];
    G4double mixPar[3];
    G4double wi16Par[4];
    G4double foilPar[4];
    G4double kevlarPar[3];
    G4double lHePar[4];
    G4double copPar[6];
    G4double mstPar[3];
    G4double meshPar[3];
    G4double shieldPar[2];
    G4double nh3hold[2];

    G4double lheLength;
    G4double lheRadius;
    G4double lhePositionShift;
    G4double nh3Length;
    G4double nh3Radius;
    G4double cellGap;
    //Riccardo's addition
    G4double targetWeights[4]; //Weights for three different targets
    G4double targetRelWeights[3]; //Weights normalized to 1
    G4double zTargetExtremes[4]; //TODO: find a way to pass this parameter by code and not by hand
    G4double targetLine[4]; //To weight in right way
    G4double weightPos[2];
};

#endif /* T4DYTARGET_HH_ */
