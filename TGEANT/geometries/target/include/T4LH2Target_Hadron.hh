#ifndef T4LH2TARGET_HADRON_HH_
#define T4LH2TARGET_HADRON_HH_

#include "T4BaseDetector.hh"

#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "T4TargetBackend.hh"

class T4LH2Target_Hadron : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4LH2Target_Hadron(T4SDetector*);
    virtual ~T4LH2Target_Hadron(void);

    void construct(G4LogicalVolume*);
    
    void getTargetDetDat(std::vector<T4STargetInformation>&);

  private:    
    double lh2Length;
    double lh2Radius;
    
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
};

#endif /* T4LH2TARGET_HADRON_HH_ */
