#ifndef T4POLARIZEDTARGET_HH_
#define T4POLARIZEDTARGET_HH_

#include "T4BaseDetector.hh"
//#include "T4ODDipoleField.hh"
#include "T4TargetBackend.hh"
#include "G4Tubs.hh"

class T4PolarizedTarget : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4PolarizedTarget(T4SDetector*);
    virtual ~T4PolarizedTarget(void);

    void construct(G4LogicalVolume*);

    void getTargetDetDat(std::vector<T4STargetInformation>&);
    
    void setCavityRadius(double r) {cavityRadius = r;}
    void setCavityThickness(double d) {cavityThickness = d;}
    void setTargetRadius(double r) {targetRadius = r;}
    
    G4LogicalVolume* getVacuumLog(void) {return vacuum_log;}

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    double zSol[4];
    double SolRay[4];
    
    G4LogicalVolume* vacuum_log;
    double cavityRadius;
    double cavityThickness;
    double targetRadius;
};

#endif /* T4POLARIZEDTARGET_HH_ */
