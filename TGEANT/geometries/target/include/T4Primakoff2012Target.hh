#ifndef T4PRIMAKOFF2012TARGET_HH_
#define T4PRIMAKOFF2012TARGET_HH_

#include "T4BaseDetector.hh"
#include "T4TargetBackend.hh"

#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"

class T4Primakoff2012Target : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4Primakoff2012Target(T4SDetector*);
    virtual ~T4Primakoff2012Target(void);

    void construct(G4LogicalVolume*);

    double getRndmTargetDist(double maxTargetLength);

    void getTargetDetDat(std::vector<T4STargetInformation>&);

  private:
    double lengthNickel;
    double lengthTungsten;

    double zPosNickel;
    double zPosTungsten;

    double theta0;

    void placeTargetHolder(G4LogicalVolume*, G4ThreeVector posU, G4ThreeVector posD);
    void placeCarbonRods(G4LogicalVolume*, G4ThreeVector);
    void placeSmallRing(G4LogicalVolume*, G4ThreeVector);
    void placeSmallCarbonRods(G4LogicalVolume*, G4ThreeVector);
    void placeNickelTarget(G4LogicalVolume*, G4ThreeVector);
    void placeTungstenTarget(G4LogicalVolume*, G4ThreeVector);


    vector<G4VSolid*> solid;
    vector<G4LogicalVolume*> log;
    vector<CLHEP::HepRotation*> rot;
};

#endif /* T4PRIMAKOFF2012TARGET_HH_ */
