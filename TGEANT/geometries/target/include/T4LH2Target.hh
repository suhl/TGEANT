#ifndef T4LH2TARGET_HH_
#define T4LH2TARGET_HH_

#include "T4BaseDetector.hh"

#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "T4TargetBackend.hh"

class T4LH2Target : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4LH2Target(T4SDetector*);
    virtual ~T4LH2Target(void);

    void construct(G4LogicalVolume*);
    
    void getTargetDetDat(std::vector<T4STargetInformation>&);
    
    double getRndmTargetDist(double maxTargetLength);

  private:
    void setUserLimits(void);

    double yearSetup;
    G4RotationMatrix* targetCellRotation;

    G4double downstreamHole;
    G4double vacuumOuterRadius;
    G4double vacuumLenght;
    G4Tubs* vacuum_tubs;
    G4LogicalVolume* vacuum_log;
    G4Tubs* vacuumRot_tubs;
    G4LogicalVolume* vacuumRot_log;

    G4double carbonThickness;
    G4double carbonLength;
    G4Tubs* carbon_tubs;
    G4LogicalVolume* carbon_log;

    G4double epoxyThickness;
    G4Tubs* epoxy_tubs;
    G4LogicalVolume* epoxy_log;

    G4double aluminiumInnerRadius;
    G4double aluminiumThickness;
    G4double aluminiumLength;
    G4Tubs* aluminium_tubs;
    G4LogicalVolume* aluminium_log;

    G4double silverThickness;
    G4Tubs* silver_tubs;
    G4LogicalVolume* silver_log;

    G4double rohacellThickness;
    G4double rohacellLength;
    G4Tubs* rohacell_tubs;
    G4LogicalVolume* rohacell_log;

    G4double carbonDownInnerRadius;
    G4double carbonDownThickness;
    G4double carbonDownLength;
    G4Tubs* carbonDown_tubs;
    G4LogicalVolume* carbonDown_log;
    G4Tubs* carbonDownEpoxy_tubs;
    G4LogicalVolume* carbonDownEpoxy_log;
    G4Tubs* carbonDownVaccum_tubs;
    G4LogicalVolume* carbonDownVaccum_log;

    G4double mylarWindowLength;
    G4Tubs* mylarWindow_tubs;
    G4LogicalVolume* mylarWindow_log;

    G4double kaptonThickness;
    G4double kaptonLength;
    G4Tubs* kapton_tubs;
    G4LogicalVolume* kapton_log;

    G4double lh2Radius;
    G4double lh2Length;
    G4Tubs* lh2_tubs;
    G4LogicalVolume* lh2_log;

    G4double inoxSuperPosUpDownInnerRadius;
    G4double inoxSuperPosUpDownLength;
    G4double inoxThickness;
    G4Tubs* inoxSuperPosUpDown_tubs;
    G4LogicalVolume* inoxSuperPosUpDown_log;

    G4double inoxSuperPosMidInnerRadius;
    G4double inoxSuperPosMidLength;
    G4Tubs* inoxSuperPosMid_tubs;
    G4LogicalVolume* inoxSuperPosMid_log;

    G4double inoxPieceInnerRadius;
    G4double inoxPieceLength;
    G4Tubs* inoxPiece_tubs;
    G4LogicalVolume* inoxPiece_log;

    G4double inoxPieceMidLength;
    G4double inoxPieceMidThickness;
    G4Tubs* inoxPieceMid_tubs;
    G4LogicalVolume* inoxPieceMid_log;

    G4RotationMatrix* rotateDown;
    G4RotationMatrix* rotateUp;
    G4double mylarEndCapRadius;
    G4double mylarEndCapThickness;
    G4Sphere* mylarEndCap_sphere;
    G4LogicalVolume* mylarEndCap_Up_log;
    G4LogicalVolume* mylarEndCap_Dn_log;
    G4Sphere* mylarEndCapLH2_sphere;
    G4LogicalVolume* mylarEndCapLH2_Up_log;
    G4LogicalVolume* mylarEndCapLH2_Dn_log;

    G4double inoxMechSmallThickness;
    G4double inoxMechSmallLength;
    G4Tubs* inoxMechSmall_tubs;
    G4LogicalVolume* inoxMechSmall_log;

    G4double inoxMechLargeThickness;
    G4double inoxMechLargeLength;
    G4Tubs* inoxMechLarge_tubs;
    G4LogicalVolume* inoxMechLarge_log;

    G4double inoxPipeOuterRadius;
    G4double inoxPipeTopLength;
    G4double inoxPipeBottomLength;
    G4Tubs* inoxPipeTop_tubs;
    G4LogicalVolume* inoxPipeTop_log;
    G4Tubs* inoxPipeBottom_tubs;
    G4LogicalVolume* inoxPipeBottom_log;
};

#endif /* T4LH2TARGET_HH_ */
