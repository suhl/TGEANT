#ifndef T4WORLDCONSTRUCTION_HH_
#define T4WORLDCONSTRUCTION_HH_

#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "T4BaseDetector.hh"
#include "T4SettingsFile.hh"

#include "CameraConstruction.hh"
#include "T4LH2Target.hh"
#include "T4LH2Target_Hadron.hh"
#include "T4DYTarget.hh"
#include "T4DYAbsorber.hh"
#include "T4Primakoff2012Target.hh"
#include "T4PolarizedTarget.hh"
#include "T4SM1.hh"
#include "T4SM2.hh"
#include "MW1Construction.hh"
#include "MW2Construction.hh"
#include "MF3Construction.hh"
#include "DCConstruction.hh"
#include "StrawConstruction.hh"
#include "RichConstruction.hh"
#include "RPDConstruction.hh"
#include "T4W45.hh"
#include "H1Construction.hh"
#include "H2Construction.hh"
#include "T4HodoscopeManager.hh"
#include "T4MainzCounter.hh"
#include "T4HO03.hh"
#include "T4HO04.hh"
#include "T4BeamKiller.hh"
#include "T4SandwichVeto.hh"
#include "T4MultiplicityCounter.hh"
#include "VetoConstruction.hh"
#include "T4PolGPD.hh"
#include "T4PolGPD_SiRPD.hh"
#include "T4TransvTargetField.hh"
#include "DummyConstruction.hh"
#include "T4TestCal.hh"
#include "SciFiTest.hh"
#include "T4CaloManager.hh"
#include "T4DeathMagnetic.hh"
#include "T4SM1MagneticPlugin.hh"
#include "T4SM2MagneticPlugin.hh"
#include "T4Gem.hh"
#include "T4Micromegas.hh"
#include "T4Mwpc.hh"
#include "T4SciFi.hh"
#include "T4Silicon.hh"
#include "T4Veto.hh"
#include "T4RichWall.hh"
#include "T4WorkshopExample.hh"

class T4WorldConstruction : public G4VUserDetectorConstruction
{
  public:
    T4WorldConstruction(void);
    ~T4WorldConstruction(void);

    G4VPhysicalVolume* Construct(void);
    const std::vector<T4BaseDetector*>& getBaseDetector(void)
      {return baseDetector;};
    
  private:
    void initialize(void);

    G4double dimensionsWorld[3];
    G4Box* world_box;
    G4LogicalVolume* world_log;
    G4VPhysicalVolume* world_phys;

    std::vector<T4BaseDetector*> baseDetector;
    
    T4PartialField* getFieldByName(T4SMagnet*);
};

#endif /* T4WORLDCONSTRUCTION_HH_ */
