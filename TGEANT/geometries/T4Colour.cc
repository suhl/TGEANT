#include "T4Colour.hh"

T4Colour* T4Colour::colour = NULL;

T4Colour* T4Colour::getInstance(void)
{
  if (colour == NULL) {
    colour = new T4Colour();
  }
  return colour;
}

T4Colour::T4Colour(void)
{
  red        = new G4VisAttributes(true, G4Colour(1.0, 0.0, 0.0));
  lightred   = new G4VisAttributes(true, G4Colour(1.0, 0.2, 0.2));
  darkred    = new G4VisAttributes(true, G4Colour(0.5, 0.0, 0.0));
  orange     = new G4VisAttributes(true, G4Colour(1.0, 0.3, 0.0));
  green      = new G4VisAttributes(true, G4Colour(0.0, 1.0, 0.0));
  darkgreen  = new G4VisAttributes(true, G4Colour(0.0, 0.5, 0.0));
  lightgreen = new G4VisAttributes(true, G4Colour(0.5, 1.0, 0.5));
  blue       = new G4VisAttributes(true, G4Colour(0.0, 0.0, 1.0));
  blue_50    = new G4VisAttributes(true, G4Colour(0.0, 0.0, 1.0, 0.5));
  darkblue   = new G4VisAttributes(true, G4Colour(0.0, 0.0, 0.5));
  mediumblue = new G4VisAttributes(true, G4Colour(0.0, 0.0, 0.8));
  lightblue  = new G4VisAttributes(true, G4Colour(0.78, 0.91, 1.0));
  lightskyblue= new G4VisAttributes(true, G4Colour(0.52, 0.81, 0.98));
  deepskyblue= new G4VisAttributes(true, G4Colour(0.0, 0.75, 1.0));
  dodgerblue = new G4VisAttributes(true, G4Colour(0.12, 0.56, 1.0));
  yellow     = new G4VisAttributes(true, G4Colour(1.0, 1.0, 0.0));
  yellow_50  = new G4VisAttributes(true, G4Colour(1.0, 0.91, 0.13, 0.3));
  darkyellow = new G4VisAttributes(true, G4Colour(1.0, 0.81, 0.23));
  deepyellow = new G4VisAttributes(true, G4Colour(0.5, 0.5, 0.0));
  lightyellow= new G4VisAttributes(true, G4Colour(0.98, 1.0, 0.4));
  magenta    = new G4VisAttributes(true, G4Colour(1.0, 0.0, 1.0));
  magenta_20 = new G4VisAttributes(true, G4Colour(1.0, 0.0, 1.0, 0.2));
  cyan       = new G4VisAttributes(true, G4Colour(0.0, 1.0, 1.0));
  darkcyan   = new G4VisAttributes(true, G4Colour(0.0, 0.8, 0.8));
  deepcyan   = new G4VisAttributes(true, G4Colour(0.0, 0.5, 0.5));
  gray       = new G4VisAttributes(true, G4Colour(0.8, 0.8, 0.8));
  gray_50    = new G4VisAttributes(true, G4Colour(0.8, 0.8, 0.8, 0.5));
  lightgray  = new G4VisAttributes(true, G4Colour(0.9, 0.9, 0.9));
  darkgray   = new G4VisAttributes(true, G4Colour(0.5, 0.5, 0.5));
  darkgray_50= new G4VisAttributes(true, G4Colour(0.5, 0.5, 0.5, 0.5));
  deepgray   = new G4VisAttributes(true, G4Colour(0.3, 0.3, 0.3));
  floorcolor = new G4VisAttributes(true, G4Colour(0.5, 0.4, 0.2));
  white      = new G4VisAttributes(true, G4Colour(1.0, 1.0, 1.0));
  black      = new G4VisAttributes(true, G4Colour(0.0, 0.0, 0.0));
  black_20   = new G4VisAttributes(true, G4Colour(0.0, 0.0, 0.0, 0.2));
  invisible  = new G4VisAttributes(false);
  silver     = new G4VisAttributes(true, G4Colour(0.75294,0.75294,0.75294));
  silver_20  = new G4VisAttributes(true, G4Colour(0.75294,0.75294,0.75294, 0.2));
  violet     = new G4VisAttributes(true, G4Colour(0.93,0.51,0.93));
  mediumpurple= new G4VisAttributes(true, G4Colour(0.62,0.61,0.95));
  
  red        ->SetForceSolid(true);
  lightred   ->SetForceSolid(true);
  darkred    ->SetForceSolid(true);
  orange     ->SetForceSolid(true);
  green      ->SetForceSolid(true);
  darkgreen  ->SetForceSolid(true);
  lightgreen ->SetForceSolid(true);
  blue       ->SetForceSolid(true);
  blue_50    ->SetForceSolid(true);
  darkblue   ->SetForceSolid(true);
  mediumblue ->SetForceSolid(true);
  lightskyblue->SetForceSolid(true);
  deepskyblue->SetForceSolid(true);
  dodgerblue ->SetForceSolid(true);
  lightblue  ->SetForceSolid(true);
  yellow     ->SetForceSolid(true);
  yellow_50  ->SetForceSolid(true);
  darkyellow ->SetForceSolid(true);
  deepyellow ->SetForceSolid(true);
  lightyellow->SetForceSolid(true);
  magenta    ->SetForceSolid(true);
  magenta_20 ->SetForceSolid(true);
  cyan       ->SetForceSolid(true);
  darkcyan   ->SetForceSolid(true);
  deepcyan   ->SetForceSolid(true);
  gray       ->SetForceSolid(true);
  gray_50    ->SetForceSolid(true);
  lightgray  ->SetForceSolid(true);
  darkgray   ->SetForceSolid(true);
  darkgray_50->SetForceSolid(true);
  deepgray   ->SetForceSolid(true);
  floorcolor ->SetForceSolid(true);
  white      ->SetForceSolid(true);
  black      ->SetForceSolid(true);
  black_20   ->SetForceSolid(true);
  invisible  ->SetForceSolid(true);
  silver     ->SetForceSolid(true);
  silver_20  ->SetForceSolid(true);
  violet     ->SetForceSolid(true);
  mediumpurple->SetForceSolid(true);
}

T4Colour::~T4Colour(void)
{
  delete red;
  delete lightred;
  delete darkred;
  delete orange;
  delete green;
  delete darkgreen;
  delete lightgreen;
  delete blue;
  delete blue_50;
  delete darkblue;
  delete lightblue;
  delete mediumblue;
  delete lightskyblue;
  delete deepskyblue;
  delete dodgerblue;
  delete yellow;
  delete yellow_50;
  delete darkyellow;
  delete deepyellow;
  delete lightyellow;
  delete magenta;
  delete magenta_20;
  delete cyan;
  delete darkcyan;
  delete deepcyan;
  delete gray;
  delete gray_50;
  delete lightgray;
  delete darkgray;
  delete darkgray_50;
  delete deepgray;
  delete floorcolor;
  delete white;
  delete black;
  delete black_20;
  delete invisible;
  delete silver;
  delete silver_20;
  delete violet;
  delete mediumpurple;
}
