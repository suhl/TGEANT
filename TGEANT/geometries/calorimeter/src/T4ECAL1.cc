#include "T4ECAL1.hh"

void T4ECAL1::constructRegions(T4SCAL* _scalIn)
{
  G4double moduleDistanceX = 3.830 * CLHEP::cm;
  G4double moduleDistanceY = 3.830 * CLHEP::cm;

  G4double zSize = 45.0 / 2 * CLHEP::cm;
  G4double posZ = _scalIn->position[2];

  // This value is for MAINZ, OLGA and Shashlik modules:
  // In detectors.dat the given z size is bigger than the length of the modules.
  // So we have to subtract this difference to keep the correct z positioning.
  // Second number for Shashlik:
  // this positionOffset value defines the difference between the z-position
  // of the center of sensitive detector volume and the hull box
  // positionOffset = dimensionHull[2] - thicknessHull - scintillatorDimensions[2]
  G4double zDifference = 0;

  TGEANT::T4CaloModule type;
  if (_scalIn->moduleName == "GAMS") {
    type = TGEANT::GAMS;
  } else if (_scalIn->moduleName == "MAINZ") {
    type = TGEANT::MAINZ;
    moduleDistanceX = 7.660 * CLHEP::cm;
    moduleDistanceY = 7.500 * CLHEP::cm;
    zDifference = -1.3 * CLHEP::cm;
    zSize = 36.0 / 2. * CLHEP::cm;
  } else if (_scalIn->moduleName == "OLGA") {
    type = TGEANT::OLGA;
    moduleDistanceX = moduleDistanceY = 14.300 * CLHEP::cm;
    zDifference = -5.0 * CLHEP::cm;
    zSize = 47.0 / 2. * CLHEP::cm;
  } else if (_scalIn->moduleName == "SHASHLIK") {
    type = TGEANT::SHASHLIK;
    zDifference = -1.85 * CLHEP::cm + 4.67 * CLHEP::cm;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
    "T4ECAL1::constructRegions: Unknown Module name: '" + _scalIn->moduleName + "'. Skip this entry! Please check your calo.xml file!");
    return;
  }

  posZ = _scalIn->position[2] + zDifference;
  buildRegionBox(type, _scalIn, "ECAL1", zSize, moduleDistanceX,
      moduleDistanceY, posZ);

  modulePositioning(type, _scalIn, moduleDistanceX, moduleDistanceY, posZ);
}

void T4ECAL1::constructMechanicalStructure()
{
  structureSize[0] = 4.00 / 2 * CLHEP::m;
  structureSize[1] = 3.00 / 2 * CLHEP::m;

  platformDimensions[0] = 5.0 * CLHEP::m;
  platformDimensions[1] = 50.0 * CLHEP::cm;
  platformDimensions[2] = 0.6 * CLHEP::m;

  wheelDimensions[0] = 20.0 * CLHEP::cm;
  wheelDimensions[1] = 10.0 * CLHEP::cm;

  frameSize[0] = 5.0 * CLHEP::cm;
  frameSize[1] = 10.0 * CLHEP::cm;

  //platform:
  platform_box[0] = new G4Box("", platformDimensions[0] / 2,
      (platformDimensions[1] - wheelDimensions[0]) / 2,
      platformDimensions[2] / 2);
  platform_box[1] = new G4Box("", 0.9 * platformDimensions[0] / 2,
      wheelDimensions[0] / 2, (platformDimensions[2] - wheelDimensions[1]) / 2);
  platform_geo = new G4UnionSolid("platform_geo", platform_box[0], platform_box[1], 0,
      G4ThreeVector(0,
          -((platformDimensions[1] - wheelDimensions[0]) / 2
              + wheelDimensions[0] / 2), 0));
  platform_log = new G4LogicalVolume(platform_geo, materials->aluminium_noOptical, "platform_log", 0,
      0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              -structureSize[1]
                  - (platformDimensions[1] - wheelDimensions[0]) / 2,
              5 * CLHEP::cm), platform_log, "", world_log, 0, 0, 0);
  platform_log->SetVisAttributes(colour->black);

  //wheels

  wheel_tubs[0] = new G4Tubs("", 0.9 * wheelDimensions[0] / 2,
      wheelDimensions[0] / 2, wheelDimensions[1] / 2, 0 * CLHEP::deg,
      360.0 * CLHEP::deg);
  wheel_tubs[1] = new G4Tubs("", 0.3 * wheelDimensions[0] / 2,
      0.9 * wheelDimensions[0] / 2, wheelDimensions[1] / 2 * 0.5,
      0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  wheel_tubs[2] = new G4Tubs("", 0.0 * CLHEP::mm, 0.3 * wheelDimensions[0] / 2,
      wheelDimensions[1] / 2, CLHEP::deg, 360.0 * CLHEP::deg);
  wheel_union = new G4UnionSolid("", wheel_tubs[0], wheel_tubs[1]);
  wheel_geo = new G4UnionSolid("", wheel_union, wheel_tubs[2]);

  for (G4int i = 0; i < 4; i++) {
    wheel_log[i] = new G4LogicalVolume(wheel_geo, materials->iron, "", 0, 0, 0,
        0);
    wheel_log[i]->SetVisAttributes(colour->black);
  }

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-platformDimensions[0] / 2 + wheelDimensions[0] / 2,
              -platformDimensions[1] + wheelDimensions[0] / 2
                  - structureSize[1],
              5.0 * CLHEP::cm - platformDimensions[2] / 2 * 0.9), wheel_log[0],
      "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-platformDimensions[0] / 2 + wheelDimensions[0] / 2,
              -platformDimensions[1] + wheelDimensions[0] / 2
                  - structureSize[1],
              5.0 * CLHEP::cm + platformDimensions[2] / 2 * 0.9), wheel_log[1],
      "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(platformDimensions[0] / 2 - wheelDimensions[0] / 2,
              -platformDimensions[1] + wheelDimensions[0] / 2
                  - structureSize[1],
              5.0 * CLHEP::cm - platformDimensions[2] / 2 * 0.9), wheel_log[2],
      "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(platformDimensions[0] / 2 - wheelDimensions[0] / 2,
              -platformDimensions[1] + wheelDimensions[0] / 2
                  - structureSize[1],
              5.0 * CLHEP::cm + platformDimensions[2] / 2 * 0.9), wheel_log[3],
      "", world_log, 0, 0, 0);

  //frame_box

  frame_box[0] = new G4Box("", frameSize[1] / 2, structureSize[1],
      frameSize[0] / 2);
  frame_box[1] = new G4Box("", structureSize[0] + frameSize[1] + 5.0 * CLHEP::cm,
      frameSize[1] / 2, frameSize[0] / 2);

  for (G4int i = 0; i < 4; i++) {
    frame_log[i] = new G4LogicalVolume(frame_box[0], materials->aluminium_noOptical, "",
        0, 0, 0, 0);
  }
  frame_log[4] = new G4LogicalVolume(frame_box[1], materials->aluminium_noOptical, "", 0,
      0, 0, 0);
  frame_log[5] = new G4LogicalVolume(frame_box[1], materials->aluminium_noOptical, "", 0,
      0, 0, 0);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(structureSize[0] + frameSize[1] / 2 + 5.0 * CLHEP::cm,
              0, frameSize[1]), frame_log[0], "", world_log, 0,
      0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-structureSize[0] - frameSize[1] / 2 - 5.0 * CLHEP::cm,
              0, frameSize[1]), frame_log[1], "", world_log, 0,
      0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(structureSize[0] + frameSize[1] / 2 + 5.0 * CLHEP::cm,
              0, 0), frame_log[2], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-structureSize[0] - frameSize[1] / 2 - 5.0 * CLHEP::cm,
              0, 0), frame_log[3], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, structureSize[1] + frameSize[1] / 2,
              frameSize[1]), frame_log[4], "", world_log, 0, 0,
      0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, structureSize[1] + frameSize[1] / 2,
              0), frame_log[5], "", world_log, 0, 0, 0);

  for (G4int i = 0; i < 6; i++) {
    frame_log[i]->SetVisAttributes(colour->black);
  }
}

T4ECAL1::T4ECAL1(vector<T4SCAL>& _in, const T4SDetector* _inDet) :
    T4CalorimeterBackend(_in, _inDet)
{
  caloName = "ECAL1";

  platform_box[0] = NULL;
  platform_box[1] = NULL;
  platform_geo = NULL;
  platform_log = NULL;
  wheel_tubs[0] = NULL;
  wheel_tubs[1] = NULL;
  wheel_tubs[2] = NULL;
  wheel_union = NULL;
  wheel_geo = NULL;
  wheel_log[0] = NULL;
  wheel_log[1] = NULL;
  wheel_log[2] = NULL;
  wheel_log[3] = NULL;
  frame_box[0] = NULL;
  frame_box[1] = NULL;
  frame_log[0] = NULL;
  frame_log[1] = NULL;
  frame_log[2] = NULL;
  frame_log[3] = NULL;
  frame_log[4] = NULL;
  frame_log[5] = NULL;
}

T4ECAL1::~T4ECAL1(void)
{
  if (useMechanicalStructure) {
    delete platform_box[0];
    delete platform_box[1];
    delete platform_geo;
    delete platform_log;

    delete wheel_tubs[0];
    delete wheel_tubs[1];
    delete wheel_tubs[2];
    delete wheel_union;
    delete wheel_geo;
    delete wheel_log[0];
    delete wheel_log[1];
    delete wheel_log[2];
    delete wheel_log[3];

    delete frame_box[0];
    delete frame_box[1];
    delete frame_log[0];
    delete frame_log[1];
    delete frame_log[2];
    delete frame_log[3];
    delete frame_log[4];
    delete frame_log[5];
  }
}
