#include "T4ECAL0.hh"

void T4ECAL0::constructRegions(T4SCAL* _scalIn)
{
  G4double moduleDistance = 4.000 * CLHEP::cm;
  G4double zSize = 34.17 / 2 * CLHEP::cm;
  // this positionOffset value defines the difference between the z-position
  // of the center of sensitive detector volume and the hull box
  // positionOffset = dimensionHull[2] - thicknessHull - scintillatorDimensions[2]
  G4double posZ = _scalIn->position[2] + 4.53 * CLHEP::cm;

  // this is a fix for the wrong detectors.dat description
  // we add the thickness of the inox plate in Y direction for the upper row (with y-Pos = 32 cm)
  if (_scalIn->position[1] >= 32.000 * CLHEP::cm && _scalIn->position[1] < 33.200 * CLHEP::cm && yearSetup == 2012)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4ECAL0: cmtx line at y-position [32.0cm, 33.2cm). This causes an overlap with the innox plate. Change the value in the corresponding calorimeter.xml file to 33.2cm. ");

  TGEANT::T4CaloModule type = TGEANT::SHASHLIK_ECAL0;
  buildRegionBox(type, _scalIn, "ECAL0", zSize, moduleDistance, moduleDistance,
      posZ);

  int id = _scalIn->detectorId;
  for (int rows = 0; rows < _scalIn->nRow; rows++)
    for (int cols = 0; cols < _scalIn->nCol; cols++) {
      G4double posX = _scalIn->position[0] + moduleDistance * cols;
      G4double posY = _scalIn->position[1] + moduleDistance * rows;

      setLocalModule(TGEANT::SHASHLIK_ECAL0, G4ThreeVector(posX, posY, posZ),
          id);
      id++;
    }
  offsetECALModule = _scalIn->position[2];
  offsetCorner = posZ;
}

void T4ECAL0::constructMechanicalStructure(void)
{
  if (yearSetup == 2012) {
    zOffset += offsetECALModule;

    constructDummyModules2012();

    constructSidebars();
    constructBottomBars();
    constructTopBars();
    constructDiagonalBars();
    constructStamps();
    constructInoxPlates();
    constructAdditionalPlates();
  } else if (yearSetup == 2016) {
    constructDummyModules2016();
    constructPlates2016();

  } else
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "T4ECAL0::constructMechanicalStructure: Mechanical structure is only defined for year 2012 and 2016, not for "
            + doubleToStrFP(yearSetup));

}

void T4ECAL0::constructDummyModules2016(void)
{
  double moduleSize = 120. / 2. * CLHEP::mm;
  double thickness = 9. * CLHEP::mm;
  double zLength = 320.0 / 2. * CLHEP::mm;

  dummy_box[0] = new G4Box("dummy_box[0]", moduleSize, moduleSize, zLength);
  dummy_box[1] = new G4Box("dummy_box[0]", moduleSize - thickness, moduleSize - thickness, zLength + 1.*CLHEP::mm); //add length in z
  dummy_sub = new G4SubtractionSolid("dummy_sub", dummy_box[0], dummy_box[1]);
  dummy_log = new G4LogicalVolume(dummy_sub, materials->aluminium_noOptical, "dummy_log");
  dummy_log->SetVisAttributes(colour->black);

  vector<pair<double, double> > pos;
  for (int i = 0; i < 5; i++)
    for (int j = i; j < 5; j++)
      pos.push_back(make_pair(960 - i * 120, 480 + j * 120));

  for (int i = 0; i < 4; i++) {
    double signX = (i < 2) ? 1. : -1.;
    double signY = (i % 2) ? 1. : -1.;
    double yShift = (signY == 1.) ? 15. : 0.;

    for (unsigned int j = 0; j < pos.size(); j++)
      new G4PVPlacement(0,
          positionVector + G4ThreeVector(pos.at(j).first * signX * CLHEP::mm,
                  (pos.at(j).second * signY + yShift) * CLHEP::mm, zOffset), dummy_log,
          "ECAL0_dummyModule", world_log, 0, 0, checkOverlap);
  }
}

void T4ECAL0::constructPlates2016(void)
{
  double thickness = 15.0 / 2. * CLHEP::mm;
  double zLength = 320.0 / 2. * CLHEP::mm;
  double xSizeHole = 840.0 / 2. * CLHEP::mm;
  double ySizeHole = 600.0 / 2. * CLHEP::mm;

  bottomBarsLongSide_box = new G4Box("bottomBarsLongSide_box", xSizeHole, thickness, zLength);
  bottomBarFront_log = new G4LogicalVolume(bottomBarsLongSide_box, materials->aluminium_noOptical, "bottomBarFront_log");
  bottomBarFront_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, -ySizeHole + thickness, zOffset), bottomBarFront_log,
      "ECAL0_bottomPlate", world_log, 0, 0, checkOverlap);


  sidebarsBigPlate_box = new G4Box("sidebarsBigPlate_box", thickness, ySizeHole, zLength);
  sidebarsLeft_log = new G4LogicalVolume(sidebarsBigPlate_box, materials->aluminium_noOptical, "sidebarsLeft_log");
  sidebarsLeft_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0, positionVector + G4ThreeVector(-xSizeHole+thickness, 0, zOffset), sidebarsLeft_log,
      "ECAL0_sidePlate", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector + G4ThreeVector(xSizeHole-thickness, 0, zOffset), sidebarsLeft_log,
      "ECAL0_sidePlate", world_log, 0, 0, checkOverlap);


  inoxPlate1_box = new G4Box("inoxPlate1_box", 2040. / 2. * CLHEP::mm, thickness, zLength);
  inoxPlate1_log = new G4LogicalVolume(inoxPlate1_box, materials->aluminium_noOptical, "inoxPlate1_log");
  inoxPlate1_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, ySizeHole + thickness, zOffset), inoxPlate1_log,
      "ECAL0_topPlate", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructDummyModules2012(void)
{
  G4double positionX = 560.0 * CLHEP::mm;
  G4double positionY = 320.0 * CLHEP::mm;

  G4double moduleDistance = 4.000 * CLHEP::cm;
  G4int x, y;
  for (int corner = 0; corner < 4; corner++) {
    x = corner > 1 ? 1 : -1;
    y = corner % 2 ? 1 : -1;
    for (int rows = 0; rows < 6; rows++)
      for (int cols = 0; cols < 3; cols++) {
        T4ECAL0Module* module = new T4ECAL0Module();
        G4double posX = x*positionX + moduleDistance * cols;
        G4double posY = y*positionY + moduleDistance * rows;
        G4double posZ = offsetCorner;

        // this is a fix for the wrong detectors.dat description
        // we add the thickness of the inox plate in Y direction for the upper row (with y-Pos = 32 cm)
        if (y == 1)
          posY += 12.0 * CLHEP::mm;
        // we have to set the position of the module on the bottom right of the group
        else
          posY -= 200.0 * CLHEP::mm;
        if (x == -1)
          posX -= 80.0 * CLHEP::mm;

        module->setPosition(positionVector + G4ThreeVector(posX, posY, posZ));

        module->construct(world_log);
        modules.push_back(module);
      }
  }
}

void T4ECAL0::constructSidebars(void)
{
  G4double sizeX = 80.0 / 2 * CLHEP::mm;
  G4double sizeY = 1100.0 / 2 * CLHEP::mm;
  G4double sizeZ = 160.0 / 2 * CLHEP::mm;
  G4double thickness = 10.0 / 2 * CLHEP::mm;

  G4double offsetX = 1330.0 * CLHEP::mm;
  G4double offsetY = 10.0 * CLHEP::mm; // thickness of additional bottom plate

  sidebarsBigPlate_box = new G4Box("sidebarsBigPlate_box", thickness, sizeY,
      sizeZ);
  sidebarsSmallPlate_box = new G4Box("sidebarsSmallPlate_box",
      sizeX - thickness, sizeY, thickness);

  sidePanels_union = new G4UnionSolid("sidePanels_union",
      sidebarsSmallPlate_box, sidebarsSmallPlate_box, 0,
      G4ThreeVector(0, 0, 2. * (sizeZ - thickness)));
  sidebarsRight_union = new G4UnionSolid("sidebarsRight_union",
      sidebarsBigPlate_box, sidePanels_union, 0,
      G4ThreeVector(sizeX, 0, -sizeZ + thickness));
  sidebarsLeft_union = new G4UnionSolid("sidebarsLeft_union",
      sidebarsBigPlate_box, sidePanels_union, 0,
      G4ThreeVector(-sizeX, 0, -sizeZ + thickness));

  sidebarsRight_log = new G4LogicalVolume(sidebarsRight_union,
      materials->aluminium_noOptical, "sidebarsRight_log", 0, 0, 0, 0);
  sidebarsLeft_log = new G4LogicalVolume(sidebarsLeft_union,
      materials->aluminium_noOptical, "sidebarsLeft_log", 0, 0, 0, 0);
  sidebarsLeft_log->SetVisAttributes(colour->silver);
  sidebarsRight_log->SetVisAttributes(colour->silver);

  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-offsetX / 2, offsetY, -sizeZ + zOffset),
      sidebarsLeft_log, "sidebarsLeft_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-offsetX / 2, offsetY, sizeZ + zOffset),
      sidebarsLeft_log, "sidebarsLeft_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(offsetX / 2, offsetY, -sizeZ + zOffset),
      sidebarsRight_log, "sidebarsRight_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(offsetX / 2, offsetY, sizeZ + zOffset),
      sidebarsRight_log, "sidebarsRight_phys", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructBottomBars(void)
{
  G4double sizeX = 1952.0 / 2 * CLHEP::mm;
  G4double sizeY = 160.0 / 2 * CLHEP::mm;
  G4double sizeZ = 80.0 / 2 * CLHEP::mm;
  G4double thickness = 10.0 / 2 * CLHEP::mm;

  G4double offsetY = (540.0 /*modules*/+ 8.0 /*inox plate*/) * CLHEP::mm;
  G4double offsetZ = 160.0 * CLHEP::mm;

  bottomBarsLongSide_box = new G4Box("bottomBarsLongSide_box", sizeX, sizeY,
      thickness);
  bottomBarsShortSide_box = new G4Box("bottomBarsShortSide_box", sizeX,
      thickness, sizeZ - thickness);
  bottomBarSideBars_union = new G4UnionSolid("bottomBarSideBars_union",
      bottomBarsShortSide_box, bottomBarsShortSide_box, 0,
      G4ThreeVector(0, 2. * (sizeY - thickness), 0));
  bottomBarFront_union = new G4UnionSolid("bottomBarFront_union",
      bottomBarsLongSide_box, bottomBarSideBars_union, 0,
      G4ThreeVector(0, -sizeY + thickness, sizeZ));
  bottomBarBack_union = new G4UnionSolid("bottomBarBack_union",
      bottomBarsLongSide_box, bottomBarSideBars_union, 0,
      G4ThreeVector(0, -sizeY + thickness, -sizeZ));

  bottomBarFront_log = new G4LogicalVolume(bottomBarFront_union,
      materials->aluminium_noOptical, "bottomBarFront_log", 0, 0, 0, 0);
  bottomBarBack_log = new G4LogicalVolume(bottomBarBack_union,
      materials->aluminium_noOptical, "bottomBarBack_log", 0, 0, 0, 0);
  bottomBarBack_log->SetVisAttributes(colour->silver);
  bottomBarFront_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -offsetY - sizeY,
              -(offsetZ - thickness) + zOffset), bottomBarFront_log,
      "bottomBarFront_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -offsetY - sizeY, (offsetZ - thickness) + zOffset),
      bottomBarBack_log, "bottomBarBack_phys", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructTopBars(void)
{
  G4double sizeX = 1632.0 / 2 * CLHEP::mm;
  G4double sizeY = 160.0 / 2 * CLHEP::mm;
  G4double sizeZ = 80.0 / 2 * CLHEP::mm;
  G4double thickness = 10.0 / 2 * CLHEP::mm;

  G4double offsetY = (1100.0 / 2 /*half length of side bars*/+ 10.0 /*side bars y offset*/
      + 8.0 /*additional plates*/) * CLHEP::mm;

  G4double offsetZ = 160.0 * CLHEP::mm;

  topBarsLongSide_box = new G4Box("topBarsLongSide_box", sizeX, sizeY,
      thickness);
  topBarsShortSide_box = new G4Box("topBarsShortSide_box", sizeX, thickness,
      sizeZ - thickness);
  topBarSideBars_union = new G4UnionSolid("topBarSideBars_union",
      topBarsShortSide_box, topBarsShortSide_box, 0,
      G4ThreeVector(0, 2. * (sizeY - thickness), 0));
  topBarFront_union = new G4UnionSolid("topBarFront_union", topBarsLongSide_box,
      topBarSideBars_union, 0, G4ThreeVector(0, -sizeY + thickness, sizeZ));
  topBarBack_union = new G4UnionSolid("topBarBack_union", topBarsLongSide_box,
      topBarSideBars_union, 0, G4ThreeVector(0, -sizeY + thickness, -sizeZ));

  topBarFront_log = new G4LogicalVolume(topBarFront_union, materials->aluminium_noOptical,
      "topBarFront_log", 0, 0, 0, 0);
  topBarBack_log = new G4LogicalVolume(topBarBack_union, materials->aluminium_noOptical,
      "topBarBack_log", 0, 0, 0, 0);
  topBarBack_log->SetVisAttributes(colour->silver);
  topBarFront_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, offsetY + sizeY, -(offsetZ - thickness) + zOffset),
      topBarFront_log, "topBarFront_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, offsetY + sizeY, (offsetZ - thickness) + zOffset),
      topBarBack_log, "topBarBack_phys", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructDiagonalBars(void)
{
// 4 x 25 cm, rechter winkel
  G4double size[] = { 4.0 * CLHEP::cm, 25.0 * CLHEP::cm, 8.0 * CLHEP::mm };
  G4double offsetX = 850.0 * CLHEP::mm;
  G4double offsetY = 540.0 * CLHEP::mm;
  G4double offsetZ = 10.0 * CLHEP::mm;

  diagonalBar_box = new G4Box("diagonalBar_box", size[0] / 2, size[1] / 2,
      size[2] / 2);
  diagonalBar_log = new G4LogicalVolume(diagonalBar_box, materials->aluminium_noOptical,
      "diagonalBar_log", 0, 0, 0, 0);
  diagonalBar_log->SetVisAttributes(colour->silver);

  rotation45Plus = new G4RotationMatrix();
  rotation45Plus->rotateZ(45.0 * CLHEP::deg);
  rotation45Minus = new G4RotationMatrix();
  rotation45Minus->rotateZ(-45.0 * CLHEP::deg);
  new G4PVPlacement(rotation45Minus,
      positionVector
          + G4ThreeVector((offsetX - size[0]),
              -offsetY + size[1] / 2 - 25.0 * CLHEP::mm,
              -(offsetZ + size[2] / 2) + zOffset), diagonalBar_log,
      "diagonalBar_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotation45Plus,
      positionVector
          + G4ThreeVector(-(offsetX - size[0]),
              -offsetY + size[1] / 2 - 25.0 * CLHEP::mm,
              -(offsetZ + size[2] / 2) + zOffset), diagonalBar_log,
      "diagonalBar_phys", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructStamps(void)
{
  G4double sizeStampBig[] = { 5.0 * CLHEP::cm, 1.5 * CLHEP::cm };
  G4double sizeStampSmall[] = { 2.0 * CLHEP::cm, 5.0 * CLHEP::cm };
  stampBig_tubs = new G4Tubs("stampBig_tubs", 0, sizeStampBig[0] / 2,
      sizeStampBig[1] / 2, 0, 360.0 * CLHEP::deg);
  stampSmall_tubs = new G4Tubs("stampSmall_tubs", 0, sizeStampSmall[0] / 2,
      sizeStampSmall[1] / 2, 0, 360.0 * CLHEP::deg);
  stamp_union = new G4UnionSolid("stamp_union", stampBig_tubs, stampSmall_tubs,
      0, G4ThreeVector(0, 0, sizeStampSmall[1] / 2 + sizeStampBig[1] / 2));
  stamp_log = new G4LogicalVolume(stamp_union, materials->iron, "stamp_log", 0,
      0, 0, 0);
  stamp_log->SetVisAttributes(colour->silver);
  G4double offsetStampX = 816.0 * CLHEP::mm;
  G4double offsetStampY = (540.0 /*modules*/+ 8.0 /*inox plate*/+ 160.0 /*bottom bar*/
      + 10.0 /*additional plate*/) * CLHEP::mm + sizeStampBig[1] / 2
      + sizeStampSmall[1];
  G4double offsetStampZ = 100.0 * CLHEP::mm;

  rotation90 = new G4RotationMatrix();
  rotation90->rotateX(90.0 * CLHEP::deg);
  new G4PVPlacement(rotation90,
      positionVector
          + G4ThreeVector(offsetStampX, -offsetStampY, offsetStampZ + zOffset),
      stamp_log, "stamp_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotation90,
      positionVector
          + G4ThreeVector(offsetStampX, -offsetStampY, -offsetStampZ + zOffset),
      stamp_log, "stamp_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotation90,
      positionVector
          + G4ThreeVector(-offsetStampX, -offsetStampY, offsetStampZ + zOffset),
      stamp_log, "stamp_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotation90,
      positionVector
          + G4ThreeVector(-offsetStampX, -offsetStampY,
              -offsetStampZ + zOffset), stamp_log, "stamp_phys", world_log, 0,
      0, checkOverlap);
  G4double sizeTub[] = { 4.0 * CLHEP::cm, 5.0 * CLHEP::cm };
  G4double sizePlate[] = { 5.0 * CLHEP::cm, 8.0 * CLHEP::mm, 5.0 * CLHEP::cm };
  G4double offsetX = 716.0 * CLHEP::mm;
  G4double offsetY = (1100.0 / 2 /*half length of side bars*/+ 10.0 /*side bars y offset*/
      + 8.0 /*additional plate*/+ 160.0 /*top bar*/+ 12.0 /*additional plate*/)
      * CLHEP::mm + sizePlate[1] / 2;

  G4double offsetZ = 100.0 * CLHEP::mm;

  ringPlate_box = new G4Box("ringPlate_box", sizePlate[0] / 2, sizePlate[1] / 2,
      sizePlate[2] / 2);
  ring_tubs = new G4Tubs("", sizeTub[0] / 2, sizeTub[1] / 2,
      (sizeTub[1] - sizeTub[0]) / 2, 0, 360.0 * CLHEP::deg);

  ring_union = new G4UnionSolid("ring_union", ringPlate_box, ring_tubs, 0,
      G4ThreeVector(0, sizeTub[1] / 2 + sizePlate[1] / 2, 0));
  ring_log = new G4LogicalVolume(ring_union, materials->iron, "ring_log", 0, 0,
      0, 0);
  ring_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(offsetX, offsetY, -offsetZ + zOffset),
      ring_log, "ring_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(offsetX, offsetY, offsetZ + zOffset),
      ring_log, "ring_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-offsetX, offsetY, -offsetZ + zOffset),
      ring_log, "ring_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-offsetX, offsetY, offsetZ + zOffset),
      ring_log, "ring_phys", world_log, 0, 0, checkOverlap);
}

void T4ECAL0::constructInoxPlates(void)
{
  G4double size1[] = { 1320.0 * CLHEP::mm, 12.0 * CLHEP::mm, 320.0 * CLHEP::mm };
  G4double size2[] = { 1952.0 * CLHEP::mm, 8.0 * CLHEP::mm, 320.0 * CLHEP::mm };
  G4double offsetFromCenterY1 = 300.0 * CLHEP::mm;
  G4double offsetFromCenterY2 = 540.0 * CLHEP::mm;
  inoxPlate1_box = new G4Box("inoxPlate1_box", size1[0] / 2, size1[1] / 2,
      size1[2] / 2);
  inoxPlate2_box = new G4Box("inoxPlate2_box", size2[0] / 2, size2[1] / 2,
      size2[2] / 2);
  inoxPlate1_log = new G4LogicalVolume(inoxPlate1_box, materials->inox,
      "inoxPlate1_log", 0, 0, 0, 0);
  inoxPlate2_log = new G4LogicalVolume(inoxPlate2_box, materials->inox,
      "inoxPlate2_log", 0, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, offsetFromCenterY1 + size1[1] / 2, zOffset),
      inoxPlate1_log, "inoxPlate1_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -offsetFromCenterY2 - size2[1] / 2, zOffset),
      inoxPlate2_log, "inoxPlate2_phys", world_log, 0, 0, checkOverlap);
  inoxPlate1_log->SetVisAttributes(colour->black);
  inoxPlate2_log->SetVisAttributes(colour->black);
}

void T4ECAL0::constructAdditionalPlates(void)
{
  G4double sizeX = 200.0 / 2 * CLHEP::mm;
  G4double sizeY[] = { 8.0 / 2 * CLHEP::mm, 10.0 / 2 * CLHEP::mm, 12.0 / 2
      * CLHEP::mm, 3.0 / 2 * CLHEP::mm };
  G4double sizeZ = 320.0 / 2 * CLHEP::mm;

  G4double sizeXPlate = 1632.0 / 2 * CLHEP::mm /*top bars*/- 2. * sizeX;

  for (G4int i = 0; i < 4; i++) {
    if (i < 3)
      additionalPlate_box[i] = new G4Box("additionalPlate_box", sizeX, sizeY[i],
          sizeZ);
    else
      additionalPlate_box[i] = new G4Box("additionalPlate_box", sizeXPlate,
          sizeY[i], sizeZ);
    additionalPlate_log[i] = new G4LogicalVolume(additionalPlate_box[i],
        materials->inox, "additionalPlate_log", 0, 0, 0, 0);
    additionalPlate_log[i]->SetVisAttributes(colour->silver);
  }

  G4double offsetY_bottom =
      (540.0 /*modules*/+ 8.0 /*inox plate*/+ 160.0 /*bottom bar*/) * CLHEP::mm;
  G4double offsetX_bottom = (660.0 /*modules*/) * CLHEP::mm; // bottom bars
  G4double offsetY_topBelow =
      (1100.0 / 2 /*half length of side bars*/+ 10.0 /*side bars y offset*/)
          * CLHEP::mm;
  G4double offsetY_top = offsetY_topBelow + 2. * sizeY[0]
      + 160.0 * CLHEP::mm/*top bar*/;

  G4double offsetX2 = 1632.0 * CLHEP::mm; // top bars

  // bottom middle
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, -(offsetY_bottom + sizeY[1]), zOffset),
      additionalPlate_log[1], "additionalPlate_phys", world_log, 0, 0,
      checkOverlap);
  // bottom side
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(offsetX_bottom + sizeX, -(offsetY_bottom + sizeY[1]),
              zOffset), additionalPlate_log[1], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-offsetX_bottom - sizeX, -(offsetY_bottom + sizeY[1]),
              zOffset), additionalPlate_log[1], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);

  // top below bars
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, offsetY_topBelow + 2. * sizeY[0] - sizeY[3],
              zOffset), additionalPlate_log[3], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              offsetY_topBelow + 2. * sizeY[0] - 2. * sizeY[3] - sizeY[1],
              zOffset), additionalPlate_log[1], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector((offsetX2 / 2 - sizeX), offsetY_topBelow + sizeY[0],
              zOffset), additionalPlate_log[0], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-(offsetX2 / 2 - sizeX), offsetY_topBelow + sizeY[0],
              zOffset), additionalPlate_log[0], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);

  // top above bars
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, offsetY_top + sizeY[2], zOffset),
      additionalPlate_log[2], "additionalPlate_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector((offsetX2 / 2 - sizeX), offsetY_top + sizeY[2],
              zOffset), additionalPlate_log[2], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-(offsetX2 / 2 - sizeX), offsetY_top + sizeY[2],
              zOffset), additionalPlate_log[2], "additionalPlate_phys",
      world_log, 0, 0, checkOverlap);
}

T4ECAL0::T4ECAL0(vector<T4SCAL>& _in, const T4SDetector* _inDet) :
    T4CalorimeterBackend(_in, _inDet)
{
  yearSetup = strToDouble(T4EventManager::getInstance()->getTriggerPlugin()->getPluginName().substr(0,4));

  caloName = "ECAL0";
  // zOffset = half hull length - hull thickness - half sd length
  zOffset = 34.17 / 2 * CLHEP::cm - 0.1 * CLHEP::mm - 12.535 * CLHEP::cm;
  offsetECALModule = 0;
  offsetCorner = 0;

  // 2016
  dummy_box[0] = NULL;
  dummy_box[1] = NULL;
  dummy_sub = NULL;
  dummy_log = NULL;

  // side bars
  sidebarsBigPlate_box = NULL;
  sidebarsSmallPlate_box = NULL;
  sidePanels_union = NULL;
  sidebarsRight_union = NULL;
  sidebarsLeft_union = NULL;
  sidebarsLeft_log = NULL;
  sidebarsRight_log = NULL;

  // bottom bars
  bottomBarsLongSide_box = NULL;
  bottomBarsShortSide_box = NULL;
  bottomBarSideBars_union = NULL;
  bottomBarFront_union = NULL;
  bottomBarBack_union = NULL;
  bottomBarFront_log = NULL;
  bottomBarBack_log = NULL;

  // top bars
  topBarsLongSide_box = NULL;
  topBarsShortSide_box = NULL;
  topBarSideBars_union = NULL;
  topBarFront_union = NULL;
  topBarBack_union = NULL;
  topBarFront_log = NULL;
  topBarBack_log = NULL;

  // diagonal bars
  diagonalBar_box = NULL;
  diagonalBar_log = NULL;
  rotation45Plus = NULL;
  rotation45Minus = NULL;

  // stamps
  stampBig_tubs = NULL;
  stampSmall_tubs = NULL;
  stamp_union = NULL;
  stamp_log = NULL;
  rotation90 = NULL;
  ringPlate_box = NULL;
  ring_tubs = NULL;
  ring_union = NULL;
  ring_log = NULL;

  // inox plates
  inoxPlate1_box = NULL;
  inoxPlate2_box = NULL;
  inoxPlate1_log = NULL;
  inoxPlate2_log = NULL;

  // additional plates
  for (G4int i = 0; i < 4; i++) {
    additionalPlate_box[i] = NULL;
    additionalPlate_log[i] = NULL;
  }
}

T4ECAL0::~T4ECAL0(void)
{
  // 2016
  if (dummy_box[0] != NULL)
    delete dummy_box[0];
  if (dummy_box[1] != NULL)
    delete dummy_box[1];
  if (dummy_sub != NULL)
    delete dummy_sub;
  if (dummy_log != NULL)
    delete dummy_log;

  // side bars
  if (sidebarsBigPlate_box != NULL)
    delete sidebarsBigPlate_box;
  if (sidebarsSmallPlate_box != NULL)
    delete sidebarsSmallPlate_box;
  if (sidePanels_union != NULL)
    delete sidePanels_union;
  if (sidebarsRight_union != NULL)
    delete sidebarsRight_union;
  if (sidebarsLeft_union != NULL)
    delete sidebarsLeft_union;
  if (sidebarsLeft_log != NULL)
    delete sidebarsLeft_log;
  if (sidebarsRight_log != NULL)
    delete sidebarsRight_log;

  // bottom bars
  if (bottomBarsLongSide_box != NULL)
    delete bottomBarsLongSide_box;
  if (bottomBarsShortSide_box != NULL)
    delete bottomBarsShortSide_box;
  if (bottomBarSideBars_union != NULL)
    delete bottomBarSideBars_union;
  if (bottomBarFront_union != NULL)
    delete bottomBarFront_union;
  if (bottomBarBack_union != NULL)
    delete bottomBarBack_union;
  if (bottomBarFront_log != NULL)
    delete bottomBarFront_log;
  if (bottomBarBack_log != NULL)
    delete bottomBarBack_log;

  // top bars
  if (topBarsLongSide_box != NULL)
    delete topBarsLongSide_box;
  if (topBarsShortSide_box != NULL)
    delete topBarsShortSide_box;
  if (topBarSideBars_union != NULL)
    delete topBarSideBars_union;
  if (topBarFront_union != NULL)
    delete topBarFront_union;
  if (topBarBack_union != NULL)
    delete topBarBack_union;
  if (topBarFront_log != NULL)
    delete topBarFront_log;
  if (topBarBack_log != NULL)
    delete topBarBack_log;

  // diagonal bars
  if (diagonalBar_box != NULL)
    delete diagonalBar_box;
  if (diagonalBar_log != NULL)
    delete diagonalBar_log;
  if (rotation45Plus != NULL)
    delete rotation45Plus;
  if (rotation45Minus != NULL)
    delete rotation45Minus;

  // stamps
  if (stampBig_tubs != NULL)
    delete stampBig_tubs;
  if (stampSmall_tubs != NULL)
    delete stampSmall_tubs;
  if (stamp_union != NULL)
    delete stamp_union;
  if (stamp_log != NULL)
    delete stamp_log;
  if (rotation90 != NULL)
    delete rotation90;
  if (ringPlate_box != NULL)
    delete ringPlate_box;
  if (ring_tubs != NULL)
    delete ring_tubs;
  if (ring_union != NULL)
    delete ring_union;
  if (ring_log != NULL)
    delete ring_log;

  // inox plates
  if (inoxPlate1_box != NULL)
    delete inoxPlate1_box;
  if (inoxPlate2_box != NULL)
    delete inoxPlate2_box;
  if (inoxPlate1_log != NULL)
    delete inoxPlate1_log;
  if (inoxPlate2_log != NULL)
    delete inoxPlate2_log;

  // additional plates
  for (G4int i = 0; i < 4; i++) {
    if (additionalPlate_box[i] != NULL)
      delete additionalPlate_box[i];
    if (additionalPlate_log[i] != NULL)
      delete additionalPlate_log[i];
  }
}
