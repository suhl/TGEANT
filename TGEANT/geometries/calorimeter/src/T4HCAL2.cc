#include "T4HCAL2.hh"

void T4HCAL2::constructRegions(T4SCAL* _scalIn)
{
  G4double moduleDistance = 20.150 * CLHEP::cm;
  G4double zSize = 165.0 / 2 * CLHEP::cm;

  // this positionOffset value defines the difference between the z-position
  // of the center of sensitive detector volume and the hull box
  // positionOffset = dimensionHull[2] - thicknessHull - scintillatorDimensions[2]
  G4double posZ = _scalIn->position[2];

  // fix for 40 layer hcal modules:
  // the front of the modules are aligned correctly, therefore we have to change
  // the given position for the longer modules from -6.0 cm to -5.6 cm
  // this is because the given module z-size from detectors.dat differs with reality
  // negativ module position is for the shorter modules with 36 layers
  // zero position is equal to the 40 layer modules
  TGEANT::T4CaloModule type;
  if (_scalIn->position[2] < 0) {
    type = TGEANT::HCAL2_36;
    posZ += 0.4 * CLHEP::cm + 28.475 * CLHEP::cm;
  } else {
    type = TGEANT::HCAL2_40;
    posZ += 22.475 * CLHEP::cm;
  }

  buildRegionBox(type, _scalIn, "HCAL2", zSize, moduleDistance, moduleDistance,
      posZ);

  modulePositioning(type, _scalIn, moduleDistance, moduleDistance, posZ);
}

void T4HCAL2::constructMechanicalStructure(void)
{
  G4double offsetToFront = modules.at(0)->getDimensionZ() / 2.;
  G4ThreeVector positionVector_Offsetted = positionVector
      + G4ThreeVector(0, 0, -56.025*CLHEP::cm); //40layer*(sd+absorber thickness) + 0.5/2mm thicknessHull

  G4double dimensionHoleStructure[3];
  dimensionHoleStructure[0] = 400.0 / 2 * CLHEP::mm;
  dimensionHoleStructure[1] = 400.0 / 2 * CLHEP::mm;
  dimensionHoleStructure[2] = 50.0 / 2 * CLHEP::mm;
  G4double thicknessHoleStructure = 10.0 * CLHEP::mm;

  G4double thicknessFrame = 130.0 / 2 * CLHEP::mm;
  G4double widthFrame = 450.0 / 2 * CLHEP::mm;
  G4double sideLengthFrame = 2610.0 / 2 * CLHEP::mm - thicknessFrame;
  G4double topLengthFrame = 4695.0 / 2 * CLHEP::mm - 2. * thicknessFrame;

  G4double wheelRadius = 200.0 * CLHEP::mm;
  G4double wheelLength = 130.0 / 2 * CLHEP::mm;
  G4double xPos_wheels = 700.0 * CLHEP::mm; // 3.5 HCAL2 modules
  G4double zPos_wheels = 170.0 * CLHEP::mm;

  G4double bigWheelRadius = 560.0 / 2 * CLHEP::mm;
  G4double bigWheelLength = 180.0 / 2 * CLHEP::mm;

  G4double dimensionTable[3];
  dimensionTable[0] = topLengthFrame + 2. * thicknessFrame;
  dimensionTable[1] = thicknessFrame;
  dimensionTable[2] = 1400.0 / 2 * CLHEP::mm;

  G4double dimensionUpperBase[5];
  dimensionUpperBase[0] = topLengthFrame;
  dimensionUpperBase[1] = 4000.0 / 2 * CLHEP::mm;
  dimensionUpperBase[2] = dimensionTable[2] - zPos_wheels - wheelLength;
  dimensionUpperBase[3] = dimensionTable[2] - zPos_wheels - wheelLength;
  dimensionUpperBase[4] = 0.75 * wheelRadius;

  G4double dimensionRail[3];
  dimensionRail[0] = 10300.0 / 2 * CLHEP::mm - 2. * thicknessFrame;
  dimensionRail[1] = 135.0 / 2 * CLHEP::mm;
  dimensionRail[2] = wheelLength;

  G4double dimensionPlate[3];
  dimensionPlate[0] = 10300.0 / 2 * CLHEP::mm - 2. * thicknessFrame;
  dimensionPlate[1] = 135.0 / 2 * CLHEP::mm;
  dimensionPlate[2] = dimensionTable[2];

  G4double dimensionBase[5];
  dimensionBase[0] = dimensionPlate[0];
  dimensionBase[1] = 8580.0 / 2 * CLHEP::mm;
  dimensionBase[2] = dimensionPlate[2];
  dimensionBase[3] = dimensionPlate[2];
  dimensionBase[4] = 690.0 / 2 * CLHEP::mm;

  G4double dimensionBigRail[3];
  dimensionBigRail[0] = bigWheelLength;
  dimensionBigRail[1] = 90.0 / 2 * CLHEP::mm;
  dimensionBigRail[2] = dimensionPlate[2] + 2. * bigWheelRadius; // *2 for overlap

  G4double dimensionFoot[3];
  dimensionFoot[0] = 200.0 / 2 * CLHEP::mm;
  dimensionFoot[1] = bigWheelRadius + dimensionBigRail[1];
  dimensionFoot[2] = 200.0 / 2 * CLHEP::mm;

  G4double dimensionWheelFoot = 0.9 * bigWheelRadius;
  G4double dimensionWheelHolder[4];
  dimensionWheelHolder[0] = dimensionFoot[2];
  dimensionWheelHolder[1] = 0.9 * bigWheelRadius;
  dimensionWheelHolder[2] = 560.0 / 2 * CLHEP::mm;
  dimensionWheelHolder[3] = 60.0 / 2 * CLHEP::mm;

  if(modules.size() == 216){
    holeStructure_box[0] = new G4Box("holeStructure_box",
        dimensionHoleStructure[0], dimensionHoleStructure[1],
        dimensionHoleStructure[2]);
    holeStructure_box[1] = new G4Box("holeStructure_box",
        dimensionHoleStructure[0] - thicknessHoleStructure,
        dimensionHoleStructure[1] - thicknessHoleStructure,
        dimensionHoleStructure[2] - thicknessHoleStructure);
    holeStructure_box[2] = new G4Box("holeStructure_box",
        dimensionHoleStructure[0] - 2. * thicknessHoleStructure,
        dimensionHoleStructure[1] - 2. * thicknessHoleStructure,
        dimensionHoleStructure[2] + 1.0 * CLHEP::mm);
    holeStructure_sub[0] = new G4SubtractionSolid("holeStructure_sub[0]",
        holeStructure_box[0], holeStructure_box[1]);
    holeStructure_sub[1] = new G4SubtractionSolid("holeStructure_sub[1]",
        holeStructure_sub[0], holeStructure_box[2]);
    holeStructure_log = new G4LogicalVolume(holeStructure_sub[1],
        materials->stainlessSteel, "holeStructure_log", 0, 0, 0, 0);
    holeStructure_log->SetVisAttributes(colour->blue);
    new G4PVPlacement(0,
        positionVector_Offsetted
            + G4ThreeVector(200.0 * CLHEP::mm, 0, dimensionHoleStructure[2]),
        holeStructure_log, "holeStructure_phys", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector_Offsetted
            + G4ThreeVector(200.0 * CLHEP::mm, 0,
                1650.0 * CLHEP::mm - dimensionHoleStructure[2]), holeStructure_log,
        "holeStructure_phys", world_log, 0, 0, checkOverlap);
  }
  G4double frameGap = 200.0 * CLHEP::mm;
  frame_box[0] = new G4Box("frame_box", thicknessFrame, sideLengthFrame,
      widthFrame);
  frame_box[1] = new G4Box("frame_box", topLengthFrame, thicknessFrame,
      widthFrame);
  frame_log[0] = new G4LogicalVolume(frame_box[0], materials->stainlessSteel,
      "frame_log", 0, 0, 0, 0);
  frame_log[1] = new G4LogicalVolume(frame_box[1], materials->stainlessSteel,
      "frame_log", 0, 0, 0, 0);
  frame_log[0]->SetVisAttributes(colour->blue);
  frame_log[1]->SetVisAttributes(colour->blue);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(topLengthFrame + thicknessFrame,
              -activeArea[1] + sideLengthFrame, widthFrame), frame_log[0],
      "frame_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(-topLengthFrame - thicknessFrame,
              -activeArea[1] + sideLengthFrame, widthFrame), frame_log[0],
      "frame_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(topLengthFrame + thicknessFrame,
              -activeArea[1] + sideLengthFrame, frameGap + 3. * widthFrame),
      frame_log[0], "frame_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(-topLengthFrame - thicknessFrame,
              -activeArea[1] + sideLengthFrame, frameGap + 3. * widthFrame),
      frame_log[0], "frame_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(0,
              -activeArea[1] + 2. * sideLengthFrame - thicknessFrame,
              widthFrame), frame_log[1], "frame_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(0,
              -activeArea[1] + 2. * sideLengthFrame - thicknessFrame,
              frameGap + 3 * widthFrame), frame_log[1], "frame_phys", world_log,
      0, 0, checkOverlap);

  table_box = new G4Box("table_box", dimensionTable[0], dimensionTable[1],
      dimensionTable[2]);
  table_log = new G4LogicalVolume(table_box, materials->stainlessSteel,
      "table_log", 0, 0, 0, 0);
  table_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(0, -activeArea[1] - dimensionTable[1],
              dimensionTable[2]), table_log, "table_phys", world_log, 0, 0,
      checkOverlap);

  G4RotationMatrix* rotationBase = new CLHEP::HepRotation;
  rotationBase->rotateX(-90.0 * CLHEP::deg);
  upperBase_trd = new G4Trd("upperBase_trd", dimensionUpperBase[0],
      dimensionUpperBase[1], dimensionUpperBase[2], dimensionUpperBase[3],
      dimensionUpperBase[4]);
  upperBase_log = new G4LogicalVolume(upperBase_trd, materials->stainlessSteel,
      "upperBase_log", 0, 0, 0, 0);
  upperBase_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(rotationBase,
      positionVector_Offsetted
          + G4ThreeVector(0,
              -activeArea[1] - 2. * dimensionTable[1] - dimensionUpperBase[4],
              dimensionTable[2]), upperBase_log, "upperBase_phys", world_log, 0,
      0, checkOverlap);

  wheel_tubs[0] = new G4Tubs("wheel_tubs", 0.9 * wheelRadius, wheelRadius,
      wheelLength, 0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  wheel_tubs[1] = new G4Tubs("wheel_tubs", 0.3 * wheelRadius, 0.9 * wheelRadius,
      0.5 * wheelLength, 0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  wheel_tubs[2] = new G4Tubs("wheel_tubs", 0, 0.3 * wheelRadius, wheelLength,
      0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  tubs_union = new G4UnionSolid("tubs_union", wheel_tubs[0], wheel_tubs[1]);
  wheel_union = new G4UnionSolid("wheel_union", tubs_union, wheel_tubs[2]);
  wheel_log = new G4LogicalVolume(wheel_union, materials->iron, "", 0, 0, 0, 0);
  wheel_log->SetVisAttributes(colour->black);

  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(-dimensionTable[0] + thicknessFrame + xPos_wheels,
              -activeArea[1] - 2. * dimensionTable[1] - wheelRadius,
              zPos_wheels), wheel_log, "wheel_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(-dimensionTable[0] + thicknessFrame + xPos_wheels,
              -activeArea[1] - 2. * dimensionTable[1] - wheelRadius,
              2. * dimensionTable[2] - zPos_wheels), wheel_log, "wheel_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(+dimensionTable[0] - thicknessFrame - xPos_wheels,
              -activeArea[1] - 2. * dimensionTable[1] - wheelRadius,
              zPos_wheels), wheel_log, "wheel_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector_Offsetted
          + G4ThreeVector(+dimensionTable[0] - thicknessFrame - xPos_wheels,
              -activeArea[1] - 2. * dimensionTable[1] - wheelRadius,
              2. * dimensionTable[2] - zPos_wheels), wheel_log, "wheel_phys",
      world_log, 0, 0, checkOverlap);

  rail_box = new G4Box("rail_box", dimensionRail[0], dimensionRail[1],
      dimensionRail[2]);
  rail_log = new G4LogicalVolume(rail_box, materials->stainlessSteel,
      "rail_log", 0, 0, 0, 0);
  rail_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      G4ThreeVector(0,
          positionVector_Offsetted.getY() - activeArea[1]
              - 2. * dimensionTable[1] - 2. * wheelRadius - dimensionRail[1],
          positionVector_Offsetted.getZ() + zPos_wheels), rail_log, "rail_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(0,
          positionVector_Offsetted.getY() - activeArea[1]
              - 2. * dimensionTable[1] - 2. * wheelRadius - dimensionRail[1],
          positionVector_Offsetted.getZ() + 2. * dimensionPlate[2]
              - zPos_wheels), rail_log, "rail_phys", world_log, 0, 0,
      checkOverlap);

  plate_box = new G4Box("plate_box", dimensionPlate[0], dimensionPlate[1],
      dimensionPlate[2]);
  plate_log = new G4LogicalVolume(plate_box, materials->stainlessSteel,
      "plate_log", 0, 0, 0, 0);
  plate_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(0,
      G4ThreeVector(0,
          positionVector_Offsetted.getY() - activeArea[1]
              - 2. * dimensionTable[1] - 2. * wheelRadius - 2. * dimensionRail[1]
              - dimensionPlate[1],
          positionVector_Offsetted.getZ() + dimensionPlate[2]), plate_log,
      "plate_phys", world_log, 0, 0, checkOverlap);

  G4double yPos_base = positionVector_Offsetted.getY() - activeArea[1]
      - 2. * dimensionTable[1] - 2. * wheelRadius - 2. * dimensionRail[1]
      - 2. * dimensionPlate[1] - dimensionBase[4];
  G4double thicknessBase = 50.0 * CLHEP::mm;
  base_trd[0] = new G4Trd("base_trd", dimensionBase[0], dimensionBase[1],
      dimensionBase[2], dimensionBase[3], dimensionBase[4]);
  base_trd[1] = new G4Trd("base_trd", dimensionBase[0] + 1 * CLHEP::mm,
      dimensionBase[1] + 1.0 * CLHEP::mm, dimensionBase[2] - thicknessBase,
      dimensionBase[3] - thicknessBase, dimensionBase[4] - thicknessBase);
  base_sub = new G4SubtractionSolid("base_sub", base_trd[0], base_trd[1]);
  base_log = new G4LogicalVolume(base_sub, materials->stainlessSteel,
      "base_log", 0, 0, 0, 0);
  base_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(rotationBase,
      G4ThreeVector(0, yPos_base,
          positionVector_Offsetted.getZ() + dimensionPlate[2]), base_log,
      "base_phys", world_log, 0, 0, checkOverlap);

  bigWheel_tubs[0] = new G4Tubs("bigWheel_tubs", 0.9 * bigWheelRadius,
      bigWheelRadius, bigWheelLength, 0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  bigWheel_tubs[1] = new G4Tubs("bigWheel_tubs", 0.3 * bigWheelRadius,
      0.9 * bigWheelRadius, 0.5 * bigWheelLength, 0.0 * CLHEP::deg,
      360.0 * CLHEP::deg);
  bigWheel_tubs[2] = new G4Tubs("bigWheel_tubs", 0, 0.3 * bigWheelRadius,
      bigWheelLength, 0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
  bigTubs_union = new G4UnionSolid("bigTubs_union", bigWheel_tubs[0],
      bigWheel_tubs[1]);
  bigWheel_union = new G4UnionSolid("bigWheel_union", bigTubs_union,
      bigWheel_tubs[2]);
  bigWheel_log = new G4LogicalVolume(bigWheel_union, materials->iron, "", 0, 0,
      0, 0);
  bigWheel_log->SetVisAttributes(colour->black);

  G4RotationMatrix* rotationBigWheel = new CLHEP::HepRotation;
  rotationBigWheel->rotateY(90.0 * CLHEP::deg);
  G4double xPos_bigWheel = 3100.0 * CLHEP::mm;
  G4double yPos_bigWheel = yPos_base - dimensionBase[4] - bigWheelRadius;
  new G4PVPlacement(rotationBigWheel,
      G4ThreeVector(xPos_bigWheel, yPos_bigWheel,
          positionVector_Offsetted.getZ()), bigWheel_log, "bigWheel_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationBigWheel,
      G4ThreeVector(xPos_bigWheel, yPos_bigWheel,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]),
      bigWheel_log, "bigWheel_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationBigWheel,
      G4ThreeVector(-xPos_bigWheel, yPos_bigWheel,
          positionVector_Offsetted.getZ()), bigWheel_log, "bigWheel_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationBigWheel,
      G4ThreeVector(-xPos_bigWheel, yPos_bigWheel,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]),
      bigWheel_log, "bigWheel_phys", world_log, 0, 0, checkOverlap);

  bigRail_box = new G4Box("bigRail_box", dimensionBigRail[0],
      dimensionBigRail[1], dimensionBigRail[2]);
  bigRail_log = new G4LogicalVolume(bigRail_box, materials->stainlessSteel,
      "bigRail_log", 0, 0, 0, 0);
  bigRail_log->SetVisAttributes(colour->darkgray);
  new G4PVPlacement(0,
      G4ThreeVector(xPos_bigWheel,
          yPos_bigWheel - bigWheelRadius - dimensionBigRail[1],
          positionVector_Offsetted.getZ() + dimensionPlate[2]), bigRail_log,
      "bigRail_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(-xPos_bigWheel,
          yPos_bigWheel - bigWheelRadius - dimensionBigRail[1],
          positionVector_Offsetted.getZ() + dimensionPlate[2]), bigRail_log,
      "bigRail_phys", world_log, 0, 0, checkOverlap);

  G4double yPos_foot = yPos_base - dimensionBase[4] - dimensionFoot[1];
  foot_box = new G4Box("foot_box", dimensionFoot[0], dimensionFoot[1],
      dimensionFoot[2]);
  foot_log = new G4LogicalVolume(foot_box, materials->stainlessSteel,
      "foot_log", 0, 0, 0, 0);
  foot_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(0,
      G4ThreeVector(0, yPos_foot,
          positionVector_Offsetted.getZ() + dimensionFoot[2]), foot_log,
      "foot_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(0, yPos_foot,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - dimensionFoot[2]), foot_log, "foot_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(dimensionBase[1] - dimensionFoot[0], yPos_foot,
          positionVector_Offsetted.getZ() + dimensionFoot[2]), foot_log,
      "foot_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(dimensionBase[1] - dimensionFoot[0], yPos_foot,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - dimensionFoot[2]), foot_log, "foot_phys", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(-dimensionBase[1] + dimensionFoot[0], yPos_foot,
          positionVector_Offsetted.getZ() + dimensionFoot[2]), foot_log,
      "foot_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(-dimensionBase[1] + dimensionFoot[0], yPos_foot,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - dimensionFoot[2]), foot_log, "foot_phys", world_log, 0, 0,
      checkOverlap);

  G4RotationMatrix* rotationWheelHolder = new CLHEP::HepRotation;
  rotationWheelHolder->rotateZ(180.0 * CLHEP::deg);
  G4RotationMatrix* rotationWheelHolder2 = new CLHEP::HepRotation;
  rotationWheelHolder2->rotateX(180.0 * CLHEP::deg);
  G4double yPos_wheelHolder = yPos_base - dimensionBase[4]
      - dimensionWheelHolder[1];
  wheelHolder_trap = new G4Trap("wheelHolder_trap", 2. * dimensionWheelHolder[0],
      2. * dimensionWheelHolder[1], 2. * dimensionWheelHolder[2],
      2. * dimensionWheelHolder[3]);
  wheelHolder_log = new G4LogicalVolume(wheelHolder_trap,
      materials->stainlessSteel, "wheelHolder_log", 0, 0, 0, 0);
  wheelHolder_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(rotationWheelHolder,
      G4ThreeVector(
          xPos_bigWheel - bigWheelLength - 2. * dimensionFoot[0]
              - 0.5 * (dimensionWheelHolder[2] + dimensionWheelHolder[3]),
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + bigWheelRadius
              - dimensionWheelFoot), wheelHolder_log, "wheelHolder_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationWheelHolder,
      G4ThreeVector(
          xPos_bigWheel - bigWheelLength - 2. * dimensionFoot[0]
              - 0.5 * (dimensionWheelHolder[2] + dimensionWheelHolder[3]),
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - bigWheelRadius + dimensionWheelFoot), wheelHolder_log,
      "wheelHolder_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationWheelHolder2,
      G4ThreeVector(
          -xPos_bigWheel + bigWheelLength + 2. * dimensionFoot[0]
              + 0.5 * (dimensionWheelHolder[2] + dimensionWheelHolder[3]),
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + bigWheelRadius
              - dimensionWheelFoot), wheelHolder_log, "wheelHolder_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationWheelHolder2,
      G4ThreeVector(
          -xPos_bigWheel + bigWheelLength + 2. * dimensionFoot[0]
              + 0.5 * (dimensionWheelHolder[2] + dimensionWheelHolder[3]),
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - bigWheelRadius + dimensionWheelFoot), wheelHolder_log,
      "wheelHolder_phys", world_log, 0, 0, checkOverlap);

  wheelFoot_box = new G4Box("wheelFoot_box", dimensionFoot[0],
      dimensionWheelHolder[1], dimensionWheelFoot);
  wheelFoot_log = new G4LogicalVolume(wheelFoot_box, materials->stainlessSteel,
      "wheelFoot_log", 0, 0, 0, 0);
  wheelFoot_log->SetVisAttributes(colour->blue);
  new G4PVPlacement(0,
      G4ThreeVector(xPos_bigWheel - bigWheelLength - dimensionFoot[0],
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + bigWheelRadius
              - dimensionWheelFoot), wheelFoot_log, "wheelFoot_phys", world_log,
      0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(xPos_bigWheel - bigWheelLength - dimensionFoot[0],
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - bigWheelRadius + dimensionWheelFoot), wheelFoot_log,
      "wheelFoot_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(-xPos_bigWheel + bigWheelLength + dimensionFoot[0],
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + bigWheelRadius
              - dimensionWheelFoot), wheelFoot_log, "wheelFoot_phys", world_log,
      0, 0, checkOverlap);
  new G4PVPlacement(0,
      G4ThreeVector(-xPos_bigWheel + bigWheelLength + dimensionFoot[0],
          yPos_wheelHolder,
          positionVector_Offsetted.getZ() + 2. * dimensionTable[2]
              - bigWheelRadius + dimensionWheelFoot), wheelFoot_log,
      "wheelFoot_phys", world_log, 0, 0, checkOverlap);
}

T4HCAL2::T4HCAL2(vector<T4SCAL>& _in, const T4SDetector* _inDet) :
    T4CalorimeterBackend(_in, _inDet)
{
  caloName = "HCAL2";

  activeArea[0] = 440.0 / 2 * CLHEP::cm;
  activeArea[1] = 200.0 / 2 * CLHEP::cm;

  holeStructure_box[0] = NULL;
  holeStructure_box[1] = NULL;
  holeStructure_box[2] = NULL;
  holeStructure_sub[0] = NULL;
  holeStructure_sub[1] = NULL;
  holeStructure_log = NULL;
  frame_box[0] = NULL;
  frame_box[1] = NULL;
  frame_log[0] = NULL;
  frame_log[1] = NULL;
  table_box = NULL;
  table_log = NULL;
  upperBase_trd = NULL;
  upperBase_log = NULL;
  wheel_tubs[0] = NULL;
  wheel_tubs[1] = NULL;
  wheel_tubs[2] = NULL;
  tubs_union = NULL;
  wheel_union = NULL;
  wheel_log = NULL;
  rail_box = NULL;
  rail_log = NULL;
  plate_box = NULL;
  plate_log = NULL;
  base_trd[0] = NULL;
  base_trd[1] = NULL;
  base_sub = NULL;
  base_log = NULL;
  bigWheel_tubs[0] = NULL;
  bigWheel_tubs[1] = NULL;
  bigWheel_tubs[2] = NULL;
  bigTubs_union = NULL;
  bigWheel_union = NULL;
  bigWheel_log = NULL;
  bigRail_box = NULL;
  bigRail_log = NULL;
  foot_box = NULL;
  foot_log = NULL;
  wheelHolder_trap = NULL;
  wheelHolder_log = NULL;
  wheelFoot_box = NULL;
  wheelFoot_log = NULL;
}

T4HCAL2::~T4HCAL2(void)
{
  if (useMechanicalStructure) {
    delete holeStructure_box[0];
    delete holeStructure_box[1];
    delete holeStructure_box[2];
    delete holeStructure_sub[0];
    delete holeStructure_sub[1];
    delete holeStructure_log;

    delete frame_box[0];
    delete frame_box[1];
    delete frame_log[0];
    delete frame_log[1];

    delete table_box;
    delete table_log;

    delete upperBase_trd;
    delete upperBase_log;

    delete wheel_tubs[0];
    delete wheel_tubs[1];
    delete wheel_tubs[2];
    delete tubs_union;
    delete wheel_union;
    delete wheel_log;

    delete rail_box;
    delete rail_log;

    delete plate_box;
    delete plate_log;

    delete base_trd[0];
    delete base_trd[1];
    delete base_sub;
    delete base_log;

    delete bigWheel_tubs[0];
    delete bigWheel_tubs[1];
    delete bigWheel_tubs[2];
    delete bigTubs_union;
    delete bigWheel_union;
    delete bigWheel_log;

    delete bigRail_box;
    delete bigRail_log;

    delete foot_box;
    delete foot_log;

    delete wheelHolder_trap;
    delete wheelHolder_log;

    delete wheelFoot_box;
    delete wheelFoot_log;
  }
}
