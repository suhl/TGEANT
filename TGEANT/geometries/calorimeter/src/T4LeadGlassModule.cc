#include "T4LeadGlassModule.hh"
#include "T4CaloManager.hh"

T4LeadGlassModule::T4LeadGlassModule(void)
{
  dimensionHull[0] = 0.0;
  dimensionHull[1] = 0.0;
  dimensionHull[2] = 0.0;
  thicknessHull = 0.0;

  materialGlass = NULL;
  materialHull = NULL;
  colourHull = NULL;
  colourGlass = NULL;

  outerHull_box = NULL;
  outerHull_log = NULL;
  leadGlass_box = NULL;
  pmt = NULL;
}

T4LeadGlassModule::~T4LeadGlassModule(void)
{
  if (outerHull_box != NULL)
    delete outerHull_box;
  if (outerHull_log != NULL) {
    if (outerHull_log == scintillator_log)
      scintillator_log = NULL;
    delete outerHull_log;
  }
  if (leadGlass_box != NULL)
    delete leadGlass_box;
  if (pmt != NULL)
    delete pmt;
}

T4Gams::T4Gams(string caloName)
{
  setDetectorName("GAMS");
  dimensionHull[0] = 38.3 / 2. * CLHEP::mm;
  dimensionHull[1] = 38.3 / 2. * CLHEP::mm;
  dimensionHull[2] = 45.0 / 2. * CLHEP::cm;
  thicknessHull = 0.05 * CLHEP::mm;

  materialGlass = materials->tf1;
  materialHull = materials->iron;
  colourHull = colourGlass = colour->dodgerblue;

  static T4RegionInformation* regionInfo1 = new T4RegionInformation(materialGlass);
  static T4RegionInformation* regionInfo2 = new T4RegionInformation(materialGlass);
  if (caloName == "ECAL1")
    regionModule = regionManager->getECAL1GamsRegion(regionInfo1);
  else if (caloName == "ECAL2")
    regionModule = regionManager->getECAL2GamsRegion(regionInfo2);
  else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__,
        "T4Gams: Name of the calorimeter does not match ECAL1 or ECAL2. Set the region of this module to ECAL1 as default.");
    regionModule = regionManager->getECAL1GamsRegion(regionInfo1);
  }

  energyScale = 0.985;
}

T4GamsRH::T4GamsRH(void)
{
  setDetectorName("GAMSRH");
  dimensionHull[0] = 38.3 / 2. * CLHEP::mm;
  dimensionHull[1] = 38.3 / 2. * CLHEP::mm;
  dimensionHull[2] = 45.0 / 2. * CLHEP::cm;
  thicknessHull = 0.05 * CLHEP::mm;

  materialGlass = materials->tf101;
  materialHull = materials->iron;
  colourHull = colourGlass = colour->deepskyblue;

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialGlass);
  regionModule = regionManager->getECAL2GamsRhRegion(regionInfo);

  energyScale = 0.985;
}

T4Olga::T4Olga(void)
{
  setDetectorName("OLGA");
  dimensionHull[0] = 143.0 / 2. * CLHEP::mm;
  dimensionHull[1] = 143.0 / 2. * CLHEP::mm;
  dimensionHull[2] = 47.0 / 2. * CLHEP::cm;

  materialHull = materialGlass = materials->sf5;
  colourHull = colourGlass = colour->lightskyblue;

  pmt = new T4PhotoMultiplier();
  pmt->setPmtType(TGEANT::XP2050);

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialGlass);
  regionModule = regionManager->getECAL1OlgaRegion(regionInfo);

  energyScale = 1.01;
}

T4Mainz::T4Mainz(void)
{
  setDetectorName("MAINZ");
  dimensionHull[0] = 75.0 / 2. * CLHEP::mm;
  dimensionHull[1] = 75.0 / 2. * CLHEP::mm;
  dimensionHull[2] = 36.0 / 2. * CLHEP::cm;

  materialHull = materialGlass = materials->sf57;
  colourHull = colourGlass = colour->deepskyblue;

  pmt = new T4PhotoMultiplier();
  pmt->setPmtType(TGEANT::EMI9236KB);

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialGlass);
  regionModule = regionManager->getECAL1MainzRegion(regionInfo);
}

void T4LeadGlassModule::construct(G4LogicalVolume* world_log)
{
  outerHull_box = new G4Box("outerHull_box", dimensionHull[0], dimensionHull[1],
      dimensionHull[2]);
  outerHull_log = new G4LogicalVolume(outerHull_box, materialHull,
      "calorimeter_outerHull_log", 0, 0, 0, 0);
  outerHull_log->SetVisAttributes(colourHull);
  // position is defined as center of sensitive volume
  // but we don't need any shift here because the detectors.dat volume is
  // placed in the center of the hull box
  new G4PVPlacement(0, positionVector,
      outerHull_log, detName + intToStr(channelID), world_log, 0, 0, checkOverlap);

  if (pmt == NULL) {
    leadGlass_box = new G4Box("leadGlass_box", dimensionHull[0] - thicknessHull,
        dimensionHull[1] - thicknessHull, dimensionHull[2]);
    scintillator_log = new G4LogicalVolume(leadGlass_box, materialGlass,
        "calorimeter_log", 0, 0, 0, 0);
    scintillator_log->SetVisAttributes(colourGlass);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), scintillator_log,
        "leadglass_phys", outerHull_log, 0, 0, checkOverlap);
  } else {
    scintillator_log = outerHull_log;
  }
}

void T4LeadGlassModule::constructReadout(G4LogicalVolume* world_log,
    G4ThreeVector position)
{
  if (pmt != NULL) {
    pmt->setAlignment(TGEANT::DOWN);
    pmt->setPosition(
        position + G4ThreeVector(0, 0, dimensionHull[2] + pmt->getPmtLength()));
    pmt->construct(world_log);
  }
}
