#include "T4CaloManager.hh"
#include "T4PhysicsList.hh"

T4CaloManager::T4CaloManager(void)
{
  cmtxList = T4SettingsFile::getInstance()->getCMTXList();
}

T4CaloManager::~T4CaloManager(void)
{
  for (unsigned int i = 0; i < activeCalos.size(); i++)
    delete activeCalos.at(i);
  activeCalos.clear();
}

void T4CaloManager::construct(G4LogicalVolume* world_log)
{
  if (!cmtxList->size()) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4CaloManager::construct: cmtxList is empty! Return.");
    return;
  }

  vector<T4SCAL>* chosenCalo;
  //first we split the lines into the different calorimeters that we want to build.
  for (unsigned int i = 0; i < cmtxList->size(); i++) {
    chosenCalo = NULL;
    // this is an electromagnetic calo

    switch (cmtxList->at(i).detName.at(0)) {
      case 'L':
        switch (cmtxList->at(i).detName.at(2)) {
          case '3':
            chosenCalo = &ECAL0;
            break;
          case '1':
            chosenCalo = &ECAL1;
            break;
          case '2':
            chosenCalo = &ECAL2;
            break;
          default:
            T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
            __FILE__,
                "T4CaloManager::construct: Unknown ECAL detector name in cmtx line. This entry will be skipped.");
            continue;
            break;
        }
        break;
      case 'H':
        switch (cmtxList->at(i).detName.at(2)) {
          case '1':
            chosenCalo = &HCAL1;
            break;
          case '2':
            chosenCalo = &HCAL2;
            break;
          default:
            T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
            __FILE__,
                "T4CaloManager::construct: Unknown HCAL detector name in cmtx line. This entry will be skipped.");
            continue;
            break;
        }
        break;

      default:
        T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
        __FILE__,
            "T4CaloManager::construct: Unknown CAL detector name in cmtx line. This entry will be skipped.");
        continue;
        break;

    }

    //copy the line into the chosen calo-vector
    chosenCalo->push_back(cmtxList->at(i));
  }

  //now that we have split the lines its time for checking what calos we have to build;
  const vector<T4SDetector>* activeCaloList = settingsFile->getStructManager()
      ->getCalorimeter();

  for (unsigned int i = 0; i < activeCaloList->size(); i++) {
    if (activeCaloList->at(i).name == "ECAL0")
      activeCalos.push_back(new T4ECAL0(ECAL0, &activeCaloList->at(i)));
    else if (activeCaloList->at(i).name == "ECAL1")
      activeCalos.push_back(new T4ECAL1(ECAL1, &activeCaloList->at(i)));
    else if (activeCaloList->at(i).name == "ECAL2")
      activeCalos.push_back(new T4ECAL2(ECAL2, &activeCaloList->at(i)));
    else if (activeCaloList->at(i).name == "HCAL1")
      activeCalos.push_back(new T4HCAL1(HCAL1, &activeCaloList->at(i)));
    else if (activeCaloList->at(i).name == "HCAL2")
      activeCalos.push_back(new T4HCAL2(HCAL2, &activeCaloList->at(i)));
  }

  for (unsigned int i = 0; i < activeCalos.size(); i++)
    activeCalos.at(i)->construct(world_log);

  if (settingsFile->getStructManager()->getGeneral()->useGflash)
    addGflash();
}

void T4CaloManager::addGflash(void)
{
  G4Region* reg;
  vector<G4Region*> leadglassModules;
  vector<G4Region*> samplingModules;

  vector<GVFlashHomoShowerTuning*> gflashTuning;
  vector<T4GFlashTuningShashlik*> gflashTuning_Sampling;

  // Only for lead glass modules:
  reg = regionManager->getECAL1GamsRegion();
  if (reg != NULL) {
    leadglassModules.push_back(reg);
    gflashTuning.push_back(new T4GFlashTuningGams());
  }

  reg = regionManager->getECAL2GamsRegion();
  if (reg != NULL) {
    leadglassModules.push_back(reg);
    gflashTuning.push_back(new T4GFlashTuningGams());
  }

  reg = regionManager->getECAL2GamsRhRegion();
  if (reg != NULL) {
    leadglassModules.push_back(reg);
    gflashTuning.push_back(new T4GFlashTuningGams());
  }

  reg = regionManager->getECAL1OlgaRegion();
  if (reg != NULL) {
    leadglassModules.push_back(reg);
    gflashTuning.push_back(new T4GFlashTuningOlga());
  }

  reg = regionManager->getECAL1MainzRegion();
  if (reg != NULL) {
    leadglassModules.push_back(reg);
    gflashTuning.push_back(new T4GFlashTuningMainz());
  }

  for (unsigned int i = 0; i < leadglassModules.size(); i++) {
    showerModels.push_back(
        new GFlashShowerModel(leadglassModules.at(i)->GetName() + "ShowerModel",
            leadglassModules.at(i)));

    T4RegionInformation* info = (T4RegionInformation*) leadglassModules.at(i)->GetUserInformation();

    parameterisations.push_back(
        new GFlashHomoShowerParameterisation(info->getMat1(),
            gflashTuning.at(i)));
  }

  // Only for sampling modules:
  reg = regionManager->getEcal0Region();
  if (reg != NULL) {
    samplingModules.push_back(reg);
    gflashTuning_Sampling.push_back(new T4GFlashTuningECAL0());
  }

//  reg = regionManager->getHcal1Region();
//  if (reg != NULL)
//    samplingModules.push_back(reg);
//
//  reg = regionManager->getHcal2Region();
//  if (reg != NULL)
//    samplingModules.push_back(reg);

  reg = regionManager->getECAL1ShashlikRegion();
  if (reg != NULL) {
    samplingModules.push_back(reg);
    gflashTuning_Sampling.push_back(new T4GFlashTuningShashlik());
  }

  reg = regionManager->getECAL2ShashlikRegion();
  if (reg != NULL) {
    samplingModules.push_back(reg);
    gflashTuning_Sampling.push_back(new T4GFlashTuningShashlik());
  }

  for (unsigned int i = 0; i < samplingModules.size(); i++) {
    showerModels.push_back(
        new GFlashShowerModel(samplingModules.at(i)->GetName() + "ShowerModel",
            samplingModules.at(i)));
    T4RegionInformation* info = (T4RegionInformation*) samplingModules.at(i)
        ->GetUserInformation();
    parameterisations.push_back(
        new T4GFlashSamplingShowerParameterisation(info->getMat1(),
            info->getMat2(), info->getD1(), info->getD2(),
            gflashTuning_Sampling.at(i)));
  }

  // For all modules:
  for (unsigned int i = 0; i < showerModels.size(); i++) {
    showerModels.at(i)->SetFlagParamType(1);
    showerModels.at(i)->SetParameterisation(*parameterisations.at(i));
    // Energy Cuts to kill particles:
    particleBounds.push_back(new GFlashParticleBounds());

    showerModels.at(i)->SetParticleBounds(*particleBounds.back());
    // Makes the EnergieSpots
    hitMakers.push_back(new GFlashHitMaker());
    showerModels.at(i)->SetHitMaker(*hitMakers.back());
  }
}
