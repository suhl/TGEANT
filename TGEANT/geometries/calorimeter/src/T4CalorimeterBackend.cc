#include "T4CalorimeterBackend.hh"

T4CalorimeterBackend::T4CalorimeterBackend(vector<T4SCAL>& _in,
    const T4SDetector* _inDet)
{
  cmtxList = _in;
  setPosition(_inDet->position);
  useMechanicalStructure = _inDet->useMechanicalStructure;

  world_log = NULL;

  for (int i = 0; i < nModules; i++)
    cmtx_log[i] = NULL;
}

T4CalorimeterBackend::T4CalorimeterBackend(void)
{
  world_log = NULL;

  for (int i = 0; i < nModules; i++)
    cmtx_log[i] = NULL;
}

T4CalorimeterBackend::~T4CalorimeterBackend(void)
{
  for (int i = 0; i < nModules; i++) {
    if (cmtx_log[i] != NULL)
      delete cmtx_log[i];

    for (unsigned int j = 0; j < cmtx_box[i].size(); j++)
      delete cmtx_box[i].at(j);
    cmtx_box[i].clear();

    for (unsigned int j = 0; j < cmtx_union[i].size(); j++)
      delete cmtx_union[i].at(j);
    cmtx_union[i].clear();
  }

  // delete modules at end to avoid a Geant4 segfault
  for (unsigned int i = 0; i < modules.size(); i++)
    delete modules.at(i);
  modules.clear();
}

void T4CalorimeterBackend::construct(G4LogicalVolume* _world_log)
{
  world_log = _world_log;

  // construct the regions first
  for (unsigned int i = 0; i < cmtxList.size(); i++)
    constructRegions(&(cmtxList.at(i)));

  // construct the modules second
  constructModules();

  // first module, second mechanical!
  if (useMechanicalStructure)
    constructMechanicalStructure();
}

void T4CalorimeterBackend::constructModules(void)
{
  for (int i = 0; i < nModules; i++) {
    if (cmtx_box[i].size() == 0)
      continue;

    G4VSolid* solid;
    if (cmtx_union[i].size() == 0)
      solid = cmtx_box[i].at(0);
    else
      solid = cmtx_union[i].back();
    cmtx_log[i] = new G4LogicalVolume(solid, materials->air_noOptical,
        caloName + intToStr(i), 0, 0, 0, 0);
    cmtx_log[i]->SetVisAttributes(colour->invisible);
    new G4PVPlacement(0, firstBoxPosition[i], cmtx_log[i],
        caloName + intToStr(i), world_log, 0, 0, checkOverlap);

    for (unsigned int j = 0; j < localModulePosition[i].size(); j++) {
      T4ModuleBackend* module;
      getModule((TGEANT::T4CaloModule) i, module);
      module->setChannelId(channelNo[i].at(j));
      module->setPosition(localModulePosition[i].at(j));
      module->construct(cmtx_log[i]);
      module->addSensitiveDetector(drsModulePosition[i].at(j));
      module->constructReadout(world_log, firstBoxPosition[i] + localModulePosition[i].at(j));
      modules.push_back(module);
    }

    regionManager->addToRegion(cmtx_log[i], getRegion((TGEANT::T4CaloModule) i));
  }
}

void T4CalorimeterBackend::getModule(TGEANT::T4CaloModule _type, T4ModuleBackend*& _mod)
{
  switch (_type) {
    case TGEANT::SHASHLIK_ECAL0:
      _mod = new T4ECAL0Module();
      break;
    case TGEANT::SHASHLIK:
      _mod = new T4ShashlikModule(caloName);
      break;
    case TGEANT::GAMS:
      _mod = new T4Gams(caloName);
      break;
    case TGEANT::MAINZ:
      _mod = new T4Mainz();
      break;
    case TGEANT::OLGA:
      _mod = new T4Olga();
      break;
    case TGEANT::GAMSRH:
      _mod = new T4GamsRH();
      break;
    case TGEANT::HCAL1:
      _mod = new T4HCAL1Module();
      break;
    case TGEANT::HCAL2_36:
      _mod = new T4HCAL2Module();
      break;
    case TGEANT::HCAL2_40:
      _mod = new T4HCAL2Module();
      _mod->setLayerNumber(40);
      break;

    default:
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
            "T4CalorimeterBackend::getModule: Unknown module type!");
      break;
  }
}

G4Region* T4CalorimeterBackend::getRegion(TGEANT::T4CaloModule _type)
{
  switch (_type) {
    case TGEANT::SHASHLIK_ECAL0:
      return regionManager->getEcal0Region();
    case TGEANT::SHASHLIK:
      if (caloName == "ECAL1")
        return regionManager->getECAL1ShashlikRegion();
      else if (caloName == "ECAL2")
        return regionManager->getECAL2ShashlikRegion();
      else
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
            "T4CalorimeterBackend::getRegion: Unknown caloName '" + caloName
                + "' type!");
    case TGEANT::GAMS:
      if (caloName == "ECAL1")
        return regionManager->getECAL1GamsRegion();
      else if (caloName == "ECAL2")
        return regionManager->getECAL2GamsRegion();
      else
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__,
            "T4CalorimeterBackend::getRegion: Unknown caloName '" + caloName
                + "' type!");
    case TGEANT::MAINZ:
      return regionManager->getECAL1MainzRegion();
    case TGEANT::OLGA:
      return regionManager->getECAL1OlgaRegion();
    case TGEANT::GAMSRH:
      return regionManager->getECAL2GamsRhRegion();
    case TGEANT::HCAL1:
      return regionManager->getHcal1Region();
    case TGEANT::HCAL2_36:
    case TGEANT::HCAL2_40:
      return regionManager->getHcal2Region();

    default:
      T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
      __FILE__, "T4CalorimeterBackend::getRegion: Unknown module type!");
      return 0;
  }
}

void T4CalorimeterBackend::buildRegionBox(TGEANT::T4CaloModule type,
    T4SCAL* _scalIn, string name, G4double zSize, G4double moduleDistanceX,
    G4double moduleDistanceY, G4double posZ)
{
  G4double xSize = _scalIn->nCol * moduleDistanceX / 2.;
  G4double ySize = _scalIn->nRow * moduleDistanceY / 2.;

  getBox(type).push_back(new G4Box(name + "_box", xSize, ySize, zSize));

  G4ThreeVector position = positionVector
      + G4ThreeVector(
          _scalIn->position[0] + moduleDistanceX * (_scalIn->nCol - 1) / 2.,
          _scalIn->position[1] + moduleDistanceY * (_scalIn->nRow - 1) / 2.,
          posZ);

  if (getUnion(type).size() == 0) {
    if (getBox(type).size() == 1) {
      getFirstBoxPosition(type) = position;
    } else {
      getUnion(type).push_back(
          new G4UnionSolid(name + "_union", getBox(type).at(0),
              getBox(type).back(), 0, position - getFirstBoxPosition(type)));
    }
  } else {
    getUnion(type).push_back(
        new G4UnionSolid(name + "_union", getUnion(type).back(),
            getBox(type).back(), 0, position - getFirstBoxPosition(type)));
  }
}

void T4CalorimeterBackend::modulePositioning(TGEANT::T4CaloModule type, T4SCAL* _scalIn, G4double moduleDistanceX,
    G4double moduleDistanceY, G4double posZ)
{
  int id = _scalIn->detectorId;
  for (int rows = 0; rows < _scalIn->nRow; rows++)
    for (int cols = 0; cols < _scalIn->nCol; cols++) {
      G4double posX = _scalIn->position[0] + moduleDistanceX * cols;
      G4double posY = _scalIn->position[1] + moduleDistanceY * rows;
      setLocalModule(type, G4ThreeVector(posX, posY, posZ), id);
      id++;
    }
}
