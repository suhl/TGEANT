#include "T4HCAL1.hh"

void T4HCAL1::constructRegions(T4SCAL* _scalIn)
{
  G4double moduleDistance = 15.150 * CLHEP::cm;
  G4double zSize = 119.0 / 2 * CLHEP::cm;

  // this positionOffset value defines the difference between the z-position
  // of the center of sensitive detector volume and the hull box
  // positionOffset = dimensionHull[2] - thicknessHull - scintillatorDimensions[2]
  G4double posZ = _scalIn->position[2] + 9.3 * CLHEP::cm;

  TGEANT::T4CaloModule type = TGEANT::HCAL1;
  buildRegionBox(type, _scalIn, "HCAL1", zSize, moduleDistance, moduleDistance,
      posZ);

  modulePositioning(type, _scalIn, moduleDistance, moduleDistance, posZ);
}

void T4HCAL1::constructMechanicalStructure(void)
{
  // The mechanical structure is built after the modules, so we are allowed to change to positionVector now.
  // detectors.dat local z position of modules: -22.5 cm
  // half thickness of sensitive detector: 50 cm
  // thickness hull of hcal1 module: 0.2 cm
  // ==> z start point of module at -72.7 cm w.r.t. positionVector
  // Drawings of the mechanical structure says: -69.5 cm
  // ==> shift with -3.2 cm
  // x shift from drawings
  positionVector += G4ThreeVector(-mechanicalXShift, 0, -3.2 * CLHEP::cm);

  createRails();
  createWheels();
  createBottomPlate();
  createFrame();
  createLadder();

  // inner structure:
  createCentralStructures();
  createSideClamps();
  createCornerStructures();
}

void T4HCAL1::createRails(void)
{
  G4double height = 130.0 / 2. * CLHEP::mm;
  G4double thickness = 50.0 / 2. * CLHEP::mm;
  G4double distance = 1200.0 / 2 * CLHEP::mm; // middle to middle
  G4double distanceToCenterY = 2181.05 * CLHEP::mm;
  G4double distanceToCenterX = 2490.0 * CLHEP::mm; //from left rail side
  G4double length = (distanceToCenterX + 4.99 * CLHEP::m) / 2.;
  rails_box = new G4Box("rails_box", length, height, thickness);
  rails_log = new G4LogicalVolume(rails_box, materials->iron, "rails_log", 0, 0,
      0, 0);
  rails_log->SetVisAttributes(colour->darkgray);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-distanceToCenterX + length,
              -height - distanceToCenterY, -distance), rails_log,
      "HCAL1_mech_rails_up", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-distanceToCenterX + length,
              -height - distanceToCenterY, distance), rails_log,
      "HCAL1_mech_rails_dn", world_log, 0, 0, checkOverlap);
}

void T4HCAL1::createWheels(void)
{
  G4double radius = 500.0 / 2. * CLHEP::mm;
  G4double thickness = 80.0 / 2. * CLHEP::mm;
  G4double xPos1 = 2030.0 / 2. * CLHEP::mm;
  G4double distanceBetweenWheels = 670.0 * CLHEP::mm;
  G4double yPos = -1931.05 * CLHEP::mm;
  G4double zPos = 1200.0 / 2. * CLHEP::mm;

  wheel_tubs = new G4Tubs("wheel_tubs", 0, radius, thickness, 0,
      360.0 * CLHEP::deg);
  wheel_log = new G4LogicalVolume(wheel_tubs, materials->iron, "wheel_log", 0,
      0, 0, 0);
  wheel_log->SetVisAttributes(colour->black);

  new G4PVPlacement(0, positionVector + G4ThreeVector(-xPos1, yPos, -zPos),
      wheel_log, "HCAL1_mech_wheel1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector + G4ThreeVector(xPos1, yPos, -zPos),
      wheel_log, "HCAL1_mech_wheel2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-xPos1 - distanceBetweenWheels, yPos, -zPos),
      wheel_log, "HCAL1_mech_wheel3", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(xPos1 + distanceBetweenWheels, yPos, -zPos),
      wheel_log, "HCAL1_mech_wheel4", world_log, 0, 0, checkOverlap);

  new G4PVPlacement(0, positionVector + G4ThreeVector(-xPos1, yPos, zPos),
      wheel_log, "HCAL1_mech_wheel5", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector + G4ThreeVector(xPos1, yPos, zPos),
      wheel_log, "HCAL1_mech_wheel6", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-xPos1 - distanceBetweenWheels, yPos, zPos),
      wheel_log, "HCAL1_mech_wheel7", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(xPos1 + distanceBetweenWheels, yPos, zPos),
      wheel_log, "HCAL1_mech_wheel8", world_log, 0, 0, checkOverlap);
}

void T4HCAL1::createBottomPlate(void)
{
  G4double length = 5250.0 / 2. * CLHEP::mm;
  G4double sizeZ = 1255.0 / 2. * CLHEP::mm;
  G4double thicknessBottomPlate = 20.0 / 2. * CLHEP::mm;
  bottomPlate_box = new G4Box("bottomPlate_box", length, thicknessBottomPlate,
      sizeZ);
  bottomPlate_log = new G4LogicalVolume(bottomPlate_box,
      materials->stainlessSteel, "bottomPlate_log", 0, 0, 0, 0);
  bottomPlate_log->SetVisAttributes(colour->blue);

  G4double distanceToCenterY = -1516.05 * CLHEP::mm;
  G4double distanceBetweenWheels = 1200.0 / 2 * CLHEP::mm;
  G4double thicknessWheels = 80.0 / 2. * CLHEP::mm;
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, distanceToCenterY - thicknessBottomPlate,
              distanceBetweenWheels - thicknessWheels - sizeZ), bottomPlate_log,
      "HCAL1_mech_bottomPlate", world_log, 0, 0, checkOverlap);

  G4double barDimensions = 220.0 / 2. * CLHEP::mm;
  G4double barSmallDimensionsZ = 140.0 / 2 * CLHEP::mm;
  G4double distanceBars1 = 320.0 * CLHEP::mm;
  G4double distanceBars2 = 220.0 * CLHEP::mm;
  barBig_box = new G4Box("barBig_box", length, barDimensions, barDimensions);
  bottomBarsBig_log = new G4LogicalVolume(barBig_box, materials->stainlessSteel,
      "bottomBarsBig_log", 0, 0, 0, 0);
  bottomBarsBig_log->SetVisAttributes(colour->blue);

  barSmall_box = new G4Box("barSmall_box", length, barDimensions,
      barSmallDimensionsZ);
  bottomBarsSmall_log = new G4LogicalVolume(barSmall_box,
      materials->stainlessSteel, "bottomBarsSmall_log", 0, 0, 0, 0);
  bottomBarsSmall_log->SetVisAttributes(colour->blue);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              distanceToCenterY - 2. * thicknessBottomPlate - barDimensions,
              -distanceBetweenWheels + thicknessWheels + barDimensions),
      bottomBarsBig_log, "HCAL1_mech_bottomBarsBig1", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              distanceToCenterY - 2. * thicknessBottomPlate - barDimensions,
              -distanceBetweenWheels + thicknessWheels + 3. * barDimensions
                  + distanceBars1), bottomBarsBig_log,
      "HCAL1_mech_bottomBarsBig2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              distanceToCenterY - 2. * thicknessBottomPlate - barDimensions,
              -distanceBetweenWheels + thicknessWheels + 4. * barDimensions
                  + distanceBars1 + distanceBars2 + barSmallDimensionsZ),
      bottomBarsSmall_log, "HCAL1_mech_bottomBarsSmall", world_log, 0, 0,
      checkOverlap);

  G4double motorX = 1200.0 / 2. * CLHEP::mm;
  G4double motorY = 340.0 / 2. * CLHEP::mm;
  G4double distanceBetweenMotorblocks = 2000.0 / 2. * CLHEP::mm;
  motorBlock_box = new G4Box("motorBlock_box", motorX, motorY,
      distanceBetweenWheels - thicknessWheels);
  motorBlock_log = new G4LogicalVolume(motorBlock_box, materials->iron,
      "motorBlock_log", 0, 0, 0, 0);
  motorBlock_log->SetVisAttributes(colour->blue);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-motorX - distanceBetweenMotorblocks,
              distanceToCenterY - 2. * thicknessBottomPlate - 2. * barDimensions
                  - motorY, 0), motorBlock_log, "HCAL1_mech_motorBlock1",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(motorX + distanceBetweenMotorblocks,
              distanceToCenterY - 2. * thicknessBottomPlate - 2. * barDimensions
                  - motorY, 0), motorBlock_log, "HCAL1_mech_motorBlock2",
      world_log, 0, 0, checkOverlap);
}

void T4HCAL1::createFrame(void)
{
  G4double frameHeight = 3032.1 / 2. * CLHEP::mm;
  G4double sizeX = 120.0 / 2. * CLHEP::mm;
  G4double sizeZ = 110.0 / 2. * CLHEP::mm;
  bar_box = new G4Box("bar_box", sizeX, frameHeight - sizeZ, sizeZ);
  bar_log = new G4LogicalVolume(bar_box, materials->stainlessSteel, "bar_log",
      0, 0, 0, 0);
  bar_log->SetVisAttributes(colour->blue);

  G4double sizeBottomPlate = 1255.0 / 2. * CLHEP::mm;
  barUp_box = new G4Box("barUp_box", sizeX, sizeZ, sizeBottomPlate);
  barUp_log = new G4LogicalVolume(barUp_box, materials->stainlessSteel,
      "barUp_log", 0, 0, 0, 0);
  barUp_log->SetVisAttributes(colour->blue);

  G4double plateTopX = 4319.3 / 2. * CLHEP::mm + 2. * sizeX;
  G4double plateTopY = 40.0 / 2. * CLHEP::mm;
  G4double plateTopZ = 1006.0 / 2. * CLHEP::mm;
  topPlate_box = new G4Box("topPlate_box", plateTopX, plateTopY, plateTopZ);
  topPlate_log = new G4LogicalVolume(topPlate_box, materials->stainlessSteel,
      "topPlate_log", 0, 0, 0, 0);
  topPlate_log->SetVisAttributes(colour->blue);

  G4double distToFront = -695.0 * CLHEP::mm;
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(plateTopX - sizeX, -sizeZ, distToFront + sizeZ),
      bar_log, "HCAL1_mech_bars1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-plateTopX + sizeX, -sizeZ, distToFront + sizeZ),
      bar_log, "HCAL1_mech_bars2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(plateTopX - sizeX, -sizeZ,
              distToFront + 2. * plateTopZ - sizeZ), bar_log,
      "HCAL1_mech_bars3", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-plateTopX + sizeX, -sizeZ,
              distToFront + 2. * plateTopZ - sizeZ), bar_log,
      "HCAL1_mech_bars4", world_log, 0, 0, checkOverlap);

  G4double distToBack = 560.0 * CLHEP::mm;
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(plateTopX - sizeX, -sizeZ, distToBack - sizeZ),
      bar_log, "HCAL1_mech_bars5", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-plateTopX + sizeX, -sizeZ, distToBack - sizeZ),
      bar_log, "HCAL1_mech_bars6", world_log, 0, 0, checkOverlap);

  G4double zPosBarUp = -67.5 * CLHEP::mm;
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(plateTopX - sizeX, frameHeight - sizeZ, zPosBarUp),
      barUp_log, "HCAL1_mech_barUp1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-plateTopX + sizeX, frameHeight - sizeZ, zPosBarUp),
      barUp_log, "HCAL1_mech_barUp2", world_log, 0, 0, checkOverlap);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, frameHeight + plateTopY, distToFront + plateTopZ),
      topPlate_log, "HCAL1_mech_topPlate", world_log, 0, 0, checkOverlap);
}

void T4HCAL1::createLadder(void)
{
  G4double frameHeight = 3032.1 / 2. * CLHEP::mm;
  G4double handrailLength = frameHeight + 900.0 / 2. * CLHEP::mm;
  G4double ladderThickness = 40.0 / 2. * CLHEP::mm;
  G4double rungLength = 630.0 / 2. * CLHEP::mm;
  G4double posX = 2490.0 * CLHEP::mm - ladderThickness;
  G4double posZ = -695.0 * CLHEP::mm + 1006.0 / 2. * CLHEP::mm;

  ladderLongBar_box = new G4Box("ladderLongBar_box", ladderThickness,
      handrailLength, ladderThickness);
  ladderLongBar_log = new G4LogicalVolume(ladderLongBar_box, materials->iron,
      "ladderLongBar_log", 0, 0, 0, 0);
  ladderLongBar_log->SetVisAttributes(colour->yellow);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posX, -frameHeight + handrailLength,
              posZ - rungLength - ladderThickness), ladderLongBar_log,
      "HCAL1_mech_ladderLongBar1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posX, -frameHeight + handrailLength,
              posZ + rungLength + ladderThickness), ladderLongBar_log,
      "HCAL1_mech_ladderLongBar2", world_log, 0, 0, checkOverlap);

  ladderStep_box = new G4Box("ladderStep_box", ladderThickness, ladderThickness,
      rungLength);
  ladderStep_log = new G4LogicalVolume(ladderStep_box, materials->iron,
      "ladderStep_log", 0, 0, 0, 0);
  ladderStep_log->SetVisAttributes(colour->yellow);

  G4double stepDistanceY = 290.0 * CLHEP::mm;
  for (G4int i = 0; i < 11; i++)
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(posX,
                -frameHeight + ladderThickness + i * stepDistanceY, posZ),
        ladderStep_log, "HCAL1_mech_ladderStep", world_log, 0, 0, checkOverlap);

  G4double radiusStepHelper = 400.0 / 2. * CLHEP::mm;
  stepHelperRotation[0] = new G4RotationMatrix();
  stepHelperRotation[1] = new G4RotationMatrix();
  stepHelperRotation[0]->rotateY(15.0 * CLHEP::deg);
  stepHelperRotation[1]->rotateY(-15.0 * CLHEP::deg);
  ladderStepHelper_tubs = new G4Tubs("ladderStepHelper_tubs",
      radiusStepHelper - 2. * ladderThickness, radiusStepHelper,
      ladderThickness, 0, 180.0 * CLHEP::deg);
  ladderStepHelper_log = new G4LogicalVolume(ladderStepHelper_tubs,
      materials->iron, "ladderStepHelper_log", 0, 0, 0, 0);
  ladderStepHelper_log->SetVisAttributes(colour->yellow);

  G4double zShift = radiusStepHelper * sin(15.0 * CLHEP::deg);
  G4double posYTop = -frameHeight + 2. * handrailLength - ladderThickness;
  new G4PVPlacement(stepHelperRotation[0],
      positionVector
          + G4ThreeVector(posX + ladderThickness - radiusStepHelper,
              posYTop + ladderThickness,
              posZ - rungLength - ladderThickness - zShift),
      ladderStepHelper_log, "HCAL1_mech_stepHelper", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(stepHelperRotation[1],
      positionVector
          + G4ThreeVector(posX + ladderThickness - radiusStepHelper,
              posYTop + ladderThickness,
              posZ + rungLength + ladderThickness + zShift),
      ladderStepHelper_log, "HCAL1_mech_stepHelper", world_log, 0, 0,
      checkOverlap);

  G4double sizeTopPlate = 4232.4 / 2. * CLHEP::mm + 120.0 * CLHEP::mm;
  G4double differenceTopPlateSizeRail = 106.2 / 2. * CLHEP::mm;
  G4double sizeHandrail = sizeTopPlate - differenceTopPlateSizeRail;
  G4double zPosOnTop = rungLength + 2. * zShift + ladderThickness;
  handrail_box = new G4Box("handrail_box", sizeHandrail, ladderThickness,
      ladderThickness);
  handrail_log = new G4LogicalVolume(handrail_box, materials->iron,
      "handrail_log", 0, 0, 0, 0);
  handrail_log->SetVisAttributes(colour->yellow);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-differenceTopPlateSizeRail, posYTop,
              posZ + zPosOnTop), handrail_log, "HCAL1_mech_handrail1",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-differenceTopPlateSizeRail, posYTop,
              posZ - zPosOnTop), handrail_log, "HCAL1_mech_handrail2",
      world_log, 0, 0, checkOverlap);

  handrailEnd_box = new G4Box("handrailEnd_box", ladderThickness,
      ladderThickness, zPosOnTop - ladderThickness);
  handrailEnd_log = new G4LogicalVolume(handrailEnd_box, materials->iron,
      "handrailEnd_log", 0, 0, 0, 0);
  handrailEnd_log->SetVisAttributes(colour->yellow);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-sizeTopPlate + ladderThickness, posYTop, posZ),
      handrailEnd_log, "HCAL1_mech_handrailEnd", world_log, 0, 0, checkOverlap);

  G4double sizeHandrailUp = 820.0 / 2. * CLHEP::mm;
  handrailUp_box = new G4Box("handrailUp_box", ladderThickness, sizeHandrailUp,
      ladderThickness);
  handrailUp_log = new G4LogicalVolume(handrailUp_box, materials->iron,
      "handrailUp_log", 0, 0, 0, 0);
  handrailUp_log->SetVisAttributes(colour->yellow);
  G4double sizeBetweenRails = 800.0 * CLHEP::mm;
  for (G4int i = 0; i < 6; i++) {
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(
                -sizeTopPlate + ladderThickness + i * sizeBetweenRails,
                posYTop - ladderThickness - sizeHandrailUp, posZ + zPosOnTop),
        handrailUp_log, "HCAL1_mech_handrailUp", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(
                -sizeTopPlate + ladderThickness + i * sizeBetweenRails,
                posYTop - ladderThickness - sizeHandrailUp, posZ - zPosOnTop),
        handrailUp_log, "HCAL1_mech_handrailUp", world_log, 0, 0, checkOverlap);
  }

  // security cage
  G4double radius = 880.0 / 2. * CLHEP::mm;
  G4double thicknessRing = 4.0 / 2. * CLHEP::mm;
  rotationLadderRings = new G4RotationMatrix();
  rotationLadderRings->rotateX(90 * CLHEP::deg);
  ladderRing_tubs = new G4Tubs("ladderRing_tubs", radius - 2. * thicknessRing,
      radius, ladderThickness, 225.0 * CLHEP::deg, 270.0 * CLHEP::deg);
  ladderRing_log = new G4LogicalVolume(ladderRing_tubs,
      materials->aluminium_noOptical, "ladderRing_log", 0, 0, 0, 0);
  ladderRing_log->SetVisAttributes(colour->yellow);

  G4double zPosLowest = -frameHeight + 790.0 * CLHEP::mm + ladderThickness;
  G4double distanceCageElements = 550.525 * CLHEP::mm;
  cageBars_box = new G4Box("cageBars_box", thicknessRing,
      3. * distanceCageElements + ladderThickness, ladderThickness);
  cageBars_log = new G4LogicalVolume(cageBars_box,
      materials->aluminium_noOptical, "cageBars_log", 0, 0, 0, 0);
  cageBars_log->SetVisAttributes(colour->yellow);
  G4double xPosCageBar = radius - thicknessRing;
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posX + 0.7 * radius + xPosCageBar,
              zPosLowest + 3. * distanceCageElements, posZ), cageBars_log,
      "HCAL1_mech_cageBars1", world_log, 0, 0, checkOverlap);
  rotationCage[0] = new G4RotationMatrix();
  rotationCage[0]->rotateY(67.0 * CLHEP::deg);
  rotationCage[1] = new G4RotationMatrix();
  rotationCage[1]->rotateY(-67.0 * CLHEP::deg);
  rotationCage[2] = new G4RotationMatrix();
  rotationCage[2]->rotateY(113.0 * CLHEP::deg);
  rotationCage[3] = new G4RotationMatrix();
  rotationCage[3]->rotateY(-113.0 * CLHEP::deg);
  G4double z1 = posZ + xPosCageBar * sin(67.0 * CLHEP::deg);
  G4double z2 = posZ - xPosCageBar * sin(67.0 * CLHEP::deg);
  G4double x1 = posX + 0.7 * radius
      + xPosCageBar * cos(67.0 * CLHEP::deg);
  G4double x2 = posX + 0.7 * radius
      - xPosCageBar * cos(67.0 * CLHEP::deg);
  new G4PVPlacement(rotationCage[0],
      positionVector
          + G4ThreeVector(x1, zPosLowest + 3. * distanceCageElements, z1),
      cageBars_log, "HCAL1_mech_cageBars2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationCage[1],
      positionVector
          + G4ThreeVector(x1, zPosLowest + 3. * distanceCageElements, z2),
      cageBars_log, "HCAL1_mech_cageBars2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationCage[2],
      positionVector
          + G4ThreeVector(x2, zPosLowest + 3. * distanceCageElements, z1),
      cageBars_log, "HCAL1_mech_cageBars2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationCage[3],
      positionVector
          + G4ThreeVector(x2, zPosLowest + 3. * distanceCageElements, z2),
      cageBars_log, "HCAL1_mech_cageBars2", world_log, 0, 0, checkOverlap);

  for (G4int i = 0; i < 7; i++)
    new G4PVPlacement(rotationLadderRings,
        positionVector
            + G4ThreeVector(posX + 0.7 * radius,
                zPosLowest + i * distanceCageElements, posZ), ladderRing_log,
        "HCAL1_mech_ladderRing", world_log, 0, 0, false);
}

void T4HCAL1::createCentralStructures(void)
{
  G4double sizeX = 1480.0 / 2. * CLHEP::mm;
  G4double sizeY = 600.0 / 2. * CLHEP::mm; //4 plates
  G4double sizeZ = 30.0 / 2. * CLHEP::mm;
  frontPlate_box = new G4Box("frontPlate_box", sizeX, sizeY, sizeZ);
  frontPlate_log = new G4LogicalVolume(frontPlate_box, materials->iron,
      "frontPlate_log", 0, 0, 0, 0);
  frontPlate_log->SetVisAttributes(colour->black);

  G4double verticalPlateX = 50.0 / 2. * CLHEP::mm;
  verticalPlate_box = new G4Box("verticalPlate_box", verticalPlateX, sizeY,
      sizeZ);
  verticalPlate_log = new G4LogicalVolume(verticalPlate_box, materials->iron,
      "verticalPlate_log", 0, 0, 0, 0);
  verticalPlate_log->SetVisAttributes(colour->black);

  G4double xLengthHole = 606.75 * CLHEP::mm; //4*module 15.15cm + 0.5*dist 0.15cm/2
  G4double yLengthHole = 303.75 * CLHEP::mm; //2*module 15.15cm + 0.5*dist 0.15cm/2
  G4double distToFront = -695.0 * CLHEP::mm;
  G4double distToBack = 560.0 * CLHEP::mm;
  G4double xPosOuterFrame = (4319.3 / 2. + 120.0) * CLHEP::mm;

  G4double x1 = mechanicalXShift + xLengthHole + verticalPlateX;
  G4double x2 = mechanicalXShift - xLengthHole - verticalPlateX;
  G4double x3 = xPosOuterFrame - verticalPlateX;
  G4double x4 = -xPosOuterFrame + verticalPlateX;

  shortPlateJura_box = new G4Box("shortPlateJura_box",
      (x3 - x1) / 2. - verticalPlateX, sizeZ, sizeZ);
  shortPlateJura_log = new G4LogicalVolume(shortPlateJura_box, materials->iron,
      "shortPlateJura_log", 0, 0, 0, 0);
  shortPlateJura_log->SetVisAttributes(colour->black);
  shortPlateSaleve_box = new G4Box("shortPlateSaleve_box",
      (x2 - x4) / 2. - verticalPlateX, sizeZ, sizeZ);
  shortPlateSaleve_log = new G4LogicalVolume(shortPlateSaleve_box,
      materials->iron, "shortPlateSaleve_log", 0, 0, 0, 0);
  shortPlateSaleve_log->SetVisAttributes(colour->black);

  longPlate_box = new G4Box("longPlate_box", xPosOuterFrame, 2. * sizeZ, sizeZ);
  longPlate_log = new G4LogicalVolume(longPlate_box, materials->iron,
      "longPlate_log", 0, 0, 0, 0);
  longPlate_log->SetVisAttributes(colour->black);

  G4double zPos;
  for (G4int i = 0; i < 2; i++) {
    if (i == 0)
      zPos = distToFront - sizeZ;
    else
      zPos = distToBack + sizeZ;

    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(mechanicalXShift, yLengthHole + sizeY, zPos),
        frontPlate_log, "HCAL1_mech_frontPlate", world_log, 0, 0, checkOverlap);

    // vertical
    new G4PVPlacement(0, positionVector + G4ThreeVector(x1, 0, zPos),
        verticalPlate_log, "HCAL1_mech_verticalPlate1", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0, positionVector + G4ThreeVector(x2, 0, zPos),
        verticalPlate_log, "HCAL1_mech_verticalPlate2", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0, positionVector + G4ThreeVector(x3, 0, zPos),
        verticalPlate_log, "HCAL1_mech_verticalPlate3", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0, positionVector + G4ThreeVector(x4, 0, zPos),
        verticalPlate_log, "HCAL1_mech_verticalPlate4", world_log, 0, 0,
        checkOverlap);

    // jura
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x1 + x3) / 2., sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura1", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x1 + x3) / 2., -sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura2", world_log, 0, 0,
        checkOverlap);

    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x1 + x3) / 2., sizeY / 2. + sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura3", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x1 + x3) / 2., sizeY / 2. + -sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura4", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x1 + x3) / 2., -sizeY / 2. + sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura5", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x1 + x3) / 2., -sizeY / 2. + -sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura6", world_log, 0, 0,
        checkOverlap);

    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x1 + x3) / 2., sizeY - sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura7", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x1 + x3) / 2., -sizeY + sizeZ, zPos),
        shortPlateJura_log, "HCAL1_mech_shortPlateJura8", world_log, 0, 0,
        checkOverlap);

    // saleve
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x2 + x4) / 2., sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve1", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x2 + x4) / 2., -sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve2", world_log, 0, 0,
        checkOverlap);

    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x2 + x4) / 2., sizeY / 2. + sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve3", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x2 + x4) / 2., sizeY / 2. + -sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve4", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x2 + x4) / 2., -sizeY / 2. + sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve5", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector((x2 + x4) / 2., -sizeY / 2. + -sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve6", world_log, 0, 0,
        checkOverlap);

    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x2 + x4) / 2., sizeY - sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve7", world_log, 0, 0,
        checkOverlap);
    new G4PVPlacement(0,
        positionVector + G4ThreeVector((x2 + x4) / 2., -sizeY + sizeZ, zPos),
        shortPlateSaleve_log, "HCAL1_mech_shortPlateSaleve8", world_log, 0, 0,
        checkOverlap);

    // long
    new G4PVPlacement(0, positionVector + G4ThreeVector(0, -1.5 * sizeY, zPos),
        longPlate_log, "HCAL1_mech_longPlate", world_log, 0, 0, checkOverlap);
  }
}

void T4HCAL1::createSideClamps(void)
{
  G4double radiusScrew = 20.0 / 2. * CLHEP::mm;
  G4double radiusPlate = 90.0 / 2. * CLHEP::mm;
  G4double thicknessPlate = 15.0 / 2. * CLHEP::mm;

  clampScrew_tubs = new G4Tubs("clampScrew_tubs", 0, radiusScrew,
      mechanicalXShift, 0, 360.0 * CLHEP::deg);
  clampPlate_tubs = new G4Tubs("clampPlate_tubs", 0, radiusPlate,
      thicknessPlate / 2, 0, 360.0 * CLHEP::deg);
  clamp_union = new G4UnionSolid("clamp_union", clampScrew_tubs,
      clampPlate_tubs, 0,
      G4ThreeVector(0, 0, mechanicalXShift - thicknessPlate));
  clamp_log = new G4LogicalVolume(clamp_union, materials->stainlessSteel,
      "clamp_log", 0, 0, 0, 0);
  clamp_log->SetVisAttributes(colour->black);
  rotationClamb = new G4RotationMatrix;
  rotationClamb->rotateY(-90.0 * CLHEP::deg);

  G4double xPos = -4319.3 / 2. * CLHEP::mm + mechanicalXShift;
  G4double hcalModuleSize = 15.15 * CLHEP::cm;
  G4double zPos;
  for (G4int i = 0; i < 2; i++) {
    if (i == 0)
      zPos = (-695.0 + 110.0 / 2.) * CLHEP::mm;
    else
      zPos = (-695.0 + 1006.0 - 110.0 / 2.) * CLHEP::mm;

    for (G4int j = 0; j < 20; j++) {
      new G4PVPlacement(rotationClamb,
          positionVector
              + G4ThreeVector(xPos, (-9.5 + j) * hcalModuleSize, zPos),
          clamp_log, "HCAL1_mech_clamp", world_log, 0, 0, checkOverlap);
    }
  }
}

void T4HCAL1::createCornerStructures(void)
{
  G4double moduleSize = 15.15 * CLHEP::cm;
  G4double moduleLength = 119.0 / 2. * CLHEP::cm;
  G4double thickness = 20.0 * CLHEP::mm;
  G4double posXJura = 4319.3 / 2. * CLHEP::mm;
  G4double posXSaleve = -4319.3 / 2. * CLHEP::mm + 2. * mechanicalXShift;
  G4double posZ = -695.0 * CLHEP::mm + moduleLength;

  bigLongFull_box = new G4Box("bigLongFull_box", 2 * moduleSize, moduleSize,
      moduleLength);
  bigLongHole_box = new G4Box("bigLongHole_box", 2 * moduleSize - thickness,
      moduleSize - thickness, moduleLength + 1.0 * CLHEP::mm);
  bigLong_box = new G4SubtractionSolid("bigLong_box", bigLongFull_box,
      bigLongHole_box);
  bigLong_log = new G4LogicalVolume(bigLong_box, materials->aluminium_noOptical,
      "bigLong_log", 0, 0, 0, 0);
  bigLong_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - 2 * moduleSize, -9. * moduleSize, posZ),
      bigLong_log, "HCAL1_mech_bigLong", world_log, 0, 0, checkOverlap);

  bigShortFull_box = new G4Box("bigShortFull_box", moduleSize, moduleSize,
      moduleLength);
  bigShortHole_box = new G4Box("bigShortHole_box", moduleSize - thickness,
      moduleSize - thickness, moduleLength + 1.0 * CLHEP::mm);
  bigShort_box = new G4SubtractionSolid("bigShort_box", bigShortFull_box,
      bigShortHole_box);
  bigShort_log = new G4LogicalVolume(bigShort_box,
      materials->aluminium_noOptical, "bigShort_log", 0, 0, 0, 0);
  bigShort_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - moduleSize, -7. * moduleSize, posZ),
      bigShort_log, "HCAL1_mech_bigShort", world_log, 0, 0, checkOverlap);

  smallLongFull_box = new G4Box("smallLongFull_box", 2 * moduleSize,
      moduleSize / 2., moduleLength);
  smallLongHole_box = new G4Box("smallLongHole_box", 2 * moduleSize - thickness,
      moduleSize / 2. - thickness, moduleLength + 1.0 * CLHEP::mm);
  smallLong_box = new G4SubtractionSolid("smallLong_box", smallLongFull_box,
      smallLongHole_box);
  smallLong_log = new G4LogicalVolume(smallLong_box,
      materials->aluminium_noOptical, "smallLong_log", 0, 0, 0, 0);
  smallLong_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + 2 * moduleSize, -9.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + 2 * moduleSize, -8.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + 2 * moduleSize, 9.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong3", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + 2 * moduleSize, 8.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong4", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - 2 * moduleSize, 9.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong5", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - 2 * moduleSize, 8.5 * moduleSize, posZ),
      smallLong_log, "HCAL1_mech_smallLong6", world_log, 0, 0, checkOverlap);

  smallShortFull_box = new G4Box("smallShortFull_box", moduleSize,
      moduleSize / 2., moduleLength);
  smallShortHole_box = new G4Box("smallShortHole_box", moduleSize - thickness,
      moduleSize / 2. - thickness, moduleLength + 1.0 * CLHEP::mm);
  smallShort_box = new G4SubtractionSolid("smallShort_box", smallShortFull_box,
      smallShortHole_box);
  smallShort_log = new G4LogicalVolume(smallShort_box,
      materials->aluminium_noOptical, "smallShort_log", 0, 0, 0, 0);
  smallShort_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + moduleSize, -7.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort1", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + moduleSize, -6.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort2", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + moduleSize, 7.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort3", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXSaleve + moduleSize, 6.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort4", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - moduleSize, 7.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort5", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(posXJura - moduleSize, 6.5 * moduleSize, posZ),
      smallShort_log, "HCAL1_mech_smallShort6", world_log, 0, 0, checkOverlap);
}

T4HCAL1::T4HCAL1(vector<T4SCAL>& _in, const T4SDetector* _inDet) :
    T4CalorimeterBackend(_in, _inDet)
{
  caloName = "HCAL1";

  mechanicalXShift = 78.8 / 2. * CLHEP::mm;

  rails_box = NULL;
  rails_log = NULL;

  // createWheels
  wheel_tubs = NULL;
  wheel_log = NULL;

  // createBottomPlate
  bottomPlate_box = NULL;
  barBig_box = NULL;
  barSmall_box = NULL;
  motorBlock_box = NULL;
  bottomPlate_log = NULL;
  bottomBarsBig_log = NULL;
  bottomBarsSmall_log = NULL;
  motorBlock_log = NULL;

  // createFrame
  bar_box = NULL;
  barUp_box = NULL;
  topPlate_box = NULL;
  bar_log = NULL;
  barUp_log = NULL;
  topPlate_log = NULL;

  // createLadder
  ladderLongBar_box = NULL;
  ladderStep_box = NULL;
  for (G4int i = 0; i < 2; i++)
    stepHelperRotation[i] = NULL;
  ladderStepHelper_tubs = NULL;
  handrail_box = NULL;
  handrailEnd_box = NULL;
  handrailUp_box = NULL;
  rotationLadderRings = NULL;
  ladderRing_tubs = NULL;
  for (G4int i = 0; i < 4; i++)
    rotationCage[i] = NULL;
  cageBars_box = NULL;
  ladderLongBar_log = NULL;
  ladderStep_log = NULL;
  ladderStepHelper_log = NULL;
  handrail_log = NULL;
  handrailEnd_log = NULL;
  handrailUp_log = NULL;
  ladderRing_log = NULL;
  cageBars_log = NULL;

  // createCentralStructures
  frontPlate_box = NULL;
  verticalPlate_box = NULL;
  shortPlateJura_box = NULL;
  shortPlateSaleve_box = NULL;
  longPlate_box = NULL;
  frontPlate_log = NULL;
  verticalPlate_log = NULL;
  shortPlateJura_log = NULL;
  shortPlateSaleve_log = NULL;
  longPlate_log = NULL;

  // createSideClamps
  clampScrew_tubs = NULL;
  clampPlate_tubs = NULL;
  clamp_union = NULL;
  clamp_log = NULL;
  rotationClamb = NULL;

  // createCornerStructures
  bigLongFull_box = NULL;
  bigLongHole_box = NULL;
  bigLong_box = NULL;
  bigLong_log = NULL;

  bigShortFull_box = NULL;
  bigShortHole_box = NULL;
  bigShort_box = NULL;
  bigShort_log = NULL;

  smallLongFull_box = NULL;
  smallLongHole_box = NULL;
  smallLong_box = NULL;
  smallLong_log = NULL;

  smallShortFull_box = NULL;
  smallShortHole_box = NULL;
  smallShort_box = NULL;
  smallShort_log = NULL;
}

T4HCAL1::~T4HCAL1(void)
{
  if (rails_box != NULL)
    delete rails_box;
  if (rails_log != NULL)
    delete rails_log;

  // createWheels
  if (wheel_tubs != NULL)
    delete wheel_tubs;
  if (wheel_log != NULL)
    delete wheel_log;

  // createBottomPlate
  if (bottomPlate_box != NULL)
    delete bottomPlate_box;
  if (barBig_box != NULL)
    delete barBig_box;
  if (barSmall_box != NULL)
    delete barSmall_box;
  if (motorBlock_box != NULL)
    delete motorBlock_box;
  if (bottomPlate_log != NULL)
    delete bottomPlate_log;
  if (bottomBarsBig_log != NULL)
    delete bottomBarsBig_log;
  if (bottomBarsSmall_log != NULL)
    delete bottomBarsSmall_log;
  if (motorBlock_log != NULL)
    delete motorBlock_log;

  // createFrame
  if (bar_box != NULL)
    delete bar_box;
  if (barUp_box != NULL)
    delete barUp_box;
  if (topPlate_box != NULL)
    delete topPlate_box;
  if (bar_log != NULL)
    delete bar_log;
  if (barUp_log != NULL)
    delete barUp_log;
  if (topPlate_log != NULL)
    delete topPlate_log;

  // createLadder
  if (ladderLongBar_box != NULL)
    delete ladderLongBar_box;
  if (ladderStep_box != NULL)
    delete ladderStep_box;
  for (G4int i = 0; i < 2; i++)
    if (stepHelperRotation[i] != NULL)
      delete stepHelperRotation[i];
  if (ladderStepHelper_tubs != NULL)
    delete ladderStepHelper_tubs;
  if (handrail_box != NULL)
    delete handrail_box;
  if (handrailEnd_box != NULL)
    delete handrailEnd_box;
  if (handrailUp_box != NULL)
    delete handrailUp_box;
  if (rotationLadderRings != NULL)
    delete rotationLadderRings;
  if (ladderRing_tubs != NULL)
    delete ladderRing_tubs;
  for (G4int i = 0; i < 4; i++)
    if (rotationCage[i] != NULL)
      delete rotationCage[i];
  if (cageBars_box != NULL)
    delete cageBars_box;
  if (ladderLongBar_log != NULL)
    delete ladderLongBar_log;
  if (ladderStep_log != NULL)
    delete ladderStep_log;
  if (ladderStepHelper_log != NULL)
    delete ladderStepHelper_log;
  if (handrail_log != NULL)
    delete handrail_log;
  if (handrailEnd_log != NULL)
    delete handrailEnd_log;
  if (handrailUp_log != NULL)
    delete handrailUp_log;
  if (ladderRing_log != NULL)
    delete ladderRing_log;
  if (cageBars_log != NULL)
    delete cageBars_log;

  // createCentralStructures
  if (frontPlate_box != NULL)
    delete frontPlate_box;
  if (verticalPlate_box != NULL)
    delete verticalPlate_box;
  if (shortPlateJura_box != NULL)
    delete shortPlateJura_box;
  if (shortPlateSaleve_box != NULL)
    delete shortPlateSaleve_box;
  if (longPlate_box != NULL)
    delete longPlate_box;
  if (frontPlate_log != NULL)
    delete frontPlate_log;
  if (verticalPlate_log != NULL)
    delete verticalPlate_log;
  if (shortPlateJura_log != NULL)
    delete shortPlateJura_log;
  if (shortPlateSaleve_log != NULL)
    delete shortPlateSaleve_log;
  if (longPlate_log != NULL)
    delete longPlate_log;

  // createSideClamps
  if (clampScrew_tubs != NULL)
    delete clampScrew_tubs;
  if (clampPlate_tubs != NULL)
    delete clampPlate_tubs;
  if (clamp_union != NULL)
    delete clamp_union;
  if (clamp_log != NULL)
    delete clamp_log;
  if (rotationClamb != NULL)
    delete rotationClamb;

// createCornerStructures
  if (bigLongFull_box != NULL)
    delete bigLongFull_box;
  if (bigLongHole_box != NULL)
    delete bigLongHole_box;
  if (bigLong_box != NULL)
    delete bigLong_box;
  if (bigLong_log != NULL)
    delete bigLong_log;

  if (bigShortFull_box != NULL)
    delete bigShortFull_box;
  if (bigShortHole_box != NULL)
    delete bigShortHole_box;
  if (bigShort_box != NULL)
    delete bigShort_box;
  if (bigShort_log != NULL)
    delete bigShort_log;

  if (smallLongFull_box != NULL)
    delete smallLongFull_box;
  if (smallLongHole_box != NULL)
    delete smallLongHole_box;
  if (smallLong_box != NULL)
    delete smallLong_box;
  if (smallLong_log != NULL)
    delete smallLong_log;

  if (smallShortFull_box != NULL)
    delete smallShortFull_box;
  if (smallShortHole_box != NULL)
    delete smallShortHole_box;
  if (smallShort_box != NULL)
    delete smallShort_box;
  if (smallShort_log != NULL)
    delete smallShort_log;
}
