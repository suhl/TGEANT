#include "T4ShashlikModule.hh"

T4ShashlikModule::T4ShashlikModule(string caloName)
{
  setDetectorName("SHASHLIK");
  nLayer = 155;
  thicknessScintillator = 1.5 * CLHEP::mm;
  thicknessAbsorber = 0.8 * CLHEP::mm;

  materialScintillator = materials->polystyrene;
  materialAbsorber = materials->lead;

  dimensionHull[0] = 38.3 / 2 * CLHEP::mm;
  dimensionHull[1] = 38.3 / 2 * CLHEP::mm;
  dimensionHull[2] = 45.0 / 2 * CLHEP::cm;
  thicknessHull = 0.05 * CLHEP::mm;

  fiberDiameter = 1.4 * CLHEP::mm;
  rodDiameter = 1.5 * CLHEP::mm;

  scintillatorDimensions[0] = dimensionHull[0] - thicknessHull;
  scintillatorDimensions[1] = dimensionHull[1] - thicknessHull;
  scintillatorDimensions[2] = (thicknessScintillator + thicknessAbsorber) / 2
      * nLayer;

  fiber_tubs = NULL;
  fiber_log = NULL;
  rod_tubs = NULL;
  rod_log = NULL;

  static T4RegionInformation* regionInfo1 = new T4RegionInformation(materialScintillator, thicknessScintillator, materialAbsorber, thicknessAbsorber);
  static T4RegionInformation* regionInfo2 = new T4RegionInformation(materialScintillator, thicknessScintillator, materialAbsorber, thicknessAbsorber);
  if (caloName == "ECAL1")
    regionModule = regionManager->getECAL1ShashlikRegion(regionInfo1);
  else if (caloName == "ECAL2")
    regionModule = regionManager->getECAL2ShashlikRegion(regionInfo2);
  else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__,
        "T4ShashlikModule: Name of the calorimeter does not match ECAL1 or ECAL2. Set the region of this module to ECAL1 as default.");
    regionModule = regionManager->getECAL1ShashlikRegion(regionInfo1);
  }

  energyScale = 0.974;
}

T4ShashlikModule::~T4ShashlikModule(void)
{
  if (fiber_tubs != NULL)
    delete fiber_tubs;
  if (fiber_log != NULL)
    delete fiber_log;
  if (rod_tubs != NULL)
    delete rod_tubs;
  if (rod_log != NULL)
    delete rod_log;

  for (unsigned int i = 0; i < absorber_sub.size(); i++)
    delete absorber_sub.at(i);
  absorber_sub.clear();
}

void T4ShashlikModule::construct(G4LogicalVolume* world_log)
{
  G4VisAttributes* hull = colour->mediumblue;
  if (!hullVisualization) {
    hull = colour->invisible;
  }

  outerHull_box = new G4Box("outerHull_box", dimensionHull[0], dimensionHull[1],
      dimensionHull[2]);
  outerHull_log = new G4LogicalVolume(outerHull_box, materials->iron,
      "calorimeter_outerHull_log", 0, 0, 0, 0);
  outerHull_log->SetVisAttributes(hull);

  new G4PVPlacement(0, positionVector,
      outerHull_log, detName + intToStr(channelID), world_log, 0, 0,
      checkOverlap);

  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;

  innerHull_box = new G4Box("innerHull_box", dimensionHull[0] - thicknessHull,
      dimensionHull[1] - thicknessHull, dimensionHull[2] - thicknessHull);
  innerHull_log = new G4LogicalVolume(innerHull_box, materials->air_noOptical,
      "innerHull_log", 0, 0, 0, 0);
  innerHull_log->SetVisAttributes(colour->invisible);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerHull_log, "sampling_air",
      outerHull_log, 0, 0, /*checkOverlap*/false);

  scintillator_box = new G4Box("scintillator_box", scintillatorDimensions[0],
      scintillatorDimensions[1], scintillatorDimensions[2]);
  scintillator_log = new G4LogicalVolume(scintillator_box,
      materialScintillator, "calorimeter_log", 0, 0, 0, 0);

  rod_tubs = new G4Tubs("rod_tubs", 0, rodDiameter / 2,
      scintillatorDimensions[2], 0, 360.0 * CLHEP::deg);
  rod_log = new G4LogicalVolume(rod_tubs, materials->stainlessSteel, "rod_log",
      0, 0, 0, 0);
  fiber_tubs = new G4Tubs("fiber_tubs", 0, fiberDiameter / 2,
      scintillatorDimensions[2], 0, 360.0 * CLHEP::deg);
  fiber_log = new G4LogicalVolume(fiber_tubs, materials->plexiglass_noOptical,
      "rod_log", 0, 0, 0, 0);

  if (hullVisualization) {
    scintillator_log->SetVisAttributes(colour->invisible);
    rod_log->SetVisAttributes(colour->invisible);
    fiber_log->SetVisAttributes(colour->invisible);
  } else {
    scintillator_log->SetVisAttributes(colour->yellow);
    rod_log->SetVisAttributes(colour->darkgray);
    fiber_log->SetVisAttributes(colour->blue);
  }

  new G4PVPlacement(0,
      G4ThreeVector(0, 0,
          -dimensionHull[2] + thicknessHull + scintillatorDimensions[2]),
      scintillator_log, "sampling_sd", innerHull_log, 0, 0, /*checkOverlap*/
      false);

  G4double rodPos = scintillatorDimensions[0] / 2;
  G4double fiberPos = rodPos / 2;

  for (int x = 0; x < 2; x++)
    for (int y = 0; y < 2; y++)
      new G4PVPlacement(0,
          G4ThreeVector(rodPos * (-1. + 2. * x), rodPos * (-1. + 2. * y), 0),
          rod_log, "rod_phys", scintillator_log, 0, 0, /*checkOverlap*/false);

  absorber_box = new G4Box("absorber_box", scintillatorDimensions[0],
      scintillatorDimensions[1], thicknessAbsorber / 2);

  absorber_sub.push_back(
      new G4SubtractionSolid("absorber_sub", absorber_box, rod_tubs, 0,
          G4ThreeVector(rodPos, rodPos, 0)));
  absorber_sub.push_back(
      new G4SubtractionSolid("absorber_sub", absorber_sub.back(), rod_tubs, 0,
          G4ThreeVector(-rodPos, rodPos, 0)));
  absorber_sub.push_back(
      new G4SubtractionSolid("absorber_sub", absorber_sub.back(), rod_tubs, 0,
          G4ThreeVector(rodPos, -rodPos, 0)));
  absorber_sub.push_back(
      new G4SubtractionSolid("absorber_sub", absorber_sub.back(), rod_tubs, 0,
          G4ThreeVector(-rodPos, -rodPos, 0)));

  for (int x = 0; x < 4; x++)
    for (int y = 0; y < 4; y++) {
      new G4PVPlacement(0,
          G4ThreeVector(fiberPos * (-3. + 2. * x), fiberPos * (-3. + 2. * y),
              0), fiber_log, "fiber_phys", scintillator_log, 0, 0,
          /*checkOverlap*/false);
      absorber_sub.push_back(
          new G4SubtractionSolid("absorber_sub", absorber_sub.back(),
              fiber_tubs, 0,
              G4ThreeVector(fiberPos * (-3. + 2. * x),
                  fiberPos * (-3. + 2. * y), 0)));
    }

  for (G4int i = 0; i < nLayer; i++) {
    absorber_log.push_back(
        new G4LogicalVolume(absorber_sub.back(), materialAbsorber,
            "calorimeter_absorber_log", 0, 0, 0, 0));

    if (hullVisualization)
      absorber_log.back()->SetVisAttributes(colour->invisible);
    else
      absorber_log.back()->SetVisAttributes(colour->black);

    new G4PVPlacement(0,
        G4ThreeVector(0, 0,
            -scintillatorDimensions[2] + thicknessAbsorber / 2
                + i * (thicknessScintillator + thicknessAbsorber)),
        absorber_log.back(), "sampling_absorber", scintillator_log, 0, 0,
        /*checkOverlap*/false);
  }
}
