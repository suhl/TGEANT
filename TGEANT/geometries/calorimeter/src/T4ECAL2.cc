#include "T4ECAL2.hh"

void T4ECAL2::constructRegions(T4SCAL* _scalIn)
{
  G4double moduleDistance = 3.830 * CLHEP::cm;
  G4double zSize = 45.0 / 2 * CLHEP::cm;
  G4double posZ = _scalIn->position[2];

  // This value is for Shashlik modules:
  // In detectors.dat the given z size is bigger than the length of the modules.
  // So we have to subtract this difference to keep the correct z positioning.
  // Second number:
  // this positionOffset value defines the difference between the z-position
  // of the center of sensitive detector volume and the hull box
  // positionOffset = dimensionHull[2] - thicknessHull - scintillatorDimensions[2]

  G4double zDifference = 0;

  TGEANT::T4CaloModule type;
  if (_scalIn->moduleName == "GAMS") {
    type = TGEANT::GAMS;
  } else if (_scalIn->moduleName == "RHGAMS") {
    type = TGEANT::GAMSRH;
  } else if (_scalIn->moduleName == "SHASHLIK") {
    type = TGEANT::SHASHLIK;
    zDifference = -1.85 * CLHEP::cm + 4.67 * CLHEP::cm;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
    "T4ECAL2::constructRegions: Unknown Module name: '" + _scalIn->moduleName + "'. Skip this entry! Please check your calo.xml file!");
    return;
  }

  posZ = _scalIn->position[2] + zDifference;
  buildRegionBox(type, _scalIn, "ECAL2", zSize, moduleDistance, moduleDistance,
      posZ);
  modulePositioning(type, _scalIn, moduleDistance, moduleDistance, posZ);
}

void T4ECAL2::constructMechanicalStructure()
{
  tableDimensions[0] = 3.0 * CLHEP::m;
  tableDimensions[1] = 25.0 * CLHEP::cm;
  tableDimensions[2] = 0.5 * CLHEP::m;

  wheelDimensions[0] = 540.0 * CLHEP::mm; // diameter
  wheelDimensions[1] = 200.0 * CLHEP::mm; // length

  frameSize[0] = 5.0 * CLHEP::cm;
  frameSize[1] = 10.0 * CLHEP::cm;

  structureSize[0] = 250.0 / 2 * CLHEP::cm;
  structureSize[1] = 185.0 / 2 * CLHEP::cm;

  //table
  table_box = new G4Box("", tableDimensions[0] / 2, tableDimensions[1] / 2,
      tableDimensions[2] / 2);
  table_log = new G4LogicalVolume(table_box, materials->aluminium_noOptical, "", 0, 0, 0,
      0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -(structureSize[1] + tableDimensions[1] / 2), 0),
      table_log, "", world_log, 0, 0, 0);
  table_log->SetVisAttributes(colour->black);

  //frame_box

  frame_box[0] = new G4Box("", frameSize[1] / 2, structureSize[1],
      frameSize[0] / 2);
  frame_box[1] = new G4Box("", structureSize[0] + frameSize[1] + 5 * CLHEP::cm,
      frameSize[1] / 2, frameSize[0] / 2);

  for (G4int i = 0; i < 4; i++) {
    frame_log[i] = new G4LogicalVolume(frame_box[0], materials->aluminium_noOptical, "",
        0, 0, 0, 0);
  }
  frame_log[4] = new G4LogicalVolume(frame_box[1], materials->aluminium_noOptical, "", 0,
      0, 0, 0);
  frame_log[5] = new G4LogicalVolume(frame_box[1], materials->aluminium_noOptical, "", 0,
      0, 0, 0);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(structureSize[0] + frameSize[1] / 2 + 5.0 * CLHEP::cm,
              0, frameSize[1]), frame_log[0], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(
              -structureSize[0] - frameSize[1] / 2 - 5.0 * CLHEP::cm, 0,
              frameSize[1]), frame_log[1], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(
              +structureSize[0] + frameSize[1] / 2 + 5.0 * CLHEP::cm, 0, 0),
      frame_log[2], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(
              -structureSize[0] - frameSize[1] / 2 - 5.0 * CLHEP::cm, 0, 0),
      frame_log[3], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, structureSize[1] + frameSize[1] / 2, frameSize[1]),
      frame_log[4], "", world_log, 0, 0, 0);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, structureSize[1] + frameSize[1] / 2, 0),
      frame_log[5], "", world_log, 0, 0, 0);

  for (G4int i = 0; i < 6; i++)
    frame_log[i]->SetVisAttributes(colour->black);
}

T4ECAL2::T4ECAL2(vector<T4SCAL>& _in, const T4SDetector* _inDet) :
    T4CalorimeterBackend(_in, _inDet)
{
  caloName = "ECAL2";

  tableSub_box = NULL;
  tableSub_log = NULL;
  table_box = NULL;
  table_log = NULL;

  frame_box[0] = NULL;
  frame_box[1] = NULL;
  frame_log[0] = NULL;
  frame_log[1] = NULL;
  frame_log[2] = NULL;
  frame_log[3] = NULL;
  frame_log[4] = NULL;
  frame_log[5] = NULL;
}

T4ECAL2::~T4ECAL2(void)
{
  if (useMechanicalStructure) {
    delete tableSub_box;
    delete tableSub_log;
    delete table_box;
    delete table_log;
    delete frame_box[0];
    delete frame_box[1];
    for (G4int i = 0; i < 6; i++) {
      delete frame_log[i];
    }
  }
}
