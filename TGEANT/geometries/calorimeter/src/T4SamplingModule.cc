#include "T4SamplingModule.hh"

T4SamplingModule::T4SamplingModule(void)
{
  hullVisualization = true;
  isHCAL1 = false;

  nLayer = 0;
  thicknessAbsorber = 0;
  thicknessScintillator = 0;
  materialScintillator = NULL;
  materialAbsorber = NULL;

  dimensionHull[0] = 0;
  dimensionHull[1] = 0;
  dimensionHull[2] = 0;
  thicknessHull = 0;

  scintillatorDimensions[0] = 0;
  scintillatorDimensions[1] = 0;
  scintillatorDimensions[2] = 0;
  airGap = 0;
  thicknessFlatLG = 0;

  outerHull_box = NULL;
  outerHull_log = NULL;
  innerHull_box = NULL;
  innerHull_log = NULL;
  scintillator_box = NULL;
  absorber_box = NULL;
  flatLG_box = NULL;
  flatLG_log = NULL;
}

T4SamplingModule::~T4SamplingModule(void)
{
  if (outerHull_box != NULL)
    delete outerHull_box;
  if (outerHull_log != NULL)
    delete outerHull_log;
  if (innerHull_box != NULL)
    delete innerHull_box;
  if (innerHull_log != NULL)
    delete innerHull_log;
  if (scintillator_box != NULL)
    delete scintillator_box;
  if (absorber_box != NULL)
    delete absorber_box;
  if (flatLG_box != NULL)
    delete flatLG_box;
  if (flatLG_log != NULL)
    delete flatLG_log;
}

T4ECAL0Module::T4ECAL0Module(void)
{
  setDetectorName("ECAL0");
  nLayer = 109;
  thicknessScintillator = 1.5 * CLHEP::mm;
  thicknessAbsorber = 0.8 * CLHEP::mm;

  materialScintillator = materials->polystyrene;
  materialAbsorber = materials->lead;

  dimensionHull[0] = 40.0 / 2 * CLHEP::mm;
  dimensionHull[1] = 40.0 / 2 * CLHEP::mm;
  dimensionHull[2] = 34.17 / 2 * CLHEP::cm;
  thicknessHull = 0.2 * CLHEP::mm;

  scintillatorDimensions[0] = dimensionHull[0] - thicknessHull;
  scintillatorDimensions[1] = dimensionHull[1] - thicknessHull;
  scintillatorDimensions[2] = (thicknessScintillator + thicknessAbsorber) / 2
      * nLayer;

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialScintillator,
      thicknessScintillator, materialAbsorber, thicknessAbsorber);
  regionModule = regionManager->getEcal0Region(regionInfo);

  energyScale = 0.92;
}

T4HCAL1Module::T4HCAL1Module(void)
{
  setDetectorName("HCAL1");
  isHCAL1 = true;
  nLayer = 40;
  thicknessScintillator = 5.0 * CLHEP::mm;
  thicknessAbsorber = 20.0 * CLHEP::mm;

  materialScintillator = materials->polystyrene;
  materialAbsorber = materials->iron;

  dimensionHull[0] = 150.0 / 2 * CLHEP::mm;
  dimensionHull[1] = 150.0 / 2 * CLHEP::mm;
  dimensionHull[2] = 119.0 / 2 * CLHEP::cm;
  thicknessHull = 2.0 * CLHEP::mm;

  scintillatorDimensions[0] = 146.0 / 2 * CLHEP::mm;
  scintillatorDimensions[1] = 142.0 / 2 * CLHEP::mm;
  scintillatorDimensions[2] = (thicknessScintillator + thicknessAbsorber) / 2
      * nLayer;

  airGap = 0.7 * CLHEP::mm;
  thicknessFlatLG = dimensionHull[1] - scintillatorDimensions[1] - thicknessHull
      - airGap / 2;

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialScintillator,
      thicknessScintillator, materialAbsorber, thicknessAbsorber);
  regionModule = regionManager->getHcal1Region(regionInfo);
}

T4HCAL2Module::T4HCAL2Module(void)
{
  setDetectorName("HCAL2");
  nLayer = 36;
  thicknessScintillator = 5.0 * CLHEP::mm;
  thicknessAbsorber = 25.0 * CLHEP::mm;

  materialScintillator = materials->polystyrene;
  materialAbsorber = materials->iron;

  dimensionHull[0] = 200.0 / 2 * CLHEP::mm;
  dimensionHull[1] = 200.0 / 2 * CLHEP::mm;
  dimensionHull[2] = 165.0 / 2 * CLHEP::cm;
  //taken from detectors.dat
  thicknessHull = 0.5 / 2. * CLHEP::mm;

  scintillatorDimensions[0] = dimensionHull[0] - thicknessHull;
  scintillatorDimensions[1] = dimensionHull[1] - thicknessHull;
  scintillatorDimensions[2] = (thicknessScintillator + thicknessAbsorber) / 2
      * nLayer;

  static T4RegionInformation* regionInfo = new T4RegionInformation(materialScintillator,
      thicknessScintillator, materialAbsorber, thicknessAbsorber);
  regionModule = regionManager->getHcal2Region(regionInfo);
}

void T4SamplingModule::construct(G4LogicalVolume* world_log)
{
  G4VisAttributes* hull = colour->darkgray;
  if (detName == "ECAL0")
    hull = colour->deepskyblue;
  if (!hullVisualization)
    hull = colour->invisible;
  
///////////////////////////////////////////////////
// RED COLORED NEW MODULES FOR 2016 ECAL0
//   vector<int> ch;
//   for (int j=0;j<15;j++) {
//     ch.push_back(12011+j*15);for (int i=1;i<6;i++) ch.push_back(ch.back()+1);
//     ch.push_back(12228+j*15);for (int i=1;i<6;i++) ch.push_back(ch.back()+1);
//   }
//   for (int j=0;j<3;j++) {
//     ch.push_back(13120+j*51);for (int i=1;i<27;i++) ch.push_back(ch.back()+1);
//     ch.push_back(12466+j*51);for (int i=1;i<27;i++) ch.push_back(ch.back()+1);
//     
//     ch.push_back(13271+j*45);for (int i=1;i<27;i++) ch.push_back(ch.back()+1);
//     ch.push_back(12617+j*45);for (int i=1;i<27;i++) ch.push_back(ch.back()+1);    
//   }
//   hull = colour->red;
//   for (int i=0;i<ch.size();i++)
//     if (channelID == ch.at(i))
//       hull = colour->deepskyblue;
///////////////////////////////////////////////////
    
  outerHull_box = new G4Box("outerHull_box", dimensionHull[0], dimensionHull[1],
      dimensionHull[2]);
  outerHull_log = new G4LogicalVolume(outerHull_box, materials->iron,
      "calorimeter_outerHull_log", 0, 0, 0, 0);
  outerHull_log->SetVisAttributes(hull);

  new G4PVPlacement(0, positionVector, outerHull_log,
      detName + "_" + intToStr(channelID), world_log, 0, 0, checkOverlap);

  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;

  innerHull_box = new G4Box("innerHull_box", dimensionHull[0] - thicknessHull,
      dimensionHull[1] - thicknessHull, dimensionHull[2] - thicknessHull);
  innerHull_log = new G4LogicalVolume(innerHull_box, materials->air_noOptical,
      "innerHull_log", 0, 0, 0, 0);
  innerHull_log->SetVisAttributes(colour->invisible);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerHull_log, "sampling_air",
      outerHull_log, 0, 0, /*checkOverlap*/false);

  scintillator_box = new G4Box("scintillator_box", scintillatorDimensions[0],
      scintillatorDimensions[1], scintillatorDimensions[2]);
  scintillator_log = new G4LogicalVolume(scintillator_box, materialScintillator,
      "calorimeter_log", 0, 0, 0, 0);

  if (hullVisualization)
    scintillator_log->SetVisAttributes(colour->invisible);
  else
    scintillator_log->SetVisAttributes(colour->yellow);

  G4double yPos = 0;
  if (isHCAL1)
    yPos = -dimensionHull[1] + thicknessHull + scintillatorDimensions[1];
  new G4PVPlacement(0,
      G4ThreeVector(0, yPos,
          -dimensionHull[2] + thicknessHull + scintillatorDimensions[2]),
      scintillator_log, "sampling_sd", innerHull_log, 0, 0, /*checkOverlap*/
      false);

  absorber_box = new G4Box("absorber_box", scintillatorDimensions[0],
      scintillatorDimensions[1], thicknessAbsorber / 2);

  for (G4int i = 0; i < nLayer; i++) {
    absorber_log.push_back(
        new G4LogicalVolume(absorber_box, materialAbsorber,
            "calorimeter_absorber_log", 0, 0, 0, 0));

    if (hullVisualization)
      absorber_log.back()->SetVisAttributes(colour->invisible);
    else
      absorber_log.back()->SetVisAttributes(colour->black);

    new G4PVPlacement(0,
        G4ThreeVector(0, 0,
            -scintillatorDimensions[2] + thicknessAbsorber / 2
                + i * (thicknessScintillator + thicknessAbsorber)),
        absorber_log.back(), "sampling_absorber", scintillator_log, 0, 0, /*checkOverlap*/
        false);
  }

  if (isHCAL1) {
    flatLG_box = new G4Box("flatLG_box", scintillatorDimensions[0],
        thicknessFlatLG, scintillatorDimensions[2]);
    flatLG_log = new G4LogicalVolume(flatLG_box,
        materials->plexiglass_noOptical, "calorimeter_flatLG_log", 0, 0, 0, 0);
    if (hullVisualization) {
      flatLG_log->SetVisAttributes(colour->invisible);
    } else {
      flatLG_log->SetVisAttributes(colour->white);
    }
    new G4PVPlacement(0,
        G4ThreeVector(0,
            -dimensionHull[1] + thicknessHull + 2. * scintillatorDimensions[1]
                + airGap + thicknessFlatLG,
            -dimensionHull[2] + thicknessHull + scintillatorDimensions[2]),
        flatLG_log, "sampling_hcal1_lg", innerHull_log, 0, 0, /*checkOverlap*/
        false);
  }
}

void T4SamplingModule::setLayerNumber(G4int input)
{
  nLayer = input;
  scintillatorDimensions[2] = (thicknessScintillator + thicknessAbsorber) / 2
      * nLayer;
}
