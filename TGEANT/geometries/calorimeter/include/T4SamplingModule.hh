#ifndef T4SAMPLINGMODULE_HH_
#define T4SAMPLINGMODULE_HH_

#include "T4BaseDetector.hh"
#include "T4ModuleBackend.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"

// position of module is defined as center of sensitive volume
// this is not necessarily the center of the hull
class T4SamplingModule : public T4ModuleBackend
{
  public:
    void construct(G4LogicalVolume*);

    void setLayerNumber(G4int);

    G4double getDimensionX(void)
      {return 2. * dimensionHull[0];}
    G4double getDimensionY(void)
      {return 2. * dimensionHull[1];}
    G4double getDimensionZ(void)
      {return 2. * dimensionHull[2];}

    void noHullVisualization(void)
      {hullVisualization = false;}
        
  protected:
    T4SamplingModule(void);
    virtual ~T4SamplingModule(void);

    G4bool hullVisualization;
    G4bool isHCAL1;

    G4int nLayer;
    G4double thicknessAbsorber;
    G4double thicknessScintillator;
    G4Material* materialScintillator;
    G4Material* materialAbsorber;

    G4double dimensionHull[3];
    G4double thicknessHull;

    G4double scintillatorDimensions[3];
    G4double airGap;
    G4double thicknessFlatLG;

    G4Box* outerHull_box;
    G4LogicalVolume* outerHull_log;
    G4Box* innerHull_box;
    G4LogicalVolume* innerHull_log;
    G4Box* scintillator_box;
    G4Box* absorber_box;
    G4Box* flatLG_box;
    G4LogicalVolume* flatLG_log;
};

class T4ECAL0Module : public T4SamplingModule
{
  public:
    T4ECAL0Module(void);
    virtual ~T4ECAL0Module(void) {}
};

class T4HCAL1Module : public T4SamplingModule
{
  public:
    T4HCAL1Module(void);
    virtual ~T4HCAL1Module(void) {}
};

class T4HCAL2Module : public T4SamplingModule
{
  public:
    T4HCAL2Module(void);
    virtual ~T4HCAL2Module(void) {}
};

#endif /* T4SAMPLINGMODULE_HH_ */
