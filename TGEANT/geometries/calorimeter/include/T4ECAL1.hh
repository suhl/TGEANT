#ifndef T4ECAL111
#define T4ECAL111

#include <string>
#include <vector>

#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Trap.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "T4Globals.hh"
#include "T4SDetectorsDat.hh"
#include "T4CalorimeterBackend.hh"

class T4ECAL1 : public T4CalorimeterBackend
{
  public:
    T4ECAL1(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    virtual ~T4ECAL1(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure(void);

  private:
    G4double structureSize[2];
    G4double platformDimensions[3];
    G4Box* platform_box[2];
    G4UnionSolid* platform_geo;
    G4LogicalVolume* platform_log;

    G4double wheelDimensions[2];
    G4Tubs* wheel_tubs[3];
    G4UnionSolid* wheel_union;
    G4UnionSolid* wheel_geo;
    G4LogicalVolume* wheel_log[4];

    G4double frameSize[2];
    G4Box* frame_box[2];
    G4LogicalVolume* frame_log[6];
};

#endif
