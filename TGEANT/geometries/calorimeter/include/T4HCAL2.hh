#ifndef T4HCAL22
#define T4HCAL22

#include <string>
#include <vector>

#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Trap.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "T4Globals.hh"
#include "T4SDetectorsDat.hh"
#include "T4CalorimeterBackend.hh"

class T4HCAL2 : public T4CalorimeterBackend
{
  public:
    T4HCAL2(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    virtual ~T4HCAL2(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure();

  private:
    G4double activeArea[2];
    G4Box* holeStructure_box[3];
    G4SubtractionSolid* holeStructure_sub[2];
    G4LogicalVolume* holeStructure_log;

    G4Box* frame_box[2];
    G4LogicalVolume* frame_log[2];

    G4Box* table_box;
    G4LogicalVolume* table_log;

    G4Trd* upperBase_trd;
    G4LogicalVolume* upperBase_log;

    G4Tubs* wheel_tubs[3];
    G4UnionSolid* tubs_union;
    G4UnionSolid* wheel_union;
    G4LogicalVolume* wheel_log;

    G4Box* rail_box;
    G4LogicalVolume* rail_log;

    G4Box* plate_box;
    G4LogicalVolume* plate_log;

    G4Trd* base_trd[2];
    G4SubtractionSolid* base_sub;
    G4LogicalVolume* base_log;

    G4Tubs* bigWheel_tubs[3];
    G4UnionSolid* bigTubs_union;
    G4UnionSolid* bigWheel_union;
    G4LogicalVolume* bigWheel_log;

    G4Box* bigRail_box;
    G4LogicalVolume* bigRail_log;

    G4Box* foot_box;
    G4LogicalVolume* foot_log;

    G4Trap* wheelHolder_trap;
    G4LogicalVolume* wheelHolder_log;

    G4Box* wheelFoot_box;
    G4LogicalVolume* wheelFoot_log;
};

#endif
