#ifndef T4ECAL00
#define T4ECAL00

#include <string>
#include <vector>

#include "G4Box.hh"
#include "G4UnionSolid.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"

#include "T4Globals.hh"
#include "T4SDetectorsDat.hh"
#include "T4CalorimeterBackend.hh"

class T4ECAL0 : public T4CalorimeterBackend
{
  public:
    T4ECAL0(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    virtual ~T4ECAL0(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure(void);

  private:
    double yearSetup;

    // 2016
    void constructDummyModules2016(void);
    void constructPlates2016(void);
    G4Box* dummy_box[2];
    G4SubtractionSolid* dummy_sub;
    G4LogicalVolume* dummy_log;

    // 2012
    void constructDummyModules2012(void);
    void constructSidebars(void);
    void constructBottomBars(void);
    void constructTopBars(void);
    void constructStamps(void);
    void constructInoxPlates(void);
    void constructAdditionalPlates(void);
    void constructDiagonalBars(void);

    G4double zOffset; // global offset between middle of ecal0-module-sd and middle of hull
    G4double offsetECALModule;
    G4double offsetCorner;

    // side bars
    G4Box* sidebarsBigPlate_box;
    G4Box* sidebarsSmallPlate_box;
    G4UnionSolid* sidePanels_union;
    G4UnionSolid* sidebarsRight_union;
    G4UnionSolid* sidebarsLeft_union;
    G4LogicalVolume* sidebarsLeft_log;
    G4LogicalVolume* sidebarsRight_log;

    // bottom bars
    G4Box* bottomBarsLongSide_box;
    G4Box* bottomBarsShortSide_box;
    G4UnionSolid* bottomBarSideBars_union;
    G4UnionSolid* bottomBarFront_union;
    G4UnionSolid* bottomBarBack_union;
    G4LogicalVolume* bottomBarFront_log;
    G4LogicalVolume* bottomBarBack_log;

    // top bars
    G4Box* topBarsLongSide_box;
    G4Box* topBarsShortSide_box;
    G4UnionSolid* topBarSideBars_union;
    G4UnionSolid* topBarFront_union;
    G4UnionSolid* topBarBack_union;
    G4LogicalVolume* topBarFront_log;
    G4LogicalVolume* topBarBack_log;

    // diagonal bars
    G4Box* diagonalBar_box;
    G4LogicalVolume* diagonalBar_log;
    G4RotationMatrix* rotation45Plus;
    G4RotationMatrix* rotation45Minus;

    // stamps
    G4Tubs* stampBig_tubs;
    G4Tubs* stampSmall_tubs;
    G4UnionSolid* stamp_union;
    G4LogicalVolume* stamp_log;
    G4RotationMatrix* rotation90;
    G4Box* ringPlate_box;
    G4Tubs* ring_tubs;
    G4UnionSolid* ring_union;
    G4LogicalVolume* ring_log;

    // inox plates
    G4Box* inoxPlate1_box;
    G4Box* inoxPlate2_box;
    G4LogicalVolume* inoxPlate1_log;
    G4LogicalVolume* inoxPlate2_log;

    // additional plates
    G4Box* additionalPlate_box[4];
    G4LogicalVolume* additionalPlate_log[4];
};

#endif
