#ifndef T4HCAL11
#define T4HCAL11

#include <string>
#include <vector>

#include "T4Globals.hh"
#include "T4SDetectorsDat.hh"
#include "T4CalorimeterBackend.hh"

#include "G4UnionSolid.hh"

class T4HCAL1 : public T4CalorimeterBackend
{
  public:
    T4HCAL1(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    virtual ~T4HCAL1(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure(void);

  private:
    G4double mechanicalXShift;
    void createRails(void);
    G4Box* rails_box;
    G4LogicalVolume* rails_log;

    void createWheels(void);
    G4Tubs* wheel_tubs;
    G4LogicalVolume* wheel_log;

    void createBottomPlate(void);
    G4Box* bottomPlate_box;
    G4Box* barBig_box;
    G4Box* barSmall_box;
    G4Box* motorBlock_box;
    G4LogicalVolume* bottomPlate_log;
    G4LogicalVolume* bottomBarsBig_log;
    G4LogicalVolume* bottomBarsSmall_log;
    G4LogicalVolume* motorBlock_log;

    void createFrame(void);
    G4Box* bar_box;
    G4Box* barUp_box;
    G4Box* topPlate_box;
    G4LogicalVolume* bar_log;
    G4LogicalVolume* barUp_log;
    G4LogicalVolume* topPlate_log;

    void createLadder(void);
    G4Box* ladderLongBar_box;
    G4Box* ladderStep_box;
    G4RotationMatrix* stepHelperRotation[2];
    G4Tubs* ladderStepHelper_tubs;
    G4Box* handrail_box;
    G4Box* handrailEnd_box;
    G4Box* handrailUp_box;
    G4RotationMatrix* rotationLadderRings;
    G4Tubs* ladderRing_tubs;
    G4RotationMatrix* rotationCage[4];
    G4Box* cageBars_box;
    G4LogicalVolume* ladderLongBar_log;
    G4LogicalVolume* ladderStep_log;
    G4LogicalVolume* ladderStepHelper_log;
    G4LogicalVolume* handrail_log;
    G4LogicalVolume* handrailEnd_log;
    G4LogicalVolume* handrailUp_log;
    G4LogicalVolume* ladderRing_log;
    G4LogicalVolume* cageBars_log;

    void createCentralStructures(void);
    G4Box* frontPlate_box;
    G4Box* verticalPlate_box;
    G4Box* shortPlateJura_box;
    G4Box* shortPlateSaleve_box;
    G4Box* longPlate_box;
    G4LogicalVolume* frontPlate_log;
    G4LogicalVolume* verticalPlate_log;
    G4LogicalVolume* shortPlateJura_log;
    G4LogicalVolume* shortPlateSaleve_log;
    G4LogicalVolume* longPlate_log;

    void createSideClamps(void);
    G4Tubs* clampScrew_tubs;
    G4Tubs* clampPlate_tubs;
    G4UnionSolid* clamp_union;
    G4LogicalVolume* clamp_log;
    G4RotationMatrix* rotationClamb;

    void createCornerStructures(void);
    G4Box* bigLongFull_box;
    G4Box* bigLongHole_box;
    G4SubtractionSolid* bigLong_box;
    G4LogicalVolume* bigLong_log;

    G4Box* bigShortFull_box;
    G4Box* bigShortHole_box;
    G4SubtractionSolid* bigShort_box;
    G4LogicalVolume* bigShort_log;

    G4Box* smallLongFull_box;
    G4Box* smallLongHole_box;
    G4SubtractionSolid* smallLong_box;
    G4LogicalVolume* smallLong_log;

    G4Box* smallShortFull_box;
    G4Box* smallShortHole_box;
    G4SubtractionSolid* smallShort_box;
    G4LogicalVolume* smallShort_log;
};

#endif
