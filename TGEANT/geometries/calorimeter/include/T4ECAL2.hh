#ifndef T4ECAL22
#define T4ECAL22

#include <string>
#include <vector>

#include "T4Globals.hh"
#include "T4SDetectorsDat.hh"
#include "T4CalorimeterBackend.hh"

class T4ECAL2 : public T4CalorimeterBackend
{
  public:
    T4ECAL2(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    virtual ~T4ECAL2(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure(void);

  private:
    G4double structureSize[2];
    G4double tableDimensions[3];
    G4double wheelDimensions[2];

    G4Box* tableSub_box;
    G4LogicalVolume* tableSub_log;
    G4Box* table_box;
    G4LogicalVolume* table_log;

    G4double frameSize[2];
    G4Box* frame_box[2];
    G4LogicalVolume* frame_log[6];
};

#endif
