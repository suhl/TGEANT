#ifndef T4LEADGLASSMODULE_HH_
#define T4LEADGLASSMODULE_HH_

#include "T4BaseDetector.hh"
#include "T4ModuleBackend.hh"
#include "T4PhotoMultiplier.hh"

#include "G4Box.hh"

// position of module is defined as center of sensitive volume
// this is not necessarily the center of the hull
class T4LeadGlassModule : public T4ModuleBackend
{
  public:
    void construct(G4LogicalVolume*);
    void constructReadout(G4LogicalVolume*, G4ThreeVector);

    G4double getDimensionX(void)
      {return 2. * dimensionHull[0];}
    G4double getDimensionY(void)
      {return 2. * dimensionHull[1];}
    G4double getDimensionZ(void)
      {return 2. * dimensionHull[2];}

  protected:
    T4LeadGlassModule(void);
    virtual ~T4LeadGlassModule(void);

    G4Material* materialGlass;
    G4Material* materialHull;
    G4VisAttributes* colourHull;
    G4VisAttributes* colourGlass;
    G4double dimensionHull[3];
    G4double thicknessHull;

    G4Box* outerHull_box;
    G4LogicalVolume* outerHull_log;
    G4Box* leadGlass_box;

    T4PhotoMultiplier* pmt;
};

class T4Gams : public T4LeadGlassModule
{
  public:
    T4Gams(string caloName);
    virtual ~T4Gams(void) {}
};

class T4GamsRH : public T4LeadGlassModule
{
  public:
    T4GamsRH(void);
    virtual ~T4GamsRH(void) {}
};

class T4Olga : public T4LeadGlassModule
{
  public:
    T4Olga(void);
    virtual ~T4Olga(void) {}
};

class T4Mainz : public T4LeadGlassModule
{
  public:
    T4Mainz(void);
    virtual ~T4Mainz(void) {}
};

#endif /* T4LEADGLASSMODULE_HH_ */
