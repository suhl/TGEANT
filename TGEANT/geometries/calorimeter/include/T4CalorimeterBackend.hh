#ifndef GENCALO_HH_
#define GENCALO_HH_

#include <string>
#include <vector>

#include "T4Globals.hh"
#include "T4SCalorimeter.hh"
#include "T4BaseDetector.hh"
#include "T4SamplingModule.hh"
#include "T4ShashlikModule.hh"
#include "T4LeadGlassModule.hh"

#include "G4UnionSolid.hh"

using namespace std;

class T4CalorimeterBackend : public T4BaseDetector
{
  public:
    T4CalorimeterBackend(vector<T4SCAL>& _in, const T4SDetector* _inDet);
    T4CalorimeterBackend(void);
    virtual ~T4CalorimeterBackend(void);

    /*! \brief constructs the calorimeter */
    virtual void construct(G4LogicalVolume*);

  protected:
    /*! \brief constructs modules from given parameters from the cmtx lines - virtual so can be reimplemented if need be*/
    virtual void constructRegions(T4SCAL*) = 0;
    void constructModules(void);
    virtual void constructMechanicalStructure(void) = 0;
    
    string caloName;

    /*! \brief the world-logical-volume pointer */
    G4LogicalVolume* world_log;

    /*! \brief vector of cmtx-lines encoded in T4SCAL structs */
    vector<T4SCAL> cmtxList;

    /*! \brief list of modules */
    vector<T4ModuleBackend*> modules;

    vector<G4Box*>& getBox(TGEANT::T4CaloModule _type) {return cmtx_box[_type];}
    vector<G4UnionSolid*>& getUnion(TGEANT::T4CaloModule _type) {return cmtx_union[_type];}

    G4ThreeVector& getFirstBoxPosition(TGEANT::T4CaloModule _type) {return firstBoxPosition[_type];}

    void setLocalModule(TGEANT::T4CaloModule _type, G4ThreeVector _pos,
        G4int _channel) {
      localModulePosition[_type].push_back(positionVector + _pos - firstBoxPosition[_type]);
      drsModulePosition[_type].push_back(_pos);
      channelNo[_type].push_back(_channel);
    }

    void buildRegionBox(TGEANT::T4CaloModule type, T4SCAL* _scalIn, string name,
        G4double zSize, G4double moduleDistanceX, G4double moduleDistanceY,
        G4double posZ);
    void modulePositioning(TGEANT::T4CaloModule type, T4SCAL* _scalIn,
        G4double moduleDistanceX, G4double moduleDistanceY, G4double posZ);

  private:
    static const int nModules = 9;
    vector<G4Box*> cmtx_box[nModules];
    vector<G4UnionSolid*> cmtx_union[nModules];

    G4ThreeVector firstBoxPosition[nModules];
    vector<G4ThreeVector> localModulePosition[nModules];
    vector<G4ThreeVector> drsModulePosition[nModules];
    vector<G4int> channelNo[nModules];

    G4LogicalVolume* cmtx_log[nModules];

    void getModule(TGEANT::T4CaloModule _type, T4ModuleBackend*&);
    G4Region* getRegion(TGEANT::T4CaloModule _type);
};

#endif
