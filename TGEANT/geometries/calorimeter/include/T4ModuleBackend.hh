#ifndef T4MODBACK_HH_
#define T4MODBACK_HH_

#include "T4BaseDetector.hh"
#include "T4SensitiveCalorimeter.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

/*! \brief Baseclass for the calorimeter modules */
class T4ModuleBackend : public T4BaseDetector
{
  public:
    T4ModuleBackend(void) {channelID = 0; scintillator_log = NULL; regionModule = NULL; energyScale = 1.;}
    virtual ~T4ModuleBackend(void) {
      if (scintillator_log != NULL)
        {delete scintillator_log; scintillator_log = NULL;}
      for (unsigned int i = 0; i < absorber_log.size(); i++)
        delete absorber_log.at(i);
    }

    virtual void construct(G4LogicalVolume*) = 0;
    virtual void constructReadout(G4LogicalVolume*, G4ThreeVector) {}

    virtual G4double getDimensionX() = 0;
    virtual G4double getDimensionY() = 0;
    virtual G4double getDimensionZ() = 0;

    virtual void setLayerNumber(G4int _layers) {}

    /* Set the channelId. The given number is read from detectors.dat and needs to be increased by 1 to avoid CORAL problems. */
    inline void setChannelId(G4int _channelID) {channelID = _channelID + 1;}
    inline void setDetectorName(string _detName) {detName = _detName;}

    void addSensitiveDetector(G4ThreeVector drsPos) {
      if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
        return;
      T4SensitiveCalorimeter* sd = new T4SensitiveCalorimeter(detName, channelID, drsPos, energyScale);
      scintillator_log->SetSensitiveDetector(sd);
      for (unsigned int i = 0; i < absorber_log.size(); i++)
        absorber_log.at(i)->SetSensitiveDetector(sd);
    }

  protected:
    G4LogicalVolume* scintillator_log;
    std::vector<G4LogicalVolume*> absorber_log;
    string detName;
    G4int channelID;

    G4Region* regionModule;
    double energyScale;
};

#endif
