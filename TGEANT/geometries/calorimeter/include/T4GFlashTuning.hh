#ifndef T4GFLASHTUNING
#define T4GFLASHTUNING

#include "GVFlashHomoShowerTuning.hh"
#include "T4SettingsFile.hh"

#include "globals.hh"
#include "GVFlashShowerParameterisation.hh"
#include "T4Materials.hh"

class T4GFlashTuningGams : public GVFlashHomoShowerTuning
{
  public:
    T4GFlashTuningGams() {}
    virtual ~T4GFlashTuningGams() {}

    G4double ParRC1(){ return 0.0251 * 2.92254;   }
    G4double ParRC2(){ return 0.00319 * 2.92254;  }

    G4double ParWC1(){ return 2.632 * 1.99612;   }
    G4double ParWC5(){ return 1.313 * 0.401956;   }
    G4double ParWC6(){ return -0.0686 * 0.401956; }

    G4double ParRT1(){ return 0.659 * 1.30558;   }
    G4double ParRT5(){ return 0.3585 * 1.56211;  }
    G4double ParRT6(){ return 0.0412 * 1.56211;  }
};

class T4GFlashTuningMainz : public GVFlashHomoShowerTuning
{
  public:
    T4GFlashTuningMainz() {}
    virtual ~T4GFlashTuningMainz() {}

    G4double ParRC1(){ return 0.0251 * 2.39534;   }
    G4double ParRC2(){ return 0.00319 * 2.39534;  }

    G4double ParWC1(){ return 2.632 * 0.788724;   }
    G4double ParWC5(){ return 1.313 * 1.575;   }
    G4double ParWC6(){ return -0.0686 * 1.575; }

    G4double ParRT1(){ return 0.659 * 1.2321;   }
    G4double ParRT5(){ return 0.3585 * 1.35387;  }
    G4double ParRT6(){ return 0.0412 * 1.35387;  }
};

class T4GFlashTuningOlga : public GVFlashHomoShowerTuning
{
  public:
    T4GFlashTuningOlga() {}
    virtual ~T4GFlashTuningOlga() {}

    G4double ParRC1(){ return 0.0251 * 4.61113;   }
    G4double ParRC2(){ return 0.00319 * 4.61113;  }

    G4double ParWC1(){ return 2.632 * 0.971324;   }
    G4double ParWC5(){ return 1.313 * 2.24568;   }
    G4double ParWC6(){ return -0.0686 * 2.24568; }

    G4double ParRT1(){ return 0.659 * 1.84528;   }
    G4double ParRT5(){ return 0.3585 * 2.63962;  }
    G4double ParRT6(){ return 0.0412 * 2.63962;  }
};

class T4GFlashTuningShashlik : public GVFlashHomoShowerTuning
{
  public:
    T4GFlashTuningShashlik() {}
    virtual ~T4GFlashTuningShashlik() {}

    virtual G4double ParRC1(){ return 0.0251 * 2.11428;   }
    virtual G4double ParRC2(){ return 0.00319 * 2.11428;  }

    virtual G4double ParWC1(){ return 2.632 * 1.00855;   }
    virtual G4double ParWC5(){ return 1.313 * 1.32474;   }
    virtual G4double ParWC6(){ return -0.0686 * 1.32474; }

    virtual G4double ParRT1(){ return 0.659 * 1.56157;   }
    virtual G4double ParRT5(){ return 0.3585 * 0.355937;  }
    virtual G4double ParRT6(){ return 0.0412 * 0.355937;  }

    // ---------------------------TGEANT---------------------------
    // COPY FROM GVFlashHomoShowerTuning.hh
    // The problem is, that the functions in the original GFlashSamplingShowerTuning class are NOT virtual, so we have to copy them. :/
    // ---------------------------TGEANT---------------------------
    G4double ParsAveT1(){ return -0.55;} // t1
    G4double ParsAveT2(){ return -0.69;} // t2
      // T_sam =  log(exp( log T_hom) + t1*Fs-1 + t2*(1-ehat))

    G4double ParsAveA1(){ return -0.476;  } // a1
      // alpha_sam = log(exp(log alphah_hom) +(a1*Fs-1))

    G4double ParsSigLogT1(){ return -2.5;} // t1
    G4double ParsSigLogT2(){ return 1.25;} // t2
      // std::sqrt(var(ln(T_sam))) = 1/(t+t2*ln(y))

    G4double ParsSigLogA1(){ return -0.82;} // a1
    G4double ParsSigLogA2(){ return 0.79; } // a2
      // std::sqrt(var(ln(alpha_sam))) = 1/(a1+a2*ln(y))

    G4double ParsRho1(){ return 0.784; } // r1
    G4double ParsRho2(){ return -0.023;} // r2
      // Correlation(ln(T),ln(alpha))=r1+r2*ln(y)

    // Radial profiles
    // f(r) := (1/dE(t))(dE(t,r)/dr)
    // Ansatz:
    // f(r) = p(2*r*Rc**2)/(r**2+Rc**2)**2+(1-p)*(2*r*Rt**2)/(r**2+Rt**2)**2,
    //        0<p<1

    G4double ParsRC1(){ return -0.0203;   } // c1
    G4double ParsRC2(){ return 0.0397;  }   // c2
      // Rc_sam = Rc_hom + c1 * (1-ehat) + c2 *Fs-1*exp (-tau)

    G4double ParsRT1(){ return -0.14;  }   // t1
    G4double ParsRT2(){ return -0.495; }   // t2
      // Rt_sam = Rc_hom + t1 * (1-ehat) + t2 *Fs-1*exp (-tau)

    G4double ParsWC1(){ return 0.348;   } // c1
    G4double ParsWC2(){ return -0.642;} // c2
      // W_sam = W_hom + (1-ehat)*(c1 + c2 *Fs-1 * exp (- (tau -1 )**2))

    // Fluctuations on radial profiles through number of spots
    // The total number of spots needed for a shower is

    G4double ParsSpotN1(){ return 10.3; } // n1
    G4double ParsSpotN2(){ return 0.959;} // n2
      // Ns = n1*ln(Z)(E/GeV)**n2

    // The number of spots per longitudinal interval is:
    // (1/Ns)(dNs(t)/dt) = f(t)
    //  = (beta*t)**(alpha-1)*beta*std::exp(-beta*t)/Gamma(alpha)
    // <t> = alpha_s/beta_s
    // Ts = (alpha_s-1)/beta_s
    // and
    // Ts = T*(t1+t2*Z)
    // alpha_s = alpha*(a1+a2*Z)

    G4double ParsSpotT1(){ return 0.813; } // t1
    G4double ParsSpotT2(){ return 0.0019;} // t2

    G4double ParsSpotA1(){ return 0.844; } //a1
    G4double ParsSpotA2(){ return 0.0026;} //a2

    // Resolution

    G4double ConstantResolution(){ return 0.00;  }
    G4double NoiseResolution()   { return 0.00;  } // not used
    G4double SamplingResolution(){ return 0.11;  } // not used
};

class T4GFlashTuningECAL0 : public T4GFlashTuningShashlik
{
  public:
    T4GFlashTuningECAL0() {}
    virtual ~T4GFlashTuningECAL0() {}

    G4double ParRC1(){ return 0.0251 * 2.10973;   }
    G4double ParRC2(){ return 0.00319 * 2.10973;  }

    G4double ParWC1(){ return 2.632 * 1.03159;   }
    G4double ParWC5(){ return 1.313 * 1.30813;   }
    G4double ParWC6(){ return -0.0686 * 1.30813; }

    G4double ParRT1(){ return 0.659 * 1.56049;   }
    G4double ParRT5(){ return 0.3585 * 0.355487;  }
    G4double ParRT6(){ return 0.0412 * 0.355487;  }
};

#endif
