#ifndef T4SHASHLIKMODULE_HH_
#define T4SHASHLIKMODULE_HH_

#include "T4SamplingModule.hh"

#include "G4SubtractionSolid.hh"

class T4ShashlikModule : public T4SamplingModule
{
  public:
    T4ShashlikModule(string caloName);
    virtual ~T4ShashlikModule(void);

    void construct(G4LogicalVolume*);

  private:
    G4double fiberDiameter;
    G4double rodDiameter;

    std::vector<G4SubtractionSolid*> absorber_sub;

    G4Tubs* fiber_tubs;
    G4LogicalVolume* fiber_log;
    G4Tubs* rod_tubs;
    G4LogicalVolume* rod_log;
};

#endif /* T4SHASHLIKMODULE_HH_ */
