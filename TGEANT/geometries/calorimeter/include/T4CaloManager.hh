#ifndef T4CALOMAN
#define T4CALOMAN

#include <vector>
#include "T4BaseDetector.hh"
#include "T4CalorimeterBackend.hh"
#include "T4ECAL0.hh"
#include "T4ECAL1.hh"
#include "T4ECAL2.hh"
#include "T4HCAL1.hh"
#include "T4HCAL2.hh"

#include "GFlashShowerModel.hh"
#include "GFlashHomoShowerParameterisation.hh"
#include "GFlashSamplingShowerParameterisation.hh"
#include "GFlashParticleBounds.hh"
#include "GFlashHitMaker.hh"

#include "T4GFlashTuning.hh"
#include "T4GFlashSamplingShowerParameterisation.hh"

using namespace std;

/*! \brief This class initializes all the chosen calorimeters and hands them their matrix-information from the CMTX-lines of the detectors dat. */

class T4CaloManager : public T4BaseDetector
{
  public:
    T4CaloManager(void);
    virtual ~T4CaloManager(void);

    /*! \brief does the actual initialization of the calorimeter-classes */
    void construct(G4LogicalVolume* world_log);

    /*! \brief returns the vector of activated calorimeters */
    const vector<T4CalorimeterBackend*>& getActiveCalos() const
        {return activeCalos;}

    void addGflash(void);

  private:
    const vector<T4SCAL>* cmtxList;
    vector<T4CalorimeterBackend*> activeCalos;

    vector<T4SCAL> ECAL0;
    vector<T4SCAL> ECAL1;
    vector<T4SCAL> ECAL2;
    vector<T4SCAL> HCAL1;
    vector<T4SCAL> HCAL2;

    vector<GFlashShowerModel*> showerModels;
    vector<GVFlashShowerParameterisation*> parameterisations;
    vector<GFlashParticleBounds*> particleBounds;
    vector<GFlashHitMaker*> hitMakers;
};

#endif
