#ifndef T4BASEDETECTOR_HH_
#define T4BASEDETECTOR_HH_

#include "T4SettingsFile.hh"
#include "T4Materials.hh"
#include "T4Colour.hh"
#include "T4RegionManager.hh"
#include "T4SensitiveDetector.hh"

#include "T4SGlobals.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"

class T4BaseDetector
{
  public:
    T4BaseDetector(void) {
      settingsFile = T4SettingsFile::getInstance();
      materials = T4Materials::getInstance();
      colour = T4Colour::getInstance();
      regionManager = T4RegionManager::getInstance();
      positionVector = G4ThreeVector(0, 0, 0);
      useMechanicalStructure = false;
      verboseLevel = settingsFile->getStructManager()->getGeneral()->verboseLevel;
      checkOverlap = settingsFile->getStructManager()->getGeneral()->checkOverlap;
      };
    virtual ~T4BaseDetector(void) {};

    virtual void construct(G4LogicalVolume*) = 0;
    
    inline void setPosition(G4ThreeVector _positionVector) {
      positionVector = _positionVector;
    };
    inline void setPosition(const G4double _positionArray[3]) {
      positionVector = G4ThreeVector(_positionArray[0], _positionArray[1],
          _positionArray[2]);
    };

    G4ThreeVector getPosition(void) {return positionVector;}

    virtual void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&) {};
    virtual const std::vector<std::string>* getCMTXDetDat(void) const {return NULL;};


  protected:
    T4SettingsFile* settingsFile;
    T4Materials* materials;
    T4Colour* colour;
    T4RegionManager* regionManager;

    G4ThreeVector positionVector;
    G4bool useMechanicalStructure;
    G4int verboseLevel;
    G4bool checkOverlap;
};


#endif /* T4BASEDETECTOR_HH_ */
