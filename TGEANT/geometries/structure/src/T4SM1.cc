#include "T4SM1.hh"

T4SM1::T4SM1(T4SDetector* sm1)
{
  // world position
  // this position is a relativ position in the world
  // point (0,0,0) is in the middle of the magnet, where the
  // beam should pass through. z axis along beam
  setPosition(sm1->position);

  for (unsigned int i = 0; i < 7; i++)
    rotationSolenoid[i] = NULL;
  solenoid_boxX = NULL;
  solenoid_boxZ = NULL;
  solenoid_tubBig = NULL;
  solenoid_tubSmall = NULL;
  for (unsigned int i = 0; i < 11; i++)
    solenoid_unionSolid[i] = NULL;
  solenoidHigh_log = NULL;
  solenoidLow_log = NULL;

  bigBoxes = NULL;
  for (unsigned int i = 0; i < 2; i++)
    bigBoxes_log[i] = NULL;

  top = NULL;
  cutTop = NULL;
  cutTub1 = NULL;
  cutTub2 = NULL;
  cutTrd = NULL;
  sideCut = NULL;
  magnetFeet = NULL;
  bigInnerBox = NULL;
  smallInnerBox = NULL;

  top_log = NULL;
  bottom_log = NULL;
  magnetFeet_log = NULL;
  bigInnerBox_log = NULL;
  smallInnerBox_log = NULL;

  rotation90X = NULL;
  rotation45Z = NULL;
  rotation180X = NULL;

  for (unsigned int i = 0; i < 6; i++)
    subtraction[i] = NULL;
}

T4SM1::~T4SM1(void)
{
  for (unsigned int i = 0; i < 7; i++)
    if (rotationSolenoid[i] != NULL)
      delete rotationSolenoid[i];
  if (solenoid_boxX != NULL)
    delete solenoid_boxX;
  if (solenoid_boxZ != NULL)
    delete solenoid_boxZ;
  if (solenoid_tubBig != NULL)
    delete solenoid_tubBig;
  if (solenoid_tubSmall != NULL)
    delete solenoid_tubSmall;
  for (unsigned int i = 0; i < 11; i++)
    if (solenoid_unionSolid[i] != NULL)
      delete solenoid_unionSolid[i];
  if (solenoidHigh_log != NULL)
    delete solenoidHigh_log;
  if (solenoidLow_log != NULL)
    delete solenoidLow_log;

  if (bigBoxes != NULL)
    delete bigBoxes;
  for (unsigned int i = 0; i < 2; i++)
    if (bigBoxes_log[i] != NULL)
      delete bigBoxes_log[i];

  if (top != NULL)
    delete top;
  if (cutTop != NULL)
    delete cutTop;
  if (cutTub1 != NULL)
    delete cutTub1;
  if (cutTub2 != NULL)
    delete cutTub2;
  if (cutTrd != NULL)
    delete cutTrd;
  if (sideCut != NULL)
    delete sideCut;
  if (magnetFeet != NULL)
    delete magnetFeet;
  if (bigInnerBox != NULL)
    delete bigInnerBox;
  if (smallInnerBox != NULL)
    delete smallInnerBox;

  if (top_log != NULL)
    delete top_log;
  if (bottom_log != NULL)
    delete bottom_log;
  if (magnetFeet_log != NULL)
    delete magnetFeet_log;
  if (bigInnerBox_log != NULL)
    delete bigInnerBox_log;
  if (smallInnerBox_log != NULL)
    delete smallInnerBox_log;

  if (rotation90X != NULL)
    delete rotation90X;
  if (rotation45Z != NULL)
    delete rotation45Z;
  if (rotation180X != NULL)
    delete rotation180X;

  for (unsigned int i = 0; i < 6; i++)
    if (subtraction[i] != NULL)
      delete subtraction[i];
}

void T4SM1::construct(G4LogicalVolume* world_log)
{
  G4double solenoidBoxX[3];
  solenoidBoxX[0] = 1500.0 * CLHEP::mm;
  solenoidBoxX[1] = 320.0 * CLHEP::mm;
  solenoidBoxX[2] = 240.0 * CLHEP::mm;
  G4double solenoidBoxZ[3];
  solenoidBoxZ[0] = 320.0 * CLHEP::mm;
  solenoidBoxZ[1] = 240.0 * CLHEP::mm;
  solenoidBoxZ[2] = 1120.0 * CLHEP::mm;
  G4double solenoidTubBig[3];
  solenoidTubBig[0] = 60.0 * CLHEP::mm;
  solenoidTubBig[1] = 380.0 * CLHEP::mm;
  solenoidTubBig[2] = 240.0 * CLHEP::mm;
  G4double solenoidTubSmall[3];
  solenoidTubSmall[0] = 60.0 * CLHEP::mm;
  solenoidTubSmall[1] = 300.0 * CLHEP::mm;
  solenoidTubSmall[2] = 320.0 * CLHEP::mm;

  solenoid_boxX = new G4Box("solenoid geometry X box", solenoidBoxX[0] / 2,
      solenoidBoxX[1] / 2, solenoidBoxX[2] / 2);
  solenoid_boxZ = new G4Box("solenoid geometry Z box", solenoidBoxZ[0] / 2,
      solenoidBoxZ[1] / 2, solenoidBoxZ[2] / 2);
  solenoid_tubBig = new G4Tubs("solenoid tubs big", solenoidTubBig[0],
      solenoidTubBig[1], solenoidTubBig[2] / 2, 0 * CLHEP::deg,
      90 * CLHEP::deg);
  solenoid_tubSmall = new G4Tubs("solenoid tubs small", solenoidTubSmall[0],
      solenoidTubSmall[1], solenoidTubSmall[2] / 2, 0 * CLHEP::deg,
      90 * CLHEP::deg);

  // add big tubs
  rotationSolenoid[0] = new CLHEP::HepRotation;
  rotationSolenoid[0]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[0]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[0]->rotateZ(90.0 * CLHEP::deg);
  solenoid_unionSolid[0] = new G4UnionSolid("solenoid union solid",
      solenoid_boxX, solenoid_tubBig, rotationSolenoid[0],
      G4ThreeVector(solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2, 0));

  rotationSolenoid[1] = new CLHEP::HepRotation;
  rotationSolenoid[1]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[1]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[1]->rotateZ(180.0 * CLHEP::deg);
  solenoid_unionSolid[1] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[0], solenoid_tubBig, rotationSolenoid[1],
      G4ThreeVector(-solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2, 0));

  // add the small tubs
  rotationSolenoid[2] = new CLHEP::HepRotation;
  rotationSolenoid[2]->rotateX(90.0 * CLHEP::deg);
  rotationSolenoid[2]->rotateY(90.0 * CLHEP::deg);
  rotationSolenoid[2]->rotateZ(0.0 * CLHEP::deg);
  solenoid_unionSolid[2] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[1], solenoid_tubSmall, rotationSolenoid[2],
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0]));
  solenoid_unionSolid[3] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[2], solenoid_tubSmall, rotationSolenoid[2],
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0])); // take it...

  // add the z boxes
  solenoid_unionSolid[4] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[3], solenoid_boxZ, 0,
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0] + solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2,
          solenoidBoxX[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2] / 2));
  solenoid_unionSolid[5] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[4], solenoid_boxZ, 0,
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0] + solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2,
          solenoidBoxX[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2] / 2));

  // add small tubs
  rotationSolenoid[3] = new CLHEP::HepRotation;
  rotationSolenoid[3]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[3]->rotateY(90.0 * CLHEP::deg);
  rotationSolenoid[3]->rotateZ(0.0 * CLHEP::deg);
  solenoid_unionSolid[6] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[5], solenoid_tubSmall, rotationSolenoid[3],
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2]));
  solenoid_unionSolid[7] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[6], solenoid_tubSmall, rotationSolenoid[3],
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2]));

  // add big tubs
  rotationSolenoid[4] = new CLHEP::HepRotation;
  rotationSolenoid[4]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[4]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[4]->rotateZ(90.0 * CLHEP::deg);
  solenoid_unionSolid[8] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[7], solenoid_tubBig, rotationSolenoid[4],
      G4ThreeVector(solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  rotationSolenoid[5] = new CLHEP::HepRotation;
  rotationSolenoid[5]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[5]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[5]->rotateZ(180.0 * CLHEP::deg);
  solenoid_unionSolid[9] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[8], solenoid_tubBig, rotationSolenoid[5],
      G4ThreeVector(-solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  solenoid_unionSolid[10] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[9], solenoid_boxX, 0,
      G4ThreeVector(0, 0,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  G4UnionSolid* solenoidLow = solenoid_unionSolid[10];
  G4UnionSolid* solenoidHigh = solenoid_unionSolid[10];

  rotationSolenoid[6] = new CLHEP::HepRotation;
  //	magneticFieldGridON = true;
  rotationSolenoid[6]->rotateX(180.0 * CLHEP::deg);
  rotationSolenoid[6]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[6]->rotateZ(0.0 * CLHEP::deg);

  solenoidHigh_log = new G4LogicalVolume(solenoidHigh, materials->iron,
      "solenoid high", 0, 0, 0, 0);
  solenoidLow_log = new G4LogicalVolume(solenoidLow, materials->iron,
      "solenoid low", 0, 0, 0, 0);

  G4double distanceUpSolenoidToDownSolenoid;
  distanceUpSolenoidToDownSolenoid = 1730.0 * CLHEP::mm;

  new G4PVPlacement(rotationSolenoid[6],
      positionVector
          + G4ThreeVector(0,
              -10 * CLHEP::mm
                  + (solenoidTubBig[0] + solenoidTubSmall[1]
                      + solenoidBoxX[1] / 2
                      + distanceUpSolenoidToDownSolenoid / 2),
              solenoidBoxZ[2] / 2 + solenoidTubSmall[0]
                  + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2),
      solenoidHigh_log, "solenoidHigh_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              -(solenoidTubBig[0] + solenoidTubSmall[1] + solenoidBoxX[1] / 2
                  + distanceUpSolenoidToDownSolenoid / 2),
              -(solenoidBoxZ[2] / 2 + solenoidTubSmall[0]
                  + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2)),
      solenoidLow_log, "solenoidLow_phys", world_log, 0, 0, checkOverlap);

  solenoidHigh_log->SetVisAttributes(colour->blue);
  solenoidLow_log->SetVisAttributes(colour->blue);

  top = new G4Box("", 4190.0 * CLHEP::mm / 2, 730.0 * CLHEP::mm / 2,
      1760.0 * CLHEP::mm / 2);
  cutTop = new G4Box("", 1890.0 * CLHEP::mm / 2,
      200.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm,
      1760.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm);
  cutTub1 = new G4Tubs("", 0, 200.0 * CLHEP::mm,
      1760.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm, 0 * CLHEP::deg, 90.0 * CLHEP::deg);
  cutTub2 = new G4Tubs("", 0, 200.0 * CLHEP::mm,
      1760.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm, 0 * CLHEP::deg, 360.0 * CLHEP::deg);
  cutTrd = new G4Trd("", 800.0 * CLHEP::mm / 2, 1600.0 * CLHEP::mm / 2,
      1760.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm2,
      1760.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm2,
      400.0 * CLHEP::mm / 2 + 0.1 * CLHEP::mm);
  sideCut = new G4Box("", 400.0 * CLHEP::mm, 400.0 * CLHEP::mm,
      1760.0 * CLHEP::mm + 0.1 * CLHEP::mm);
  rotation90X = new G4RotationMatrix;
  rotation90X->rotateX(90.0 * CLHEP::deg);

  subtraction[0] = new G4SubtractionSolid("", top, cutTop, 0,
      G4ThreeVector(0, -265.0 * CLHEP::mm, 0));
  subtraction[1] = new G4SubtractionSolid("", subtraction[0],
      cutTub1, 0, G4ThreeVector(+1890.0 * CLHEP::mm / 2, -365.0 * CLHEP::mm, 0));
  subtraction[2] = new G4SubtractionSolid("", subtraction[1],
      cutTub2, 0, G4ThreeVector(-1890.0 * CLHEP::mm / 2, -365.0 * CLHEP::mm, 0));
  subtraction[3] = new G4SubtractionSolid("", subtraction[2],
      cutTrd, rotation90X, G4ThreeVector(0, 165.0 * CLHEP::mm, 0));

  rotation45Z = new G4RotationMatrix;
  rotation45Z->rotateZ(45.0 * CLHEP::deg);
  subtraction[4] = new G4SubtractionSolid("", subtraction[3],
      sideCut, rotation45Z,
      G4ThreeVector(2095.0 * CLHEP::mm, 365.0 * CLHEP::mm, 0));
  subtraction[5] = new G4SubtractionSolid("", subtraction[4],
      sideCut, rotation45Z,
      G4ThreeVector(-2095.0 * CLHEP::mm, 365.0 * CLHEP::mm, 0));

  rotation180X = new G4RotationMatrix;
  rotation180X->rotateX(180.0 * CLHEP::deg);
  top_log = new G4LogicalVolume(subtraction[5], materials->iron, "", 0, 0, 0, 0);
  bottom_log = new G4LogicalVolume(subtraction[3], materials->iron, "", 0, 0, 0,
      0);
  top_log->SetVisAttributes(colour->red);
  bottom_log->SetVisAttributes(colour->red);

  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 1725.0 * CLHEP::mm, 0),
      top_log, "top magnet", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotation180X,
      positionVector - G4ThreeVector(0, 1725.0 * CLHEP::mm, 0), bottom_log,
      "bottom magnet", world_log, 0, 0, checkOverlap);

  bigBoxes = new G4Box("", 950.0 * CLHEP::mm / 2, 272.0 * CLHEP::cm / 2,
      1760.0 * CLHEP::mm / 2);
  bigBoxes_log[0] = new G4LogicalVolume(bigBoxes, materials->iron, "", 0, 0, 0,
      0);
  bigBoxes_log[1] = new G4LogicalVolume(bigBoxes, materials->iron, "", 0, 0, 0,
      0);
  bigBoxes_log[0]->SetVisAttributes(colour->red);
  bigBoxes_log[1]->SetVisAttributes(colour->red);

  new G4PVPlacement(0, positionVector + G4ThreeVector(1620.0 * CLHEP::mm, 0, 0),
      bigBoxes_log[0], "bigBoxes_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector - G4ThreeVector(1620.0 * CLHEP::mm, 0, 0),
      bigBoxes_log[1], "bigBoxes_phys", world_log, 0, 0, checkOverlap);

  magnetFeet = new G4Box("", 950.0 * CLHEP::mm / 2, 155.0 * CLHEP::mm / 2,
      1760.0 * CLHEP::mm / 2);
  magnetFeet_log = new G4LogicalVolume(magnetFeet, materials->iron, "", 0, 0, 0,
      0);
  magnetFeet_log->SetVisAttributes(colour->red);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(+1300.0 * CLHEP::mm, -2167.5 * CLHEP::mm, 0),
      magnetFeet_log, "magnetFeet_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-1300.0 * CLHEP::mm, -2167.5 * CLHEP::mm, 0),
      magnetFeet_log, "magnetFeet_phys", world_log, 0, 0, checkOverlap);

  smallInnerBox = new G4Box("", 1525.0 * CLHEP::mm / 2, 200.0 * CLHEP::mm / 2,
      550 * CLHEP::mm / 2);
  bigInnerBox = new G4Box("", 1525.0 * CLHEP::mm / 2, 500.0 * CLHEP::mm / 2,
      1100 * CLHEP::mm / 2);

  smallInnerBox_log = new G4LogicalVolume(smallInnerBox, materials->iron, "", 0,
      0, 0, 0);
  bigInnerBox_log = new G4LogicalVolume(bigInnerBox, materials->iron, "", 0, 0,
      0, 0);
  smallInnerBox_log->SetVisAttributes(colour->red);
  bigInnerBox_log->SetVisAttributes(colour->red);

  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, -760.0, -225.0 * CLHEP::mm),
      smallInnerBox_log, "smallInnerBox_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, +760.0, -225.0 * CLHEP::mm),
      smallInnerBox_log, "smallInnerBox_phys", world_log, 0, 0, checkOverlap);

  new G4PVPlacement(0, positionVector + G4ThreeVector(0, -1110.0, 0),
      bigInnerBox_log, "bigInnerBox_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, +1110.0, 0),
      bigInnerBox_log, "bigInnerBox_phys", world_log, 0, 0, checkOverlap);

  addToRegion();
}

void T4SM1::addToRegion(void)
{
  regionManager->addToAbsorberRegion(top_log);
  regionManager->addToAbsorberRegion(bottom_log);
  regionManager->addToAbsorberRegion(magnetFeet_log);
  regionManager->addToAbsorberRegion(bigInnerBox_log);
  regionManager->addToAbsorberRegion(smallInnerBox_log);
}
