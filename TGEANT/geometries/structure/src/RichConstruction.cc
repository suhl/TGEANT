#include "RichConstruction.hh"

RichConstruction::RichConstruction(void)
{
  rich = settingsFile->getStructManager()->getRICH();

  if (rich->useOpticalPhysics && settingsFile->isOpticalPhysicsActivated()) {
    if (rich->gas == "C4F10")
      gasMat = materials->c4f10_optical;
    else if (rich->gas == "N2")
      gasMat = materials->n2_optical;
    else {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__, "RichConstruction: Unknown rich gas name: " + rich->gas + ".");
      gasMat = materials->c4f10_optical;
    }
    aluminiumMat = materials->aluminium_optical;
    vacuumMat = materials->vacuum_optical;
    glassMat = materials->glass_optical;
    bialkaliMat = materials->bialkali_optical;
  } else {
    if (rich->gas == "C4F10")
      gasMat = materials->c4f10_noOptical;
    else if (rich->gas == "N2")
      gasMat = materials->n2_noOptical;
    else {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__, "RichConstruction: Unknown rich gas name: " + rich->gas + ".");
      gasMat = materials->c4f10_noOptical;
    }
    aluminiumMat = materials->aluminium_noOptical;
    vacuumMat = materials->vacuum_noOptical;
    glassMat = materials->glass_noOptical;
    bialkaliMat = materials->bialkali_noOptical;
  }

  setPosition(rich->general.position);

  thicknessSide = 150.0 * CLHEP::mm;
  thicknessFrontBack = 0.5 * CLHEP::mm;
  beamPipeLength = 3125.0 / 2 * CLHEP::mm;
  innerRadius = 6600. * CLHEP::mm;

  vesselBack[0] = 6600.0 / 2 * CLHEP::mm;
  vesselBack[1] = 5325.0 / 2 * CLHEP::mm;
  vesselBack[2] = 1200.0 / 2 * CLHEP::mm;

  vesselMid[0] = 3700.0 / 2 * CLHEP::mm;
  vesselMid[1] = vesselBack[0];
  vesselMid[2] = vesselBack[1];
  vesselMid[3] = vesselBack[1];
  vesselMid[4] = 2000.0 / 2 * CLHEP::mm;

  vesselFront[0] = vesselMid[0];
  vesselFront[1] = vesselMid[0];
  vesselFront[2] = 2420.0 / 2 * CLHEP::mm + thicknessSide;
  vesselFront[3] = vesselMid[3];
  vesselFront[4] = 197.0 / 2 * CLHEP::mm + thicknessFrontBack / 2;

  // distance between pcb position and mirror position from detectors.dat: 3556 * CLHEP::mm
  // distance between mirror and vessel back from technical drawings: 195.0 * CLHEP::mm
  double shiftPcbVessel = - 3556 * CLHEP::mm + innerRadius + 195.0 * CLHEP::mm
      + thicknessFrontBack - 2. * vesselBack[2] - vesselMid[4];
  vesselMidPosition = positionVector + G4ThreeVector(0, 0, shiftPcbVessel);

  alpha = atan(1302.5 / 197);
  pbcRotation[0] = new CLHEP::HepRotation;
  pbcRotation[0]->rotateX(-(90.0 * CLHEP::deg - alpha));
  pbcRotation[1] = new CLHEP::HepRotation;
  pbcRotation[1]->rotateX(90.0 * CLHEP::deg - alpha);

  offsetY = offsetZ = 0;

  vesselBack_box = NULL;
  innerBack_box = NULL;
  vesselMid_trd = NULL;
  innerMid_trd = NULL;
  vesselFront_trd = NULL;
  innerFront_trd = NULL;
  vesselBack_union = NULL;
  innerBack_union = NULL;
  vessel_union = NULL;
  inner_union = NULL;
  inner_inter = NULL;
  vessel_log = NULL;
  inner_log = NULL;
  inner_phys = NULL;

  mirrorRotation = NULL;
  mirror_sphere = NULL;
  mirror_box = NULL;
  mirrorTop_inter[0] = NULL;
  mirrorTop_inter[1] = NULL;
  mirrorBottom_inter[0] = NULL;
  mirrorBottom_inter[1] = NULL;
  mirrorTop_log = NULL;
  mirrorBottom_log = NULL;
  mirrorTop_phys = NULL;
  mirrorBottom_phys = NULL;

  beamPipeAl_tubs = NULL;
  beamPipePET_tubs = NULL;
  beamPipeMylar_tubs = NULL;
  beamPipeHe_tubs = NULL;
  beamPipeAl_log = NULL;
  beamPipePET_log = NULL;
  beamPipeMylar_log = NULL;
  beamPipeHe_log = NULL;

  housingPCB_box = NULL;
  vacuumPCB_box = NULL;
  windowPCB_box = NULL;
  cathodePCB_box = NULL;
}

RichConstruction::~RichConstruction(void)
{
  if (vesselBack_box != NULL)
    delete vesselBack_box;
  if (innerBack_box != NULL)
    delete innerBack_box;
  if (vesselMid_trd != NULL)
    delete vesselMid_trd;
  if (innerMid_trd != NULL)
    delete innerMid_trd;
  if (vesselFront_trd != NULL)
    delete vesselFront_trd;
  if (innerFront_trd != NULL)
    delete innerFront_trd;
  if (vesselBack_union != NULL)
    delete vesselBack_union;
  if (innerBack_union != NULL)
    delete innerBack_union;
  if (vessel_union != NULL)
    delete vessel_union;
  if (inner_union != NULL)
    delete inner_union;
  if (inner_inter != NULL)
    delete inner_inter;
  for (unsigned int i = 0; i < vessel_sub.size(); i++)
    delete vessel_sub.at(i);
  vessel_sub.clear();
  if (vessel_log != NULL)
    delete vessel_log;
  if (inner_log != NULL)
    delete inner_log;

  if (mirrorRotation != NULL)
    delete mirrorRotation;
  if (mirror_sphere != NULL)
    delete mirror_sphere;
  if (mirror_box != NULL)
    delete mirror_box;
  if (mirrorTop_log != NULL)
    delete mirrorTop_log;
  if (mirrorBottom_log != NULL)
    delete mirrorBottom_log;

  if (beamPipeAl_tubs != NULL)
    delete beamPipeAl_tubs;
  if (beamPipePET_tubs != NULL)
    delete beamPipePET_tubs;
  if (beamPipeMylar_tubs != NULL)
    delete beamPipeMylar_tubs;
  if (beamPipeHe_tubs != NULL)
    delete beamPipeHe_tubs;
  if (beamPipeAl_log != NULL)
    delete beamPipeAl_log;
  if (beamPipePET_log != NULL)
    delete beamPipePET_log;
  if (beamPipeMylar_log != NULL)
    delete beamPipeMylar_log;
  if (beamPipeHe_log != NULL)
    delete beamPipeHe_log;

  if (housingPCB_box != NULL)
    delete housingPCB_box;
  if (vacuumPCB_box != NULL)
    delete vacuumPCB_box;
  if (windowPCB_box != NULL)
    delete windowPCB_box;
  if (cathodePCB_box != NULL)
    delete cathodePCB_box;

  for (unsigned int i = 0; i < housingPCB_log.size(); i++)
    delete housingPCB_log.at(i);
  housingPCB_log.clear();
  for (unsigned int i = 0; i < vacuumPCB_log.size(); i++)
    delete vacuumPCB_log.at(i);
  vacuumPCB_log.clear();
  for (unsigned int i = 0; i < windowPCB_log.size(); i++)
    delete windowPCB_log.at(i);
  windowPCB_log.clear();
  for (unsigned int i = 0; i < cathodePCB_log.size(); i++)
    delete cathodePCB_log.at(i);
  cathodePCB_log.clear();

  for (unsigned int i = 0; i < 2; i++) {
    if (mirrorTop_inter[i] != NULL)
      delete mirrorTop_inter[i];
    if (mirrorBottom_inter[i] != NULL)
      delete mirrorBottom_inter[i];
    if (pbcRotation[i] != NULL)
      delete pbcRotation[i];
  }

  for (unsigned int i = 0; i < surface.size(); i++)
    delete surface.at(i);
  surface.clear();
}

void RichConstruction::construct(G4LogicalVolume* world_log)
{
  constructBeamPipe(world_log); // don't change order of functions!
  constructReadout(world_log);
  constructVessel(world_log);
  constructMirror();
  if (rich->useOpticalPhysics && settingsFile->isOpticalPhysicsActivated())
    constructSurface();
}

void RichConstruction::constructBeamPipe(G4LogicalVolume* world_log)
{
  double thicknessMylarPipe = 0.100 * CLHEP::mm;
  double thicknessAl = 0.0002 * CLHEP::mm;
  double thicknessPET = 0.006 * CLHEP::mm;
  double thicknessMylarCap = 0.140 * CLHEP::mm;
  double beamPipeRadius = 50.0 * CLHEP::mm;

  beamPipeAl_tubs = new G4Tubs("beamPipeAl_tubs", 0.0,
      beamPipeRadius + thicknessMylarPipe + thicknessPET + thicknessAl,
      beamPipeLength + thicknessMylarCap + thicknessAl, 0.0, 360 * CLHEP::deg);
  beamPipePET_tubs = new G4Tubs("beamPipePET_tubs", 0.0,
      beamPipeRadius + thicknessMylarPipe + thicknessPET,
      beamPipeLength + thicknessMylarCap + thicknessAl, 0.0, 360 * CLHEP::deg);
  beamPipeMylar_tubs = new G4Tubs("beamPipeMylar_tubs", 0.0,
      beamPipeRadius + thicknessMylarPipe, beamPipeLength + thicknessMylarCap,
      0.0, 360 * CLHEP::deg);
  beamPipeHe_tubs = new G4Tubs("beamPipeHe_tubs", 0.0, beamPipeRadius,
      beamPipeLength, 0.0, 360 * CLHEP::deg);

  beamPipeAl_log = new G4LogicalVolume(beamPipeAl_tubs, aluminiumMat,
      "beamPipeAl_log", 0, 0, 0);
  beamPipePET_log = new G4LogicalVolume(beamPipePET_tubs,
      materials->polyethylene, "beamPipePET_log", 0, 0, 0);
  beamPipeMylar_log = new G4LogicalVolume(beamPipeMylar_tubs, materials->mylar,
      "beamPipeMylar_log", 0, 0, 0);
  beamPipeHe_log = new G4LogicalVolume(beamPipeHe_tubs, materials->helium,
      "beamPipeHe_log", 0, 0, 0);

  new G4PVPlacement(0,
      vesselMidPosition
          + G4ThreeVector(0, 0,
              -beamPipeLength + vesselMid[4] + 2. * vesselBack[2]),
      beamPipeAl_log, "beamPipeAl_phys", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), beamPipePET_log,
      "beamPipePET_phys", beamPipeAl_log, false, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), beamPipeMylar_log,
      "beamPipeMylar_phys", beamPipePET_log, false, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), beamPipeHe_log,
      "beamPipeHe_phys", beamPipeMylar_log, false, 0, checkOverlap);

  beamPipeAl_log->SetVisAttributes(colour->deepyellow);
}

void RichConstruction::constructVessel(G4LogicalVolume* world_log)
{
  vesselBack_box = new G4Box("vesselBack_box", vesselBack[0], vesselBack[1],
      vesselBack[2]);
  innerBack_box = new G4Box("innerBack_box", vesselBack[0] - thicknessSide,
      vesselBack[1] - thicknessSide, vesselBack[2] - thicknessFrontBack / 2);

  vesselMid_trd = new G4Trd("vesselMid_trd", vesselMid[0], vesselMid[1],
      vesselMid[2], vesselMid[3], vesselMid[4]);
  innerMid_trd = new G4Trd("innerMid_trd", vesselMid[0] - thicknessSide,
      vesselMid[1] - thicknessSide, vesselMid[2] - thicknessSide,
      vesselMid[3] - thicknessSide, vesselMid[4]);

  vesselFront_trd = new G4Trd("vesselFront_trd", vesselFront[0], vesselFront[1],
      vesselFront[2], vesselFront[3], vesselFront[4]);
  innerFront_trd = new G4Trd("innerFront_trd", vesselFront[0] - thicknessSide,
      vesselFront[1] - thicknessSide, vesselFront[2] - thicknessSide,
      vesselFront[3] - thicknessSide, vesselFront[4] - thicknessFrontBack / 2);

  // union mid + back
  vesselBack_union = new G4UnionSolid("vesselBack_union", vesselMid_trd,
      vesselBack_box, 0, G4ThreeVector(0, 0, vesselMid[4] + vesselBack[2]));
  innerBack_union = new G4UnionSolid("innerBack_union", innerMid_trd,
      innerBack_box, 0,
      G4ThreeVector(0, 0,
          vesselMid[4] + vesselBack[2] - thicknessFrontBack / 2));

  // union mid/back + front => main position = mid position
  vessel_union = new G4UnionSolid("vessel_union", vesselBack_union,
      vesselFront_trd, 0, G4ThreeVector(0, 0, -vesselMid[4] - vesselFront[4]));
  inner_union = new G4UnionSolid("inner_union", innerBack_union, innerFront_trd,
      0,
      G4ThreeVector(0, 0,
          -vesselMid[4] - vesselFront[4] + thicknessFrontBack / 2));

  vessel_sub.push_back(
      new G4SubtractionSolid("", vessel_union, beamPipeAl_tubs, 0,
          G4ThreeVector(0, 0,
              vesselMid[4] + 2.* vesselBack[2] - beamPipeLength)));
  for (unsigned int i = 0; i < pcbInformation.size(); i++) {
    int k = i < 8 ? 0 : 1;
    vessel_sub.push_back(
        new G4SubtractionSolid("", vessel_sub.back(), housingPCB_box,
            pbcRotation[k],
            pcbInformation.at(i).position
                + G4ThreeVector(0, (1 - 2 * k) * offsetY, -offsetZ)
                - vesselMidPosition));
  }

  vessel_log = new G4LogicalVolume(vessel_sub.back(), aluminiumMat,
      "vessel_log", 0, 0, 0);
  regionManager->addToOpticalRegion(vessel_log);

  inner_inter = new G4IntersectionSolid("inner_inter", inner_union,
      vessel_sub.back());
  inner_log = new G4LogicalVolume(inner_inter, gasMat, "inner_log", 0, 0, 0);
  new G4PVPlacement(0, vesselMidPosition, vessel_log, "vessel_phys", world_log,
      false, 0, checkOverlap);
  inner_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), inner_log,
      "inner_phys", vessel_log, false, 0, checkOverlap);

  if (rich->visibleHousing)
    vessel_log->SetVisAttributes(colour->silver_20);
  inner_log->SetVisAttributes(colour->invisible);
}

void RichConstruction::constructMirror(void)
{
  double outerRadius = innerRadius + 7.0 * CLHEP::mm;

  mirror_sphere = new G4Sphere("mirror_sphere", innerRadius, outerRadius, 0.0,
      180.0 * CLHEP::deg, 0.0, 360.0 * CLHEP::deg);

  mirror_box = new G4Box("mirror_box", vesselBack[0], vesselBack[1] / 2,
      vesselBack[2] + vesselMid[4]);

  mirrorRotation = new CLHEP::HepRotation;
  mirrorRotation->rotateX(-90.0 * CLHEP::deg);

  // intersection 1: c4f10 volume with mirror
  mirrorTop_inter[0] = new G4IntersectionSolid("mirrorTop_inter[0]",
      inner_inter, mirror_sphere, mirrorRotation,
      G4ThreeVector(0, 1600.0 * CLHEP::mm,
          vesselMid[4] + 2. * vesselBack[2] - thicknessFrontBack - innerRadius
              - 195.0 * CLHEP::mm));
  // intersection 2: cut the bottom half, keep the top
  mirrorTop_inter[1] = new G4IntersectionSolid("mirrorTop_inter[1]",
      mirrorTop_inter[0], mirror_box, 0,
      G4ThreeVector(0, vesselBack[1] / 2, vesselBack[2]));

  mirrorBottom_inter[0] = new G4IntersectionSolid("mirrorBottom_inter[0]",
      inner_inter, mirror_sphere, mirrorRotation,
      G4ThreeVector(0, -1600.0 * CLHEP::mm,
          vesselMid[4] + 2. * vesselBack[2] - thicknessFrontBack - innerRadius
              - 195.0 * CLHEP::mm));
  mirrorBottom_inter[1] = new G4IntersectionSolid("mirrorBottom_inter[1]",
      mirrorBottom_inter[0], mirror_box, 0,
      G4ThreeVector(0, -vesselBack[1] / 2, vesselBack[2]));

  mirrorTop_log = new G4LogicalVolume(mirrorTop_inter[1],
      materials->borosilicate, "mirrorTop_log", 0, 0, 0);
  mirrorBottom_log = new G4LogicalVolume(mirrorBottom_inter[1],
      materials->borosilicate, "mirrorBottom_log", 0, 0, 0);

  mirrorTop_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), mirrorTop_log,
      "mirrorTop_phys", inner_log, false, 0, checkOverlap);
  mirrorBottom_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0),
      mirrorBottom_log, "mirrorBottom_phys", inner_log, false, 0, checkOverlap);

//   mirrorTop_log->SetVisAttributes(colour->lightskyblue);
//   mirrorBottom_log->SetVisAttributes(colour->lightskyblue);
  mirrorTop_log->SetVisAttributes(colour->yellow_50); // only for Nicoles 45deg picture
  mirrorBottom_log->SetVisAttributes(colour->yellow_50);
}

void RichConstruction::constructReadout(G4LogicalVolume* world_log)
{
  double sdSize = 57.6 / 2 * CLHEP::cm;
  double frameSize = 59.52 / 2 * CLHEP::cm;
  double quarzThickness = 5.0 / 2 * CLHEP::mm;
  double gapThickness = 3.4 * CLHEP::cm;

  double moduleThickness = 20.0 / 2 * CLHEP::cm;
  double aluThickness = frameSize - sdSize;

  housingPCB_box = new G4Box("housingPCB_box", frameSize, frameSize,
      moduleThickness);
  vacuumPCB_box = new G4Box("vacuumPCB_box", frameSize - aluThickness,
      frameSize - aluThickness, moduleThickness - aluThickness / 2);
  windowPCB_box = new G4Box("windowPCB_box", sdSize, sdSize, quarzThickness);
  cathodePCB_box = new G4Box("cathodePCB_box", sdSize, sdSize, quarzThickness);

  // positioning of modules (this is the position of the sensitive detector from detectors.dat!)
  double distToMod = 193.1 * CLHEP::cm;
  G4ThreeVector referenceTop = positionVector + G4ThreeVector(0, distToMod, 0);
  G4ThreeVector referenceBot = positionVector + G4ThreeVector(0, -distToMod, 0);
  double posX_a = 95.25 * CLHEP::cm;
  double posX_b = 31.75 * CLHEP::cm;
  double posY = 29.76 * CLHEP::cm * sin(alpha);
  double posZ = 29.76 * CLHEP::cm * cos(alpha);

  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(posX_a, posY, posZ), "RA01P0_u",
          901));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(posX_a, -posY, -posZ),
          "RA01P0_d", 902));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(posX_b, posY, posZ), "RA01P1_u",
          903));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(posX_b, -posY, -posZ),
          "RM01P1_d", 904));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(-posX_b, posY, posZ),
          "RA01P2_u", 905));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(-posX_b, -posY, -posZ),
          "RM01P2_d", 906));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(-posX_a, posY, posZ),
          "RA01P3_u", 907));
  pcbInformation.push_back(
      setPCBStruct(referenceTop + G4ThreeVector(-posX_a, -posY, -posZ),
          "RA01P3_d", 908));

  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(posX_a, posY, -posZ),
          "RA01P4_u", 909));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(posX_a, -posY, posZ),
          "RA01P4_d", 910));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(posX_b, posY, -posZ),
          "RM01P5_u", 911));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(posX_b, -posY, posZ),
          "RA01P5_d", 912));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(-posX_b, posY, -posZ),
          "RM01P6_u", 913));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(-posX_b, -posY, posZ),
          "RA01P6_d", 914));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(-posX_a, posY, -posZ),
          "RA01P7_u", 915));
  pcbInformation.push_back(
      setPCBStruct(referenceBot + G4ThreeVector(-posX_a, -posY, posZ),
          "RA01P7_d", 916));

  // offset between given position (center of sd plane) and middle of pcb box
  double distCatToMiddle = moduleThickness - gapThickness
      - 2.0 * quarzThickness;
  offsetY = distCatToMiddle * cos(alpha);
  offsetZ = distCatToMiddle * sin(alpha);

  for (unsigned int i = 0; i < pcbInformation.size(); i++) {
    housingPCB_log.push_back(
        new G4LogicalVolume(housingPCB_box, aluminiumMat, "housingPCB_log", 0,
            0, 0));
    vacuumPCB_log.push_back(
        new G4LogicalVolume(vacuumPCB_box, vacuumMat, "vacuumPCB_log", 0, 0,
            0));
    windowPCB_log.push_back(
        new G4LogicalVolume(windowPCB_box, glassMat, "windowPCB_log", 0, 0, 0));
    cathodePCB_log.push_back(
        new G4LogicalVolume(cathodePCB_box, bialkaliMat, "cathodePCB_log", 0, 0,
            0));

    housingPCB_log.back()->SetVisAttributes(colour->green);
    vacuumPCB_log.back()->SetVisAttributes(colour->invisible);
    windowPCB_log.back()->SetVisAttributes(colour->invisible);
    cathodePCB_log.back()->SetVisAttributes(colour->invisible);
    
    regionManager->addToOpticalRegion(housingPCB_log.back());

    int k = i < 8 ? 0 : 1;
    housingPCB_phys.push_back(
        new G4PVPlacement(pbcRotation[k],
            pcbInformation.at(i).position
                + G4ThreeVector(0, (1 - 2 * k) * offsetY, -offsetZ),
            housingPCB_log.back(), pcbInformation.at(i).name, world_log, false, 0,
            checkOverlap));
    vacuumPCB_phys.push_back(
        new G4PVPlacement(0, G4ThreeVector(0, 0, aluThickness / 2),
            vacuumPCB_log.back(), "vacuum" + pcbInformation.at(i).name, housingPCB_log.back(),
            false, 0, checkOverlap));
    windowPCB_phys.push_back(
        new G4PVPlacement(0,
            G4ThreeVector(0, 0,
                moduleThickness - aluThickness / 2 - quarzThickness),
            windowPCB_log.back(), "window" + pcbInformation.at(i).name, vacuumPCB_log.back(),
            false, 0, checkOverlap));
    cathodePCB_phys.push_back(
        new G4PVPlacement(0,
            G4ThreeVector(0, 0,
                moduleThickness - aluThickness / 2 - 3 * quarzThickness
                    - gapThickness), cathodePCB_log.back(),
            "cathode" + pcbInformation.at(i).name, vacuumPCB_log.back(), false, 0,
            checkOverlap));

    cathodePCB_log.back()->SetSensitiveDetector(
        new T4SensitiveRich(pcbInformation.at(i)));
  }
}

void RichConstruction::constructSurface(void)
{
  surface.push_back(
      new G4LogicalBorderSurface("mirrorSurface[0]", inner_phys, mirrorTop_phys,
          materials->surfaceMirror));
  surface.push_back(
      new G4LogicalBorderSurface("mirrorSurface[1]", inner_phys,
          mirrorBottom_phys, materials->surfaceMirror));

  for (unsigned int i = 0; i < housingPCB_phys.size(); i++) {
    surface.push_back(
        new G4LogicalBorderSurface("vacAl", vacuumPCB_phys.at(i),
            housingPCB_phys.at(i), materials->surfaceAluminium));
    surface.push_back(
        new G4LogicalBorderSurface("windowAl", windowPCB_phys.at(i),
            housingPCB_phys.at(i), materials->surfaceAluminium));
    surface.push_back(
        new G4LogicalBorderSurface("windowVac", windowPCB_phys.at(i),
            vacuumPCB_phys.at(i), materials->surfaceGlassVacuum));
    surface.push_back(
        new G4LogicalBorderSurface("vacWindow", vacuumPCB_phys.at(i),
            windowPCB_phys.at(i), materials->surfaceGlassVacuum));
    surface.push_back(
        new G4LogicalBorderSurface("vacBialkali", vacuumPCB_phys.at(i),
            cathodePCB_phys.at(i), materials->surfaceVacuumBialkali));
  }
}

T4RichPCB RichConstruction::setPCBStruct(G4ThreeVector position, G4String name, int id)
{
  T4RichPCB richPCB = {position, name, id};
  return richPCB;
}
