#include "MF3Construction.hh"

MF3Construction::MF3Construction(T4SDetector* mf3)
{
  setPosition(mf3->position);

  absorber_box = NULL;
  hole_box = NULL;
  absorber_sub = NULL;
  absorber_log = NULL;
}

MF3Block::MF3Block(T4SDetector* mf3)
{
  setPosition(mf3->position);

  block_box = NULL;
  block_log = NULL;
}

MF3Construction::~MF3Construction(void)
{
  if (absorber_box != NULL)
    delete absorber_box;
  if (hole_box != NULL)
    delete hole_box;
  if (absorber_sub != NULL)
    delete absorber_sub;
  if (absorber_log != NULL)
    delete absorber_log;
}

MF3Block::~MF3Block(void)
{
  if (block_box != NULL)
    delete block_box;
  if (block_log != NULL)
    delete block_log;
}

void MF3Construction::construct(G4LogicalVolume* world_log)
{
  G4double dimensionsMF3[3];
  dimensionsMF3[0] = 800.0 / 2 * CLHEP::cm;
  dimensionsMF3[1] = 420.0 / 2 * CLHEP::cm;
  dimensionsMF3[2] = 40.0 / 2 * CLHEP::cm;

  G4double dimensionsHole[3];
  dimensionsHole[0] = 90.0 / 2 * CLHEP::cm;
  dimensionsHole[1] = 60.0 / 2 * CLHEP::cm;
  dimensionsHole[2] = dimensionsMF3[2] + 0.1 * CLHEP::cm;

  absorber_box = new G4Box("absorber_box", dimensionsMF3[0], dimensionsMF3[1],
      dimensionsMF3[2]);
  hole_box = new G4Box("hole_box", dimensionsHole[0], dimensionsHole[1],
      dimensionsHole[2]);

  absorber_sub = new G4SubtractionSolid("absorber_sub", absorber_box, hole_box,
      0, G4ThreeVector(35.0 * CLHEP::cm, 0, 0));
  absorber_log = new G4LogicalVolume(absorber_sub, materials->iron, "absorber_log", 0, 0,
      0, 0);
  absorber_log->SetVisAttributes(colour->darkgreen);
  new G4PVPlacement(0, positionVector, absorber_log,
      "MF3", world_log, 0, 0, checkOverlap);

  regionManager->addToAbsorberRegion(absorber_log);
}

void MF3Block::construct(G4LogicalVolume* world_log)
{
  G4double dimensionsBlock[3];
  dimensionsBlock[0] = 40.0 / 2 * CLHEP::cm;
  dimensionsBlock[1] = 300.0 / 2 * CLHEP::cm;
  dimensionsBlock[2] = 80.0 / 2 * CLHEP::cm;
  block_box = new G4Box("block_box", dimensionsBlock[0], dimensionsBlock[1],
      dimensionsBlock[2]);
  block_log = new G4LogicalVolume(block_box, materials->iron, "block_log", 0, 0,
      0, 0);
  block_log->SetVisAttributes(colour->darkgreen);
  new G4PVPlacement(0, positionVector, block_log, "MF3_Block", world_log, 0, 0,
      checkOverlap);

  regionManager->addToAbsorberRegion(block_log);
}
