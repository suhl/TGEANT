#include "T4DYAbsorber.hh"
#include "T4WorldConstruction.hh"

T4DYAbsorber::T4DYAbsorber(T4SDetector* det)
{
  yearSetup = strToDouble(T4EventManager::getInstance()->getTriggerPlugin()->getPluginName().substr(0,4));

  setPosition(det->position);

  zLayer = 20. / 2. * CLHEP::cm;
  blockDimension[0] = 120. / 2. * CLHEP::cm;
  blockDimension[1] = 110. / 2. * CLHEP::cm;
  blockDimension[2] = 11. * zLayer;

  zLayerAlumina[0] = 19. / 2. * CLHEP::cm; // layer 1-8
  zLayerAlumina[1] = 19. / 2. * CLHEP::cm; // layer 9-> changed according to CAD drawing
//  zLayerAlumina[2] = 20. / 2. * CLHEP::cm; // layer 0

  // DONE: correct values for alumina boxes (not squared), according to last CAD drawing
  xLayerAlumina[0] = 64. / 2. * CLHEP::cm;
  xLayerAlumina[1] = 72. / 2. * CLHEP::cm;
  xLayerAlumina[2] = 72. / 2. * CLHEP::cm;
  xLayerAlumina[3] = 82. / 2. * CLHEP::cm;
  xLayerAlumina[4] = 82. / 2. * CLHEP::cm;
  xLayerAlumina[5] = 92. / 2. * CLHEP::cm;
  xLayerAlumina[6] = 92. / 2. * CLHEP::cm;
  xLayerAlumina[7] = 102. / 2. * CLHEP::cm;
  xLayerAlumina[8] = 102. / 2. * CLHEP::cm;
  xLayerAlumina[9] = 112. / 2. * CLHEP::cm;

  yLayerAlumina[0] = 52. / 2. * CLHEP::cm;
  yLayerAlumina[1] = 62. / 2. * CLHEP::cm;
  yLayerAlumina[2] = 62. / 2. * CLHEP::cm;
  yLayerAlumina[3] = 72. / 2. * CLHEP::cm;
  yLayerAlumina[4] = 72. / 2. * CLHEP::cm;
  yLayerAlumina[5] = 82. / 2. * CLHEP::cm;
  yLayerAlumina[6] = 82. / 2. * CLHEP::cm;
  yLayerAlumina[7] = 92. / 2. * CLHEP::cm;
  yLayerAlumina[8] = 92. / 2. * CLHEP::cm;
  yLayerAlumina[9] = 102. / 2. * CLHEP::cm;

  innerRadiusHole[0] = 10. / 2. * CLHEP::cm;
  innerRadiusHole[1] = 10. / 2. * CLHEP::cm;
  innerRadiusHole[2] = 10. / 2. * CLHEP::cm;
  innerRadiusHole[3] = 9.5 / 2. * CLHEP::cm;
  innerRadiusHole[4] = 9.5 / 2. * CLHEP::cm;
  innerRadiusHole[5] = 9.5 / 2. * CLHEP::cm;
  innerRadiusHole[6] = 9.5 / 2. * CLHEP::cm;
  innerRadiusHole[7] = 9.0 / 2. * CLHEP::cm;
  innerRadiusHole[8] = 8.5 / 2. * CLHEP::cm;

  xConcrete[0] = 680. / 2. * CLHEP::cm;  // bottom block
  xConcrete[1] = 560. / 2. * CLHEP::cm;  // base block
  xConcrete[2] = 200. / 2. * CLHEP::cm;  // right and left block
  xConcrete[3] = 132. / 2. * CLHEP::cm;  // little base block
  xConcrete[4] = 480. / 2. * CLHEP::cm;  // top block


  yConcrete[0] = 80. / 2. * CLHEP::cm; //Bottom block
  yConcrete[1] = 80. / 2. * CLHEP::cm; //base block
  yConcrete[2] = 240. / 2. * CLHEP::cm; //right and left block
  yConcrete[3] = 68. / 2. * CLHEP::cm; //Little base block
  yConcrete[4] = 160. / 2. * CLHEP::cm; //top block

  zConcrete[0] = 230. / 2. * CLHEP::cm;
  zConcrete[1] = 220. / 2. * CLHEP::cm;
  zConcrete[2] = 200. / 2. * CLHEP::cm;
  zConcrete[3] = 200. / 2. * CLHEP::cm;
  zConcrete[4] = 200. / 2. * CLHEP::cm;

  zShiftConc = - 10. * CLHEP::cm;
  steelThickness = 0.25 * CLHEP::cm;
  aluminiumTargetLength = 7. / 2. * CLHEP::cm;
  noseRadiusUp = 27.5 * CLHEP::cm;
  noseRadiusDown = 32.8 * CLHEP::cm;
  noseZlenght = 29.5 / 2. * CLHEP::cm;
  noseSpace = 6.37 * CLHEP::cm;
  noseEndLenght = 6. / 2. *CLHEP::cm;

  SheetDimensions[0] = 118. / 2. * CLHEP::cm;
  SheetDimensions[1] = 110. / 2. * CLHEP::cm;
  SheetDimensions[2] = 0.3175 / 2. * CLHEP::cm;

  PolyeDimensions[0] = 108. / 2. * CLHEP::cm;
  PolyeDimensions[1] = 100. / 2. * CLHEP::cm;
  PolyeDimensions[2] = 1. / 2. * CLHEP::cm;

  nose_cons = NULL;
  nose_end = NULL; //Add nose endcap
}

void T4DYAbsorber::construct(G4LogicalVolume* world_log)
{
  //TODO: Aluminum layers are covering all the surface, not just the alumina one
  T4TargetBackend* targetBackend = T4ActiveTarget::getInstance()->getTarget();
  if (targetBackend == NULL)
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
         "T4DYAbsorber::construct: T4TargetBackend not found! The DYAbsorber will only interact as a target if a target is activated.");
  else {
    targetBackend->setZLast(positionVector.z() + 11. * zLayer);
  }
  // stainless steel (this is used as mother volume for all layers)
  // the positionVector of the DYAbsorber is the center of this box
  // (or: the center of the 11 layers)

  //Bottom concrete block
  box.push_back(new G4Box("bottom_shielding", xConcrete[0], yConcrete[0], zConcrete[0]));
  log.push_back(new G4LogicalVolume(box.back(), materials->concrete, "bottom_shield_log"));
  new G4PVPlacement(0, G4ThreeVector(0.,-274.5 * CLHEP::cm,15.2*CLHEP::cm+zShiftConc)+positionVector,log.back(), "bottom_shield", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);

  //Base concrete block
  box.push_back(new G4Box("big_base_shielding", xConcrete[1], yConcrete[1], zConcrete[1]));
  log.push_back(new G4LogicalVolume(box.back(), materials->concrete, "big_base_shield_log"));
  new G4PVPlacement(0, G4ThreeVector(0.,-194.5 * CLHEP::cm,5.*CLHEP::cm+zShiftConc)+positionVector,log.back(), "big_base_shield", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);

  box.push_back(new G4Box("side_shielding", xConcrete[2], yConcrete[2], zConcrete[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->concrete, "side_shield_log"));
  new G4PVPlacement(0, G4ThreeVector(166. * CLHEP::cm,-34.5 * CLHEP::cm,zShiftConc)+positionVector,log.back(), "right_shield", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);
  //right concrete block
  new G4PVPlacement(0, G4ThreeVector(-166. * CLHEP::cm,-34.5 * CLHEP::cm,zShiftConc)+positionVector,log.back(), "left_shield", world_log,0,0, checkOverlap);
  //left concrete block
  log.back()->SetVisAttributes(colour->silver_20);

  //little Base concrete block
  box.push_back(new G4Box("little_base_shielding", xConcrete[3], yConcrete[3], zConcrete[3]));
  log.push_back(new G4LogicalVolume(box.back(), materials->concrete, "little_base_shield_log"));
  new G4PVPlacement(0, G4ThreeVector(0.,-120.5 * CLHEP::cm, zShiftConc)+positionVector,log.back(), "little_base_shield", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);

  //top concrete block
  box.push_back(new G4Box("top_shielding", xConcrete[4], yConcrete[4], zConcrete[4]));
  log.push_back(new G4LogicalVolume(box.back(), materials->concrete, "top_shield_log"));
  new G4PVPlacement(0, G4ThreeVector(0.,165.5*CLHEP::cm, zShiftConc)+positionVector,log.back(), "top_shield", world_log,0,0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);

  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("absorber_mother_box", blockDimension[0], blockDimension[1], blockDimension[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->stainlessSteel, "absorber_mother_log"));
  new G4PVPlacement(0, positionVector, log.back(), "DYAbsorber_mother", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray_50);


  if (yearSetup <= 2014) {
      //Last 5 mm of stainless steel
      box.push_back(new G4Box("last_lay",blockDimension[0],blockDimension[1],0.25 * CLHEP::cm)); //Before was just 0.25, to be checked with Angelo
      log.push_back(new G4LogicalVolume(box.back(), materials->stainlessSteel, "add_last_layer"));
      new G4PVPlacement(0, positionVector+G4ThreeVector(0.,0., blockDimension[2]+0.25 * CLHEP::cm), log.back(), "add_last_layer", world_log,0,0, checkOverlap);
      log.back()->SetVisAttributes(colour->darkgray_50);
  } else {
      //Trick: we subtract a volume of air and we re-put it in the back part
      unsigned int fair_index = log.size();
      box.push_back(new G4Box("air_4_lit",blockDimension[0],blockDimension[1],0.65 * CLHEP::cm));
      log.push_back(new G4LogicalVolume(box.back(),materials->air_noOptical,"air_4_lit"));
      new G4PVPlacement(0, G4ThreeVector(0.,0.,blockDimension[2] - zLayer + 0.65*CLHEP::cm),log.back(),"air_4_add",log.at(motherIndex),0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->invisible);
      //Li2CO3 sheet
      box.push_back(new G4Box("LiC02sheet", SheetDimensions[0], SheetDimensions[1], SheetDimensions[2]));
      log.push_back(new G4LogicalVolume(box.back(),materials->Li2CO3,"Li2CO3_Sheet"));
      new G4PVPlacement(0, G4ThreeVector(0.,0.,0.),log.back(),"LiSheet",log.at(fair_index),0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->darkblue);
      //Its stainless steel support
      box.push_back(new G4Box("end_1ss",blockDimension[0],blockDimension[1],0.05 * CLHEP::cm));
      log.push_back(new G4LogicalVolume(box.back(),materials->stainlessSteel,"end_1ss"));
      new G4PVPlacement(0, G4ThreeVector(0.,0.,SheetDimensions[2]+ 0.05*CLHEP::cm),log.back(),"end_1ss",log.at(fair_index),0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->black);
      //Adding again stainless steel at the end
      box.push_back(new G4Box("end_ss",blockDimension[0],blockDimension[1],0.65 * CLHEP::cm));
      log.push_back(new G4LogicalVolume(box.back(),materials->air_noOptical,"end_ss"));
      new G4PVPlacement(0,positionVector+G4ThreeVector(0.,0.,blockDimension[2] + 0.65*CLHEP::cm),log.back(),"end_ss",world_log,0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->darkgray);
      //Last 5 mm of stainless steel, shifted of 1.3 cm
      box.push_back(new G4Box("last_lay",blockDimension[0],blockDimension[1],0.05 * CLHEP::cm));
      log.push_back(new G4LogicalVolume(box.back(), materials->stainlessSteel, "add_last_layer"));
      new G4PVPlacement(0, positionVector+G4ThreeVector(0.,0., blockDimension[2]+1.35 * CLHEP::cm), log.back(), "add_last_layer", world_log,0,0, checkOverlap);
      log.back()->SetVisAttributes(colour->black);
      //Li2CO3 sheet 2
      box.push_back(new G4Box("LiC02sheet_2", SheetDimensions[0], SheetDimensions[1], SheetDimensions[2]));
      log.push_back(new G4LogicalVolume(box.back(),materials->Li2CO3,"Li2CO3_Sheet_2"));
      new G4PVPlacement(0, positionVector+G4ThreeVector(0.,0., blockDimension[2]+1.4*CLHEP::cm+SheetDimensions[2]),log.back(),"LiSheet_2",world_log,0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->darkblue);
      //Polyethilene backend
      box.push_back(new G4Box("PolyBack", PolyeDimensions[0], PolyeDimensions[1], PolyeDimensions[2]));
      log.push_back(new G4LogicalVolume(box.back(),materials->polyethylene,"Polyethilene_Sheet"));
      new G4PVPlacement(0, positionVector+G4ThreeVector(0.,0., blockDimension[2]+1.4*CLHEP::cm+2*SheetDimensions[2]+PolyeDimensions[2]),log.back(),"Polyethi_bkend",world_log,0,0,checkOverlap);
      log.back()->SetVisAttributes(colour->gray_50);
  }

  // 10 layers with alumina, each center filled with stainless steel crown
  // and air, aluminium or tungsten.

  for (int i = 0; i < 10; i++) {

    // aluminium tube first
    unsigned int currentIndex = log.size();
    box.push_back(new G4Box("aluminium_box", xLayerAlumina[i], yLayerAlumina[i], zLayer));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "aluminium_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, zLayer * (i-5) * 2.), log.back(), "DYAbsorber_aluminium", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->magenta_20/*gray_50*/);

    // aluminium filled with aluminia
    box.push_back(new G4Box("alumina_box", xLayerAlumina[i], yLayerAlumina[i], zLayerAlumina[i==9]));
    G4VSolid* solid;

    if (i != 9 ) {
      tubs.push_back(new G4Tubs("substract_tubs", 0, innerRadiusHole[i] + steelThickness, zLayerAlumina[0], 0, 2.* M_PI));
      subs.push_back(new G4SubtractionSolid("subtraction_subs", box.back(), tubs.back()));
      solid = subs.back();
    } else {
      solid = box.back();
    }

    log.push_back(new G4LogicalVolume(solid, materials->Al2O3, "alumina_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "DYAbsorber_alumina", log.at(currentIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible/*magenta*/);

    // no center for the last layer no.9
    if (i == 9)
      break;

    // stainless steel crown inside
    tubs.push_back(new G4Tubs("steel_tubs", innerRadiusHole[i], innerRadiusHole[i] + steelThickness, zLayer, 0, 2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->stainlessSteel, "steel_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "DYAbsorber_steelcrown", log.at(currentIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible/*black_20*/);
    
    
    // center filled with air, aluminium or tungsten
    G4Material* mat = materials->air_noOptical;
    bool addTarget = false;
    string physName = "DYAbsorber_air";
    if (i >= 3) {
      mat = materials->tungsten;
      addTarget = true;
      physName = "TARGET_w";
    }
    
    unsigned int indexCenter = log.size();
    tubs.push_back(new G4Tubs("center_tubs", 0., innerRadiusHole[i], zLayer, 0, 2.* M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), mat, "center_log"));
    G4VPhysicalVolume* phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), physName, log.at(currentIndex), 0, 0, checkOverlap);

    // we want to use this volume if it is tungsten as target
    if (addTarget && targetBackend != NULL)
      targetBackend->addToTarget(phys);

    // add logical volumes to target region to limit the maximal step size
    if (addTarget)
      regionManager->addToTargetRegion(log.at(indexCenter));

    // some visualization
    if (i < 3)
      log.back()->SetVisAttributes(colour->invisible);
    else
      log.back()->SetVisAttributes(colour->white);

    // aluminium target in layer no.1
    if (i == 1) {
      tubs.push_back(new G4Tubs("aluTarget_tubs", 0., innerRadiusHole[i], aluminiumTargetLength, 0, 2.* M_PI));
      log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "aluTarget_log"));
      G4VPhysicalVolume* al_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "TARGET_al", log.at(indexCenter), 0, 0, checkOverlap);
      if (targetBackend != NULL)
        targetBackend->addToTarget(al_phys);
      regionManager->addToTargetRegion(log.back());
      log.back()->SetVisAttributes(colour->white);
    }
  }

  //Now we build up the nose
  //Adding nose endcap
  nose_end = new G4Cons("nose_end",innerRadiusHole[0],noseRadiusDown,innerRadiusHole[0],noseRadiusDown,noseEndLenght,0.,2.*M_PI);
  log.push_back(new G4LogicalVolume(nose_end,materials->aluminium_noOptical, "nose_end_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-blockDimension[2]-noseSpace-noseEndLenght), log.back(), "DYAbsorber_endcap_nose", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);
  nose_cons = new G4Cons("nose_cons",innerRadiusHole[0], noseRadiusUp,innerRadiusHole[0],noseRadiusDown, noseZlenght,0.,2.*M_PI);
  log.push_back(new G4LogicalVolume(nose_cons, materials->aluminium_noOptical, "nose_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0.,0.,-blockDimension[2]-noseSpace-(noseEndLenght*2)-noseZlenght), log.back(), "DYAbsorber_nose", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver_20);
}

T4DYAbsorber::~T4DYAbsorber(void)
{
  for (unsigned int i = 0; i < box.size(); i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size(); i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < subs.size(); i++)
    delete subs.at(i);
  subs.clear();

  for (unsigned int i = 0; i < log.size(); i++)
    delete log.at(i);
  log.clear();
  //some cosmetics to check well the memory leaks (e.g. with Valgrind)
  if (nose_cons != NULL)
    delete nose_cons;
  if (nose_end != NULL)
    delete nose_end;
}
