#include "T4SM2.hh"

T4SM2::T4SM2(T4SDetector* sm2)
{
  // world position
  // this position is a relativ position in the world
  // point (0,0,0) is in the middle of the magnet, where the
  // beam should pass through. z axis along beam
  setPosition(sm2->position);

  for (unsigned int i = 0; i < 7; i++)
    rotationSolenoid[i] = NULL;
  solenoid_boxX = NULL;
  solenoid_boxZ = NULL;
  solenoid_tubBig = NULL;
  solenoid_tubSmall = NULL;
  for (unsigned int i = 0; i < 11; i++)
    solenoid_unionSolid[i] = NULL;
  solenoidHigh_log = NULL;
  solenoidLow_log = NULL;

  magnet_sideWall = NULL;
  magnet_innerPlate = NULL;
  magnet_innerBlock = NULL;
  for (unsigned int i = 0; i < 5; i++)
    magnet_unionSolid[i] = NULL;
  magnetMainVolume_log = NULL;

  rotationMagnet = NULL;
  for (unsigned int i = 0; i < 2; i++)
    rotationMagnetCut[i] = NULL;
  magnet_bottomPlate = NULL;
  magnet_topPlate = NULL;
  magnet_feet = NULL;
  magnet_cutBox = NULL;
  for (unsigned int i = 0; i < 4; i++)
    magnet_cutCircle[i] = NULL;
  magnet_cutBoxUp = NULL;
  magnet_cutTrd = NULL;
  yokeHole_box = NULL;
  yokeHoleTop_box = NULL;
  magnet_bottomUnion = NULL;
  for (unsigned int i = 0; i < 7; i++)
    magnet_bottomSubstraction[i] = NULL;
  for (unsigned int i = 0; i < 10; i++)
    magnet_topSubstraction[i] = NULL;
  magnetBottom_log = NULL;
  magnetTop_log = NULL;
}

T4SM2::~T4SM2(void)
{
  for (unsigned int i = 0; i < 7; i++)
    if (rotationSolenoid[i] != NULL)
      delete rotationSolenoid[i];
  if (solenoid_boxX != NULL)
    delete solenoid_boxX;
  if (solenoid_boxZ != NULL)
    delete solenoid_boxZ;
  if (solenoid_tubBig != NULL)
    delete solenoid_tubBig;
  if (solenoid_tubSmall != NULL)
    delete solenoid_tubSmall;
  for (unsigned int i = 0; i < 11; i++)
    if (solenoid_unionSolid[i] != NULL)
      delete solenoid_unionSolid[i];
  if (solenoidHigh_log != NULL)
    delete solenoidHigh_log;
  if (solenoidLow_log != NULL)
    delete solenoidLow_log;

  if (magnet_sideWall != NULL)
    delete magnet_sideWall;
  if (magnet_innerPlate != NULL)
    delete magnet_innerPlate;
  if (magnet_innerBlock != NULL)
    delete magnet_innerBlock;
  for (unsigned int i = 0; i < 5; i++)
    if (magnet_unionSolid[i] != NULL)
      delete magnet_unionSolid[i];
  if (magnetMainVolume_log != NULL)
    delete magnetMainVolume_log;

  if (rotationMagnet != NULL)
    delete rotationMagnet;
  for (unsigned int i = 0; i < 2; i++)
    if (rotationMagnetCut[i] != NULL)
      delete rotationMagnetCut[i];
  if (magnet_bottomPlate != NULL)
    delete magnet_bottomPlate;
  if (magnet_topPlate != NULL)
    delete magnet_topPlate;
  if (magnet_feet != NULL)
    delete magnet_feet;
  if (magnet_cutBox != NULL)
    delete magnet_cutBox;
  for (unsigned int i = 0; i < 4; i++)
    if (magnet_cutCircle[i] != NULL)
      delete magnet_cutCircle[i];
  if (magnet_cutBoxUp != NULL)
    delete magnet_cutBoxUp;
  if (magnet_cutTrd != NULL)
    delete magnet_cutTrd;
  if (yokeHole_box != NULL)
    delete yokeHole_box;
  if (yokeHoleTop_box != NULL)
    delete yokeHoleTop_box;
  if (magnet_bottomUnion != NULL)
    delete magnet_bottomUnion;
  for (unsigned int i = 0; i < 7; i++)
    if (magnet_bottomSubstraction[i] != NULL)
      delete magnet_bottomSubstraction[i];
  for (unsigned int i = 0; i < 10; i++)
    if (magnet_topSubstraction[i] != NULL)
      delete magnet_topSubstraction[i];
  if (magnetBottom_log != NULL)
    delete magnetBottom_log;
  if (magnetTop_log != NULL)
    delete magnetTop_log;
}

void T4SM2::construct(G4LogicalVolume* world_log)
{
  G4double solenoidBoxX[3];
  solenoidBoxX[0] = 1474.0 * CLHEP::mm;
  solenoidBoxX[1] = 320.0 * CLHEP::mm;
  solenoidBoxX[2] = 240.0 * CLHEP::mm;
  G4double solenoidBoxZ[3];
  solenoidBoxZ[0] = 320.0 * CLHEP::mm;
  solenoidBoxZ[1] = 240.0 * CLHEP::mm;
  solenoidBoxZ[2] = 1120.0 * CLHEP::mm;
  G4double solenoidTubBig[3];
  solenoidTubBig[0] = 60.0 * CLHEP::mm;
  solenoidTubBig[1] = 380.0 * CLHEP::mm;
  solenoidTubBig[2] = 240.0 * CLHEP::mm;
  G4double solenoidTubSmall[3];
  solenoidTubSmall[0] = 60.0 * CLHEP::mm;
  solenoidTubSmall[1] = 300.0 * CLHEP::mm;
  solenoidTubSmall[2] = 320.0 * CLHEP::mm;

  solenoid_boxX = new G4Box("solenoid geometry X box", solenoidBoxX[0] / 2,
      solenoidBoxX[1] / 2, solenoidBoxX[2] / 2);
  solenoid_boxZ = new G4Box("solenoid geometry Z box", solenoidBoxZ[0] / 2,
      solenoidBoxZ[1] / 2, solenoidBoxZ[2] / 2);
  solenoid_tubBig = new G4Tubs("solenoid tubs big", solenoidTubBig[0],
      solenoidTubBig[1], solenoidTubBig[2] / 2, 0.0 * CLHEP::deg,
      90.0 * CLHEP::deg);
  solenoid_tubSmall = new G4Tubs("solenoid tubs small", solenoidTubSmall[0],
      solenoidTubSmall[1], solenoidTubSmall[2] / 2, 0.0 * CLHEP::deg,
      90.0 * CLHEP::deg);

  // add big tubs
  rotationSolenoid[0] = new CLHEP::HepRotation;
  rotationSolenoid[0]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[0]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[0]->rotateZ(90.0 * CLHEP::deg);
  solenoid_unionSolid[0] = new G4UnionSolid("solenoid union solid",
      solenoid_boxX, solenoid_tubBig, rotationSolenoid[0],
      G4ThreeVector(solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2, 0));

  rotationSolenoid[1] = new CLHEP::HepRotation;
  rotationSolenoid[1]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[1]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[1]->rotateZ(180.0 * CLHEP::deg);
  solenoid_unionSolid[1] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[0], solenoid_tubBig, rotationSolenoid[1],
      G4ThreeVector(-solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2, 0));

  // add the small tubs
  rotationSolenoid[2] = new CLHEP::HepRotation;
  rotationSolenoid[2]->rotateX(90.0 * CLHEP::deg);
  rotationSolenoid[2]->rotateY(90.0 * CLHEP::deg);
  rotationSolenoid[2]->rotateZ(0.0 * CLHEP::deg);
  solenoid_unionSolid[2] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[1], solenoid_tubSmall, rotationSolenoid[2],
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0]));
  solenoid_unionSolid[3] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[2], solenoid_tubSmall, rotationSolenoid[2],
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0])); // take it...

  // add the z boxes
  solenoid_unionSolid[4] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[3], solenoid_boxZ, 0,
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0] + solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2,
          solenoidBoxX[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2] / 2));
  solenoid_unionSolid[5] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[4], solenoid_boxZ, 0,
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0] + solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2,
          solenoidBoxX[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2] / 2));

  // add small tubs
  rotationSolenoid[3] = new CLHEP::HepRotation;
  rotationSolenoid[3]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[3]->rotateY(90.0 * CLHEP::deg);
  rotationSolenoid[3]->rotateZ(0.0 * CLHEP::deg);
  solenoid_unionSolid[6] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[5], solenoid_tubSmall, rotationSolenoid[3],
      G4ThreeVector(
          solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2,
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2]));
  solenoid_unionSolid[7] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[6], solenoid_tubSmall, rotationSolenoid[3],
      G4ThreeVector(
          -(solenoidBoxX[0] / 2 + solenoidTubBig[0]
              + (solenoidTubBig[1] - solenoidTubBig[0]) / 2),
          solenoidBoxX[1] / 2 + solenoidTubBig[0],
          solenoidTubBig[2] / 2 + solenoidTubSmall[0] + solenoidBoxZ[2]));

  // add big tubs
  rotationSolenoid[4] = new CLHEP::HepRotation;
  rotationSolenoid[4]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[4]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[4]->rotateZ(90.0 * CLHEP::deg);
  solenoid_unionSolid[8] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[7], solenoid_tubBig, rotationSolenoid[4],
      G4ThreeVector(solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  rotationSolenoid[5] = new CLHEP::HepRotation;
  rotationSolenoid[5]->rotateX(0.0 * CLHEP::deg);
  rotationSolenoid[5]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[5]->rotateZ(180.0 * CLHEP::deg);
  solenoid_unionSolid[9] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[8], solenoid_tubBig, rotationSolenoid[5],
      G4ThreeVector(-solenoidBoxX[0] / 2,
          solenoidTubBig[1] / 2 + solenoidTubBig[0] / 2,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  solenoid_unionSolid[10] = new G4UnionSolid("solenoid union solid",
      solenoid_unionSolid[9], solenoid_boxX, 0,
      G4ThreeVector(0, 0,
          solenoidBoxZ[2] + 2. * solenoidTubSmall[0]
              + (solenoidTubSmall[1] - solenoidTubSmall[0])));

  G4UnionSolid* solenoidLow = solenoid_unionSolid[10];
  G4UnionSolid* solenoidHigh = solenoid_unionSolid[10];

  rotationSolenoid[6] = new CLHEP::HepRotation;
  rotationSolenoid[6]->rotateX(180.0 * CLHEP::deg);
  rotationSolenoid[6]->rotateY(0.0 * CLHEP::deg);
  rotationSolenoid[6]->rotateZ(0.0 * CLHEP::deg);

  solenoidHigh_log = new G4LogicalVolume(solenoidHigh, materials->iron,
      "solenoid high");
  solenoidLow_log = new G4LogicalVolume(solenoidLow, materials->iron,
      "solenoid low");

  G4double distanceUpSolenoidToDownSolenoid;
//  distanceUpSolenoidToDownSolenoid = 1730.0 * CLHEP::mm;
  distanceUpSolenoidToDownSolenoid = 1640.0 * CLHEP::mm;

  new G4PVPlacement(rotationSolenoid[6],
      positionVector
          + G4ThreeVector(0,
              solenoidTubBig[0] + solenoidTubSmall[1] + solenoidBoxX[1] / 2
                  + distanceUpSolenoidToDownSolenoid / 2,
              solenoidBoxZ[2] / 2 + solenoidTubSmall[0]
                  + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2),
      solenoidHigh_log, "solenoidHigh_phys", world_log, 0, 0, checkOverlap);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              -(solenoidTubBig[0] + solenoidTubSmall[1] + solenoidBoxX[1] / 2
                  + distanceUpSolenoidToDownSolenoid / 2),
              -(solenoidBoxZ[2] / 2 + solenoidTubSmall[0]
                  + (solenoidTubSmall[1] - solenoidTubSmall[0]) / 2)),
      solenoidLow_log, "solenoidLow_phys", world_log, 0, 0, checkOverlap);

  solenoidHigh_log->SetVisAttributes(colour->blue);
  solenoidLow_log->SetVisAttributes(colour->blue);

  // create inner structure
  G4double magnetSideWall[3];
  magnetSideWall[0] = 1935.0 * CLHEP::mm;
  magnetSideWall[1] = 3000.0 * CLHEP::mm;
  magnetSideWall[2] = 4000.0 * CLHEP::mm;
  G4double magnetInnerPlate[3];
  magnetInnerPlate[0] = 2290.0 * CLHEP::mm;
  magnetInnerPlate[1] = 300.0 * CLHEP::mm;
  magnetInnerPlate[2] = 1100.0 * CLHEP::mm;
  G4double magnetInnerBlock[3];
  magnetInnerBlock[0] = 1525.0 * CLHEP::mm;
  magnetInnerBlock[1] = 300.0 * CLHEP::mm;
  magnetInnerBlock[2] = 1100.0 * CLHEP::mm;
  G4double distanceInnerPlates;
  distanceInnerPlates = 2290.0 * CLHEP::mm;

  magnet_sideWall = new G4Box("", magnetSideWall[0] / 2, magnetSideWall[1] / 2,
      magnetSideWall[2] / 2);
  magnet_innerPlate = new G4Box("", magnetInnerPlate[0] / 2,
      magnetInnerPlate[1] / 2, magnetInnerPlate[2] / 2);
  magnet_innerBlock = new G4Box("", magnetInnerBlock[0] / 2,
      magnetInnerBlock[1] / 2, magnetInnerBlock[2] / 2);
  magnet_unionSolid[0] = new G4UnionSolid("", magnet_sideWall,
      magnet_innerPlate, 0,
      G4ThreeVector(+(magnetInnerPlate[0] / 2 + magnetSideWall[0] / 2),
          -(distanceInnerPlates / 2 + magnetInnerPlate[1] / 2), 0));
  magnet_unionSolid[1] = new G4UnionSolid("", magnet_unionSolid[0],
      magnet_innerPlate, 0,
      G4ThreeVector(+(magnetInnerPlate[0] / 2 + magnetSideWall[0] / 2),
          +(distanceInnerPlates / 2 + magnetInnerPlate[1] / 2), 0));
  magnet_unionSolid[2] = new G4UnionSolid("", magnet_unionSolid[1],
      magnet_sideWall, 0,
      G4ThreeVector(+(magnetInnerPlate[0] + magnetSideWall[0]), 0, 0));
  magnet_unionSolid[3] = new G4UnionSolid("", magnet_unionSolid[2],
      magnet_innerBlock, 0,
      G4ThreeVector(+(magnetInnerPlate[0] / 2 + magnetSideWall[0] / 2),
          -(distanceInnerPlates / 2 - magnetInnerBlock[1] / 2), 0));
  magnet_unionSolid[4] = new G4UnionSolid("", magnet_unionSolid[3],
      magnet_innerBlock, 0,
      G4ThreeVector(+(magnetInnerPlate[0] / 2 + magnetSideWall[0] / 2),
          +(distanceInnerPlates / 2 - magnetInnerBlock[1] / 2), 0));
  G4UnionSolid* magnet_mainGeometry = magnet_unionSolid[4];

  magnetMainVolume_log = new G4LogicalVolume(magnet_mainGeometry,
      materials->iron, "");
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-(magnetSideWall[0] / 2 + magnetInnerPlate[0] / 2), 0,
              0), magnetMainVolume_log, "magnetMainVolume_phys", world_log, 0,
      0, checkOverlap);
  magnetMainVolume_log->SetVisAttributes(colour->red);

  // create top & bottom structure
  G4double magnetBottomPlate[3];
  magnetBottomPlate[0] = 6160.0 * CLHEP::mm;
  magnetBottomPlate[1] = 580.0 * CLHEP::mm;
  magnetBottomPlate[2] = 4000.0 * CLHEP::mm;
  G4double magnetTopPlate[3];
  magnetTopPlate[0] = 6160.0 * CLHEP::mm;
  magnetTopPlate[1] = 730.0 * CLHEP::mm;
  magnetTopPlate[2] = 4000.0 * CLHEP::mm;
  G4double magnetFeet[4];
  magnetFeet[0] = 5000.0 * CLHEP::mm;
  magnetFeet[1] = 5500.0 * CLHEP::mm;
  magnetFeet[2] = 4000.0 * CLHEP::mm;
  magnetFeet[3] = 155.0 * CLHEP::mm;
  G4double yokeHoleBox[3];
  yokeHoleBox[0] = 1300.0 * CLHEP::mm;
  yokeHoleBox[1] = 435.0 * CLHEP::mm;
  yokeHoleBox[2] = 4001.0 * CLHEP::mm;

  magnet_bottomPlate = new G4Box("", magnetBottomPlate[0] / 2,
      magnetBottomPlate[1] / 2, magnetBottomPlate[2] / 2);
  magnet_topPlate = new G4Box("", magnetTopPlate[0] / 2, magnetTopPlate[1] / 2,
      magnetTopPlate[2] / 2);
  magnet_feet = new G4Trd("", magnetFeet[0] / 2, magnetFeet[1] / 2,
      magnetFeet[2] / 2, magnetFeet[2] / 2, magnetFeet[3] / 2);
  yokeHole_box = new G4Box("", yokeHoleBox[0] / 2, yokeHoleBox[1] / 2,
      yokeHoleBox[2] / 2);

  rotationMagnet = new CLHEP::HepRotation;
  rotationMagnet->rotateX(90.0 * CLHEP::deg);
  rotationMagnet->rotateY(0.0 * CLHEP::deg);
  rotationMagnet->rotateZ(0.0 * CLHEP::deg);
  magnet_bottomUnion = new G4UnionSolid("", magnet_bottomPlate, magnet_feet,
      rotationMagnet,
      G4ThreeVector(0, -(magnetFeet[3] / 2 + magnetBottomPlate[1] / 2), 0));

  G4double magnetCutBox[3];
  magnetCutBox[0] = 1850.0 * CLHEP::mm;
  magnetCutBox[1] = 221.0 * CLHEP::mm;
  magnetCutBox[2] = 1201.0 * CLHEP::mm;

  magnet_cutBox = new G4Box("", magnetCutBox[0] / 2, magnetCutBox[1] / 2,
      magnetCutBox[2] / 2);
  magnet_bottomSubstraction[0] = new G4SubtractionSolid("", magnet_bottomUnion,
      magnet_cutBox, 0,
      G4ThreeVector(0,
          magnetBottomPlate[1] / 2 - magnetCutBox[1] / 2 + 0.5 * CLHEP::mm,
          -(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2)));
  magnet_bottomSubstraction[1] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[0], magnet_cutBox, 0,
      G4ThreeVector(0,
          magnetBottomPlate[1] / 2 - magnetCutBox[1] / 2 + 0.5 * CLHEP::mm,
          +(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2)));
  magnet_topSubstraction[0] = new G4SubtractionSolid("", magnet_topPlate,
      magnet_cutBox, 0,
      G4ThreeVector(0,
          -magnetTopPlate[1] / 2 + magnetCutBox[1] / 2 - 0.5 * CLHEP::mm,
          -(magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_topSubstraction[1] = new G4SubtractionSolid("",
      magnet_topSubstraction[0], magnet_cutBox, 0,
      G4ThreeVector(0,
          -magnetTopPlate[1] / 2 + magnetCutBox[1] / 2 - 0.5 * CLHEP::mm,
          +(magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));

  magnet_cutCircle[0] = new G4Tubs("", 0, magnetCutBox[1], magnetCutBox[2] / 2,
      270 * CLHEP::deg, 90 * CLHEP::deg);
  magnet_cutCircle[1] = new G4Tubs("", 0, magnetCutBox[1], magnetCutBox[2] / 2,
      180 * CLHEP::deg, 90 * CLHEP::deg);
  magnet_bottomSubstraction[2] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[1], magnet_cutCircle[0], 0,
      G4ThreeVector(+(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          magnetBottomPlate[1] / 2 + 0.5 * CLHEP::mm,
          +(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_bottomSubstraction[3] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[2], magnet_cutCircle[0], 0,
      G4ThreeVector(+(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          magnetBottomPlate[1] / 2 + 0.5 * CLHEP::mm,
          -(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_bottomSubstraction[4] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[3], magnet_cutCircle[1], 0,
      G4ThreeVector(-(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          magnetBottomPlate[1] / 2 + 0.5 * CLHEP::mm,
          +(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_bottomSubstraction[5] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[4], magnet_cutCircle[1], 0,
      G4ThreeVector(-(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          magnetBottomPlate[1] / 2 + 0.5 * CLHEP::mm,
          -(magnetBottomPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_bottomSubstraction[6] = new G4SubtractionSolid("",
      magnet_bottomSubstraction[5], yokeHole_box, 0,
      G4ThreeVector(0, -yokeHoleBox[1] / 2 - magnetFeet[3] / 2, 0));

  magnetBottom_log = new G4LogicalVolume(magnet_bottomSubstraction[6],
      materials->iron, "");
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              -(magnetBottomPlate[1] / 2 + magnetSideWall[1] / 2), 0),
      magnetBottom_log, "magnetBottom_phys", world_log, 0, 0, checkOverlap);
  magnetBottom_log->SetVisAttributes(colour->red);

  magnet_cutCircle[2] = new G4Tubs("", 0, magnetCutBox[1], magnetCutBox[2] / 2,
      90 * CLHEP::deg, 90 * CLHEP::deg);
  magnet_cutCircle[3] = new G4Tubs("", 0, magnetCutBox[1], magnetCutBox[2] / 2,
      0 * CLHEP::deg, 90 * CLHEP::deg);
  magnet_topSubstraction[2] = new G4SubtractionSolid("",
      magnet_topSubstraction[1], magnet_cutCircle[2], 0,
      G4ThreeVector(-(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          -magnetTopPlate[1] / 2 + -0.5 * CLHEP::mm,
          -(magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_topSubstraction[3] = new G4SubtractionSolid("",
      magnet_topSubstraction[2], magnet_cutCircle[3], 0,
      G4ThreeVector(+(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          -magnetTopPlate[1] / 2 + -0.5 * CLHEP::mm,
          -(magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm)));
  magnet_topSubstraction[4] = new G4SubtractionSolid("",
      magnet_topSubstraction[3], magnet_cutCircle[2], 0,
      G4ThreeVector(-(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          -magnetTopPlate[1] / 2 + -0.5 * CLHEP::mm,
          magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm));
  magnet_topSubstraction[5] = new G4SubtractionSolid("",
      magnet_topSubstraction[4], magnet_cutCircle[3], 0,
      G4ThreeVector(+(magnetCutBox[0] / 2 - 0.5 * CLHEP::mm),
          -magnetTopPlate[1] / 2 + -0.5 * CLHEP::mm,
          magnetTopPlate[2] / 2 - magnetCutBox[2] / 2 + 0.5 * CLHEP::mm));

  G4double magnetCutBoxUp[3];
  magnetCutBoxUp[0] = 616.0 * CLHEP::mm;
  magnetCutBoxUp[1] = 616.0 * CLHEP::mm;
  magnetCutBoxUp[2] = 4001.0 * CLHEP::mm;

  magnet_cutBoxUp = new G4Box("", magnetCutBoxUp[0] / 2, magnetCutBoxUp[1] / 2,
      magnetCutBoxUp[2] / 2);

  rotationMagnetCut[0] = new CLHEP::HepRotation;
  rotationMagnetCut[0]->rotateX(0.0 * CLHEP::deg);
  rotationMagnetCut[0]->rotateY(0.0 * CLHEP::deg);
  rotationMagnetCut[0]->rotateZ(-45.0 * CLHEP::deg);

  rotationMagnetCut[1] = new CLHEP::HepRotation;
  rotationMagnetCut[1]->rotateX(0.0 * CLHEP::deg);
  rotationMagnetCut[1]->rotateY(0.0 * CLHEP::deg);
  rotationMagnetCut[1]->rotateZ(45.0 * CLHEP::deg);

  magnet_topSubstraction[6] = new G4SubtractionSolid("",
      magnet_topSubstraction[5], magnet_cutBoxUp, rotationMagnetCut[0],
      G4ThreeVector(magnetTopPlate[0] / 2, +magnetTopPlate[1] / 2, 0));
  magnet_topSubstraction[7] = new G4SubtractionSolid("",
      magnet_topSubstraction[6], magnet_cutBoxUp, rotationMagnetCut[1],
      G4ThreeVector(-magnetTopPlate[0] / 2, +magnetTopPlate[1] / 2, 0));
  magnet_topSubstraction[8] = new G4SubtractionSolid("",
      magnet_topSubstraction[7], yokeHole_box);

  magnet_topSubstraction[9] = new G4SubtractionSolid("",
      magnet_topSubstraction[8], yokeHole_box, 0,
      G4ThreeVector(0, yokeHoleBox[1] / 2, 0));

  magnetTop_log = new G4LogicalVolume(magnet_topSubstraction[9],
      materials->iron, "");
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, magnetTopPlate[1] / 2 + magnetSideWall[1] / 2, 0),
      magnetTop_log, "magnetTop_phys", world_log, 0, 0, checkOverlap);
  magnetTop_log->SetVisAttributes(colour->red);

  addToRegion();
}

void T4SM2::addToRegion(void)
{
  regionManager->addToAbsorberRegion(solenoidHigh_log);
  regionManager->addToAbsorberRegion(solenoidLow_log);
  regionManager->addToAbsorberRegion(magnetMainVolume_log);
  regionManager->addToAbsorberRegion(magnetBottom_log);
  regionManager->addToAbsorberRegion(magnetTop_log);
}
