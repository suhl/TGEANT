#ifndef T4DYABSORBER_HH_
#define T4DYABSORBER_HH_

#include "T4BaseDetector.hh"
#include "T4TargetBackend.hh"

#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "G4Cons.hh"

class T4DYAbsorber : public T4BaseDetector
{
  public:
    T4DYAbsorber(T4SDetector*);
    virtual ~T4DYAbsorber();

    void construct(G4LogicalVolume*);

  private:
    double yearSetup;

    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4SubtractionSolid*> subs;
    vector<G4LogicalVolume*> log;
    G4Cons * nose_cons;
    G4Cons * nose_end; //Add nose endcap

    G4double zLayer;
    G4double zLayerAlumina[3];
    G4double blockDimension[3];
    G4double xLayerAlumina[10];
    G4double yLayerAlumina[10];
    G4double innerRadiusHole[9];
    G4double xConcrete[5]; //1->5 from bottom to top
    G4double yConcrete[5];
    G4double zConcrete[5];
    G4double zShiftConc;

    G4double steelThickness;
    G4double aluminiumTargetLength;

    //2015 addictions
    G4double SheetDimensions[3];
    G4double PolyeDimensions[3];

    G4double noseRadiusUp;
    G4double noseRadiusDown;
    G4double noseZlenght;
    G4double noseSpace;
    G4double noseEndLenght; //Add nose endcap
};

#endif /* T4DYABSORBER_HH_ */
