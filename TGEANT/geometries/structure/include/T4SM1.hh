#ifndef T4SM1_HH_
#define T4SM1_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4Trd.hh"

class T4SM1 : public T4BaseDetector
{
  public:
    T4SM1(T4SDetector*);
    virtual ~T4SM1(void);

    void construct(G4LogicalVolume*);

  private:
    void addToRegion(void);

    CLHEP::HepRotation* rotationSolenoid[7];
    G4Box* solenoid_boxX;
    G4Box* solenoid_boxZ;
    G4Tubs* solenoid_tubBig;
    G4Tubs* solenoid_tubSmall;
    G4UnionSolid* solenoid_unionSolid[11];
    G4LogicalVolume* solenoidHigh_log;
    G4LogicalVolume* solenoidLow_log;

    G4Box* bigBoxes;
    G4LogicalVolume* bigBoxes_log[2];

    G4Box* top;
    G4Box* cutTop;
    G4Tubs* cutTub1;
    G4Tubs* cutTub2;
    G4Trd* cutTrd;
    G4Box* sideCut;
    G4Box* magnetFeet;
    G4Box* bigInnerBox;
    G4Box* smallInnerBox;

    G4LogicalVolume* top_log;
    G4LogicalVolume* bottom_log;
    G4LogicalVolume* magnetFeet_log;
    G4LogicalVolume* bigInnerBox_log;
    G4LogicalVolume* smallInnerBox_log;

    G4RotationMatrix* rotation90X;
    G4RotationMatrix* rotation45Z;
    G4RotationMatrix* rotation180X;
    G4SubtractionSolid* subtraction[6];
};

#endif /* T4SM1_HH_ */
