#ifndef T4SM2_HH_
#define T4SM2_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4Trd.hh"

class T4SM2 : public T4BaseDetector
{
  public:
    T4SM2(T4SDetector*);
    virtual ~T4SM2(void);

    void construct(G4LogicalVolume*);

  private:
    void addToRegion(void);

    CLHEP::HepRotation* rotationSolenoid[7];
    G4Box* solenoid_boxX;
    G4Box* solenoid_boxZ;
    G4Tubs* solenoid_tubBig;
    G4Tubs* solenoid_tubSmall;
    G4UnionSolid* solenoid_unionSolid[11];
    G4LogicalVolume* solenoidHigh_log;
    G4LogicalVolume* solenoidLow_log;

    G4Box* magnet_sideWall;
    G4Box* magnet_innerPlate;
    G4Box* magnet_innerBlock;
    G4UnionSolid* magnet_unionSolid[5];
    G4LogicalVolume* magnetMainVolume_log;

    CLHEP::HepRotation* rotationMagnet;
    CLHEP::HepRotation* rotationMagnetCut[2];
    G4Box* magnet_bottomPlate;
    G4Box* magnet_topPlate;
    G4Trd* magnet_feet;
    G4Box* magnet_cutBox;
    G4Tubs* magnet_cutCircle[4];
    G4Box* magnet_cutBoxUp;
    G4Trd* magnet_cutTrd;
    G4Box* yokeHole_box;
    G4Box* yokeHoleTop_box;
    G4UnionSolid* magnet_bottomUnion;
    G4SubtractionSolid* magnet_bottomSubstraction[7];
    G4SubtractionSolid* magnet_topSubstraction[10];
    G4LogicalVolume* magnetBottom_log;
    G4LogicalVolume* magnetTop_log;
};

#endif /* T4SM2_HH_ */
