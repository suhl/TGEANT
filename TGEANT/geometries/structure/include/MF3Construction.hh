#ifndef MF3CONSTRUCTION_HH_
#define MF3CONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"

class MF3Construction : public T4BaseDetector
{
  public:
    MF3Construction(T4SDetector*);
    virtual ~MF3Construction(void);

    void construct(G4LogicalVolume*);

  private:
    G4Box* absorber_box;
    G4Box* hole_box;
    G4SubtractionSolid* absorber_sub;
    G4LogicalVolume* absorber_log;
};

class MF3Block : public T4BaseDetector
{
  public:
    MF3Block(T4SDetector*);
    virtual ~MF3Block(void);

    void construct(G4LogicalVolume*);

  private:
    G4Box* block_box;
    G4LogicalVolume* block_log;
};

#endif /* MF3CONSTRUCTION_HH_ */
