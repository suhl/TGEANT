#ifndef RICHCONSTRUCTION_HH_
#define RICHCONSTRUCTION_HH_

#include "T4BaseDetector.hh"
#include "T4SensitiveRich.hh"

#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"

// RICH position is defined as the center of Up and Down readout box.
// This is the average of both detectors.dat lines.
class RichConstruction : public T4BaseDetector
{
  public:
    RichConstruction(void);
    virtual ~RichConstruction(void);

    void construct(G4LogicalVolume*);

    G4LogicalVolume*& getRichLogical(void) {return inner_log;}
    G4ThreeVector getVesselMidPosition(void) {return vesselMidPosition;}

  private:
    void constructBeamPipe(G4LogicalVolume*);
    void constructVessel(G4LogicalVolume*);
    void constructMirror(void);
    void constructReadout(G4LogicalVolume*);
    void constructSurface(void);
    T4RichPCB setPCBStruct(G4ThreeVector position, G4String name, int id);

    T4SRICH* rich;
    G4Material* gasMat;
    G4Material* aluminiumMat;
    G4Material* vacuumMat;
    G4Material* glassMat;
    G4Material* bialkaliMat;

    G4double thicknessSide;
    G4double thicknessFrontBack;
    G4double beamPipeLength;
    G4double innerRadius;
    G4double vesselBack[3];
    G4double vesselMid[5];
    G4double vesselFront[5];
    G4ThreeVector vesselMidPosition;
    G4double alpha;
    CLHEP::HepRotation* pbcRotation[2];
    G4double offsetY;
    G4double offsetZ;

    G4Box* vesselBack_box;
    G4Box* innerBack_box;
    G4Trd* vesselMid_trd;
    G4Trd* innerMid_trd;
    G4Trd* vesselFront_trd;
    G4Trd* innerFront_trd;
    G4UnionSolid* vesselBack_union;
    G4UnionSolid* innerBack_union;
    G4UnionSolid* vessel_union;
    G4UnionSolid* inner_union;
    vector<G4SubtractionSolid*> vessel_sub;
    G4IntersectionSolid* inner_inter;
    G4LogicalVolume* vessel_log;
    G4LogicalVolume* inner_log;
    G4VPhysicalVolume* inner_phys;

    CLHEP::HepRotation* mirrorRotation;
    G4Sphere* mirror_sphere;
    G4Box* mirror_box;
    G4IntersectionSolid* mirrorTop_inter[2];
    G4IntersectionSolid* mirrorBottom_inter[2];
    G4LogicalVolume* mirrorTop_log;
    G4LogicalVolume* mirrorBottom_log;
    G4VPhysicalVolume* mirrorTop_phys;
    G4VPhysicalVolume* mirrorBottom_phys;

    G4Tubs* beamPipeAl_tubs;
    G4Tubs* beamPipePET_tubs;
    G4Tubs* beamPipeMylar_tubs;
    G4Tubs* beamPipeHe_tubs;
    G4LogicalVolume* beamPipeAl_log;
    G4LogicalVolume* beamPipePET_log;
    G4LogicalVolume* beamPipeMylar_log;
    G4LogicalVolume* beamPipeHe_log;

    vector<T4RichPCB> pcbInformation;

    G4Box* housingPCB_box;
    G4Box* vacuumPCB_box;
    G4Box* windowPCB_box;
    G4Box* cathodePCB_box;
    vector<G4LogicalVolume*> housingPCB_log;
    vector<G4LogicalVolume*> vacuumPCB_log;
    vector<G4LogicalVolume*> windowPCB_log;
    vector<G4LogicalVolume*> cathodePCB_log;

    vector<G4VPhysicalVolume*> housingPCB_phys;
    vector<G4VPhysicalVolume*> vacuumPCB_phys;
    vector<G4VPhysicalVolume*> windowPCB_phys;
    vector<G4VPhysicalVolume*> cathodePCB_phys;

    vector<G4LogicalBorderSurface*> surface;
};

#endif /* RICHCONSTRUCTION_HH_ */
