#ifndef T4COLOUR_HH_
#define T4COLOUR_HH_

#include "G4VisAttributes.hh"

/*! \class T4Colour
 *  \brief Singleton class to control all G4VisAttributes.
 *
 *  This class is used in each T4BaseDetector class to give all
 *  logical volumes a colour for the visualization mode.
 */
class T4Colour
{
  public:
    /*! \brief Get the instance of this class.*/
    static T4Colour* getInstance(void);
    /*! \brief Default destructor.*/
    ~T4Colour(void);

    /*! \brief Some colours for logical volumes.*/
    G4VisAttributes* red;
    G4VisAttributes* lightred;
    G4VisAttributes* darkred;
    G4VisAttributes* orange;
    G4VisAttributes* green;
    G4VisAttributes* darkgreen;
    G4VisAttributes* lightgreen;
    G4VisAttributes* blue;
    G4VisAttributes* blue_50;
    G4VisAttributes* darkblue;
    G4VisAttributes* lightblue;
    G4VisAttributes* mediumblue;
    G4VisAttributes* lightskyblue;
    G4VisAttributes* deepskyblue;
    G4VisAttributes* dodgerblue;
    G4VisAttributes* yellow;
    G4VisAttributes* yellow_50;
    G4VisAttributes* darkyellow;
    G4VisAttributes* deepyellow;
    G4VisAttributes* lightyellow;
    G4VisAttributes* magenta;
    G4VisAttributes* magenta_20;
    G4VisAttributes* cyan;
    G4VisAttributes* darkcyan;
    G4VisAttributes* deepcyan;
    G4VisAttributes* gray;
    G4VisAttributes* gray_50;
    G4VisAttributes* lightgray;
    G4VisAttributes* darkgray;
    G4VisAttributes* darkgray_50;
    G4VisAttributes* deepgray;
    G4VisAttributes* floorcolor;
    G4VisAttributes* white;
    G4VisAttributes* black;
    G4VisAttributes* black_20;
    G4VisAttributes* invisible;
    G4VisAttributes* silver;
    G4VisAttributes* silver_20;
    G4VisAttributes* violet;
    G4VisAttributes* mediumpurple;

    /*! \brief Reset and destruct the instance.*/
    static void resetInstance(void) {delete colour; colour = NULL;}

  private:
    /*! \brief Private constructor.*/
    T4Colour(void);
    /*! \brief Private class pointer.*/
    static T4Colour* colour;
};

#endif /* T4COLOUR_HH_ */
