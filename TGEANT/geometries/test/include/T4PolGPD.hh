#ifndef T4POLGPD_HH_
#define T4POLGPD_HH_

#include "T4BaseDetector.hh"
#include "T4TargetBackend.hh"

#include "RingBElement.hh"
#include "T4ODDipoleField.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

class T4PolGPD : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4PolGPD(void);
    virtual ~T4PolGPD(void);

    void construct(G4LogicalVolume*);

    void getTargetDetDat(std::vector<T4STargetInformation>&) {};

  private:
    T4SPolGPD* polGPD;

    void buildTarget(G4LogicalVolume*);
    void buildMicromegas(G4LogicalVolume*);
    void buildRingB(G4LogicalVolume*);

    //TEST ONLY:
    void buildPhotonWall(G4LogicalVolume*);

    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;

    vector<G4ThreeVector> positionSciB;
    vector<CLHEP::HepRotation*> rotationSciB;
    vector<RingBElement*> ringBElement;
};

#endif /* T4POLGPD_HH_ */
