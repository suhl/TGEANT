#ifndef T4POLGPD_SIRPD_HH_
#define T4POLGPD_SIRPD_HH_

#include "T4BaseDetector.hh"
#include "T4TargetBackend.hh"

#include "T4PolarizedTarget.hh"
#include "T4ODDipoleField.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

class T4PolGPD_SiRPD : public T4BaseDetector
{
  public:
    T4PolGPD_SiRPD(T4SDetector*);
    virtual ~T4PolGPD_SiRPD(void);

    void construct(G4LogicalVolume*);

  private:
    T4PolarizedTarget* polTarget;
    
    void buildSiliconRing(double r, double dr, int channelNo, G4LogicalVolume*);
  
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
};

#endif /* T4POLGPD_SIRPD_HH_ */
