#ifndef T4TESTCAL_HH_
#define T4TESTCAL_HH_

#include "T4CalorimeterBackend.hh"
#include "T4CaloManager.hh"

class T4TestCal : public T4CalorimeterBackend
{
  public:
    T4TestCal(T4SDetector*);
    virtual ~T4TestCal(void);

    void constructRegions(T4SCAL* _scalIn);
    void constructMechanicalStructure(void);
    void construct(G4LogicalVolume*);

  private:
    G4double moduleDistance;
    T4CaloManager* caloManager;
};

#endif /* T4TESTCAL_HH_ */
