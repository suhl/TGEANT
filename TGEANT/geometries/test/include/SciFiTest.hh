#ifndef SCIFITEST_HH_
#define SCIFITEST_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4Trd.hh"

class SciFiTest : public T4BaseDetector
{
  public:
    SciFiTest(void);
    virtual ~SciFiTest(void);

    void construct(G4LogicalVolume*);

  private:
    void buildGeometries(double yPos, G4LogicalVolume*, bool shortAir = false);
    void buildPMT(double gap, int nChannel, string name);
    void buildPMT_LG(double gap, double lgLength, int nChannel, string name);

    double lgSpace;
    double sizeX;
    double sizeY;
    double sizeZ;
    double airGap;
    double sizePMT;
    
    G4Box* alu_box;
    G4Box* air_box[2];
    G4Box* scinti_box;
    G4Box* pmt_box;
    G4Box* pmt_hull_box;

    vector <G4LogicalVolume*> alu_log;
    vector <G4LogicalVolume*> air_log;
    vector <G4LogicalVolume*> scinti_log;
    vector <G4LogicalVolume*> pmt_log;
    vector <G4LogicalVolume*> pmt_hull_log;

    vector <G4PVPlacement*> alu_phys;
    vector <G4PVPlacement*> air_phys;
    vector <G4PVPlacement*> scinti_phys;
    vector <G4PVPlacement*> pmt_phys;
    vector <G4PVPlacement*> pmt_hull_phys;
    
    vector <G4LogicalVolume*> air_trd_log;
    vector <G4LogicalVolume*> plexi_trd_log;
    vector <G4PVPlacement*> air_trd_phys;
    vector <G4PVPlacement*> plexi_trd_phys;
};

#endif /* SCIFITEST_HH_ */
