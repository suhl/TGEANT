#ifndef T4WORKSHOPEXAMPLE_HH_
#define T4WORKSHOPEXAMPLE_HH_

#include "T4BaseDetector.hh"

#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "T4TargetBackend.hh"

class T4WorkshopExample : public T4BaseDetector, public T4TargetBackend
{
  public:
    T4WorkshopExample(T4SDetector*);
    virtual ~T4WorkshopExample(void);

    void construct(G4LogicalVolume*);
    
    void getTargetDetDat(std::vector<T4STargetInformation>&);
    
    double getRndmTargetDist(double maxTargetLength);

  private:
    double targetLength;
    double targetRadius;

    G4Tubs* target_tubs;
    G4LogicalVolume* target_log;
    
    G4RotationMatrix* targetCellRotation;
};

#endif /* T4WORKSHOPEXAMPLE_HH_ */
