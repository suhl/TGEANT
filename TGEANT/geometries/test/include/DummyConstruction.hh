#ifndef DUMMYCONSTRUCTION_HH_
#define DUMMYCONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"

class DummyConstruction : public T4BaseDetector
{
  public:
    DummyConstruction(void) {}
    virtual ~DummyConstruction(void);

    void construct(G4LogicalVolume*);

  private:
    vector<G4Box*> dummy_box;
    vector<G4LogicalVolume*> dummy_log;
};

#endif /* DUMMYCONSTRUCTION_HH_ */
