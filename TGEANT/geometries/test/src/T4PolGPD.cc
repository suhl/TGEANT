#include "T4PolGPD.hh"

T4PolGPD::T4PolGPD(void)
{
  polGPD = settingsFile->getStructManager()->getPolGPD();
  T4ODDipoleField* field = new T4ODDipoleField(polGPD->general.position);
  field->setFieldStrength(-0.5 * CLHEP::tesla);
  T4DeathMagnetic::getInstance()->registerPartialField(field);

  //additional parameters for target
  setDimension(-100 * CLHEP::cm, 100 * CLHEP::cm);
  setELossParams(0., 0.);
}

void T4PolGPD::construct(G4LogicalVolume* world_log)
{
  buildTarget(world_log);
  buildMicromegas(world_log);
//  buildRingB(world_log);

  buildPhotonWall(world_log);
}

void T4PolGPD::buildPhotonWall(G4LogicalVolume* world_log)
{
  box.push_back(new G4Box("polGPD_photonWall", 3.*CLHEP::m, 3.*CLHEP::m, 0.01*CLHEP::m));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "polGPD_photonWall_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0,0,3.01*CLHEP::m), log.back(), "polGPD_photonWall", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->gray);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector("PHOTONWALL", 0, TGEANT::PMT, 666));
}

void T4PolGPD::buildTarget(G4LogicalVolume* world_log)
{
  // Target cryostat
  unsigned int cryostatIndex = log.size();
  tubs.push_back(new G4Tubs("polGPD_target_cryostat_tubs", 0, polGPD->target_cryostat_radius, polGPD->target_cryostat_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->carbonLH2, "polGPD_target_cryostat_log"));
  new G4PVPlacement(0, positionVector, log.back(), "polGPD_target_cryostat", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->blue_50);

  // Target cryostat filled with Vacuum
  unsigned int vacuumIndex = log.size();
  tubs.push_back(new G4Tubs("polGPD_vacuum_tubs", 0, polGPD->target_cryostat_radius - polGPD->target_cryostat_thickness, (polGPD->target_cryostat_length - polGPD->target_cryostat_thickness) / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->vacuum_noOptical, "polGPD_vacuum_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_vacuum", log.at(cryostatIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

//  // Mylar window (downstream) in Vacuum
  // falls das wieder eingebaut werden soll, die z-laenge vom vacuum auf volle laenge aendern
//  tubs.push_back(new G4Tubs("polGPD_mylarWindow_tubs", 0, target_cryostat_radius - target_cryostat_thickness, target_mylar_window_thickness / 2., 0, 2.*M_PI));
//  log.push_back(new G4LogicalVolume(tubs.back(), materials->mylar, "polGPD_mylarWindow_log"));
//  new G4PVPlacement(0, G4ThreeVector(0, 0, (target_cryostat_length - target_mylar_window_thickness) / 2.), log.back(), "polGPD_mylarWindow", log.at(vacuumIndex), 0, 0, checkOverlap);
//  log.back()->SetVisAttributes(colour->green);

  // Aluminium radiation shield inside the vacuum volume
  tubs.push_back(new G4Tubs("polGPD_aluminium_tubs", polGPD->target_aluminium_radius, polGPD->target_aluminium_radius + polGPD->target_aluminium_thickness, polGPD->target_aluminium_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->aluminium_noOptical, "polGPD_aluminium_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_aluminium", log.at(vacuumIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver);

  // Cu+NbTi (superconducting coils) inside the vacuum volume
  if (polGPD->target_coils_thickness > 0.) {
//    tubs.push_back(new G4Tubs("polGPD_superconducting_coils_tubs", polGPD->target_coils_radius, polGPD->target_coils_radius + polGPD->target_coils_thickness, polGPD->target_coils_length / 2., 0, 2.*M_PI));
//    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu/*materials->superconducter*/, "polGPD_superconducting_coils_log"));
//    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_superconducting_coils", log.at(vacuumIndex), 0, 0, checkOverlap);
//    log.back()->SetVisAttributes(colour->red);

    // geometry a la Dutz
    tubs.push_back(new G4Tubs("polGPD_superconducting_coils_Cu_tubs", polGPD->target_coils_radius, polGPD->target_coils_radius + 2.0 * CLHEP::mm, polGPD->target_coils_length / 2., 0, 2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "polGPD_superconducting_coils_Cu_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_superconducting_coils_Cu", log.at(vacuumIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->red);

    tubs.push_back(new G4Tubs("polGPD_superconducting_coils_NbTi_tubs", polGPD->target_coils_radius + 2.0 * CLHEP::mm, polGPD->target_coils_radius + 3.72 * CLHEP::mm, polGPD->target_coils_length / 2., 0, 2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->NbTi, "polGPD_superconducting_coils_NbTi_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_superconducting_coils_NbTi", log.at(vacuumIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->green);
  }

  // Fiber glass hull inside the vacuum volume
  unsigned int fiberGlassIndex = log.size();
  tubs.push_back(new G4Tubs("polGPD_fiber_glass_tubs", 0., polGPD->target_lhe_radius + polGPD->target_fiber_thickness, polGPD->target_length / 2. + polGPD->target_fiber_thickness, 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->g10, "polGPD_fiber_glass_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_fiber_glass", log.at(vacuumIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->red);

  // Fiber glass filled with LHe
  unsigned int lheIndex = log.size();
  tubs.push_back(new G4Tubs("polGPD_lHe_tubs", 0, polGPD->target_lhe_radius, polGPD->target_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->lhe, "polGPD_lHe_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_lHe", log.at(fiberGlassIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->blue);

  // Kevlar hull inside LHe
  unsigned int kevlarIndex = log.size();
  tubs.push_back(new G4Tubs("polGPD_kevlar_tubs", 0, polGPD->target_nh3_radius + polGPD->target_kevlar_thickness, polGPD->target_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->kevlar, "polGPD_kevlar_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "polGPD_kevlar", log.at(lheIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->blue);

  // NH3 target volume inside the Kevlar hull
  tubs.push_back(new G4Tubs("polGPD_nh3_tubs", 0, polGPD->target_nh3_radius, polGPD->target_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->nh3_he, "polGPD_nh3_log"));
  addToTarget(new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "TARGET", log.at(kevlarIndex), 0, 0, checkOverlap));
  log.back()->SetVisAttributes(colour->red);
  regionManager->addToTargetRegion(log.back());
}

void T4PolGPD::buildMicromegas(G4LogicalVolume* world_log)
{
  // just a simple He volume for the start...

  std::string mm_tbName_inner = "MM01R1__";
  std::string mm_tbName_outer = "MM01R2__";
  G4int mm_detectorId_inner = 929;
  G4int mm_detectorId_outer = 930;


  // MM position offset
  G4ThreeVector mm_position = positionVector;// + G4ThreeVector(0, 0,-target_nh3_length / 2. + mm_length / 2.);

  // Inner MM layer
  G4double innerRadius = polGPD->mm_radius - (polGPD->mm_layer_distance + polGPD->mm_layer_thickness) / 2.;
  tubs.push_back(new G4Tubs("polGPD_mm_inner_tubs", innerRadius - polGPD->mm_layer_thickness / 2., innerRadius + polGPD->mm_layer_thickness / 2., polGPD->mm_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->helium, "polGPD_mm_inner_log"));
  new G4PVPlacement(0, mm_position, log.back(), "polGPD_mm_inner", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->green);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(mm_tbName_inner, 0, TGEANT::HIT, mm_detectorId_inner));

  // Outer MM layer
  G4double outerRadius = polGPD->mm_radius + (polGPD->mm_layer_distance + polGPD->mm_layer_thickness) / 2.;
  tubs.push_back(new G4Tubs("polGPD_mm_outer_tubs", outerRadius - polGPD->mm_layer_thickness / 2., outerRadius + polGPD->mm_layer_thickness / 2., polGPD->mm_length / 2., 0, 2.*M_PI));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->helium, "polGPD_mm_outer_log"));
  new G4PVPlacement(0, mm_position, log.back(), "polGPD_mm_outer", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->green);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(mm_tbName_outer, 0, TGEANT::HIT, mm_detectorId_outer));
}

void T4PolGPD::buildRingB(G4LogicalVolume* world_log)
{
  G4bool ringB_useDetector = true;
  G4bool ringB_useOptical = false;
  std::string ringB_tbName = "CA02R2__";
  G4int ringB_detectorId = 928;

  for (G4int i = 0; i < 24; i++) {
    positionSciB.push_back(G4ThreeVector(sin(M_PI / 12. * i) * polGPD->ringB_radius,
        cos(M_PI / 12. * i) * polGPD->ringB_radius, polGPD->ringB_zPosOffset) + positionVector);

    rotationSciB.push_back(new CLHEP::HepRotation);
    rotationSciB.back()->rotateZ(15.0 * i * CLHEP::deg);

    if (ringB_useDetector) {
      ringBElement.push_back(new RingBElement());
      ringBElement.back()->setPosition(positionSciB.back());
      ringBElement.back()->setRotation(*rotationSciB.back());

      ringBElement.back()->setLengthZ(polGPD->ringB_length);

      ringBElement.back()->setDetectorName(ringB_tbName);
      ringBElement.back()->setChannelNo(i);
      ringBElement.back()->setDetectorId(ringB_detectorId);
      if (ringB_useOptical)
        ringBElement.back()->useOpticalPhysics();
      ringBElement.back()->construct(world_log);
    }
  }
}

T4PolGPD::~T4PolGPD(void)
{
  for (unsigned int i = 0; i < box.size(); i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size(); i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size(); i++)
    delete log.at(i);
  log.clear();

  for (unsigned int i = 0; i < rotationSciB.size(); i++)
    delete rotationSciB.at(i);
  rotationSciB.clear();

  for (unsigned int i = 0; i < ringBElement.size(); i++)
    delete ringBElement.at(i);
  ringBElement.clear();
}
