#include "T4PolGPD_SiRPD.hh"

T4PolGPD_SiRPD::T4PolGPD_SiRPD(T4SDetector* det)
{
  setPosition(det->position);
  useMechanicalStructure = det->useMechanicalStructure;
//   T4ODDipoleField* field = new T4ODDipoleField(det->position);
//   T4DeathMagnetic::getInstance()->registerPartialField(field);

  polTarget = new T4PolarizedTarget(det);
  polTarget->setCavityRadius(100. * CLHEP::mm);
  polTarget->setCavityThickness(0.6 * CLHEP::mm);
  polTarget->setTargetRadius(2.0 * CLHEP::cm);
}

void T4PolGPD_SiRPD::construct(G4LogicalVolume* world_log)
{
  polTarget->construct(world_log);

  buildSiliconRing(15. * CLHEP::cm, 0.3 * CLHEP::mm, 0, polTarget->getVacuumLog());
  buildSiliconRing(25. * CLHEP::cm, 1.0 * CLHEP::mm, 1, polTarget->getVacuumLog());
}

void T4PolGPD_SiRPD::buildSiliconRing(double r, double dr, int channelNo, G4LogicalVolume* world_log)
{
  tubs.push_back(new G4Tubs("Si_box", r, r + dr, 77. * CLHEP::cm, 0, 2.*M_PI));//154cm
  log.push_back(new G4LogicalVolume(tubs.back(), materials->Si, "Si_log"));
  new G4PVPlacement(0, positionVector, log.back(), "Si_RPD", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->yellow);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector("Si_RPD", channelNo, TGEANT::HIT, 2222));
}


T4PolGPD_SiRPD::~T4PolGPD_SiRPD(void)
{
  for (unsigned int i = 0; i < box.size(); i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size(); i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size(); i++)
    delete log.at(i);
  log.clear();
}
