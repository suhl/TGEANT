#include "DummyConstruction.hh"

DummyConstruction::~DummyConstruction(void)
{
  for (unsigned int i = 0; i < dummy_box.size(); i++)
    delete dummy_box.at(i);
  dummy_box.clear();

  for (unsigned int i = 0; i < dummy_log.size(); i++)
    delete dummy_log.at(i);
  dummy_log.clear();
}

void DummyConstruction::construct(G4LogicalVolume* world_log)
{
  for (unsigned int i = 0; i < settingsFile->getStructManager()->getDummies()->size(); i++) {
    const T4SDummy* dummy = &settingsFile->getStructManager()->getDummies() ->at(i);

    setPosition(dummy->general.position);

    dummy_box.push_back(new G4Box("dummy_box", dummy->planeSize[0], dummy->planeSize[1], dummy->planeSize[2]));
    dummy_log.push_back(new G4LogicalVolume(dummy_box.back(), materials->air_noOptical, "dummy_log"));
    dummy_log.back()->SetVisAttributes(colour->cyan);
    new G4PVPlacement(0, positionVector, dummy_log.back(), "dummy_phys", world_log, 0, 0, checkOverlap);

    dummy_log.back()->SetSensitiveDetector(new T4SensitiveDetector("DUMMY", i, TGEANT::HIT, 6666));
  }
}
