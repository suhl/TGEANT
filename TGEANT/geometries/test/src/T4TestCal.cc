#include "T4TestCal.hh"

T4TestCal::T4TestCal(T4SDetector* det)
{
  caloName = "ECAL1";

  setPosition(det->position);

  string module;
  module = "GAMS";
//  if (det->position[2] == 1)
//    module = "GAMS";
//  else if (det->position[2] == 2)
//    module = "RHGAMS";
//  else if (det->position[2] == 3)
//    module = "MAINZ";
//  else if (det->position[2] == 4)
//    module = "OLGA";
//  else if (det->position[2] == 5)
//    module = "SHASHLIK";
//  else if (det->position[2] == 6)
//    module = "ECAL0";
//  else {
//    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
//    __FILE__, "T4TestCal wrong position - tmp feature");
//  }
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__,
  __FILE__, "T4TestCal module: " + module );


  useMechanicalStructure = det->useMechanicalStructure;

  T4SCAL myCalo;
  myCalo.detectorId = 7777;
  myCalo.detName = caloName;
  myCalo.moduleName = module;
  myCalo.nRow = 11;
  myCalo.nCol = 11;

  if (myCalo.moduleName == "MAINZ")
    moduleDistance = 7.500 * CLHEP::cm;
  else if (myCalo.moduleName == "OLGA")
    moduleDistance = 14.300 * CLHEP::cm;
  else if (myCalo.moduleName == "ECAL0")
    moduleDistance = 4.0 * CLHEP::cm;
  else
    moduleDistance = 3.830 * CLHEP::cm;

  myCalo.position[0] = -((double) myCalo.nRow - 1)/2. * moduleDistance;
  myCalo.position[1] = -((double) myCalo.nCol - 1)/2. * moduleDistance;
  myCalo.position[2] = 0;

  cmtxList.push_back(myCalo);

  caloManager = NULL;
  if (settingsFile->getStructManager()->getCalorimeter()->size() == 0)
    caloManager = new T4CaloManager();
}

void T4TestCal::constructRegions(T4SCAL* _scalIn)
{
  G4double zSize = 45.0 / 2 * CLHEP::cm;

  TGEANT::T4CaloModule type;
  if (_scalIn->moduleName == "GAMS") {
    type = TGEANT::GAMS;
  } else if (_scalIn->moduleName == "RHGAMS") {
    type = TGEANT::GAMSRH;
  } else if (_scalIn->moduleName == "SHASHLIK") {
    type = TGEANT::SHASHLIK;
  } else if (_scalIn->moduleName == "MAINZ") {
    type = TGEANT::MAINZ;
    zSize = 36.0 / 2. * CLHEP::cm;
  } else if (_scalIn->moduleName == "OLGA") {
    type = TGEANT::OLGA;
    zSize = 47.0 / 2. * CLHEP::cm;
  } else if (_scalIn->moduleName == "ECAL0") {
    type = TGEANT::SHASHLIK_ECAL0;
    zSize = 34.17 / 2 * CLHEP::cm;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__, __FILE__,
    "T4TestCal::constructRegions: Unknown module name: '" + _scalIn->moduleName + "'. Setting it to GAMS.");
    type = TGEANT::GAMS;
  }

  buildRegionBox(type, _scalIn, caloName, zSize, moduleDistance, moduleDistance, 0.);
  modulePositioning(type, _scalIn, moduleDistance, moduleDistance, 0.);
}

void T4TestCal::construct(G4LogicalVolume* _world_log)
{
  T4CalorimeterBackend::construct(_world_log);
  
//   G4Box* dummy_box = new G4Box("dummy_box", 10.0 / 2 * CLHEP::m, 10.0 / 2 * CLHEP::m, 10.0 / 2 * CLHEP::m);
//   G4LogicalVolume* dummy_log = new G4LogicalVolume(dummy_box, materials->lead, "dummy_log");
//   dummy_log->SetVisAttributes(colour->red);
//   new G4PVPlacement(0, positionVector + G4ThreeVector(0,0,45.0 / 2. * CLHEP::cm+10.0 / 2 * CLHEP::m), 
// 		    dummy_log, "dummy_phys", _world_log, 0, 0, checkOverlap);
//     T4SensitiveCalorimeter* sdW = new T4SensitiveCalorimeter("BACK", 0, G4ThreeVector(0, 0, 0), 1.);
//   dummy_log->SetSensitiveDetector(sdW);

  if (caloManager != NULL && settingsFile->getStructManager()->getGeneral()->useGflash)
    caloManager->addGflash();
}

void T4TestCal::constructMechanicalStructure()
{

}

T4TestCal::~T4TestCal(void)
{

}
