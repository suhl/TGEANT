#include "T4WorkshopExample.hh"

T4WorkshopExample::T4WorkshopExample(T4SDetector* input)
{
  setPosition(input->position);
  useMechanicalStructure = input->useMechanicalStructure;

  targetLength = 1.0 / 2. * CLHEP::m;
  targetRadius = 2.0 * CLHEP::cm;
  
  //additional parameters for target
  setDimension(positionVector.z() - targetLength, positionVector.z() + targetLength);
//   setELossParams(7.0 * CLHEP::MeV, 0.028 * CLHEP::MeV / CLHEP::mm);
}

void T4WorkshopExample::construct(G4LogicalVolume* world_log)
{
  targetCellRotation = new G4RotationMatrix();
  targetCellRotation->rotateX(-5.0 * CLHEP::deg);
  
  target_tubs = new G4Tubs("target_tubs", 0, targetRadius, targetLength, 0, 360 * CLHEP::deg);
  target_log = new G4LogicalVolume(target_tubs, materials->lh2, "target_log");
  addToTarget(new G4PVPlacement(targetCellRotation, positionVector, target_log, "target_phys", world_log, 0, 0, checkOverlap));
  target_log->SetVisAttributes(colour->red);

  regionManager->addToTargetRegion(target_log);
}

void T4WorkshopExample::getTargetDetDat(
    std::vector<T4STargetInformation>& targetInformation)
{
  T4STargetInformation target;
  target.name = "T001";
  target.rotMatrix = TGEANT::ROT_XtoZ;
  target.sh = 5;
  target.xSize = targetRadius;
  target.ySize = targetLength * 2.;
  target.zSize = 0;
  target.xCen = positionVector.getX();
  target.yCen = positionVector.getY();
  target.zCen = positionVector.getZ();

  targetInformation.push_back(target);
}


double T4WorkshopExample::getRndmTargetDist(double maxTargetLength) 
{
  if (maxTargetLength == 0)
    return -1;
  
  return maxTargetLength * T4SettingsFile::getInstance()->getRandom()->flat();
//   return 2. * targetLength * T4SettingsFile::getInstance()->getRandom()->flat();
}

T4WorkshopExample::~T4WorkshopExample(void)
{

}
