#include "SciFiTest.hh"

SciFiTest::SciFiTest(void)
{
  T4SSciFiTest* sciFiTest = settingsFile->getStructManager()->getSciFiTest();
  positionVector = G4ThreeVector(sciFiTest->general.position[0], sciFiTest->general.position[1], sciFiTest->general.position[2]);

}

SciFiTest::~SciFiTest(void)
{

}

void SciFiTest::construct(G4LogicalVolume* world_log)
{
  sizeX = 10./2. * CLHEP::cm;
  sizeY = 1./2. * CLHEP::cm;
  sizeZ = 30./2. * CLHEP::cm;
  
  airGap = 0.1 * CLHEP::mm;
  sizePMT = 6. / 2. * CLHEP::mm;
  
  lgSpace = 30. * CLHEP::cm;
  alu_box = new G4Box("alu_box", sizeX + 2.*airGap, sizeY + 2.*airGap, sizeZ + 2.*airGap + lgSpace);
  air_box[0] = new G4Box("air_box[0]", sizeX + airGap, sizeY + airGap, sizeZ + airGap);
  air_box[1] = new G4Box("air_box[1]", sizeX + airGap, sizeY + airGap, sizeZ + airGap/2.);
  scinti_box = new G4Box("scinti_box", sizeX, sizeY, sizeZ);
  pmt_box = new G4Box("pmt_box", sizePMT, sizePMT, airGap / 2.);
  pmt_hull_box = new G4Box("pmt_hull_box", sizePMT+airGap, sizePMT+airGap, airGap / 2.);
    
//   // #0 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log);
//   buildPMT(4. * CLHEP::mm, 10, "PMT_0");
// 
//   // #1 -----------------------------------------------------------------TODO
   buildGeometries(4.5*CLHEP::m, world_log);
   buildPMT(64./6. * CLHEP::mm, 6, "PMT_1");
//   
//   // #2 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.01 / 2. *CLHEP::cm, 6, "PMT_2");
// 
//   // #3 -----------------------------------------------------------------TODO
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.5 / 2. *CLHEP::cm, 6, "PMT_3");
// 
//   // #4 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 1. / 2. *CLHEP::cm, 6, "PMT_4");
// 
//   // #5 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 2. / 2. *CLHEP::cm, 6, "PMT_5");
// 
//   // #6 -----------------------------------------------------------------TODO
//  buildGeometries(4.5*CLHEP::m, world_log, true);
//  buildPMT_LG(64./6. * CLHEP::mm, 3. / 2. *CLHEP::cm, 6, "PMT_6");
// 
//   // #7 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 10. / 2. *CLHEP::cm, 6, "PMT_7"); 
//     
//   // #8 -----------------------------------------------------------------
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 30. / 2. *CLHEP::cm, 6, "PMT_8"); 

  // hier verschiedene:
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 1.1 / 2. *CLHEP::cm, 6, "PMT_9"); 
//   
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.9 / 2. *CLHEP::cm, 6, "PMT_10");   
//   
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 1.0 / 2. *CLHEP::cm, 6, "PMT_11");   
//   
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.8 / 2. *CLHEP::cm, 6, "PMT_12");   
//   
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.7 / 2. *CLHEP::cm, 6, "PMT_13");   
  
  
//   buildGeometries(4.5*CLHEP::m, world_log, true);
//   buildPMT_LG(64./6. * CLHEP::mm, 0.4 / 2. *CLHEP::cm, 6, "PMT_16");   

  
  // --------------------------------------------------------------------
  for (unsigned int i = 0; i < alu_phys.size() && settingsFile->isOpticalPhysicsActivated(); i++) {
    new G4LogicalBorderSurface("sciAir", scinti_phys.at(i), air_phys.at(i), materials->surfaceBc408Air);
//     new G4LogicalBorderSurface("airSci", air_phys.at(i), scinti_phys.at(i), materials->surfaceAirBc408);
//     new G4LogicalBorderSurface("airAl", air_phys.at(i), alu_phys.at(i), materials->surfaceAluminium);
    
  }
}

void SciFiTest::buildGeometries(double yPos, G4LogicalVolume* world_log, bool shortAir) 
{
  alu_log.push_back(new G4LogicalVolume(alu_box, materials->air_noOptical, "alu_log", 0, 0, 0));
  alu_log.back()->SetVisAttributes(colour->black_20);
  regionManager->addToOpticalRegion(alu_log.back());
  if (shortAir)
    air_log.push_back(new G4LogicalVolume(air_box[1], materials->air_optical, "air_log", 0, 0, 0));
  else
    air_log.push_back(new G4LogicalVolume(air_box[0], materials->air_optical, "air_log", 0, 0, 0));
  scinti_log.push_back(new G4LogicalVolume(scinti_box, materials->bc408_optical, "scinti_log", 0, 0, 0));
  scinti_log.back()->SetVisAttributes(colour->yellow);
  
  alu_phys.push_back(new G4PVPlacement(0, positionVector + G4ThreeVector(0,yPos,-lgSpace), alu_log.back(), "alu_phys", world_log, false, 0, checkOverlap));
  
  if (shortAir) {
    air_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,lgSpace+airGap/2.), air_log.back(), "air_phys", alu_log.back(), false, 0, checkOverlap));
    scinti_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,-airGap/2.), scinti_log.back(), "scinti_phys", air_log.back(), false, 0, checkOverlap));
  } else {
    air_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,lgSpace), air_log.back(), "air_phys", alu_log.back(), false, 0, checkOverlap));
    scinti_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,0), scinti_log.back(), "scinti_phys", air_log.back(), false, 0, checkOverlap));
  }
}

void SciFiTest::buildPMT(double gap, int nChannel, string name)
{
  for (int ch = 0; ch < nChannel; ch++) {
    pmt_hull_log.push_back(new G4LogicalVolume(pmt_hull_box, materials->air_noOptical, "pmt_hull_log", 0, 0, 0));
    pmt_log.push_back(new G4LogicalVolume(pmt_box, materials->bialkali_optical, "pmt_log", 0, 0, 0));
    pmt_hull_log.back()->SetVisAttributes(colour->darkgray_50);
    pmt_log.back()->SetVisAttributes(colour->green);
    pmt_hull_phys.push_back(new G4PVPlacement(0, G4ThreeVector(-sizeX + gap/2. + sizePMT + (gap+sizePMT*2.)*ch, 0,-sizeZ-airGap / 2.), 
	pmt_hull_log.back(), "pmt_hull_phys", air_log.back(), false, 0, checkOverlap));
    pmt_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,0), 
	pmt_log.back(), "pmt_phys", pmt_hull_log.back(), false, 0, checkOverlap));
      
    pmt_log.back()->SetSensitiveDetector(new T4SensitiveDetector(name, ch, TGEANT::PMT));
    new G4LogicalBorderSurface("vacBialkali", scinti_phys.back(), pmt_phys.back(), materials->surfaceVacuumBialkali);
  }
}

void SciFiTest::buildPMT_LG(double gap, double lgLength, int nChannel, string name)
{
  double size_z = 1.0 * CLHEP::cm;
  
  G4Box* box_air = new G4Box("box_air", sizeX+airGap, sizeY+airGap, size_z/2.);
  G4Box* box_lg = new G4Box("box_lg", sizeX, sizeY, 1.0/2. * CLHEP::cm);
  
  G4LogicalVolume* log_air = new G4LogicalVolume(box_air, materials->air_optical, "log_air", 0, 0, 0);
  G4LogicalVolume* log_lg = new G4LogicalVolume(box_lg, materials->plexiglass_optical, "log_lg", 0, 0, 0);
  log_lg->SetVisAttributes(colour->white);
  air_trd_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,lgSpace-sizeZ-size_z/2.), log_air, "air_trd_phys", alu_log.back(), false, 0, checkOverlap));
  plexi_trd_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,0), log_lg, "plexi_trd_phys", log_air, false, 0, checkOverlap));

  
  G4Trd* air_trd = new G4Trd("air_trd", sizePMT+airGap, sizeX/6., sizePMT+airGap, sizeY+airGap, lgLength);
  G4Trd* lg_trd = new G4Trd("lgPMT_2_trd", sizePMT, sizeX/6.-airGap, sizePMT, sizeY, lgLength);

  for (int ch = 0; ch < nChannel; ch++) {
    double xpos = -sizeX + gap/2. + sizePMT + (gap+sizePMT*2.)*ch;
    
    air_trd_log.push_back(new G4LogicalVolume(air_trd, materials->air_optical, "air_trd_log", 0, 0, 0));
    plexi_trd_log.push_back(new G4LogicalVolume(lg_trd, materials->plexiglass_optical, "plexi_trd_log", 0, 0, 0));
    plexi_trd_log.back()->SetVisAttributes(colour->white);
    
    air_trd_phys.push_back(new G4PVPlacement(0, G4ThreeVector(xpos,0,lgSpace-sizeZ-lgLength-size_z), air_trd_log.back(), "air_trd_phys", alu_log.back(), false, 0, checkOverlap));
    plexi_trd_phys.push_back(new G4PVPlacement(0, G4ThreeVector(0,0,0), plexi_trd_log.back(), "plexi_trd_phys", air_trd_log.back(), false, 0, checkOverlap));

    pmt_log.push_back(new G4LogicalVolume(pmt_box, materials->bialkali_optical, "pmt_log", 0, 0, 0));
    pmt_log.back()->SetVisAttributes(colour->green);
    pmt_phys.push_back(new G4PVPlacement(0, G4ThreeVector(xpos, 0,lgSpace-sizeZ-lgLength*2.-size_z-airGap / 2.), 
	pmt_log.back(), "pmt_phys", alu_log.back(), false, 0, checkOverlap));
      
    pmt_log.back()->SetSensitiveDetector(new T4SensitiveDetector(name, ch, TGEANT::PMT));
    
    
    if (settingsFile->isOpticalPhysicsActivated()) {
      new G4LogicalBorderSurface("vacBialkali", plexi_trd_phys.back(), pmt_phys.back(), materials->surfaceVacuumBialkali);
      new G4LogicalBorderSurface("sciPlexi", scinti_phys.back(), plexi_trd_phys.back(), materials->surfaceBc408Plexiglass);
      new G4LogicalBorderSurface("plexiAir", plexi_trd_phys.back(), air_trd_phys.back(), materials->surfacePlexiglassAir);
//       new G4LogicalBorderSurface("airPlexi", air_trd_phys.back(), plexi_trd_phys.back(), materials->surfaceAirPlexiglass);
//       new G4LogicalBorderSurface("airAl", air_trd_phys.back(), alu_phys.back(), materials->surfaceAluminium);
    }
  }
}
