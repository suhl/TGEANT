#include "T4TargetBackend.hh"

T4ActiveTarget* T4ActiveTarget::instance = NULL;

T4ActiveTarget* T4ActiveTarget::getInstance(void)
{
  if (instance == NULL)
    instance = new T4ActiveTarget();
  return instance;
}

void T4ActiveTarget::registerTarget(T4TargetBackend* target)
{
  if (activeTarget == NULL)
    activeTarget = target;
  else
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__,
        "T4ActiveTarget::registerTarget: There is already an active target registered! Only one target can be used!");
}

T4TargetBackend::T4TargetBackend(void)
{
  setDimension(0.0,0.0);
  setELossParams(0.0,0.0);
  T4ActiveTarget::getInstance()->registerTarget(this);
}

bool T4TargetBackend::isVolumeInTarget(G4VPhysicalVolume* phys)
{
  return std::find(activeTargetVolume.begin(), activeTargetVolume.end(), phys)
      != activeTargetVolume.end();
}
