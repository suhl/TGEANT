#ifndef H2CONSTRUCTION_HH_
#define H2CONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"

#include "LGFishtail.hh"
#include "LightGuideRingA.hh"
#include "T4PhotoMultiplier.hh"
#include "T4SensitiveDetector.hh"

// Position is defined as the center of the Y2 (more upstream) plane.
// Each plane is build with alternating scintillators. The center is defined as the middle between both layers.
class H2Construction : public T4BaseDetector
{
  public:
    H2Construction(T4SDetectorRes*);
    virtual ~H2Construction(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4String name_Y1;
    G4String name_Y2;
    G4int detectorId_Y1;
    G4int detectorId_Y2;
    G4bool useOptical;
    G4ThreeVector slabPosition[64];

    G4double slabDimensions[3];
    G4double lengthShort;
    G4double lengthBent;
    G4double lengthPlexi;
    G4double lengthLong;
    G4double overlap;
    G4double overlapY;
    G4double overlapZ;

    G4double airGap;
    G4double aluFoil;
    G4double plasticFoil;
    G4double pmtRadius;
    G4double pmtLength;
    G4double lgLength;

    G4Box* plasticNormal_box;
    G4Box* aluminiumNormal_box;
    G4Box* airGapNormal_box;
    G4Box* scintNormal_box;

    G4Box* plasticShort_box;
    G4Box* aluminiumShort_box;
    G4Box* airGapShort_box;
    G4Box* scintShort_box;

    G4Box* plasticBent_box;
    G4Box* aluminiumBent_box;
    G4Box* airGapBent_box;
    G4Box* scintBent_box;

    G4Box* plasticLong_box;
    G4Box* aluminiumLong_box;
    G4Box* airGapLong_box;
    G4Box* plexi_box;

    G4LogicalVolume* plastic_log[64];
    G4LogicalVolume* aluminium_log[64];
    G4LogicalVolume* airGap_log[64];
    G4LogicalVolume* scintillator_log[64];
    G4LogicalVolume* plexi_log[4];

    G4RotationMatrix* rotationLG;
    G4RotationMatrix* rotationY1Bent;

    LGFishtail* lightGuideY1Jura[32];
    LGFishtail* lightGuideY1Saleve[26];
    LGFishtail* lightGuideY2Jura[26];
    LGFishtail* lightGuideY2Saleve[32];

    LightGuideRingA* lightGuideY1Bent[6];
    LightGuideRingA* lightGuideY2Bent[6];

    T4PhotoMultiplier* pmtY1Jura[32];
    T4PhotoMultiplier* pmtY1Saleve[26];
    T4PhotoMultiplier* pmtY2Jura[26];
    T4PhotoMultiplier* pmtY2Saleve[32];
    T4PhotoMultiplier* pmtY1Bent[6];
    T4PhotoMultiplier* pmtY2Bent[6];
};

#endif /* H2CONSTRUCTION_HH_ */
