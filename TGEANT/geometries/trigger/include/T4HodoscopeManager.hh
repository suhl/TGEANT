#ifndef T4HODOSCOPEMANAGER_HH_
#define T4HODOSCOPEMANAGER_HH_

#include "T4HodoscopeBackEnd.hh"

// X: channel number increasing from saleve (negative x values) to jura (positive x values)
// Y: channel number increasing from bottom (negative y values) to top (positive y values)
// The detectorIds are strictly numbered beginning with detectorId = 1001.
class T4HodoscopeManager : public T4BaseDetector
{
  public:
    T4HodoscopeManager(void);
    virtual ~T4HodoscopeManager(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    void addHI04(void);
    void addHI05(void);
    void addHL04(void);
    void addHL05(void);
    void addHM04(void);
    void addHM05(void);

    std::vector<T4HodoscopeBackEnd*> hodoscopes;
    G4int currentId;

    // H4
    T4SDetector* hi04x1_u;
    T4SDetector* hi04x1_d;
    T4SDetector* hm04y1_u1;
    T4SDetector* hm04y1_u2;
    T4SDetector* hm04y1_d1;
    T4SDetector* hm04y1_d2;
    T4SDetector* hm04x1_u;
    T4SDetector* hm04x1_d;
    T4SDetector* hl04x1_1;
    T4SDetector* hl04x1_2;
    T4SDetector* hl04x1_3;
    T4SDetector* hl04x1_4;

    // H5
    T4SDetector* hi05x1_u;
    T4SDetector* hi05x1_d;
    T4SDetector* hm05y1_u1;
    T4SDetector* hm05y1_u2;
    T4SDetector* hm05y1_d1;
    T4SDetector* hm05y1_d2;
    T4SDetector* hm05x1_u;
    T4SDetector* hm05x1_d;
    T4SDetector* hl05x1_1;
    T4SDetector* hl05x1_2;
    T4SDetector* hl05x1_3;
    T4SDetector* hl05x1_4;
};

#endif /* T4HODOSCOPEMANAGER_HH_ */
