#ifndef T4HO04_HH_
#define T4HO04_HH_

#include "T4HodoscopeBackEnd.hh"

class T4HO04 : public T4HodoscopeBackEnd
{
  public:
    T4HO04(T4SDetector*);
    virtual ~T4HO04(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    // saleve = Y2;
    // jura = Y1;
    G4double scintiLengthSaleve;
    G4double scintiLengthJura_1;
    G4double scintiLengthJura_2;
    G4double zDistanceY1Y2;
    G4double overlapY1Y2;

    G4int detectorIdY1[5];
    G4int detectorIdY2[5];

    G4Box* scintiNormal_box;
    G4Box* scintiShortSaleve_box;
    G4Box* scintiShortJura1_box;
    G4Box* scintiShortJura2_box;

    double yearSetup;
};

#endif /* T4HO04_HH_ */
