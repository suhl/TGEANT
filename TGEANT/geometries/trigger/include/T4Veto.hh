#ifndef T4VETO_HH_
#define T4VETO_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

class T4VetoPlane : public T4BaseDetector
{
  public:
    T4VetoPlane(const T4SDetector*, int _firstDetectorId);
    virtual ~T4VetoPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;

    int detectorId;
    const T4SDetector* det;

    G4double dimension[3];
    G4double dimensionHole[3];
    G4bool isTube;
    string detDat_detName;
};

class T4Veto : public T4BaseDetector
{
  public:
    T4Veto(void) {}
    virtual ~T4Veto(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4VetoPlane*> vetoPlane;
};

#endif /* T4VETO_HH_ */
