#ifndef T4BEAMKILLER_HH_
#define T4BEAMKILLER_HH_

#include "T4BaseDetector.hh"
#include "G4Tubs.hh"

class T4BeamKiller : public T4BaseDetector
{
  public:
    T4BeamKiller(T4SDetector*);
    virtual ~T4BeamKiller(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4double diameter;
    G4double thickness;
    G4Tubs* bk_tubs;
    G4LogicalVolume* bk_log;

    G4int detectorId;
    string tbName;
};

#endif /* T4BEAMKILLER_HH_ */
