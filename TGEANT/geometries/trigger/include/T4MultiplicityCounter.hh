#ifndef T4MULTIPLICITYCOUNTER_HH_
#define T4MULTIPLICITYCOUNTER_HH_

#include "T4BaseDetector.hh"

#include "LightGuideMultCounter.hh"
#include "T4PhotoMultiplier.hh"
#include "ScintillatorTrd.hh"

#include "G4SubtractionSolid.hh"

class T4MultiplicityCounter : public T4BaseDetector
{
  public:
    T4MultiplicityCounter(T4SDetector*);
    virtual ~T4MultiplicityCounter(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    CLHEP::HepRotation rotationMatrix;

    ScintillatorTrd* scintillatorTrd;
    LightGuideMultCounter* lg_up;
    LightGuideMultCounter* lg_down;
    T4PhotoMultiplier* photoMultiplier_up;
    T4PhotoMultiplier* photoMultiplier_down;

    string tbName;
    int detectorId;

    double sci_x;
    double sci_y;
    double sci_z;
};
#endif
