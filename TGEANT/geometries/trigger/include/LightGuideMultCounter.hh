#ifndef LIGHTGUIDEMULTCOUNTER_HH_
#define LIGHTGUIDEMULTCOUNTER_HH_

#include "T4BaseDetector.hh"
#include "LightGuideBackEnd.hh"

class LightGuideMultCounter : public T4BaseDetector, public LightGuideBackEnd
{
  public:
    LightGuideMultCounter(void);
    virtual ~LightGuideMultCounter(void);

    void construct(G4LogicalVolume*);

    void setAngle(G4double _angle)
      {angle = _angle;};
    G4ThreeVector getPmtPosition(G4double pmtLenght);
    void setRotationZ(double _angle) {rotZ = _angle;}

  private:
    G4double box_length;
    G4double avgRadius;
    G4double angle;
    G4double cylinder_Length ;

    G4Box* lightguide_box;
    G4Tubs* lightguide_arc;
    G4UnionSolid* lightguide_union2;
    G4UnionSolid* lightguide_union3;
    G4Box* plastic_box;
    G4Tubs* plastic_arc;
    G4UnionSolid* plastic_union2;
    G4UnionSolid* plastic_union3;
    G4Box* airGap_box;
    G4Tubs* airGap_arc;
    G4UnionSolid* airGap_union2;
    G4UnionSolid* airGap_union3;
    G4Box* alu_box;
    G4Tubs* alu_arc;
    G4UnionSolid* alu_union2;
    G4UnionSolid* alu_union3;
};

#endif /* LIGHTGUIDEMULTCOUNTER_HH_ */
