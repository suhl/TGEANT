#ifndef VETOCONSTRUCTION_HH_
#define VETOCONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"

#include "LGFishtail.hh"
#include "T4PhotoMultiplier.hh"
#include "T4SensitiveDetector.hh"

class VetoConstruction : public T4BaseDetector
{
  public:
    VetoConstruction(T4SDetectorRes*);
    virtual ~VetoConstruction(void);

    void construct(G4LogicalVolume*);

  private:
    void constructMechanicalStructure(G4LogicalVolume*);

    G4bool useOptical;

    G4ThreeVector slabPosition[20];
    G4double slabDimensions[3];
    G4double lengthShort;
    G4double lengthHole;
    G4double airGap;
    G4double aluFoil;
    G4double plasticFoil;
    G4double pmtRadius;
    G4double pmtLength;
    G4double lgLength;
    G4double overlapX;
    G4double offsetZ;

    G4Box* plastic_box[2];
    G4Box* aluminium_box[2];
    G4Box* airGap_box[2];
    G4Box* scintillator_box[2];

    G4LogicalVolume* plastic_log[20];
    G4LogicalVolume* aluminium_log[20];
    G4LogicalVolume* airGap_log[20];
    G4LogicalVolume* scintillator_log[20];

    G4RotationMatrix* rotationLG;
    LGFishtail* lightGuideTop[18];
    LGFishtail* lightGuideBottom[18];
    T4PhotoMultiplier* pmtTop[18];
    T4PhotoMultiplier* pmtBottom[18];

    G4Box* table_box;
    G4Box* tableBarX_box;
    G4Box* tableBarY_box;
    G4Box* tableBarZ_box;

    G4LogicalVolume* table_log;
    G4LogicalVolume* tableBoschX_log;
    G4LogicalVolume* tableBoschY_log;
    G4LogicalVolume* tableBoschZ_log;

    G4Box* topBarX_box[2];
    G4Box* topBarY_box;

    G4LogicalVolume* topBoschX_log[2];
    G4LogicalVolume* topBoschY_log;

    CLHEP::HepRotation* rotationAnglePiece;
    G4Box* anglePiece_box;
    G4Box* boxAnglePiece_box;
    G4IntersectionSolid* anglePiece_intersection;
    G4LogicalVolume* anglePiece_log;
};

#endif /* VETOCONSTRUCTION_HH_ */
