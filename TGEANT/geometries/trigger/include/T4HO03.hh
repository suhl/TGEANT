#ifndef T4HO03_HH_
#define T4HO03_HH_

#include "T4HodoscopeBackEnd.hh"
#include "G4SubtractionSolid.hh"

class T4HO03 : public T4HodoscopeBackEnd
{
  public:
    T4HO03(T4SDetector*);
    virtual ~T4HO03(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4double holeLength;
    G4double holeLengthLong;
    G4int detectorId[5];

    G4Box* scinti_box;
    G4Box* plexi_box;
    G4Box* plexiLong_box;
    G4LogicalVolume* plexi_log;
    G4LogicalVolume* plexiLong_log;
    
    double yearSetup;
};

#endif /* T4HO03_HH_ */
