#ifndef H1CONSTRUCTION_HH_
#define H1CONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

#include "LGFishtail.hh"
#include "T4PhotoMultiplier.hh"
#include "T4SensitiveDetector.hh"

// positionVector: middle of plane (not the hole)
// z position: middle between two planes
class H1Construction : public T4BaseDetector
{
  public:
    H1Construction(T4SDetectorRes*);
    virtual ~H1Construction(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4int detectorId[5];
    G4String tbName;
    G4String detName[5];

    G4bool useOptical;

    G4int holeIndex[2];

    void constructMechanicalStructure(G4LogicalVolume*);

    G4double xAxisOffset;
    G4double zPositionShift;
    G4ThreeVector slabPosition[32];
    G4double slabDimensions[3];
    G4double lengthJura;
    G4double lengthSaleve;
    G4double lengthHole;
    G4double airGap;
    G4double aluFoil;
    G4double plasticFoil;
    G4double pmtRadius;
    G4double pmtLength;
    G4double lgLength;

    G4Box* plastic_box;
    G4Box* aluminium_box;
    G4Box* airGap_box;
    G4Box* scintillator_box;
    G4Box* holeLG_box;
    G4Box* holeLG_box2;
    G4SubtractionSolid* scintillator_sub;

    G4LogicalVolume* plastic_log[32];
    G4LogicalVolume* aluminium_log[32];
    G4LogicalVolume* airGap_log[32];
    G4LogicalVolume* scintillator_log[32];
    G4LogicalVolume* holeLG_log[6];

    G4RotationMatrix* rotationLG;
    LGFishtail* lightGuideJura[32];
    LGFishtail* lightGuideSaleve[32];
    T4PhotoMultiplier* pmtJura[32];
    T4PhotoMultiplier* pmtSaleve[32];

    G4Box* boschX_box[5];
    G4Box* boschY_box[2];
    G4Box* boschZ_box;

    G4LogicalVolume* boschX_log[5];
    G4LogicalVolume* boschY_log[2];
    G4LogicalVolume* boschZ_log;
};

#endif /* H1CONSTRUCTION_HH_ */
