#ifndef T4SANDWICHVETO_HH_
#define T4SANDWICHVETO_HH_

#include "T4BaseDetector.hh"
#include "G4Box.hh"
#include "G4Polyhedra.hh"
#include "G4SubtractionSolid.hh"

class T4SandwichVeto : public T4BaseDetector
{
  public:
    T4SandwichVeto(T4SDetector*);
    virtual ~T4SandwichVeto(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    void buildModule(G4LogicalVolume*, int);
    void subtractAndPlace(const G4String& pName, G4Box*, G4ThreeVector,
        G4Material*, G4LogicalVolume*, const G4VisAttributes*,
        bool placeAtOrigin = true);

    vector<G4VSolid*> box;
    vector<G4LogicalVolume*> log;
    G4Polyhedra* hole_pgon;

    double moduleSize[3];

    double posX[12];
    double posY[12];

    G4Box* box_szinti[2];
    G4Box* box_pb[2];
    G4Box* box_steel[2];
    G4Box* box_steel_end[2];
    G4Box* box_air_big[2];
    G4Box* box_air_small[2];

    double posZ_pb[10];
    double posZ_steel[6];
    double posZ_airGap[10];

    string tbName[12];
    int detectorId[12];
};

#endif /* T4SANDWICHVETO_HH_ */
