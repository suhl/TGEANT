#ifndef T4MAINZCOUNTER_HH_
#define T4MAINZCOUNTER_HH_

#include "T4BaseDetector.hh"
#include "G4Box.hh"

class T4MainzCounter02 : public T4BaseDetector
{
  public:
    T4MainzCounter02(T4SDetector*);
    virtual ~T4MainzCounter02(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4Box* hh02_box;
    G4LogicalVolume* hh02_log;
};

class T4MainzCounter03 : public T4BaseDetector
{
  public:
    T4MainzCounter03(T4SDetector*);
    virtual ~T4MainzCounter03(void);

    void construct(G4LogicalVolume*);

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:

};

#endif /* T4MAINZCOUNTER_HH_ */
