#ifndef T4HODOSCOPEBACKEND_HH_
#define T4HODOSCOPEBACKEND_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"

struct T4HodoscopeInformation
{
    G4String tbName;
    G4String det;
    G4int detectorId;
    G4int nChannel;
    G4int firstChannelNo;
    G4double scintillatorSize[3];
    G4double overlap;
    G4double overlapShift;
    G4double airGap;
    G4double angle;
    G4Material* material;
};

class T4HodoscopeBackEnd : public T4BaseDetector
{
  public:
    T4HodoscopeBackEnd(T4HodoscopeInformation);
    T4HodoscopeBackEnd(void) {scintillator_box = NULL;};
    virtual ~T4HodoscopeBackEnd(void);

    virtual void construct(G4LogicalVolume*);

    virtual void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  protected:
    void setLocalPosition(void);

    T4HodoscopeInformation hInfo;
    std::vector<G4LogicalVolume*> scintillator_log;
    std::vector<G4ThreeVector> localPosition;

  private:
    G4Box* scintillator_box;
};

#endif /* T4THODOSCOPEBACKEND_HH_ */
