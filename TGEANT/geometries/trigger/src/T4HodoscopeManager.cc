#include "T4HodoscopeManager.hh"

T4HodoscopeManager::T4HodoscopeManager(void)
{
  std::vector<T4SDetector>* list = settingsFile->getStructManager()->getDetector();

  // Info: The hodoscope names of this list are from the GUI and libT4Settings.
  // We can identify the different planes with these unique names.
  // The tbNames will be different (and not unique), because they are hard coded in phast.
  for (unsigned int i = 0; i < list->size(); i++) {
    if (list->at(i).name == "HI04X1_u")
      hi04x1_u = &list->at(i);
    else if (list->at(i).name == "HI04X1_d")
      hi04x1_d = &list->at(i);
    else if (list->at(i).name == "HM04Y1_u1")
      hm04y1_u1 = &list->at(i);
    else if (list->at(i).name == "HM04Y1_u2")
      hm04y1_u2 = &list->at(i);
    else if (list->at(i).name == "HM04Y1_d1")
      hm04y1_d1 = &list->at(i);
    else if (list->at(i).name == "HM04Y1_d2")
      hm04y1_d2 = &list->at(i);
    else if (list->at(i).name == "HM04X1_u")
      hm04x1_u = &list->at(i);
    else if (list->at(i).name == "HM04X1_d")
      hm04x1_d = &list->at(i);
    else if (list->at(i).name == "HL04X1_1")
      hl04x1_1 = &list->at(i);
    else if (list->at(i).name == "HL04X1_2")
      hl04x1_2 = &list->at(i);
    else if (list->at(i).name == "HL04X1_3")
      hl04x1_3 = &list->at(i);
    else if (list->at(i).name == "HL04X1_4")
      hl04x1_4 = &list->at(i);

    else if (list->at(i).name == "HI05X1_u")
      hi05x1_u = &list->at(i);
    else if (list->at(i).name == "HI05X1_d")
      hi05x1_d = &list->at(i);
    else if (list->at(i).name == "HM05Y1_u1")
      hm05y1_u1 = &list->at(i);
    else if (list->at(i).name == "HM05Y1_u2")
      hm05y1_u2 = &list->at(i);
    else if (list->at(i).name == "HM05Y1_d1")
      hm05y1_d1 = &list->at(i);
    else if (list->at(i).name == "HM05Y1_d2")
      hm05y1_d2 = &list->at(i);
    else if (list->at(i).name == "HM05X1_u")
      hm05x1_u = &list->at(i);
    else if (list->at(i).name == "HM05X1_d")
      hm05x1_d = &list->at(i);
    else if (list->at(i).name == "HL05X1_1")
      hl05x1_1 = &list->at(i);
    else if (list->at(i).name == "HL05X1_2")
      hl05x1_2 = &list->at(i);
    else if (list->at(i).name == "HL05X1_3")
      hl05x1_3 = &list->at(i);
    else if (list->at(i).name == "HL05X1_4")
      hl05x1_4 = &list->at(i);
  }

  // set the start id
  currentId = 1051;

  addHI04();
  addHI05();
  addHL04();
  addHL05();
  addHM04();
  addHM05();
}

T4HodoscopeManager::~T4HodoscopeManager(void)
{
  for (unsigned int i = 0; i < hodoscopes.size(); i++)
    delete hodoscopes.at(i);
  hodoscopes.clear();
}

void T4HodoscopeManager::construct(G4LogicalVolume* world_log)
{
  for (unsigned int i = 0; i < hodoscopes.size(); i++)
    hodoscopes.at(i)->construct(world_log);
}

void T4HodoscopeManager::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < hodoscopes.size(); i++)
    hodoscopes.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4HodoscopeManager::addHI04(void)
{
  // ATTENTION: inclination angle (30 degree) not considered
  T4HodoscopeInformation h;
  h.tbName = "HI04X1_u";
  h.det = "H4D2";
  h.detectorId = currentId;
  h.nChannel = 32;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 6.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 194.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 10.0 / 2 * CLHEP::mm;
  h.overlap = 0.6 * CLHEP::mm;
  h.overlapShift = 0;
  h.airGap = 0.1 * CLHEP::cm;
  h.angle = 0;
  h.material = materials->bc408_noOptical;
  if (hi04x1_u->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hi04x1_u->position);
  }
  currentId += h.nChannel;

  h.tbName = "HI04X1_d";
  h.det = "H4D1";
  h.detectorId = currentId;
  if (hi04x1_d->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hi04x1_d->position);
  }
  currentId += h.nChannel;
}

void T4HodoscopeManager::addHI05(void)
{
  T4HodoscopeInformation h;
  h.tbName = "HI05X1_u";
  h.det = "H5D2";
  h.detectorId = currentId;
  h.nChannel = 32;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 12.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 259.5 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 20.0 / 2 * CLHEP::mm;
  h.overlap = 1.0 * CLHEP::mm;
  h.overlapShift = 0;
  h.airGap = 0.1 * CLHEP::cm;
  h.angle = 0;
  h.material = materials->bc408_noOptical;
  if (hi05x1_u->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hi05x1_u->position);
  }
  currentId += h.nChannel;

  h.tbName = "HI05X1_d";
  h.det = "H5D1";
  h.detectorId = currentId;
  if (hi05x1_d->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hi05x1_d->position);
  }
  currentId += h.nChannel;
}

void T4HodoscopeManager::addHL04(void)
{
  // They all have the same tbName!
  T4HodoscopeInformation h;
  h.tbName = "HL04X1_m";
  h.det = "H4L1";
  h.detectorId = currentId;
  h.nChannel = 8;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 22.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 400.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 15.0 / 2 * CLHEP::mm;
  h.overlap = 2.0 * CLHEP::mm;
  h.overlapShift = 1.0 * CLHEP::mm;
  h.airGap = 2.5 * CLHEP::cm;
  h.angle = 0;
  h.material = materials->bc408_noOptical;
  if (hl04x1_1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl04x1_1->position);
  }
  currentId += h.nChannel;

  h.det = "H4L2";
  h.detectorId = currentId;
  h.firstChannelNo = 8;
  h.scintillatorSize[0] = 32.0 / 2 * CLHEP::mm;
  if (hl04x1_2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl04x1_2->position);
  }
  currentId += h.nChannel;

  h.det = "H4L3";
  h.detectorId = currentId;
  h.firstChannelNo = 16;
  h.scintillatorSize[0] = 47.0 / 2 * CLHEP::mm;
  if (hl04x1_3->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl04x1_3->position);
  }
  currentId += h.nChannel;

  h.det = "H4L4";
  h.detectorId = currentId;
  h.firstChannelNo = 24;
  h.scintillatorSize[0] = 67.0 / 2 * CLHEP::mm;
  if (hl04x1_4->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl04x1_4->position);
  }
  currentId += h.nChannel;
}

void T4HodoscopeManager::addHL05(void)
{
  T4HodoscopeInformation h;
  h.tbName = "HL05X1_m";
  h.det = "H5L1";
  h.detectorId = currentId;
  h.nChannel = 8;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 27.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 475.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 15.0 / 2 * CLHEP::mm;
  h.overlap = 2.0 * CLHEP::mm;
  h.overlapShift = 1.0 * CLHEP::mm;
  h.airGap = 2.5 * CLHEP::cm;
  h.angle = 0;
  h.material = materials->bc408_noOptical;
  if (hl05x1_1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl05x1_1->position);
  }
  currentId += h.nChannel;

  h.det = "H5L2";
  h.detectorId = currentId;
  h.firstChannelNo = 8;
  h.scintillatorSize[0] = 42.0 / 2 * CLHEP::mm;
  if (hl05x1_2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl05x1_2->position);
  }
  currentId += h.nChannel;

  h.det = "H5L3";
  h.detectorId = currentId;
  h.firstChannelNo = 16;
  h.scintillatorSize[0] = 62.0 / 2 * CLHEP::mm;
  if (hl05x1_3->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl05x1_3->position);
  }
  currentId += h.nChannel;

  h.det = "H5L4";
  h.detectorId = currentId;
  h.firstChannelNo = 24;
  h.scintillatorSize[0] = 87.0 / 2 * CLHEP::mm;
  if (hl05x1_4->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hl05x1_4->position);
  }
  currentId += h.nChannel;
}

void T4HodoscopeManager::addHM04(void)
{
  // Y planes:
  T4HodoscopeInformation h;
  h.tbName = "HM04Y1_d";
  h.det = "H4G1";
  h.detectorId = currentId;
  h.nChannel = 8;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 1200.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 25.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 20.0 / 2 * CLHEP::mm;
  h.overlap = 1.5 * CLHEP::mm;
  h.overlapShift = 0;
  h.airGap = 1.3 * CLHEP::cm;
  h.angle = 90;
  h.material = materials->bc408_noOptical;
  if (hm04y1_d2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04y1_d2->position);
  }
  currentId += h.nChannel;

  h.det = "H4G2";
  h.detectorId = currentId;
  h.firstChannelNo = 8;
  h.scintillatorSize[1] = 21.5 / 2 * CLHEP::mm;
  if (hm04y1_d1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04y1_d1->position);
  }
  currentId += h.nChannel;

  h.tbName = "HM04Y1_u";
  h.det = "H4G3";
  h.detectorId = currentId;
  h.firstChannelNo = 16;
  if (hm04y1_u1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04y1_u1->position);
  }
  currentId += h.nChannel;

  h.det = "H4G4";
  h.detectorId = currentId;
  h.firstChannelNo = 24;
  h.scintillatorSize[1] = 25.0 / 2 * CLHEP::mm;
  if (hm04y1_u2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04y1_u2->position);
  }
  currentId += h.nChannel;

  // X planes:
  h.tbName = "HM04X1_d";
  h.det = "H4U1";
  h.detectorId = currentId;
  h.nChannel = 20;
  h.firstChannelNo = 0;
  h.overlap = 1.0 * CLHEP::mm;
  h.overlapShift = 0;
  h.scintillatorSize[0] = 62.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 355.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 10.0 / 2 * CLHEP::mm;
  h.airGap = 0.7 * CLHEP::cm;
  h.angle = 0;
  if (hm04x1_d->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04x1_d->position);
  }
  currentId += h.nChannel;

  h.tbName = "HM04X1_u";
  h.det = "H4U2";
  h.detectorId = currentId;
  if (hm04x1_u->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm04x1_u->position);
  }
  currentId += h.nChannel;
}

void T4HodoscopeManager::addHM05(void)
{
  // Y planes:
  T4HodoscopeInformation h;
  h.tbName = "HM05Y1_d";
  h.det = "H5G1";
  h.detectorId = currentId;
  h.nChannel = 8;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 1500.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 30.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 20.0 / 2 * CLHEP::mm;
  h.overlap = 1.5 * CLHEP::mm;
  h.overlapShift = 0;
  h.airGap = 1.3 * CLHEP::cm;
  h.angle = 90;
  h.material = materials->bc408_noOptical;
  if (hm05y1_d2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05y1_d2->position);
  }
  currentId += h.nChannel;

  h.det = "H5G2";
  h.detectorId = currentId;
  h.firstChannelNo = 8;
  h.scintillatorSize[1] = 25.0 / 2 * CLHEP::mm;
  if (hm05y1_d1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05y1_d1->position);
  }
  currentId += h.nChannel;

  h.tbName = "HM05Y1_u";
  h.det = "H5G3";
  h.detectorId = currentId;
  h.firstChannelNo = 16;
  if (hm05y1_u1->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05y1_u1->position);
  }
  currentId += h.nChannel;

  h.det = "H5G4";
  h.detectorId = currentId;
  h.firstChannelNo = 24;
  h.scintillatorSize[1] = 30.0 / 2 * CLHEP::mm;
  if (hm05y1_u2->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05y1_u2->position);
  }
  currentId += h.nChannel;

  // X planes:
  h.tbName = "HM05X1_d";
  h.det = "H5U1";
  h.detectorId = currentId;
  h.nChannel = 20;
  h.firstChannelNo = 0;
  h.scintillatorSize[0] = 77.0 / 2 * CLHEP::mm;
  h.scintillatorSize[1] = 425.0 / 2 * CLHEP::mm;
  h.scintillatorSize[2] = 10.0 / 2 * CLHEP::mm;
  h.airGap = 0.7 * CLHEP::cm;
  h.angle = 0;
  h.overlap = 1.0 * CLHEP::mm;
  h.overlapShift = 0;
  if (hm05x1_d->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05x1_d->position);
  }
  currentId += h.nChannel;

  h.tbName = "HM05X1_u";
  h.det = "H5U2";
  h.detectorId = currentId;
  if (hm05x1_u->useDetector) {
    hodoscopes.push_back(new T4HodoscopeBackEnd(h));
    hodoscopes.back()->setPosition(hm05x1_u->position);
  }
  currentId += h.nChannel;
}
