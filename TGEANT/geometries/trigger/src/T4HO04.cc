#include "T4HO04.hh"
#include "T4SGlobals.hh"

T4HO04::T4HO04(T4SDetector* ho04)
{
  setPosition(ho04->position);

  hInfo.angle = 90; // Y-plane
  hInfo.scintillatorSize[0] = 250.0 / 2 * CLHEP::cm;
  hInfo.scintillatorSize[1] = 15.0 / 2 * CLHEP::cm;
  hInfo.scintillatorSize[2] = 2.0 / 2 * CLHEP::cm;
  hInfo.airGap = 0.1 * CLHEP::cm;
  hInfo.material = materials->bc408_noOptical;
  hInfo.nChannel = 16;
  hInfo.overlap = 10.0 * CLHEP::mm;
  hInfo.tbName = "HO04";
  hInfo.overlapShift = 0;

  setLocalPosition();

  yearSetup = strToDouble(T4EventManager::getInstance()->getTriggerPlugin()->getPluginName().substr(0,4));

  if (yearSetup <= 2012) {
    scintiLengthSaleve = 220.0 / 2 * CLHEP::cm;
    scintiLengthJura_1 = 140.0 / 2 * CLHEP::cm;
    scintiLengthJura_2 = 65.0 / 2 * CLHEP::cm;
  } else if (yearSetup >= 2016) {
    scintiLengthSaleve = 231.0 / 2 * CLHEP::cm;
    scintiLengthJura_1 = 145.0 / 2 * CLHEP::cm;
    scintiLengthJura_2 = 90.0 / 2 * CLHEP::cm;
  } else {
    scintiLengthSaleve = 185.0 / 2 * CLHEP::cm;
    scintiLengthJura_1 = scintiLengthJura_2 = scintiLengthSaleve;
  }
//  zDistanceY1Y2 = 35.3 * CLHEP::mm;
  zDistanceY1Y2 = 4.2 * CLHEP::cm;
  overlapY1Y2 = 15.0 * CLHEP::cm;

  detectorIdY1[0] = 1001; // 5 channel
  detectorIdY1[1] = 1006; // 2 channel
  detectorIdY1[2] = 1008; // 2 channel
  detectorIdY1[3] = 1010; // 2 channel
  detectorIdY1[4] = 1012; // 5 channel

  detectorIdY2[0] = 1017; // 5 channel
  detectorIdY2[1] = 1022; // 2 channel
  detectorIdY2[2] = 1024; // 2 channels for coral, they do not exist in 2012 but they are needed
  detectorIdY2[3] = 1026; // 2 channel
  detectorIdY2[4] = 1028; // 5 channel

  scintiNormal_box = NULL;
  scintiShortSaleve_box = NULL;
  scintiShortJura1_box = NULL;
  scintiShortJura2_box = NULL;
}

T4HO04::~T4HO04(void)
{
  if (scintiNormal_box != NULL)
    delete scintiNormal_box;
  if (scintiShortSaleve_box != NULL)
    delete scintiShortSaleve_box;
  if (scintiShortJura1_box != NULL)
    delete scintiShortJura1_box;
  if (scintiShortJura2_box != NULL)
    delete scintiShortJura2_box;
}

void T4HO04::construct(G4LogicalVolume* world_log)
{
  scintiNormal_box = new G4Box("scintiNormal_box", hInfo.scintillatorSize[0],
      hInfo.scintillatorSize[1], hInfo.scintillatorSize[2]);
  scintiShortSaleve_box = new G4Box("scintiShortSaleve_box", scintiLengthSaleve,
      hInfo.scintillatorSize[1], hInfo.scintillatorSize[2]);
  scintiShortJura1_box = new G4Box("scintiShortJura1_box", scintiLengthJura_1,
      hInfo.scintillatorSize[1], hInfo.scintillatorSize[2]);
  scintiShortJura2_box = new G4Box("scintiShortJura2_box", scintiLengthJura_2,
      hInfo.scintillatorSize[1], hInfo.scintillatorSize[2]);

  G4int k;
  G4ThreeVector internalPosition;

  // Y2 plane, Saleve, channel 0-15, more downstream
  for (G4int i = 0; i < hInfo.nChannel; i++) {
     if (i >= 5 && i <= 10) {
       if ((yearSetup <= 2012 /*|| yearSetup >= 2016*/) && (i == 7 || i == 8))
               continue;

      scintillator_log.push_back(
          new G4LogicalVolume(scintiShortSaleve_box, hInfo.material,
              "scintillator_log", 0, 0, 0));
      internalPosition = G4ThreeVector(overlapY1Y2/2 - 2. * hInfo.scintillatorSize[0] + scintiLengthSaleve, 0, zDistanceY1Y2/2);
      k = (i < 7) ? 1 : 3;
      if (i == 7 || i == 8)
        k = 2;
    } else {
      scintillator_log.push_back(
          new G4LogicalVolume(scintiNormal_box, hInfo.material,
              "scintillator_log", 0, 0, 0));
      internalPosition = G4ThreeVector(overlapY1Y2/2 - hInfo.scintillatorSize[0], 0, zDistanceY1Y2/2);
      k = (i < 6) ? 0 : 4;
    }

    scintillator_log.back()->SetVisAttributes(colour->black);

    G4String physName = hInfo.tbName + "Y2" + "_ch" + intToStr(i);
    new G4PVPlacement(0, positionVector + localPosition.at(i) + internalPosition,
        scintillator_log.back(), physName, world_log, false, 0, checkOverlap);

    scintillator_log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(hInfo.tbName + "Y2_m", i, TGEANT::TRIGGER, detectorIdY2[k]));
  }

  // Y1 plane, Jura, channel 16-31, more upstream
  for (G4int i = 0; i < hInfo.nChannel; i++) {
    if (i == 5 || i == 6 || i == 9 || i == 10) {
      scintillator_log.push_back(
          new G4LogicalVolume(scintiShortJura1_box, hInfo.material,
              "scintillator_log", 0, 0, 0));
      internalPosition = G4ThreeVector(-overlapY1Y2/2 + 2. * hInfo.scintillatorSize[0] - scintiLengthJura_1, 0, -zDistanceY1Y2/2);
      k = (i < 7) ? 1 : 3;
    } else if (i == 7 || i == 8) {
      scintillator_log.push_back(
          new G4LogicalVolume(scintiShortJura2_box, hInfo.material,
              "scintillator_log", 0, 0, 0));
      internalPosition = G4ThreeVector(-overlapY1Y2/2 + 2. * hInfo.scintillatorSize[0] - scintiLengthJura_2, 0, -zDistanceY1Y2/2);
      k = 2;
    } else {
      scintillator_log.push_back(
          new G4LogicalVolume(scintiNormal_box, hInfo.material,
              "scintillator_log", 0, 0, 0));
      internalPosition = G4ThreeVector(-overlapY1Y2/2 + hInfo.scintillatorSize[0], 0, -zDistanceY1Y2/2);
      k = (i < 6) ? 0 : 4;
    }

    scintillator_log.back()->SetVisAttributes(colour->black);

    G4String physName = hInfo.tbName + "Y1" + "_ch" + intToStr(hInfo.nChannel + i);
    new G4PVPlacement(0, positionVector + localPosition.at(i) + internalPosition,
        scintillator_log.back(), physName, world_log, false, 0, checkOverlap);

    scintillator_log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(hInfo.tbName + "Y1_m", hInfo.nChannel + i, TGEANT::TRIGGER,
            detectorIdY1[k]));
  }
}

void T4HO04::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  // 5 entries for Y1, 4 for Y2
  T4SWireDetector wire;

  // for all the same:
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.angle = hInfo.angle;
  wire.zSize = 2. * hInfo.scintillatorSize[2];
  wire.unit = 1;
  wire.type = 41;
  wire.pitch = 2. * hInfo.scintillatorSize[1] - hInfo.overlap;

  wire.radLength = 1000.0; // radiation length
  wire.effic = 1.0; // detector efficiency
  wire.backgr = 0.0; // detector background
  wire.tgate = 20.0; // detector gate time
  wire.drVel = 1.0; // drift velocity
  wire.t0 = 800.0; // ???
  wire.res2hit = 2.0; // 2 hit resolution
  wire.space = 0.0; // space resolution
  wire.tslice = 0.1; // time slice

  // for all Y1 the same:
  wire.zCen = positionVector[2] - zDistanceY1Y2/2;
  wire.tbName = hInfo.tbName + "Y1_m";



  // Y1 outer bottom: H4A1
  wire.nWires = 5;
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
  wire.xCen = positionVector[0] - overlapY1Y2/2 + hInfo.scintillatorSize[0];
  wire.id = detectorIdY1[0];
  wire.det = "H4A1";
  wire.yCen = positionVector[1] + localPosition.at(2).getY();
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y1 middle bottom: H4A2
  wire.nWires = 2;
//  wire.xSize = 2. * scintiLengthJura_1;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
//  wire.xCen = positionVector[0] - overlapY1Y2/2 + 2. * hInfo.scintillatorSize[0] - scintiLengthJura_1;
  wire.id = detectorIdY1[1];
  wire.det = "H4A2";
  wire.yCen = positionVector[1] + (localPosition.at(5).getY() + localPosition.at(6).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
  wireDet.push_back(wire);

  // Y1 center: H4A3
  wire.nWires = 2;
//  wire.xSize = 2. * scintiLengthJura_2;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
//  wire.xCen = positionVector[0] - overlapY1Y2/2 + 2. * hInfo.scintillatorSize[0] - scintiLengthJura_2;
  wire.id = detectorIdY1[2];
  wire.det = "H4A3";
  wire.yCen = positionVector[1];
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y1 middle top: H4A4
  wire.nWires = 2;
//  wire.xSize = 2. * scintiLengthJura_1;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
//  wire.xCen = positionVector[0] - overlapY1Y2/2 + 2. * hInfo.scintillatorSize[0] - scintiLengthJura_1;
  wire.id = detectorIdY1[3];
  wire.det = "H4A4";
  wire.yCen = positionVector[1] + (localPosition.at(9).getY() + localPosition.at(10).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y1 outer top: H4A5
  wire.nWires = 5;
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
//  wire.xCen = positionVector[0] - overlapY1Y2/2 + hInfo.scintillatorSize[0];
  wire.id = detectorIdY1[4];
  wire.det = "H4A5";
  wire.yCen = positionVector[1] + localPosition.at(13).getY();
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);


  // for all Y2 the same:
  wire.zCen = positionVector[2] + zDistanceY1Y2/2;
  wire.tbName = hInfo.tbName + "Y2_m";

  // Y2 outer bottom: H4B1
  wire.nWires = 5;
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
  wire.xCen = positionVector[0] + overlapY1Y2/2 - hInfo.scintillatorSize[0];
  wire.id = detectorIdY2[0];
  wire.det = "H4B1";
  wire.yCen = positionVector[1] + localPosition.at(2).getY();
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y2 middle bottom: H4B2
  wire.nWires = 2;
//  wire.xSize = 2. * scintiLengthSaleve;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
//  wire.xCen = positionVector[0] + overlapY1Y2/2 - 2. * hInfo.scintillatorSize[0] + scintiLengthSaleve;
  wire.id = detectorIdY2[1];
  wire.det = "H4B2";
  wire.yCen = positionVector[1] + (localPosition.at(5).getY() + localPosition.at(6).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y2 middle: H4B3 (this is the dummy entry with xSize = 0 for 2012)
  wire.id = detectorIdY2[2];
  wire.det = "H4B3";
  wire.yCen = positionVector[1];
  if (yearSetup <= 2012 || yearSetup >= 2016)
    wire.xSize = 0;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y2 middle top: H4B4
  wire.id = detectorIdY2[3];
  wire.det = "H4B4";
  wire.xSize = 2. * hInfo.scintillatorSize[0]; // added for detectors.dat... continuity test
//  wire.xSize = 2. * scintiLengthSaleve;
  wire.yCen = positionVector[1] + (localPosition.at(9).getY() + localPosition.at(10).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // Y2 outer top: H4B5
  wire.nWires = 5;
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;
  wire.xCen = positionVector[0] + overlapY1Y2/2 - hInfo.scintillatorSize[0];
  wire.id = detectorIdY2[4];
  wire.det = "H4B5";
  wire.yCen = positionVector[1] + localPosition.at(13).getY();
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // dead zones
  T4SDeadZone deadY1, deadY2;
  deadY1.id = detectorIdY1[0];
  deadY2.id = detectorIdY2[0];
  deadY1.tbName = hInfo.tbName + "Y1_m";
  deadY2.tbName = hInfo.tbName + "Y2_m";
  deadY1.det = "H4A1";
  deadY2.det = "H4B1";
  deadY1.unit = deadY2.unit = 1;
  deadY1.rotMatrix = deadY2.rotMatrix = TGEANT::ROT_0DEG;
  deadY1.sh = deadY2.sh = 1;

  deadY1.xSize = 2. * hInfo.scintillatorSize[0] - 2. * scintiLengthJura_1;
  deadY2.xSize = 2. * hInfo.scintillatorSize[0] - 2. * scintiLengthSaleve;
  deadY1.ySize = deadY2.ySize = 2. * hInfo.scintillatorSize[1] * 6. - 7. * hInfo.overlap;
  deadY1.zSize = deadY2.zSize = 2. * hInfo.scintillatorSize[2];

  deadY1.xCen = positionVector[0] - overlapY1Y2/2 + deadY1.xSize / 2;
  deadY2.xCen = positionVector[0] + overlapY1Y2/2 - deadY2.xSize / 2;
  deadY1.yCen = deadY2.yCen = positionVector[1];
  deadY1.zCen = positionVector[2] - zDistanceY1Y2/2;
  deadY2.zCen = positionVector[2] + zDistanceY1Y2/2;

  deadZone.push_back(deadY1);
  deadZone.push_back(deadY2);
}
