#include "T4HO03.hh"
#include "T4SGlobals.hh"

T4HO03::T4HO03(T4SDetector* ho03)
{
  setPosition(ho03->position);
  hInfo.tbName = "HO03Y1_m";
  hInfo.angle = 90; // Y-plane
  hInfo.scintillatorSize[0] = 2500.0 / 2 * CLHEP::mm;
  hInfo.scintillatorSize[1] = 70.0 / 2 * CLHEP::mm;
  hInfo.scintillatorSize[2] = 9.0 / 2 * CLHEP::mm;
  hInfo.airGap = 1.7 * CLHEP::cm;
  hInfo.material = materials->bc408_noOptical;
  hInfo.nChannel = 18;
  hInfo.overlap = 0.5 * CLHEP::cm;
  hInfo.overlapShift = 0;

  setLocalPosition();

  detectorId[0] = 1033; // 6 channel, ch0-5
  detectorId[1] = 1039; // 2 channel, ch6-7
  detectorId[2] = 1041; // 2 channel, no TGEANT channel number for 2012, but for 2014
  detectorId[3] = 1043; // 2 channel, ch8-9
  detectorId[4] = 1045; // 6 channel, ch10-15
  
  scinti_box = NULL;
  plexi_box = NULL;
  plexi_log = NULL;
  plexiLong_box = NULL;
  plexiLong_log = NULL;

  yearSetup = strToDouble(T4EventManager::getInstance()->getTriggerPlugin()->getPluginName().substr(0,4));
  
  if (yearSetup >= 2016) {
    holeLength = 45.0 / 2 * CLHEP::cm;
    holeLengthLong = 65.0 / 2 * CLHEP::cm;
  } else {
    holeLength = 40.0 / 2 * CLHEP::cm;
  }
}

T4HO03::~T4HO03(void)
{
  if (scinti_box != NULL)
    delete scinti_box;
  if (plexi_box != NULL)
    delete plexi_box;
  if (plexi_log != NULL)
    delete plexi_log;
  if (plexiLong_box != NULL)
    delete plexiLong_box;
  if (plexiLong_log != NULL)
    delete plexiLong_log;
}

void T4HO03::construct(G4LogicalVolume* world_log)
{
  scinti_box = new G4Box("scinti_box", hInfo.scintillatorSize[0],
      hInfo.scintillatorSize[1], hInfo.scintillatorSize[2]);
  plexi_box = new G4Box("plexi_box", holeLength, hInfo.scintillatorSize[1],
      hInfo.scintillatorSize[2]);
  plexi_log = new G4LogicalVolume(plexi_box, materials->plexiglass_noOptical,
      "plexi_log", 0, 0, 0);
  plexi_log->SetVisAttributes(colour->white);
  
  if (yearSetup >= 2016) {
    plexiLong_box = new G4Box("plexiLong_box", holeLengthLong, hInfo.scintillatorSize[1],
        hInfo.scintillatorSize[2]);
    plexiLong_log = new G4LogicalVolume(plexiLong_box, materials->air_noOptical,
        "plexiLong_log", 0, 0, 0);
    plexiLong_log->SetVisAttributes(colour->white);
  }

  int channelNo = 0;
  int channelNo_newSlabs = 16; // new central scintillators in 2014, but with channel number 17 & 18
  bool newChannels;
  int k;

  for (int i = 0; i < hInfo.nChannel; i++) {

    newChannels = false;
    if (i < 6)
      k = 0;
    else if (i < 8)
      k = 1;
    else if (i < 10) {
      k = 2;
      newChannels = true;
      if (yearSetup <= 2012)
        continue; // no scintillators in middle
    } else if (i < 12)
      k = 3;
    else
      k = 4;
   
    int* thisChannel = newChannels ? &channelNo_newSlabs : &channelNo;

    scintillator_log.push_back(
        new G4LogicalVolume(scinti_box, hInfo.material, "scintillator_log", 0,
            0, 0));
    scintillator_log.back()->SetVisAttributes(colour->black);

    string physName = hInfo.tbName + "_ch" + intToStr((*thisChannel));
    new G4PVPlacement(0, positionVector + localPosition.at(i),
        scintillator_log.back(), physName, world_log, false, 0, checkOverlap);

    // plexiglass in the middle
    if (yearSetup < 2016) {
      if (i > 5 && i < 12)
	new G4PVPlacement(0, G4ThreeVector(0, 0, 0), plexi_log, "plexi_phys",
	    scintillator_log.back(), false, 0, checkOverlap);
    } else {
      if (i == 8 || i == 9)
	new G4PVPlacement(0, G4ThreeVector(23.1 * CLHEP::cm, 0, 0), plexiLong_log, "plexiLong_phys",
	    scintillator_log.back(), false, 0, checkOverlap);
      else if (i > 4 && i < 13)
	new G4PVPlacement(0, G4ThreeVector(13.1 * CLHEP::cm, 0, 0), plexi_log, "plexi_phys",
	    scintillator_log.back(), false, 0, checkOverlap);
    }
    
    scintillator_log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(hInfo.tbName, (*thisChannel), TGEANT::TRIGGER,
            detectorId[k]));
    (*thisChannel)++;
  }
}

void T4HO03::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;

  // for all the same:
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.angle = hInfo.angle;
  wire.xCen = positionVector[0];
  wire.zCen = positionVector[2];
  wire.tbName = hInfo.tbName;
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.zSize = 2. * hInfo.scintillatorSize[2];
  wire.unit = 1;
  wire.type = 41;
  wire.pitch = 2. * hInfo.scintillatorSize[1] - hInfo.overlap;;

  wire.radLength = 1000.0; // radiation length
  wire.effic = 1.0; // detector efficiency
  wire.backgr = 0.0; // detector background
  wire.tgate = 20.0; // detector gate time
  wire.drVel = 1.0; // drift velocity
  wire.t0 = 800.0; // ???
  wire.res2hit = 2.0; // 2 hit resolution
  wire.space = 0.0; // space resolution
  wire.tslice = 0.1; // time slice

  // outer bottom: H3H1
  wire.nWires = 6;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;;
  wire.id = detectorId[0];
  wire.det = "H3H1";
  wire.yCen = positionVector[1]
      + (localPosition.at(2).getY() + localPosition.at(3).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // middle bottom: H3H2
  wire.nWires = 2;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;;
  wire.id = detectorId[1];
  wire.det = "H3H2";
  wire.yCen = positionVector[1]
      + (localPosition.at(6).getY() + localPosition.at(7).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // middle: H3H3
  wire.nWires = 2;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;;
  wire.id = detectorId[2];
  wire.det = "H3H3";
  wire.yCen = positionVector[1];
  if (yearSetup <= 2012)
    wire.xSize = 0;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // middle top: H3H4
  wire.xSize = 2. * hInfo.scintillatorSize[0];
  wire.nWires = 2;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;;
  wire.id = detectorId[3];
  wire.det = "H3H4";
  wire.yCen = positionVector[1]
      + (localPosition.at(10).getY() + localPosition.at(11).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  // outer top: H3H5
  wire.nWires = 6;
  wire.ySize = 2. * hInfo.scintillatorSize[1] * wire.nWires - (wire.nWires - 1) * hInfo.overlap;;
  wire.id = detectorId[4];
  wire.det = "H3H5";
  wire.yCen = positionVector[1]
      + (localPosition.at(14).getY() + localPosition.at(15).getY()) / 2;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;
  wireDet.push_back(wire);

  T4SDeadZone dead;
  // one entry for the hole in the middle:
  dead.tbName = hInfo.tbName;
  dead.det = "H3H1";
  dead.id = detectorId[0];
  dead.unit = 1;
  dead.sh = 1;
  dead.xSize = 2. * holeLength;
  int nHole = 6;
  if (yearSetup >= 2016)
    nHole = 8;
  dead.ySize = 2. * hInfo.scintillatorSize[1] * nHole - hInfo.overlap * (nHole + 1);
  dead.zSize = 2. * hInfo.scintillatorSize[2];
  dead.xCen = positionVector[0];
  if (yearSetup >= 2016)
    dead.xCen += 13.1 * CLHEP::cm;
  dead.yCen = positionVector[1];
  dead.zCen = positionVector[2];
  dead.rotMatrix = TGEANT::ROT_0DEG;
  deadZone.push_back(dead);
}

