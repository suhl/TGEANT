#include "H2Construction.hh"

H2Construction::H2Construction(T4SDetectorRes* h2)
{
  setPosition(h2->general.position);
  useOptical = h2->useOptical;

  slabDimensions[0] = 252.5 / 2. * CLHEP::cm;
  slabDimensions[1] = 13.6 / 2. * CLHEP::cm;
  slabDimensions[2] = 2.0 / 2. * CLHEP::cm;

  lengthShort = 221.7 / 2. * CLHEP::cm;
  lengthBent = 177.5 / 2. * CLHEP::cm;
  lengthPlexi = 63.0 / 2. * CLHEP::cm;
  lengthLong = slabDimensions[0] + lengthPlexi;

  overlap = 5.0 / 2. * CLHEP::cm;
  overlapY = 2.0 / 2. * CLHEP::mm;
  overlapZ = 2.2 * CLHEP::cm;

  airGap = 0.1 * CLHEP::mm;
  aluFoil = 0.2 * CLHEP::mm;
  plasticFoil = 0.5 * CLHEP::mm;
  pmtRadius = 4.0 / 2. * CLHEP::cm; //diameter with housing 6.0 cm
  pmtLength = 30.5 / 2. * CLHEP::cm;
  lgLength = 40.5 / 2. * CLHEP::cm;

  checkOverlap = settingsFile->getStructManager()->getGeneral()->checkOverlap;

  name_Y1 = "HG02Y1__";
  name_Y2 = "HG02Y2__";
  detectorId_Y1 = 1860; // 32 channel
  detectorId_Y2 = 1892; // 32 channel

  plasticNormal_box = NULL;
  aluminiumNormal_box = NULL;
  airGapNormal_box = NULL;
  scintNormal_box = NULL;

  plasticShort_box = NULL;
  aluminiumShort_box = NULL;
  airGapShort_box = NULL;
  scintShort_box = NULL;

  plasticBent_box = NULL;
  aluminiumBent_box = NULL;
  airGapBent_box = NULL;
  scintBent_box = NULL;

  plasticLong_box = NULL;
  aluminiumLong_box = NULL;
  airGapLong_box = NULL;
  plexi_box = NULL;

  for (G4int i = 0; i < 64; i++) {
    plastic_log[i] = NULL;
    aluminium_log[i] = NULL;
    airGap_log[i] = NULL;
    scintillator_log[i] = NULL;
  }

  for (G4int i = 0; i < 4; i++) {
    plexi_log[i] = NULL;
  }

  rotationLG = NULL;
  rotationY1Bent = NULL;

  for (G4int i = 0; i < 32; i++) {
    lightGuideY1Jura[i] = NULL;
    lightGuideY2Saleve[i] = NULL;
    pmtY1Jura[i] = NULL;
    pmtY2Saleve[i] = NULL;
  }

  for (G4int i = 0; i < 26; i++) {
    lightGuideY1Saleve[i] = NULL;
    lightGuideY2Jura[i] = NULL;
    pmtY1Saleve[i] = NULL;
    pmtY2Jura[i] = NULL;
  }

  for (G4int i = 0; i < 6; i++) {
    lightGuideY1Bent[i] = NULL;
    lightGuideY2Bent[i] = NULL;
    pmtY1Bent[i] = NULL;
    pmtY2Bent[i] = NULL;
  }
}

H2Construction::~H2Construction(void)
{

  if (plasticNormal_box != NULL)
    delete plasticNormal_box;
  if (aluminiumNormal_box != NULL)
    delete aluminiumNormal_box;
  if (airGapNormal_box != NULL)
    delete airGapNormal_box;
  if (scintNormal_box != NULL)
    delete scintNormal_box;

  if (plasticShort_box != NULL)
    delete plasticShort_box;
  if (aluminiumShort_box != NULL)
    delete aluminiumShort_box;
  if (airGapShort_box != NULL)
    delete airGapShort_box;
  if (scintShort_box != NULL)
    delete scintShort_box;

  if (plasticBent_box != NULL)
    delete plasticBent_box;
  if (aluminiumBent_box != NULL)
    delete aluminiumBent_box;
  if (airGapBent_box != NULL)
    delete airGapBent_box;
  if (scintBent_box != NULL)
    delete scintBent_box;

  if (plasticLong_box != NULL)
    delete plasticLong_box;
  if (aluminiumLong_box != NULL)
    delete aluminiumLong_box;
  if (airGapLong_box != NULL)
    delete airGapLong_box;
  if (plexi_box != NULL)
    delete plexi_box;

  for (G4int i = 0; i < 64; i++) {
    if (plastic_log[i] != NULL)
      delete plastic_log[i];
    if (aluminium_log[i] != NULL)
      delete aluminium_log[i];
    if (airGap_log[i] != NULL)
      delete airGap_log[i];
    if (scintillator_log[i] != NULL)
      delete scintillator_log[i];
  }

  for (G4int i = 0; i < 4; i++) {
    if (plexi_log[i] != NULL)
      delete plexi_log[i];
  }

  if (rotationLG != NULL)
    delete rotationLG;
  if (rotationY1Bent != NULL)
    delete rotationY1Bent;

  for (G4int i = 0; i < 32; i++) {
    if (lightGuideY1Jura[i] != NULL)
      delete lightGuideY1Jura[i];
    if (lightGuideY2Saleve[i] != NULL)
      delete lightGuideY2Saleve[i];
    if (pmtY1Jura[i] != NULL)
      delete pmtY1Jura[i];
    if (pmtY2Saleve[i] != NULL)
      delete pmtY2Saleve[i];
  }

  for (G4int i = 0; i < 26; i++) {
    if (lightGuideY1Saleve[i] != NULL)
      delete lightGuideY1Saleve[i];
    if (lightGuideY2Jura[i] != NULL)
      delete lightGuideY2Jura[i];
    if (pmtY1Saleve[i] != NULL)
      delete pmtY1Saleve[i];
    if (pmtY2Jura[i] != NULL)
      delete pmtY2Jura[i];
  }

  for (G4int i = 0; i < 6; i++) {
    if (lightGuideY1Bent[i] != NULL)
      delete lightGuideY1Bent[i];
    if (lightGuideY2Bent[i] != NULL)
      delete lightGuideY2Bent[i];
    if (pmtY1Bent[i] != NULL)
      delete pmtY1Bent[i];
    if (pmtY2Bent[i] != NULL)
      delete pmtY2Bent[i];
  }
}

void H2Construction::construct(G4LogicalVolume* world_log)
{
  plasticNormal_box = new G4Box("plasticNormal_box", slabDimensions[0],
      slabDimensions[1] + airGap + aluFoil + plasticFoil,
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminiumNormal_box = new G4Box("aluminiumNormal_box", slabDimensions[0],
      slabDimensions[1] + airGap + aluFoil,
      slabDimensions[2] + airGap + aluFoil);
  airGapNormal_box = new G4Box("airGapNormal_box", slabDimensions[0],
      slabDimensions[1] + airGap, slabDimensions[2] + airGap);
  scintNormal_box = new G4Box("scintNormal_box", slabDimensions[0],
      slabDimensions[1], slabDimensions[2]);

  plasticShort_box = new G4Box("plasticShort_box", lengthShort,
      slabDimensions[1] + airGap + aluFoil + plasticFoil,
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminiumShort_box = new G4Box("aluminiumShort_box", lengthShort,
      slabDimensions[1] + airGap + aluFoil,
      slabDimensions[2] + airGap + aluFoil);
  airGapShort_box = new G4Box("airGapShort_box", lengthShort,
      slabDimensions[1] + airGap, slabDimensions[2] + airGap);
  scintShort_box = new G4Box("scintShort_box", lengthShort, slabDimensions[1],
      slabDimensions[2]);

  plasticBent_box = new G4Box("plasticBent_box", lengthBent,
      slabDimensions[1] + airGap + aluFoil + plasticFoil,
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminiumBent_box = new G4Box("aluminiumBent_box", lengthBent,
      slabDimensions[1] + airGap + aluFoil,
      slabDimensions[2] + airGap + aluFoil);
  airGapBent_box = new G4Box("airGapBent_box", lengthBent,
      slabDimensions[1] + airGap, slabDimensions[2] + airGap);
  scintBent_box = new G4Box("scintBent_box", lengthBent, slabDimensions[1],
      slabDimensions[2]);

  plasticLong_box = new G4Box("plasticLong_box", lengthLong,
      slabDimensions[1] + airGap + aluFoil + plasticFoil,
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminiumLong_box = new G4Box("aluminiumLong_box", lengthLong,
      slabDimensions[1] + airGap + aluFoil,
      slabDimensions[2] + airGap + aluFoil);
  airGapLong_box = new G4Box("airGapLong_box", lengthLong,
      slabDimensions[1] + airGap, slabDimensions[2] + airGap);
  plexi_box = new G4Box("plexi_box", lengthPlexi, slabDimensions[1],
      slabDimensions[2]);

  G4Material* scintMat;
  G4Material* alMat;
  G4Material* airMat;
  G4Material* plexiMat;
  if (useOptical) {
    scintMat = materials->bc408_optical;
    alMat = materials->aluminium_optical;
    airMat = materials->air_optical;
    plexiMat = materials->plexiglass_optical;
  } else {
    scintMat = materials->bc408_noOptical;
    alMat = materials->aluminium_noOptical;
    airMat = materials->air_noOptical;
    plexiMat = materials->plexiglass_noOptical;
  }

  // 2x32 slabs position loop
  // z position alternating each slab
  for (G4int i = 0; i < 64; i++) {
    if (i % 2 == 0 && i < 32) { // jura side, front, Y1
      slabPosition[i] = G4ThreeVector(slabDimensions[0] - overlap,
          (15.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[1] - overlapY),
          overlap + slabDimensions[2]);
    } else if (i % 2 == 0 && i > 31) { // saleve side, front, Y2
      slabPosition[i] = G4ThreeVector(-slabDimensions[0] + overlap,
          (15.5 - i + 32) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[1] - overlapY),
          -overlap - slabDimensions[2] - overlapZ);
    } else if (i % 2 != 0 && i < 32) { // jura side, back, Y1
      slabPosition[i] = G4ThreeVector(slabDimensions[0] - overlap,
          (15.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[1] - overlapY),
          overlap + slabDimensions[2] + overlapZ);
    } else { // saleve side, back, Y2
      slabPosition[i] = G4ThreeVector(-slabDimensions[0] + overlap,
          (15.5 - i + 32) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[1] - overlapY),
          -overlap - slabDimensions[2]);
    }
  }

  // construction loop
  for (G4int i = 0; i < 64; i++) {
    if (i == 0 || i == 32) { // short
      plastic_log[i] = new G4LogicalVolume(plasticShort_box,
          materials->polypropylene, "plastic_log");
      aluminium_log[i] = new G4LogicalVolume(aluminiumShort_box, alMat,
          "aluminium_log");
      airGap_log[i] = new G4LogicalVolume(airGapShort_box, airMat, "airGaplog");
      scintillator_log[i] = new G4LogicalVolume(scintShort_box, scintMat,
          "scintillator_log");
      if (i == 0) {
        slabPosition[i] += G4ThreeVector(-slabDimensions[0] + lengthShort, 0,
            0);
      } else {
        slabPosition[i] += G4ThreeVector(slabDimensions[0] - lengthShort, 0, 0);
      }
    } else if ((i > 0 && i < 12) || (i > 19 && i < 32) || (i > 32 && i < 44)
        || i > 51) { // normal
      plastic_log[i] = new G4LogicalVolume(plasticNormal_box,
          materials->polypropylene, "plastic_log");
      aluminium_log[i] = new G4LogicalVolume(aluminiumNormal_box, alMat,
          "aluminium_log");
      airGap_log[i] = new G4LogicalVolume(airGapNormal_box, airMat, "airGaplog");
      scintillator_log[i] = new G4LogicalVolume(scintNormal_box, scintMat,
          "scintillator_log");
    } else if ((i > 12 && i < 19) || (i > 44 && i < 51)) { // bent
      plastic_log[i] = new G4LogicalVolume(plasticBent_box,
          materials->polypropylene, "plastic_log");
      aluminium_log[i] = new G4LogicalVolume(aluminiumBent_box, alMat,
          "aluminium_log");
      airGap_log[i] = new G4LogicalVolume(airGapBent_box, airMat, "airGaplog");
      scintillator_log[i] = new G4LogicalVolume(scintBent_box, scintMat,
          "scintillator_log");
      if (i > 11 && i < 20) {
        slabPosition[i] += G4ThreeVector(slabDimensions[0] - lengthBent, 0, 0);
      } else {
        slabPosition[i] += G4ThreeVector(-slabDimensions[0] + lengthBent, 0, 0);
      }
    } else if (i == 12 || i == 19) { // long
      plastic_log[i] = new G4LogicalVolume(plasticLong_box,
          materials->polypropylene, "plastic_log");
      aluminium_log[i] = new G4LogicalVolume(aluminiumLong_box, alMat,
          "aluminium_log");
      airGap_log[i] = new G4LogicalVolume(airGapLong_box, airMat, "airGaplog");
      scintillator_log[i] = new G4LogicalVolume(scintNormal_box, scintMat,
          "scintillator_log");
      slabPosition[i] += G4ThreeVector(slabDimensions[0] - lengthLong, 0, 0);
      if (i == 12) {
        plexi_log[0] = new G4LogicalVolume(plexi_box, plexiMat, "plexi_log");
        plexi_log[1] = new G4LogicalVolume(plexi_box, plexiMat, "plexi_log");
      }
    } else if (i == 44 || i == 51) { // long
      plastic_log[i] = new G4LogicalVolume(plasticLong_box,
          materials->polypropylene, "plastic_log");
      aluminium_log[i] = new G4LogicalVolume(aluminiumLong_box, alMat,
          "aluminium_log");
      airGap_log[i] = new G4LogicalVolume(airGapLong_box, airMat, "airGaplog");
      scintillator_log[i] = new G4LogicalVolume(scintNormal_box, scintMat,
          "scintillator_log");
      slabPosition[i] += G4ThreeVector(-slabDimensions[0] + lengthLong, 0, 0);
      if (i == 44) {
        plexi_log[2] = new G4LogicalVolume(plexi_box, plexiMat, "plexi_log");
        plexi_log[3] = new G4LogicalVolume(plexi_box, plexiMat, "plexi_log");
      }
    } else {
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
          "H2Construction::construct: Unknown module.");
      continue;
    }

    string physName;
    if (i < 32)
      physName = name_Y2 + intToStr(31 - i);
    else
      physName = name_Y1 + intToStr(31 - (i - 32));
    new G4PVPlacement(0, positionVector + slabPosition[i], plastic_log[i],
        physName, world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), aluminium_log[i],
        "aluminium_phys", plastic_log[i], false, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log[i], "airGap_phys",
        aluminium_log[i], false, 0, checkOverlap);
    if (i == 12) {
      new G4PVPlacement(0, G4ThreeVector(lengthLong - slabDimensions[0], 0, 0),
          scintillator_log[i], "scintillator_phys", airGap_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(-lengthLong + lengthPlexi, 0, 0),
          plexi_log[0], "plexi_phys", airGap_log[i], false, 0, checkOverlap);
    } else if (i == 19) {
      new G4PVPlacement(0, G4ThreeVector(lengthLong - slabDimensions[0], 0, 0),
          scintillator_log[i], "scintillator_phys", airGap_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(-lengthLong + lengthPlexi, 0, 0),
          plexi_log[1], "plexi_phys", airGap_log[i], false, 0, checkOverlap);
    } else if (i == 44) {
      new G4PVPlacement(0, G4ThreeVector(-lengthLong + slabDimensions[0], 0, 0),
          scintillator_log[i], "scintillator_phys", airGap_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(lengthLong - lengthPlexi, 0, 0),
          plexi_log[2], "plexi_phys", airGap_log[i], false, 0, checkOverlap);
    } else if (i == 51) {
      new G4PVPlacement(0, G4ThreeVector(-lengthLong + slabDimensions[0], 0, 0),
          scintillator_log[i], "scintillator_phys", airGap_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(lengthLong - lengthPlexi, 0, 0),
          plexi_log[3], "plexi_phys", airGap_log[i], false, 0, checkOverlap);
    } else {
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), scintillator_log[i],
          "scintillator_phys", airGap_log[i], false, 0, checkOverlap);
    }

    plastic_log[i]->SetVisAttributes(colour->black);
    scintillator_log[i]->SetVisAttributes(colour->blue);
  }

  plexi_log[0]->SetVisAttributes(colour->lightblue);
  plexi_log[1]->SetVisAttributes(colour->lightblue);
  plexi_log[2]->SetVisAttributes(colour->lightblue);
  plexi_log[3]->SetVisAttributes(colour->lightblue);

  rotationLG = new CLHEP::HepRotation;
  rotationLG->rotateZ(90.0 * CLHEP::deg);
  rotationLG->rotateX(-90.0 * CLHEP::deg);

  rotationY1Bent = new CLHEP::HepRotation;
  rotationY1Bent->rotateZ(90.0 * CLHEP::deg);
  rotationY1Bent->rotateX(-90.0 * CLHEP::deg);
  rotationY1Bent->rotateZ(180.0 * CLHEP::deg);

  for (G4int i = 0; i < 32; i++) {
    lightGuideY1Jura[i] = new LGFishtail();
    if (i == 0) {
      lightGuideY1Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(2. * lengthShort - overlap + lgLength,
                  slabPosition[i].getY(), slabPosition[i].getZ() + lgLength));
    } else {
      lightGuideY1Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(2. * slabDimensions[0] - overlap + lgLength,
                  slabPosition[i].getY(), slabPosition[i].getZ() + lgLength));
    }
    lightGuideY1Jura[i]->setDimensions(slabDimensions[1], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideY1Jura[i]->setAlignment(TGEANT::UP);
    lightGuideY1Jura[i]->setRotation(*rotationLG);
    lightGuideY1Jura[i]->construct(world_log);

    lightGuideY2Saleve[i] = new LGFishtail();
    if (i == 0) {
      lightGuideY2Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(-2. * lengthShort + overlap - lgLength,
                  slabPosition[i + 32].getY(),
                  slabPosition[i + 32].getZ() - lgLength));
    } else {
      lightGuideY2Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(-2. * slabDimensions[0] + overlap - lgLength,
                  slabPosition[i + 32].getY(),
                  slabPosition[i + 32].getZ() - lgLength));
    }
    lightGuideY2Saleve[i]->setDimensions(slabDimensions[1], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideY2Saleve[i]->setAlignment(TGEANT::DOWN);
    lightGuideY2Saleve[i]->setRotation(*rotationLG);
    lightGuideY2Saleve[i]->construct(world_log);

    pmtY1Jura[i] = new T4PhotoMultiplier();
    if (i == 0) {
      pmtY1Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  2. * lengthShort - overlap + 2. * lgLength + pmtLength,
                  slabPosition[i].getY(), slabPosition[i].getZ()));
    } else {
      pmtY1Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  2. * slabDimensions[0] - overlap + 2. * lgLength + pmtLength,
                  slabPosition[i].getY(), slabPosition[i].getZ()));
    }
    pmtY1Jura[i]->setDimensions(pmtLength, pmtRadius);
    pmtY1Jura[i]->setAlignment(TGEANT::UP);
    pmtY1Jura[i]->setRotation(*rotationLG);
    pmtY1Jura[i]->setSensitiveDetector("HG02Y1_jura", i);
    pmtY1Jura[i]->construct(world_log);

    pmtY2Saleve[i] = new T4PhotoMultiplier();
    if (i == 0) {
      pmtY2Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  -2. * lengthShort + overlap - 2. * lgLength - pmtLength,
                  slabPosition[i + 32].getY(), slabPosition[i + 32].getZ()));
    } else {
      pmtY2Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  -2. * slabDimensions[0] + overlap - 2. * lgLength - pmtLength,
                  slabPosition[i + 32].getY(), slabPosition[i + 32].getZ()));
    }
    pmtY2Saleve[i]->setDimensions(pmtLength, pmtRadius);
    pmtY2Saleve[i]->setAlignment(TGEANT::DOWN);
    pmtY2Saleve[i]->setRotation(*rotationLG);
    pmtY2Saleve[i]->setSensitiveDetector("HG02Y2_saleve", i);
    pmtY2Saleve[i]->construct(world_log);

    // the channel number should increase from bottom to top, so we are using "31 - i"
    scintillator_log[i]->SetSensitiveDetector(
        new T4SensitiveDetector(name_Y1, 31 - i, TGEANT::TRIGGER,
            detectorId_Y1));

    scintillator_log[i + 32]->SetSensitiveDetector(
        new T4SensitiveDetector(name_Y2, 31 - i, TGEANT::TRIGGER,
            detectorId_Y2));
  }

  G4int skipBent = 0;
  for (G4int i = 0; i < 26; i++) {
    if (i == 13)
      skipBent = 6;
    lightGuideY2Jura[i] = new LGFishtail();
    if (i == 12 || i == 13) {
      lightGuideY2Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(overlap + 2. * lengthPlexi + lgLength,
                  slabPosition[i + 32 + skipBent].getY(),
                  slabPosition[i + 32 + skipBent].getZ() + lgLength));
    } else {
      lightGuideY2Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(overlap + lgLength,
                  slabPosition[i + 32 + skipBent].getY(),
                  slabPosition[i + 32 + skipBent].getZ() + lgLength));
    }
    lightGuideY2Jura[i]->setDimensions(slabDimensions[1], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideY2Jura[i]->setAlignment(TGEANT::UP);
    lightGuideY2Jura[i]->setRotation(*rotationLG);
    lightGuideY2Jura[i]->construct(world_log);

    lightGuideY1Saleve[i] = new LGFishtail();
    if (i == 12 || i == 13) {
      lightGuideY1Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(-overlap - 2. * lengthPlexi - lgLength,
                  slabPosition[i + skipBent].getY(),
                  slabPosition[i + skipBent].getZ() - lgLength));
    } else {
      lightGuideY1Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(-overlap - lgLength,
                  slabPosition[i + skipBent].getY(),
                  slabPosition[i + skipBent].getZ() - lgLength));
    }
    lightGuideY1Saleve[i]->setDimensions(slabDimensions[1], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideY1Saleve[i]->setAlignment(TGEANT::DOWN);
    lightGuideY1Saleve[i]->setRotation(*rotationLG);
    lightGuideY1Saleve[i]->construct(world_log);

    pmtY2Jura[i] = new T4PhotoMultiplier();
    if (i == 12 || i == 13) {
      pmtY2Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  overlap + 2. * lengthPlexi + 2. * lgLength + pmtLength,
                  slabPosition[i + 32 + skipBent].getY(),
                  slabPosition[i + 32 + skipBent].getZ()));
    } else {
      pmtY2Jura[i]->setPosition(
          positionVector
              + G4ThreeVector(overlap + 2. * lgLength + pmtLength,
                  slabPosition[i + 32 + skipBent].getY(),
                  slabPosition[i + 32 + skipBent].getZ()));
    }
    pmtY2Jura[i]->setDimensions(pmtLength, pmtRadius);
    pmtY2Jura[i]->setAlignment(TGEANT::UP);
    pmtY2Jura[i]->setRotation(*rotationLG);
    pmtY2Jura[i]->setSensitiveDetector("HG02Y2_jura", 31 - i - skipBent);
    pmtY2Jura[i]->construct(world_log);

    pmtY1Saleve[i] = new T4PhotoMultiplier();
    if (i == 12 || i == 13) {
      pmtY1Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(
                  -overlap - 2. * lengthPlexi - 2. * lgLength - pmtLength,
                  slabPosition[i + skipBent].getY(),
                  slabPosition[i + skipBent].getZ()));
    } else {
      pmtY1Saleve[i]->setPosition(
          positionVector
              + G4ThreeVector(-overlap - 2. * lgLength - pmtLength,
                  slabPosition[i + skipBent].getY(),
                  slabPosition[i + skipBent].getZ()));
    }
    pmtY1Saleve[i]->setDimensions(pmtLength, pmtRadius);
    pmtY1Saleve[i]->setAlignment(TGEANT::DOWN);
    pmtY1Saleve[i]->setRotation(*rotationLG);
    pmtY1Saleve[i]->setSensitiveDetector("HG02Y1_saleve", 31 - i - skipBent);
    pmtY1Saleve[i]->construct(world_log);
  }

  G4double angle = 180.0 * CLHEP::deg;
  G4double radius = 4.45 * CLHEP::cm;
  G4double lgHeight = 13.0 / 2. * CLHEP::cm;
  G4double avoidOverlap = 320. * CLHEP::micrometer;
  for (G4int i = 0; i < 6; i++) {
    lightGuideY1Bent[i] = new LightGuideRingA();
    lightGuideY1Bent[i]->setPosition(
        positionVector
            + G4ThreeVector(
                -overlap + 2. * slabDimensions[0] - 2. * lengthBent + radius
                    - slabDimensions[2], slabPosition[i + 13].getY(),
                slabPosition[i + 13].getZ() - radius + slabDimensions[2]));
    lightGuideY1Bent[i]->setRotation(*rotationY1Bent);
    lightGuideY1Bent[i]->setDimensions(lgHeight, slabDimensions[2], pmtRadius,
        lgLength, radius);
    lightGuideY1Bent[i]->setAngle(angle);
    lightGuideY1Bent[i]->setAlignment(TGEANT::DOWN);
    lightGuideY1Bent[i]->construct(world_log);

    lightGuideY2Bent[i] = new LightGuideRingA();
    lightGuideY2Bent[i]->setPosition(
        positionVector
            + G4ThreeVector(
                overlap - 2. * slabDimensions[0] + 2. * lengthBent - radius
                    + slabDimensions[2], slabPosition[i + 32 + 13].getY(),
                slabPosition[i + 32 + 13].getZ() + radius - slabDimensions[2]));
    lightGuideY2Bent[i]->setRotation(*rotationLG);
    lightGuideY2Bent[i]->setDimensions(lgHeight, slabDimensions[2], pmtRadius,
        lgLength, radius);
    lightGuideY2Bent[i]->setAngle(angle);
    lightGuideY2Bent[i]->setAlignment(TGEANT::UP);
    lightGuideY2Bent[i]->construct(world_log);

    pmtY1Bent[i] = new T4PhotoMultiplier();
    pmtY1Bent[i]->setPosition(
        positionVector
            + G4ThreeVector(
                -overlap + 2. * slabDimensions[0] - 2. * lengthBent
                    + 2 * lgLength + pmtLength + avoidOverlap, slabPosition[i + 13].getY(),
                slabPosition[i + 13].getZ() - 2. * radius
                    + 2. * slabDimensions[2]));
    pmtY1Bent[i]->setDimensions(pmtLength, pmtRadius);
    pmtY1Bent[i]->setAlignment(TGEANT::UP);
    pmtY1Bent[i]->setRotation(*rotationLG);
    pmtY1Bent[i]->setSensitiveDetector("HG02Y1_saleve", 31 - i - 13);
    pmtY1Bent[i]->construct(world_log);

    pmtY2Bent[i] = new T4PhotoMultiplier();
    pmtY2Bent[i]->setPosition(
        positionVector
            + G4ThreeVector(
                overlap - 2. * slabDimensions[0] + 2. * lengthBent
                    - 2. * lgLength - pmtLength - avoidOverlap,
                slabPosition[i + 32 + 13].getY(),
                slabPosition[i + 32 + 13].getZ() + 2. * radius
                    - 2. * slabDimensions[2]));
    pmtY2Bent[i]->setDimensions(pmtLength, pmtRadius);
    pmtY2Bent[i]->setAlignment(TGEANT::DOWN);
    pmtY2Bent[i]->setRotation(*rotationLG);
    pmtY2Bent[i]->setSensitiveDetector("HG02Y2_jura", 31 - i - 13);
    pmtY2Bent[i]->construct(world_log);
  }
}

void H2Construction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wireY1, wireY2;
  wireY1.id = detectorId_Y1;
  wireY2.id = detectorId_Y2;
  wireY1.tbName = name_Y1;
  wireY2.tbName = name_Y2;
  wireY1.det = "HGJ1";
  wireY2.det = "HGS1";
  wireY1.unit = wireY2.unit = 1;
  wireY1.type = wireY2.type = 41;
  wireY1.rotMatrix = wireY2.rotMatrix = TGEANT::ROT_0DEG;
  wireY1.angle = wireY2.angle = 90;
  wireY1.nWires = wireY2.nWires = 32;

  wireY1.xSize = wireY2.xSize = 2. * slabDimensions[0];
  wireY1.ySize = wireY2.ySize = 2. * slabDimensions[1] * 32;
  wireY1.zSize = wireY2.zSize = 2. * slabDimensions[2];

  wireY1.xCen = positionVector[0] + slabDimensions[0] - overlap;
  wireY2.xCen = positionVector[0] - slabDimensions[0] + overlap;

  wireY1.yCen = wireY2.yCen = positionVector[1];

  wireY1.zCen = positionVector[2] + 2. * overlap;
  wireY2.zCen = positionVector[2];

  wireY1.pitch = wireY2.pitch = 2
      * (plasticFoil + aluFoil + airGap + slabDimensions[1] - overlapY);
  wireY1.wireDist = wireY2.wireDist = -1.0 * (wireY1.nWires - 1) / 2.
      * wireY1.pitch;

  wireY1.radLength = wireY2.radLength = 1000.0; // radiation length
  wireY1.effic = wireY2.effic = 1.0; // detector efficiency
  wireY1.backgr = wireY2.backgr = 0.0; // detector background
  wireY1.tgate = wireY2.tgate = 20.0; // detector gate time
  wireY1.drVel = wireY2.drVel = 1.0; // drift velocity
  wireY1.t0 = wireY2.t0 = 800.0; // ???
  wireY1.res2hit = wireY2.res2hit = 2.0; // 2 hit resolution
  wireY1.space = wireY2.space = 0.0; // space resolution
  wireY1.tslice = wireY2.tslice = 0.1; // time slice

  wireDet.push_back(wireY1);
  wireDet.push_back(wireY2);

  T4SDeadZone deadY1, deadY2;
  deadY1.id = detectorId_Y1;
  deadY2.id = detectorId_Y2;
  deadY1.tbName = name_Y1;
  deadY2.tbName = name_Y2;
  deadY1.det = wireY1.det;
  deadY2.det = wireY2.det;
  deadY1.unit = deadY2.unit = 1;
  deadY1.rotMatrix = deadY2.rotMatrix = TGEANT::ROT_0DEG;
  deadY1.sh = deadY2.sh = 1;

  deadY1.xSize = deadY2.xSize = 2. * slabDimensions[0] - 2. * lengthBent;
  deadY1.ySize = deadY2.ySize = 2. * slabDimensions[1] * 6.;
  deadY1.zSize = deadY2.zSize = 2. * slabDimensions[2];

  deadY1.xCen = wireY1.xCen - wireY1.xSize / 2. + deadY1.xSize / 2.;
  deadY2.xCen = wireY2.xCen + wireY2.xSize / 2. - deadY2.xSize / 2.;

  deadY1.yCen = deadY2.yCen = positionVector[1];

  deadY1.zCen = wireY1.zCen;
  deadY2.zCen = wireY2.zCen;

  deadZone.push_back(deadY1);
  deadZone.push_back(deadY2);
}
