#include "H1Construction.hh"

H1Construction::H1Construction(T4SDetectorRes* h1)
{
  setPosition(h1->general.position);
  useMechanicalStructure = h1->general.useMechanicalStructure;

  useOptical = h1->useOptical;

  detectorId[0] = 1820; // 7 channel
  detectorId[1] = 1827; // 6 channel
  detectorId[2] = 1833; // 6 channel
  detectorId[3] = 1839; // 6 channel
  detectorId[4] = 1845; // 7 channel
  tbName = "HG01Y1__";
  detName[0] = "HG11";
  detName[1] = "HG12";
  detName[2] = "HG13";
  detName[3] = "HG14";
  detName[4] = "HG15";

  // for inner 4 slabs (14-17) use scintillator with hole
  // changed: (30.01.15) (13-18)
  holeIndex[0] = 13;
  holeIndex[1] = 18;

  // ======================
  // ATTENTION: THIS IS A HOTFIX!!
  // coral has a problem with subdetectors with different z-positions (keyword: continuity test)
  // so we put (hopefully temporary) all subplanes on the same z-Position
  zPositionShift = 0.0; // hotfix value
  // comment out:
  // zPositionShift = 1.0 * CLHEP::cm; // offset between the z position of the 5 groups
  // ======================

  // offset between geometrical center of H1 to middle of hole
  // changed: (30.01.15) from 10cm (paper) to 0cm (rd det.dat)
  // PLEASE NOTE: The implemented version of this geometry is the new one, see:
  // https://wwwcompass.cern.ch/compass/notes/2013-6/2013-6.pdf
  xAxisOffset = 0.0 * CLHEP::cm;
  
  slabDimensions[0] = 230.0 / 2 * CLHEP::cm;
  slabDimensions[1] = 6.0 / 2 * CLHEP::cm;
  slabDimensions[2] = 1.0 / 2 * CLHEP::cm;
  lengthHole = 50.0 / 2 * CLHEP::cm;

  lengthJura = (slabDimensions[0] - lengthHole - xAxisOffset) / 2.;
  lengthSaleve = (slabDimensions[0] - lengthHole + xAxisOffset) / 2.;
  
  airGap = 0.1 * CLHEP::mm;
  aluFoil = 0.2 * CLHEP::mm;
  plasticFoil = 0.5 * CLHEP::mm;
  pmtRadius = 2.85 / 2 * CLHEP::cm; //diameter with housing 4.85 cm
  pmtLength = 30.5 / 2 * CLHEP::cm;
  lgLength = 40.5 / 2 * CLHEP::cm;

  plastic_box = NULL;
  aluminium_box = NULL;
  airGap_box = NULL;
  scintillator_box = NULL;
  holeLG_box = NULL;
  holeLG_box2 = NULL;
  scintillator_sub = NULL;
  rotationLG = NULL;

  for (G4int i = 0; i < 32; i++) {
    plastic_log[i] = NULL;
    aluminium_log[i] = NULL;
    airGap_log[i] = NULL;
    scintillator_log[i] = NULL;
    lightGuideJura[i] = NULL;
    lightGuideSaleve[i] = NULL;
    pmtJura[i] = NULL;
    pmtSaleve[i] = NULL;
  }

  for (G4int i = 0; i < 2; i++) {
    boschY_box[i] = NULL;
    boschY_log[i] = NULL;
  }

  for (G4int i = 0; i < 6; i++) {
    holeLG_log[i] = NULL;
  }

  for (G4int i = 0; i < 5; i++) {
    boschX_box[i] = NULL;
    boschX_log[i] = NULL;
  }

  boschZ_box = NULL;
  boschZ_log = NULL;
}

H1Construction::~H1Construction(void)
{

  delete plastic_box;
  delete aluminium_box;
  delete airGap_box;
  delete scintillator_box;
  delete holeLG_box;
  delete holeLG_box2;
  delete scintillator_sub;

  for (G4int i = 0; i < 32; i++) {
    delete plastic_log[i];
    delete aluminium_log[i];
    delete airGap_log[i];
    delete scintillator_log[i];
    delete lightGuideJura[i];
    delete lightGuideSaleve[i];
    delete pmtJura[i];
    delete pmtSaleve[i];
  }

  for (G4int i = 0; i < 6; i++) {
    delete holeLG_log[i];
  }

  if (useMechanicalStructure) {
    for (G4int i = 0; i < 5; i++) {
      delete boschX_box[i];
      delete boschX_log[i];
    }

    for (G4int i = 0; i < 2; i++) {
      delete boschY_box[i];
      delete boschY_log[i];
    }

    delete boschZ_box;
    delete boschZ_log;
  }
}

void H1Construction::construct(G4LogicalVolume* world_log)
{
  // long slabs
  plastic_box = new G4Box("plastic_box", slabDimensions[0], slabDimensions[1],
      slabDimensions[2]);
  aluminium_box = new G4Box("aluminium_box", slabDimensions[0],
      slabDimensions[1] - plasticFoil, slabDimensions[2] - plasticFoil);
  airGap_box = new G4Box("airGap_box", slabDimensions[0],
      slabDimensions[1] - plasticFoil - aluFoil,
      slabDimensions[2] - plasticFoil - aluFoil);
  scintillator_box = new G4Box("scintillator_box", slabDimensions[0],
      slabDimensions[1] - plasticFoil - aluFoil - airGap,
      slabDimensions[2] - plasticFoil - aluFoil - airGap);
  // scintillator with hole
  holeLG_box = new G4Box("holeLG_box", lengthHole,
      slabDimensions[1] - plasticFoil - aluFoil - airGap,
      slabDimensions[2] - plasticFoil - aluFoil - airGap);
  holeLG_box2 = new G4Box("holeLG_box", lengthHole, slabDimensions[1],
      slabDimensions[2]); // a little bit bigger as scintillator_box in y and z direction
  scintillator_sub = new G4SubtractionSolid("scintillator_sub",
      scintillator_box, holeLG_box2, 0, G4ThreeVector(xAxisOffset, 0, 0));

  rotationLG = new CLHEP::HepRotation;
  rotationLG->rotateZ(90.0 * CLHEP::deg);
  rotationLG->rotateX(-90.0 * CLHEP::deg);

  G4Material* scintMat;
  G4Material* alMat;
  G4Material* airMat;
  if (useOptical) {
    scintMat = materials->bc408_optical;
    alMat = materials->aluminium_optical;
    airMat = materials->air_optical;
  } else {
    scintMat = materials->bc408_noOptical;
    alMat = materials->aluminium_noOptical;
    airMat = materials->air_noOptical;
  }

  G4int counter = 0;
  // we have 32 slabs
  for (G4int i = 0; i < 32; i++) {
    // set logical volumes, materials
    plastic_log[i] = new G4LogicalVolume(plastic_box, materials->polypropylene,
        "plastic_log");
    aluminium_log[i] = new G4LogicalVolume(aluminium_box, alMat,
        "aluminium_log");
    airGap_log[i] = new G4LogicalVolume(airGap_box, airMat,
        "airGap_log");

    // for inner slabs use scintillator with hole
    if (i < holeIndex[0] || i > holeIndex[1]) {
      scintillator_log[i] = new G4LogicalVolume(scintillator_box,
          scintMat, "scintillator_log");
    } else {
      scintillator_log[i] = new G4LogicalVolume(scintillator_sub,
          scintMat, "scintillator_log");
      holeLG_log[counter] = new G4LogicalVolume(holeLG_box, airMat,
          "holeLG_log");
    }
    scintillator_log[i]->SetVisAttributes(colour->black);

    // 5 groups, 2x upstream (minus), 3x downstream (plus)
    G4double posZ;
    G4int groupNo;
    if (i < 7) { // group 0: (7 slabs): 0-6
      posZ = zPositionShift;
      groupNo = 0;
    } else if (i < 13) { // group 1: (6 slabs): 7-12
      posZ = -zPositionShift;
      groupNo = 1;
    } else if (i < 19) { // group 2: (6 slabs): 13-18
      posZ = zPositionShift;
      groupNo = 2;
    } else if (i < 25) { // group 3: (6 slabs): 19-24
      posZ = -zPositionShift;
      groupNo = 3;
    } else { // group 4: (7 slabs): 25-31
      posZ = zPositionShift;
      groupNo = 4;
    }

    // set position of all 32 slabs
    // group 0-4: top-bottom
    slabPosition[i] = positionVector
        + G4ThreeVector(0, (-15.5 + i) * 2. * slabDimensions[1], posZ);

    // place all objects: plastic, aluminium in plastic, then air and scintillator
    new G4PVPlacement(0, slabPosition[i], plastic_log[i], tbName + "ch" + intToStr(i),
        world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), aluminium_log[i],
        "aluminium_phys", plastic_log[i], false, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log[i], "airGap_phys",
        aluminium_log[i], false, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), scintillator_log[i],
        "scintillator_phys", airGap_log[i], false, 0, checkOverlap);

    // for the hole slabs we have to add an additional air volume
    if (i >= holeIndex[0] && i <= holeIndex[1]) {
      new G4PVPlacement(0, G4ThreeVector(xAxisOffset, 0, 0),
          holeLG_log[counter], "holeLG_phys", airGap_log[i], false, 0,
          checkOverlap);
      counter++;
    }

    // place 2x32 lightguides
    lightGuideJura[i] = new LGFishtail();
    lightGuideJura[i]->setPosition(
        slabPosition[i]
            + G4ThreeVector(
                xAxisOffset + lengthHole + 2. * lengthJura + lgLength, 0,
                lgLength));
    lightGuideJura[i]->setDimensions(
        slabDimensions[1] - airGap - aluFoil - plasticFoil,
        slabDimensions[2] - airGap - aluFoil - plasticFoil, pmtRadius, lgLength,
        airGap, aluFoil, plasticFoil);
    lightGuideJura[i]->setAlignment(TGEANT::UP);
    lightGuideJura[i]->setRotation(*rotationLG);
    lightGuideJura[i]->construct(world_log);

    lightGuideSaleve[i] = new LGFishtail();
    lightGuideSaleve[i]->setPosition(
        slabPosition[i]
            + G4ThreeVector(
                xAxisOffset - lengthHole - 2. * lengthSaleve - lgLength, 0,
                -lgLength));
    lightGuideSaleve[i]->setDimensions(
        slabDimensions[1] - airGap - aluFoil - plasticFoil,
        slabDimensions[2] - airGap - aluFoil - plasticFoil, pmtRadius, lgLength,
        airGap, aluFoil, plasticFoil);
    lightGuideSaleve[i]->setAlignment(TGEANT::DOWN);
    lightGuideSaleve[i]->setRotation(*rotationLG);
    lightGuideSaleve[i]->construct(world_log);

    // place 2x32 pmts
    pmtJura[i] = new T4PhotoMultiplier();
    pmtJura[i]->setPosition(
        slabPosition[i]
            + G4ThreeVector(
                xAxisOffset + lengthHole + 2. * lengthJura + 2. * lgLength
                    + pmtLength, 0, 0));
    pmtJura[i]->setDimensions(pmtLength, pmtRadius);
    pmtJura[i]->setAlignment(TGEANT::UP);
    pmtJura[i]->setRotation(*rotationLG);
    pmtJura[i]->setSensitiveDetector("H1_jura", 31 - i);
    pmtJura[i]->construct(world_log);

    pmtSaleve[i] = new T4PhotoMultiplier();
    pmtSaleve[i]->setPosition(
        slabPosition[i]
            + G4ThreeVector(
                xAxisOffset - lengthHole - 2. * lengthSaleve - 2. * lgLength
                    - pmtLength, 0, 0));
    pmtSaleve[i]->setDimensions(pmtLength, pmtRadius);
    pmtSaleve[i]->setAlignment(TGEANT::DOWN);
    pmtSaleve[i]->setRotation(*rotationLG);
    pmtJura[i]->setSensitiveDetector("H1_saleve", 31 - i);
    pmtSaleve[i]->construct(world_log);

    // create 32 sensitive detectors
    scintillator_log[i]->SetSensitiveDetector(
        new T4SensitiveDetector(tbName, i, TGEANT::TRIGGER, detectorId[groupNo]));
  }

  if (useMechanicalStructure)
    constructMechanicalStructure(world_log);
}

void H1Construction::constructMechanicalStructure(G4LogicalVolume* world_log)
{
  G4double thicknessBosch = 9.0 / 2 * CLHEP::cm;
  G4double xLengthLong = 382.0 / 2 * CLHEP::cm;
  G4double xLengthShort = 357.0 / 2 * CLHEP::cm;
  G4double yLengthLong = 250.0 / 2 * CLHEP::cm;
  G4double yThickness = 16.0 / 2 * CLHEP::cm;
  G4double zLengthLong = 150.0 / 2 * CLHEP::cm;
  G4double zLengthShort = 2.25 * thicknessBosch;

  boschX_box[0] = new G4Box("boschX_box", xLengthShort, thicknessBosch,
      thicknessBosch);
  boschX_box[1] = new G4Box("boschX_box", xLengthLong, thicknessBosch,
      thicknessBosch);
  boschX_box[2] = new G4Box("boschX_box", xLengthLong + 2. * thicknessBosch,
      thicknessBosch, thicknessBosch);
  boschX_box[3] = new G4Box("boschX_box", xLengthShort, thicknessBosch / 2,
      thicknessBosch / 2);
  boschX_box[4] = new G4Box("boschX_box", xLengthShort - 2. * yThickness,
      thicknessBosch / 2, thicknessBosch / 2);

  boschY_box[0] = new G4Box("boschY_box", thicknessBosch, yLengthLong,
      thicknessBosch);
  boschY_box[1] = new G4Box("boschY_box", yThickness, yLengthLong,
      thicknessBosch / 2);

  boschZ_box = new G4Box("boschZ_box", thicknessBosch, thicknessBosch / 2,
      zLengthShort);

  for (G4int i = 0; i < 5; i++) {
    boschX_log[i] = new G4LogicalVolume(boschX_box[i], materials->aluminium_noOptical,
        "boschX_log");
    boschX_log[i]->SetVisAttributes(colour->gray);
  }

  for (G4int i = 0; i < 2; i++) {
    boschY_log[i] = new G4LogicalVolume(boschY_box[i], materials->aluminium_noOptical,
        "boschY_log");
    boschY_log[i]->SetVisAttributes(colour->gray);
  }

  boschZ_log = new G4LogicalVolume(boschZ_box, materials->aluminium_noOptical,
      "boschZ_log");
  boschZ_log->SetVisAttributes(colour->gray);

  G4double zPosBosch = pmtRadius + 1.1 * CLHEP::cm + zPositionShift + thicknessBosch / 2;

//  new G4PVPlacement(0,
//      positionVector
//          + G4ThreeVector(0, yLengthLong + 3 * thicknessBosch,
//              -0.5 * thicknessBosch + zPosBosch), boschX_log[0], "H1_boschX_phys[0]", world_log,
//      false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, -yLengthLong + thicknessBosch, 0),
      boschX_log[1], "H1_boschX_phys[1]", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, yLengthLong - thicknessBosch, -2. * thicknessBosch),
      boschX_log[2], "H1_boschX_phys[2]", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -yLengthLong + 2.5 * thicknessBosch,
              -4.5 * thicknessBosch + zPosBosch), boschX_log[3], "H1_boschX_phys[3]",
      world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, -yLengthLong + 2.5 * thicknessBosch,
              zPosBosch), boschX_log[4], "H1_boschX_phys[4]", world_log,
      false, 0, checkOverlap);

  new G4PVPlacement(0,
      positionVector + G4ThreeVector(xLengthLong + thicknessBosch, 0, 0),
      boschY_log[0], "H1_boschY_phys[0]", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(-xLengthLong - thicknessBosch, 0, 0),
      boschY_log[0], "H1_boschY_phys[1]", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(xLengthShort - yThickness, 2. * thicknessBosch,
              zPosBosch), boschY_log[1], "H1_boschY_phys[2]", world_log,
      false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-xLengthShort + yThickness, 2. * thicknessBosch,
              zPosBosch), boschY_log[1], "H1_boschY_phys[3]", world_log,
      false, 0, checkOverlap);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(xLengthShort - thicknessBosch,
              -yLengthLong + 3.5 * thicknessBosch, -2.75 * thicknessBosch + zPosBosch),
      boschZ_log, "H1_boschZ_phys[0]", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-xLengthShort + thicknessBosch,
              -yLengthLong + 3.5 * thicknessBosch, -2.75 * thicknessBosch + zPosBosch),
      boschZ_log, "H1_boschZ_phys[1]", world_log, false, 0, checkOverlap);
}

void H1Construction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  // 5 T4SWireDetector, one for each group
  T4SWireDetector wire[5];

  wire[0].nWires = 7;
  wire[1].nWires = 6;
  wire[2].nWires = 6;
  wire[3].nWires = 6;
  wire[4].nWires = 7;

  for (unsigned int i = 0; i < 5; i++) {
    wire[i].id = detectorId[i];
    wire[i].tbName = tbName;
    wire[i].det = detName[i];
    wire[i].unit = 1;
    wire[i].type = 41;
    wire[i].rotMatrix = TGEANT::ROT_0DEG;
    wire[i].angle = 90;

    wire[i].xSize = 2. * slabDimensions[0];
    wire[i].ySize = 2. * slabDimensions[1] * wire[i].nWires;
    wire[i].zSize = 2. * slabDimensions[2];

    wire[i].pitch = 2. * slabDimensions[1];
    wire[i].wireDist = -1.0 * (wire[i].nWires - 1) / 2 * wire[i].pitch;


    wire[i].xCen = positionVector[0];

    if (i == 0 || i == 2 || i == 4)
      wire[i].zCen = positionVector[2] + zPositionShift;
    else
      wire[i].zCen = positionVector[2] - zPositionShift;


    wire[i].radLength = 1000.0; // radiation length
    wire[i].effic = 1.0; // detector efficiency
    wire[i].backgr = 0.0; // detector background
    wire[i].tgate = 20.0; // detector gate time
    wire[i].drVel = 1.0; // drift velocity
    wire[i].t0 = 800.0; // ???
    wire[i].res2hit = 2.0; // 2 hit resolution
    wire[i].space = 0.0; // space resolution
    wire[i].tslice = 0.1; // time slice
  }

  // group 0-4: bottom-top
  wire[0].yCen = positionVector[1] - wire[2].ySize / 2 - wire[1].ySize
      - wire[0].ySize / 2;
  wire[1].yCen = positionVector[1] - wire[2].ySize / 2 - wire[1].ySize / 2;
  wire[2].yCen = positionVector[1];
  wire[3].yCen = positionVector[1] + wire[2].ySize / 2 + wire[1].ySize / 2;
  wire[4].yCen = positionVector[1] + wire[2].ySize / 2 + wire[1].ySize
      + wire[0].ySize / 2;

  for (unsigned int i = 0; i < 5; i++)
    wireDet.push_back(wire[i]);

  T4SDeadZone dead;
  dead.id = wire[0].id;
  dead.det = "HG11";
  dead.unit = 1;
  dead.tbName = wire[2].tbName;
  dead.rotMatrix = TGEANT::ROT_0DEG;
  dead.sh = 1;

  dead.xSize = 2. * lengthHole;
  dead.ySize = 2. * slabDimensions[1] * (holeIndex[1] - holeIndex[0] + 1);
  dead.zSize = 2. * slabDimensions[2];

  dead.xCen = wire[2].xCen + xAxisOffset;
  dead.yCen = wire[2].yCen;
  dead.zCen = wire[2].zCen;

  deadZone.push_back(dead);
}
