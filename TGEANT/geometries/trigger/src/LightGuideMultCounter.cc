#include "LightGuideMultCounter.hh"
#include "CameraConstruction.hh"

LightGuideMultCounter::LightGuideMultCounter(void)
{

  plastic_box = NULL;
  alu_box = NULL;
  airGap_box = NULL;
  lightguide_box = NULL;
  plastic_arc = NULL;
  alu_arc = NULL;
  airGap_arc = NULL;
  lightguide_arc = NULL;
  plastic_union2 = NULL;
  alu_union2 = NULL;
  lightguide_union2 = NULL;
  airGap_union2 = NULL;
  plastic_union3 = NULL;
  alu_union3 = NULL;
  lightguide_union3 = NULL;
  sci_x = 6.5 / 2 * CLHEP::cm;
  sci_y = 0.4 / 2 * CLHEP::cm;
  pmt_radius = 1.0 * CLHEP::cm;
  length = 86.0 / 2 * CLHEP::cm;
  lgRadius = 27.4 * CLHEP::cm;
  cylinder_Length = 55 / 2. * CLHEP::mm;
  alignment = TGEANT::DOWN;
  rotZ = 0;

  angle = 44.6 * CLHEP::deg;

  positionVector = G4ThreeVector(0, 0, 0);
}

LightGuideMultCounter::~LightGuideMultCounter(void) {
  if(plastic_box!=NULL)
    delete plastic_box;
  if(alu_box!=NULL)
    delete alu_box;
  if(airGap_box!=NULL)
    delete airGap_box;
  if(lightguide_box!=NULL)
    delete lightguide_box;
  if(plastic_arc!=NULL)
    delete plastic_arc;
  if(alu_arc!=NULL)
    delete alu_arc;
  if(airGap_arc!=NULL)
    delete airGap_arc;
  if(lightguide_arc!=NULL)
    delete lightguide_arc;
  if(plastic_union2!=NULL)
    delete plastic_union2;
  if(alu_union2!=NULL)
    delete alu_union2;
  if(airGap_union2!=NULL)
    delete airGap_union2;
  if(lightguide_union2!=NULL)
    delete lightguide_union2;
  if(plastic_union3!=NULL)
    delete plastic_union3;
  if(alu_union3!=NULL)
    delete alu_union3;
  if(airGap_union3!=NULL)
    delete airGap_union3;
  if(lightguide_union3!=NULL)
    delete lightguide_union3;
}

void LightGuideMultCounter::construct(G4LogicalVolume* world_log)
{
  G4double airGap = 0.1 * CLHEP::mm;
  G4double aluFoil = 0.02 * CLHEP::mm;
  G4double plasticFoil = 0.20 * CLHEP::mm;

  G4double dx[4] = { sci_x + airGap + aluFoil + plasticFoil,
                     sci_x + airGap + aluFoil,
                     sci_x + airGap,
                     sci_x };

  G4double dy[4] = { sci_y + airGap + aluFoil + plasticFoil,
                     sci_y + airGap + aluFoil,
                     sci_y + airGap,
                     sci_y };

  G4double dr[4] = { pmt_radius + airGap + aluFoil + plasticFoil,
                     pmt_radius + airGap + aluFoil,
                     pmt_radius + airGap,
                     pmt_radius };

  G4double outerRadius[4] = { 163 * CLHEP::mm + airGap + aluFoil + plasticFoil,
                              163 * CLHEP::mm + airGap + aluFoil,
                              163 * CLHEP::mm + airGap,
                              163 * CLHEP::mm};

  G4double innerRadius[4] = { 160 * CLHEP::mm - airGap - aluFoil - plasticFoil,
                              160 * CLHEP::mm - airGap - aluFoil,
                              160 * CLHEP::mm - airGap,
                              160 * CLHEP::mm};

  rotationMatrix.rotateY(90.0 * CLHEP::deg);
  rotationMatrix.rotateZ(90.0 * CLHEP::deg);
  if (alignment == TGEANT::UP)
    rotationMatrix.rotateX(180.0 * CLHEP::deg);
  rotationMatrix.rotateX(180 * CLHEP::deg);
  rotationMatrix.rotateZ(angle);

  avgRadius = (innerRadius[3] + outerRadius[3]) / 2.;
  double arcLength = 197.33 * CLHEP::mm;
  double chordLength = 193.99 * CLHEP::mm;
  box_length = (arcLength + chordLength) / 2. - avgRadius * angle;

  positionVector += G4ThreeVector(0, 0, avgRadius);

  lightguide_arc = new G4Tubs("lightguide_arc", innerRadius[3], outerRadius[3], dx[3], 0., angle);
  airGap_arc = new G4Tubs("airGap_arc", innerRadius[2], outerRadius[2], dx[2], 0., angle);
  alu_arc = new G4Tubs("alu_arc", innerRadius[1], outerRadius[1], dx[1], 0., angle);
  plastic_arc = new G4Tubs("plastic_arc", innerRadius[0], outerRadius[0], dx[0], 0., angle);

  lightguide_box = new G4Box("lightguide_box", dy[3], box_length / 2., dx[3]);
  airGap_box = new G4Box("airGap_box", dy[2], box_length / 2., dx[2]);
  alu_box = new G4Box("alu_box", dy[1], box_length / 2., dx[1]);
  plastic_box = new G4Box("plastic_box", dy[0], box_length / 2., dx[0]);

  G4ThreeVector posBox = G4ThreeVector(avgRadius, -box_length / 2, 0);
  lightguide_union = new G4UnionSolid("lightguide_union", lightguide_arc, lightguide_box, 0, posBox);
  airGap_union = new G4UnionSolid("airGap_union", airGap_arc, airGap_box, 0, posBox);
  alu_union = new G4UnionSolid("alu_union", alu_arc, alu_box, 0, posBox);
  plastic_union = new G4UnionSolid("plastic_union", plastic_arc, plastic_box, 0, posBox);

  lightguide_trd = new G4Trd("lightguide_trd", dr[3], dx[3], dr[3], dy[3], length);
  airGap_trd = new G4Trd("plastic_trd", dr[2], dx[2], dr[2], dy[2], length);
  alu_trd = new G4Trd("plastic_trd", dr[1], dx[1], dr[1], dy[1], length);
  plastic_trd = new G4Trd("plastic_trd", dr[0], dx[0], dr[0], dy[0], length);

  lightguide_cons = new G4Cons("lightguide_cons", 0, dr[3], 0,
      sqrt(dx[3] * dx[3] + dy[3] * dy[3]), length, 0., 2. * M_PI);
  airGap_cons = new G4Cons("airGap_cons", 0, dr[2], 0,
      sqrt(dx[2] * dx[2] + dy[2] * dy[2]), length, 0., 2. * M_PI);
  alu_cons = new G4Cons("alu_cons", 0, dr[1], 0,
      sqrt(dx[1] * dx[1] + dy[1] * dy[1]), length, 0., 2. * M_PI);
  plastic_cons = new G4Cons("plastic_cons", 0, dr[0], 0,
      sqrt(dx[0] * dx[0] + dy[0] * dy[0]), length, 0., 2. * M_PI);

  lightguide_intersection = new G4IntersectionSolid("lightguide_intersection", lightguide_cons, lightguide_trd);
  airGap_intersection = new G4IntersectionSolid("airGap_intersection", airGap_cons, airGap_trd);
  alu_intersection = new G4IntersectionSolid("alu_intersection", alu_cons, alu_trd);
  plastic_intersection = new G4IntersectionSolid("plastic_intersection", plastic_cons, plastic_trd);

  lightguide_tubs = new G4Tubs("lightguide_tubs", 0, dr[3], cylinder_Length, 0., 2. * M_PI);
  airGap_tubs = new G4Tubs("airGap_tubs", 0, dr[2], cylinder_Length, 0., 2. * M_PI);
  alu_tubs = new G4Tubs("alu_tubs", 0, dr[1], cylinder_Length, 0., 2. * M_PI);
  plastic_tubs = new G4Tubs("plastic_tubs", 0, dr[0], cylinder_Length, 0., 2. * M_PI);

  G4ThreeVector posTubs = G4ThreeVector(0, 0, -cylinder_Length - length);
  lightguide_union2 = new G4UnionSolid("lightguide_union", lightguide_intersection, lightguide_tubs, 0, posTubs);
  airGap_union2 = new G4UnionSolid("airGap_union", airGap_intersection, airGap_tubs, 0, posTubs);
  alu_union2 = new G4UnionSolid("alu_union", alu_intersection, alu_tubs, 0, posTubs);
  plastic_union2 = new G4UnionSolid("plastic_union", plastic_intersection, plastic_tubs, 0, posTubs);

  G4ThreeVector posCons = G4ThreeVector(avgRadius, -box_length - length, 0);
  rotationTubs = new CLHEP::HepRotation;
  rotationTubs->rotateY(90 * CLHEP::deg);
  rotationTubs->rotateX(90 * CLHEP::deg);
  lightguide_union3 = new G4UnionSolid("lightguide_union2", lightguide_union, lightguide_union2, rotationTubs, posCons);
  airGap_union3 = new G4UnionSolid("airGap_union2", airGap_union, airGap_union2, rotationTubs, posCons);
  alu_union3 = new G4UnionSolid("alu_union2", alu_union, alu_union2, rotationTubs, posCons);
  plastic_union3 = new G4UnionSolid("plastic_union2", plastic_union, plastic_union2, rotationTubs, posCons);

  G4Material* aluminiumMat;
  G4Material* airMat;
  G4Material* plexiglassMat;
  if (useOptical) {
    aluminiumMat = materials->aluminium_optical;
    airMat = materials->air_optical;
    plexiglassMat = materials->plexiglass_optical;
  } else {
    aluminiumMat = materials->aluminium_noOptical;
    airMat = materials->air_noOptical;
    plexiglassMat = materials->plexiglass_noOptical;
  }

  lightguide_log = new G4LogicalVolume(lightguide_union3, plexiglassMat, "lightguide_log");
  airGap_log = new G4LogicalVolume(airGap_union3, airMat, "airGap_log");
  alu_log = new G4LogicalVolume(alu_union3, aluminiumMat, "alu_log");
  plastic_log = new G4LogicalVolume(plastic_union3, materials->polypropylene, "plastic_log");

  plastic_phys = new G4PVPlacement(&rotationMatrix, positionVector, plastic_log,
      "plastic_phys", world_log, false, 0, false);
  alu_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), alu_log,
      "alu_phys", plastic_log, false, 0, checkOverlap);
  airGap_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log,
      "airGap_phys", alu_log, false, 0, false);
  lightguide_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), lightguide_log,
      "lightguide_phys", airGap_log, false, 0, false);

  plastic_log->SetVisAttributes(colour->orange);

  if (settingsFile->isOpticalPhysicsActivated()) {
    plexiAir = new G4LogicalBorderSurface("plexiAir", lightguide_phys,
        airGap_phys, materials->surfacePlexiglassAir);
    airPlexi = new G4LogicalBorderSurface("airPlexi", airGap_phys,
        lightguide_phys, materials->surfaceAirPlexiglass);
    airAl = new G4LogicalBorderSurface("airAl", airGap_phys, alu_phys,
        materials->surfaceAluminium);
  }
}

G4ThreeVector LightGuideMultCounter::getPmtPosition(G4double _length)
{
  if (plastic_trd == NULL)
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "LightGuideMultCounter::getPmtPosition: Can't return PMT position. LightGuideMultCounter is not constructed.");

  double alpha = box_length + 2 * (cylinder_Length+length) +_length;
  double beta = avgRadius;
  if(alignment==TGEANT::DOWN) {
    return positionVector
        + alpha * G4ThreeVector(0, -sin(90 * CLHEP::deg - angle), cos(90 * CLHEP::deg - angle)).rotateZ(-rotZ)
        + beta * G4ThreeVector(0, -sin(angle), -cos(angle)).rotateZ(-rotZ);
  }
  else
    return positionVector
        + alpha * G4ThreeVector(0, sin(90 * CLHEP::deg - angle), cos(90 * CLHEP::deg - angle)).rotateZ(-rotZ)
        + beta * G4ThreeVector(0, sin(angle), -cos(angle)).rotateZ(-rotZ);
}
