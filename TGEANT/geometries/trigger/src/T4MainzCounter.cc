#include "T4MainzCounter.hh"

T4MainzCounter02::T4MainzCounter02(T4SDetector* hh)
{
  setPosition(hh->position);
  hh02_box = NULL;
  hh02_log = NULL;
}

T4MainzCounter02::~T4MainzCounter02(void)
{
  if (hh02_box != NULL)
    delete hh02_box;
  if (hh02_log != NULL)
    delete hh02_log;
}

void T4MainzCounter02::construct(G4LogicalVolume* world_log)
{
//  hh02_box = new G4Box("hh02_box", 30. * CLHEP::cm, 30. * CLHEP::cm, 1. * CLHEP::cm);


//  bk_tubs = new G4Tubs("bk_tubs", 0., diameter / 2., thickness / 2., 0., 2. * M_PI);
//  bk_log = new G4LogicalVolume(bk_tubs, materials->bc408_noOptical, "bk_log");
//  bk_log->SetVisAttributes(colour->black);
//  new G4PVPlacement(0, positionVector, bk_log, tbName, world_log, false, 0,
//      checkOverlap);
//
//  bk_log->SetSensitiveDetector(
//      new T4SensitiveDetector(tbName, 0, TGEANT::TRIGGER, detectorId));


//  PARVOL75 'HHH2'  1  'HALL'     119.    0.    0.   0  'BOX '  3    1.  30.   30.

}

void T4MainzCounter02::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
//  det 1835  HH02R1__  H_21  1   42  1000.00   0.300   6.547  29.376    119.0000     0.0000     0.0000     13        0.0000    0.000    12  30.000002 1.000   0.0   99.0 1.00000   350.0  100.0   0.00  1.000


//  T4SWireDetector wire;
//  wire.id = detectorId;
//  wire.tbName = tbName;
//  wire.det = (string) "BK" + tbName[3] + "1";
//  wire.unit = 1;
//  wire.type = 41;
//  wire.xSize = wire.ySize = wire.pitch = diameter;
//  wire.zSize = thickness;
//  wire.xCen = positionVector.getX();
//  wire.yCen = positionVector.getY();
//  wire.zCen = positionVector.getZ();
//  wire.rotMatrix = TGEANT::ROT_0DEG;
//  wire.wireDist = wire.angle = 0;
//  wire.nWires = 1;
//  wireDet.push_back(wire);
}

T4MainzCounter03::T4MainzCounter03(T4SDetector* hh)
{
  setPosition(hh->position);

}

T4MainzCounter03::~T4MainzCounter03(void)
{

}

void T4MainzCounter03::construct(G4LogicalVolume* world_log)
{
//  PARVOL76 'HHH3'  2   'HALL'     3250.    15.5    0.   0  'BOX'  3    3.  50.   50.
//  PARVOL77 'HHR1'  36  'HHH3'     0.    0.    41.   0  'BOX'  3    1.5  42.5   1.5
//  PARVOL78 'HHR2'  36  'HHH3'     0.    41.    0.   0  'BOX'  3    1.5  1.5   39.5
//  PARVOL79 'HHR3'  36  'HHH3'     0.    -41.   0.   0  'BOX'  3    1.5  1.5   39.5
//  PARVOL80 'HHR4'  36  'HHH3'     0.    0.   -41.   0  'BOX'  3    1.5  42.5   1.5
//  C
//  PARVOL81 'HHR5' 36 'HHH3' 0. -29. -29. 65 'TRD2' 5 1.5 1.5 13.44 16.26 1.5
//  PARVOL88 'HHR6' 36 'HHH3' 0.  29. -29. 78 'TRD2' 5 1.5 1.5 13.44 16.26 1.5
//  PARVOL86 'HHR7' 36 'HHH3' 0. -29.  29. 79 'TRD2' 5 1.5 1.5 13.44 16.26 1.5
//  PARVOL87 'HHR8' 36 'HHH3' 0.  29.  29. 80 'TRD2' 5 1.5 1.5 13.44 16.26 1.5
}

void T4MainzCounter03::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{

}

