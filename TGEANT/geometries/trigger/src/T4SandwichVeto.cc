#include "T4SandwichVeto.hh"

T4SandwichVeto::T4SandwichVeto(T4SDetector* det)
{
  setPosition(det->position);

  moduleSize[0] = 80.08 / 2. * CLHEP::cm;
  moduleSize[1] = 40.01 / 2. * CLHEP::cm;
  moduleSize[2] = 8.95 / 2. * CLHEP::cm;

  double posX_tmp[12] = {60.55, 60.55, 60.55, 60.55, 60.55, 0, 0, -60.55, -60.55, -60.55, -60.55, -60.55};
  double posY_tmp[12] = {-80.4, -40.2, 0, 40.2, 80.4, -60.55, 60.55, -80.4, -40.2, 0, 40.2, 80.4};

  for (int i = 0; i < 12; i++) {
    posX[i] = posX_tmp[i] * CLHEP::cm;
    posY[i] = posY_tmp[i] * CLHEP::cm;
    tbName[i] = (string) "HH" + intToStr(static_cast<int>((i+1)>=10)) + intToStr((i+1)%10) + "X1__";
    detectorId[i] = 1790 + i;
  }

  double x = moduleSize[0];
  double y = moduleSize[1];
  for (int i = 0; i < 2; i++) {
    box_szinti[i] = new G4Box("box_szinti", x, y, moduleSize[2]);
    box_pb[i] = new G4Box("box_pb", x, y, 1. / 2. * CLHEP::mm);
    box_steel[i] = new G4Box("box_steel", x, y, 5. / 2. * CLHEP::mm);
    box_steel_end[i] = new G4Box("box_steel_end", x, y, 8. / 2. * CLHEP::mm);
    box_air_big[i] = new G4Box("box_air_big", x, y, 1.5 / 2. * CLHEP::mm);
    box_air_small[i] = new G4Box("box_air_small", x, y, 1. / 2. * CLHEP::mm);
    swap(x,y);
  }

  posZ_pb[0] = -moduleSize[2] + 1. / 2. * CLHEP::mm;
  posZ_pb[1] = posZ_pb[0] + 6. * CLHEP::mm;
  posZ_pb[2] = posZ_pb[1] + (1.+1.5+10.) * CLHEP::mm;
  posZ_pb[3] = posZ_pb[2] + 6. * CLHEP::mm;
  posZ_pb[4] = posZ_pb[3] + (1.+1.5+10.) * CLHEP::mm;
  posZ_pb[5] = posZ_pb[4] + 6. * CLHEP::mm;
  posZ_pb[6] = posZ_pb[5] + (1.+1.5+10.) * CLHEP::mm;
  posZ_pb[7] = posZ_pb[6] + 6. * CLHEP::mm;
  posZ_pb[8] = posZ_pb[7] + (1.+1.5+5.) * CLHEP::mm;
  posZ_pb[9] = posZ_pb[8] + 6. * CLHEP::mm;

  posZ_steel[0] = posZ_pb[0] + 3. * CLHEP::mm;
  posZ_steel[1] = posZ_pb[2] + 3. * CLHEP::mm;
  posZ_steel[2] = posZ_pb[4] + 3. * CLHEP::mm;
  posZ_steel[3] = posZ_pb[6] + 3. * CLHEP::mm;
  posZ_steel[4] = posZ_pb[8] + 3. * CLHEP::mm;
  posZ_steel[5] = moduleSize[2] - 4. * CLHEP::mm;

  posZ_airGap[0] = posZ_pb[1] + 1.25 * CLHEP::mm;
  posZ_airGap[1] = posZ_pb[2] - 1.25 * CLHEP::mm;
  posZ_airGap[2] = posZ_pb[3] + 1.25 * CLHEP::mm;
  posZ_airGap[3] = posZ_pb[4] - 1.25 * CLHEP::mm;
  posZ_airGap[4] = posZ_pb[5] + 1.25 * CLHEP::mm;
  posZ_airGap[5] = posZ_pb[6] - 1.25 * CLHEP::mm;
  posZ_airGap[6] = posZ_pb[7] + 1. * CLHEP::mm;
  posZ_airGap[7] = posZ_pb[8] - 1. * CLHEP::mm;
  posZ_airGap[8] = posZ_pb[9] + 1. * CLHEP::mm;
  posZ_airGap[9] = moduleSize[2] - 8.5 * CLHEP::mm;

  hole_pgon = NULL;
}

T4SandwichVeto::~T4SandwichVeto(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  if (hole_pgon != NULL)
    delete hole_pgon;
}

void T4SandwichVeto::construct(G4LogicalVolume* world_log)
{
  // steel mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 105.0 * CLHEP::cm, 105.0 * CLHEP::cm, 4.475 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->stainlessSteel, "mother_log"));
  new G4PVPlacement(0, positionVector, log.back(), "SWV_mother", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // hole polygon
  G4double zPlane[2] = { -4.475 * CLHEP::cm, 4.475 * CLHEP::cm };
  G4double rMin[2] = { 0, 0 };
  G4double rMax[2] = { 27.89 * CLHEP::cm, 29.09 * CLHEP::cm };
  hole_pgon = new G4Polyhedra("hole_pgon", 0., 2. * M_PI, 12, 2, zPlane, rMin, rMax);
  log.push_back(new G4LogicalVolume(hole_pgon,materials->air_noOptical,"hole_log"));
  new G4PVPlacement(0, G4ThreeVector(0., 0., 0), log.back(), "SWV_hole", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  for (int i = 0; i < 12; i++)
    buildModule(log.at(motherIndex), i);
}

void T4SandwichVeto::buildModule(G4LogicalVolume* mother_log, int i)
{
  int index = (i == 5 || i == 6);

  unsigned int szintiIndex = log.size();
  subtractAndPlace("szinti", box_szinti[index], G4ThreeVector(posX[i], posY[i], 0),
      materials->polystyrene, mother_log, colour->invisible, false);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(tbName[i], 0, TGEANT::TRIGGER, detectorId[i]));

  for (int j = 0; j < 10; j++) {
    subtractAndPlace("pb", box_pb[index], G4ThreeVector(posX[i], posY[i], posZ_pb[j]),
        materials->lead, log.at(szintiIndex), colour->black);
    if (j == 0) // visualization problems with subtraction (it looks like there is no hole but it is)
      log.back()->SetVisAttributes(colour->invisible);
  }

  for (int j = 0; j < 5; j++) {
    subtractAndPlace("steel", box_steel[index], G4ThreeVector(posX[i], posY[i], posZ_steel[j]),
        materials->stainlessSteel, log.at(szintiIndex), colour->gray);
  }
  subtractAndPlace("steel", box_steel_end[index], G4ThreeVector(posX[i], posY[i], posZ_steel[5]),
      materials->stainlessSteel, log.at(szintiIndex), colour->gray);

  for (int j = 0; j < 6; j++) {
    subtractAndPlace("airGap", box_air_big[index], G4ThreeVector(posX[i], posY[i], posZ_airGap[j]),
        materials->air_noOptical, log.at(szintiIndex), colour->invisible);
  }
  for (int j = 6; j < 10; j++) {
    subtractAndPlace("airGap", box_air_small[index], G4ThreeVector(posX[i], posY[i], posZ_airGap[j]),
        materials->air_noOptical, log.at(szintiIndex), colour->invisible);
  }
}

void T4SandwichVeto::subtractAndPlace(const G4String& pName, G4Box* solid,
    G4ThreeVector pos, G4Material* material, G4LogicalVolume* mother_log,
    const G4VisAttributes* vis, bool placeAtOrigin)
{
  box.push_back(new G4SubtractionSolid(pName + "_sub", solid, hole_pgon, 0, -pos));
  log.push_back(new G4LogicalVolume(box.back(), material, pName + "_log"));
  if (placeAtOrigin) {
    pos.setX(0);
    pos.setY(0);
  }
  new G4PVPlacement(0, pos, log.back(), "SWV_" + pName, mother_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(vis);
}

void T4SandwichVeto::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  for (int i = 0; i < 12; i++) {
    wire.id = detectorId[i];
    wire.tbName = tbName[i];
    wire.det = (string) "SW" + tbName[i].substr(2,2);
    wire.unit = 1;
    wire.type = 41;

    wire.xSize = wire.pitch = moduleSize[static_cast<int>(i == 5 || i == 6)] * 2.;
    wire.ySize = moduleSize[static_cast<int>(!(i == 5 || i == 6))] * 2.;
    wire.zSize = moduleSize[2] * 2.;

    wire.xCen = positionVector.getX() + posX[i];
    wire.yCen = positionVector.getY() + posY[i];
    wire.zCen = positionVector.getZ();

    wire.rotMatrix = TGEANT::ROT_0DEG;
    wire.wireDist = wire.angle = 0;
    wire.nWires = 1;
    wireDet.push_back(wire);
  }
}
