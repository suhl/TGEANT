#include "T4Veto.hh"

void T4Veto::construct(G4LogicalVolume* world_log)
{
  int detectorId = 1670;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getVeto()->size(); i++) {
    const T4SDetector* veto = &settingsFile->getStructManager()->getVeto()->at(i);

    vetoPlane.push_back(new T4VetoPlane(veto, detectorId));
    detectorId += 10;

    vetoPlane.back()->construct(world_log);
  }
}

T4VetoPlane::T4VetoPlane(const T4SDetector* _det, int _detectorId)
{
  det = _det;
  detectorId = _detectorId;

  if (det->name == "VI01P1__") {
    dimension[0] = 15. * CLHEP::cm;
    dimension[1] = 15. * CLHEP::cm;
    dimension[2] = 0.5 * CLHEP::cm;

    dimensionHole[0] = 0. * CLHEP::cm;
    dimensionHole[1] = 2. * CLHEP::cm;
    dimensionHole[2] = 0.5 * CLHEP::cm;

    isTube = true;
    detDat_detName = "VI11";
  } else if (det->name == "VO01X1__") {
    dimension[0] = 75. * CLHEP::cm;
    dimension[1] = 60. * CLHEP::cm;
    dimension[2] = 0.5 * CLHEP::cm;

    dimensionHole[0] = 15. * CLHEP::cm;
    dimensionHole[1] = 15. * CLHEP::cm;
    dimensionHole[2] = 0.5 * CLHEP::cm;

    isTube = false;
    detDat_detName = "VO11";
  } else if (det->name == "VI02X1__") {
    dimension[0] = 30. * CLHEP::cm;
    dimension[1] = 30. * CLHEP::cm;
    dimension[2] = 0.5 * CLHEP::cm;

    dimensionHole[0] = 0. * CLHEP::cm;
    dimensionHole[1] = 2. * CLHEP::cm;
    dimensionHole[2] = 0.5 * CLHEP::cm;

    isTube = true;
    detDat_detName = "VI21";
  }

  setPosition(det->position);
}

void T4VetoPlane::construct(G4LogicalVolume* world_log)
{
  // mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", dimension[0], dimension[1], dimension[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector, log.back(), det->name, world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->black);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::TRIGGER, detectorId));

  // hole
  if (isTube) {
    tubs.push_back(new G4Tubs("hole_tubs", dimensionHole[0], dimensionHole[1], dimensionHole[2], 0, 2.*M_PI));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "hole_log"));
  } else {
    box.push_back(new G4Box("hole_box", dimensionHole[0], dimensionHole[1], dimensionHole[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "hole_log"));
  }
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), det->name + "_hole", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);
}

void T4Veto::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < vetoPlane.size();i++)
    vetoPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4VetoPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  wire.id = detectorId;
  wire.tbName = det->name;
  wire.det = detDat_detName;
  wire.unit = 1;
  wire.type = 41;
  wire.xSize = dimension[0] * 2.;
  wire.ySize = dimension[1] * 2.;
  wire.zSize = dimension[2] * 2.;
  wire.xCen = positionVector[0];
  wire.yCen = positionVector[1];
  wire.zCen = positionVector[2];
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.wireDist = 0;
  wire.angle = 0;
  wire.nWires = 1;
  wire.pitch = dimension[0] * 2.;

  wireDet.push_back(wire);
}

T4Veto::~T4Veto(void)
{
  for (unsigned int i = 0; i < vetoPlane.size();i++)
    delete vetoPlane.at(i);
  vetoPlane.clear();
}

T4VetoPlane::~T4VetoPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}
