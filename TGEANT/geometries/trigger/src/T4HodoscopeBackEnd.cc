#include "T4HodoscopeBackEnd.hh"

T4HodoscopeBackEnd::T4HodoscopeBackEnd(T4HodoscopeInformation _hInfo)
{
  hInfo = _hInfo;
  scintillator_box = NULL;

  setLocalPosition();
}

T4HodoscopeBackEnd::~T4HodoscopeBackEnd(void)
{
  for (unsigned int i = 0; i < scintillator_log.size(); i++)
    delete scintillator_log.at(i);
  scintillator_log.clear();

  if (scintillator_box != NULL)
    delete scintillator_box;
}

void T4HodoscopeBackEnd::setLocalPosition(void)
{
  localPosition.clear();
  G4double xPos, yPos, zPos;
  G4int k;
  G4bool isX = (hInfo.angle == 0);
  for (G4int i = 0; i < hInfo.nChannel; i++) {
    k = (i % 2 == 0) ? -1 : 1; // first scintillator more upstream, second downstream...
    zPos = k * (hInfo.airGap / 2 + hInfo.scintillatorSize[2]);
    
    if (isX) { // channel number increasing from saleve (negative x values) to jura (positive x values)
      xPos = ((1. - hInfo.nChannel)/2 + i) * (2. * hInfo.scintillatorSize[0] - hInfo.overlap);
      if (i%2!=0) xPos += hInfo.overlapShift;
      yPos = 0;
 
    } else { // channel number increasing from bottom (negative y values) to top (positive y values)
      xPos = 0;
      yPos = ((1. - hInfo.nChannel)/2 + i) * (2. * hInfo.scintillatorSize[1] - hInfo.overlap);
      if (i%2!=0) yPos += hInfo.overlapShift;

    }

    localPosition.push_back(G4ThreeVector(xPos, yPos, zPos));
  }
  
}

void T4HodoscopeBackEnd::construct(G4LogicalVolume* world_log)
{
  if (hInfo.material == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4HodoscopeBackEnd::construct: Material of scintillators is not set up correctly. Return.");
    return;
  }

  if (hInfo.scintillatorSize[0] == 0 || hInfo.scintillatorSize[1] == 0 || hInfo.scintillatorSize[2] == 0) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4HodoscopeBackEnd::construct: Size of scintillator is 0. Return.");
    return;
  }

  scintillator_box = new G4Box("scintillator_box", hInfo.scintillatorSize[0], hInfo.scintillatorSize[1],
      hInfo.scintillatorSize[2]);

  for (G4int i = 0; i < hInfo.nChannel; i++) {
    scintillator_log.push_back(new G4LogicalVolume(scintillator_box, hInfo.material,
        "scintillator_log", 0, 0, 0));
    scintillator_log.back()->SetVisAttributes(colour->black);

    G4String physName = hInfo.tbName + "_ch" + intToStr(hInfo.firstChannelNo + i);
    new G4PVPlacement(0, positionVector + localPosition.at(i), scintillator_log.back(), physName,
            world_log, false, 0, checkOverlap);

    scintillator_log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(hInfo.tbName, hInfo.firstChannelNo + i,
            TGEANT::TRIGGER, hInfo.detectorId));
  }
}

void T4HodoscopeBackEnd::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;

  wire.nWires = hInfo.nChannel;
  wire.id = hInfo.detectorId;
  wire.tbName = hInfo.tbName;
  wire.det = hInfo.det;
  wire.unit = 1;
  wire.type = 41;
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.angle = hInfo.angle;

  if (hInfo.angle == 0) { // X-plane with vertical scintillators
    wire.xSize = 2. * hInfo.scintillatorSize[0] * hInfo.nChannel - (hInfo.nChannel - 1) * hInfo.overlap;
    wire.ySize = 2. * hInfo.scintillatorSize[1];
    wire.pitch = 2. * hInfo.scintillatorSize[0] - hInfo.overlap;
  } else {
    wire.xSize = 2. * hInfo.scintillatorSize[0];
    wire.ySize = 2. * hInfo.scintillatorSize[1] * hInfo.nChannel - (hInfo.nChannel - 1) * hInfo.overlap;
    wire.pitch = 2. * hInfo.scintillatorSize[1] - hInfo.overlap;
  }

  wire.zSize = 2. * hInfo.scintillatorSize[2];

  wire.xCen = positionVector[0];
  wire.yCen = positionVector[1];
  wire.zCen = positionVector[2];

  wire.wireDist = -1.0 * (wire.nWires - 1) / 2 * wire.pitch;

  wire.radLength = 1000.0; // radiation length
  wire.effic = 1.0; // detector efficiency
  wire.backgr = 0.0; // detector background
  wire.tgate = 20.0; // detector gate time
  wire.drVel = 1.0; // drift velocity
  wire.t0 = 800.0; // ???
  wire.res2hit = 2.0; // 2 hit resolution
  wire.space = 0.0; // space resolution
  wire.tslice = 0.1; // time slice

  wireDet.push_back(wire);
}
