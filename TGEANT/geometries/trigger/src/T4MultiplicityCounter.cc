#include "T4MultiplicityCounter.hh"

T4MultiplicityCounter::T4MultiplicityCounter(T4SDetector* det)
{
  setPosition(det->position);
  detectorId = 12345;
  tbName = "HM01P2__";

  scintillatorTrd = NULL;
  lg_up = NULL;
  lg_down = NULL;
  photoMultiplier_up = NULL;
  photoMultiplier_down = NULL;
}

T4MultiplicityCounter::~T4MultiplicityCounter(void)
{
  if (scintillatorTrd != NULL)
    delete scintillatorTrd;
  if (lg_up != NULL)
    delete lg_up;
  if (lg_down != NULL)
    delete lg_down;
  if (photoMultiplier_up != NULL)
    delete photoMultiplier_up;
  if (photoMultiplier_down != NULL)
    delete photoMultiplier_down;
}

void T4MultiplicityCounter::construct(G4LogicalVolume* world_log)
{
//  CLHEP::HepRotation* rotationMatrix = new CLHEP::HepRotation;
//  CLHEP::HepRotation* rotationMatrix2 = new CLHEP::HepRotation;
//  CLHEP::HepRotation* rotationMatrix3 = new CLHEP::HepRotation;
//  CLHEP::HepRotation* rotationMatrix4 = new CLHEP::HepRotation;
//  CLHEP::HepRotation* rotationMatrix5 = new CLHEP::HepRotation;
//  CLHEP::HepRotation* rotationMatrix6 = new CLHEP::HepRotation;
//
//  rotationMatrix2->rotateX(-90*CLHEP::deg);
//  rotationMatrix2->rotateY(360/12.*CLHEP::deg);
//  rotationMatrix3->rotateX(35*CLHEP::deg);
//  rotationMatrix4->rotateX(14*CLHEP::deg);
//  rotationMatrix5->rotateX(-35*CLHEP::deg);
//  rotationMatrix6->rotateX(-14*CLHEP::deg);
//
////  positionVector += G4ThreeVector(0,0, 62.745 * CLHEP::cm + 4.9121 * CLHEP::cm - (-59.+1.83)*CLHEP::mm);
//  G4Box *mother_box1 = new G4Box("mother_box1", 30.8 * CLHEP::mm, 59 * CLHEP::mm, 30.4 * CLHEP::cm);
//  G4Box *mother_box2 = new G4Box("mother_box2", 45.0 * CLHEP::mm, 140.0 * CLHEP::mm, 64.0 * CLHEP::cm);
//  G4Box *mother_box_sub = new G4Box("mother_box_sub", 50 * CLHEP::mm, 70.0 * CLHEP::mm, 190 * CLHEP::mm);
//  G4UnionSolid *mother_union = new G4UnionSolid("mother_union", mother_box1, mother_box2, 0, G4ThreeVector(0,199*CLHEP::mm,0));
//  G4SubtractionSolid * mothersub1 = new G4SubtractionSolid("mothersub1", mother_union, mother_box_sub, rotationMatrix3, G4ThreeVector(0, -99 *CLHEP::mm, 190*CLHEP::mm));
//  G4SubtractionSolid * mothersub2 = new G4SubtractionSolid("mothersub2", mothersub1, mother_box_sub, rotationMatrix4, G4ThreeVector(0, 4 *CLHEP::mm, 405*CLHEP::mm));
//  G4SubtractionSolid * mothersub3 = new G4SubtractionSolid("mothersub3", mothersub2, mother_box_sub, rotationMatrix5, G4ThreeVector(0, -99 *CLHEP::mm, -190*CLHEP::mm));
//  G4SubtractionSolid * mothersub4 = new G4SubtractionSolid("mothersub4", mothersub3, mother_box_sub, rotationMatrix6, G4ThreeVector(0, 4 *CLHEP::mm, -405*CLHEP::mm));
//  G4LogicalVolume* mother_log = new G4LogicalVolume(mothersub4, materials->air_noOptical, "mother_log");
//  new G4PVPlacement(rotationMatrix2, positionVector + G4ThreeVector(0,0,50*CLHEP::cm), mother_log, "MultCounter_air", world_log, 0, 0, checkOverlap);
//  mother_log->SetVisAttributes(colour->magenta);
//
//  G4ThreeVector localPositionVector(0,(-59.+1.83)*CLHEP::mm,0);

  rotationMatrix.rotateX(-90.*CLHEP::deg);
  double rot = 360./12.*CLHEP::deg;
  rotationMatrix.rotateY(rot);

  sci_x = 6.0 / 2 * CLHEP::cm;
  sci_y = 0.3 / 2 * CLHEP::cm;
  sci_z = sci_x;
  scintillatorTrd = new ScintillatorTrd();
  scintillatorTrd->setDimensions(sci_x, sci_x, sci_y, sci_z);
  scintillatorTrd->setPosition(positionVector);
  scintillatorTrd->setRotation(rotationMatrix);
  scintillatorTrd->setName(tbName);
  scintillatorTrd->setChannelNo(0);
  scintillatorTrd->setDetectorId(detectorId);
  scintillatorTrd->deactivateCap();
  scintillatorTrd->construct(world_log);

  double lgLength = 6.0 / 2 * CLHEP::cm;
  double pmtRadius = 2.5 * CLHEP::cm;
  G4double angle = 35.0 * CLHEP::deg;

  G4ThreeVector posLG = G4ThreeVector(sci_z * sin(rot), sci_z * cos(rot), 0);
  lg_up = new LightGuideMultCounter();
  lg_up->setPosition(positionVector + posLG);
  lg_up->setRotation(rotationMatrix);
  lg_up->setDimensions(sci_x, sci_y, pmtRadius, lgLength, 5.0 * CLHEP::cm);
  lg_up->setAngle(angle);
  lg_up->setRotationZ(rot);
  lg_up->setAlignment(TGEANT::UP);
  lg_up->construct(world_log);

  lg_down = new LightGuideMultCounter();
  lg_down->setPosition(positionVector - posLG);
  lg_down->setRotation(rotationMatrix);
  lg_down->setDimensions(sci_x, sci_y, pmtRadius, lgLength, 5.0 * CLHEP::cm);
  lg_down->setAngle(angle);
  lg_down->setRotationZ(rot);
  lg_down->setAlignment(TGEANT::DOWN);
  lg_down->construct(world_log);

  double pmtLength = 34.7 / 2 * CLHEP::cm;

  photoMultiplier_up = new T4PhotoMultiplier();
  photoMultiplier_up->setPosition(lg_up->getPmtPosition(pmtLength));
  photoMultiplier_up->setDimensions(pmtLength, pmtRadius);
  photoMultiplier_up->setAlignment(TGEANT::UP);
  photoMultiplier_up->rotateZ(rot);
  photoMultiplier_up->rotateX(-90 * CLHEP::deg - angle);
  photoMultiplier_up->construct(world_log);

  photoMultiplier_down = new T4PhotoMultiplier();
  photoMultiplier_down->setPosition(lg_down->getPmtPosition(pmtLength));
  photoMultiplier_down->setDimensions(pmtLength, pmtRadius);
  photoMultiplier_down->setAlignment(TGEANT::DOWN);
  photoMultiplier_down->rotateZ(rot);
  photoMultiplier_down->rotateX(-90 * CLHEP::deg + angle);
  photoMultiplier_down->construct(world_log);
}


void T4MultiplicityCounter::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  wire.id = detectorId;
  wire.tbName = tbName;
  wire.det = "HM01";
  wire.unit = 1;
  wire.type = 1;
  wire.xSize = wire.ySize = 2. * sci_x;
  wire.zSize = 2. * sci_y;
  wire.xCen = positionVector.getX();
  wire.yCen = positionVector.getY();
  wire.zCen = positionVector.getZ();
  wire.rotMatrix = TGEANT::ROT_30pDEG;
  wire.wireDist = 0;
  wire.angle = 0;
  wire.nWires = 1;
  wire.pitch = wire.xSize;
  wireDet.push_back(wire);
}

