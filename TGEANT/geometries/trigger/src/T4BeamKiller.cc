#include "T4BeamKiller.hh"

T4BeamKiller::T4BeamKiller(T4SDetector* hk)
{
  setPosition(hk->position);
  tbName = hk->name;

  if (tbName[3] == '1') {
    detectorId = 1675;
    diameter = 3.5 * CLHEP::cm;
    thickness = 0.3 * CLHEP::cm;
  } else if (tbName[3] == '2') {
    detectorId = 1685;
    diameter = 3.5 * CLHEP::cm;
    thickness = 0.5 * CLHEP::cm;
  } else if (tbName[3] == '3') {
    detectorId = 1695;
    diameter = 3.2 * CLHEP::cm;
    thickness = 0.3 * CLHEP::cm;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4BeamKiller:: Unknown BK TBname: " + hk->name);
  }

  bk_tubs = NULL;
  bk_log = NULL;
}

T4BeamKiller::~T4BeamKiller(void)
{
  if (bk_tubs != NULL)
    delete bk_tubs;
  if (bk_log != NULL)
    delete bk_log;
}

void T4BeamKiller::construct(G4LogicalVolume* world_log)
{
  bk_tubs = new G4Tubs("bk_tubs", 0., diameter / 2., thickness / 2., 0., 2. * M_PI);
  bk_log = new G4LogicalVolume(bk_tubs, materials->bc408_noOptical, "bk_log");
  bk_log->SetVisAttributes(colour->black);
  new G4PVPlacement(0, positionVector, bk_log, tbName, world_log, false, 0,
      checkOverlap);

  bk_log->SetSensitiveDetector(
      new T4SensitiveDetector(tbName, 0, TGEANT::TRIGGER, detectorId));
}

void T4BeamKiller::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  wire.id = detectorId;
  wire.tbName = tbName;
  wire.det = (string) "BK" + tbName[3] + "1";
  wire.unit = 1;
  wire.type = 41;
  wire.xSize = wire.ySize = wire.pitch = diameter;
  wire.zSize = thickness;
  wire.xCen = positionVector.getX();
  wire.yCen = positionVector.getY();
  wire.zCen = positionVector.getZ();
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.wireDist = wire.angle = 0;
  wire.nWires = 1;
  wireDet.push_back(wire);
}
