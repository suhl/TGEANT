#include "VetoConstruction.hh"

VetoConstruction::VetoConstruction(T4SDetectorRes* veto)
{
  setPosition(veto->general.position);
  useMechanicalStructure = veto->general.useMechanicalStructure;

  useOptical = veto->useOptical;

  slabDimensions[0] = 15.0 / 2. * CLHEP::cm;
  slabDimensions[1] = 125.0 / 2. * CLHEP::cm;
  slabDimensions[2] = 2.0 / 2. * CLHEP::cm;
  lengthShort = 50.0 / 2. * CLHEP::cm;
  lengthHole = slabDimensions[1] - 2. * lengthShort;

  airGap = 0.1 * CLHEP::mm;
  aluFoil = 0.2 * CLHEP::mm;
  plasticFoil = 0.5 * CLHEP::mm;
  pmtRadius = 2.0 / 2. * CLHEP::cm; //diameter with housing 4.0 cm
  pmtLength = 35.0 / 2. * CLHEP::cm;
  lgLength = 30.0 / 2. * CLHEP::cm;
  overlapX = 2.0 / 2. * CLHEP::mm;
  offsetZ = 4.0 / 2. * CLHEP::cm;

  for (int i = 0; i < 2; i++) {
    plastic_box[i] = NULL;
    aluminium_box[i] = NULL;
    airGap_box[i] = NULL;
    scintillator_box[i] = NULL;
    topBarX_box[i] = NULL;
    topBoschX_log[i] = NULL;
  }

  for (int i = 0; i < 18; i++) {
    lightGuideTop[i] = NULL;
    lightGuideBottom[i] = NULL;
    pmtTop[i] = NULL;
    pmtBottom[i] = NULL;
  }

  for (int i = 0; i < 20; i++) {
    plastic_log[i] = NULL;
    aluminium_log[i] = NULL;
    airGap_log[i] = NULL;
    scintillator_log[i] = NULL;
  }

  rotationLG = NULL;
  table_box = NULL;
  tableBarX_box = NULL;
  tableBarY_box = NULL;
  tableBarZ_box = NULL;
  table_log = NULL;
  tableBoschX_log = NULL;
  tableBoschY_log = NULL;
  tableBoschZ_log = NULL;
  topBarY_box = NULL;
  topBoschY_log = NULL;
  rotationAnglePiece = NULL;
  anglePiece_box = NULL;
  boxAnglePiece_box = NULL;
  anglePiece_intersection = NULL;
  anglePiece_log = NULL;
}

VetoConstruction::~VetoConstruction(void)
{
  for (int i = 0; i < 2; i++) {
    plastic_box[i] = NULL;
    aluminium_box[i] = NULL;
    airGap_box[i] = NULL;
    scintillator_box[i] = NULL;
    topBarX_box[i] = NULL;
    topBoschX_log[i] = NULL;
  }

  for (int i = 0; i < 18; i++) {
    lightGuideTop[i] = NULL;
    lightGuideBottom[i] = NULL;
    pmtTop[i] = NULL;
    pmtBottom[i] = NULL;
  }

  for (int i = 0; i < 20; i++) {
    plastic_log[i] = NULL;
    aluminium_log[i] = NULL;
    airGap_log[i] = NULL;
    scintillator_log[i] = NULL;
  }

  if (rotationLG != NULL)
    delete rotationLG;
  if (table_box != NULL)
    delete table_box;
  if (tableBarX_box != NULL)
    delete tableBarX_box;
  if (tableBarY_box != NULL)
    delete tableBarY_box;
  if (tableBarZ_box != NULL)
    delete tableBarZ_box;
  if (table_log != NULL)
    delete table_log;
  if (tableBoschX_log != NULL)
    delete tableBoschX_log;
  if (tableBoschY_log != NULL)
    delete tableBoschY_log;
  if (tableBoschZ_log != NULL)
    delete tableBoschZ_log;
  if (topBarY_box != NULL)
    delete topBarY_box;
  if (topBoschY_log != NULL)
    delete topBoschY_log;
  if (rotationAnglePiece != NULL)
    delete rotationAnglePiece;
  if (anglePiece_box != NULL)
    delete anglePiece_box;
  if (boxAnglePiece_box != NULL)
    delete boxAnglePiece_box;
  if (anglePiece_intersection != NULL)
    delete anglePiece_intersection;
  if (anglePiece_log != NULL)
    delete anglePiece_log;
}

void VetoConstruction::construct(G4LogicalVolume* world_log)
{
  plastic_box[0] = new G4Box("plastic_box",
      slabDimensions[0] + airGap + aluFoil + plasticFoil, slabDimensions[1],
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminium_box[0] = new G4Box("aluminium_box",
      slabDimensions[0] + airGap + aluFoil, slabDimensions[1],
      slabDimensions[2] + airGap + aluFoil);
  airGap_box[0] = new G4Box("airGap_box", slabDimensions[0] + airGap,
      slabDimensions[1], slabDimensions[2] + airGap);
  scintillator_box[0] = new G4Box("scintillator_box", slabDimensions[0],
      slabDimensions[1], slabDimensions[2]);

  plastic_box[1] = new G4Box("plastic_box",
      slabDimensions[0] + airGap + aluFoil + plasticFoil, lengthShort,
      slabDimensions[2] + airGap + aluFoil + plasticFoil);
  aluminium_box[1] = new G4Box("aluminium_box",
      slabDimensions[0] + airGap + aluFoil, lengthShort - plasticFoil / 2,
      slabDimensions[2] + airGap + aluFoil);
  airGap_box[1] = new G4Box("airGap_box", slabDimensions[0] + airGap,
      lengthShort - plasticFoil / 2 - aluFoil / 2, slabDimensions[2] + airGap);
  scintillator_box[1] = new G4Box("scintillator_box", slabDimensions[0],
      lengthShort - plasticFoil / 2 - aluFoil / 2 - airGap / 2,
      slabDimensions[2]);

  G4Material* scintMat;
  G4Material* alMat;
  G4Material* airMat;
  if (useOptical) {
    scintMat = materials->bc408_optical;
    alMat = materials->aluminium_optical;
    airMat = materials->air_optical;
  } else {
    scintMat = materials->bc408_noOptical;
    alMat = materials->aluminium_noOptical;
    airMat = materials->air_noOptical;
  }

  for (G4int i = 0; i < 20; i++) {
    G4int k = 0;
    if (i > 7 && i < 12)
      k = 1;
    plastic_log[i] = new G4LogicalVolume(plastic_box[k],
        materials->polypropylene, "plastic_log", 0, 0, 0);
    aluminium_log[i] = new G4LogicalVolume(aluminium_box[k],
        alMat, "aluminium_log", 0, 0, 0);
    airGap_log[i] = new G4LogicalVolume(airGap_box[k], airMat,
        "airGaplog", 0, 0, 0);
    scintillator_log[i] = new G4LogicalVolume(scintillator_box[k],
        scintMat, "scintillator_log", 0, 0, 0);

    plastic_log[i]->SetVisAttributes(colour->black);

    if (i < 8 && i % 2 == 0) {
      slabPosition[i] = G4ThreeVector(
          (8.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          0, offsetZ);
    } else if (i > 11 && i % 2 == 0) {
      slabPosition[i] = G4ThreeVector(
          (10.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          0, offsetZ);
    } else if (i < 8 && i % 2 != 0) {
      slabPosition[i] = G4ThreeVector(
          (8.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          0, -offsetZ);
    } else if (i > 11 && i % 2 != 0) {
      slabPosition[i] = G4ThreeVector(
          (10.5 - i) * 2
              * (plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          0, -offsetZ);
    } else if (i == 8) {
      slabPosition[i] = G4ThreeVector(
          plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX,
          slabDimensions[1] - lengthShort, offsetZ);
    } else if (i == 9) {
      slabPosition[i] = G4ThreeVector(
          plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX,
          -slabDimensions[1] + lengthShort, offsetZ);
    } else if (i == 10) {
      slabPosition[i] = G4ThreeVector(
          -(plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          slabDimensions[1] - lengthShort, -offsetZ);
    } else {
      slabPosition[i] = G4ThreeVector(
          -(plasticFoil + aluFoil + airGap + slabDimensions[0] - overlapX),
          -slabDimensions[1] + lengthShort, -offsetZ);
    }

    if (i == 8 || i == 10) {
      new G4PVPlacement(0, positionVector + slabPosition[i], plastic_log[i],
          "plastic_phys", world_log, false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, plasticFoil / 2, 0),
          aluminium_log[i], "aluminium_phys", plastic_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, aluFoil / 2, 0), airGap_log[i],
          "airGap_phys", aluminium_log[i], false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, airGap / 2, 0), scintillator_log[i],
          "scintillator_phys", airGap_log[i], false, 0, checkOverlap);
    } else if (i == 9 || i == 11) {
      new G4PVPlacement(0, positionVector + slabPosition[i], plastic_log[i],
          "plastic_phys", world_log, false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, -plasticFoil / 2, 0),
          aluminium_log[i], "aluminium_phys", plastic_log[i], false, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, -aluFoil / 2, 0), airGap_log[i],
          "airGap_phys", aluminium_log[i], false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, -airGap / 2, 0),
          scintillator_log[i], "scintillator_phys", airGap_log[i], false, 0,
          checkOverlap);
    } else {
      new G4PVPlacement(0, positionVector + slabPosition[i], plastic_log[i],
          "plastic_phys", world_log, false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), aluminium_log[i],
          "aluminium_phys", plastic_log[i], false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log[i], "airGap_phys",
          aluminium_log[i], false, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), scintillator_log[i],
          "scintillator_phys", airGap_log[i], false, 0, checkOverlap);
    }

    scintillator_log[i]->SetSensitiveDetector(
        new T4SensitiveDetector("Veto", i, TGEANT::TRIGGER));
  }

  rotationLG = new CLHEP::HepRotation;
  rotationLG->rotateX(-90.0 * CLHEP::deg);

  for (G4int i = 0; i < 18; i++) {
    G4int k = 0;
    if (i == 9)
      k = 1;
    if (i > 9)
      k = 2;

    lightGuideTop[i] = new LGFishtail();
    lightGuideTop[i]->setPosition(
        positionVector
            + G4ThreeVector(slabPosition[i + k].getX(),
                slabDimensions[1] + lgLength,
                slabPosition[i + k].getZ() + lgLength));
    lightGuideTop[i]->setDimensions(slabDimensions[0], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideTop[i]->setAlignment(TGEANT::UP);
    lightGuideTop[i]->setRotation(*rotationLG);
    lightGuideTop[i]->construct(world_log);

    lightGuideBottom[i] = new LGFishtail();
    lightGuideBottom[i]->setPosition(
        positionVector
            + G4ThreeVector(slabPosition[i + k].getX(),
                -slabDimensions[1] - lgLength,
                slabPosition[i + k].getZ() - lgLength));
    lightGuideBottom[i]->setDimensions(slabDimensions[0], slabDimensions[2],
        pmtRadius, lgLength);
    lightGuideBottom[i]->setAlignment(TGEANT::DOWN);
    lightGuideBottom[i]->setRotation(*rotationLG);
    lightGuideBottom[i]->construct(world_log);

    pmtTop[i] = new T4PhotoMultiplier();
    pmtTop[i]->setPosition(
        positionVector
            + G4ThreeVector(slabPosition[i + k].getX(),
                slabDimensions[1] + 2. * lgLength + pmtLength,
                slabPosition[i + k].getZ()));
    pmtTop[i]->setDimensions(pmtLength, pmtRadius);
    pmtTop[i]->setAlignment(TGEANT::UP);
    pmtTop[i]->setRotation(*rotationLG);
    pmtTop[i]->setSensitiveDetector("Veto_top", i);
    pmtTop[i]->construct(world_log);

    pmtBottom[i] = new T4PhotoMultiplier();
    pmtBottom[i]->setPosition(
        positionVector
            + G4ThreeVector(slabPosition[i + k].getX(),
                -slabDimensions[1] - 2. * lgLength - pmtLength,
                slabPosition[i + k].getZ()));
    pmtBottom[i]->setDimensions(pmtLength, pmtRadius);
    pmtBottom[i]->setAlignment(TGEANT::DOWN);
    pmtBottom[i]->setRotation(*rotationLG);
    pmtBottom[i]->setSensitiveDetector("Veto_bottom", i);
    pmtBottom[i]->construct(world_log);
  }

  if (useMechanicalStructure) {
    constructMechanicalStructure(world_log);
  }
}

void VetoConstruction::constructMechanicalStructure(G4LogicalVolume* world_log)
{
  G4double thicknessTableBar = 8.0 / 2 * CLHEP::cm;
  G4double xLengthTable = 184.0 / 2 * CLHEP::cm;
  G4double yLengthTable = 158.0 / 2 * CLHEP::cm;
  G4double zLengthTable = 62.0 / 2 * CLHEP::cm;
  G4double dimensionsPlate[3];
  dimensionsPlate[0] = 210.0 / 2 * CLHEP::cm;
  dimensionsPlate[1] = 2.0 / 2 * CLHEP::cm;
  dimensionsPlate[2] = 83.0 / 2 * CLHEP::cm;

  G4double yPosTopTable = -slabDimensions[1] - 2. * lgLength - 2. * pmtLength
      - 30.0 * CLHEP::cm;
  G4double zPosTopTable = -dimensionsPlate[2] + 9.0 * CLHEP::cm;

  table_box = new G4Box("table_box", dimensionsPlate[0], dimensionsPlate[1],
      dimensionsPlate[2]);
  tableBarX_box = new G4Box("barX_box", xLengthTable, thicknessTableBar,
      thicknessTableBar);
  tableBarY_box = new G4Box("barY_box", thicknessTableBar, yLengthTable,
      thicknessTableBar);
  tableBarZ_box = new G4Box("barZ_box", thicknessTableBar, thicknessTableBar,
      zLengthTable);

  table_log = new G4LogicalVolume(table_box, materials->aluminium_noOptical, "table_log",
      0, 0, 0);
  tableBoschX_log = new G4LogicalVolume(tableBarX_box, materials->aluminium_noOptical,
      "boschX_log", 0, 0, 0);
  tableBoschY_log = new G4LogicalVolume(tableBarY_box, materials->aluminium_noOptical,
      "boschY_log", 0, 0, 0);
  tableBoschZ_log = new G4LogicalVolume(tableBarZ_box, materials->aluminium_noOptical,
      "boschZ_log", 0, 0, 0);

  table_log->SetVisAttributes(colour->yellow);
  tableBoschX_log->SetVisAttributes(colour->yellow);
  tableBoschY_log->SetVisAttributes(colour->yellow);
  tableBoschZ_log->SetVisAttributes(colour->yellow);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, yPosTopTable - dimensionsPlate[1], zPosTopTable),
      table_log, "table_phys", world_log, false, 0, checkOverlap);

  G4ThreeVector tableBoschX_position[4], tableBoschY_position[4],
      tableBoschZ_position[4];

  tableBoschX_position[0] = positionVector
      + G4ThreeVector(0,
          yPosTopTable - 2. * dimensionsPlate[1] - 8.0 * CLHEP::cm
              - thicknessTableBar,
          zPosTopTable + zLengthTable + thicknessTableBar);
  tableBoschX_position[1] = positionVector
      + G4ThreeVector(0,
          yPosTopTable - 2. * dimensionsPlate[1] - 8.0 * CLHEP::cm
              - thicknessTableBar,
          zPosTopTable - zLengthTable - thicknessTableBar);
  tableBoschX_position[2] = positionVector
      + G4ThreeVector(0,
          yPosTopTable - 2. * dimensionsPlate[1] - 2. * yLengthTable
              + 23.0 * CLHEP::cm + thicknessTableBar,
          zPosTopTable + zLengthTable + thicknessTableBar);
  tableBoschX_position[3] = positionVector
      + G4ThreeVector(0,
          yPosTopTable - 2. * dimensionsPlate[1] - 2. * yLengthTable
              + 23.0 * CLHEP::cm + thicknessTableBar,
          zPosTopTable - zLengthTable - thicknessTableBar);

  tableBoschY_position[0] = positionVector
      + G4ThreeVector(xLengthTable + thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - yLengthTable,
          zPosTopTable + zLengthTable + thicknessTableBar);
  tableBoschY_position[1] = positionVector
      + G4ThreeVector(-xLengthTable - thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - yLengthTable,
          zPosTopTable + zLengthTable + thicknessTableBar);
  tableBoschY_position[2] = positionVector
      + G4ThreeVector(xLengthTable + thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - yLengthTable,
          zPosTopTable - zLengthTable - thicknessTableBar);
  tableBoschY_position[3] = positionVector
      + G4ThreeVector(-xLengthTable - thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - yLengthTable,
          zPosTopTable - zLengthTable - thicknessTableBar);

  tableBoschZ_position[0] = positionVector
      + G4ThreeVector(xLengthTable + thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - 45.0 * CLHEP::cm
              - thicknessTableBar, zPosTopTable);
  tableBoschZ_position[1] = positionVector
      + G4ThreeVector(-xLengthTable - thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - 45.0 * CLHEP::cm
              - thicknessTableBar, zPosTopTable);
  tableBoschZ_position[2] = positionVector
      + G4ThreeVector(xLengthTable + thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - 2. * yLengthTable
              + 23.0 * CLHEP::cm + thicknessTableBar, zPosTopTable);
  tableBoschZ_position[3] = positionVector
      + G4ThreeVector(-xLengthTable - thicknessTableBar,
          yPosTopTable - 2. * dimensionsPlate[1] - 2. * yLengthTable
              + 23.0 * CLHEP::cm + thicknessTableBar, zPosTopTable);

  for (G4int i = 0; i < 4; i++) {
    new G4PVPlacement(0, tableBoschX_position[i], tableBoschX_log,
        "boschX_phys", world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, tableBoschY_position[i], tableBoschY_log,
        "boschY_phys", world_log, false, 0, checkOverlap);
    new G4PVPlacement(0, tableBoschZ_position[i], tableBoschZ_log,
        "boschZ_phys", world_log, false, 0, checkOverlap);
  }

  G4double thicknessTopBar = 5.0 / 2 * CLHEP::cm;
  G4double xLengthTop = (slabPosition[2].getX() - slabPosition[17].getX()) / 2
      - thicknessTopBar; // from 3rd outer slab each side
  G4double pmtTopPosition = slabDimensions[1] + 2. * lgLength + pmtLength;
  G4double yLengthTop = (pmtTopPosition + thicknessTopBar - yPosTopTable) / 2; // from table to middle of upper PMT

  topBarX_box[0] = new G4Box("topBarX_box[0]", 265.0 * CLHEP::cm / 2,
      15.0 * CLHEP::cm / 2, 1.0 * CLHEP::cm / 2);
  topBarX_box[1] = new G4Box("topBarX_box[1]", xLengthTop, thicknessTopBar,
      thicknessTopBar);
  topBarY_box = new G4Box("topBarY_box", thicknessTopBar, yLengthTop,
      thicknessTopBar);

  topBoschX_log[0] = new G4LogicalVolume(topBarX_box[0], materials->aluminium_noOptical,
      "topBoschX_log", 0, 0, 0);
  topBoschX_log[1] = new G4LogicalVolume(topBarX_box[1], materials->aluminium_noOptical,
      "topBoschX_log", 0, 0, 0);
  topBoschY_log = new G4LogicalVolume(topBarY_box, materials->aluminium_noOptical,
      "topBoschY_log", 0, 0, 0);

  topBoschX_log[0]->SetVisAttributes(colour->gray);
  topBoschX_log[1]->SetVisAttributes(colour->gray);
  topBoschY_log->SetVisAttributes(colour->gray);

  G4ThreeVector topBoschX_position[3], topBoschY_position[2];

  topBoschX_position[0] = positionVector
      + G4ThreeVector(0, -pmtTopPosition,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm / 2); // 4cm PMT housing + 1cm airGap
  topBoschX_position[1] = positionVector
      + G4ThreeVector(0, pmtTopPosition,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm / 2);
  topBoschX_position[2] = positionVector
      + G4ThreeVector(0, pmtTopPosition,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm - thicknessTopBar);

  topBoschY_position[0] = positionVector
      + G4ThreeVector(slabPosition[2].getX(), yPosTopTable + yLengthTop,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm - thicknessTopBar);
  topBoschY_position[1] = positionVector
      + G4ThreeVector(slabPosition[17].getX(), yPosTopTable + yLengthTop,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm - thicknessTopBar);

  new G4PVPlacement(0, topBoschX_position[0], topBoschX_log[0],
      "topBoschX_phys", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0, topBoschX_position[1], topBoschX_log[0],
      "topBoschX_phys", world_log, false, 0, checkOverlap);
  new G4PVPlacement(0, topBoschX_position[2], topBoschX_log[1],
      "topBoschX_phys", world_log, false, 0, checkOverlap);

  new G4PVPlacement(0, topBoschY_position[0], topBoschY_log, "topBoschY_phys",
      world_log, false, 0, checkOverlap);
  new G4PVPlacement(0, topBoschY_position[1], topBoschY_log, "topBoschY_phys",
      world_log, false, 0, checkOverlap);

  G4double yDimBox = 65.0 * CLHEP::cm / 2;
  G4double zDimBox = 40.0 * CLHEP::cm / 2;

  G4ThreeVector positionAnglePiece[2];
  positionAnglePiece[0] = positionVector
      + G4ThreeVector(slabPosition[2].getX(), yPosTopTable + yDimBox,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm - 2. * thicknessTopBar - zDimBox);
  positionAnglePiece[1] = positionVector
      + G4ThreeVector(slabPosition[17].getX(), yPosTopTable + yDimBox,
          -5.0 * CLHEP::cm - 1.0 * CLHEP::cm - 2. * thicknessTopBar - zDimBox);

  rotationAnglePiece = new CLHEP::HepRotation;
  rotationAnglePiece->rotateX(-30.0 * CLHEP::deg);

  boxAnglePiece_box = new G4Box("boxAnglePiece_box", thicknessTopBar, yDimBox,
      zDimBox);
  anglePiece_box = new G4Box("anglePiece_box", thicknessTopBar, 3 * zDimBox,
      thicknessTopBar);
  anglePiece_intersection = new G4IntersectionSolid("anglePiece_intersection",
      boxAnglePiece_box, anglePiece_box, rotationAnglePiece,
      G4ThreeVector(0, -2. * thicknessTopBar, 0));
  anglePiece_log = new G4LogicalVolume(anglePiece_intersection,
      materials->aluminium_noOptical, "anglePiece_log", 0, 0, 0);
  anglePiece_log->SetVisAttributes(colour->gray);
  new G4PVPlacement(0, positionAnglePiece[0], anglePiece_log, "anglePiece_phys",
      world_log, false, 0, checkOverlap);
  new G4PVPlacement(0, positionAnglePiece[1], anglePiece_log, "anglePiece_phys",
      world_log, false, 0, checkOverlap);
}
