#ifndef T4TARGETBACKEND_HH
#define T4TARGETBACKEND_HH

#include <string>
#include <vector>

#include "G4VPhysicalVolume.hh"

#include "T4SMessenger.hh"
#include "T4SGlobals.hh"
#include "T4SettingsFile.hh"

class T4TargetBackend;

class T4ActiveTarget
{
  public:
    static T4ActiveTarget* getInstance(void);
    virtual ~T4ActiveTarget(void) {}

    void registerTarget(T4TargetBackend*);
    T4TargetBackend* getTarget(void) {return activeTarget;}

  private:
    T4ActiveTarget(void) {activeTarget = NULL;}
    static T4ActiveTarget* instance;

    T4TargetBackend* activeTarget;
};

/*! \file T4TargetBackend 
 *  \class T4TargetBackend
 *  \brief Target special functions for geometries
 */

class T4TargetBackend
{
  public:
    T4TargetBackend(void);
    virtual ~T4TargetBackend(void) {}

    /*! \brief T4STargetInformation to write a detectors.dat file */
    virtual void getTargetDetDat(std::vector<T4STargetInformation>&) = 0;

    /*! \brief sets the dimensions for the extrapolation */
    void setDimension(double _zFirst, double _zLast) {zFirst = _zFirst; zLast = _zLast;}
    
    /*! \brief sets the zFirst for the extrapolation */
    void setZFirst(double _zFirst) {zFirst = _zFirst;}
    
    /*! \brief sets the zFirst for the extrapolation */
    void setZLast(double _zLast) {zLast = _zLast;}
    
    /*! \brief gets the rough estimate on where to start with exact stepping */
    double getZFirst(void) {return zFirst;}

    /*! \brief gets the rough estimate on where to stop with exact stepping */
    double getZLast(void) {return zLast;}

    /*! \brief sets the linear parameters for the energy correction while extrapolation */
    void setELossParams(double _eConst, double _eSlope) {eConst = _eConst; eSlope = _eSlope;}

    /*! \brief returns the constant term of the linear energy correction */
    double getEConst(void) {return eConst;}

    /*! \brief returns the slope of the linear energy correction */
    double getESlope(void) {return eSlope;}

    /*! \brief returns the distance of the primary particle inside the target after which the event generator will be called */
    virtual double getRndmTargetDist(double maxTargetLength) {
      if (maxTargetLength == 0)
        return -1;
      return maxTargetLength * T4SettingsFile::getInstance()->getRandom()->flat();
    }

    /*! \brief Check if a G4VPhysicalVolume is a active target volume. */
    bool isVolumeInTarget(G4VPhysicalVolume*);

    /*! \brief Adds a G4VPhysicalVolume to the active target vector. */
    void addToTarget(G4VPhysicalVolume* phys) {activeTargetVolume.push_back(phys);}

  private:
    /*! \brief Container for active target volumes. */
    std::vector<G4VPhysicalVolume*> activeTargetVolume;

    //approximate values for the begin and the end of the region where TARGET volumes are expected
    double zFirst, zLast;
    //energy loss correction in the mean way
    double eConst, eSlope;
};

#endif
