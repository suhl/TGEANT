#include "T4WorldConstruction.hh"

T4WorldConstruction::T4WorldConstruction(void)
{
  world_box = NULL;
  world_log = NULL;
  world_phys = NULL;

  dimensionsWorld[0] = 10.0 / 2 * CLHEP::m;
  dimensionsWorld[1] = 10.0 / 2 * CLHEP::m;
  dimensionsWorld[2] = 130.0 / 2 * CLHEP::m;

  initialize();
}

T4WorldConstruction::~T4WorldConstruction(void)
{
  T4SMessenger::getReference().setupStream(T4SNotice, __LINE__, __FILE__);
  T4SMessenger::getReference() << "Deleting T4WorldConstruction";

  for (unsigned int i = 0; i < baseDetector.size(); i++) {
    T4SMessenger::getReference() << ".";
  }

  if (world_box != NULL)
    delete world_box;
  if (world_log != NULL)
    delete world_log;
  if (world_phys != NULL)
    delete world_phys;

  T4RegionManager::resetInstance();
  T4Materials::resetInstance();
  T4Colour::resetInstance();
  T4DeathMagnetic::resetInstance();

  T4SMessenger::getReference() << " finished.";
  T4SMessenger::getReference().endStream();
}

void T4WorldConstruction::initialize(void)
{
  T4SettingsFile* settingsFile = T4SettingsFile::getInstance();

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getRPD()->size(); i++) {
    T4SRPD* rpd = &settingsFile->getStructManager()->getRPD()->at(i);
    if ((rpd->general.name == "CA01R1" || rpd->general.name == "CA01R2")
        && rpd->general.useDetector) {
      baseDetector.push_back(new CameraConstruction());
      break; // we break here to get sure that a) we only build one CAMERA (not one for each ring) and b) we don't build the RPD
    } else if ((rpd->general.name == "RP01R1" || rpd->general.name == "RP01R2")
        && rpd->general.useDetector) {
      baseDetector.push_back(new RPDConstruction());
      break; // see camera comment
    }
  }

  T4DeathMagnetic* magneticField = T4DeathMagnetic::getInstance();
  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getMagnet()->size(); i++) {
    T4SMagnet* magnet = &settingsFile->getStructManager()->getMagnet()->at(i);
    if (magnet->general.name == "SM1" && magnet->general.useDetector) {
      if (magnet->general.useMechanicalStructure)
        baseDetector.push_back(new T4SM1(&magnet->general));
      if (magnet->useField)
        magneticField->registerPartialField(new T4SM1MagneticPlugin(magnet));

    } else if (magnet->general.name == "SM2" && magnet->general.useDetector) {
      if (magnet->general.useMechanicalStructure)
        baseDetector.push_back(new T4SM2(&magnet->general));
      if (magnet->useField)
        magneticField->registerPartialField(new T4SM2MagneticPlugin(magnet));
      
    } else if (magnet->general.name == "DYTARGET" && magnet->general.useDetector) {
      baseDetector.push_back(new T4DYTarget(&magnet->general));
      if (magnet->useField)
        magneticField->registerPartialField(getFieldByName(magnet));
    } else if (magnet->general.name == "POLTARGET" && magnet->general.useDetector) {
      baseDetector.push_back(new T4PolarizedTarget(&magnet->general));
      if (magnet->useField)
        magneticField->registerPartialField(getFieldByName(magnet));
    }

  }

  if (settingsFile->getStructManager()->getStraw()->size() != 0)
    baseDetector.push_back(new StrawConstruction());
  if (settingsFile->getStructManager()->getMicromegas()->size() != 0)
    baseDetector.push_back(new T4Micromegas());
  if (settingsFile->getStructManager()->getGem()->size() != 0)
    baseDetector.push_back(new T4Gem());
  if (settingsFile->getStructManager()->getMWPC()->size() != 0)
    baseDetector.push_back(new T4Mwpc());
  if (settingsFile->getStructManager()->getSilicon()->size() != 0)
    baseDetector.push_back(new T4Silicon());
  if (settingsFile->getStructManager()->getRichWall()->size() != 0)
    baseDetector.push_back(new T4RichWall());

  // Do not change the order! RICH has to be called first, SciFi (FI05) second.
  G4LogicalVolume** rich_log = NULL;
  G4ThreeVector richPos;
  if (settingsFile->getStructManager()->getRICH()->general.useDetector) {
    RichConstruction* rich = new RichConstruction();
    baseDetector.push_back(rich);
    rich_log = &rich->getRichLogical();
    richPos = rich->getVesselMidPosition();
  }
  if (settingsFile->getStructManager()->getSciFi()->size() != 0)
    baseDetector.push_back(new T4SciFi(rich_log, richPos));
  if (settingsFile->getStructManager()->getDC()->size() != 0)
    baseDetector.push_back(new DCConstruction());
  if (settingsFile->getStructManager()->getW45()->size() != 0)
    baseDetector.push_back(new T4W45());
  if (settingsFile->getStructManager()->getMW1()->general.useDetector)
    baseDetector.push_back(new MW1Construction());
  if (settingsFile->getStructManager()->getMW2()->general.useDetector)
    baseDetector.push_back(new MW2Construction());
  if (settingsFile->getStructManager()->getCalorimeter()->size() != 0)
    baseDetector.push_back(new T4CaloManager());
  if (settingsFile->getStructManager()->getVeto()->size() != 0)
    baseDetector.push_back(new T4Veto());

  G4bool calledHodoManager = false;
  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getDetector()->size(); i++) {
    T4SDetector* detector =
        &settingsFile->getStructManager()->getDetector()->at(i);
    if (detector->name == "LH2" && detector->useDetector)
      baseDetector.push_back(new T4LH2Target(detector));
    else if (detector->name == "LH2Hadron" && detector->useDetector)
      baseDetector.push_back(new T4LH2Target_Hadron(detector));
    else if (detector->name == "DYABSORBER" && detector->useDetector)
      baseDetector.push_back(new T4DYAbsorber(detector));
    else if (detector->name == "PRIMAKOFFTARGET" && detector->useDetector)
      baseDetector.push_back(new T4Primakoff2012Target(detector));
    else if (detector->name == "MF3" && detector->useDetector)
      baseDetector.push_back(new MF3Construction(detector));
    else if (detector->name == "MF3_BLOCK" && detector->useDetector)
      baseDetector.push_back(new MF3Block(detector));
    else if (detector->name == "TESTCAL" && detector->useDetector)
      baseDetector.push_back(new T4TestCal(detector));
    else if (detector->name == "PolGPDSiRPD" && detector->useDetector)
      baseDetector.push_back(new T4PolGPD_SiRPD(detector));
    else if (detector->name == "WORKSHOPEXAMPLE" && detector->useDetector)
      baseDetector.push_back(new T4WorkshopExample(detector));

    // condition for all hodoscopes
    // first char of detector name should be a H
    else if (!calledHodoManager && detector->useDetector
        && detector->name[0] == 'H' && detector->name != "HO03"
        && detector->name != "HO04" && detector->name[1] != 'K') {
      baseDetector.push_back(new T4HodoscopeManager());
      calledHodoManager = true;
    }

    else if (detector->name == "HO04" && detector->useDetector)
      baseDetector.push_back(new T4HO04(detector));
    else if (detector->name == "HO03" && detector->useDetector)
      baseDetector.push_back(new T4HO03(detector));
    else if (detector->name.substr(0,2) == "HK" && detector->useDetector)
      baseDetector.push_back(new T4BeamKiller(detector));
    else if (detector->name == "SANDWICHVETO" && detector->useDetector)
      baseDetector.push_back(new T4SandwichVeto(detector));
    else if (detector->name == "MultiplicityCounter" && detector->useDetector)
      baseDetector.push_back(new T4MultiplicityCounter(detector));
    else if (detector->name == "HH02R1__" && detector->useDetector)
      baseDetector.push_back(new T4MainzCounter02(detector));
    else if (detector->name == "HH03R1__" && detector->useDetector)
      baseDetector.push_back(new T4MainzCounter03(detector));
  }

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getDetectorRes()->size(); i++) {
    T4SDetectorRes* detectorRes = &settingsFile->getStructManager()
        ->getDetectorRes()->at(i);
    if (detectorRes->general.name == "HG01Y"
        && detectorRes->general.useDetector)
      baseDetector.push_back(new H1Construction(detectorRes));
    else if (detectorRes->general.name == "HG02Y"
        && detectorRes->general.useDetector)
      baseDetector.push_back(new H2Construction(detectorRes));
    else if (detectorRes->general.name == "VETO"
        && detectorRes->general.useDetector)
      baseDetector.push_back(new VetoConstruction(detectorRes));
  }

  if (settingsFile->getStructManager()->getPolGPD()->general.useDetector)
    baseDetector.push_back(new T4PolGPD());
  if (settingsFile->getStructManager()->getSciFiTest()->general.useDetector)
    baseDetector.push_back(new SciFiTest());
  if (settingsFile->getStructManager()->getDummies()->size() != 0)
    baseDetector.push_back(new DummyConstruction());
}

G4VPhysicalVolume* T4WorldConstruction::Construct(void)
{
  T4SMessenger::getReference().setupStream(T4SNotice, __LINE__, __FILE__);
  T4SMessenger::getReference() << "T4WorldConstruction: Construct geometries";

  world_box = new G4Box("world_box", dimensionsWorld[0], dimensionsWorld[1],
      dimensionsWorld[2]);
  world_log = new G4LogicalVolume(world_box,
      T4Materials::getInstance()->air_noOptical, "tgeant_world", 0, 0, 0, 0);
  world_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), world_log, "world",
      0, false, 0, false);
  world_log->SetVisAttributes(T4Colour::getInstance()->invisible);

  // Now, construct all detectors:
  for (unsigned int i = 0; i < baseDetector.size(); i++) {
    baseDetector.at(i)->construct(world_log);
    T4SMessenger::getReference() << ".";
  }
  T4SMessenger::getReference() << " finished.";
  T4SMessenger::getReference().endStream();

  return world_phys;
}

T4PartialField* T4WorldConstruction::getFieldByName(T4SMagnet* magnet)
{
  T4PartialField* field = NULL;
  std::vector<std::string> name = explodeStringCustomDelim(magnet->fieldmapPath, "/");
  
  if (name.back() == "OD_dipole.fieldmap")
    field = new T4ODDipoleField(magnet);
  else if (name.back() == "SOL_map_fabrice.dat")
    field = new T4TransvTargetField(magnet);
  else
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
    "T4WorldConstruction::getFieldByName: Unknown field map name: '" + magnet->fieldmapPath + "'. Accepted names are 'OD_dipole.fieldmap' and 'SOL_map_fabrice.dat'.");

  return field;
}
