#ifndef STRAWPLANE_HH_
#define STRAWPLANE_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"

// position of plane: center of both double layers
// from detectors.dat: position up + down / 2
class StrawPlane : public T4BaseDetector
{
  public:
    StrawPlane(const T4SDetector*);
    virtual ~StrawPlane(void);

    void construct(G4LogicalVolume*);
    virtual void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&) {};

  protected:
    G4String detectorName_a[2];
    G4String detectorName_b[2];
    G4String detectorName_c[2];
    G4int detectorId_a[2];
    G4int detectorId_b[2];
    G4int detectorId_c[2];
    G4String mechName;

    virtual G4ThreeVector getVector(G4double, G4double, G4double) = 0;
    G4RotationMatrix* rotationPlane;
    G4RotationMatrix* rotationTube;
    G4double strawLength;
    G4double outerRadiusBig;
    G4double outerRadiusSmall;
    unsigned int nOuterWires;
    unsigned int nInnerLongWires;
    unsigned int nInnerShortWires;
    G4double mylarSize[3];
    G4double holeLength;
    G4double carbonSize[3];
    G4double zPosSmall[2];
    G4double zPosBig[2];
    G4double posOffsetSmall[2];
    G4double posOffsetBig[2];

    T4SWireDetector wire_a[2];
    T4SWireDetector wire_b[2];
    T4SWireDetector wire_c[2];
    T4SDeadZone dead[2];
    void setDefault(void);
    void setDetAndUnit(T4SWireDetector&);

  private:
    static int nInstances;
    static int counterSTY[6]; // counter for X-Plane
    static int counterSTZ[6]; // counter for Y-Plane
    static int counterSTU[6]; // counter for U-Plane
    static int counterSTV[6]; // counter for V-Plane

    G4Box* mylar_box;
    G4LogicalVolume* mylar_log;
    G4Box* nitrogen_box;
    G4LogicalVolume* nitrogen_log;

    // 0 for big straw in outer region
    // 1 for small straw in inner region (full length)
    // 2 for small straw in hole region
    enum StrawSize
    {
      Big = 0, SmallLong = 1, SmallShort = 2
    };
    void buildStrawTube(G4ThreeVector, StrawPlane::StrawSize, G4String planeName, G4int detectorId);

    G4CSGSolid* aluminium_tubs[3];
    G4Tubs* outerKapton_tubs[3];
    G4Tubs* glue_tubs[3];
    G4Tubs* innerKapton_tubs[3];
    G4Tubs* gas_tubs[3];
    G4Tubs* wire_tubs[3];
    std::vector<G4LogicalVolume*> aluminium_log;
    std::vector<G4LogicalVolume*> outerKapton_log;
    std::vector<G4LogicalVolume*> glue_log;
    std::vector<G4LogicalVolume*> innerKapton_log;
    std::vector<G4LogicalVolume*> sensitive_log;
    std::vector<G4LogicalVolume*> wire_log;

    void buildMechanicalStructure(G4LogicalVolume*);
    G4Box* frame_box;
    G4SubtractionSolid* frame_sub;
    G4LogicalVolume* frame_log;

    G4Box* carbon_box;
    G4LogicalVolume* carbon_log;

};

#endif /* STRAWPLANE_HH_ */
