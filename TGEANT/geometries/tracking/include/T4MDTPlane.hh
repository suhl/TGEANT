#ifndef T4MDTPLANE_HH_
#define T4MDTPLANE_HH_

#include "T4MDTModule.hh"
#include "T4BaseDetector.hh"

class T4MDTPlane : public T4BaseDetector
{
  public:
    enum PlaneTypeMDT
    {
      MW1_X, MW1_Y, DR_X, DR_Y
    };

    T4MDTPlane(T4DetIdent detIdent, G4int detectorId, PlaneTypeMDT planeType);
    virtual ~T4MDTPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    T4DetIdent detIdent;
    G4int detectorId;
    PlaneTypeMDT planeType;
    void setDimensions(void);

    G4int nModules;
    G4int nModulesHole;
    G4double lengthFull;
    G4double lengthAroundHole;
    G4double moduleWidth;
    G4double detectorThickness;

    CLHEP::HepRotation* moduleRotation;

    std::vector<T4MDTModule*> mdtModules;
};

#endif /* T4MDTPLANE_HH_ */
