#ifndef T4MWPC_HH_
#define T4MWPC_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Para.hh"

class T4MwpcPlane : public T4BaseDetector
{
  public:
    T4MwpcPlane(const T4SDetector*, int& _firstDetectorId);
    virtual ~T4MwpcPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    void buildX();
    void buildY();
    void buildU();
    void buildV();

    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4Para*> para;
    vector<G4LogicalVolume*> log;
    vector<unsigned int> sdIndex;

    int firstDetectorId;
    const T4SDetector* det;
    G4double deadZoneRadius;
    G4double angleUV;
    string namePlaneU;
    string namePlaneV;
    string namePlaneY;
    int nSd;
    G4double zSize;
    G4double xSizeSD;
    G4double ySizeSD;
    G4double worldDimension[3];
    G4double ySizeG10;
    G4double xSizeGas;
    G4double zSupport_1;
    G4double zSupport_2;
    G4double ySizeG10_10;
    G4double yPosG10_10;
    G4double zPosX;
    G4double zPosY;
    G4double zPosU;
    G4double zPosV;
    unsigned int mwpcGasIndex;
    G4ThreeVector motherShift;
    G4double xPosDeadZone;
};

class T4Mwpc : public T4BaseDetector
{
  public:
    T4Mwpc(void) {}
    virtual ~T4Mwpc(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4MwpcPlane*> mwpcPlane;
};

#endif /* T4MWPC_HH_ */
