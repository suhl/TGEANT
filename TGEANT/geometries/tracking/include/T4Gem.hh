#ifndef T4GEM_HH_
#define T4GEM_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

class T4GemPlane : public T4BaseDetector
{
  public:
    T4GemPlane(const T4SDetector*, int _firstDetectorId, bool _isPixel);
    virtual ~T4GemPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    CLHEP::HepRotation* rot;
    G4double zMult;

    int firstDetectorId;
    const T4SDetector* det;
    string namePlane2;

    G4double xySize;
    G4double zSize;
    G4double deadZoneRadius;
    int unit;

    bool isPixel;
    string namePlanePixel;
};

class T4Gem : public T4BaseDetector
{
  public:
    T4Gem(void) {}
    virtual ~T4Gem(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4GemPlane*> gemPlane;
};

#endif /* T4GEM_HH_ */
