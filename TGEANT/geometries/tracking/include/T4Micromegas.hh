#ifndef T4MICROMEGAS_HH_
#define T4MICROMEGAS_HH_

#include "T4BaseDetector.hh"

#include "T4PixelMM.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"

class T4MMPlane : public T4BaseDetector
{
  public:
    T4MMPlane(const T4SDetector*, int _firstDetectorId);
    virtual ~T4MMPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    CLHEP::HepRotation* rot;
    G4double zMult;

    int firstDetectorId;
    const T4SDetector* det;

    G4double dimensionMiddle[3];
    G4double dimensionOuterX;
};

class T4Micromegas : public T4BaseDetector
{
  public:
    T4Micromegas(void) {}
    virtual ~T4Micromegas(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4MMPlane*> mmPlane;
    vector<T4PixelMM*> pmmPlane;
};

#endif /* T4MICROMEGAS_HH_ */
