#ifndef MW2CONSTRUCTION_HH_
#define MW2CONSTRUCTION_HH_

#include "T4BaseDetector.hh"
#include "MW2Plane.hh"

#include "G4SubtractionSolid.hh"

class MW2Construction : public T4BaseDetector
{
  public:
    MW2Construction(void);
    virtual ~MW2Construction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    T4SMW2* mw2;

    G4double dimensionsAbsorber[3];
    G4double dimensionsHole[2];
    G4Box* absorber_box;
    G4Box* hole_box;
    G4SubtractionSolid* absorber_sub;
    G4LogicalVolume* absorber_log;

    std::vector<MW2Plane*> mw2Planes;
};

#endif /* MW2CONSTRUCTION_HH_ */
