#ifndef STRAWCONSTRUCTION_HH_
#define STRAWCONSTRUCTION_HH_

#include "T4BaseDetector.hh"
#include "StrawPlaneX.hh"
#include "StrawPlaneY.hh"

class StrawConstruction : public T4BaseDetector
{
  public:
    StrawConstruction(void) {};
    virtual ~StrawConstruction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    std::vector<StrawPlane*> strawPlanes;
};

#endif /* STRAWCONSTRUCTION_HH_ */
