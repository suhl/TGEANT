#ifndef STRAWPLANEY_H_
#define STRAWPLANEY_H_

#include "StrawPlane.hh"

class StrawPlaneY : public StrawPlane
{
  public:
    StrawPlaneY(const T4SDetector*);
    virtual ~StrawPlaneY(void) {};

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4ThreeVector getVector(G4double x, G4double y, G4double z)
      {return G4ThreeVector(y, x, z);};
};

#endif /* STRAWPLANEY_H_ */
