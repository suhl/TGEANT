#ifndef T4SILICON_HH_
#define T4SILICON_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

class T4SIPlane : public T4BaseDetector
{
  public:
    T4SIPlane(const T4SDetector*, int _firstDetectorId);
    virtual ~T4SIPlane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    CLHEP::HepRotation* rot;
    G4double zMult;

    int firstDetectorId;
    const T4SDetector* det;
    string namePlane2;
    bool isX;
    bool isDownstream;
    CLHEP::HepRotation* rotTube_1;
    CLHEP::HepRotation* rotTube_2;
};

class T4Silicon : public T4BaseDetector
{
  public:
    T4Silicon(void) {}
    virtual ~T4Silicon(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4SIPlane*> siPlane;
};

#endif /* T4SILICON_HH_ */
