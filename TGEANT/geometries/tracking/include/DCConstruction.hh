#ifndef DCCONSTRUCTION_HH_
#define DCCONSTRUCTION_HH_

#include "T4BaseDetector.hh"
#include "DCPlane.hh"
#include "DCMechanicalStructure.hh"

class DCConstruction : public T4BaseDetector
{
  public:
    DCConstruction(void) {};
    virtual ~DCConstruction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    void addDetector(const T4SDetector*);
    std::vector<DCPlane*> dcPlanes;
    std::vector<DCMechanicalStructure*> dcMech;
};

#endif /* DCCONSTRUCTION_HH_ */
