#ifndef MW2PLANE_HH_
#define MW2PLANE_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

// MW2 Plane consists of one double layer
// u = upstream
// d = downstream
// b = bottom
// c = top
// r = right = jura = positive X-direction
// l = left = saleve = negative X-direction
// X-Plane: db(140), dc(28), ub(140), uc(28)
//          channel number increasing from saleve to jura side (increasing with X-coordinate)
// Y-Plane: dl(24), dr(60), ul(24), ur(60)
//          channel number increasing from bottom to top (increasing with Y-coordinate)
// V-Plane: db(140), dc(28), ub(140), uc(28)
//          channel number increasing from saleve to jura side (increasing with X-coordinate)

class MW2Plane : public T4BaseDetector
{
  public:
    enum MW2Type
    {
      X, Y, V
    };

    MW2Plane(T4DetIdent detIdent, MW2Plane::MW2Type);
    virtual ~MW2Plane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4ThreeVector shiftToHole;
    CLHEP::HepRotation* rotationTube;
    MW2Plane::MW2Type mw2Type;

    static int detectorIdCounter;
    G4int detectorId[4];
    G4String detTypeName[4];
    T4DetIdent detIdent;

    G4double lengthTubeLong;
    G4double lengthTubeShort_A; // A and B for bottom(A) and top(B) in case of X and V plane
    G4double lengthTubeShort_B; // or saleve(A) and jura(B) in case of Y plane

    G4int numWires_1; // 1 and 2 for bottom(1) and top(2) in case of Y plane
    G4int numWires_2; // or saleve(1) and jura(2) in case of X and V plane
    G4int numWires_hole; // number of wires in hole region (respectively for tubeShort_A and _B)

    G4double lengthHole;

    G4double outerRadius;
    G4double innerRadius;
    G4double wireRadius;
    G4double distanceXY;
    G4double distanceZ;

    G4CSGSolid* outerTubeLong_tubs;
    G4CSGSolid* outerTubeShort_A_tubs;
    G4CSGSolid* outerTubeShort_B_tubs;

    G4Tubs* innerTubeLong_tubs;
    G4Tubs* innerTubeShort_A_tubs;
    G4Tubs* innerTubeShort_B_tubs;

    G4Tubs* wireLong_tubs;
    G4Tubs* wireShort_A_tubs;
    G4Tubs* wireShort_B_tubs;

    G4LogicalVolume* wireLong_log;
    G4LogicalVolume* wireShort_A_log;
    G4LogicalVolume* wireShort_B_log;

    std::vector<G4LogicalVolume*> outerTubeLong_1_log;
    std::vector<G4LogicalVolume*> outerTubeLong_2_log;
    std::vector<G4LogicalVolume*> outerTubeShort_A_log;
    std::vector<G4LogicalVolume*> outerTubeShort_B_log;

    std::vector<G4LogicalVolume*> innerTube_log;
    std::vector<G4LogicalVolume*> innerTube_2_log;
};

#endif /* MW2PLANE_HH_ */
