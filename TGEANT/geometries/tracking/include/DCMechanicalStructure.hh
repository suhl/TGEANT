#ifndef DCMECHANICALSTRUCTURE_HH_
#define DCMECHANICALSTRUCTURE_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

class DCMechanicalStructure : public T4BaseDetector
{
  public:
    DCMechanicalStructure(void);
    virtual ~DCMechanicalStructure(void);

    void setPlaneSize(const G4double* _planeSize) {planeSize = _planeSize;};
    void setZOffset(G4double _zOffset) {zOffset = _zOffset;};
    void setDC04(void) {isDC04 = true;};

    void construct(G4LogicalVolume*);

  private:
    const G4double* planeSize;
    G4double zOffset;

    G4bool isDC04;

    G4Box* alWindow_box;
    G4LogicalVolume* alWindow_log;
    G4Box* mylarWindow_box;
    G4LogicalVolume* mylarWindow_log;

    CLHEP::HepRotation* rotation35p;
    CLHEP::HepRotation* rotation35n;
    G4Box* bar_box;
    G4Box* barBox_box;
    G4IntersectionSolid* bar_intersection[4];
    G4LogicalVolume* bar_log[4];

    G4Box* outerFrame_box;
    G4Box* mainFrameComplete_box;
    G4SubtractionSolid* outerFrame_sub;
    G4LogicalVolume* outerFrame_log;

    G4Box* mainFrameSteel_box;
    G4Box* mainFrameG11_box;
    G4Box* innerFrame_box;
    G4SubtractionSolid* mainFrameSteel_sub;
    G4SubtractionSolid* mainFrameG11_sub;
    G4LogicalVolume* mainFrameSteel_log;
    G4LogicalVolume* mainFrameG11_log;

    G4Box* frameHolding_box;
    G4LogicalVolume* frameHolding_log;

    CLHEP::HepRotation* rotationChain;
    G4Tubs* lowerChain_tubs;
    G4LogicalVolume* lowerChain_log;

    G4Box* middleBar_box;
    G4LogicalVolume* middleBar_log;

    G4Box* sideBlock_box;
    G4LogicalVolume* sideBlock_log;

    G4Tubs* upperChain_tubs;
    G4LogicalVolume* upperChain_log;

    G4Box* redHoldingBlock_box[3];
    G4SubtractionSolid* redHoldingBlock_sub;
    G4UnionSolid* redBar_union[2];
    G4LogicalVolume* redBar_log;

    G4Box* yellowBar_box;
    G4LogicalVolume* yellowBar_log;
};

#endif /* DCMECHANICALSTRUCTURE_HH_ */
