#ifndef T4SCIFI_HH_
#define T4SCIFI_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polyhedra.hh"
#include "G4Trd.hh"
// #include "TGeoMatrix.h"

class T4FIPlane : public T4BaseDetector
{
  public:
    T4FIPlane(void);
    virtual ~T4FIPlane(void);

    virtual void construct(G4LogicalVolume*) = 0;
    virtual void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  protected:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4Polyhedra*> poly;
    vector<G4Trd*> trap;
    vector<G4LogicalVolume*> log;

    int firstDetectorId;
    const T4SDetector* det;
    string namePlane2;
    string namePlane3;
    bool usePlane3;

    G4double dimSens[3];
    G4double dimSensV;
    G4double pitch;
    G4double nWires;
    G4double nWiresV;
    G4double plane2Dist;
    G4double plane3Dist;
    G4ThreeVector detDatPos;

    void setGeneralWireInfo(T4SWireDetector& wire);
};

class T4FIPlane01 : public T4FIPlane
{
  public:
    T4FIPlane01(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane01(void) {}

    virtual void construct(G4LogicalVolume*);
};

class T4FIPlane15 : public T4FIPlane
{
  public:
    T4FIPlane15(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane15(void) {delete rotY;}

    virtual void construct(G4LogicalVolume*);

  private:
    G4RotationMatrix* rotY;
    G4RotationMatrix* rotU;
};

class T4FIPlane03 : public T4FIPlane
{
  public:
    T4FIPlane03(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane03(void) {}

    virtual void construct(G4LogicalVolume*);

  private:
    G4RotationMatrix* rotY;
    G4RotationMatrix* rotU;
    G4RotationMatrix* rot1;
};
//New vertex detector implementation
class T4FIPlane35 : public T4FIPlane
{
  public:
  T4FIPlane35(const T4SDetector*_det, int _firstDetectorId);
  virtual ~T4FIPlane35(void) {}

  virtual void construct(G4LogicalVolume*);
  void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
  G4RotationMatrix* rotV;
  G4RotationMatrix* rotU;
  G4RotationMatrix* rotX;
  G4RotationMatrix* rotS;
  G4RotationMatrix* rotAS;

  string namePlane4;
  string namePlane5;
  string namePlane6;
  G4double plane4Dist;
  G4double plane5Dist;
  G4double plane6Dist;
  G4double nWires2;

  G4double dimSensX2[2];
  G4double dimSensV2[2];

  G4double dimDeadX[2];
  G4double dimDeadV[2];
  G4double posDeadX[2];
  G4double posDeadV[2];
  G4double posDeadU[2];

  G4double posSensU2[2];
  G4double posSensV2[2];

  G4double stagU[2];
  G4double stagV[2];
  G4double coordDDatU2[2];
  G4double coordDDatV2[2];
  G4double anglesU[2];
  G4double anglesV[2];
  G4double compU[7];
  G4double compV[7];
  G4double compDU[7];
  G4double compDV[7];

  G4double coordDeadU2[2];
  G4double coordDeadV2[2];

};
/*
class T4FIPlane36 : public T4FIPlane
{
  public:
    T4FIPlane36(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane36(void) {}

    virtual void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    G4RotationMatrix* rotV;
    G4RotationMatrix* rotU;

    string namePlane4;
    string namePlane5;
    string namePlane6;
    G4double plane4Dist;
    G4double plane5Dist;
    G4double plane6Dist;
    G4double nWires2;

    G4double dimSensX2[2];
    G4double dimSensV2[2];

    G4double dimDeadX[2];
    G4double dimDeadV[2];
    G4double posDeadX[2];
    G4double posDeadV[2];
};
*/
class T4FIPlane04 : public T4FIPlane
{
  public:
    T4FIPlane04(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane04(void) {delete rotY; delete rotU;}

    virtual void construct(G4LogicalVolume*);

  private:
    G4RotationMatrix* rotY;
    G4RotationMatrix* rotU;
};

class T4FIPlane05 : public T4FIPlane
{
  public:
    T4FIPlane05(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane05(void) {}

    virtual void construct(G4LogicalVolume*);
};

class T4FIPlane55 : public T4FIPlane
{
  public:
    T4FIPlane55(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane55(void) {delete rotU; delete rotV;}

    virtual void construct(G4LogicalVolume*);

  private:
    G4RotationMatrix* rotU;
    G4RotationMatrix* rotV;
};

class T4FIPlane06 : public T4FIPlane
{
  public:
    T4FIPlane06(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane06(void) {delete rotV;}

    virtual void construct(G4LogicalVolume*);

  private:
    G4RotationMatrix* rotV;
};

class T4FIPlane07 : public T4FIPlane
{
  public:
    T4FIPlane07(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane07(void) {}

    virtual void construct(G4LogicalVolume*);
};

class T4FIPlane08 : public T4FIPlane
{
  public:
    T4FIPlane08(const T4SDetector*, int _firstDetectorId);
    virtual ~T4FIPlane08(void) {}

    virtual void construct(G4LogicalVolume*);
};

class T4SciFi : public T4BaseDetector
{
  public:
    T4SciFi(G4LogicalVolume** _rich_log, G4ThreeVector _rich_pos)
      {rich_log = _rich_log; rich_pos = _rich_pos;}
    virtual ~T4SciFi(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4FIPlane*> fiPlane;
    G4LogicalVolume** rich_log;
    G4ThreeVector rich_pos;
};

#endif /* T4SCIFI_HH_ */
