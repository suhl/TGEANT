#ifndef DCPLANE_HH_
#define DCPLANE_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Para.hh"
#include "G4Tubs.hh"

#include "G4SubtractionSolid.hh"

// position of plane: center of the first double layer box
class DCPlane : public T4BaseDetector
{
  public:
    DCPlane(void);
    virtual ~DCPlane(void);

    const G4double* getSize(void) {return &planeDimension[0];}
    G4double getZOffset(void) {return shiftFromFirstPlaneToMiddle;}

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  protected:
    std::vector<T4DetIdent> detIdentVec;
    std::vector<G4double> wireDist;
    G4int firstDetectorId;
    G4int nWiresXY;
    G4int nWiresUV;
    G4double pitch;

    G4double bkLineThickness;
    G4double bkLineUVShift;
    G4bool bkOnJuraSide;

    G4double planeDimension[3];
    G4double sizeXPlane[2];
    G4double sizeYPlane[2];
    G4double sizeUVPlane[2];
    G4double angleUVPlane;

  private:
    void buildPlane(G4String planeName);
    G4int calls;
    G4double shiftFromFirstPlaneToMiddle;
    G4double mylarThickness;
    G4double graphiteThickness;
    G4double wireDistance; // distance between XX', YY', UU' or VV'
    G4double layerDistance; // distance between VU, UY or YX
    G4double sdThickness;
    G4double zPosSensitive[4];
    G4double deadDiameter;

    G4Box* plane_box;
    G4LogicalVolume* plane_log;
    G4Box* mylar_box;
    G4LogicalVolume* mylar_log;
    G4Box* graphite_box;
    G4LogicalVolume* graphite_log;

    G4Box* sdX_box;
    G4Box* sdY_box;
    G4Para* sdU_para;
    G4Para* sdV_para;
    std::vector<G4LogicalVolume*> sensitive_log;

    T4SWireDetector wire;
    T4SDeadZone dead;

    G4double potWireDiameter;
    G4double sensWireDiameter;

    G4Tubs* xWire_pot;
    G4Tubs* xWire_sens;
    G4Tubs* yWire_pot;
    G4Tubs* yWire_sens;
    G4Tubs* uvWire_pot;
    G4Tubs* uvWire_sens;

    G4LogicalVolume* xWire_pot_log;
    G4LogicalVolume* xWire_sens_log;
    G4LogicalVolume* yWire_pot_log;
    G4LogicalVolume* yWire_sens_log;
    G4LogicalVolume* uvWire_pot_log;
    G4LogicalVolume* uvWire_sens_log;

    G4RotationMatrix* xWire_rot;
    G4RotationMatrix* yWire_rot;
    G4RotationMatrix* uWire_rot;
    G4RotationMatrix* vWire_rot;

    void addBKLine(G4String planeName, T4SensitiveDetector* sens);
};

class DC00Plane : public DCPlane
{
  public:
    DC00Plane(void);
    virtual ~DC00Plane(void);
};

class DC01Plane : public DCPlane
{
  public:
    DC01Plane(void);
    virtual ~DC01Plane(void);
};

class DC04Plane : public DCPlane
{
  public:
    DC04Plane(void);
    virtual ~DC04Plane(void);
};

class DC05Plane : public DCPlane
{
  public:
    DC05Plane(void);
    virtual ~DC05Plane(void);
};

#endif /* DCPLANE_HH_ */
