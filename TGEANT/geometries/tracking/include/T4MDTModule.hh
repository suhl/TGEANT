#ifndef T4MDTMODULE_HH_
#define T4MDTMODULE_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"

// position = middle of the module
class T4MDTModule : public T4BaseDetector
{
  public:
    T4MDTModule(void);
    virtual ~T4MDTModule(void);

    void construct(G4LogicalVolume*);

    void setFullLength(G4double);
    void setRotation(CLHEP::HepRotation* _rotationMatrix)
      {rotationMatrix = _rotationMatrix;}
    void setAluThickness(G4double althick)
      {
       aluThickness = althick * CLHEP::mm;
       gasChamber[0] = (wireDistance - aluThickness) / 2.;
       gasChamber[1] = aluHousing[1] - aluThickness;
       gasChamber[2] = aluHousing[2] - aluThickness / 2.;
      }
    G4double getFullWidth(void)
      {return /*84.9121*/ 85.0 * CLHEP::mm;}
    G4double getDetectorThickness(void)
      {return gasChamber[2] * 2;}
    G4int getNumChambers(void)
      {return nChambers;}
    G4LogicalVolume* getChamber(G4int chamberNo)
      {return sens_log.at(chamberNo);}


  private:
    CLHEP::HepRotation* rotationMatrix;

    G4int nChambers;
    G4double plasticThickness;
    G4double steelThickness;
    G4double aluThickness;
    G4double wireThickness;
    G4double wireDistance;

    G4double plasticHousing[3];
    G4double steelHousing[3];
    G4double aluHousing[3];
    G4double gasChamber[3];

    CLHEP::HepRotation* wireRotation;
    G4Tubs* wire_tubs;

    vector<G4Box*> box;
    vector<G4LogicalVolume*> log;
    vector<G4LogicalVolume*> sens_log;
};

#endif /* T4MDTMODULE_HH_ */
