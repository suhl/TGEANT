#ifndef STRAWPLANEX_H_
#define STRAWPLANEX_H_

#include "StrawPlane.hh"

class StrawPlaneX : public StrawPlane
{
  public:
    StrawPlaneX(const T4SDetector*);
    virtual ~StrawPlaneX(void) {};

    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  protected:
    virtual void setRotation(void);

    G4ThreeVector getVector(G4double x, G4double y, G4double z)
      {return G4ThreeVector(x, y, z);};
};

class StrawPlaneU : public StrawPlaneX
{
  public:
    StrawPlaneU(const T4SDetector*);
    virtual ~StrawPlaneU(void) {};

  private:
    void setRotation(void);
};

class StrawPlaneV : public StrawPlaneX
{
  public:
    StrawPlaneV(const T4SDetector*);
    virtual ~StrawPlaneV(void) {};

  private:
    void setRotation(void);
};

#endif /* STRAWPLANEX_H_ */
