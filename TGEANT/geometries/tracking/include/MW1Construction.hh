#ifndef MW1CONSTRUCTION_HH_
#define MW1CONSTRUCTION_HH_

#include "T4MDTPlane.hh"
#include "T4BaseDetector.hh"
#include "G4Box.hh"
#include "G4SubtractionSolid.hh"

class MW1Construction : public T4BaseDetector
{
  public:
    MW1Construction(void);
    virtual ~MW1Construction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    T4SMW1* mw1;

    void constructAbsorber(G4LogicalVolume*);

    std::vector<T4MDTPlane*> planes;

    std::vector<G4LogicalVolume*> log_edges;

    G4double absorberBoxBig[3];
    G4double absorberBoxHole[3];
    G4double absorberBoxEdges[3];
    G4double absorberTBEdgeShift;
    G4Box* absorberBox_big;
    G4Box* absorberBox_hole;
    G4Box* absorberBox_edges;

    G4SubtractionSolid* absorberBox_box;
    G4LogicalVolume* absorberBox_log;
};

#endif /* MW1CONSTRUCTION_HH_ */
