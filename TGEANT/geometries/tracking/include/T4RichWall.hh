#ifndef T4RICHWALL_HH_
#define T4RICHWALL_HH_

#include "T4BaseDetector.hh"
#include "T4MDTPlane.hh"

class T4RichWall : public T4BaseDetector
{
  public:
    T4RichWall(void) {};
    virtual ~T4RichWall(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<T4MDTPlane*> planes;
};

#endif /* T4RICHWALL_HH_ */
