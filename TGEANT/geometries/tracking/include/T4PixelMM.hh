#ifndef T4PIXELMM_HH_
#define T4PIXELMM_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"

class T4PixelMM : public T4BaseDetector
{
  public:
    T4PixelMM(const T4SDetector*);
    virtual ~T4PixelMM(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    vector<G4Box*> box;
    vector<G4Tubs*> tubs;
    vector<G4LogicalVolume*> log;
    CLHEP::HepRotation* rot;

    const T4SDetector* det;
    static int detectorIdCounter;
    G4int detectorId[5];

    G4bool isX;
    G4bool isY;
    G4bool isU;
    G4bool isV;
    G4String tbNamePixel;
    G4double zMult;
    G4double coordStrip[2];
    G4double coordPixel[2];
};

#endif /* T4PIXELMM_HH_ */
