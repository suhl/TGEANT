#ifndef T4W45_HH_
#define T4W45_HH_

#include "T4BaseDetector.hh"

#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

class T4W45Plane : public T4BaseDetector
{
  public:
    T4W45Plane(const T4SDetector* t4SDetector, G4int detectorId);
    virtual ~T4W45Plane(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  protected:
    G4Box* sdVolume_box;
    G4LogicalVolume* sdVolume_log[2];

    virtual void constructWire(void) = 0;
    G4double size[3];
    G4double angle;
    G4int nWires;
    G4double pitch;
    G4double sensWireDiameter;
    G4double fieldWireDiameter;
    G4double cathodeWireDiameter;
    G4double wireDist[2];

    G4Tubs* sensWire;
    G4Tubs* fieldWire;
    G4LogicalVolume* sensWire_log;
    G4LogicalVolume* fieldWire_log;
    G4RotationMatrix* sensWireRot;

  private:
    G4String detectorName[2];
    G4int detectorId[2];

    G4String getDetName(G4String tbName);
    static int counterDet[6]; // counter for det name

    G4double deadDiameter;

    G4Box* mylarHull_box;
    G4Box* gasVolume_box;
    G4Box* cathodeVolume_box;
    G4LogicalVolume* gasVolume_log;
    G4LogicalVolume* cathodeVolume_log;
    G4LogicalVolume* mylarHull_log;

    void constructFrame(G4LogicalVolume*);
    G4Box* outerFrame_box;
    G4Box* innerFrame_box;
    G4SubtractionSolid* frame_sub;
    G4LogicalVolume* frame_log;

    T4SWireDetector wire1;
    T4SWireDetector wire2;
    T4SDeadZone dead1;
    T4SDeadZone dead2;
};

class T4W45PlaneX : public T4W45Plane
{
  public:
    T4W45PlaneX(const T4SDetector* t4SDetector, G4int detectorId);
    virtual ~T4W45PlaneX(void) {}

  private:
    void constructWire(void);
};

class T4W45PlaneY : public T4W45Plane
{
  public:
    T4W45PlaneY(const T4SDetector* t4SDetector, G4int detectorId);
    virtual ~T4W45PlaneY(void) {}

  private:
    void constructWire(void);
};

class T4W45PlaneUV : public T4W45Plane
{
  public:
    T4W45PlaneUV(const T4SDetector* t4SDetector, G4int detectorId);
    virtual ~T4W45PlaneUV(void) {}

  private:
    void constructWire(void);
    vector<G4IntersectionSolid*> intersection_solid;
    vector<G4LogicalVolume*> intersection_log;
};

class T4W45 : public T4BaseDetector
{
  public:
    T4W45(void) {};
    ~T4W45(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

  private:
    std::vector<T4W45Plane*> w45Planes;
};

#endif /* T4W45_HH_ */
