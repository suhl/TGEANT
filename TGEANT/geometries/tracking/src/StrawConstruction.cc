#include "StrawConstruction.hh"

StrawConstruction::~StrawConstruction(void)
{
  for (unsigned int i = 0; i < strawPlanes.size(); i++)
    delete strawPlanes.at(i);
  strawPlanes.clear();
}

void StrawConstruction::construct(G4LogicalVolume* world_log)
{
  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getStraw()->size(); i++) {
    const T4SDetector* straw = &settingsFile->getStructManager()->getStraw()->at(i);

    switch (straw->name[4]) {
      case 'X':
        strawPlanes.push_back(new StrawPlaneX(straw));
        break;
      case 'Y':
        strawPlanes.push_back(new StrawPlaneY(straw));
        break;
      case 'U':
        strawPlanes.push_back(new StrawPlaneU(straw));
        break;
      case 'V':
        strawPlanes.push_back(new StrawPlaneV(straw));
        break;
      default:
        T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
            __FILE__,
            "StrawConstruction::construct: Unknown Straw type with name: "
                + straw->name);
    }
    strawPlanes.back()->construct(world_log);
  }
}

void StrawConstruction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < strawPlanes.size(); i++)
    strawPlanes.at(i)->getWireDetDat(wireDet, deadZone);
}
