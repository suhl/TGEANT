#include "DCMechanicalStructure.hh"

DCMechanicalStructure::DCMechanicalStructure(void)
{
  planeSize = NULL;
  zOffset = 0;

  isDC04 = false;

  alWindow_box = NULL;
  alWindow_log = NULL;
  mylarWindow_box = NULL;
  mylarWindow_log = NULL;
  rotation35p = NULL;
  rotation35n = NULL;
  bar_box = NULL;
  barBox_box = NULL;
  bar_intersection[0] = NULL;
  bar_intersection[1] = NULL;
  bar_intersection[2] = NULL;
  bar_intersection[3] = NULL;
  bar_log[0] = NULL;
  bar_log[1] = NULL;
  bar_log[2] = NULL;
  bar_log[3] = NULL;
  outerFrame_box = NULL;
  mainFrameComplete_box = NULL;
  outerFrame_sub = NULL;
  outerFrame_log = NULL;
  mainFrameSteel_box = NULL;
  mainFrameG11_box = NULL;
  innerFrame_box = NULL;
  mainFrameSteel_sub = NULL;
  mainFrameG11_sub = NULL;
  mainFrameSteel_log = NULL;
  mainFrameG11_log = NULL;
  frameHolding_box = NULL;
  frameHolding_log = NULL;
  rotationChain = NULL;
  lowerChain_tubs = NULL;
  lowerChain_log = NULL;
  middleBar_box = NULL;
  middleBar_log = NULL;
  sideBlock_box = NULL;
  sideBlock_log = NULL;
  upperChain_tubs = NULL;
  upperChain_log = NULL;
  redHoldingBlock_box[0] = NULL;
  redHoldingBlock_box[1] = NULL;
  redHoldingBlock_box[2] = NULL;
  redHoldingBlock_sub = NULL;
  redBar_union[0] = NULL;
  redBar_union[1] = NULL;
  redBar_log = NULL;
  yellowBar_box = NULL;
  yellowBar_log = NULL;
}

DCMechanicalStructure::~DCMechanicalStructure(void)
{
  if (alWindow_box != NULL) delete alWindow_box;
  if (alWindow_log != NULL) delete alWindow_log;
  if (mylarWindow_box != NULL) delete mylarWindow_box;
  if (mylarWindow_log != NULL) delete mylarWindow_log;
  if (rotation35p != NULL) delete rotation35p;
  if (rotation35n != NULL) delete rotation35n;
  if (bar_box != NULL) delete bar_box;
  if (barBox_box != NULL) delete barBox_box;
  if (bar_intersection[0] != NULL) delete bar_intersection[0];
  if (bar_intersection[1] != NULL) delete bar_intersection[1];
  if (bar_intersection[2] != NULL) delete bar_intersection[2];
  if (bar_intersection[3] != NULL) delete bar_intersection[3];
  if (bar_log[0] != NULL) delete bar_log[0];
  if (bar_log[1] != NULL) delete bar_log[1];
  if (bar_log[2] != NULL) delete bar_log[2];
  if (bar_log[3] != NULL) delete bar_log[3];
  if (outerFrame_box != NULL) delete outerFrame_box;
  if (mainFrameComplete_box != NULL) delete mainFrameComplete_box;
  if (outerFrame_sub != NULL) delete outerFrame_sub;
  if (outerFrame_log != NULL) delete outerFrame_log;
  if (mainFrameSteel_box != NULL) delete mainFrameSteel_box;
  if (mainFrameG11_box != NULL) delete mainFrameG11_box;
  if (innerFrame_box != NULL) delete innerFrame_box;
  if (mainFrameSteel_sub != NULL) delete mainFrameSteel_sub;
  if (mainFrameG11_sub != NULL) delete mainFrameG11_sub;
  if (mainFrameSteel_log != NULL) delete mainFrameSteel_log;
  if (mainFrameG11_log != NULL) delete mainFrameG11_log;
  if (frameHolding_box != NULL) delete frameHolding_box;
  if (frameHolding_log != NULL) delete frameHolding_log;
  if (rotationChain != NULL) delete rotationChain;
  if (lowerChain_tubs != NULL) delete lowerChain_tubs;
  if (lowerChain_log != NULL) delete lowerChain_log;
  if (middleBar_box != NULL) delete middleBar_box;
  if (middleBar_log != NULL) delete middleBar_log;
  if (sideBlock_box != NULL) delete sideBlock_box;
  if (sideBlock_log != NULL) delete sideBlock_log;
  if (upperChain_tubs != NULL) delete upperChain_tubs;
  if (upperChain_log != NULL) delete upperChain_log;
  if (redHoldingBlock_box[0] != NULL) delete redHoldingBlock_box[0];
  if (redHoldingBlock_box[1] != NULL) delete redHoldingBlock_box[1];
  if (redHoldingBlock_box[2] != NULL) delete redHoldingBlock_box[2];
  if (redHoldingBlock_sub != NULL) delete redHoldingBlock_sub;
  if (redBar_union[0] != NULL) delete redBar_union[0];
  if (redBar_union[1] != NULL) delete redBar_union[1];
  if (redBar_log != NULL) delete redBar_log;
  if (yellowBar_box != NULL) delete yellowBar_box;
  if (yellowBar_log != NULL) delete yellowBar_log;
}

void DCMechanicalStructure::construct(G4LogicalVolume* world_log)
{
  if (planeSize == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "DCMechanicalStructure::construct: Unknown plane size. Call function void setPlaneSize(const G4double* _planeSize) first.");
    return;
  }

  G4double alWindowThickness = 25.0 / 2 * CLHEP::micrometer;
  G4double mylarWindowThickness = 40.0 / 2 * CLHEP::micrometer;

  // entrance and exit window
  alWindow_box = new G4Box("alWindow_box", planeSize[0], planeSize[1],
      alWindowThickness);
  mylarWindow_box = new G4Box("mylarWindow_box", planeSize[0], planeSize[1],
      mylarWindowThickness);

  alWindow_log = new G4LogicalVolume(alWindow_box, materials->aluminium_noOptical,
      "alWindow_log", 0, 0, 0, 0);
  alWindow_log->SetVisAttributes(colour->invisible);
  mylarWindow_log = new G4LogicalVolume(mylarWindow_box, materials->aluminium_noOptical,
      "mylarWindow_log", 0, 0, 0, 0);
  mylarWindow_log->SetVisAttributes(colour->invisible);

  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, 0,
              zOffset - planeSize[2] - 2. * mylarWindowThickness
                  - alWindowThickness), alWindow_log, "alWindowFront_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, 0,
              zOffset + planeSize[2] + 2. * mylarWindowThickness
                  + alWindowThickness), alWindow_log, "alWindowBack_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, 0, zOffset - planeSize[2] - mylarWindowThickness),
      mylarWindow_log, "mylarWindowFront_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, 0, zOffset + planeSize[2] + mylarWindowThickness),
      mylarWindow_log, "mylarWindowBack_phys", world_log, 0, 0, checkOverlap);

  // values for stainless steel frame
  G4double barsThickness = 2.5 / 2. * CLHEP::cm;
  G4double distToBar = planeSize[2] + 2. * mylarWindowThickness
      + 2. * alWindowThickness + barsThickness;

  // 8 stainless steel reinforcements for the main frame with trapezoidal shape
  if (isDC04) {
    G4double barBoxDimension[2];
    barBoxDimension[0] = 64.0 / 2. * CLHEP::cm;
    barBoxDimension[1] = 91.4 / 2. * CLHEP::cm;
    G4double barsY = 10.0 / 2. * CLHEP::cm;
    G4double barsAngle = 55.0 * CLHEP::deg;

    bar_box = new G4Box("bar_box", 150.0 * CLHEP::cm / 2, barsY, barsThickness);
    barBox_box = new G4Box("barBox_box", barBoxDimension[0], barBoxDimension[1],
        barsThickness);

    rotation35p = new CLHEP::HepRotation;
    rotation35p->rotateZ(barsAngle);
    rotation35n = new CLHEP::HepRotation;
    rotation35n->rotateZ(-barsAngle);

    G4double dx = barsY * sin(barsAngle);
    G4double dy = barsY * cos(barsAngle);

    // jura-top
    bar_intersection[0] = new G4IntersectionSolid("bar_intersection[0]",
        barBox_box, bar_box, rotation35p, G4ThreeVector(dx, dy, 0));
    // jura-bottom
    bar_intersection[1] = new G4IntersectionSolid("bar_intersection[1]",
        barBox_box, bar_box, rotation35n, G4ThreeVector(dx, -dy, 0));
    // saleve-top
    bar_intersection[2] = new G4IntersectionSolid("bar_intersection[2]",
        barBox_box, bar_box, rotation35n, G4ThreeVector(-dx, dy, 0));
    // saleve-bottom
    bar_intersection[3] = new G4IntersectionSolid("bar_intersection[3]",
        barBox_box, bar_box, rotation35p, G4ThreeVector(-dx, -dy, 0));

    for (unsigned int i = 0; i < 4; i++) {
      bar_log[i] = new G4LogicalVolume(bar_intersection[i],
          materials->stainlessSteel, "bar_log[i]", 0, 0, 0, 0);
      bar_log[i]->SetVisAttributes(colour->black);
    }

    // jura-top
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(planeSize[0] - barBoxDimension[0],
                planeSize[1] - barBoxDimension[1], zOffset - distToBar),
        bar_log[0], "barsFront_phys[0]", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(planeSize[0] - barBoxDimension[0],
                planeSize[1] - barBoxDimension[1], zOffset + distToBar),
        bar_log[0], "barsBack_phys[0]", world_log, 0, 0, checkOverlap);
    // jura-bottom
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(planeSize[0] - barBoxDimension[0],
                -planeSize[1] + barBoxDimension[1], zOffset - distToBar),
        bar_log[1], "barsFront_phys[1]", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(planeSize[0] - barBoxDimension[0],
                -planeSize[1] + barBoxDimension[1], zOffset + distToBar),
        bar_log[1], "barsBack_phys[1]", world_log, 0, 0, checkOverlap);
    // saleve-top
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(-planeSize[0] + barBoxDimension[0],
                planeSize[1] - barBoxDimension[1], zOffset - distToBar),
        bar_log[2], "barsFront_phys[2]", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(-planeSize[0] + barBoxDimension[0],
                planeSize[1] - barBoxDimension[1], zOffset + distToBar),
        bar_log[2], "barsBack_phys[2]", world_log, 0, 0, checkOverlap);
    // saleve-bottom
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(-planeSize[0] + barBoxDimension[0],
                -planeSize[1] + barBoxDimension[1], zOffset - distToBar),
        bar_log[3], "barsFront_phys[3]", world_log, 0, 0, checkOverlap);
    new G4PVPlacement(0,
        positionVector
            + G4ThreeVector(-planeSize[0] + barBoxDimension[0],
                -planeSize[1] + barBoxDimension[1], zOffset + distToBar),
        bar_log[3], "barsBack_phys[3]", world_log, 0, 0, checkOverlap);
  }

  // outer frame with a material mixture for the surrounding electronics front-end box
  // and main frame with 2*2.5 cm stainless steel with G11 in between

  G4double mainFrameDimension[2];
  mainFrameDimension[0] = 218.0 / 2 * CLHEP::cm;
  mainFrameDimension[1] = 165.0 / 2 * CLHEP::cm;
  if (isDC04) {
    mainFrameDimension[0] = 294.0 / 2 * CLHEP::cm;
    mainFrameDimension[1] = 254.0 / 2 * CLHEP::cm;
  }

  G4double outerFrameDimension[2];
  outerFrameDimension[0] = mainFrameDimension[0] + 80.0 / 2 * CLHEP::cm;
  outerFrameDimension[1] = mainFrameDimension[1] + 80.0 / 2 * CLHEP::cm;

  G4double zLengthFrame = planeSize[2] + 2. * mylarWindowThickness
      + 2. * alWindowThickness + 2. * barsThickness;

  outerFrame_box = new G4Box("outerFrame_box", outerFrameDimension[0],
      outerFrameDimension[1], zLengthFrame);
  mainFrameComplete_box = new G4Box("mainFrameComplete_box",
      mainFrameDimension[0], mainFrameDimension[1],
      zLengthFrame + 1.0 * CLHEP::mm);
  outerFrame_sub = new G4SubtractionSolid("outerFrame_sub", outerFrame_box,
      mainFrameComplete_box);
  outerFrame_log = new G4LogicalVolume(outerFrame_sub,
      materials->dcEBox, "outerFrame_log", 0, 0, 0, 0);
  outerFrame_log->SetVisAttributes(colour->silver);

  innerFrame_box = new G4Box("innerFrame_box", planeSize[0], planeSize[1],
      zLengthFrame);
  mainFrameSteel_box = new G4Box("mainFrameSteel_box", mainFrameDimension[0],
      mainFrameDimension[1], barsThickness);
  mainFrameSteel_sub = new G4SubtractionSolid("mainFrameSteel_sub",
      mainFrameSteel_box, innerFrame_box);
  mainFrameSteel_log = new G4LogicalVolume(mainFrameSteel_sub,
      materials->stainlessSteel, "mainFrameSteel_log", 0, 0, 0, 0);
  mainFrameSteel_log->SetVisAttributes(colour->black);

  mainFrameG11_box = new G4Box("mainFrameG11_box", mainFrameDimension[0],
      mainFrameDimension[1], zLengthFrame - 2. * barsThickness);
  mainFrameG11_sub = new G4SubtractionSolid("mainFrameG11_sub",
      mainFrameG11_box, innerFrame_box);
  mainFrameG11_log = new G4LogicalVolume(mainFrameG11_sub, materials->g11,
      "mainFrameG11_log", 0, 0, 0, 0);
  mainFrameG11_log->SetVisAttributes(colour->silver);

  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, zOffset),
      outerFrame_log, "outerFrame_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, zOffset),
      mainFrameG11_log, "mainFrameG11_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, 0, zOffset - distToBar),
      mainFrameSteel_log, "mainFrameSteelFront_log", world_log, 0, 0,
      checkOverlap);
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, 0, zOffset + distToBar),
      mainFrameSteel_log, "mainFrameSteelBack_log", world_log, 0, 0,
      checkOverlap);

  // upper support structure:
  // frame bar, two chains, middle bar, two blocks with chains, upper red bar and yellow support structure
  G4double frameHoldingThickness = 16.7 / 2. * CLHEP::cm;
  frameHolding_box = new G4Box("frameHolding_box", 2.0 * CLHEP::m,
      frameHoldingThickness, frameHoldingThickness);
  frameHolding_log = new G4LogicalVolume(frameHolding_box, materials->aluminium_noOptical,
      "frameHolding_log", 0, 0, 0, 0);
  frameHolding_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0, outerFrameDimension[1] + frameHoldingThickness,
              zOffset), frameHolding_log, "frameHolding_phys", world_log, 0, 0,
      checkOverlap);

  rotationChain = new CLHEP::HepRotation;
  rotationChain->rotateX(90 * CLHEP::deg);

  // lower chains
  G4double lowerChainLength;
  lowerChainLength = 119.5 / 2. * CLHEP::cm;
  if (isDC04) lowerChainLength = 75.0 / 2. * CLHEP::cm;
  G4double lowerChainDistance = 3.88 / 2. * CLHEP::m;
  G4double chainOuterRadius = 1.8 * CLHEP::cm;
  lowerChain_tubs = new G4Tubs("lowerChain_tubs", 0, chainOuterRadius,
      lowerChainLength, 0, 360 * CLHEP::deg);
  lowerChain_log = new G4LogicalVolume(lowerChain_tubs, materials->aluminium_noOptical,
      "lowerChain_log", 0, 0, 0, 0);
  lowerChain_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(rotationChain,
      positionVector
          + G4ThreeVector(lowerChainDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + lowerChainLength, zOffset), lowerChain_log,
      "lowerChain_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationChain,
      positionVector
          + G4ThreeVector(-lowerChainDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + lowerChainLength, zOffset), lowerChain_log,
      "lowerChain_phys", world_log, 0, 0, checkOverlap);

  // middle bar
  G4double middleBarThickness = 20.0 / 2. * CLHEP::cm;
  middleBar_box = new G4Box("middleBar_box", 2.0 * CLHEP::m, middleBarThickness,
      middleBarThickness);
  middleBar_log = new G4LogicalVolume(middleBar_box, materials->aluminium_noOptical,
      "middleBar_log", 0, 0, 0, 0);
  middleBar_log->SetVisAttributes(colour->silver/*red*/);
  new G4PVPlacement(rotationChain,
      positionVector
          + G4ThreeVector(0,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + middleBarThickness, zOffset),
      middleBar_log, "middleBar_phys", world_log, 0, 0, checkOverlap);

  // two side blocks
  G4double sideBlockLength = 23.0 / 2. * CLHEP::cm;
  G4double sideBlockDistance = 2.76 / 2. * CLHEP::m;
  sideBlock_box = new G4Box("sideBlock_box", middleBarThickness,
      sideBlockLength, middleBarThickness);
  sideBlock_log = new G4LogicalVolume(sideBlock_box, materials->aluminium_noOptical,
      "sideBlock_log", 0, 0, 0, 0);
  sideBlock_log->SetVisAttributes(colour->silver/*red*/);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(sideBlockDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + sideBlockLength, zOffset), sideBlock_log, "sideBlock_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(-sideBlockDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + sideBlockLength, zOffset), sideBlock_log, "sideBlock_phys",
      world_log, 0, 0, checkOverlap);

  // upper chains
  G4double upperChainLength = 30.0 / 2. * CLHEP::cm;
  upperChain_tubs = new G4Tubs("upperChain_tubs", 0, chainOuterRadius,
      upperChainLength, 0, 360.0 * CLHEP::deg);
  upperChain_log = new G4LogicalVolume(upperChain_tubs, materials->aluminium_noOptical,
      "upperChain_log", 0, 0, 0, 0);
  upperChain_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(rotationChain,
      positionVector
          + G4ThreeVector(sideBlockDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + 2. * sideBlockLength + upperChainLength, zOffset),
      upperChain_log, "upperChain_phys", world_log, 0, 0, checkOverlap);
  new G4PVPlacement(rotationChain,
      positionVector
          + G4ThreeVector(-sideBlockDistance,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + 2. * sideBlockLength + upperChainLength, zOffset),
      upperChain_log, "upperChain_phys", world_log, 0, 0, checkOverlap);

  // upper red bar
  G4double redBarThickness = 20.0 / 2. * CLHEP::cm;
  G4double redHoldingBlockZ = 30.0 / 2. * CLHEP::cm;
  redHoldingBlock_box[0] = new G4Box("redHoldingBlock_box[0]", redBarThickness,
      2. * redBarThickness, 20.0 * CLHEP::cm);
  redHoldingBlock_box[1] = new G4Box("redHoldingBlock_box[1]",
      redBarThickness + 1.0 * CLHEP::mm, redBarThickness, redHoldingBlockZ);
  redHoldingBlock_box[2] = new G4Box("redHoldingBlock_box[2]",
      sideBlockDistance - redBarThickness, redBarThickness, redBarThickness);
  redHoldingBlock_sub = new G4SubtractionSolid("redHoldingBlock_sub",
      redHoldingBlock_box[0], redHoldingBlock_box[1], 0,
      G4ThreeVector(0, redBarThickness, 0));
  redBar_union[0] = new G4UnionSolid("redBar_union[0]", redHoldingBlock_box[2],
      redHoldingBlock_sub, 0,
      G4ThreeVector(sideBlockDistance, redBarThickness, 0));
  redBar_union[1] = new G4UnionSolid("redBar_union[1]", redBar_union[0],
      redHoldingBlock_sub, 0,
      G4ThreeVector(-sideBlockDistance, redBarThickness, 0));
  redBar_log = new G4LogicalVolume(redBar_union[1], materials->aluminium_noOptical,
      "redBar_log", 0, 0, 0, 0);
  redBar_log->SetVisAttributes(colour->silver/*red*/);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + 2. * sideBlockLength + 2. * upperChainLength
                  + redBarThickness, zOffset), redBar_log, "upperBarRed_phys",
      world_log, 0, 0, 0);

  // yellow bar
  G4double yellowBarHeight = 60.0 / 2. * CLHEP::cm;
  yellowBar_box = new G4Box("yellowBar_box", 3.0 * CLHEP::m, yellowBarHeight,
      redHoldingBlockZ);
  yellowBar_log = new G4LogicalVolume(yellowBar_box, materials->aluminium_noOptical,
      "yellowBar_log", 0, 0, 0, 0);
  yellowBar_log->SetVisAttributes(colour->silver_20/*yellow*/);
  new G4PVPlacement(0,
      positionVector
          + G4ThreeVector(0,
              outerFrameDimension[1] + 2. * frameHoldingThickness
                  + 2. * lowerChainLength + 2. * middleBarThickness
                  + 2. * sideBlockLength + 2. * upperChainLength
                  + 3. * redBarThickness + yellowBarHeight, zOffset),
      yellowBar_log, "yellowBar_phys", world_log, 0, 0, checkOverlap);
}

