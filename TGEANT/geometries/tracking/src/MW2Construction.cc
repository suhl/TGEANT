#include "MW2Construction.hh"

MW2Construction::MW2Construction(void)
{
  mw2 = settingsFile->getStructManager()->getMW2();
  positionVector = G4ThreeVector(mw2->general.position[0], mw2->general.position[1],
      mw2->general.position[2]);

  dimensionsAbsorber[0] = 800.0 / 2. * CLHEP::cm;
  dimensionsAbsorber[1] = 420.0 / 2. * CLHEP::cm;
  dimensionsAbsorber[2] = 240.0 / 2. * CLHEP::cm;

  dimensionsHole[0] = 70.0 / 2. * CLHEP::cm;
  dimensionsHole[1] = 60.0 / 2. * CLHEP::cm;

  absorber_box = NULL;
  hole_box = NULL;
  absorber_sub = NULL;
  absorber_log = NULL;
}

MW2Construction::~MW2Construction(void)
{
  if (absorber_box != NULL)
    delete absorber_box;
  if (hole_box != NULL)
    delete hole_box;
  if (absorber_sub != NULL)
    delete absorber_sub;
  if (absorber_log != NULL)
    delete absorber_log;

  for (unsigned int i = 0; i < mw2Planes.size(); i++)
    delete mw2Planes.at(i);
}

void MW2Construction::construct(G4LogicalVolume* world_log)
{
  if (mw2->useAbsorber) {
    absorber_box = new G4Box("absorber_box", dimensionsAbsorber[0],
        dimensionsAbsorber[1], dimensionsAbsorber[2]);
    hole_box = new G4Box("hole_box", dimensionsHole[0], dimensionsHole[1],
        dimensionsAbsorber[2] + 1.0 * CLHEP::mm);
    absorber_sub = new G4SubtractionSolid("absorber_sub", absorber_box,
        hole_box, 0, G4ThreeVector(37.0 * CLHEP::cm, 0, 0));
    absorber_log = new G4LogicalVolume(absorber_sub, materials->concrete,
        "absorber_log", 0, 0, 0, 0);
    absorber_log->SetVisAttributes(colour->darkgreen);
    new G4PVPlacement(0, positionVector, absorber_log, "absorber_phys",
        world_log, 0, 0,
        settingsFile->getStructManager()->getGeneral()->checkOverlap);

    regionManager->addToAbsorberRegion(absorber_log);
  }

  if (mw2->MB01X) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB01X1", "MBY", 1), MW2Plane::X));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB01X[0], mw2->positionMB01X[1],
            mw2->positionMB01X[2]));
  }

  if (mw2->MB01Y) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB01Y1", "MBZ", 1), MW2Plane::Y));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB01Y[0], mw2->positionMB01Y[1],
            mw2->positionMB01Y[2]));
  }

  if (mw2->MB01V) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB01V1", "MBV", 1), MW2Plane::V));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB01V[0], mw2->positionMB01V[1],
            mw2->positionMB01V[2]));
  }

  if (mw2->MB02X) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB02X2", "MBY", 2), MW2Plane::X));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB02X[0], mw2->positionMB02X[1],
            mw2->positionMB02X[2]));
  }

  if (mw2->MB02Y) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB02Y2", "MBZ", 2), MW2Plane::Y));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB02Y[0], mw2->positionMB02Y[1],
            mw2->positionMB02Y[2]));
  }

  if (mw2->MB02V) {
    mw2Planes.push_back(
        new MW2Plane(detIdent("MB02V2", "MBV", 2), MW2Plane::V));
    mw2Planes.back()->setPosition(
        G4ThreeVector(mw2->positionMB02V[0], mw2->positionMB02V[1],
            mw2->positionMB02V[2]));
  }

  for (unsigned int i = 0; i < mw2Planes.size(); i++)
    mw2Planes.at(i)->construct(world_log);
}

void MW2Construction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < mw2Planes.size(); i++)
    mw2Planes.at(i)->getWireDetDat(wireDet, deadZone);
}
