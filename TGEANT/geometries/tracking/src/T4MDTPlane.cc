#include "T4MDTPlane.hh"

T4MDTPlane::T4MDTPlane(T4DetIdent _detIdent, G4int _detectorId, PlaneTypeMDT _planeType)
{
  detIdent = _detIdent;
  detectorId = _detectorId;
  moduleRotation = new CLHEP::HepRotation;

  planeType = _planeType;
  setDimensions();

  moduleWidth = 0;
  detectorThickness = 0;
}

T4MDTPlane::~T4MDTPlane(void)
{
  delete moduleRotation;
  for (unsigned int i = 0; i < mdtModules.size(); i++)
    delete mdtModules.at(i);
  mdtModules.clear();
}

void T4MDTPlane::setDimensions(void)
{
  if (planeType == T4MDTPlane::MW1_X) {
    nModules = 57;
    nModulesHole = 17;
    lengthFull = 403.0 * CLHEP::cm; //408.6?
    lengthAroundHole = 156.75 * CLHEP::cm;

  } else if (planeType == T4MDTPlane::MW1_Y) {
    moduleRotation->rotateZ(90.0 * CLHEP::deg);
    nModules = 49;
    nModulesHole = 9;
    lengthFull = 471.0 * CLHEP::cm;
    lengthAroundHole = 161.0 * CLHEP::cm;

  } else if (planeType == T4MDTPlane::DR_X) {
    nModules = 62;
    nModulesHole = 12;
    lengthFull = 391.0 * CLHEP::cm;
    lengthAroundHole = 163.5 * CLHEP::cm;

  } else if (planeType == T4MDTPlane::DR_Y) {
    moduleRotation->rotateZ(90.0 * CLHEP::deg);
    nModules = 46;
    nModulesHole = 6;
    lengthFull = 513.5 * CLHEP::cm;
    lengthAroundHole = 199.25 * CLHEP::cm;
  }
}

void T4MDTPlane::construct(G4LogicalVolume* world_log)
{
  for (G4int i = 0; i < nModules + nModulesHole; i++)
    {
      mdtModules.push_back(new T4MDTModule());
      if(planeType == T4MDTPlane::DR_X || planeType == T4MDTPlane::DR_Y) mdtModules.at(i)->setAluThickness(0.44);
    }
  moduleWidth = mdtModules.at(0)->getFullWidth();
  detectorThickness = mdtModules.at(0)->getDetectorThickness();

  if (planeType == T4MDTPlane::MW1_X || planeType == T4MDTPlane::DR_X) {

    G4int moduleNo = 0;

    for (G4int i = 0; i < nModules; i++) {
      // build outer modules
      if (i < 0.5 * (nModules - nModulesHole)
          || i >= 0.5 * (nModules + nModulesHole)) {
        mdtModules.at(moduleNo)->setPosition(
            positionVector
                + G4ThreeVector((i - (nModules - 1) / 2) * moduleWidth, 0, 0));
        mdtModules.at(moduleNo)->setFullLength(lengthFull);
        moduleNo++;

        // build inner modules (top)
      } else {
        mdtModules.at(moduleNo)->setPosition(
            positionVector
                + G4ThreeVector((i - (nModules - 1) / 2) * moduleWidth,
                    lengthFull / 2 - lengthAroundHole / 2, 0));
        mdtModules.at(moduleNo)->setFullLength(lengthAroundHole);
        moduleNo++;
      }
    }

    // build inner modules (bottom)
    for (G4int i = 0; i < nModulesHole; i++) {
      mdtModules.at(moduleNo)->setPosition(
          positionVector
              + G4ThreeVector((i - (nModulesHole - 1) / 2) * moduleWidth,
                  -lengthFull / 2 + lengthAroundHole / 2, 0));
      mdtModules.at(moduleNo)->setFullLength(lengthAroundHole);
      moduleNo++;
    }


  } else if (planeType == T4MDTPlane::MW1_Y || planeType == T4MDTPlane::DR_Y) {

    G4int moduleNo = 0;

    for (G4int i = 0; i < nModules; i++) {
      // build outer modules
      if (i < 0.5 * (nModules - nModulesHole)
          || i >= 0.5 * (nModules + nModulesHole)) {
        mdtModules.at(moduleNo)->setPosition(
            positionVector
                + G4ThreeVector(0, (i - (nModules - 1) / 2) * moduleWidth, 0));
        mdtModules.at(moduleNo)->setFullLength(lengthFull);
        moduleNo++;

        // build inner modules (top)
      } else {
        mdtModules.at(moduleNo)->setPosition(
            positionVector
                + G4ThreeVector(lengthFull / 2 - lengthAroundHole / 2,
                    (i - (nModules - 1) / 2) * moduleWidth, 0));
        mdtModules.at(moduleNo)->setFullLength(lengthAroundHole);
        moduleNo++;
      }
    }

    // build inner modules (bottom)
    for (G4int i = 0; i < nModulesHole; i++) {
      mdtModules.at(moduleNo)->setPosition(
          positionVector
              + G4ThreeVector(-lengthFull / 2 + lengthAroundHole / 2,
                  (i - (nModulesHole - 1) / 2) * moduleWidth, 0));
      mdtModules.at(moduleNo)->setFullLength(lengthAroundHole);
      moduleNo++;
    }
  }

  G4int channelNo = 0;
  // set sensitive detector
  for (G4int i = 0; i < nModules + nModulesHole; i++) {
    mdtModules.at(i)->setRotation(moduleRotation);
    mdtModules.at(i)->construct(world_log);

    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
      for (G4int j = 0; j < mdtModules.at(i)->getNumChambers(); j++) {
        mdtModules.at(i)->getChamber(j)->SetSensitiveDetector(
            new T4SensitiveDetector(detIdent.tbName, channelNo, TGEANT::HIT,
                detectorId));
        channelNo++;
      }
  }
}

void T4MDTPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  T4SDeadZone dead;

  wire.id = dead.id = detectorId;
  wire.tbName = dead.tbName = detIdent.tbName;
  wire.det = dead.det = detIdent.detName;
  wire.unit = dead.unit = detIdent.unit;
  if(detIdent.tbName.at(0) == 'M')  wire.type = 1;
  else wire.type = 11;
  if (detIdent.tbName.at(4) == 'X') {
    wire.xSize = nModules * moduleWidth;
    wire.ySize = lengthFull;
    dead.xSize = nModulesHole * moduleWidth;
    dead.ySize = lengthFull - 2. * lengthAroundHole;
    wire.angle = 0;
  } else {
    wire.xSize = lengthFull;
    wire.ySize = nModules * moduleWidth;
    dead.xSize = lengthFull - 2. * lengthAroundHole;
    dead.ySize = nModulesHole * moduleWidth;
    wire.angle = 90;
  }

  wire.zSize = dead.zSize = detectorThickness;
  //if(detIdent.tbName.at(0) == 'D') wire.nWires = (nModules-nModulesHole) * 8 + nModulesHole * 16; //RW convention.
  /*else*/ wire.nWires = nModules * 8;
  wire.pitch = moduleWidth / 8.;//  10.*CLHEP::mm;Forcing right pitch
  wire.wireDist = -1.0 * wire.pitch * (wire.nWires - 1) / 2;


  wire.xCen = dead.xCen = positionVector[0];
  wire.yCen = dead.yCen = positionVector[1];
  wire.zCen = dead.zCen = positionVector[2];
  wire.rotMatrix = dead.rotMatrix = TGEANT::ROT_0DEG;
  dead.sh = 1;

  wireDet.push_back(wire);
  deadZone.push_back(dead);
}
