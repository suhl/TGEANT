#include "StrawPlane.hh"

int StrawPlane::nInstances = 0;
int StrawPlane::counterSTY[6] = { 1, 1, 1, 1, 1, 1 };
int StrawPlane::counterSTZ[6] = { 1, 1, 1, 1, 1, 1 };
int StrawPlane::counterSTU[6] = { 1, 1, 1, 1, 1, 1 };
int StrawPlane::counterSTV[6] = { 1, 1, 1, 1, 1, 1 };

StrawPlane::StrawPlane(const T4SDetector* straw)
{
  nInstances++;

  // u = up, d = down
  // a,b,c increasing with x/y axis (=> b = middle)
  detectorName_a[0] = straw->name + "ua";
  detectorName_a[1] = straw->name + "da";
  detectorName_b[0] = straw->name + "ub";
  detectorName_b[1] = straw->name + "db";
  detectorName_c[0] = straw->name + "uc";
  detectorName_c[1] = straw->name + "dc";
  mechName = straw->name;

  detectorId_a[0] = nInstances * 6 - 3; // starting with 3-8, 9-14, ...
  detectorId_b[0] = detectorId_a[0] + 1;
  detectorId_c[0] = detectorId_a[0] + 2;
  detectorId_a[1] = detectorId_a[0] + 3;
  detectorId_b[1] = detectorId_a[0] + 4;
  detectorId_c[1] = detectorId_a[0] + 5;

  useMechanicalStructure = straw->useMechanicalStructure;
  setPosition(straw->position);

  rotationPlane = new G4RotationMatrix;
  rotationTube = new G4RotationMatrix;

  mylarSize[2] = 40.0 / 2 * CLHEP::mm;
  carbonSize[2] = 0.5 / 2 * CLHEP::mm;

  outerRadiusBig = 9.654 / 2 * CLHEP::mm;
  outerRadiusSmall = 6.144 / 2 * CLHEP::mm;

  zPosSmall[0] = -3.325 * CLHEP::mm;
  zPosSmall[1] = 3.325 * CLHEP::mm;

  zPosBig[0] = -5.08 * CLHEP::mm;
  zPosBig[1] = 5.08 * CLHEP::mm;

  posOffsetSmall[0] = 0;
  posOffsetSmall[1] = outerRadiusSmall;

  posOffsetBig[0] = 0;
  posOffsetBig[1] = outerRadiusBig;

  mylar_box = NULL;
  mylar_log = NULL;
  nitrogen_box = NULL;
  nitrogen_log = NULL;
  frame_box = NULL;
  frame_sub = NULL;
  frame_log = NULL;
  carbon_box = NULL;
  carbon_log = NULL;

  for (unsigned int i = 0; i < 3; i++) {
    aluminium_tubs[i] = NULL;
    outerKapton_tubs[i] = NULL;
    glue_tubs[i] = NULL;
    innerKapton_tubs[i] = NULL;
    gas_tubs[i] = NULL;
    wire_tubs[i] = NULL;
  }

  nOuterWires = 0;
  nInnerShortWires = 0;
  holeLength = 0;
  strawLength = 0;
  nInnerLongWires = 0;
}

StrawPlane::~StrawPlane(void)
{
  nInstances--;
  if (mylar_box != NULL)
    delete mylar_box;
  if (mylar_log != NULL)
    delete mylar_log;
  if (nitrogen_box != NULL)
    delete nitrogen_box;
  if (nitrogen_log != NULL)
    delete nitrogen_log;
  if (frame_box != NULL)
    delete frame_box;
  if (frame_sub != NULL)
    delete frame_sub;
  if (frame_log != NULL)
    delete frame_log;
  if (carbon_box != NULL)
    delete carbon_box;
  if (carbon_log != NULL)
    delete carbon_log;

  for (unsigned int i = 0; i < 3; i++) {
    if (aluminium_tubs[i] != NULL)
      delete aluminium_tubs[i];
    if (outerKapton_tubs[i] != NULL)
      delete outerKapton_tubs[i];
    if (glue_tubs[i] != NULL)
      delete glue_tubs[i];
    if (innerKapton_tubs[i] != NULL)
      delete innerKapton_tubs[i];
    if (gas_tubs[i] != NULL)
      delete gas_tubs[i];
    if (wire_tubs[i] != NULL)
      delete wire_tubs[i];
  }

  for (unsigned int i = 0; i < aluminium_log.size(); i++)
    delete aluminium_log.at(i);
  aluminium_log.clear();
  for (unsigned int i = 0; i < outerKapton_log.size(); i++)
    delete outerKapton_log.at(i);
  outerKapton_log.clear();
  for (unsigned int i = 0; i < glue_log.size(); i++)
    delete glue_log.at(i);
  glue_log.clear();
  for (unsigned int i = 0; i < innerKapton_log.size(); i++)
    delete innerKapton_log.at(i);
  innerKapton_log.clear();
  for (unsigned int i = 0; i < sensitive_log.size(); i++)
    delete sensitive_log.at(i);
  sensitive_log.clear();
  for (unsigned int i = 0; i < wire_log.size(); i++)
    delete wire_log.at(i);
  wire_log.clear();

  delete rotationPlane;
  delete rotationTube;
}

void StrawPlane::construct(G4LogicalVolume* world_log)
{
  // outer hull of plane (mylar foil with nitrogen inside)
  G4double mylarThickness = 12.0 * CLHEP::micrometer;
  mylar_box = new G4Box("mylar_box", mylarSize[0], mylarSize[1], mylarSize[2]);
  mylar_log = new G4LogicalVolume(mylar_box, materials->mylar, "mylar_log");
  mylar_log->SetVisAttributes(colour->invisible);

  nitrogen_box = new G4Box("nitrogen_box", mylarSize[0], mylarSize[1],
      mylarSize[2] - mylarThickness);
  nitrogen_log = new G4LogicalVolume(nitrogen_box, materials->n2_noOptical,
      "nitrogen_log");
  nitrogen_log->SetVisAttributes(colour->invisible);

  new G4PVPlacement(rotationPlane, positionVector, mylar_log, mechName + "_mylar_phys",
      world_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), nitrogen_log, mechName + "_nitrogen_phys",
      mylar_log, 0, 0, checkOverlap);

  // construction of all straw tubes
  G4double alThickness = 0.05 * CLHEP::micrometer;
  G4double kaptonOuterThickness = 12.0 * CLHEP::micrometer;
  G4double glueThickness = 7.0 * CLHEP::micrometer;
  G4double kaptonInnerThickness = 40.0 * CLHEP::micrometer;
  G4double wireRadius = 30.0 / 2 * CLHEP::micrometer;

  // 0 for big straw in outer region
  // 1 for small straw in inner region (full length)
  // 2 for small straw in hole region
  G4double length[] =
      { strawLength, strawLength, (strawLength - holeLength) / 2 };
  G4double outerRadius[] =
      { outerRadiusBig, outerRadiusSmall, outerRadiusSmall };

  for (unsigned int i = 0; i < 3; i++) {
    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
      aluminium_tubs[i] = new G4Tubs("aluminium_tubs", 0, outerRadius[i],
          length[i], 0, 360 * CLHEP::deg);
    else
      aluminium_tubs[i] = new G4Box("aluminium_tubs", outerRadius[i]*0.99, outerRadius[i]*0.99,
          length[i]);
    outerKapton_tubs[i] = new G4Tubs("outerKapton_tubs", 0,
        outerRadius[i] - alThickness, length[i], 0, 360 * CLHEP::deg);
    glue_tubs[i] = new G4Tubs("glue_tubs", 0,
        outerRadius[i] - alThickness - kaptonOuterThickness, length[i], 0,
        360 * CLHEP::deg);
    innerKapton_tubs[i] = new G4Tubs("innerKapton_tubs", 0,
        outerRadius[i] - alThickness - kaptonOuterThickness - glueThickness,
        length[i], 0, 360 * CLHEP::deg);
    gas_tubs[i] = new G4Tubs("gas_tubs", 0,
        outerRadius[i] - alThickness - kaptonOuterThickness - glueThickness
            - kaptonInnerThickness, length[i], 0, 360 * CLHEP::deg);
    wire_tubs[i] = new G4Tubs("wire_tubs", 0, wireRadius, length[i], 0,
        360 * CLHEP::deg);
  }

  // x positions of upstream plane
  // 96 big, 95 small, 32/2 hole
  std::vector<G4double> positions;
  // small
  for (unsigned int i = 0; i < nInnerLongWires + nInnerShortWires / 2; i++)
    positions.push_back((2. * i + 1.) * outerRadiusSmall);
  // first big
  positions.push_back(positions.back() + outerRadiusSmall + outerRadiusBig);
  // big
  for (unsigned int i = 1; i < nOuterWires; i++)
    positions.push_back(positions.back() + 2. * outerRadiusBig);

  G4double posHole = length[1] - length[2];

  for (unsigned int i = 0; i < 2; i++) {
    // short straws
    for (unsigned int j = 0; j < nInnerShortWires / 2; j++) {
      buildStrawTube(
          getVector(positions.at(j) + posOffsetSmall[i], posHole, zPosSmall[i]),
          StrawPlane::SmallShort, detectorName_b[i], detectorId_b[i]);
      buildStrawTube(
          getVector(positions.at(j) + posOffsetSmall[i], -posHole,
              zPosSmall[i]), StrawPlane::SmallShort, detectorName_b[i],
          detectorId_b[i]);
      buildStrawTube(
          getVector(-positions.at(j) + posOffsetSmall[i], posHole,
              zPosSmall[i]), StrawPlane::SmallShort, detectorName_b[i],
          detectorId_b[i]);
      buildStrawTube(
          getVector(-positions.at(j) + posOffsetSmall[i], -posHole,
              zPosSmall[i]), StrawPlane::SmallShort, detectorName_b[i],
          detectorId_b[i]);
    }
    // small straws with full length
    for (unsigned int j = nInnerShortWires / 2;
        j < nInnerShortWires / 2 + nInnerLongWires; j++) {
      buildStrawTube(
          getVector(positions.at(j) + posOffsetSmall[i], 0, zPosSmall[i]),
          StrawPlane::SmallLong, detectorName_b[i], detectorId_b[i]);
      buildStrawTube(
          getVector(-positions.at(j) + posOffsetSmall[i], 0, zPosSmall[i]),
          StrawPlane::SmallLong, detectorName_b[i], detectorId_b[i]);
    }
    // big straws
    for (unsigned int j = nInnerShortWires / 2 + nInnerLongWires;
        j < nInnerShortWires / 2 + nInnerLongWires + nOuterWires; j++) {
      buildStrawTube(
          getVector(positions.at(j) + posOffsetBig[i], 0, zPosBig[i]),
          StrawPlane::Big, detectorName_c[i], detectorId_c[i]);
      // to avoid overlap in downstream plane: shift saleve side
      // overlap would be outerRadiusBig - outerRadiusSmall
      buildStrawTube(
          getVector(
              -positions.at(j) - (outerRadiusBig - outerRadiusSmall)
                  + posOffsetBig[i], 0, zPosBig[i]), StrawPlane::Big,
          detectorName_a[i], detectorId_a[i]);
    }
  }

  for (unsigned int i = 0; i < aluminium_log.size(); i++) {
    aluminium_log.at(i)->SetVisAttributes(colour->white);
    outerKapton_log.at(i)->SetVisAttributes(colour->invisible);
    glue_log.at(i)->SetVisAttributes(colour->invisible);
    innerKapton_log.at(i)->SetVisAttributes(colour->invisible);
    sensitive_log.at(i)->SetVisAttributes(colour->invisible);
    wire_log.at(i)->SetVisAttributes(colour->invisible);
  }

  if (useMechanicalStructure)
    buildMechanicalStructure(world_log);
}

void StrawPlane::buildStrawTube(G4ThreeVector position,
    StrawPlane::StrawSize strawSize, G4String planeName, G4int planeId)
{
  aluminium_log.push_back(
      new G4LogicalVolume(aluminium_tubs[strawSize],
          materials->aluminium_noOptical, "aluminium_log"));
  outerKapton_log.push_back(
      new G4LogicalVolume(outerKapton_tubs[strawSize], materials->kapton,
          "outerKapton_log"));
  glue_log.push_back(
      new G4LogicalVolume(glue_tubs[strawSize], materials->plastic,
          "glue_log")); //TODO glue material
  innerKapton_log.push_back(
      new G4LogicalVolume(innerKapton_tubs[strawSize], materials->kapton,
          "innerKapton_log"));
  sensitive_log.push_back(
      new G4LogicalVolume(gas_tubs[strawSize], materials->strawGas,
          "sensitive_log"));
  wire_log.push_back(
      new G4LogicalVolume(wire_tubs[strawSize], materials->tungsten, "wire_log"));

  new G4PVPlacement(rotationTube, position, aluminium_log.back(),
      planeName + "_aluminium_phys", nitrogen_log, 0, 0, checkOverlap);

  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), outerKapton_log.back(),
      planeName + "_outerKapton_phys", aluminium_log.back(), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), glue_log.back(),
      planeName + "_glue_phys", outerKapton_log.back(), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerKapton_log.back(),
      planeName + "_innerKapton_phys", glue_log.back(), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), sensitive_log.back(),
      planeName + "_sensitive_phys", innerKapton_log.back(), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), wire_log.back(),
      planeName + "_wire_phys", sensitive_log.back(), 0, 0, checkOverlap);

  sensitive_log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(planeName, sensitive_log.size() - 1, TGEANT::HIT,
          planeId));
}

void StrawPlane::buildMechanicalStructure(G4LogicalVolume* world_log)
{
  // frame (with rotation)
  G4double frameSize[3];
  frameSize[0] = mylarSize[0] + 418. / 2 * CLHEP::mm;
  frameSize[1] = mylarSize[1] + 418. / 2 * CLHEP::mm;
  frameSize[2] = mylarSize[2] - 0.5 * CLHEP::mm;

  frame_box = new G4Box("frame_box", frameSize[0], frameSize[1], frameSize[2]);
  frame_sub = new G4SubtractionSolid("frame_sub", frame_box, mylar_box);
  frame_log = new G4LogicalVolume(frame_sub, materials->aluminium_noOptical,
      "frame_log");
  frame_log->SetVisAttributes(colour->gray);
  new G4PVPlacement(rotationPlane, positionVector, frame_log, mechName + "_frame_phys",
      world_log, 0, 0, checkOverlap);

  // carbon strips (inside nitrogen volume)
  carbon_box = new G4Box("carbon_box", carbonSize[0], carbonSize[1],
      carbonSize[2]);
  carbon_log = new G4LogicalVolume(carbon_box, materials->carbonFibre,
      "carbon_log");
  carbon_log->SetVisAttributes(colour->black);
  // 10mm distance to middle: approximately zPosBig[1] + outerRadiusBig
  new G4PVPlacement(0,
      getVector(0, 90 * CLHEP::cm, -10 * CLHEP::mm - carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      getVector(0, 90 * CLHEP::cm, 10 * CLHEP::mm + carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);

  new G4PVPlacement(0,
      getVector(0, 30 * CLHEP::cm, -10 * CLHEP::mm - carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      getVector(0, 30 * CLHEP::cm, 10 * CLHEP::mm + carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);

  new G4PVPlacement(0,
      getVector(0, -30 * CLHEP::cm, -10 * CLHEP::mm - carbonSize[2]),
      carbon_log, mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      getVector(0, -30 * CLHEP::cm, 10 * CLHEP::mm + carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);

  new G4PVPlacement(0,
      getVector(0, -90 * CLHEP::cm, -10 * CLHEP::mm - carbonSize[2]),
      carbon_log, mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);
  new G4PVPlacement(0,
      getVector(0, -90 * CLHEP::cm, 10 * CLHEP::mm + carbonSize[2]), carbon_log,
      mechName + "_carbon_phys", nitrogen_log, 0, 0, checkOverlap);
}

void StrawPlane::setDefault(void)
{
  for (unsigned int i = 0; i < 2; i++) {
    wire_a[i].id = detectorId_a[i];
    wire_b[i].id = detectorId_b[i];
    wire_c[i].id = detectorId_c[i];
    dead[i].id = detectorId_b[i];

    wire_a[i].tbName = detectorName_a[i];
    wire_b[i].tbName = detectorName_b[i];
    wire_c[i].tbName = detectorName_c[i];
    dead[i].tbName = wire_b[i].tbName;

    setDetAndUnit(wire_a[i]);
    setDetAndUnit(wire_b[i]);
    setDetAndUnit(wire_c[i]);
    dead[i].det = wire_b[i].det;
    dead[i].unit = wire_b[i].unit;

    wire_a[i].type = wire_b[i].type = wire_c[i].type = 11;

    wire_a[i].zSize = wire_c[i].zSize = 2. * outerRadiusBig;
    wire_b[i].zSize = dead[i].zSize = 2. * outerRadiusSmall;

    wire_a[i].pitch = wire_c[i].pitch = 2. * outerRadiusBig;
    wire_b[i].pitch = 2. * outerRadiusSmall;

    wire_a[i].zCen = wire_c[i].zCen = zPosBig[i] + positionVector[2];
    wire_b[i].zCen = dead[i].zCen = zPosSmall[i] + positionVector[2];

    wire_a[i].nWires = wire_c[i].nWires = nOuterWires;
    wire_b[i].nWires = 2. * nInnerLongWires + nInnerShortWires;

    wire_a[i].wireDist = -1.0 * (wire_a[i].nWires - 1) / 2 * wire_a[i].pitch;
    wire_b[i].wireDist = -1.0 * (wire_b[i].nWires - 1) / 2 * wire_b[i].pitch;
    wire_c[i].wireDist = -1.0 * (wire_c[i].nWires - 1) / 2 * wire_c[i].pitch;
  }
}

void StrawPlane::setDetAndUnit(T4SWireDetector& wire)
{
  char type = wire.tbName[4];
  G4String subType = wire.tbName.substr(wire.tbName.size() - 2, 2);
  if (type == 'X') {
    if (subType == "ub") {
      wire.det = "STY1"; // yes, this is not an error: it is called Y in X-Plane!
      wire.unit = counterSTY[0];
      counterSTY[0]++;
    } else if (subType == "db") {
      wire.det = "STY2";
      wire.unit = counterSTY[1];
      counterSTY[1]++;
    } else if (subType == "uc") {
      wire.det = "STY3";
      wire.unit = counterSTY[2];
      counterSTY[2]++;
    } else if (subType == "dc") {
      wire.det = "STY4";
      wire.unit = counterSTY[3];
      counterSTY[3]++;
    } else if (subType == "ua") {
      wire.det = "STY5";
      wire.unit = counterSTY[4];
      counterSTY[4]++;
    } else if (subType == "da") {
      wire.det = "STY6";
      wire.unit = counterSTY[5];
      counterSTY[5]++;
    }
  } else if (type == 'Y') {
    if (subType == "ub") {
      wire.det = "STZ1";
      wire.unit = counterSTZ[0];
      counterSTZ[0]++;
    } else if (subType == "db") {
      wire.det = "STZ2";
      wire.unit = counterSTZ[1];
      counterSTZ[1]++;
    } else if (subType == "uc") {
      wire.det = "STZ3";
      wire.unit = counterSTZ[2];
      counterSTZ[2]++;
    } else if (subType == "dc") {
      wire.det = "STZ4";
      wire.unit = counterSTZ[3];
      counterSTZ[3]++;
    } else if (subType == "ua") {
      wire.det = "STZ5";
      wire.unit = counterSTZ[4];
      counterSTZ[4]++;
    } else if (subType == "da") {
      wire.det = "STZ6";
      wire.unit = counterSTZ[5];
      counterSTZ[5]++;
    }
  } else if (type == 'U') {
    if (subType == "ub") {
      wire.det = "STU1";
      wire.unit = counterSTU[0];
      counterSTU[0]++;
    } else if (subType == "db") {
      wire.det = "STU2";
      wire.unit = counterSTU[1];
      counterSTU[1]++;
    } else if (subType == "uc") {
      wire.det = "STU3";
      wire.unit = counterSTU[2];
      counterSTU[2]++;
    } else if (subType == "dc") {
      wire.det = "STU4";
      wire.unit = counterSTU[3];
      counterSTU[3]++;
    } else if (subType == "ua") {
      wire.det = "STU5";
      wire.unit = counterSTU[4];
      counterSTU[4]++;
    } else if (subType == "da") {
      wire.det = "STU6";
      wire.unit = counterSTU[5];
      counterSTU[5]++;
    }
  } else if (type == 'V') {
    if (subType == "ub") {
      wire.det = "STV1";
      wire.unit = counterSTV[0];
      counterSTV[0]++;
    } else if (subType == "db") {
      wire.det = "STV2";
      wire.unit = counterSTV[1];
      counterSTV[1]++;
    } else if (subType == "uc") {
      wire.det = "STV3";
      wire.unit = counterSTV[2];
      counterSTV[2]++;
    } else if (subType == "dc") {
      wire.det = "STV4";
      wire.unit = counterSTV[3];
      counterSTV[3]++;
    } else if (subType == "ua") {
      wire.det = "STV5";
      wire.unit = counterSTV[4];
      counterSTV[4]++;
    } else if (subType == "da") {
      wire.det = "STV6";
      wire.unit = counterSTV[5];
      counterSTV[5]++;
    }
  }
}
