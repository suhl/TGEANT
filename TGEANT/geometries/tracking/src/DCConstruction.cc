#include "DCConstruction.hh"

DCConstruction::~DCConstruction(void)
{
  for (unsigned int i = 0; i < dcPlanes.size(); i++)
    delete dcPlanes.at(i);
  dcPlanes.clear();
  for (unsigned int i = 0; i < dcMech.size(); i++)
    delete dcMech.at(i);
  dcMech.clear();
}

void DCConstruction::construct(G4LogicalVolume* world_log)
{
  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getDC()->size(); i++) {
    const T4SDetector* dc = &settingsFile->getStructManager()->getDC()->at(i);

    if (dc->name == "DC00") {
      dcPlanes.push_back(new DC00Plane());
      addDetector(dc);
    } else if (dc->name == "DC01") {
      dcPlanes.push_back(new DC01Plane());
      addDetector(dc);
    } else if (dc->name == "DC04") {
      dcPlanes.push_back(new DC04Plane());
      addDetector(dc);
      if (dc->useMechanicalStructure)
        dcMech.back()->setDC04();
    } else if (dc->name == "DC05") {
      dcPlanes.push_back(new DC05Plane());
      addDetector(dc);
      if (dc->useMechanicalStructure)
        dcMech.back()->setDC04();
    }
  }

  for (unsigned int i = 0; i < dcPlanes.size(); i++)
    dcPlanes.at(i)->construct(world_log);
  for (unsigned int i = 0; i < dcMech.size(); i++)
    dcMech.at(i)->construct(world_log);
}

void DCConstruction::addDetector(const T4SDetector* dc)
{
  dcPlanes.back()->setPosition(dc->position);
  if (dc->useMechanicalStructure) {
    dcMech.push_back(new DCMechanicalStructure());
    dcMech.back()->setPosition(dc->position);
    dcMech.back()->setPlaneSize(dcPlanes.back()->getSize());
    dcMech.back()->setZOffset(dcPlanes.back()->getZOffset());
  }
}

void DCConstruction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < dcPlanes.size(); i++)
    dcPlanes.at(i)->getWireDetDat(wireDet, deadZone);
}
