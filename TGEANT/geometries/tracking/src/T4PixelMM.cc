#include "T4PixelMM.hh"

int T4PixelMM::detectorIdCounter = 380;

T4PixelMM::T4PixelMM(const T4SDetector* _det)
{
  det = _det;
  setPosition(det->position);
  isX = (det->name[4] == 'X');
  isY = (det->name[4] == 'Y');
  isV = (det->name[4] == 'V');
  isU = (det->name[4] == 'U');

  tbNamePixel = det->name;
  tbNamePixel[4] = 'M';

  if (isX) {
    tbNamePixel[5] = 'X';
    rot = NULL;
    coordPixel[0] = 0.;
    coordPixel[1] = 0.;
    coordStrip[0] = 13.824 * CLHEP::cm;
    coordStrip[1] = 0;//-0.06 * CLHEP::cm;
    zMult = 1.0;
  } else  if (isY) {
    tbNamePixel[5] = 'Y';
    rot = new CLHEP::HepRotation;
    rot->rotateZ(-90.0 * CLHEP::deg);
    coordStrip[0] = 13.824 * CLHEP::cm;
    coordStrip[1] = 0;//0.074 * CLHEP::cm;
    coordPixel[0] = 0.;
    coordPixel[1] = 0.;
    zMult = -1.0;
  } else if (isV) {
    tbNamePixel[5] = 'V';
    rot = new CLHEP::HepRotation;
    rot->rotateZ(-45.0 * CLHEP::deg);
    coordStrip[0] = -13.824 * CLHEP::cm;
    coordStrip[1] = 0.;
    coordPixel[0] = -0.052 * CLHEP::cm;
    coordPixel[1] = -0.058 * CLHEP::cm;
    zMult = 1.0;
  } else if (isU) {
    tbNamePixel[5] = 'U';
    rot = new CLHEP::HepRotation;
    rot->rotateZ(45.0 * CLHEP::deg);
    coordStrip[0] = -13.824 * CLHEP::cm;
    coordStrip[1] = 0.;
    coordPixel[0] = 0.262 * CLHEP::cm;
    coordPixel[1] = -0.62 * CLHEP::cm;
    zMult = 1.0;
  }

  for (int i = 0; i < 5; i++)
    detectorId[i] = detectorIdCounter++;
}

void T4PixelMM::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 60. * CLHEP::cm, 70.0 * CLHEP::cm, 1.00500 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(rot, positionVector + G4ThreeVector(0, 0, 0.47 * CLHEP::cm * zMult), log.back(), "MP_air", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // Support Cu 5um +  FR4 200um + Rohacell 4mm + FR4 200um + Cu 10um
  // Rohacell in mother
  unsigned int rohacellIndex = log.size();
  box.push_back(new G4Box("rohacell_box", 32.0 * CLHEP::cm, 32.0 * CLHEP::cm, 0.22075 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.22075 * CLHEP::cm * zMult), log.back(), "MP_rohacell", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu in Rohacell
  box.push_back(new G4Box("cu_box", 32.0 * CLHEP::cm, 32.0 * CLHEP::cm, 0.00025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.22050 * CLHEP::cm * zMult), log.back(), "MP_cu", log.at(rohacellIndex), 0, 0, checkOverlap);

  // G10 in Rohacell
  box.push_back(new G4Box("g10_box", 32.0 * CLHEP::cm, 32.0 * CLHEP::cm, 0.01000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.21025 * CLHEP::cm * zMult), log.back(), "MP_g10", log.at(rohacellIndex), 0, 0, checkOverlap);

  unsigned int g10_forTubsIndex = log.size();
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.20975 * CLHEP::cm * zMult), log.back(), "MP_g10_2", log.at(rohacellIndex), 0, 0, checkOverlap);

  // Cu_2 in Rohacell
  unsigned int cu_forTubsIndex = log.size();
  box.push_back(new G4Box("cu_2_box", 32.0 * CLHEP::cm, 32.0 * CLHEP::cm, 0.00050 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.22025 * CLHEP::cm * zMult), log.back(), "MP_cu_2", log.at(rohacellIndex), 0, 0, checkOverlap);

  // 5cm diameter hole in Rohacell in beam area in 1Y position
  if (isY) {
    tubs.push_back(new G4Tubs("air_t1_tubs", 0, 2.5 * CLHEP::cm,  0.20 * CLHEP::cm, 0., 360.*CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_t1_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.00025 * CLHEP::cm), log.back(), "GEM_air_t1", log.at(rohacellIndex), 0, 0, checkOverlap);

    tubs.push_back(new G4Tubs("air_t2_tubs", 0, 2.5 * CLHEP::cm,  0.01 * CLHEP::cm, 0., 360.*CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_t2_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_air_t2", log.at(g10_forTubsIndex), 0, 0, checkOverlap);

    tubs.push_back(new G4Tubs("air_t3_tubs", 0, 2.5 * CLHEP::cm,  0.0005 * CLHEP::cm, 0., 360.*CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_t3_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_air_t3", log.at(cu_forTubsIndex), 0, 0, checkOverlap);
  }

  // Gas enclosure + top
  // Air in mother
  unsigned int airIndex = log.size();
  box.push_back(new G4Box("air_box", 26.8* CLHEP::cm, 27.0 * CLHEP::cm, 0.50250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.50250 * CLHEP::cm * zMult), log.back(), "MP_air2", log.at(motherIndex), 0, 0, checkOverlap);

  // CH2 in Air
  unsigned int ch2Index = log.size();
  box.push_back(new G4Box("ch2_box", 26.8 * CLHEP::cm, 27.0 * CLHEP::cm, 0.50000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->polyethylene, "ch2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.00250 * CLHEP::cm * zMult), log.back(), "MP_ch2", log.at(airIndex), 0, 0, checkOverlap);

  // Ne in CH2
  unsigned int neIndex = log.size();
  box.push_back(new G4Box("ne_box", 26.0 * CLHEP::cm, 26.0 * CLHEP::cm, 0.50000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne", log.at(ch2Index), 0, 0, checkOverlap);

  // Cu/Kap in Air
  box.push_back(new G4Box("cu_kap_box", 26.8 * CLHEP::cm, 27.0 * CLHEP::cm, 0.00250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_kap_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.5 * CLHEP::cm * zMult), log.back(), "MP_cu_kap", log.at(airIndex), 0, 0, checkOverlap);

  // frame + Mesh 14um 550% at 28um) steel at 120um
  // G10 in Ne
  unsigned int g10_3Index = log.size();
  box.push_back(new G4Box("g10_3_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.00600 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.49400 * CLHEP::cm * zMult), log.back(), "MP_g10_3", log.at(neIndex), 0, 0, checkOverlap);

  // Ne in G10
  box.push_back(new G4Box("ne_2_box", 22.0 * CLHEP::cm, 22.0 * CLHEP::cm, 0.00600 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne_2", log.at(g10_3Index), 0, 0, checkOverlap);

  // steel in Ne
  box.push_back(new G4Box("steel_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.00150 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->stainlessSteel, "steel_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.48650 * CLHEP::cm * zMult), log.back(), "MP_steel", log.at(neIndex), 0, 0, checkOverlap);

  // frame + GEM 50um at 2mm
  // G10 in Ne
  unsigned int g10_4Index = log.size();
  box.push_back(new G4Box("g10_4_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.10000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_4_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.38500 * CLHEP::cm * zMult), log.back(), "MP_g10_4", log.at(neIndex), 0, 0, checkOverlap);

  // Ne in G10
  box.push_back(new G4Box("ne_3_box", 22.0 * CLHEP::cm, 22.0 * CLHEP::cm, 0.10000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne_3", log.at(g10_4Index), 0, 0, checkOverlap);

  // Cu_Kap_2 in Ne
  box.push_back(new G4Box("cu_kap_2_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.00250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_kap_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.28250 * CLHEP::cm * zMult), log.back(), "MP_cu_kap_2", log.at(neIndex), 0, 0, checkOverlap);

  // frame + drift 10um at 5mm
  // G10 in Ne
  unsigned int g10_5Index = log.size();
  box.push_back(new G4Box("g10_5_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.25000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_5_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.03000 * CLHEP::cm * zMult), log.back(), "MP_g10_5", log.at(neIndex), 0, 0, checkOverlap);

  // Ne in G10
  unsigned int ne_4Index = log.size();
  box.push_back(new G4Box("ne_4_box", 22.0 * CLHEP::cm, 22.0 * CLHEP::cm, 0.25000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_4_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne_4", log.at(g10_5Index), 0, 0, checkOverlap);

  // Cu/10 in Ne
  box.push_back(new G4Box("cu_10_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.00050 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_10, "cu_10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.22050 * CLHEP::cm * zMult), log.back(), "MP_cu_10", log.at(neIndex), 0, 0, checkOverlap);

  // last frame 5mm
  // G10 in Ne
  // Following twos should have z = 0.25 but it leads to overlaps. Temporary solution
  unsigned int g10_6Index = log.size();
  box.push_back(new G4Box("g10_6_box", 23.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.13950 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_6_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.3605 * CLHEP::cm * zMult), log.back(), "MP_g10_6", log.at(neIndex), 0, 0, checkOverlap);

  // Ne in G10
  box.push_back(new G4Box("ne_5_box", 22.0 * CLHEP::cm, 22.0 * CLHEP::cm, 0.13950 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_5_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne_5", log.at(g10_6Index), 0, 0, checkOverlap);

  // Strip part gas MM
  // Ne_sens_1 in Ne_4
  unsigned int ne_sens_1Index = log.size();
  box.push_back(new G4Box("ne_sens_1_box", 7.68 * CLHEP::cm, 20.0 * CLHEP::cm, 0.16000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_sens_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MP_ne_sens_1", log.at(ne_4Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, detectorId[0]));

  // Ne_sens_2 in Ne_4
  box.push_back(new G4Box("ne_sens_2_box", 6.144 * CLHEP::cm, 20.0 * CLHEP::cm, 0.16000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_sens_2_log"));
  new G4PVPlacement(0, G4ThreeVector(coordStrip[0], coordStrip[1], 0), log.back(), "MP_ne_sens_2", log.at(ne_4Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 1, TGEANT::HIT, detectorId[1]));

  // Ne_sens_3 in Ne_4
  box.push_back(new G4Box("ne_sens_3_box", 6.144 * CLHEP::cm, 20.0 * CLHEP::cm, 0.16000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_sens_3_log"));
  new G4PVPlacement(0, G4ThreeVector(-coordStrip[0], -coordStrip[1], 0), log.back(), "MP_ne_sens_3", log.at(ne_4Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 2, TGEANT::HIT, detectorId[2]));

  // Pixel part gas MM
  // Ne_sens_4 in Ne_sens_1
  box.push_back(new G4Box("ne_sens_4_box", 2.56 * CLHEP::cm, 2.5 * CLHEP::cm, 0.16000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_sens_4_log"));
  new G4PVPlacement(0, G4ThreeVector(coordPixel[0], coordPixel[1], 0), log.back(), "MP_ne_sens_4", log.at(ne_sens_1Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(tbNamePixel, 0, TGEANT::HIT, detectorId[3], TGEANT::PMTDUMMY, detectorId[4]));

  // Support extension, upper/lower part Cu-kapton 50um + G10 200um + Rohacell 4mm + G10 200um
  // Rohacell_2 in mother
  unsigned int rohacell_2Index = log.size();
  box.push_back(new G4Box("rohacell_2_box", 32.0 * CLHEP::cm, 20.0 * CLHEP::cm, 0.22250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 42.5 * CLHEP::cm, 0.66400 * CLHEP::cm * zMult), log.back(), "MP_rohacell_2", log.at(motherIndex), 0, 0, checkOverlap);

  // Rohacell_3 in mother
  unsigned int rohacell_3Index = log.size();
  box.push_back(new G4Box("rohacell_3_box", 32.0 * CLHEP::cm, 20.0 * CLHEP::cm, 0.22250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -42.5 * CLHEP::cm, 0.66400 * CLHEP::cm * zMult), log.back(), "MP_rohacell_3", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu_Kap_2 in Rohacell_2
  box.push_back(new G4Box("cu_kap_3_box", 32.0 * CLHEP::cm, 20.0 * CLHEP::cm, 0.00250 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_kap_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.22000 * CLHEP::cm * zMult), log.back(), "MP_cu_kap_3", log.at(rohacell_2Index), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.22000 * CLHEP::cm * zMult), log.back(), "MP_cu_kap_3", log.at(rohacell_3Index), 0, 0, checkOverlap);

  // G10_7/8 in Rohacell_2
  box.push_back(new G4Box("g10_7_box", 32.0 * CLHEP::cm, 20.0 * CLHEP::cm, 0.01000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_7_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.20750 * CLHEP::cm * zMult), log.back(), "MP_g10_7", log.at(rohacell_2Index), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.21250 * CLHEP::cm * zMult), log.back(), "MP_g10_8", log.at(rohacell_2Index), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.20750 * CLHEP::cm * zMult), log.back(), "MP_g10_9", log.at(rohacell_3Index), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.21250 * CLHEP::cm * zMult), log.back(), "MP_g10_10", log.at(rohacell_3Index), 0, 0, checkOverlap);


  // Extension cards for electronics, lower part Cu-kapton 50um + Rohacell 4mm + G10 500um + some Cu
  // replaced by 750um of G10 (same rad length), 4 cards unioned in 1
  // Top & Bottom part of extension cards
  // G10_11 in mother
  box.push_back(new G4Box("g10_11_box", 2.35 * CLHEP::cm, 16.5 * CLHEP::cm, 0.03754 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_11_log"));
  new G4PVPlacement(0, G4ThreeVector(22.17 * CLHEP::cm, 43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_11", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(12.06 * CLHEP::cm, 43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_12", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-22.17 * CLHEP::cm, -43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_13", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-12.06 * CLHEP::cm, -43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_14", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_15 in mother
  box.push_back(new G4Box("g10_15_box", 9.915 * CLHEP::cm, 16.5 * CLHEP::cm, 0.03754 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_15_log"));
  new G4PVPlacement(0, G4ThreeVector(-2.52 * CLHEP::cm, 43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_15", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(2.52 * CLHEP::cm, -43.50 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_16", log.at(motherIndex), 0, 0, checkOverlap);

  // epoxy in mother
  box.push_back(new G4Box("epoxy_box", 1.75 * CLHEP::cm, 0.2 * CLHEP::cm, 0.30000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->epoxy, "epoxy_log"));
  new G4PVPlacement(0, G4ThreeVector(22.17 * CLHEP::cm, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(12.06 * CLHEP::cm, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_2", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(5.04 * CLHEP::cm, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_3", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_4", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-5.04 * CLHEP::cm, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_5", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-10.09 * CLHEP::cm, 28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_6", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-22.17 * CLHEP::cm, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_7", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-12.06 * CLHEP::cm, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_8", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-5.04 * CLHEP::cm, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_9", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_10", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(5.04 * CLHEP::cm, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_11", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(10.09 * CLHEP::cm, -28.48 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_12", log.at(motherIndex), 0, 0, checkOverlap);

  // Left & Right part of extension cards
  // G10_17 in mother
  box.push_back(new G4Box("g10_17_box", 16.5 * CLHEP::cm, 9.915 * CLHEP::cm, 0.03754 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_17_log"));
  new G4PVPlacement(0, G4ThreeVector(43.30 * CLHEP::cm, -2.52 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_17", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-43.30 * CLHEP::cm, 2.52 * CLHEP::cm * zMult, -0.63754 * CLHEP::cm * zMult), log.back(), "MP_g10_18", log.at(motherIndex), 0, 0, checkOverlap);

  // epoxy in mother
  box.push_back(new G4Box("epoxy_13_box", 0.2 * CLHEP::cm, 1.75 * CLHEP::cm, 0.30000 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->epoxy, "epoxy_13_log"));
  new G4PVPlacement(0, G4ThreeVector(28.35 * CLHEP::cm, -10.086 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(28.35 * CLHEP::cm, -5.043 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(28.35 * CLHEP::cm, 0, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(28.35 * CLHEP::cm, 5.043 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-28.35 * CLHEP::cm, 10.086 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-28.35 * CLHEP::cm, 5.043 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-28.35 * CLHEP::cm, 0, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-28.35 * CLHEP::cm, -5.043 * CLHEP::cm * zMult, -0.300 * CLHEP::cm * zMult), log.back(), "MP_epoxy_1", log.at(motherIndex), 0, 0, checkOverlap);

  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->green);
}

void T4PixelMM::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire[5];
  wire[0].nWires = 384;
  wire[1].nWires = wire[2].nWires = 256;
  wire[3].nWires = 128;
  wire[4].nWires = 40;

  wire[0].pitch = 0.040 * CLHEP::cm;
  wire[1].pitch = wire[2].pitch = 0.048 * CLHEP::cm;
  wire[3].pitch = 0.040 * CLHEP::cm;
  wire[4].pitch = 0.125 * CLHEP::cm;

  wire[0].angle = wire[1].angle = wire[2].angle = wire[3].angle = 0;
  wire[4].angle = 90;

  wire[0].xCen = wire[1].xCen = wire[2].xCen = positionVector[0];
  wire[3].xCen = wire[4].xCen = positionVector[0] + coordPixel[0];
  wire[0].yCen = wire[1].yCen = wire[2].yCen = positionVector[1];
  wire[3].yCen = wire[4].yCen = positionVector[1] + coordPixel[1];

    if(isX || isY){
      wire[1].xCen += coordStrip[static_cast<int>(isY)];
      wire[2].xCen -= coordStrip[static_cast<int>(isY)];
      wire[1].yCen += coordStrip[static_cast<int>(!isY)];
      wire[2].yCen -= coordStrip[static_cast<int>(!isY)];
    }
    else if(isU || isV){
      double rad = (-coordStrip[0]/sqrt(2));//This we use to get the right values
      if(isU){
      wire[1].xCen -= rad;
      wire[2].xCen += rad;
      wire[1].yCen -= rad;
      wire[2].yCen += rad;
      }
      if(isV){
      wire[1].xCen -= rad;
      wire[2].xCen += rad;
      wire[1].yCen += rad;
      wire[2].yCen -= rad;
      }
    }

  for (int i = 0; i < 3; i++) {
    wire[i].tbName = det->name;
    wire[i].xSize = wire[i].pitch * wire[i].nWires;
    wire[i].ySize = 40.0 * CLHEP::cm;
  }

  for (int i = 3; i < 5; i++) {
    wire[i].tbName = tbNamePixel;
    wire[i].xSize = wire[3].nWires * wire[3].pitch;
    wire[i].ySize = wire[4].nWires * wire[4].pitch;
  }

  stringstream myStream;
  myStream << det->name[3];

  for (int i = 0; i < 5; i++) {
    wire[i].id = detectorId[i];
    wire[i].unit = strToInt(myStream.str());
    wire[i].type = 30;
    wire[i].zSize = 0.32 * CLHEP::cm;

    wire[i].zCen = positionVector[2];
    wire[i].wireDist = -1.0 * wire[i].pitch * (wire[i].nWires - 1) / 2;

    if (isX) {
      wire[i].det = "PMX" + intToStr(i+1);
      wire[i].rotMatrix = TGEANT::ROT_0DEG;
    } else if (isY) {
      wire[i].det = "PMY" + intToStr(i+1);
      wire[i].rotMatrix = TGEANT::ROT_90pDEG;
    } else if (isU) {
      wire[i].det = "PMU" + intToStr(i+1);
      wire[i].rotMatrix = TGEANT::ROT_45pDEG;
    } else if (isV) {
      wire[i].det = "PMV" + intToStr(i+1);
      wire[i].rotMatrix = TGEANT::ROT_45nDEG;
    }

    wireDet.push_back(wire[i]);
  }

  T4SDeadZone dead;
  dead.id = detectorId[0];
  dead.tbName = det->name;
  if (isX) {
    dead.det = "PMX1";
    dead.rotMatrix = TGEANT::ROT_0DEG;
  } else if (isY) {
    dead.det = "PMY1";
    dead.rotMatrix = TGEANT::ROT_90pDEG;
  } else if (isU) {
    dead.det = "PMU1";
    dead.rotMatrix = TGEANT::ROT_45pDEG;
  } else if (isV) {
    dead.det = "PMV1";
    dead.rotMatrix = TGEANT::ROT_45nDEG;
  }
  dead.unit = wire[0].unit;
  dead.sh = 1;
  dead.zSize = 5.;
  dead.xSize = 44.8;
  dead.ySize = 37.5;
  dead.xCen = positionVector[0];
  dead.yCen = positionVector[1];
  dead.zCen = positionVector[2];

  deadZone.push_back(dead);
}

T4PixelMM::~T4PixelMM(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  if (rot != NULL)
    delete rot;
}
