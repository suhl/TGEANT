#include "MW1Construction.hh"

MW1Construction::MW1Construction(void)
{
  mw1 = settingsFile->getStructManager()->getMW1();

  absorberBoxBig[0] = 4.8 / 2. * CLHEP::m;
  absorberBoxBig[1] = 4.23 / 2. * CLHEP::m;
  absorberBoxBig[2] = 60.0 / 2. * CLHEP::cm;

  absorberBoxHole[0] = 140.0 / 2. * CLHEP::cm;
  absorberBoxHole[1] = 70.0 / 2. * CLHEP::cm;
  absorberBoxHole[2] = absorberBoxBig[2] + 1.0 * CLHEP::mm;

  absorberBoxEdges[0] = 158.0 / 2. * CLHEP::cm;
  absorberBoxEdges[1] = 7.0 / 2. * CLHEP::cm;
  absorberBoxEdges[2] = 60.0 / 2. * CLHEP::cm;

  absorberTBEdgeShift = 12. * CLHEP::cm;

  absorberBox_big = NULL;
  absorberBox_hole = NULL;
  absorberBox_box = NULL;
  absorberBox_log = NULL;
  absorberBox_edges = NULL;
}

MW1Construction::~MW1Construction(void)
{
  if (absorberBox_big != NULL)
    delete absorberBox_big;
  if (absorberBox_hole != NULL)
    delete absorberBox_hole;
  if (absorberBox_box != NULL)
    delete absorberBox_box;
  if (absorberBox_edges != NULL)
      delete absorberBox_edges;
  if (absorberBox_log != NULL)
    delete absorberBox_log;

  for (unsigned int j = 0; j < log_edges.size(); j++)
      delete log_edges.at(j);
  log_edges.clear();
  for (unsigned int i = 0; i < planes.size(); i++)
    delete planes.at(i);
  planes.clear();
}

void MW1Construction::construct(G4LogicalVolume* world_log)
{
  if (mw1->useAbsorber)
    constructAbsorber(world_log);

  if (mw1->useMA01X1) {
    planes.push_back(new T4MDTPlane(detIdent("MA01X1__", "MWA1", 1), 214, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
        G4ThreeVector(mw1->posMA01X1[0], mw1->posMA01X1[1], mw1->posMA01X1[2]));
  }

  if (mw1->useMA01X3) {
    planes.push_back(new T4MDTPlane(detIdent("MA01X3__", "MWA1", 2), 215, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
        G4ThreeVector(mw1->posMA01X3[0], mw1->posMA01X3[1], mw1->posMA01X3[2]));
  }

  if (mw1->useMA02X1) {
    planes.push_back(new T4MDTPlane(detIdent("MA02X1__", "MWA1", 3), 216, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02X1[0], mw1->posMA02X1[1], mw1->posMA02X1[2]));
  }

  if (mw1->useMA02X3) {
    planes.push_back(new T4MDTPlane(detIdent("MA02X3__", "MWA1", 4), 217, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
        G4ThreeVector(mw1->posMA02X3[0], mw1->posMA02X3[1], mw1->posMA02X3[2]));
  }

  if (mw1->useMA01X1) {
    planes.push_back(new T4MDTPlane(detIdent("MA01X2__", "MWA2", 1), 218, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
        G4ThreeVector(mw1->posMA01X1[0], mw1->posMA01X1[1],
            mw1->posMA01X1[2] + 5.3 * CLHEP::cm));
  }

  if (mw1->useMA01X3) {
    planes.push_back(new T4MDTPlane(detIdent("MA01X4__", "MWA2", 2), 219, T4MDTPlane::MW1_X));
    planes.back()->setPosition(
        G4ThreeVector(mw1->posMA01X3[0], mw1->posMA01X3[1],
            mw1->posMA01X3[2] + 5.3 * CLHEP::cm));
  }

  if (mw1->useMA02X1) {
    planes.push_back(new T4MDTPlane(detIdent("MA02X2__", "MWA2", 3), 220, T4MDTPlane::MW1_X));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02X1[0], mw1->posMA02X1[1],
             mw1->posMA02X1[2] + 5.3 * CLHEP::cm));
   }

   if (mw1->useMA02X3) {
     planes.push_back(new T4MDTPlane(detIdent("MA02X4__", "MWA2", 4), 221, T4MDTPlane::MW1_X));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02X3[0], mw1->posMA02X3[1],
             mw1->posMA02X3[2] + 5.3 * CLHEP::cm));
   }

   if (mw1->useMA01Y1) {
     planes.push_back(new T4MDTPlane(detIdent("MA01Y1__", "MWA3", 1), 222, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA01Y1[0], mw1->posMA01Y1[1], mw1->posMA01Y1[2]));
   }

   if (mw1->useMA01Y3) {
     planes.push_back(new T4MDTPlane(detIdent("MA01Y3__", "MWA3", 2), 223, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA01Y3[0], mw1->posMA01Y3[1], mw1->posMA01Y3[2]));
   }

   if (mw1->useMA02Y1) {
     planes.push_back(new T4MDTPlane(detIdent("MA02Y1__", "MWA3", 3), 224, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02Y1[0], mw1->posMA02Y1[1], mw1->posMA02Y1[2]));
   }

   if (mw1->useMA02Y3) {
     planes.push_back(new T4MDTPlane(detIdent("MA02Y3__", "MWA3", 4), 225, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02Y3[0], mw1->posMA02Y3[1], mw1->posMA02Y3[2]));
   }

   if (mw1->useMA01Y1) {
     planes.push_back(new T4MDTPlane(detIdent("MA01Y2__", "MWA4", 1), 226, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA01Y1[0], mw1->posMA01Y1[1],
             mw1->posMA01Y1[2] + 5.3 * CLHEP::cm));
   }

   if (mw1->useMA01Y3) {
     planes.push_back(new T4MDTPlane(detIdent("MA01Y4__", "MWA4", 2), 227, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA01Y3[0], mw1->posMA01Y3[1],
             mw1->posMA01Y3[2] + 5.3 * CLHEP::cm));
   }

   if (mw1->useMA02Y1) {
     planes.push_back(new T4MDTPlane(detIdent("MA02Y2__", "MWA4", 3), 228, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02Y1[0], mw1->posMA02Y1[1],
             mw1->posMA02Y1[2] + 5.3 * CLHEP::cm));
   }

   if (mw1->useMA02Y3) {
     planes.push_back(new T4MDTPlane(detIdent("MA02Y4__", "MWA4", 4), 229, T4MDTPlane::MW1_Y));
     planes.back()->setPosition(
         G4ThreeVector(mw1->posMA02Y3[0], mw1->posMA02Y3[1],
             mw1->posMA02Y3[2] + 5.3 * CLHEP::cm));
   }

  for (unsigned int i = 0; i < planes.size(); i++)
    planes.at(i)->construct(world_log);
}

void MW1Construction::constructAbsorber(G4LogicalVolume* world_log)
{
  absorberBox_big = new G4Box("absorberBox_big", absorberBoxBig[0], absorberBoxBig[1],
      absorberBoxBig[2]);
  absorberBox_hole = new G4Box("absorberBox_hole", absorberBoxHole[0],
      absorberBoxHole[1], absorberBoxHole[2]);
  absorberBox_box = new G4SubtractionSolid("absorberBox_box", absorberBox_big,
      absorberBox_hole, 0, G4ThreeVector(0, 0, 0));
  absorberBox_log = new G4LogicalVolume(absorberBox_box, materials->iron, "absorberBox_log");
  new G4PVPlacement(0,
      G4ThreeVector(mw1->general.position[0], mw1->general.position[1],
          mw1->general.position[2]), absorberBox_log, "MW1_absorberBox_phys", world_log, 0, 0,
      settingsFile->getStructManager()->getGeneral()->checkOverlap);
  absorberBox_log->SetVisAttributes(colour->darkgreen);

  absorberBox_edges = new G4Box("absorberBox_edges", absorberBoxEdges[0], absorberBoxEdges[1], absorberBoxEdges[2]);
  log_edges.push_back(new G4LogicalVolume(absorberBox_edges, materials->iron, "absorberBox_edges_log"));
  //Top tooth
  new G4PVPlacement(0,G4ThreeVector(mw1->general.position[0]+absorberBoxHole[0]+absorberBoxEdges[0]+absorberTBEdgeShift,
          mw1->general.position[1]+absorberBoxEdges[1]+absorberBoxBig[1],
          mw1->general.position[2]), log_edges.back(), "MW1_absorberBox_edges", world_log, 0, 0,
      settingsFile->getStructManager()->getGeneral()->checkOverlap);
  //Bottom tooth
  new G4PVPlacement(0,G4ThreeVector(mw1->general.position[0]-absorberBoxHole[0]-absorberBoxEdges[0]-absorberTBEdgeShift,
          mw1->general.position[1]-absorberBoxEdges[1]-absorberBoxBig[1],
          mw1->general.position[2]), log_edges.back(), "MW1_absorberBox_edges", world_log, 0, 0,
      settingsFile->getStructManager()->getGeneral()->checkOverlap);
  log_edges.back()->SetVisAttributes(colour->darkgreen);
  regionManager->addToAbsorberRegion(log_edges.back());
  regionManager->addToAbsorberRegion(absorberBox_log);

}

void MW1Construction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < planes.size(); i++)
    planes.at(i)->getWireDetDat(wireDet, deadZone);
}
