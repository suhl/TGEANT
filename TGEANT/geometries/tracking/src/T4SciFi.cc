#include "T4SciFi.hh"

T4FIPlane::T4FIPlane(void)
{
  firstDetectorId = 751;
  det = NULL;
  usePlane3 = false;

  dimSensV = 0;
  pitch = 0;
  nWires = 0;
  nWiresV = 0;
  plane2Dist = 0;
  plane3Dist = 0;
}

void T4SciFi::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 751;
  const T4SDetector* fi;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getSciFi()->size(); i++) {
    fi = &settingsFile->getStructManager()->getSciFi()->at(i);

    stringstream myStream;
    myStream << fi->name[2] << fi->name[3];
    int unit = strToInt(myStream.str());

    switch (unit) {
      case 1:
      case 2:
        fiPlane.push_back(new T4FIPlane01(fi, firstDetectorId));
        firstDetectorId += 2;
        break;
      case 15:
        fiPlane.push_back(new T4FIPlane15(fi, firstDetectorId));
        if (fi->useMechanicalStructure)
          firstDetectorId += 3;
        else
          firstDetectorId += 2;
        break;
      case 3:
        fiPlane.push_back(new T4FIPlane03(fi, firstDetectorId));
        firstDetectorId += 3;
        break;
      case 4:
        fiPlane.push_back(new T4FIPlane04(fi, firstDetectorId));
        firstDetectorId += 3;
        break;
      case 5:
        fiPlane.push_back(new T4FIPlane05(fi, firstDetectorId));
        firstDetectorId += 2;
        break;
      case 55:
        fiPlane.push_back(new T4FIPlane55(fi, firstDetectorId));
        firstDetectorId += 2;
        break;
      case 6:
        fiPlane.push_back(new T4FIPlane06(fi, firstDetectorId));
        firstDetectorId += 3;
        break;
      case 7:
        fiPlane.push_back(new T4FIPlane07(fi, firstDetectorId));
        firstDetectorId += 2;
        break;
      case 8:
        fiPlane.push_back(new T4FIPlane08(fi, firstDetectorId));
        firstDetectorId += 2;
        break;
      case 35:
        fiPlane.push_back(new T4FIPlane35(fi, firstDetectorId));
        firstDetectorId += 6;
       break;
      default:
        T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
            "T4SciFi::construct: Unknown SciFi unit or TBname: " + fi->name
                + ".");
        break;
    }

    if (unit == 5 && settingsFile->getStructManager()->getRICH()->general.useDetector && rich_log != NULL) {
      fiPlane.back()->setPosition(G4ThreeVector(fi->position[0], fi->position[1], fi->position[2]) - rich_pos);
      fiPlane.back()->construct(*rich_log);
    } else {
      fiPlane.back()->construct(world_log);
    }
  }
}

T4FIPlane01::T4FIPlane01(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 1.97 * CLHEP::cm;
  dimSens[1] = 1.97 * CLHEP::cm;
  dimSens[2] = 0.211 * CLHEP::cm;

  nWires = 96;
  pitch = 0.041 * CLHEP::cm;
  plane2Dist = 1.6 * CLHEP::cm;
  if (det->name[3] == '2')
    plane2Dist *= -1.0;
}

void T4FIPlane01::construct(G4LogicalVolume* world_log)
{
  // FI01: X in front, FI02: Y in front
  G4double zMult = 1.0;
  if (det->name[3] == '2')
    zMult = -1.0;

  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 35.0 * CLHEP::cm, 35.0 * CLHEP::cm, 2.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.8 * CLHEP::cm * zMult), log.back(), "FI01_2_air", world_log, 0, 0, checkOverlap);

  // sens_1 in mother
  box.push_back(new G4Box("sens_1_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8 * CLHEP::cm * zMult), log.back(), "FI01_2_sens_1", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.8 * CLHEP::cm * zMult), log.back(), "FI01_2_sens_2", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  // scint_1 in mother
  box.push_back(new G4Box("scint_1_box", 10.0 * CLHEP::cm, 2.0 * CLHEP::cm, 0.211 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scitn_1_log"));
  new G4PVPlacement(0, G4ThreeVector(11.97 * CLHEP::cm, 0, 0), log.back(), "FI01_2_scint_1", log.at(motherIndex), 0, 0, checkOverlap);

  // scint_2 in mother
  box.push_back(new G4Box("scint_2_box", 2.0 * CLHEP::cm, 10.0 * CLHEP::cm, 0.211 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scitn_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 11.97 * CLHEP::cm * zMult, 0), log.back(), "FI01_2_scint_2", log.at(motherIndex), 0, 0, checkOverlap);

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

T4FIPlane15::T4FIPlane15(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = namePlane3 = det->name;
  namePlane2[4] = 'Y';
  namePlane3[4] = 'U';
  usePlane3 = _det->useMechanicalStructure; // useMechanicalStructure is used for the third plane in this special case

  rotY = new CLHEP::HepRotation;
  rotY->rotateZ(90. * CLHEP::deg);
  rotU = new CLHEP::HepRotation;
  rotU->rotateZ(-45.0 * CLHEP::deg);

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 2.3 * CLHEP::cm;
  dimSens[1] = 3.5 * CLHEP::cm;
  dimSens[2] = 0.16 * CLHEP::cm;

  nWires = 64;
  pitch = 0.07 * CLHEP::cm;
  plane2Dist = 0.67 * CLHEP::cm;
  plane3Dist = 1.465 * CLHEP::cm;
}

void T4FIPlane15::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 18.0 * CLHEP::cm, 18.0 * CLHEP::cm, 1.76 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.665 * CLHEP::cm), log.back(), "FI15_air", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // air_X in mother
  unsigned int al_XIndex = log.size();
  box.push_back(new G4Box("air_X_box", 4.5 * CLHEP::cm, 4.5 * CLHEP::cm, 0.125 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "al_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.33 * CLHEP::cm), log.back(), "FI15_al_X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightgray);

   //Alu_X in air_X
  box.push_back(new G4Box("al_X_box", 3. * CLHEP::cm, 3. * CLHEP::cm,0.125 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0.,0.,0), log.back(), "FI15_al_X", log.at(al_XIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // Alu_box1_in_air_X
  box.push_back(new G4Box("alu_box1_X", 0.5 * CLHEP::cm, 4.5 * CLHEP::cm, 0.4 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X1_log"));
  new G4PVPlacement(0,G4ThreeVector(2.75 * CLHEP::cm, 9.30 * CLHEP::cm, -0.65  * CLHEP::cm), log.back(), "FI15_albox1X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-2.75 * CLHEP::cm, 9.30 * CLHEP::cm, -0.65  * CLHEP::cm), log.back(), "FI15_albox4X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);


  // Alu_box2_in_air_X
  box.push_back(new G4Box("alu_box2_X", 0.1 * CLHEP::cm, 4.75 * CLHEP::cm, 0.3 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X2_log"));
  new G4PVPlacement(0,G4ThreeVector(3.15 * CLHEP::cm, 0.05 * CLHEP::cm, -0.77  * CLHEP::cm), log.back(), "FI15_albox2X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-3.15 * CLHEP::cm, 0.05 * CLHEP::cm, -0.77  * CLHEP::cm), log.back(), "FI15_albox5X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  // Alu_box3_in_air_X
  box.push_back(new G4Box("alu_box3_X", 0.4 * CLHEP::cm, 0.15 * CLHEP::cm, 0.1 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X3_log"));
  new G4PVPlacement(0,G4ThreeVector(2.65 * CLHEP::cm, 4.65 * CLHEP::cm, -0.35  * CLHEP::cm), log.back(), "FI15_albox3X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-2.65 * CLHEP::cm, 4.65 * CLHEP::cm, -0.35  * CLHEP::cm), log.back(), "FI15_albox6X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  // bc408_box7_in_air_X
  box.push_back(new G4Box("alu_box7_X", 0.35 * CLHEP::cm, 0.3 * CLHEP::cm, 0.35 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_X7_log"));
  new G4PVPlacement(0,G4ThreeVector(2.65 * CLHEP::cm, -3.5 * CLHEP::cm, -0.92  * CLHEP::cm), log.back(), "FI15_bcbox7X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-2.65 * CLHEP::cm, -3.5 * CLHEP::cm, -0.92  * CLHEP::cm), log.back(), "FI15_bcbox8X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box9_in_air_X
  box.push_back(new G4Box("alu_box9_X", 2.3 * CLHEP::cm, 0.15 * CLHEP::cm, 0.35 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_X9_log"));
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, -3.65 * CLHEP::cm, -0.92  * CLHEP::cm), log.back(), "FI15_bcbox9X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box10_in_air_X
  box.push_back(new G4Box("bc408_box10_X", 2.3 * CLHEP::cm, 0.15 * CLHEP::cm, 0.20 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_X10_log"));
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, -3.35 * CLHEP::cm, -1.075  * CLHEP::cm), log.back(), "FI15_bcbox10X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box11_in_air_X
  box.push_back(new G4Box("bc408_box11_X", 0.35 * CLHEP::cm, 0.3 * CLHEP::cm, 0.15 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_X11_log"));
  new G4PVPlacement(0,G4ThreeVector(-2.65 * CLHEP::cm, 3.2 * CLHEP::cm, -0.715  * CLHEP::cm), log.back(), "FI15_bcbox11X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(2.65 * CLHEP::cm, 3.2 * CLHEP::cm, -0.715  * CLHEP::cm), log.back(), "FI15_bcbox12X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box13_in_air_X
  box.push_back(new G4Box("bc408_box13_X", 3. * CLHEP::cm, 0.3 * CLHEP::cm, 0.20 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_X13_log"));
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 3.2 * CLHEP::cm, -1.075  * CLHEP::cm), log.back(), "FI15_bcbox13X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // epoxy_box14_in_air_X
  box.push_back(new G4Box("epoxy_box13_X", 2.3 * CLHEP::cm, 3.5 * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "epoxy_box_X11_log"));
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 0. * CLHEP::cm, -0.48  * CLHEP::cm), log.back(), "FI15_epbox14X", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 0. * CLHEP::cm, -0.85  * CLHEP::cm), log.back(), "FI15_epbox15X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // sensX_box
  box.push_back(new G4Box("sensX_plane", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sensX_log"));
  new G4PVPlacement(0,G4ThreeVector(0., 0., -0.665 * CLHEP::cm), log.back(), "FI15_sensX", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkred);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  //Now Y plane
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sensX_log"));
  new G4PVPlacement(rotY,G4ThreeVector(0., 0., 0.005* CLHEP::cm), log.back(), "FI15_sensY", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkred);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId+1));

  if (usePlane3) {
    //Now U plane
    // sensX_box
    box.push_back(new G4Box("sensU_plane", dimSens[1], dimSens[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sensU_log"));
    new G4PVPlacement(rotU,G4ThreeVector(0., 0., 0.8 * CLHEP::cm), log.back(), "FI15_sensU", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkred);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId+2));
  }

  // Alu_box1
  box.push_back(new G4Box("alu_box1_X", 4.5 * CLHEP::cm, 0.5 * CLHEP::cm,0.4 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X1_log"));
  new G4PVPlacement(0,G4ThreeVector(-9.30 * CLHEP::cm, 2.75 * CLHEP::cm, -0.01 * CLHEP::cm), log.back(), "FI15_albox1Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-9.30 * CLHEP::cm, -2.75 * CLHEP::cm, -0.01 * CLHEP::cm), log.back(), "FI15_albox1Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  // Alu_box2_Y
  box.push_back(new G4Box("alu_box2_X",  4.75 * CLHEP::cm,0.1 * CLHEP::cm, 0.275 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X2_log"));
  new G4PVPlacement(0,G4ThreeVector(-0.05 * CLHEP::cm, 3.15 * CLHEP::cm, 0.07 * CLHEP::cm), log.back(), "FI15_albox2Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-0.05 * CLHEP::cm, -3.15 * CLHEP::cm, 0.07 * CLHEP::cm), log.back(), "FI15_albox5Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  // Alu_box3_Y
  box.push_back(new G4Box("alu_box3_X",  0.15 * CLHEP::cm,0.4 * CLHEP::cm, 0.1 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_box_X3_log"));
  new G4PVPlacement(0,G4ThreeVector(-4.65 * CLHEP::cm,2.65 * CLHEP::cm, -0.31  * CLHEP::cm), log.back(), "FI15_albox3Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(-4.65 * CLHEP::cm,-2.65 * CLHEP::cm, -0.31  * CLHEP::cm), log.back(), "FI15_albox6Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  // bc408_box7_Y
  box.push_back(new G4Box("alu_box7_Y",  0.3 * CLHEP::cm,0.35 * CLHEP::cm, 0.35 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_Y7_log"));
  new G4PVPlacement(0,G4ThreeVector(3.5 * CLHEP::cm, 2.65 * CLHEP::cm, 0.26  * CLHEP::cm ), log.back(), "FI15_bcbox7Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(3.5 * CLHEP::cm,-2.65 * CLHEP::cm,  0.26 * CLHEP::cm), log.back(), "FI15_bcbox8Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box9_Y
  box.push_back(new G4Box("alu_box9_Y",  0.15 * CLHEP::cm,2.3 * CLHEP::cm, 0.34 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_Y9_log"));
  new G4PVPlacement(0,G4ThreeVector(3.65 * CLHEP::cm, 0. * CLHEP::cm , 0.25  * CLHEP::cm), log.back(), "FI15_bcbox9Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box10_Y
  box.push_back(new G4Box("bc408_box10_Y", 0.15 * CLHEP::cm, 2.3 * CLHEP::cm, 0.185 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_Y10_log"));
  new G4PVPlacement(0,G4ThreeVector(3.35 * CLHEP::cm, 0. * CLHEP::cm, 0.40  * CLHEP::cm), log.back(), "FI15_bcbox10Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box11_Y
  box.push_back(new G4Box("bc408_box11_Y", 0.3 * CLHEP::cm, 0.35 * CLHEP::cm, 0.15 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_Y11_log"));
  new G4PVPlacement(0,G4ThreeVector(-3.2 * CLHEP::cm, -2.65 * CLHEP::cm, 0.05  * CLHEP::cm), log.back(), "FI15_bcbox11Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector (-3.2 * CLHEP::cm, 2.65 * CLHEP::cm, 0.05 * CLHEP::cm), log.back(), "FI15_bcbox12Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // bc408_box13_Y
  box.push_back(new G4Box("bc408_box13_Y", 0.3 * CLHEP::cm, 3. * CLHEP::cm,  0.185 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc408_box_Y13_log"));
  new G4PVPlacement(0,G4ThreeVector(-3.2 * CLHEP::cm, 0. * CLHEP::cm, 0.40  * CLHEP::cm), log.back(), "FI15_bcbox13Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->lightblue);

  // epoxy_box14_Y
  box.push_back(new G4Box("epoxy_box14_Y", 3.5 * CLHEP::cm,2.3 * CLHEP::cm,  0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "epoxy_box_Y11_log"));
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 0. * CLHEP::cm, 0.19  * CLHEP::cm), log.back(), "FI15_epbox14Y", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 0. * CLHEP::cm, -0.18  * CLHEP::cm), log.back(), "FI15_epbox15Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  if (usePlane3) {
    //U relative structure
    box.push_back(new G4Box("bc408_box16_U", 3.5 * CLHEP::cm,0.15 * CLHEP::cm,  0.2 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc04_box_U16_log"));
    new G4PVPlacement(rotU,G4ThreeVector(2.42 * CLHEP::cm, -2.42 * CLHEP::cm, 1.215  * CLHEP::cm), log.back(), "FI15_box16U", log.at(motherIndex), 0, 0, checkOverlap);
    new G4PVPlacement(rotU,G4ThreeVector(-2.42 * CLHEP::cm, 2.42 * CLHEP::cm, 1.215 * CLHEP::cm), log.back(), "FI15_box18U", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->lightblue);

    box.push_back(new G4Box("bc408_box17_U", 3.5 * CLHEP::cm,0.15 * CLHEP::cm,  0.35 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "bc04_box_U17_log"));
    new G4PVPlacement(rotU,G4ThreeVector(2.6331 * CLHEP::cm, -2.6331 * CLHEP::cm, 1.065  * CLHEP::cm), log.back(), "FI15_box17U", log.at(motherIndex), 0, 0, checkOverlap);
    new G4PVPlacement(rotU,G4ThreeVector(-2.6331 * CLHEP::cm, 2.6331 * CLHEP::cm, 1.065  * CLHEP::cm), log.back(), "FI15_box19U", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->lightblue);

    box.push_back(new G4Box("epoxy_box20_U", 3.5 * CLHEP::cm,3.5 * CLHEP::cm,  0.025 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "epoxy_box_U15_log"));
    new G4PVPlacement(rotU,G4ThreeVector(0.,0.,0.615 * CLHEP::cm), log.back(), "FI15_epo1U", log.at(motherIndex),0,0,checkOverlap);
    new G4PVPlacement(rotU,G4ThreeVector(0.,0.,0.985 * CLHEP::cm), log.back(), "FI15_epo2U", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->orange);
  }

  /*
  C              med moth rot     X    Y    Z   shape  npar   X     Y     Z
  GSET03VOL1PAR   625  21   0   -0.66   0.   0.  'BOX '  3    0.16   2.3   3.5
  GSET03VOL2PAR   625  21   3    0.000  0.   0.  'BOX '  3    0.16   2.3   3.5
  GSET03VOL3PAR   625  21   6    0.8   0.   0.  'BOX '  3    0.16   3.5   3.5

  GSET03VOL4PAR   248  21   0  -0.845  0.   0.  'BOX '  3    0.025   2.3   3.5
  GSET03VOL5PAR   248  21   0  -0.475  0.   0.  'BOX '  3    0.025   2.3   3.5
  GSET03VOL6PAR   224  21   0  -1.07   0.   3.2 'BOX '  3    0.20    3.0   0.3
  GSET03VOL7PAR   224  21   0  -0.71   2.65 3.2 'BOX '  3    0.15    0.35  0.3
  GSET03VOL8PAR   224  21   0  -0.71  -2.65 3.2 'BOX '  3    0.15    0.35  0.3
  GSET03VOL9PAR   224  21   0  -0.92   0.  -3.65 'BOX ' 3    0.35    2.3   0.15
  GSET03VOLAPAR   224  21   0  -1.07   0.  -3.35 'BOX ' 3    0.20    2.3   0.15	!10
  GSET03VOLBPAR   224  21   0  -0.92   2.65 -3.5 'BOX ' 3    0.35    0.35  0.3    !11
  GSET03VOLCPAR   224  21   0  -0.92  -2.65 -3.5 'BOX ' 3    0.35    0.35  0.3    !12
  GSET03VOLDPAR   235  21   0  -0.35  -2.65 0.05 'BOX ' 3    0.1     0.4   4.75   !13
  GSET03VOLEPAR   235  21   0  -0.65  -3.15 0.05 'BOX ' 3    0.4     0.1   4.75   !14
  GSET03VOLFPAR   235  21   0  -0.65  -2.75 9.30 'BOX ' 3    0.4     0.5   4.5    !15
  GSET03VOLGPAR   235  21   0  -0.35   2.65 0.05 'BOX ' 3    0.1     0.4   4.75   !16
  GSET03VOLHPAR   235  21   0  -0.65   3.15 0.05 'BOX ' 3    0.4     0.1   4.75   !17
  GSET03VOLIPAR   235  21   0  -0.65   2.75 9.30 'BOX ' 3    0.4     0.5   4.5    !18

  GSET03VOLJPAR   235  21   0  -0.33   0.0  0.0  'BOX ' 3    0.125   4.5   4.5    !19
  GSET03VOLKPAR   201  19   0   0.0    0.0  0.0  'BOX ' 3    0.125   3.0   3.0    !20

  GSET03VOLLPAR   201  0    0   0.0    0.0  0.0  'BOX ' 3    1.76   18.0  18.0    !21

  GSET03VOLMPAR   248  21   0  -0.185  0.   0.   'BOX ' 3    0.025   3.5   2.3    !22
  GSET03VOLNPAR   248  21   0   0.185  0.   0.   'BOX ' 3    0.025   3.5   2.3    !23
  GSET03VOLOPAR   224  21   0   0.41  -3.2  0.   'BOX ' 3    0.20    0.3   3.0    !24
  GSET03VOLPPAR   224  21   0   0.05  -3.2  2.65 'BOX ' 3    0.15    0.3   0.35   !25
  GSET03VOLQPAR   224  21   0   0.05  -3.2 -2.65 'BOX ' 3    0.15    0.3   0.35   !26
  GSET03VOLRPAR   224  21   0   0.26   3.65 0.   'BOX ' 3    0.35    0.15  2.3    !27
  GSET03VOLSPAR   224  21   0   0.41   3.35 0.   'BOX ' 3    0.20    0.15  2.3    !28
  GSET03VOLTPAR   224  21   0   0.26   3.5  2.65 'BOX ' 3    0.35    0.3   0.35   !29
  GSET03VOLUPAR   224  21   0   0.26   3.5 -2.65 'BOX ' 3    0.35    0.3   0.35   !30
  GSET03VOLVPAR   235  21   0  -0.31  -0.05 -2.65 'BOX ' 3    0.1     4.75  0.4   !31
  GSET03VOLWPAR   235  21   0  -0.01  -0.05 -3.15 'BOX ' 3    0.4     4.75  0.1   !32
  GSET03VOLXPAR   235  21   0  -0.01  -9.30 -2.75 'BOX ' 3    0.4     4.5   0.5   !33
  GSET03VOLYPAR   235  21   0  -0.31  -0.05  2.65 'BOX ' 3    0.1     4.75  0.4   !34
  GSET03VOLZPAR   235  21   0  -0.01  -0.05  3.15 'BOX ' 3    0.4     4.75  0.1   !35
  GSET03VO24PAR   235  21   0  -0.01  -9.30  2.75 'BOX ' 3    0.4     4.5   0.5   !36
  GSET03VO25PAR   248  21   6   0.615  0.    0.   'BOX ' 3    0.025   3.5   3.5   !37
  GSET03VO26PAR   248  21   6   0.985  0.    0.   'BOX ' 3    0.025   3.5   3.5   !38
  GSET03VO27PAR   224  21   6   1.21  -2.25  2.47 'BOX ' 3    0.20    4.2   0.3   !39
  GSET03VO28PAR   224  21   6   1.06   2.58 -2.58 'BOX ' 3    0.35    3.5   0.15  !40
  GSET03VO29PAR   224  21   6   1.21   2.58 -2.28 'BOX ' 3    0.20    3.5   0.15  !41
  */


  //  GSET03VOL1PAR   625  18   0    0.000  0.   0.  'BOX '  3    0.16   2.3   3.5 bc408
  //  GSET03VOL2PAR   625  22   0    0.000  0.   0.  'BOX '  3    0.16   2.3   3.5
  //  GSET03VOL3PAR   248  18   0  -0.185  0.   0.  'BOX '  3    0.025   2.3   3.5 /epoxy_09
  //  GSET03VOL4PAR   248  18   0   0.185  0.   0.  'BOX '  3    0.025   2.3   3.5
  //  GSET03VOL5PAR   224  18   0   -0.41  0.   3.2  'BOX '  3    0.20   3.0   0.3 bc408
  //  GSET03VOL6PAR   224  18   0   -0.05  2.65 3.2  'BOX '  3    0.15  0.35   0.3
  //  GSET03VOL7PAR   224  18   0   -0.05 -2.65 3.2  'BOX '  3    0.15  0.35   0.3
  //  GSET03VOL8PAR   224  18   0   -0.26  0.  -3.65  'BOX '  3    0.35   2.3   0.15
  //  GSET03VOL9PAR   224  18   0   -0.41  0.  -3.35  'BOX '  3    0.20   2.3   0.15
  //  GSET03VOLAPAR   224  18   0   -0.26  2.65  -3.5  'BOX '  3   0.35   0.35   0.3
  //  GSET03VOLBPAR   224  18   0   -0.26 -2.65  -3.5  'BOX '  3   0.35   0.35   0.3
  //  GSET03VOLCPAR   235  18   0   0.31 -2.65  0.05  'BOX '  3   0.1    0.4   4.75 /alu
  //  GSET03VOLDPAR   235  18   0   0.01 -3.15  0.05  'BOX '  3   0.4    0.1   4.75
  //  GSET03VOLEPAR   235  18   0   0.01 -2.75  9.30  'BOX '  3   0.4    0.5   4.5
  //  GSET03VOLFPAR   235  18   0   0.31  2.65  0.05  'BOX '  3   0.1    0.4   4.75
  //  GSET03VOLGPAR   235  18   0   0.01  3.15  0.05  'BOX '  3   0.4    0.1   4.75
  //  GSET03VOLHPAR   235  18   0   0.01  2.75  9.30  'BOX '  3   0.4    0.5   4.5
  //  GSET03VOLIPAR   201  21   0  -0.33   0.0   0.0   'BOX '  3   0.41  3.2   14.0 /air     18
  //  GSET03VOLJPAR   235  21   0    0.0   0.0   0.0   'BOX'   3   0.125  4.5   4.5
  //  GSET03VOLKPAR   201  19   0    0.0   0.0   0.0   'BOX'   3   0.125  3.0   3.0
  //  GSET03VOLLPAR   201  0   0    0.0   0.0   0.0   'BOX'   3   1.10  18.0   18.0       21 mother
  //  GSET03VOLMPAR   201  21  81   0.33   0.0   0.0   'BOX '  3   0.41  3.2   14.0         22
  //  GSET03VOLNPAR   248  22   0  -0.185  0.   0.  'BOX '  3    0.025   2.3   3.5
  //  GSET03VOLOPAR   248  22   0   0.185  0.   0.  'BOX '  3    0.025   2.3   3.5
  //  GSET03VOLPPAR   224  22   0   -0.41  0.   3.2  'BOX '  3    0.20   3.0   0.3
  //  GSET03VOLQPAR   224  22   0   -0.05  2.65 3.2  'BOX '  3    0.15  0.35   0.3
  //  GSET03VOLRPAR   224  22   0   -0.05 -2.65 3.2  'BOX '  3    0.15  0.35   0.3
  //  GSET03VOLSPAR   224  22   0   -0.26  0.  -3.65  'BOX '  3    0.35   2.3   0.15
  //  GSET03VOLTPAR   224  22   0   -0.41  0.  -3.35  'BOX '  3    0.20   2.3   0.15
  //  GSET03VOLUPAR   224  22   0   -0.26  2.65  -3.5  'BOX '  3   0.35   0.35   0.3
  //  GSET03VOLVPAR   224  22   0   -0.26 -2.65  -3.5  'BOX '  3   0.35   0.35   0.3
  //  GSET03VOLWPAR   235  22   0   0.31 -2.65  0.05  'BOX '  3   0.1    0.4   4.75
  //  GSET03VOLXPAR   235  22   0   0.01 -3.15  0.05  'BOX '  3   0.4    0.1   4.75
  //  GSET03VOLYPAR   235  22   0   0.01 -2.75  9.30  'BOX '  3   0.4    0.5   4.5
  //  GSET03VOLZPAR   235  22   0   0.31  2.65  0.05  'BOX '  3   0.1    0.4   4.75
  //  GSET03VO24PAR   235  22   0   0.01  3.15  0.05  'BOX '  3   0.4    0.1   4.75
  //  GSET03VO25PAR   235  22   0   0.01  2.75  9.30  'BOX '  3   0.4    0.5   4.5
}

T4FIPlane03::T4FIPlane03(const T4SDetector* _det, int _firstDetectorId)
{
   det = _det;
   firstDetectorId = _firstDetectorId;

   setPosition(det->position);
   detDatPos = positionVector;
   namePlane2 = det->name;
   namePlane2[4] = 'Y';

   namePlane3 = det->name;
   namePlane3[4] = 'U';
   usePlane3 = true;

   rotY = new CLHEP::HepRotation;
   rotY->rotateZ(90.0 * CLHEP::deg);
   rotU = new CLHEP::HepRotation;
   rotU->rotateZ(-45.0 * CLHEP::deg);
   rot1 = new CLHEP::HepRotation;
   rot1->rotateX(-90.0 * CLHEP::deg);
   rot1->rotateY(-90.0 * CLHEP::deg);
   rot1->rotateZ(-90.0 * CLHEP::deg);

   nWires = 128;
   pitch = 0.041 * CLHEP::cm;

   dimSens[0] = 2.625 * CLHEP::cm; // these values are from riccardos implementation
   dimSens[1] = 2.625 * CLHEP::cm;
   dimSens[2] = 0.211 * CLHEP::cm;

   // reading detectors.dat this should be correct, but we use the numbers from the .ffr geometry description
//   plane2Dist = -1.1 * CLHEP::cm; // Y 1.1cm upstream of X
//   plane3Dist = plane2Dist - 0.5 * CLHEP::cm; // U 0.5cm upstream of Y
   plane2Dist = -0.5 * CLHEP::cm;
   plane3Dist = plane2Dist - 1.1 * CLHEP::cm;
}

void T4FIPlane03::construct(G4LogicalVolume* world_log)
{
    //FI03
    unsigned int motherIndex = log.size();
    box.push_back(new G4Box("FI03_mother_box", 39.  * CLHEP::cm, 39. * CLHEP::cm, 3.225 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "FI03_mother_log"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0., 0., -2.125 * CLHEP::cm), log.back(), "FI03_air", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    unsigned int X_mother_Index = log.size();
    box.push_back(new G4Box("xFI03_mother_box", 4.325 * CLHEP::cm, 15. * CLHEP::cm, 0.55 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "XFI03_mother_log"));
    new G4PVPlacement(0, G4ThreeVector(0., 0., 2.425 * CLHEP::cm),log.back(),"x_FI03_air", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    unsigned int U_mother_Index = log.size();
    box.push_back(new G4Box("uFI03_mother_box", 4.325 * CLHEP::cm, 14.9 * CLHEP::cm, 0.8 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "uFI03_mother_log"));
    new G4PVPlacement(rotU, G4ThreeVector(-0.033 * CLHEP::cm, 0.052 * CLHEP::cm, -0.025 * CLHEP::cm),log.back(),"u_FI03_air", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    unsigned int Y_mother_Index = log.size();
    box.push_back(new G4Box("y_mother_box", 4.325 * CLHEP::cm, 14.9 * CLHEP::cm, 0.55 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "yFI03_mother_log"));
    new G4PVPlacement(rotY, G4ThreeVector(0., 0., 1.325 * CLHEP::cm),log.back(),"y_FI03_air", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    box.push_back(new G4Box("x_ClFi_box",2.625 * CLHEP::cm,6.1875 * CLHEP::cm, 0.211 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "x_ClFi_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,-8.8125 * CLHEP::cm,-0.3 * CLHEP::cm),log.back(),"x_ClFi_box",log.at(X_mother_Index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    box.push_back(new G4Box("u_ClFi_box",2.625 * CLHEP::cm,6.0875 * CLHEP::cm, 0.211 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "u_ClFi_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,-8.7125 * CLHEP::cm,0.55 * CLHEP::cm),log.back(),"u_ClFi_box",log.at(U_mother_Index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    box.push_back(new G4Box("y_ClFi_box",2.625 * CLHEP::cm,6.0875 * CLHEP::cm, 0.211 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "y_ClFi_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,8.7125 * CLHEP::cm,-0.3 * CLHEP::cm),log.back(),"y_ClFi_box",log.at(Y_mother_Index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    box.push_back(new G4Box("x_SFS_box", dimSens[0], dimSens[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "x_SFS_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-0.3 * CLHEP::cm),log.back(), "x_SFS_box", log.at(X_mother_Index),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));
    log.back()->SetVisAttributes(colour->red);

    box.push_back(new G4Box("y_SFS_box", dimSens[0], dimSens[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "y_SFS_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,-0.3 * CLHEP::cm),log.back(), "y_SFS_box", log.at(Y_mother_Index),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));
    log.back()->SetVisAttributes(colour->red);

    box.push_back(new G4Box("u_SFS_box", dimSens[0], dimSens[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "u_SFS_log"));
    new G4PVPlacement(0,G4ThreeVector(0.,0.,0.55 * CLHEP::cm),log.back(), "u_SFS_box", log.at(U_mother_Index),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId + 2));
    log.back()->SetVisAttributes(colour->red);

    // rohacell in air_X and air_Y
    box.push_back(new G4Box("FI03_rohacell_box", 0.85 * CLHEP::cm, 4.3 * CLHEP::cm, 0.55 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "FI03_rohacell_log"));
    new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_X1", log.at(X_mother_Index), 0, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_Y1", log.at(Y_mother_Index), 0, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_X2", log.at(X_mother_Index), 0, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_Y2", log.at(Y_mother_Index), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);

    // rohacell_U in air_U
    box.push_back(new G4Box("FI03_rohacell_U_box", 0.85 * CLHEP::cm, 4.3 * CLHEP::cm, 0.8 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_U_log"));
    new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_U1", log.at(U_mother_Index), 0, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI03_rohacell_U2", log.at(U_mother_Index), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);

    G4double zPlane[2] = {-2.*CLHEP::cm, 2.*CLHEP::cm};
    G4double rMin[2] = {15.*CLHEP::cm, 15.*CLHEP::cm};
    G4double rMax[2] = {16.*CLHEP::cm, 16.*CLHEP::cm};
    G4double rMPVC[2] = {16.025*CLHEP::cm, 16.025*CLHEP::cm};

    // rohacell polygon
    poly.push_back(new G4Polyhedra("rohacell_polg",22.5 * CLHEP::degree,360. * CLHEP::degree,8,2,zPlane,rMin,rMax));
    log.push_back(new G4LogicalVolume(poly.back(),materials->rohacell,"rohacell_polg_log"));
    new G4PVPlacement(0, G4ThreeVector(0., 0., 1.175 * CLHEP::cm), log.back(), "FI03_rohacell_polg", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);

    // pvc polygon
    poly.push_back(new G4Polyhedra("pvc_polg",22.5 * CLHEP::degree, 360. * CLHEP::degree,8,2,zPlane,rMax,rMPVC));
    log.push_back(new G4LogicalVolume(poly.back(),materials->pvc,"pvc_polg_log"));
    new G4PVPlacement(0, G4ThreeVector(0., 0., 1.175* CLHEP::cm), log.back(), "FI03_pvc_polg", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray);

    box.push_back(new G4Box("pvc_box_FI03",16. * CLHEP::cm, 16. * CLHEP::cm, 0.025 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->pvc,"pvc_block_log"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,3.2 * CLHEP::cm), log.back(), "FI03_pvc_block", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->gray);

    unsigned int roha_index = log.size();
    box.push_back(new G4Box("rohacell_box_FI03",39. * CLHEP::cm, 39. * CLHEP::cm, 1.2 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->rohacell,"rohacell_box_log"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,-2.025 * CLHEP::cm), log.back(), "FI03_rohacell_box", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->orange);

    //Air tube
    tubs.push_back(new G4Tubs("air_tube",0.,3. * CLHEP::cm, 1.2 * CLHEP::cm, 0.,360 * CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(),materials->air_noOptical,"air_tube_FI03_log"));
    new G4PVPlacement(0, G4ThreeVector(0.,0.,0.), log.back(), "FI03_airtube", log.at(roha_index), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //ClFi tubs
    double ang = 4.10;
    tubs.push_back(new G4Tubs("ClFi_tubs1",20. * CLHEP::cm, 28. * CLHEP::cm, 0.137 * CLHEP::cm, 180 * CLHEP::deg, 55 * CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(),materials->bc408_noOptical,"Clfi_tube1_log"));
    new G4PVPlacement(0, G4ThreeVector(24. * CLHEP::cm, -16.025 * CLHEP::cm, 2.125 * CLHEP::cm),log.back(), "FI03_ClFitube1",log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    tubs.push_back(new G4Tubs("ClFi_tubs2",22.3 * CLHEP::cm, 30.3 * CLHEP::cm, 0.137 * CLHEP::cm, 225. * CLHEP::deg, 50. * CLHEP::deg));
    log.push_back(new G4LogicalVolume(tubs.back(),materials->bc408_noOptical,"Clfi_tube2_log"));
    new G4PVPlacement(0, G4ThreeVector(30.1 * CLHEP::cm, 7.4 * CLHEP::cm, 0.525 * CLHEP::cm),log.back(), "FI03_ClFitube2",log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //Last ClFi box
    box.push_back(new G4Box("lClfi_box_FI03",10.985 * CLHEP::cm, 4. * CLHEP::cm, 0.137 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical, "last_ClFi_box_log"));
    new G4PVPlacement(0,G4ThreeVector(27.015 * CLHEP::cm,0.,1.125 * CLHEP::cm),log.back(), "last_ClFi_box", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    /*
    C              med moth rot     X    Y     Z     shape  npar     X     Y      Z
    GSET12VOL1PAR   625  7   0     0.55  0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens
    GSET12VOL2PAR   625  8   0     0.3   0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens
    GSET12VOL3PAR   625  9   0    -0.3   0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens
    GSET12VOL4PAR   224  7   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi
    GSET12VOL5PAR   224  8   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi
    GSET12VOL6PAR   224  9   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi
    GSET12VOL7PAR   201 10   6   -0.025 -0.033 0.052  'BOX '  3    0.8    4.325 15.0     Air
    GSET12VOL8PAR   201 10   3    1.325  0.0   0.     'BOX '  3    0.55   4.325 15.0     Air
    GSET12VOL9PAR   201 10   0    2.425  0.0   0.     'BOX '  3    0.55   4.325 15.0     Air
    GSET12VOLAPAR   201  0   0    0.0    0.0   0.     'BOX '  3    3.225  39.0  39.0     Air
    GSET12VOLBPAR   237  7   0    0.0    3.475 0.     'BOX '  3    0.8    0.85   4.3     Rcell50
    GSET12VOLCPAR   237  8   0    0.0    3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
    GSET12VOLDPAR   237  9   0    0.0    3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
    GSET12VOLEPAR   237  7   0    0.0   -3.475 0.     'BOX '  3    0.8    0.85   4.3     Rcell50
    GSET12VOLFPAR   237  8   0    0.0   -3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
    GSET12VOLGPAR   237  9   0    0.0   -3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
    GSET12VOLHPAR   237 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 15. 16. 2. 15.  16.     Rc50
    GSET12VOLIPAR   228 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 16. 16.025 2. 16. 16.025 PVC
    GSET12VOLJPAR   228 10   0    3.2    0.0   0.     'BOX '  3    0.025 16.    16.      PVC
    GSET12VOLKPAR   237 10   0   -2.025  0.0   0.     'BOX '  3    1.2   39.    39.      Rcell50
    GSET12VOLLPAR   201 20   1    0.0    0.0   0.     'TUBE'  3    0.     3.0    1.2     Air
    C
    GSET12VOLMPAR   224 10   1    1.2   24.  -16.025  'TUBS'  5   20.    28.0    0.137  180. 235.    ClFi
    GSET12VOLNPAR   224 10   1    1.2   29.1   6.4    'TUBS'  5   22.3    30.3   0.137  225. 275.    ClFi
    GSET12VOLOPAR   224 10   0    1.2   27.015 0.     'BOX '  3    0.137  10.985 4.      ClFi
      */
}

T4FIPlane35::T4FIPlane35(const T4SDetector *_det, int _firstDetectorId)
{
    det = _det;
    firstDetectorId = _firstDetectorId;
    namePlane2 = namePlane3 = namePlane4 = namePlane5 = namePlane6 = det->name;
    namePlane2[4] = namePlane5[4] = 'V';
    namePlane3[4] = namePlane6[4] = 'U';
    usePlane3 = true;

    namePlane4[5] = namePlane5[5] = namePlane6[5] = '2';

    rotU = new CLHEP::HepRotation;
    rotU->rotateZ(45.0 * CLHEP::deg);

    rotV = new CLHEP::HepRotation;
    rotV->rotateZ(-45.0 * CLHEP::deg);

    rotX = new CLHEP::HepRotation;
    rotX->rotateX(90.0 * CLHEP::deg);

    rotS = new CLHEP::HepRotation;
    rotS->rotateZ(30.0 * CLHEP::deg);

    rotAS = new CLHEP::HepRotation;
    rotAS->rotateZ(-30.0 * CLHEP::deg);
    // U plane in principal the same box, no need for the -45deg rotation
    setPosition(det->position);
    detDatPos = positionVector;

    dimSens[0] = 11.3745 * CLHEP::cm;
    dimSens[1] = 11.3745 * CLHEP::cm;
    dimSens[2] = 0.2985 * CLHEP::cm;
    dimSensV = 7.9665 * CLHEP::cm;

    dimSensX2[0] = 1.704 * CLHEP::cm;
    dimSensX2[1] = 4.825 * CLHEP::cm;
    dimSensV2[0] = 1.704 * CLHEP::cm;
    dimSensV2[1] = 3.125 * CLHEP::cm;

    dimDeadX[0] = 1.704 * CLHEP::cm;
    dimDeadX[1] = 6.525/*6.409*/ * CLHEP::cm;
    dimDeadV[0] = 1.704 * CLHEP::cm;
    dimDeadV[1] = 4.825 * CLHEP::cm;

    posSensU2[0] = /*0.094887*/ 0.04325 * CLHEP::cm;
    posSensU2[1] = /*4.752238*/ 4.78502 * CLHEP::cm;
    posSensV2[0] = -0.06755 * CLHEP::cm;
    posSensV2[1] = 4.8415 * CLHEP::cm;

    posDeadX[0] = 0;
    posDeadX[1] = 4.8495 * CLHEP::cm;
    /*posDeadV[0] = 0;
    posDeadV[1] = 3.226 * CLHEP::cm;
    posDeadU[0] = 0;
    posDeadU[1] = 3.226 * CLHEP::cm;*/
    //Staggering different planes
    stagU[0] = -0.13345 * CLHEP::cm;
    stagU[1] = -0.15468 * CLHEP::cm;
    stagV[0] = 0.00944 * CLHEP::cm;
    stagV[1] = 0.00072 * CLHEP::cm;
    //Variables for placing '2' planes
    //U
    coordDDatU2[0] = -3.25887 * CLHEP::cm;
    coordDDatU2[1] = 3.4626 * CLHEP::cm;
    coordDDatV2[0] = 3.74702 * CLHEP::cm;
    coordDDatV2[1] = 3.63749 * CLHEP::cm;
    coordDeadU2[0] = -2.1200 * CLHEP::cm;
    coordDeadU2[1] =  2.2548 * CLHEP::cm;
    coordDeadV2[0] = 2.4400 * CLHEP::cm;
    coordDeadV2[1] =  2.4139 * CLHEP::cm;
    //Computing position of '2' planes and dead zones
    compU[0] = positionVector[0]+stagU[0]; //Absolute Position x of U center
    compU[1] = positionVector[1]+stagU[1]; //Absolute Position y of U center
    compU[2] = coordDDatU2[0]-compU[0]; //x distance
    compU[3] = coordDDatU2[1]-compU[1]; //y distance
    compU[4] = sqrt(pow(compU[2],2) + pow(compU[3],2));
    compU[5] = atan2(compU[3],compU[2]);
    compU[6] = (3./4.*M_PI)-compU[5];
    posSensU2[0] = compU[4] * sin(compU[6]);
    posSensU2[1] = compU[4] * cos(compU[6]);

    compDU[0] = positionVector[0]+stagU[0]; //Absolute Position x of U center
    compDU[1] = positionVector[1]+stagU[1]; //Absolute Position y of U center
    compDU[2] = coordDeadU2[0]-compDU[0]; //x distance
    compDU[3] = coordDeadU2[1]-compDU[1]; //y distance
    compDU[4] = sqrt(pow(compDU[2],2) + pow(compDU[3],2));
    compDU[5] = atan2(compDU[3],compDU[2]);
    compDU[6] = (3./4.*M_PI)-compDU[5];
    posDeadU[0] = compDU[4] * sin(compDU[6]);
    posDeadU[1] = compDU[4] * cos(compDU[6]);

    compDV[0] = positionVector[0]+stagV[0]; //Absolute Position x of U center
    compDV[1] = positionVector[1]+stagV[1]; //Absolute Position y of U center
    compDV[2] = coordDeadV2[0]-compDV[0]; //x distance
    compDV[3] = coordDeadV2[1]-compDV[1]; //y distance
    compDV[4] = sqrt(pow(compDV[2],2) + pow(compDV[3],2));
    compDV[5] = atan2(compDV[3],compDV[2]);
    compDV[6] = (1./4.*M_PI)-compDV[5];
    posDeadV[0] = compDV[4] * sin(compDV[6]);
    posDeadV[1] = compDV[4] * cos(compDV[6]);
    //V
    compV[0] = positionVector[0]+stagV[0]; //Absolute Position x of U center
    compV[1] = positionVector[1]+stagV[1]; //Absolute Position y of U center
    compV[2] = coordDDatV2[0]-compV[0]; //x distance
    compV[3] = coordDDatV2[1]-compV[1]; //y distance
    compV[4] = sqrt(pow(compV[2],2) + pow(compV[3],2));
    compV[5] = atan2(compV[3],compV[2]);
    compV[6] = (1./4.*M_PI)-compV[5];
    posSensV2[0] = compV[4] * sin(compV[6]);
    posSensV2[1] = compV[4] * cos(compV[6]);

    nWires = 320;
    nWiresV = 224;
    nWires2 = 48;
    pitch = 0.071 * CLHEP::cm;
    plane2Dist = -1.8 * CLHEP::cm;
    plane3Dist = plane2Dist - 1.8 * CLHEP::cm;

    plane4Dist = -0.6 * CLHEP::cm;
    plane5Dist = plane2Dist -0.6 * CLHEP::cm;
    plane6Dist = plane3Dist -0.6 * CLHEP::cm;
}

void T4FIPlane35::construct(G4LogicalVolume* world_log)
{
    //Debug
    /*    cout << "U center x : " << compU[0] << "; U center y : " << compU[1] << endl;
    cout << "Distance in x : " << compU[2] << "; Distance in y : " << compU[3] << endl;
    cout << "l : " << compU[4] << ", ALPHA: " << compU[5] << ", THETA: " << compU[6] << endl;
    cout << "PosSensU2x: " << posSensU2[0] << ", PosSensU2y: " << posSensU2[1] << endl;

    cout << "V center x : " << compV[0] << "; V center y : " << compV[1] << endl;
    cout << "Distance in x : " << compV[2] << "; Distance in y : " << compV[3] << endl;
    cout << "l : " << compV[4] << ", ALPHA: " << compV[5] << ", THETA: " << compV[6] << endl;
    cout << "PosSensU2x: " << posSensV2[0] << ", PosSensU2y: " << posSensV2[1] << endl;*/
    //Air mother volume
    unsigned int motherIndex = log.size();
    box.push_back(new G4Box("mother_box", 16.5 * CLHEP::cm, 23. * CLHEP::cm, 2.7 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0., 4.6255 * CLHEP::cm, -2.4015 * CLHEP::cm), log.back(), "FI35_air", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //Air mother volume for X
    unsigned int motherX = log.size();
    box.push_back(new G4Box("mother_box", 13.425  * CLHEP::cm, 17. * CLHEP::cm, 0.900 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "motherX_log"));
    new G4PVPlacement(0, G4ThreeVector(0., 0., 1.800 * CLHEP::cm), log.back(), "FI35_airX", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //Air mother volume for V
    unsigned int motherV = log.size();
    box.push_back(new G4Box("mother_box", 10.025  * CLHEP::cm, 12. * CLHEP::cm, 0.900 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "motherV_log"));
    new G4PVPlacement(rotV, G4ThreeVector(stagV[0], -4.6255 * CLHEP::cm + stagV[1], 0. * CLHEP::cm), log.back(), "FI35_airV", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //Air mother volume for U
    unsigned int motherU = log.size();
    box.push_back(new G4Box("mother_box", 10.025  * CLHEP::cm, 12. * CLHEP::cm, 0.900 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "motherU_log"));
    new G4PVPlacement(rotU, G4ThreeVector(stagU[0], -4.6255 * CLHEP::cm + stagU[1], -1.800 * CLHEP::cm), log.back(), "FI35_airU", log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //Air for supports of U (stupid way to add them)
    double zMotherShift = -0.6015 * CLHEP::cm;

    unsigned int motherSU = log.size();
    box.push_back(new G4Box("mother_box", 11.1  * CLHEP::cm, 11.45 * CLHEP::cm, 0.25 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "motherUS_log"));
    new G4PVPlacement(rotU, positionVector + G4ThreeVector(0.6255 * CLHEP::cm, /*-4.6255*/0 * CLHEP::cm, -4.7500 * CLHEP::cm + zMotherShift), log.back(), "FI35_airSU", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);

    //Air for supports of V
    unsigned int motherSV = log.size();
    box.push_back(new G4Box("mother_box", 10.  * CLHEP::cm, 1. * CLHEP::cm, 0.25 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "motherU_log"));
    new G4PVPlacement(rotV, G4ThreeVector(-8.13175 * CLHEP::cm, 3.50625 * CLHEP::cm, -1.1500 * CLHEP::cm), log.back(), "FI35_airSV",log.at(motherIndex), 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->invisible);


    //Lateral aluminum bars (aluminum to be checked!!!)
    // X plane
    box.push_back(new G4Box("Side_aluminum",1. * CLHEP::cm, 16. * CLHEP::cm, 0.900 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,"barX_log"));
    new G4PVPlacement(0, G4ThreeVector(12.425 * CLHEP::cm,0.,0. * CLHEP::cm), log.back(), "FI35_all", log.at(motherX), 0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    new G4PVPlacement(0, G4ThreeVector(-12.425 * CLHEP::cm,0.,0. * CLHEP::cm), log.back(), "FI35_alr", log.at(motherX), 0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    // V plane
    box.push_back(new G4Box("Side_aluminum_UV", 1. * CLHEP::cm, 11 * CLHEP::cm, 0.900 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,"bar VU_log"));
    new G4PVPlacement(0, G4ThreeVector(9.025 * CLHEP::cm,1. * CLHEP::cm,0.), log.back(), "FI35V_all", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    new G4PVPlacement(0, G4ThreeVector(-9.025 * CLHEP::cm,1. * CLHEP::cm,0.), log.back(), "FI35V_alr", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    // U Plane
    new G4PVPlacement(0, G4ThreeVector(9.025 * CLHEP::cm,1. * CLHEP::cm,0.), log.back(), "FI35U_all", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    new G4PVPlacement(0, G4ThreeVector(-9.025 * CLHEP::cm,1. * CLHEP::cm,0.), log.back(), "FI35U_alr", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);

    //External front locking bars
    // X Plane
    box.push_back(new G4Box("Front_aluminum_1",12.925 * CLHEP::cm, 0.5 * CLHEP::cm, 0.35 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,"lbarX_front"));
    new G4PVPlacement(0, G4ThreeVector(0.,15.5 * CLHEP::cm,0.55 * CLHEP::cm), log.back(), "FI35X_ft1", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    new G4PVPlacement(0, G4ThreeVector(0.,13.85 * CLHEP::cm,0.55 * CLHEP::cm), log.back(), "FI35X_ft2", log.at(motherIndex),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    //V plane
    box.push_back(new G4Box("Front_aluminum_1_UV",   9.525 * CLHEP::cm,0.5 * CLHEP::cm, 0.25 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,"lbarV_front"));
    new G4PVPlacement(0, G4ThreeVector(/*-11.5*/0. * CLHEP::cm,0., /*0.65*/0. * CLHEP::cm), log.back(), "FI35V_ft1", log.at(motherSV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    //U plane (to be done in a smart way)-> I selected easieast
    box.push_back(new G4Box("Front_aluminum_1_UV", 9.525 * CLHEP::cm, 0.5 * CLHEP::cm, 0.25 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,"lbarV_front"));
    new G4PVPlacement(0, G4ThreeVector((-0.5) * CLHEP::cm,(14.5-3.6255) * CLHEP::cm, 0. * CLHEP::cm), log.back(), "FI35U_ft1", log.at(motherSU),0,0,checkOverlap);
    new G4PVPlacement(0, G4ThreeVector((-0.5) * CLHEP::cm,(12.5-3.6255) * CLHEP::cm, 0. * CLHEP::cm), log.back(), "FI35U_ft2", log.at(motherSU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);

    //Internal front locking bars
    // X plane
    box.push_back(new G4Box("Internal_aluminum_1",11.425 * CLHEP::cm,0.5 * CLHEP::cm,0.6 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "lbarX_int"));
    new G4PVPlacement(0, G4ThreeVector(0.,15.5 * CLHEP::cm,-0.3 * CLHEP::cm), log.back(), "FI35X_it1", log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    new G4PVPlacement(0, G4ThreeVector(0.,13.85 * CLHEP::cm,-0.3 * CLHEP::cm), log.back(), "FI35X_it2", log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    // V plane
    box.push_back(new G4Box("Internal_aluminum_1_UV", 8.025 * CLHEP::cm , 0.5 * CLHEP::cm, 0.6 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "lbarUV_int"));
    new G4PVPlacement(0, G4ThreeVector(0.,11.5 * CLHEP::cm, -0.3 * CLHEP::cm), log.back(), "FI35V_it1", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    // U Plane
    new G4PVPlacement(0, G4ThreeVector(0.,11.5 * CLHEP::cm, -0.3 * CLHEP::cm), log.back(), "FI35U_it1", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    //In the drawing but not in the pictures
    //new G4PVPlacement(0, G4ThreeVector(0.,9.95 * CLHEP::cm, -0.3 * CLHEP::cm), log.back(), "FI35U_it2", log.at(motherU),0,0,checkOverlap);
    //log.back()->SetVisAttributes(colour->gray);

    //Bottom locker
    //X Plane
    box.push_back(new G4Box("Bottom_locker_X", 13.025 * CLHEP::cm, 0.5 * CLHEP::cm, 0.75 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "top_barX"));
    new G4PVPlacement(0, G4ThreeVector(0.,-16.5 * CLHEP::cm, 0.15 * CLHEP::cm), log.back(), "FI35X_bl", log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    //V Plane
    box.push_back(new G4Box("Bottom_locker_Y", 9.025 * CLHEP::cm ,0.25 * CLHEP::cm, 0.5 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "bottom_barV"));
    new G4PVPlacement(0, G4ThreeVector(0.,-10.25 * CLHEP::cm, 0.4 * CLHEP::cm), log.back(), "FI35V_b1", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    // U plane
    new G4PVPlacement(0, G4ThreeVector(0.,-10.25 * CLHEP::cm, 0.4 * CLHEP::cm), log.back(), "FI35U_b1", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);

    // V plane
    box.push_back(new G4Box("Bottom_locker_thick_Y", 8.025 * CLHEP::cm ,1. * CLHEP::cm, 0.5 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "bottom_barV"));
    new G4PVPlacement(0, G4ThreeVector(0.,-9.0 * CLHEP::cm, 0.4 * CLHEP::cm), log.back(), "FI35V_b2", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);
    //U Plane
    new G4PVPlacement(0, G4ThreeVector(0.,-9.0 * CLHEP::cm, 0.4 * CLHEP::cm), log.back(), "FI35U_b2", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->gray);

    //Sensitive plane X
    unsigned int sensX_index = log.size();
    box.push_back(new G4Box("Sense_X",dimSens[0],dimSens[1],dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_SensX"));
    new G4PVPlacement(0, G4ThreeVector(0.,-4.6255 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_senseX", log.at(motherX),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));
    log.back()->SetVisAttributes(colour->blue);

    //Sensitive plane V
    unsigned int sensV_index = log.size();
    box.push_back(new G4Box("Sense_UV",dimSensV,dimSensV,dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_SensV"));
    new G4PVPlacement(0, G4ThreeVector(0.,0., 0.6015 * CLHEP::cm), log.back(), "FI35_senseV", log.at(motherV),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId+1));
    log.back()->SetVisAttributes(colour->blue);
    //Sensitive plane U
    unsigned int sensU_index = log.size();
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_SensU"));
    new G4PVPlacement(0, G4ThreeVector(0.,0., 0.6015 * CLHEP::cm), log.back(), "FI35_senseU", log.at(motherU),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId+2));
    log.back()->SetVisAttributes(colour->blue);

    //Dead Zone X1
    box.push_back(new G4Box("dead_X",dimDeadX[0],dimDeadX[1],dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_deadX"));
    new G4PVPlacement(0, G4ThreeVector(posDeadX[0],posDeadX[1], 0.), log.back(), "FI35_deadX", log.at(sensX_index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //Dead Zone V1 and U1
    box.push_back(new G4Box("dead_V",dimDeadV[0],dimDeadV[1],dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_deadV"));
    new G4PVPlacement(0, G4ThreeVector(posDeadV[0],posDeadV[1], 0.), log.back(), "FI35_deadV", log.at(sensV_index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    box.push_back(new G4Box("dead_U",dimDeadV[0],dimDeadV[1],dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_deadU"));
    new G4PVPlacement(0, G4ThreeVector(posDeadV[0],posDeadV[1], 0.), log.back(), "FI35_deadU", log.at(sensU_index),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //Infrabar between sensitive and non sensitive part (it not exists, converted in unactive fiber)
    //X plane
    box.push_back(new G4Box("Infra_X", dimSens[0], 0.5035 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_infraX"));
    new G4PVPlacement(0, G4ThreeVector(0.,7.2525 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_infraX", log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);
    //V plane
    box.push_back(new G4Box("Infra_UV", dimSensV, 0.55 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_infraUV"));
    new G4PVPlacement(0, G4ThreeVector(0.,8.5165 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_infraV", log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);
    //U plane
    new G4PVPlacement(0, G4ThreeVector(0.,8.5165 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_infraU", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //Top dead X
    box.push_back(new G4Box("Top_dead_X", dimSens[0], 4.122 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_top_dead_X"));
    new G4PVPlacement(0, G4ThreeVector(0.,11.878 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_tdeadX", log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //Top dead V
    box.push_back(new G4Box("Top_dead_X", dimSensV, 1.4535 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI35_top_dead_UV"));
    new G4PVPlacement(0, G4ThreeVector(0.,10.52 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_tdeadV", log.at(motherV),0,0,checkOverlap);
    //Top dead U
    new G4PVPlacement(0, G4ThreeVector(0.,10.52 * CLHEP::cm, 0.6015 * CLHEP::cm), log.back(), "FI35_tdeadU", log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //X2 plane
    box.push_back(new G4Box("x2_box",dimSensX2[0], dimSensX2[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"x2_log"));
    new G4PVPlacement(0,G4ThreeVector(-0.06755 * CLHEP::cm, 1.924 * CLHEP::cm ,0.0015 * CLHEP::cm),log.back(),"FI35_X2",log.at(motherX),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane4, 0, TGEANT::HIT, firstDetectorId+3));
    log.back()->SetVisAttributes(colour->blue);

    //V2 plane
    box.push_back(new G4Box("V2_box",dimSensV2[0], dimSensV2[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"v2_log"));
    new G4PVPlacement(0,G4ThreeVector(posSensV2[0], posSensV2[1] ,0.0015 * CLHEP::cm),log.back(),"FI35_V2",log.at(motherV),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane5, 0, TGEANT::HIT, firstDetectorId+4));
    log.back()->SetVisAttributes(colour->blue);
    //U2 plane
    box.push_back(new G4Box("U2_box",dimSensV2[0], dimSensV2[1], dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u2_log"));
    new G4PVPlacement(0,G4ThreeVector(posSensU2[0], posSensU2[1] ,0.0015 * CLHEP::cm),log.back(),"FI35_U2",log.at(motherU),0,0,checkOverlap);
    log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane6, 0, TGEANT::HIT, firstDetectorId+5));
    log.back()->SetVisAttributes(colour->blue);

    //X2 lightguide (supposed)
    box.push_back(new G4Box("x_lg_box",dimSensX2[0], 3.2505 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"x2_lg_log"));
    new G4PVPlacement(0,G4ThreeVector(-0.06755 * CLHEP::cm, 9.9995 * CLHEP::cm ,0.0015 * CLHEP::cm),log.back(),"FI35_X2_lightguide",log.at(motherX),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //V2 lightguide (supposed)
    box.push_back(new G4Box("v2_lg_box",dimSensX2[0], 1.4635 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"v2_lg_log"));
    new G4PVPlacement(0,G4ThreeVector(0.05 * CLHEP::cm, 9.5355 * CLHEP::cm ,0.0015 * CLHEP::cm),log.back(),"FI35_V2_lightguide",log.at(motherV),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);
    //U2 lightguide (supposed)
    box.push_back(new G4Box("u2_lg_box",dimSensX2[0], 1.5435 * CLHEP::cm, dimSens[2]));
    log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u2_lg_log"));
    new G4PVPlacement(0,G4ThreeVector(0. * CLHEP::cm, 9.4555 * CLHEP::cm ,0.0015 * CLHEP::cm),log.back(),"FI35_U2_lightguide",log.at(motherU),0,0,checkOverlap);
    log.back()->SetVisAttributes(colour->darkblue);

    //=================================
    //Support structure
    //Front and back rectangular panel
    box.push_back(new G4Box("SP_front_rt",18.5 * CLHEP::cm, 11.7 * CLHEP::cm, 0.025 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->aluminium_noOptical,"SP_front_rt_log"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(0., -7.525 * CLHEP::cm, -5.1250 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_Front", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(0, positionVector + G4ThreeVector(0., -7.525 * CLHEP::cm, 0.9250 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_Front", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Lateral rectangular panels
    box.push_back(new G4Box("SP_side_rt",1. * CLHEP::cm, 11.7 * CLHEP::cm, 2.95 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->aluminium_noOptical,"SP_side_rt_log"));
    new G4PVPlacement(0, positionVector + G4ThreeVector(17.5 * CLHEP::cm, -7.525 * CLHEP::cm, -2.1000 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_Lat", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(0, positionVector + G4ThreeVector(-17.5 * CLHEP::cm, -7.525 * CLHEP::cm, -2.1000 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_Lat", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Thin trapezoidal planes
    trap.push_back(new G4Trd("Front_Trapez_panel_1", 18.5 * CLHEP::cm, 56.25 * CLHEP::cm, 0.025 * CLHEP::cm, 0.025 * CLHEP::cm, 32.525 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(trap.back(),materials->aluminium_noOptical,"SP_trapFront_log"));
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0. * CLHEP::cm, 36.7 * CLHEP::cm, -5.125 * CLHEP::cm + zMotherShift), log.back(), "FI35_Trap_SP", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0. * CLHEP::cm, 36.7 * CLHEP::cm, 0.9250 * CLHEP::cm + zMotherShift), log.back(), "FI35_Trap_SP", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Lateral closures to thin trapezoidal planes
    box.push_back(new G4Box("Lateral_inc_SP", 1.*CLHEP::cm, 37.26655 * CLHEP::cm, 2.95 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->aluminium_noOptical,"SP_side_inc_log"));
    new G4PVPlacement(rotS, positionVector + G4ThreeVector(36.700 * CLHEP::cm, 36.9864 * CLHEP::cm, -2.10 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_LatIn", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(rotAS, positionVector + G4ThreeVector(-36.700 * CLHEP::cm, 36.9864 * CLHEP::cm, -2.10 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_LatIn", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Thick trapezoidal structure
    trap.push_back(new G4Trd("Front_Trapez_panel_2",  56.25 * CLHEP::cm, 92.5 * CLHEP::cm, 0.025 * CLHEP::cm, 0.025 * CLHEP::cm, 30.74 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(trap.back(),materials->aluminium_noOptical,"SP_trapFront_2_log"));
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0. * CLHEP::cm, 99.965 * CLHEP::cm, -15.725 * CLHEP::cm + zMotherShift), log.back(), "FI35_Trap2_SPF", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0. * CLHEP::cm, 99.965 * CLHEP::cm, 0.925 * CLHEP::cm + zMotherShift), log.back(), "FI35_Trap2_SPB", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Box base for additional part
    //box.push_back(new G4Box("Infra_Base_SP", 54.5 * CLHEP::cm, 2. * CLHEP::cm, 2 * CLHEP::cm));
    trap.push_back(new G4Trd("Infra_Trapez_Base_SP",  53.5 * CLHEP::cm, 56. * CLHEP::cm, 2. * CLHEP::cm, 2. * CLHEP::cm, 2. * CLHEP::cm));
    log.push_back(new G4LogicalVolume(trap.back(), materials->aluminium_noOptical,"Infra_SP_log"));
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0., 71.225 * CLHEP::cm, -13.7 * CLHEP::cm + zMotherShift),log.back(), "FI35_Infra", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Closures for thick trapezoidal structures
    box.push_back(new G4Box("Lateral_inc2_SP", 1.*CLHEP::cm, 34.999 * CLHEP::cm, 8.25 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(),materials->aluminium_noOptical,"SP_side_inc_log"));
    new G4PVPlacement(rotS, positionVector + G4ThreeVector(72.8333 * CLHEP::cm, 99.7441 * CLHEP::cm, -7.35 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_LatIn", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);
    new G4PVPlacement(rotAS, positionVector + G4ThreeVector(-72.8333 * CLHEP::cm, 99.7441 * CLHEP::cm, -7.35 * CLHEP::cm + zMotherShift), log.back(), "FI35_SP_LatIn", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

    //Infra bar in middle of thick trapezoidal structure
    trap.push_back(new G4Trd("Infra_Trapez_box_2_SP",  60. * CLHEP::cm, 64.45 * CLHEP::cm, 2. * CLHEP::cm, 2. * CLHEP::cm, 4. * CLHEP::cm));
    log.push_back(new G4LogicalVolume(trap.back(),materials->aluminium_noOptical,"SP_trapFront_2_log"));
    new G4PVPlacement(rotX, positionVector + G4ThreeVector(0. * CLHEP::cm, 84.425 * CLHEP::cm, -1.1 * CLHEP::cm + zMotherShift), log.back(), "FI35_Trap2_SP", world_log, 0, 0, checkOverlap);
    log.back()->SetVisAttributes(colour->darkgray_50);

}
/*
T4FIPlane36::T4FIPlane36(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = namePlane3 = namePlane4 = namePlane5 = namePlane6 = det->name;

  namePlane2[4] = namePlane5[4] = 'U';
  namePlane3[4] = namePlane6[4] = 'V';
  usePlane3 = true;

  namePlane4[5] = namePlane5[5] = namePlane6[5] = '2';

  rotU = new CLHEP::HepRotation;
  rotU->rotateZ(45.0 * CLHEP::deg);

  rotV = new CLHEP::HepRotation;
  rotV->rotateZ(-45.0 * CLHEP::deg);
  // U plane in principal the same box, no need for the -45deg rotation

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 11.3745 * CLHEP::cm;
  dimSens[1] = 11.3745 * CLHEP::cm;
  dimSens[2] = 0.2985 * CLHEP::cm;
  dimSensV = 7.9665 * CLHEP::cm;

  dimSensX2[0] = 1.704 * CLHEP::cm;
  dimSensX2[1] = 4.825 * CLHEP::cm;
  dimSensV2[0] = 1.704 * CLHEP::cm;
  dimSensV2[1] = 3.125 * CLHEP::cm;

  dimDeadX[0] = 1.5 * CLHEP::cm;
  dimDeadX[1] = 6.409 * CLHEP::cm;
  dimDeadV[0] = 1.5 * CLHEP::cm;
  dimDeadV[1] = 4.725 * CLHEP::cm;

  posDeadX[0] = 0;
  posDeadX[1] = 4.9655 * CLHEP::cm;
  posDeadV[0] = 0;
  posDeadV[1] = 3.226 * CLHEP::cm;

  nWires = 320;
  nWiresV = 224;
  nWires2 = 48;
  pitch = 0.071 * CLHEP::cm;
  plane2Dist = -1.8 * CLHEP::cm;
  plane3Dist = plane2Dist - 1.8 * CLHEP::cm;

  plane4Dist = -0.6 * CLHEP::cm;
  plane5Dist = plane2Dist -0.6 * CLHEP::cm;
  plane6Dist = plane3Dist -0.6 * CLHEP::cm;

//  // Vertex detector - FI35
//  det   51  FI35U2__  VSY1  1   22    42.14   0.597   3.408    6.250    -91.3700   -3.27176    3.45993    5     -1.6434   -1.213    48  0.0699283 1.000   0.0    5.0
//  det   21  FI35U1__  VSZ1  1   22    42.14   0.597  15.933   15.933    -90.7700    0.09172    0.04967    5     -7.8761   -0.887   224  0.0706382 1.000   0.0    5.0
//  det   41  FI35V2__  VSY3  1   22    42.14   0.597   3.408    6.250    -89.5700    3.73824    3.64646    7     -1.6861   -1.390    48  0.0717522 1.000   0.0    5.0
//  det   11  FI35V1__  VSZ3  1   22    42.14   0.597  15.933   15.933    -88.9700    0.23386    0.20350    7     -7.8886   -0.818   224  0.0707502 1.000   0.0    5.0
//  det   31  FI35X2__  VSY2  1   22    42.14   0.597   3.408    9.650    -87.7700    0.30939    6.72858    1     -1.6565   -1.392    48  0.0704888 1.000   0.0    5.0
//  det    1  FI35X1__  VSZ2  1   22    42.14   0.597  22.749   22.749    -87.1700    0.22547    0.20325    1    -11.2661   -0.521   320  0.0706339 1.000   0.0    5.0

//  #wires  pitch    angle  wir1-orig  flag   dead
//  USET01PLA1PAR   224    0.071     0.    -7.9165    0     11
//  USET01PLA2PAR   224    0.071     0.    -7.9165    0     12
//  USET01PLA3PAR   320    0.071     0.   -11.3245    0     13
//  USET01PLA4PAR    48    0.071     0.    -1.65      0
//  USET01PLA5PAR    48    0.071     0.    -1.65      0
//  USET01PLA6PAR    48    0.071     0.    -1.65      0
}

void T4FIPlane36::construct(G4LogicalVolume* world_log)
{
  //Implementation a la COMGEANT, from Catarina's file 27/05/2015
  
//   C              med moth rot     X    Y    Z   shape  npar   X      Y       Z
//   USET01VOL1PAR   625  4   0    0.     0.   0.  'BOX '  3    0.2985  7.9665  7.9665
//   USET01VOL2PAR   625  5   0    0.     0.   0.  'BOX '  3    0.2985  7.9665  7.9665
//   USET01VOL3PAR   625  6   0    0.     0.   0.  'BOX '  3    0.2985 11.3745 11.3745
//   USET01VOL4PAR   224  7   0    0.0    0.   0.  'BOX '  3    0.2985  7.97    7.97
//   USET01VOL5PAR   224  8   0    0.0    0.   0.  'BOX '  3    0.2985  7.97    7.97
//   USET01VOL6PAR   224  9   0    0.0    0.   0.  'BOX '  3    0.2985 11.4    11.4
//   USET01VOL7PAR   201 10   6   -1.5    0.09 0.05 'BOX ' 3    0.2985  8.    8.         <-FI35U1__
//   USET01VOL8PAR   201 10   8    0.3    0.23 0.20 'BOX ' 3    0.2985  8.    8.         <-FI35V1__
//   USET01VOL9PAR   201 10   0    2.1    0.22 0.20 'BOX ' 3    0.2985 11.5  11.5        <-FI35X1__ (global position)
//   USET01VOLAPAR   201  0   0    0.0    0.   0.  'BOX '  3    2.6    11.8  11.7
//   USET01VOLBPAR   224  1   0    0.     0.   3.226 'BOX '  3  0.2985  1.5   4.725
//   USET01VOLCPAR   224  2   0    0.     0.   3.226 'BOX '  3  0.2985  1.5   4.725
//   USET01VOLDPAR   224  3   0    0.     0.   3.226 'BOX '  3  0.2985  1.5   4.725
//   USET01VOLEPAR   625 10   6   -2.1    0.   0.  'BOX '  3    0.2985 1.704 3.125       <-FI35U2__
//   USET02VOLFPAR   625 10   8   -0.3    0.   0.  'BOX '  3    0.2985 1.704 3.125       <-FI35V2__
//   USET02VOLGPAR   625 10   0   -(+)1.5    0.   0.  'BOX '  3    0.2985 1.704 4.825       <-FI35X2__
//   C
//   USET01TBNAME     'FI35U1__' 'FI35V1__' 'FI35X1__' 'FI35U2__' 'FI35V2__' 'FI35X2__'
//



  // Air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 11.8  * CLHEP::cm, 11.7 * CLHEP::cm, 2.6 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, -2.1 * CLHEP::cm), log.back(), "FI35_air", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int X_mother_Index = log.size();
  // most downstream
  box.push_back(new G4Box("x_mother_box", 11.5 * CLHEP::cm, 11.5 * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "x_mother_log"));
  new G4PVPlacement(0, G4ThreeVector(0.22 * CLHEP::cm, 0.20 * CLHEP::cm, 2.1 * CLHEP::cm),log.back(),"x_FI35_air", log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int U_mother_Index = log.size();
  // most upstream
  box.push_back(new G4Box("u_mother_box", 8. * CLHEP::cm, 8. * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "u_mother_log"));
  new G4PVPlacement(rotU, G4ThreeVector(0.09 * CLHEP::cm, 0.05 * CLHEP::cm, -1.5 * CLHEP::cm),log.back(),"u_FI35_air", log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int V_mother_Index = log.size();
  // middle
  box.push_back(new G4Box("v_mother_box", 8. * CLHEP::cm, 8. * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "v_mother_log"));
  new G4PVPlacement(rotV, G4ThreeVector(0.23 * CLHEP::cm, 0.20 * CLHEP::cm, 0.3 * CLHEP::cm),log.back(),"v_FI35_air", log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int X_un_index = log.size();
  box.push_back(new G4Box("X_unac_box",11.4 * CLHEP::cm, 11.4 * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"x_un_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"xun_FI35",log.at(X_mother_Index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int U_un_index = log.size();
  box.push_back(new G4Box("U_unac_box",7.97 * CLHEP::cm, 7.97 * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u_un_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"uun_FI35",log.at(U_mother_Index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int V_un_index = log.size();
  box.push_back(new G4Box("V_unac_box",7.97 * CLHEP::cm, 7.97 * CLHEP::cm, 0.2985 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u_un_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"vun_FI35",log.at(V_mother_Index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int X_act_index = log.size();
  box.push_back(new G4Box("X_act_box",dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"x_act_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"xact_FI35",log.at(X_un_index),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));
  log.back()->SetVisAttributes(colour->cyan);

  unsigned int U_act_index = log.size();
  box.push_back(new G4Box("U_act_box",dimSensV, dimSensV, dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u_act_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"uact_FI35",log.at(U_un_index),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId+1));
  log.back()->SetVisAttributes(colour->cyan);

  unsigned int V_act_index = log.size();
  box.push_back(new G4Box("V_act_box",dimSensV, dimSensV, dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"v_act_log"));
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),log.back(),"vact_FI35",log.at(V_un_index),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId+2));
  log.back()->SetVisAttributes(colour->cyan);

  box.push_back(new G4Box("X_dead_box",dimDeadX[0], dimDeadX[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"x_dead_log"));
  new G4PVPlacement(0,G4ThreeVector(posDeadX[0],posDeadX[1],0.),log.back(),"xdead_FI35",log.at(X_act_index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);

  box.push_back(new G4Box("U_dead_box",dimDeadV[0], dimDeadV[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"u_dead_log"));
  new G4PVPlacement(0,G4ThreeVector(posDeadV[0],posDeadV[1],0.),log.back(),"udead_FI35",log.at(U_act_index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);

  box.push_back(new G4Box("V_dead_box",dimDeadV[0], dimDeadV[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"v_dead_log"));
  new G4PVPlacement(0,G4ThreeVector(posDeadV[0],posDeadV[1],0.),log.back(),"vdead_FI35",log.at(V_act_index),0,0,checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);

  box.push_back(new G4Box("x_lg_box",dimSensX2[0], dimSensX2[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"xlg_log"));
  new G4PVPlacement(0,G4ThreeVector(0.32 * CLHEP::cm, 6.72 * CLHEP::cm ,1.5 * CLHEP::cm),log.back(),"xlg_FI35",log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane4, 0, TGEANT::HIT, firstDetectorId+3));
  log.back()->SetVisAttributes(colour->darkgreen);

  box.push_back(new G4Box("u_lg_box",dimSensV2[0], dimSensV2[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"ulg_log"));
  new G4PVPlacement(rotU,G4ThreeVector(3.75 * CLHEP::cm ,3.64 *CLHEP::cm,-2.1 * CLHEP::cm),log.back(),"ulg_FI35",log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane5, 0, TGEANT::HIT, firstDetectorId+4));
  log.back()->SetVisAttributes(colour->darkgreen);

  box.push_back(new G4Box("v_lg_box",dimSensV2[0], dimSensV2[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(),materials->bc408_noOptical,"vlg_log"));
  new G4PVPlacement(rotV,G4ThreeVector(-3.26 * CLHEP::cm , 3.46 *CLHEP::cm,-0.3 * CLHEP::cm),log.back(),"vlg_FI35",log.at(motherIndex),0,0,checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane6, 0, TGEANT::HIT, firstDetectorId+5));
  log.back()->SetVisAttributes(colour->darkgreen);
}
*/
T4FIPlane04::T4FIPlane04(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';
  namePlane3 = det->name;
  namePlane3[4] = 'U';
  usePlane3 = true;

  rotY = new CLHEP::HepRotation;
  rotY->rotateZ(90.0 * CLHEP::deg);
  rotU = new CLHEP::HepRotation;
  rotU->rotateZ(-45.0 * CLHEP::deg);

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 2.625 * CLHEP::cm;
  dimSens[1] = 2.625 * CLHEP::cm;
  dimSens[2] = 0.211 * CLHEP::cm;

  nWires = 128;
  pitch = 0.041 * CLHEP::cm;
  plane2Dist = 0.5 * CLHEP::cm;
  plane3Dist = plane2Dist + 1.1 * CLHEP::cm;
}

void T4FIPlane04::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 100.0 * CLHEP::cm, 100.0 * CLHEP::cm, 3.225 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, -0.025 * CLHEP::cm), log.back(), "FI04_air", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // air_X in mother
  unsigned int air_XIndex = log.size();
  box.push_back(new G4Box("air_X_box", 4.325 * CLHEP::cm, 15.0 * CLHEP::cm, 0.55 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.275 * CLHEP::cm), log.back(), "FI04_air_X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);
  
  // air_Y in mother
  unsigned int air_YIndex = log.size();
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_Y_log"));
  new G4PVPlacement(rotY, G4ThreeVector(0, 0, 0.825 * CLHEP::cm), log.back(), "FI04_air_Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // air_U in mother
  unsigned int air_UIndex = log.size();
  box.push_back(new G4Box("air_U_box", 4.325 * CLHEP::cm, 15.0 * CLHEP::cm, 0.8 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_U_log"));
  new G4PVPlacement(rotU, G4ThreeVector(0, 0, 2.175 * CLHEP::cm), log.back(), "FI04_air_U", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // sens_X in air_X
  box.push_back(new G4Box("sens_X_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI04_sens_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.3 * CLHEP::cm), log.back(), "FI04_sens_X", log.at(air_XIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));
  log.back()->SetVisAttributes(colour->red);
  
  // sens_Y in air_Y
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI04_sens_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.3 * CLHEP::cm), log.back(), "FI04_sens_Y", log.at(air_YIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));
  log.back()->SetVisAttributes(colour->red);
  
  // sens_U in air_U
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI04_sens_U_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.55 * CLHEP::cm), log.back(), "FI04_sens_U", log.at(air_UIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId + 2));
  log.back()->SetVisAttributes(colour->red);
  
  // scint in air_XYU
  box.push_back(new G4Box("scint_box", 2.625 * CLHEP::cm, 6.1875 * CLHEP::cm, 0.211 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -8.8125 * CLHEP::cm, 0), log.back(), "FI04_scint_X", log.at(air_XIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 8.8125 * CLHEP::cm, 0), log.back(), "FI04_scint_Y", log.at(air_YIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, -8.8125 * CLHEP::cm, 0), log.back(), "FI04_scint_U", log.at(air_UIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);
  
  // rohacell in air_XY
  box.push_back(new G4Box("rohacell_box", 0.85 * CLHEP::cm, 4.3 * CLHEP::cm, 0.55 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_log"));
  new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_X1", log.at(air_XIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_Y1", log.at(air_YIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_X2", log.at(air_XIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_Y2", log.at(air_YIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // rohacell_U in air_U
  box.push_back(new G4Box("rohacell_U_box", 0.85 * CLHEP::cm, 4.3 * CLHEP::cm, 0.8 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_U_log"));
  new G4PVPlacement(0, G4ThreeVector(3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_U1", log.at(air_UIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-3.475 * CLHEP::cm, 0, 0), log.back(), "FI04_rohacell_U2", log.at(air_UIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  G4double zPlane[2] = {-2.*CLHEP::cm, 2.*CLHEP::cm};
  G4double rMin[2] = {15.*CLHEP::cm, 15.*CLHEP::cm};
  G4double rMax[2] = {16.*CLHEP::cm, 16.*CLHEP::cm};
  G4double rMPVC[2] = {16.025*CLHEP::cm, 16.025*CLHEP::cm};

  // rohacell polygon
  poly.push_back(new G4Polyhedra("rohacell_polg04",22.5 * CLHEP::degree,360. * CLHEP::degree,8,2,zPlane,rMin,rMax));
  log.push_back(new G4LogicalVolume(poly.back(),materials->rohacell,"rohacell_polg04_log"));
  new G4PVPlacement(0, G4ThreeVector(0., 0., 1.175 * CLHEP::cm), log.back(), "FI04_rohacell_polg", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // pvc polygon
  poly.push_back(new G4Polyhedra("pvc_polg04",22.5 * CLHEP::degree, 360. * CLHEP::degree,8,2,zPlane,rMax,rMPVC));
  log.push_back(new G4LogicalVolume(poly.back(),materials->pvc,"pvc_polg04_log"));
  new G4PVPlacement(0, G4ThreeVector(0., 0., 1.175* CLHEP::cm), log.back(), "FI04_pvc_polg", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkgray);

  //  GSET13VOLHPAR   237 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 15. 16. 2. 15.  16.     Rc50
  //  GSET13VOLIPAR   228 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 16. 16.025 2. 16. 16.025 PVC /pvc

  // pvc in mother
  box.push_back(new G4Box("pvc_box", 16. * CLHEP::cm, 16. * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->pvc, "pvc_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 3.2 * CLHEP::cm), log.back(), "FI04_pvc", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // rohacell_3 in mother
  unsigned int rohacell_3Index = log.size();
  box.push_back(new G4Box("rohacell_3_box", 100. * CLHEP::cm, 100. * CLHEP::cm, 1.2 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "rohacell_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -2.025 * CLHEP::cm), log.back(), "FI04_rohacell_3", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // air_2 in rohacell_3
  tubs.push_back(new G4Tubs("air_2_tubs", 0, 3.0 * CLHEP::cm,  1.2 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI04_air_2", log.at(rohacell_3Index), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  // scint_2 in mother
  tubs.push_back(new G4Tubs("scint_2_tubs", 20. * CLHEP::cm, 28.0 * CLHEP::cm,  0.137 * CLHEP::cm, 180.*CLHEP::deg, 90.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->bc408_noOptical, "scint_2_log"));
  new G4PVPlacement(0, G4ThreeVector(24. * CLHEP::cm, -16.025 * CLHEP::cm, 1.2 * CLHEP::cm), log.back(), "FI04_scint_2", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);

  // scint_3 in mother
  tubs.push_back(new G4Tubs("scint_3_tubs", 22.3 * CLHEP::cm, 30.3 * CLHEP::cm,  0.137 * CLHEP::cm, 225.*CLHEP::deg, 45.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->bc408_noOptical, "scint_3_log"));
  new G4PVPlacement(0, G4ThreeVector(29.0 * CLHEP::cm, 6.3 * CLHEP::cm, 1.2 * CLHEP::cm), log.back(), "FI04_scint_3", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->darkblue);

  // scint_4 in mother
  box.push_back(new G4Box("scint_4_box", 38. * CLHEP::cm, 4. * CLHEP::cm, 0.137 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "scint_4_log"));
  new G4PVPlacement(0, G4ThreeVector(62.0 * CLHEP::cm, -40. * CLHEP::cm, 1.2 * CLHEP::cm), log.back(), "FI04_scint_4", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // scint_5 in mother
  box.push_back(new G4Box("scint_5_box", 35.5 * CLHEP::cm, 4. * CLHEP::cm, 0.137 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "scint_5_log"));
  new G4PVPlacement(0, G4ThreeVector(64.5 * CLHEP::cm, -20. * CLHEP::cm, 1.2 * CLHEP::cm), log.back(), "FI04_scint_5", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);

  // scint_6 in mother
  box.push_back(new G4Box("scint_6_box", 41.98 * CLHEP::cm, 4. * CLHEP::cm, 0.137 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell, "scint_6_log"));
  new G4PVPlacement(0, G4ThreeVector(58.01 * CLHEP::cm, 0, 1.2 * CLHEP::cm), log.back(), "FI04_scint_6", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->orange);


  //04
  //  GSET13VOL1PAR   625  7   0    0.3    0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens bc408
  //  GSET13VOL2PAR   625  8   0   -0.3    0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens
  //  GSET13VOL3PAR   625  9   0   -0.55   0.    0.     'BOX '  3    0.211  2.625  2.625   SciFi sens
  //  GSET13VOL4PAR   224  7   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi bc408
  //  GSET13VOL5PAR   224  8   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi
  //  GSET13VOL6PAR   224  9   0    0.0    0.   -8.8125 'BOX '  3    0.211  2.625  6.1875  ClFi
  //  GSET13VOL7PAR   201 10   0   -0.275   0.0   0.    'BOX '  3    0.55   4.325 15.0     Air /air
  //  GSET13VOL8PAR   201 10   3   +0.825   0.0   0.    'BOX '  3    0.55   4.325 15.0     Air
  //  GSET13VOL9PAR   201 10   6   +2.175   0.0   0.    'BOX '  3    0.8    4.325 15.0     Air
  //  GSET13VOLAPAR   201  0   0    0.0     0.0   0.    'BOX '  3    3.225 100.0 100.0     Air
  //  GSET13VOLBPAR   237  7   0    0.0    3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50 rohacell_50
  //  GSET13VOLCPAR   237  8   0    0.0    3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
  //  GSET13VOLDPAR   237  9   0    0.0    3.475 0.     'BOX '  3    0.8    0.85   4.3     Rcell50
  //  GSET13VOLEPAR   237  7   0    0.0   -3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
  //  GSET13VOLFPAR   237  8   0    0.0   -3.475 0.     'BOX '  3    0.55   0.85   4.3     Rcell50
  //  GSET13VOLGPAR   237  9   0    0.0   -3.475 0.     'BOX '  3    0.8    0.85   4.3     Rcell50
//  GSET13VOLHPAR   237 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 15. 16. 2. 15.  16.     Rc50
//  GSET13VOLIPAR   228 10   1    1.175  0.0   0.  'PGON' 10 22.5 360. 8. 2. -2. 16. 16.025 2. 16. 16.025 PVC /pvc
  //  GSET13VOLJPAR   228 10   0    3.2    0.0   0.     'BOX '  3    0.025 16.    16.      PVC
  //  GSET13VOLKPAR   237 10   0   -2.025  0.0   0.     'BOX '  3    1.2  100.   100.      Rcell50
  //  GSET13VOLLPAR   201 20   1    0.0    0.0   0.     'TUBE'  3    0.     3.0    1.2     Air /ari
  //  GSET13VOLMPAR   224 10   1    1.2   24.  -16.025  'TUBS'  5   20.    28.0    0.137  180. 270.    ClFi /bc408
  //  GSET13VOLNPAR   224 10   1    1.2   29.0   6.3    'TUBS'  5   22.3    30.3   0.137  225. 270.    ClFi
  //  GSET13VOLOPAR   224 10   0    1.2   58.01  0.     'BOX '  3    0.137  41.98  4.      ClFi
  //  GSET13VOLPPAR   224 10   0    1.2   62.0 -40.     'BOX '  3    0.137  38.    4.      ClFi
  //  GSET13VOLQPAR   224 10   0    1.2   64.5 -20.     'BOX '  3    0.137  35.5   4.      ClFi


  /*
  GSET12VOLMPAR   224 10   1    1.2   24.  -16.025  'TUBS'  5   20.    28.0    0.137  180. 235.    ClFi
  GSET12VOLNPAR   224 10   1    1.2   29.1   6.4    'TUBS'  5   22.3    30.3   0.137  225. 275.    ClFi
  GSET12VOLOPAR   224 10   0    1.2   27.015 0.     'BOX '  3    0.137  10.985 4.      ClFi
    */
}

T4FIPlane05::T4FIPlane05(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 4.207 * CLHEP::cm;
  dimSens[1] = 4.207 * CLHEP::cm;
  dimSens[2] = 0.327 * CLHEP::cm;

  nWires = 160;
  pitch = 0.0525 * CLHEP::cm;
  plane2Dist = 1.6 * CLHEP::cm;
}

void T4FIPlane05::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 168.0 * CLHEP::cm, 120.0 * CLHEP::cm, 3.0 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI05_air", world_log, 0, 0, checkOverlap);

  // sens_1 in mother
  box.push_back(new G4Box("sens_1_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8 * CLHEP::cm), log.back(), "FI05_sens_1", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // sens_1 in mother
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI05_sens_2", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  // plastic_1 in mother
  box.push_back(new G4Box("plastic_1_box", 4.21 * CLHEP::cm, 57.5 * CLHEP::cm, 0.327 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "plastic_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -61.71 * CLHEP::cm, -0.8 * CLHEP::cm), log.back(), "FI05_plastic_1", log.at(motherIndex), 0, 0, checkOverlap);

  // plastic_2 in mother
  box.push_back(new G4Box("plastic_2_box", 80.0 * CLHEP::cm, 4.21 * CLHEP::cm, 0.327 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "plastic_2_log"));
  new G4PVPlacement(0, G4ThreeVector(-84.21 * CLHEP::cm, 0, 0.8 * CLHEP::cm), log.back(), "FI05_plastic_2", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_1 in mother
  box.push_back(new G4Box("nylon_1_box", 168.0 * CLHEP::cm, 120.0 * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -2.975 * CLHEP::cm), log.back(), "FI05_nylon_1", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_2 in mother
  box.push_back(new G4Box("nylon_2_box", 4.21 * CLHEP::cm, 4.21 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.423 * CLHEP::cm), log.back(), "FI05_nylon_2", log.at(motherIndex), 0, 0, checkOverlap);


  //05
    //  GSET23VOL1PAR   625  7   0   -0.8    0.   0.   'BOX '  3    0.327   4.207  4.207   Scint bc408
    //  GSET23VOL2PAR   625  7   3   +0.8    0.   0.   'BOX '  3    0.327   4.207  4.207   Scint
    //  GSET23VOL3PAR   224  7   0   -0.8    0. -61.71 'BOX '  3    0.327   4.21  57.5     Plastic bc408
    //  GSET23VOL4PAR   224  7   0   +0.8  -84.21 0.   'BOX '  3    0.327  80.0    4.21    Plastic
    //  GSET23VOL5PAR   229  7   0   -2.975  0.   0.   'BOX '  3    0.025 168.0  120.0     Nylon nylon
    //  GSET23VOL6PAR   229  7   0    0.423  0.   0.   'BOX '  3    0.05    4.21   4.21    Nylon
    //  GSET23VOL7PAR   238  0   0    0.0    0.   0.   'BOX '  3    3.0   168.0  120.0     Rcell rohacell_30

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

T4FIPlane55::T4FIPlane55(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'V';

  rotV = new CLHEP::HepRotation;
  rotV->rotateZ(45.0 * CLHEP::deg);

  rotU = new CLHEP::HepRotation;
  rotU->rotateZ(-45.0 * CLHEP::deg);

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 3.375 * CLHEP::cm;
  dimSens[1] = 6.15 * CLHEP::cm;
  dimSens[2] = 0.294 * CLHEP::cm;

  nWires = 96;
  pitch = 0.07 * CLHEP::cm;
  plane2Dist = 1.6 * CLHEP::cm;
}

void T4FIPlane55::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 80.0 * CLHEP::cm, 80.0 * CLHEP::cm, 4.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI55_air", world_log, 0, 0, checkOverlap);

  // rot_U in mother
  unsigned int rotUIndex = log.size();
  box.push_back(new G4Box("rot_U_box", 6.15 * CLHEP::cm, 80.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "rot_U_log"));
  new G4PVPlacement(rotU, G4ThreeVector(0, 0, -0.8 * CLHEP::cm), log.back(), "FI55_rot_U", log.at(motherIndex), 0, 0, checkOverlap);

  // rot_V in mother
  unsigned int rotVIndex = log.size();
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "rot_V_log"));
  new G4PVPlacement(rotV, G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI55_rot_V", log.at(motherIndex), 0, 0, checkOverlap);

  // scint_U in mother
  unsigned int scintUIndex = log.size();
  box.push_back(new G4Box("scint_U_box", 6.15 * CLHEP::cm, 6.15 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_U_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI55_scint_U", log.at(rotUIndex), 0, 0, checkOverlap);

  // scint_V in mother
  unsigned int scintVIndex = log.size();
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_V_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI55_scint_V", log.at(rotVIndex), 0, 0, checkOverlap);

  // sens_U in mother
  box.push_back(new G4Box("sens_U_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_U_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI55_sens_U", log.at(scintUIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // sens_V in mother
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_V_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI55_sens_V", log.at(scintVIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  //55
  //  GSET30VOL1PAR   625  3   0    0.0    0.     0.   'BOX '  3    0.294   3.375  6.15   Scint bc408
  //  GSET30VOL2PAR   625  4   0    0.0    0.     0.   'BOX '  3    0.294   3.375  6.15   Scint
  //  GSET30VOL3PAR   224  5   0    0.0    0.     0.   'BOX '  3    0.294   6.15  6.15   ScintNoLight bc408
  //  GSET30VOL4PAR   224  6   0    0.0    0.     0.   'BOX '  3    0.294   6.15  6.15   ScintNoLight
  //  GSET30VOL5PAR   238  7   6   -0.8    0.0    0.   'BOX '  3    0.294   6.15  80.0    Rcell rohacell_30
  //  GSET30VOL6PAR   238  7   8    0.8    0.0    0.   'BOX '  3    0.294   6.15  80.0    Rcell
  //  GSET30VOL7PAR   238  0   0    0.0    0.0    0.   'BOX '  3    4.00   80.0  80.0    RCell

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

T4FIPlane06::T4FIPlane06(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';
  namePlane3 = det->name;
  namePlane3[4] = 'V';
  usePlane3 = true;

  rotV = new CLHEP::HepRotation;
  rotV->rotateZ(45.0 * CLHEP::deg);

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 5.014 * CLHEP::cm;
  dimSens[1] = 5.014 * CLHEP::cm;
  dimSens[2] = 0.294 * CLHEP::cm;
  dimSensV = 6.17 * CLHEP::cm;

  nWires = 143;
  nWiresV = 176;
  pitch = 0.07 * CLHEP::cm;
  plane2Dist = 0.9 * CLHEP::cm;
  plane3Dist = plane2Dist + 5.1 * CLHEP::cm;
}

void T4FIPlane06::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 80.0 * CLHEP::cm, 80.0 * CLHEP::cm, 6.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.9 * CLHEP::cm), log.back(), "FI06_air", world_log, 0, 0, checkOverlap);

  // sens_X in mother
  box.push_back(new G4Box("sens_X_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI06_sens_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.9 * CLHEP::cm), log.back(), "FI06_sens_X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // sens_Y in mother
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI06_sens_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI06_sens_Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  // rohacell_V in mother
  unsigned int rohacellVIndex = log.size();
  box.push_back(new G4Box("rohacell_V_box", 6.2 * CLHEP::cm, 80.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "rohacell_V_log"));
  new G4PVPlacement(rotV, G4ThreeVector(0, 0, 5.1 * CLHEP::cm), log.back(), "FI06_rohacell_V", log.at(motherIndex), 0, 0, checkOverlap);

  // sens_V in rohacell_V
  box.push_back(new G4Box("sens_V_box", dimSensV, dimSensV, dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "sens_V_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI06_sens_V", log.at(rohacellVIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane3, 0, TGEANT::HIT, firstDetectorId + 2));

  // scint_V in rohacell_V
  box.push_back(new G4Box("scint_V_box", 6.2 * CLHEP::cm, 36.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_V_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -42.17 * CLHEP::cm, 0), log.back(), "FI06_scint_V", log.at(rohacellVIndex), 0, 0, checkOverlap);

  // scint_X in mother
  box.push_back(new G4Box("scint_X_box", 5.1 * CLHEP::cm, 37.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -42.02 * CLHEP::cm, -0.9 * CLHEP::cm), log.back(), "FI06_scint_X", log.at(motherIndex), 0, 0, checkOverlap);

  // scint_Y in mother
  box.push_back(new G4Box("scint_Y_box", 37.0 * CLHEP::cm, 5.1 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(-42.02 * CLHEP::cm, 0, 0), log.back(), "FI06_scint_Y", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_1 in mother
  box.push_back(new G4Box("nylon_1_box", 80.0 * CLHEP::cm, 80.0 * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -5.975 * CLHEP::cm), log.back(), "FI06_nylon_1", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_2 in mother
  box.push_back(new G4Box("nylon_2_box", 6.0 * CLHEP::cm, 6.0 * CLHEP::cm, 0.075 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.369 * CLHEP::cm), log.back(), "FI06_nylon_2", log.at(motherIndex), 0, 0, checkOverlap);


  //06
    //  GSET24VOL1PAR   625 10   0   -0.900  0.     0.   'BOX '  3    0.294   5.014 5.014  Scint bc408
    //  GSET24VOL2PAR   625 10   3    0.000  0.     0.   'BOX '  3    0.294   5.014 5.014  Scint
    //  GSET24VOL3PAR   625  4   0    0.0    0.     0.   'BOX '  3    0.294   6.17  6.17   Scint
    //  GSET24VOL4PAR   238 10   8   +5.1    0.0    0.   'BOX '  3    0.294   6.2  80.0    Rcell
    //  GSET24VOL5PAR   224 10   0   -0.9    0.0  -42.02 'BOX '  3    0.294   5.1  37.0    Plastic bc408
    //  GSET24VOL6PAR   224 10   0    0.0  -42.02   0.   'BOX '  3    0.294  37.0   5.1    Plastic
    //  GSET24VOL7PAR   224  4   0    0.0    0.0 -42.17  'BOX '  3    0.294  6.2   36.0    Plastic
    //  GSET24VOL8PAR   229 10   0   -5.975  0.0    0.   'BOX '  3    0.025  80.0  80.0    Nylon nylon
    //  GSET24VOL9PAR   229 10   0    0.369  0.0    0.   'BOX '  3    0.075   6.0   6.0    Nylon
    //  GSET24VOLAPAR   238  0   0    0.0    0.0    0.   'BOX '  3    6.00   80.0  80.0    RCell rohacell_30

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

T4FIPlane07::T4FIPlane07(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 5.014 * CLHEP::cm;
  dimSens[1] = 5.014 * CLHEP::cm;
  dimSens[2] = 0.294 * CLHEP::cm;

  nWires = 143;
  pitch = 0.07 * CLHEP::cm;
  plane2Dist = 1.6 * CLHEP::cm;
}

void T4FIPlane07::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 135. * CLHEP::cm, 80.0 * CLHEP::cm, 3.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI07_air", world_log, 0, 0, checkOverlap);

  // sens_X in mother
  box.push_back(new G4Box("sens_X_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI07_sens_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8 * CLHEP::cm), log.back(), "FI07_sens_X", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // sens_Y in mother
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI07_sens_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI07_sens_Y", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  // scint_X in mother
  box.push_back(new G4Box("scint_X_box", 5.1 * CLHEP::cm, 37.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -42.014 * CLHEP::cm, -0.8 * CLHEP::cm), log.back(), "FI07_scint_X", log.at(motherIndex), 0, 0, checkOverlap);

  // scint_Y in mother
  box.push_back(new G4Box("scint_Y_box", 64.0 * CLHEP::cm, 5.1 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(-69.014 * CLHEP::cm, 0, 0.8 * CLHEP::cm), log.back(), "FI07_scint_Y", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_1 in mother
  box.push_back(new G4Box("nylon_1_box", 135.0 * CLHEP::cm, 80.0 * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -2.975 * CLHEP::cm), log.back(), "FI07_nylon_1", log.at(motherIndex), 0, 0, checkOverlap);

  // nylon_2 in mother
  box.push_back(new G4Box("nylon_2_box", 5.1 * CLHEP::cm, 5.1 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.456 * CLHEP::cm), log.back(), "FI07_nylon_2", log.at(motherIndex), 0, 0, checkOverlap);


  //07
  //  GSET25VOL1PAR   625  7   0   -0.800  0.     0.    'BOX '  3    0.294   5.014 5.014  Scint bc408
  //  GSET25VOL2PAR   625  7   3    0.800  0.     0.    'BOX '  3    0.294   5.014 5.014  Scint
  //  GSET25VOL3PAR   224  7   0   -0.8    0.   -42.014 'BOX '  3    0.294   5.1  37.0    Plastic bc408
  //  GSET25VOL4PAR   224  7   0    0.8  -69.014  0.    'BOX '  3    0.294  64.0   5.1    Plastic
  //  GSET25VOL5PAR   229  7   0   -2.975  0.0    0.    'BOX '  3    0.025 135.0  80.0    Nylon nylon
  //  GSET25VOL6PAR   229  7   0    0.456  0.0    0.    'BOX '  3    0.05    5.1   5.1    Nylon
  //  GSET25VOL7PAR   238  0   0    0.0    0.0    0.    'BOX '  3    3.00  135.   80.0    Rcell rohacell_30

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

T4FIPlane08::T4FIPlane08(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  namePlane2 = det->name;
  namePlane2[4] = 'Y';

  setPosition(det->position);
  detDatPos = positionVector;

  dimSens[0] = 6.17 * CLHEP::cm;
  dimSens[1] = 6.17 * CLHEP::cm;
  dimSens[2] = 0.294 * CLHEP::cm;

  nWires = 176;
  pitch = 0.07 * CLHEP::cm;
  plane2Dist = 1.6 * CLHEP::cm;
}

void T4FIPlane08::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 152.0 * CLHEP::cm, 121.0 * CLHEP::cm, 3.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI08_air", world_log, 0, 0, checkOverlap);

  // rohacell in mother
  unsigned int rohacellIndex = log.size();
  box.push_back(new G4Box("rohacell_box", 100.0 * CLHEP::cm, 75.0 * CLHEP::cm, 3.00 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->rohacell_30, "rohacell_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "FI08_rohacell", log.at(motherIndex), 0, 0, checkOverlap);

  // sens_X in rohacell
  box.push_back(new G4Box("sens_X_box", dimSens[0], dimSens[1], dimSens[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI08_sens_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8 * CLHEP::cm), log.back(), "FI08_sens_X", log.at(rohacellIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // sens_Y in rohacell
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "FI08_sens_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.8 * CLHEP::cm), log.back(), "FI08_sens_Y", log.at(rohacellIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1));

  // scint_X in rohacell
  box.push_back(new G4Box("scint_X_box", 6.2 * CLHEP::cm, 34.0 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_X_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -40.17 * CLHEP::cm, -0.8 * CLHEP::cm), log.back(), "FI08_scint_X", log.at(rohacellIndex), 0, 0, checkOverlap);

  // scint_Y in rohacell
  box.push_back(new G4Box("scint_Y_box", 46. * CLHEP::cm, 6.2 * CLHEP::cm, 0.294 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->bc408_noOptical, "scint_Y_log"));
  new G4PVPlacement(0, G4ThreeVector(-52.17 * CLHEP::cm, 0, 0.8 * CLHEP::cm), log.back(), "FI08_scint_Y", log.at(rohacellIndex), 0, 0, checkOverlap);

  // nylon_1 in rohacell
  box.push_back(new G4Box("nylon_1_box", 6.2 * CLHEP::cm, 6.2 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.456 * CLHEP::cm), log.back(), "FI08_nylon_1", log.at(rohacellIndex), 0, 0, checkOverlap);

  // nylon_2 in rohacell
  box.push_back(new G4Box("nylon_2_box", 100.0 * CLHEP::cm, 75.0 * CLHEP::cm, 0.025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nylon, "nylon_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -2.975 * CLHEP::cm), log.back(), "FI08_nylon_2", log.at(rohacellIndex), 0, 0, checkOverlap);

  // alu_1 in mother
  unsigned int alu_1Index = log.size();
  box.push_back(new G4Box("alu_1_box", 132.0 * CLHEP::cm, 23.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_1_log"));
  new G4PVPlacement(0, G4ThreeVector(-20.0 * CLHEP::cm, -98.0 * CLHEP::cm, 2.6 * CLHEP::cm), log.back(), "FI08_alu_1", log.at(motherIndex), 0, 0, checkOverlap);

  // air_1 in alu_1
  box.push_back(new G4Box("air_1_box", 100.0 * CLHEP::cm, 17.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_1_log"));
  new G4PVPlacement(0, G4ThreeVector(20.0 * CLHEP::cm, 0, 0), log.back(), "FI08_air_1", log.at(alu_1Index), 0, 0, checkOverlap);

  // air_2 in alu_1
  box.push_back(new G4Box("air_2_box", 17.0 * CLHEP::cm, 17.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_2_log"));
  new G4PVPlacement(0, G4ThreeVector(-103.0 * CLHEP::cm, 0, 0), log.back(), "FI08_air_2", log.at(alu_1Index), 0, 0, checkOverlap);

  // alu_2 in mother
  unsigned int alu_2Index = log.size();
  box.push_back(new G4Box("alu_2_box", 26.0 * CLHEP::cm, 78.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_2_log"));
  new G4PVPlacement(0, G4ThreeVector(-126.0 * CLHEP::cm, 3.0 * CLHEP::cm, 2.6 * CLHEP::cm), log.back(), "FI08_alu_2", log.at(motherIndex), 0, 0, checkOverlap);

  // air_3 in alu_2
  box.push_back(new G4Box("air_3_box", 17.0 * CLHEP::cm, 75.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_3_log"));
  new G4PVPlacement(0, G4ThreeVector(3.0 * CLHEP::cm, -3.0 * CLHEP::cm, 0), log.back(), "FI08_air_3", log.at(alu_2Index), 0, 0, checkOverlap);

  // alu_3 in mother
  box.push_back(new G4Box("alu_3_box", 100.0 * CLHEP::cm, 3.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 78.0 * CLHEP::cm, 2.6 * CLHEP::cm), log.back(), "FI08_alu_3", log.at(motherIndex), 0, 0, checkOverlap);

  // alu_4 in mother
  box.push_back(new G4Box("alu_4_box", 6.0 * CLHEP::cm, 78.0 * CLHEP::cm, 0.40 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_4_log"));
  new G4PVPlacement(0, G4ThreeVector(106.0 * CLHEP::cm, 3.0 * CLHEP::cm, 2.6 * CLHEP::cm), log.back(), "FI08_alu_4", log.at(motherIndex), 0, 0, checkOverlap);

  // copper in mother
  box.push_back(new G4Box("copper_box", 3.0 * CLHEP::cm, 3.0 * CLHEP::cm, 2.6 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "copper_log"));
  new G4PVPlacement(0, G4ThreeVector(109.0 * CLHEP::cm, -96.0 * CLHEP::cm, -0.4 * CLHEP::cm), log.back(), "FI08_copper_1", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(109.0 * CLHEP::cm, 56.0 * CLHEP::cm, -0.4 * CLHEP::cm), log.back(), "FI08_copper_2", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-149.0 * CLHEP::cm, -96.0 * CLHEP::cm, -0.4 * CLHEP::cm), log.back(), "FI08_copper_3", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(-149.0 * CLHEP::cm, 56.0 * CLHEP::cm, -0.4 * CLHEP::cm), log.back(), "FI08_copper_4", log.at(motherIndex), 0, 0, checkOverlap);

  //08
  //  GSET26VOL1PAR   425  7   0   -0.800  0.     0.   'BOX '  3    0.294  6.17  6.17    Scint bc408
  //  GSET26VOL2PAR   425  7   3    0.800  0.     0.   'BOX '  3    0.294  6.17  6.17    Scint
  //  GSET26VOL3PAR    24  7   0   -0.8    0.   -40.17 'BOX '  3    0.294  6.2  34.0     Plastic bc408
  //  GSET26VOL4PAR    24  7   0    0.8  -52.17   0.   'BOX '  3    0.294 46.    6.2     Plastic
  //  GSET26VOL5PAR    29  7   0    0.456  0.0    0.   'BOX '  3    0.05   6.2   6.2     Nylon nylon
  //  GSET26VOL6PAR    29  7   0   -2.975  0.0    0.   'BOX '  3    0.025 100.0 75.0     Nylon
  //  GSET26VOL7PAR    38  8   0    0.0    0.0    0.   'BOX '  3    3.00  100.0 75.0     Rcell rohacell_30
  //  GSET26VOL8PAR     2  0   0    0.0    0.0    0.   'BOX '  3    3.00  152.0 121.0    Airnmf air
  //  GSET26VOL9PAR    35  8   0    2.6  -20.0 -98.0   'BOX '  3    0.40  132.0  23.0    Al /alu
  //  GSET26VOLAPAR     2  9   0    0.0   20.0   0.0   'BOX '  3    0.40  100.0  17.0    Airnmf
  //  GSET26VOLBPAR     2  9   0    0.0 -103.0   0.0   'BOX '  3    0.40   17.0  17.0    Airnmf
  //  GSET26VOLCPAR    35  8   0    2.6 -126.0   3.0   'BOX '  3    0.40   26.0  78.0    Al
  //  GSET26VOLDPAR     2 12   0    0.0    3.0  -3.0   'BOX '  3    0.40   17.0  75.0    Airnmf
  //  GSET26VOLEPAR    35  8   0    2.6    0.0  78.0   'BOX '  3    0.40  100.0   3.0    Al
  //  GSET26VOLFPAR    35  8   0    2.6  106.0   3.0   'BOX '  3    0.40    6.0  78.0    Al
  //  GSET26VOLGPAR    32  8   0   -0.4  109.0  -96.0  'BOX '  3    2.6     3.0   3.0    Cu /copper
  //  GSET26VOLHPAR    32  8   0   -0.4  109.0   56.0  'BOX '  3    2.6     3.0   3.0    Cu
  //  GSET26VOLIPAR    32  8   0   -0.4 -149.0  -96.0  'BOX '  3    2.6     3.0   3.0    Cu
  //  GSET26VOLJPAR    32  8   0   -0.4 -149.0   56.0  'BOX '  3    2.6     3.0   3.0    Cu

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->orange);
}

void T4SciFi::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < fiPlane.size(); i++)
    fiPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4FIPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  if (det->name.size() < 4) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4FIPlane::getWireDetDat: Detector TBname: " + det->name
            + " is to small. Should have at least 4 chars.");
    return;
  }

  setGeneralWireInfo(wire);
  // special cases for wire 1
  if (wire.tbName[2] == '5') // FI55
    wire.rotMatrix = TGEANT::ROT_45pDEG;
  if (wire.tbName[2] == '1') // FI15
    wire.wireDist = -2.2000 * CLHEP::cm;
  wireDet.push_back(wire);

  // wire 2
  wire.id = firstDetectorId + 1;
  wire.tbName = namePlane2;
  wire.unit = 2;
  wire.zCen = detDatPos.getZ() + plane2Dist;
  wire.rotMatrix = TGEANT::ROT_90pDEG;
  if (wire.tbName[2] == '5') // FI55
    wire.rotMatrix = TGEANT::ROT_45nDEG;

  wireDet.push_back(wire);

  // wire 3
  if (usePlane3) {
    wire.id = firstDetectorId + 2;
    wire.tbName = namePlane3;
    wire.unit = 3;
    wire.zCen = detDatPos.getZ() + plane3Dist;
    wire.rotMatrix = TGEANT::ROT_45pDEG;

    if (wire.tbName[3] == '6') {
      wire.rotMatrix = TGEANT::ROT_45nDEG;
      wire.xSize = wire.ySize = dimSensV * 2.;
      wire.nWires = nWiresV;
      wire.wireDist = -1.0 * wire.pitch * (wire.nWires - 1) / 2;
    }

    wireDet.push_back(wire);
  }
}

void T4FIPlane::setGeneralWireInfo(T4SWireDetector& wire)
{
  wire.id = firstDetectorId;
  wire.tbName = det->name;
  wire.det = det->name.substr(0, 4);
  wire.unit = 1;
  wire.type = 22;
  wire.xSize = dimSens[0] * 2.;
  wire.ySize = dimSens[1] * 2.;
  wire.zSize = dimSens[2] * 2.;
  wire.xCen = detDatPos.getX();
  wire.yCen = detDatPos.getY();
  wire.zCen = detDatPos.getZ();
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.angle = 0;
  wire.nWires = nWires;
  wire.pitch = pitch;
  wire.wireDist = -1.0 * wire.pitch * (wire.nWires - 1) / 2;
}

void T4FIPlane35::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  if (det->name.size() < 4) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4FIPlane35::getWireDetDat: Detector TBname: " + det->name
            + " is to small. Should have at least 4 chars.");
    return;
  }

  // wire 1 (X)
  setGeneralWireInfo(wire);
  wireDet.push_back(wire);

  // wire 2 (V)
  wire.id = firstDetectorId + 1;
  wire.tbName = namePlane2;
  wire.unit = 2;
  wire.xSize = wire.ySize = dimSensV * 2.;
  wire.xCen = detDatPos.getX() + 0.00944 * CLHEP::cm;
  wire.yCen = detDatPos.getY() + 0.00072 * CLHEP::cm;
  wire.zCen = detDatPos.getZ() + plane2Dist;
  wire.nWires = nWiresV;
  wire.wireDist = -1.0 * wire.pitch * (wire.nWires - 1) / 2;
  wire.rotMatrix = TGEANT::ROT_45pDEG;
  wireDet.push_back(wire);

  // wire 3 (U)
  wire.id = firstDetectorId + 2;
  wire.tbName = namePlane3;
  wire.unit = 3;
  wire.xCen = detDatPos.getX() - 0.13345 * CLHEP::cm;
  wire.yCen = detDatPos.getY() - 0.15468 * CLHEP::cm;
  wire.zCen = detDatPos.getZ() + plane3Dist;
  wire.rotMatrix = TGEANT::ROT_45nDEG;
  wireDet.push_back(wire);

  // wire 4 (X2)
  wire.id = firstDetectorId + 3;
  wire.tbName = namePlane4;
  wire.unit = 4;
  wire.xSize = 2. * dimSensX2[0];
  wire.ySize = 2. * dimSensX2[1];
  wire.xCen = detDatPos.getX() + 0.09139 * CLHEP::cm;
  wire.yCen = detDatPos.getY() + 6.51958 * CLHEP::cm;
  wire.zCen = detDatPos.getZ() + plane4Dist;
  wire.nWires = nWires2;
  wire.wireDist = -1.0 * wire.pitch * (wire.nWires - 1) / 2;
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wireDet.push_back(wire);

  // wire 5 (V2)
  wire.id = firstDetectorId + 4;
  wire.tbName = namePlane5;
  wire.unit = 5;
  wire.xSize = 2. * dimSensV2[0];
  wire.ySize = 2. * dimSensV2[1];
  wire.xCen = coordDDatV2[0];
  wire.yCen = coordDDatV2[1];
  wire.zCen = detDatPos.getZ() + plane5Dist;
  //cout << "V2, x: " << wire.xCen / 10 << " , y: " << wire.yCen / 10 << " , z: " << wire.zCen / 10 << endl;
  wire.rotMatrix = TGEANT::ROT_45nDEG;
  wireDet.push_back(wire);

  // wire 6 (U2)
  wire.id = firstDetectorId + 5;
  wire.tbName = namePlane6;
  wire.unit = 6;
  wire.xCen = coordDDatU2[0];
  wire.yCen = coordDDatU2[1];
  wire.zCen = detDatPos.getZ() + plane6Dist;
  //cout << "U2, x: " << wire.xCen / 10 << " , y: " << wire.yCen / 10 << " , z: " << wire.zCen / 10 << endl;
  wire.rotMatrix = TGEANT::ROT_45pDEG;
  wireDet.push_back(wire);


  T4SDeadZone dead;
  // dead wire 1
  dead.id = firstDetectorId;
  dead.tbName = det->name;
  dead.det = det->name.substr(0, 4);
  dead.unit = 1;
  dead.sh = 1;
  dead.xSize = dimDeadX[0] * 2.;
  dead.ySize = dimDeadX[1] * 2.;
  dead.zSize = dimSens[2] * 2.;
  dead.xCen = detDatPos.getX() + posDeadX[0];
  dead.yCen = detDatPos.getY() + posDeadX[1];
  dead.zCen = detDatPos.getZ();
  dead.rotMatrix = TGEANT::ROT_0DEG;
  deadZone.push_back(dead);

  // dead wire 2
  dead.id = firstDetectorId + 1;
  dead.tbName = namePlane2;
  dead.unit = 2;
  dead.xSize = dimDeadV[0] * 2.;
  dead.ySize = dimDeadV[1] * 2.;
  dead.xCen = coordDeadV2[0];
  dead.yCen = coordDeadV2[1];
  dead.zCen = detDatPos.getZ() + plane2Dist;
  dead.rotMatrix = TGEANT::ROT_45nDEG;
  deadZone.push_back(dead);

  // dead wire 3
  dead.id = firstDetectorId + 2;
  dead.tbName = namePlane3;
  dead.unit = 3;
  coordDeadU2[0] = -2.1200 * CLHEP::cm;
  dead.xCen = coordDeadU2[0];
  dead.yCen = coordDeadU2[1];
  dead.zCen = detDatPos.getZ() + plane3Dist;
  dead.rotMatrix = TGEANT::ROT_45pDEG;
  deadZone.push_back(dead);
}

T4SciFi::~T4SciFi(void)
{
  for (unsigned int i = 0; i < fiPlane.size();i++)
    delete fiPlane.at(i);
  fiPlane.clear();
}

T4FIPlane::~T4FIPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}
