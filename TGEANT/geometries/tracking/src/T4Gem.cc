#include "T4Gem.hh"

void T4Gem::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 601;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getGem()->size(); i++) {
    const T4SDetector* gem = &settingsFile->getStructManager()->getGem()->at(i);

    if (gem->name[1] == 'M') {
      gemPlane.push_back(new T4GemPlane(gem, firstDetectorId, false));
      firstDetectorId += 3;
    } else if (gem->name[1] == 'P') {
      gemPlane.push_back(new T4GemPlane(gem, firstDetectorId, true));
      firstDetectorId += 4;
    }
    gemPlane.back()->construct(world_log);
  }
}

T4GemPlane::T4GemPlane(const T4SDetector* _det, int _firstDetectorId, bool _isPixel)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  isPixel = _isPixel;

  // If we read X plane, Y is build, too. X is the more DOWNstream plane.
  // If we read U plane, V is build, too. U is the more UPstream plane.
  // This is switched because of the zMult factor.
  namePlane2 = det->name;
  namePlanePixel = det->name;
  namePlanePixel[4] = 'P';

  rot = NULL;
  zMult = 1.0;

  if (det->name[4] == 'X') {
    namePlane2[4] = 'Y';
    namePlanePixel[5] = '2';
  } else if (det->name[4] == 'U') {
    namePlane2[4] = 'V';
    zMult = -1.0;
    rot = new CLHEP::HepRotation;
    rot->rotateZ(-45.0 * CLHEP::deg);
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "T4GemPlane:: Unknown GEM TBname: " + det->name
            + ". Treat it as X plane.");
  }

  stringstream myStream;
  myStream << det->name[2] << det->name[3];
  unit = strToInt(myStream.str());

  xySize = 15.8 * CLHEP::cm;
  zSize = 0.0025 * CLHEP::cm;
  if (unit == 11)
    zSize = 0.0075 * CLHEP::cm;
  deadZoneRadius = 2.5 * CLHEP::cm;



  setPosition(det->position);
}

void T4GemPlane::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 30.0 * CLHEP::cm, 30.0 * CLHEP::cm, 0.795 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(rot, positionVector + G4ThreeVector(0, 0, -0.3185 * CLHEP::cm * zMult), log.back(), "GEM_air", world_log, 0, 0, checkOverlap);

  // G10 in mother
  unsigned int g10Index = log.size();
  box.push_back(new G4Box("g10_box", 16.5 * CLHEP::cm, 16.5 * CLHEP::cm, 0.459 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.012 * CLHEP::cm * zMult), log.back(), "GEM_g10", log.at(motherIndex), 0, 0, checkOverlap);

  // ArCo2 in G10
  unsigned int arco2Index = log.size();
  box.push_back(new G4Box("arco2_box", 15.8 * CLHEP::cm, 15.8 * CLHEP::cm, 0.459 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->ArCo2, "arco2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_arco2", log.at(g10Index), 0, 0, checkOverlap);

  // Cu in ArCo2
  box.push_back(new G4Box("cu_box", xySize, xySize, 0.0015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_cu", log.at(arco2Index), 0, 0, checkOverlap);

  // Kapton in ArCo2
  box.push_back(new G4Box("kapton_box", xySize, xySize, 0.0085 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->kapton, "kapton_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.01 * CLHEP::cm * zMult), log.back(), "GEM_kapton", log.at(arco2Index), 0, 0, checkOverlap);

  if (!isPixel) {
    G4double pos1, pos2;
    if (unit != 11) {
      pos1 = 0.309 * CLHEP::cm;
      pos2 = 0.304 * CLHEP::cm;
    } else {
      pos1 = 0.314 * CLHEP::cm;
      pos2 = 0.299 * CLHEP::cm;
    }

    // sens_n1 in ArCo2 (more upstream)
    unsigned int sens_1Index = log.size();
    box.push_back(new G4Box("sens_n1_box", xySize, xySize, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->gemMaterial, "sens_n1_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, pos1 * zMult), log.back(), "GEM_sens_n1", log.at(arco2Index), 0, 0, checkOverlap);
    log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

    // sens_n2 in ArCo2 (more downstream)
    unsigned int sens_2Index = log.size();
    box.push_back(new G4Box("sens_n2_box", xySize, xySize, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->gemMaterial, "sens_n2_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, pos2 * zMult), log.back(), "GEM_sens_n2", log.at(arco2Index), 0, 0, checkOverlap);
    log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId+1));

    if (unit != 11) {
      // Kapton_n2 and Kapton_n3 in sens_n1 and sens_n2
      tubs.push_back(new G4Tubs("kapton_n3_tubs", 0, deadZoneRadius,  zSize, 0., 360.*CLHEP::deg));
      log.push_back(new G4LogicalVolume(tubs.back(), materials->kapton, "kapton_n3_log"));
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_kapton_n2", log.at(sens_1Index), 0, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_kapton_n3", log.at(sens_2Index), 0, 0, checkOverlap);
    }
  } else {
    // sens_n1 in ArCo2 (more upstream)
    unsigned int sens_1Index = log.size();
    box.push_back(new G4Box("sens_n1_box", 5.0 * CLHEP::cm, 5.0 * CLHEP::cm, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->gemMaterial, "sens_n1_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.309 * CLHEP::cm * zMult), log.back(), "PGEM_sens_n1", log.at(arco2Index), 0, 0, checkOverlap);
    log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

    // sens_n2 in ArCo2 (more downstream)
    unsigned int sens_2Index = log.size();
    box.push_back(new G4Box("sens_n2_box", 5.0 * CLHEP::cm, 5.0 * CLHEP::cm, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->gemMaterial, "sens_n2_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.304 * CLHEP::cm * zMult), log.back(), "PGEM_sens_n2", log.at(arco2Index), 0, 0, checkOverlap);
    log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId+1));

    // ArCo2 in sens_n1 and sens_n1
    box.push_back(new G4Box("arco2_n2_box", 1.6 * CLHEP::cm, 1.6 * CLHEP::cm, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->ArCo2, "arco2_n2_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "PGEM_arco2_n2", log.at(sens_1Index), 0, 0, checkOverlap);
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "PGEM_arco2_n2", log.at(sens_2Index), 0, 0, checkOverlap);

    // sens_n3 in ArCo2 (pixel)
    box.push_back(new G4Box("sens_n3_box", 1.6 * CLHEP::cm, 1.6 * CLHEP::cm, zSize));
    log.push_back(new G4LogicalVolume(box.back(), materials->gemMaterial, "sens_n3_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.314 * CLHEP::cm * zMult), log.back(), "PGEM_sens_n3", log.at(arco2Index), 0, 0, checkOverlap);
    log.back()->SetSensitiveDetector(
        new T4SensitiveDetector(namePlanePixel, 0, TGEANT::HIT, firstDetectorId+2, TGEANT::PMTDUMMY, firstDetectorId+3));
  }

  // G10_n2 in mother
  unsigned int g10_2Index = log.size();
  box.push_back(new G4Box("g10_n2_box", 16.5 * CLHEP::cm, 16.5 * CLHEP::cm, 0.012 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.783 * CLHEP::cm * zMult), log.back(), "GEM_g10_n2", log.at(motherIndex), 0, 0, checkOverlap);

  // Air in G10_n2
  tubs.push_back(new G4Tubs("air_tubs", 0, 2.5 * CLHEP::cm,  0.012 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_air", log.at(g10_2Index), 0, 0, checkOverlap);

  // Nomex in mother
  unsigned int nomexIndex = log.size();
  box.push_back(new G4Box("nomex_box", 16.5 * CLHEP::cm, 16.5 * CLHEP::cm, 0.15 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nomex, "nomex_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.621 * CLHEP::cm * zMult), log.back(), "GEM_nomex", log.at(motherIndex), 0, 0, checkOverlap);

  // Air_n2 in Nomex
  tubs.push_back(new G4Tubs("air_n2_tubs", 0, 2.5 * CLHEP::cm,  0.15 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_n2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "GEM_air_n2", log.at(nomexIndex), 0, 0, checkOverlap);

  // G10_n3 in mother
  unsigned int g10_3Index = log.size();
  box.push_back(new G4Box("g10_n3_box", 25.0 * CLHEP::cm, 25.0 * CLHEP::cm, 0.012 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n3_log"));
  new G4PVPlacement(0, G4ThreeVector(5. * CLHEP::cm * zMult, -1.5 * CLHEP::cm, -0.783 * CLHEP::cm * zMult), log.back(), "GEM_g10_n3", log.at(motherIndex), 0, 0, checkOverlap);

  // Air_n3 in G10_n3
  tubs.push_back(new G4Tubs("air_n3_tubs", 0, 2.5 * CLHEP::cm,  0.012 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_n3_log"));
  new G4PVPlacement(0, G4ThreeVector(-5. * CLHEP::cm * zMult, 1.5 * CLHEP::cm, 0), log.back(), "GEM_air_n3", log.at(g10_3Index), 0, 0, checkOverlap);

  // Nomex_n2 in mother
  unsigned int nomex_n2Index = log.size();
  box.push_back(new G4Box("nomex_n2_box", 25.0 * CLHEP::cm, 25.0 * CLHEP::cm, 0.15 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->nomex, "nomex_n2_log"));
  new G4PVPlacement(0, G4ThreeVector(5. * CLHEP::cm * zMult, -1.5 * CLHEP::cm, -0.621 * CLHEP::cm * zMult), log.back(), "GEM_nomex_n2", log.at(motherIndex), 0, 0, checkOverlap);

  // Air_n4 in Nomex_n2
  tubs.push_back(new G4Tubs("air_n3_tubs", 0, 2.5 * CLHEP::cm,  0.15 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "air_n3_log"));
  new G4PVPlacement(0, G4ThreeVector(-5. * CLHEP::cm * zMult, 1.5 * CLHEP::cm, 0), log.back(), "GEM_air_n3", log.at(nomex_n2Index), 0, 0, checkOverlap);

  // G10_n4 in mother
  box.push_back(new G4Box("g10_n4_box", 22.5 * CLHEP::cm, 22.5 * CLHEP::cm, 0.012 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n4_log"));
  new G4PVPlacement(0, G4ThreeVector(2.5 * CLHEP::cm * zMult, 1.0 * CLHEP::cm, -0.459 * CLHEP::cm * zMult), log.back(), "GEM_g10_n4", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_n5 in mother
  box.push_back(new G4Box("g10_n5_box", 15.5 * CLHEP::cm, 4.4 * CLHEP::cm, 0.03 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n5_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -21.6 * CLHEP::cm, -0.417 * CLHEP::cm * zMult), log.back(), "GEM_g10_n5", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_n6 in mother
  box.push_back(new G4Box("g10_n6_box", 4.4 * CLHEP::cm, 15.5 * CLHEP::cm, 0.03 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n6_log"));
  new G4PVPlacement(0, G4ThreeVector(21.6 * CLHEP::cm * zMult, 0, -0.417 * CLHEP::cm * zMult), log.back(), "GEM_g10_n6", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_n7 in mother
  box.push_back(new G4Box("g10_n7_box", 1.75 * CLHEP::cm, 16.5 * CLHEP::cm, 0.0063 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n7_log"));
  new G4PVPlacement(0, G4ThreeVector(-18.25 * CLHEP::cm * zMult, 0, -0.4407 * CLHEP::cm * zMult), log.back(), "GEM_g10_n7", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_n8 in mother
  box.push_back(new G4Box("g10_n8_box", 16.5 * CLHEP::cm, 3.5 * CLHEP::cm, 0.0063 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_n8_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 20.0 * CLHEP::cm, -0.4407 * zMult), log.back(), "GEM_g10_n8", log.at(motherIndex), 0, 0, checkOverlap);

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->green);
}

void T4Gem::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < gemPlane.size();i++)
    gemPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4GemPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  T4SDeadZone dead;
  // If we read X plane, Y is build, too. X is the more DOWNstream plane.
  // If we read U plane, V is build, too. U is the more UPstream plane.

  wire.unit = dead.unit = unit;
  if (unit == 11)
    wire.unit = dead.unit = 1;
  if (isPixel)
    wire.unit = dead.unit = unit;

  wire.type = 26;
  if (isPixel)
    wire.type = 28;

  wire.xSize = wire.ySize = xySize * 2.;
  if (isPixel)
    wire.xSize = wire.ySize = 10.0 * CLHEP::cm;

  wire.zSize = zSize * 2.;
  wire.xCen = dead.xCen = positionVector.getX();
  wire.yCen = dead.yCen = positionVector.getY();
  wire.nWires = 768;
  if (isPixel)
    wire.nWires = 256;

  wire.pitch = 0.04 * CLHEP::cm;
  if (isPixel)
    wire.pitch = 0.0394 * CLHEP::cm;

  wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
  dead.sh = 5;
  dead.xSize = deadZoneRadius * 2.;
  dead.ySize = zSize * 2.;
  dead.zSize = 0;
  if (isPixel) {
    dead.sh = 1;
    dead.xSize = dead.ySize = 3.2 * CLHEP::cm;
    dead.zSize = zSize * 2.;
  }

  if (det->name[4] == 'X') {
    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = det->name;
    wire.det = dead.det = "MG11";
    if (unit == 11)
      wire.det = dead.det = "MG31";
    if (isPixel)
      wire.det = dead.det = "PG32";

    wire.zCen = dead.zCen = positionVector.getZ();
    wire.rotMatrix = TGEANT::ROT_0DEG;
    wire.angle = 0;
    dead.rotMatrix = 12;
    if (isPixel)
      dead.rotMatrix = 1;

    wireDet.push_back(wire);
    if (unit != 11)
      deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId + 1;// and Y more up
    wire.tbName = dead.tbName = namePlane2;
    wire.det = dead.det = "MG12";
    if (unit == 11)
      wire.det = dead.det = "MG32";
    if (isPixel)
      wire.det = dead.det = "PG31";

    wire.zCen = dead.zCen = positionVector.getZ() - wire.zSize;
    wire.angle = 90;
    wireDet.push_back(wire);
    if (unit != 11)
      deadZone.push_back(dead);

  } else if (det->name[4] == 'U') {

    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = det->name;
    wire.det = dead.det = "MG21";
    if (unit == 11)
      wire.det = dead.det = "MG41";
    if (isPixel)
      wire.det = dead.det = "PG41";

    wire.zCen = dead.zCen = positionVector.getZ();
    wire.rotMatrix = TGEANT::ROT_45pDEG;
    wire.angle = 0;
    dead.rotMatrix = 13;
    if (isPixel)
      dead.rotMatrix = TGEANT::ROT_45pDEG;

    wireDet.push_back(wire);
    if (unit != 11)
      deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId + 1;// and V more down
    wire.tbName = dead.tbName = namePlane2;
    wire.det = dead.det = "MG22";
    if (unit == 11)
      wire.det = dead.det = "MG42";
    if (isPixel)
      wire.det = dead.det = "PG42";

    wire.zCen = dead.zCen = positionVector.getZ() + wire.zSize;
    wire.angle = -90;
    wireDet.push_back(wire);
    if (unit != 11)
      deadZone.push_back(dead);
  }

  if (isPixel) {
    wire.tbName = namePlanePixel;
    wire.xSize = wire.ySize = 3.2 * CLHEP::cm;
    wire.zSize = 2.0* zSize;

    wire.nWires = 32;
    wire.pitch = 0.1 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;

    if (det->name[4] == 'X') {
      wire.zCen = positionVector.getZ() + wire.zSize;

      wire.det = "PG33";
      wire.angle = 0;
      wire.id = firstDetectorId + 2;
      wireDet.push_back(wire);

      wire.det = "PG34";
      wire.angle = 90;
      wire.id = firstDetectorId + 3;
      wireDet.push_back(wire);

    } else if (det->name[4] == 'U') {
      wire.zCen = positionVector.getZ() - wire.zSize;

      wire.det = "PG43";
      wire.angle = 0;
      wire.id = firstDetectorId + 2;
      wireDet.push_back(wire);

      wire.det = "PG44";
      wire.angle = 90;
      wire.id = firstDetectorId + 3;
      wireDet.push_back(wire);
    }
  }
}

T4Gem::~T4Gem(void)
{
  for (unsigned int i = 0; i < gemPlane.size();i++)
    delete gemPlane.at(i);
  gemPlane.clear();
}

T4GemPlane::~T4GemPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  if (rot != NULL)
    delete rot;
}
