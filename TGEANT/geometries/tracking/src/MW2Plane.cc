#include "MW2Plane.hh"

int MW2Plane::detectorIdCounter = 230;

MW2Plane::MW2Plane(T4DetIdent _detIdent, MW2Plane::MW2Type _mw2Type)
{
  rotationTube = new CLHEP::HepRotation;

  mw2Type = _mw2Type;
  detIdent = _detIdent;
  for (int i = 0; i < 4; i++)
    detectorId[i] = detectorIdCounter++;

  if (mw2Type == MW2Plane::Y) {
    detTypeName[0] = "ur";
    detTypeName[1] = "dr";
    detTypeName[2] = "ul";
    detTypeName[3] = "dl";
  } else {
    detTypeName[0] = "ub";
    detTypeName[1] = "db";
    detTypeName[2] = "uc";
    detTypeName[3] = "dc";
  }

  outerRadius = 30.0 / 2. * CLHEP::mm;
  innerRadius = 29.0 / 2. * CLHEP::mm;
  wireRadius = 0.05 / 2. * CLHEP::mm;

  distanceXY = 16.75 * CLHEP::mm;
  distanceZ = 29.0 * CLHEP::mm;

  if (mw2Type == MW2Plane::X) {

    lengthTubeLong = 218.0 / 2. * CLHEP::cm;
    lengthTubeShort_A = 69.0 / 2. * CLHEP::cm; // bottom
    lengthTubeShort_B = 69.0 / 2. * CLHEP::cm; // top
    lengthHole = 80.0 * CLHEP::cm;

    numWires_1 = 4 * 16; // saleve
    numWires_2 = 3 * 16; // jura
    numWires_hole = 28;

    rotationTube->rotateX(90.0 * CLHEP::deg);
    // shift by 8 channels
    shiftToHole = G4ThreeVector(8 * (distanceXY * 2), 0, 0);

  } else if (mw2Type == MW2Plane::Y) {

    lengthTubeLong = 418.0 / 2. * CLHEP::cm;
    lengthTubeShort_A = 160.0 / 2. * CLHEP::cm; // jura
    lengthTubeShort_B = 218.0 / 2. * CLHEP::cm; // saleve
    lengthHole = 102.0 * CLHEP::cm;

    numWires_1 = 18; // bottom
    numWires_2 = 18; // top
    numWires_hole = 24;

    rotationTube->rotateY(90.0 * CLHEP::deg);
    // shift by 60 cm
    shiftToHole = G4ThreeVector(lengthTubeLong - 2. * lengthTubeShort_A - lengthHole / 2., 0, 0);

  } else if (mw2Type == MW2Plane::V) {

    lengthTubeLong = 218.0 / 2. * CLHEP::cm;
    lengthTubeShort_A = 69.0 / 2. * CLHEP::cm; // bottom
    lengthTubeShort_B = 69.0 / 2. * CLHEP::cm; // top
    lengthHole = 80.0 * CLHEP::cm;

    numWires_1 = 4 * 16; // saleve
    numWires_2 = 3 * 16; // jura
    numWires_hole = 28;

    rotationTube->rotateX(90.0 * CLHEP::deg);
    rotationTube->rotateY(-15.0 * CLHEP::deg);
    // shift by 8 channels
    shiftToHole = G4ThreeVector(8. * (distanceXY * 2.), 0, 0);
  }

  outerTubeLong_tubs = NULL;
  outerTubeShort_A_tubs = NULL;
  outerTubeShort_B_tubs = NULL;

  innerTubeLong_tubs = NULL;
  innerTubeShort_A_tubs = NULL;
  innerTubeShort_B_tubs = NULL;

  wireLong_tubs = NULL;
  wireShort_A_tubs = NULL;
  wireShort_B_tubs = NULL;

  wireLong_log = NULL;
  wireShort_A_log = NULL;
  wireShort_B_log = NULL;
}

MW2Plane::~MW2Plane(void)
{
  if (outerTubeLong_tubs != NULL)
    delete outerTubeLong_tubs;
  if (outerTubeShort_A_tubs != NULL)
    delete outerTubeShort_A_tubs;
  if (outerTubeShort_B_tubs != NULL)
    delete outerTubeShort_B_tubs;

  if (innerTubeLong_tubs != NULL)
    delete innerTubeLong_tubs;
  if (innerTubeShort_A_tubs != NULL)
    delete innerTubeShort_A_tubs;
  if (innerTubeShort_B_tubs != NULL)
    delete innerTubeShort_B_tubs;

  if (wireLong_tubs != NULL)
    delete wireLong_tubs;
  if (wireShort_A_tubs != NULL)
    delete wireShort_A_tubs;
  if (wireShort_B_tubs != NULL)
    delete wireShort_B_tubs;

  if (wireLong_log != NULL)
    delete wireLong_log;
  if (wireShort_A_log != NULL)
    delete wireShort_A_log;
  if (wireShort_B_log != NULL)
    delete wireShort_B_log;

  for (unsigned int i = 0; i < outerTubeLong_1_log.size(); i++)
    delete outerTubeLong_1_log.at(i);
  outerTubeLong_1_log.clear();
  for (unsigned int i = 0; i < outerTubeLong_2_log.size(); i++)
    delete outerTubeLong_2_log.at(i);
  outerTubeLong_2_log.clear();
  for (unsigned int i = 0; i < outerTubeShort_A_log.size(); i++)
    delete outerTubeShort_A_log.at(i);
  outerTubeShort_A_log.clear();
  for (unsigned int i = 0; i < outerTubeShort_B_log.size(); i++)
    delete outerTubeShort_B_log.at(i);
  outerTubeShort_B_log.clear();

  for (unsigned int i = 0; i < innerTube_log.size(); i++)
    delete innerTube_log.at(i);
  innerTube_log.clear();

  delete rotationTube;
}

void MW2Plane::construct(G4LogicalVolume* world_log)
{
  if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries) {
    outerTubeLong_tubs = new G4Tubs("outerTubeLong_tubs", 0, outerRadius,
        lengthTubeLong, 0, 360.0 * CLHEP::deg);
    outerTubeShort_A_tubs = new G4Tubs("outerTubeShort_A_tubs", 0, outerRadius,
        lengthTubeShort_A, 0, 360.0 * CLHEP::deg);
    outerTubeShort_B_tubs = new G4Tubs("outerTubeShort_B_tubs", 0, outerRadius,
        lengthTubeShort_B, 0, 360.0 * CLHEP::deg);
  } else {
    outerTubeLong_tubs = new G4Box("outerTubeLong_tubs", outerRadius*0.99, outerRadius*0.99,
        lengthTubeLong);
    outerTubeShort_A_tubs = new G4Box("outerTubeShort_A_tubs", outerRadius*0.99, outerRadius*0.99,
        lengthTubeShort_A);
    outerTubeShort_B_tubs = new G4Box("outerTubeShort_B_tubs", outerRadius*0.99, outerRadius*0.99,
        lengthTubeShort_B);
  }

  G4double thickness = outerRadius - innerRadius;
  innerTubeLong_tubs = new G4Tubs("innerTubeLong_tubs", 0, innerRadius,
      lengthTubeLong - thickness, 0, 360.0 * CLHEP::deg);
  innerTubeShort_A_tubs = new G4Tubs("innerTubeShort_A_tubs", 0, innerRadius,
      lengthTubeShort_A - thickness, 0, 360.0 * CLHEP::deg);
  innerTubeShort_B_tubs = new G4Tubs("innerTubeShort_B_tubs", 0, innerRadius,
      lengthTubeShort_B - thickness, 0, 360.0 * CLHEP::deg);

  wireLong_tubs = new G4Tubs("wireLong_tubs", 0, wireRadius,
      lengthTubeLong - thickness, 0, 360.0 * CLHEP::deg);
  wireShort_A_tubs = new G4Tubs("wireShort_A_tubs", 0, wireRadius,
      lengthTubeShort_A - thickness, 0, 360.0 * CLHEP::deg);
  wireShort_B_tubs = new G4Tubs("wireShort_B_tubs", 0, wireRadius,
      lengthTubeShort_B - thickness, 0, 360.0 * CLHEP::deg);

  wireLong_log = new G4LogicalVolume(wireLong_tubs, materials->gold,
      "wireLong_log", 0, 0, 0, 0);
  wireShort_A_log = new G4LogicalVolume(wireShort_A_tubs, materials->gold,
      "wireShort_A_log", 0, 0, 0, 0);
  wireShort_B_log = new G4LogicalVolume(wireShort_B_tubs, materials->gold,
      "wireShort_B_log", 0, 0, 0, 0);

  std::vector<G4ThreeVector> tubePositionLong_1;
  tubePositionLong_1.resize(2. * numWires_1);
  std::vector<G4ThreeVector> tubePositionLong_2;
  tubePositionLong_2.resize(2. * numWires_2);
  std::vector<G4ThreeVector> tubePositionShort_A;
  tubePositionShort_A.resize(2. * numWires_hole);
  std::vector<G4ThreeVector> tubePositionShort_B;
  tubePositionShort_B.resize(2. * numWires_hole);

  G4double x, y, z;

  // hole region
  for (G4int i = 0; i < 2 * numWires_hole; i++) {
    if (i % 2 == 0)
      z = 0;
    else
      z = distanceZ;

    if (mw2Type == MW2Plane::X) {
      x = (numWires_hole - 1 - i) * distanceXY;
      tubePositionShort_A.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x, -lengthHole / 2. - lengthTubeShort_A, z);
      tubePositionShort_B.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x, lengthHole / 2. + lengthTubeShort_B, z);
    } else if (mw2Type == MW2Plane::Y) {
      y = (-numWires_hole + 1 + i) * distanceXY;
      tubePositionShort_A.at(i) = positionVector + shiftToHole
          + G4ThreeVector(lengthHole / 2. + lengthTubeShort_A, y, z);
      tubePositionShort_B.at(i) = positionVector + shiftToHole
          + G4ThreeVector(-lengthHole / 2. - lengthTubeShort_B, y, z);
    } else if (mw2Type == MW2Plane::V) {
      x = (numWires_hole - 1 - i) * distanceXY
          * cos(M_PI * -15. / 180.);
      y = (numWires_hole - 1 - i) * distanceXY
          * sin(M_PI * -15. / 180.);
      tubePositionShort_A.at(i) = positionVector + shiftToHole
          + G4ThreeVector(
              (lengthHole / 2. + lengthTubeShort_A)
                  * sin(M_PI * -15. / 180.) + x,
              (-lengthHole / 2. - lengthTubeShort_A)
                  * cos(M_PI * -15. / 180.) + y, z);
      tubePositionShort_B.at(i) = positionVector + shiftToHole
          + G4ThreeVector(
              (-lengthHole / 2. - lengthTubeShort_B)
                  * sin(M_PI * -15. / 180.) + x,
              (lengthHole / 2. + lengthTubeShort_B)
                  * cos(M_PI * -15. / 180.) + y, z);
    }

    outerTubeShort_A_log.push_back(
        new G4LogicalVolume(outerTubeShort_A_tubs, materials->stainlessSteel,
            "outerTubeShort_A_log", 0, 0, 0, 0));
    outerTubeShort_B_log.push_back(
        new G4LogicalVolume(outerTubeShort_B_tubs, materials->stainlessSteel,
            "outerTubeShort_B_log", 0, 0, 0, 0));
    outerTubeShort_A_log.back()->SetVisAttributes(colour->silver);
    outerTubeShort_B_log.back()->SetVisAttributes(colour->silver);

    innerTube_log.push_back(
        new G4LogicalVolume(innerTubeShort_A_tubs, materials->mw2Gas,
            "innerTubeShort_A_log", 0, 0, 0, 0));
    innerTube_2_log.push_back(
        new G4LogicalVolume(innerTubeShort_B_tubs, materials->mw2Gas,
            "innerTubeShort_B_log", 0, 0, 0, 0));

    new G4PVPlacement(rotationTube, tubePositionShort_A.at(i),
        outerTubeShort_A_log.back(), "outerTubeShort_A_phys", world_log, 0, 0,
        checkOverlap);
    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries) {
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerTube_log.back(),
          "innerTubeShort_A_phys", outerTubeShort_A_log.back(), 0, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), wireShort_A_log,
          "wireShort_A_phys", innerTube_log.back(), 0, 0, checkOverlap);
    }

    new G4PVPlacement(rotationTube, tubePositionShort_B.at(i),
        outerTubeShort_B_log.back(), "outerTubeShort_B_phys", world_log, 0, 0,
        checkOverlap);
    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries) {
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerTube_2_log.back(),
          "innerTubeShort_B_phys", outerTubeShort_B_log.back(), 0, 0,
          checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), wireShort_B_log,
          "wireShort_B_phys", innerTube_2_log.back(), 0, 0, checkOverlap);
    }
  }

  // X/V-Plane: saleve, Y-Plane: bottom
  for (G4int i = 0; i < 2 * numWires_1; i++) {
    if (i % 2 == 0)
      z = 0;
    else
      z = distanceZ;

    if (mw2Type == MW2Plane::X) {
      x = (-numWires_hole - 1 - i) * distanceXY;
      tubePositionLong_1.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x,
              -lengthHole / 2. - 2. * lengthTubeShort_A + lengthTubeLong, z);
    } else if (mw2Type == MW2Plane::Y) {
      y = (-numWires_hole - (numWires_1 - 0.5) * 2. + i) * distanceXY;
      tubePositionLong_1.at(i) = positionVector + shiftToHole
          + G4ThreeVector(
              lengthHole / 2. + 2. * lengthTubeShort_A - lengthTubeLong, y, z);
    } else if (mw2Type == MW2Plane::V) {
      G4double groupDistance = (15 - (i % 32)) * distanceXY;
      G4double groupXCentre = (-numWires_hole - 16 - 32 * (i / 32)) * distanceXY
          / cos(M_PI * -15 / 180);
      x = groupXCentre + groupDistance * cos(M_PI * -15. / 180.);
      tubePositionLong_1.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x,
              -lengthHole / 2. - 2. * lengthTubeShort_A + lengthTubeLong
                  + groupDistance * sin(M_PI * -15. / 180.), z);
    }

    outerTubeLong_1_log.push_back(
        new G4LogicalVolume(outerTubeLong_tubs, materials->stainlessSteel,
            "outerTubeLong_1_log", 0, 0, 0, 0));
    innerTube_log.push_back(
        new G4LogicalVolume(innerTubeLong_tubs, materials->mw2Gas,
            "innerTubeLong_1_log", 0, 0, 0, 0));
    outerTubeLong_1_log.back()->SetVisAttributes(colour->silver);

    new G4PVPlacement(rotationTube, tubePositionLong_1.at(i),
        outerTubeLong_1_log.back(), "outerTubeLong_1_phys", world_log, 0, 0,
        checkOverlap);
    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries) {
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerTube_log.back(),
          "innerTubeLong_1_phys", outerTubeLong_1_log.back(), 0, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), wireLong_log, "wireLong_phys",
          innerTube_log.back(), 0, 0, checkOverlap);
    }
  }

  // X/V-Plane: jura, Y-Plane: top
  for (G4int i = 0; i < 2 * numWires_2; i++) {
    if (i % 2 == 0)
      z = 0;
    else
      z = distanceZ;

    if (mw2Type == MW2Plane::X) {
      x = ((numWires_hole / 2 + numWires_2 - 0.5) * 2 - i) * distanceXY;
      tubePositionLong_2.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x,
              -lengthHole / 2. - 2. * lengthTubeShort_A + lengthTubeLong, z);
    } else if (mw2Type == MW2Plane::Y) {
      y = (numWires_hole + 1 + i) * distanceXY;
      tubePositionLong_2.at(i) = positionVector + shiftToHole
          + G4ThreeVector(
              lengthHole / 2. + 2. * lengthTubeShort_A - lengthTubeLong, y, z);
    } else if (mw2Type == MW2Plane::V) {
      G4double groupDistance = (15 - (i % 32)) * distanceXY;
      G4double groupXCentre = (numWires_hole + 16 + 32 * (2 - (i / 32)))
          * distanceXY / cos(M_PI * -15 / 180);
      x = groupXCentre + groupDistance * cos(M_PI * -15 / 180);
      tubePositionLong_2.at(i) = positionVector + shiftToHole
          + G4ThreeVector(x,
              -lengthHole / 2. - 2. * lengthTubeShort_A + lengthTubeLong
                  + groupDistance * sin(M_PI * -15 / 180), z);
    }

    outerTubeLong_2_log.push_back(
        new G4LogicalVolume(outerTubeLong_tubs, materials->stainlessSteel,
            "outerTubeLong_2_log", 0, 0, 0, 0));
    innerTube_log.push_back(
        new G4LogicalVolume(innerTubeLong_tubs, materials->mw2Gas,
            "innerTubeLong_2_log", 0, 0, 0, 0));
    outerTubeLong_2_log.back()->SetVisAttributes(colour->silver);

    new G4PVPlacement(rotationTube, tubePositionLong_2.at(i),
        outerTubeLong_2_log.back(), "outerTubeLong_2_phys", world_log, 0, 0,
        checkOverlap);

    if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries) {
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), innerTube_log.back(),
          "innerTubeLong_2_phys", outerTubeLong_2_log.back(), 0, 0, checkOverlap);
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), wireLong_log, "wireLong_phys",
          innerTube_log.back(), 0, 0, checkOverlap);
    }
  }

  // sensitive detector
  G4int k;
  for (unsigned int i = 0; i < innerTube_log.size(); i++) {
    k = (i % 2 == 0) ? 0 : 1;
    innerTube_log.at(i)->SetSensitiveDetector(
        new T4SensitiveDetector(detIdent.tbName + detTypeName[k], i / 2, TGEANT::HIT, detectorId[k]));
  }

  for (unsigned int i = 0; i < innerTube_2_log.size(); i++) {
    k = (i % 2 == 0) ? 2 : 3;
    innerTube_2_log.at(i)->SetSensitiveDetector(
        new T4SensitiveDetector(detIdent.tbName + detTypeName[k], i / 2, TGEANT::HIT, detectorId[k]));
  }
}

void MW2Plane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire[4];
  wire[0].nWires = wire[1].nWires = numWires_1 + numWires_2 + numWires_hole;
  wire[2].nWires = wire[3].nWires = numWires_hole;

  for (int i = 0; i < 4; i++) {
    wire[i].id = detectorId[i];
    wire[i].tbName = detIdent.tbName + detTypeName[i];
    wire[i].det = detIdent.detName + intToStr(i+1);
    wire[i].unit = detIdent.unit;
    wire[i].type = 11;
    wire[i].zSize = 2. * innerRadius;
    wire[i].pitch = distanceXY * 2;
    wire[i].wireDist = -1.0 * wire[i].pitch * (wire[i].nWires - 1) / 2;
  }

  wire[0].zCen = wire[2].zCen = positionVector[2];
  wire[1].zCen = wire[3].zCen = positionVector[2] + distanceZ;

  if (mw2Type == MW2Plane::X) {
    for (int i = 0; i < 4; i++) {
      wire[i].angle = 0;
      wire[i].rotMatrix = TGEANT::ROT_0DEG;
      wire[i].xSize = (wire[i].nWires - 1) * wire[i].pitch + 2. * outerRadius;
    }
    wire[0].ySize = wire[1].ySize = 2. * lengthTubeLong;
    wire[2].ySize = wire[3].ySize = 2. * lengthTubeShort_B;

    wire[0].xCen = positionVector[0];
    wire[1].xCen = positionVector[0] - distanceXY;
    wire[2].xCen = positionVector[0] + shiftToHole[0];
    wire[3].xCen = positionVector[0] + shiftToHole[0] - distanceXY;

    wire[0].yCen = wire[1].yCen = positionVector[1];
    wire[2].yCen = wire[3].yCen = positionVector[1] + lengthHole / 2. + lengthTubeShort_B;

  } else if (mw2Type == MW2Plane::Y) {
    for (int i = 0; i < 4; i++) {
      wire[i].angle = 90;
      wire[i].rotMatrix = TGEANT::ROT_0DEG;
      wire[i].ySize = (wire[i].nWires - 1) * wire[i].pitch + 2. * outerRadius;
    }
    wire[0].xSize = wire[1].xSize = 2. * lengthTubeLong;
    wire[2].xSize = wire[3].xSize = 2. * lengthTubeShort_B;

    wire[0].xCen = wire[1].xCen = positionVector[0];
    wire[2].xCen = wire[3].xCen = positionVector[0] + shiftToHole[0] - lengthHole / 2. - lengthTubeShort_B;

    wire[0].yCen = wire[2].yCen = positionVector[1];
    wire[1].yCen = wire[3].yCen = positionVector[1] + distanceXY;

  } else { // V-Plane
    wire[0].angle = wire[1].angle = -15;
    wire[2].angle = wire[3].angle = 0;
    wire[0].rotMatrix = wire[1].rotMatrix = TGEANT::ROT_0DEG;
    wire[2].rotMatrix = wire[3].rotMatrix = TGEANT::ROT_15nDEG;

    wire[0].xSize = wire[1].xSize = 5380.0; // copy from detectors.dat
    wire[2].xSize = wire[3].xSize = (wire[2].nWires - 1) * wire[2].pitch + 2. * outerRadius;

    wire[0].ySize = wire[1].ySize = 2244.0; // copy from detectors.dat
    wire[2].ySize = wire[3].ySize = 2. * lengthTubeShort_B;

    wire[0].xCen = positionVector[0];
    wire[1].xCen = positionVector[0] - distanceXY * cos(M_PI * -15. / 180.);
    wire[2].xCen = positionVector[0] + shiftToHole[0] + (lengthHole / 2. + lengthTubeShort_B)
                      * sin(M_PI * 15. / 180.);
    wire[3].xCen = wire[2].xCen - distanceXY * cos(M_PI * -15. / 180.);

    wire[0].yCen = positionVector[1];
    wire[1].yCen = positionVector[1]
        + distanceXY * sin(M_PI * 15. / 180.);
    wire[2].yCen = positionVector[1]
        + (lengthHole / 2. + lengthTubeShort_B)
            * cos(M_PI * 15. / 180.);
    wire[3].yCen = wire[2].yCen
        + distanceXY * sin(M_PI * 15. / 180.);
  }

  T4SDeadZone dead[2];
  for (int i = 0; i < 2; i++) {
    dead[i].id = detectorId[i];
    dead[i].tbName = detIdent.tbName + detTypeName[i];
    dead[i].det = wire[i].det;
    dead[i].unit = detIdent.unit;
    dead[i].sh = 1;
    dead[i].zSize = 2. * innerRadius;
    dead[i].zCen = wire[i].zCen;
  }

  if (mw2Type == MW2Plane::X) {
    dead[0].xSize = dead[1].xSize = wire[2].xSize;
    dead[0].ySize = dead[1].ySize = lengthHole + 2. * lengthTubeShort_B;
    dead[0].xCen = wire[2].xCen;
    dead[1].xCen = wire[3].xCen;
    dead[0].yCen = wire[0].yCen + lengthTubeShort_B;
    dead[1].yCen = wire[1].yCen + lengthTubeShort_B;
    dead[0].rotMatrix = dead[1].rotMatrix = TGEANT::ROT_0DEG;
  } else if (mw2Type == MW2Plane::Y) {
    dead[0].xSize = dead[1].xSize = lengthHole + 2. * lengthTubeShort_B;
    dead[0].ySize = dead[1].ySize = wire[2].ySize;
    dead[0].xCen = wire[0].xCen - lengthTubeShort_B;
    dead[1].xCen = wire[1].xCen - lengthTubeShort_B;
    dead[0].yCen = wire[2].yCen;
    dead[1].yCen = wire[3].yCen;
    dead[0].rotMatrix = dead[1].rotMatrix = TGEANT::ROT_0DEG;
  } else {
    dead[0].xSize = dead[1].xSize = wire[2].xSize;
    dead[0].ySize = dead[1].ySize = lengthHole + 2. * lengthTubeShort_B;
    dead[0].xCen = positionVector[0] + shiftToHole[0]
        + lengthTubeShort_B * sin(M_PI * 15. / 180.);
    dead[1].xCen = dead[0].xCen
        - distanceXY * cos(M_PI * -15. / 180.);
    dead[0].yCen = positionVector[1]
        + lengthTubeShort_B * cos(M_PI * 15. / 180.);
    dead[1].yCen = dead[0].yCen
        + distanceXY * sin(M_PI * 15. / 180.);
    dead[0].rotMatrix = dead[1].rotMatrix = TGEANT::ROT_15nDEG;
  }

  for (int i = 0; i < 4; i++)
    wireDet.push_back(wire[i]);
  for (int i = 0; i < 2; i++)
    deadZone.push_back(dead[i]);
}
