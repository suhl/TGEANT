#include "T4Silicon.hh"

void T4Silicon::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 701;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getSilicon()->size(); i++) {
    const T4SDetector* si = &settingsFile->getStructManager()->getSilicon()->at(i);

    siPlane.push_back(new T4SIPlane(si, firstDetectorId));
    siPlane.back()->construct(world_log);
    firstDetectorId += 2;
  }
}

T4SIPlane::T4SIPlane(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;
  isDownstream =  false;

  if (det->name[4] == 'X') {
    zMult = 1.0;
    isX = true;
    namePlane2 = det->name;
    namePlane2[4] = 'Y';
  } else if (det->name[4] == 'U') {
    zMult = -1.0;
    isX = false;
    namePlane2 = det->name;
    namePlane2[4] = 'V';
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4SIPlane:: Unknown Silicon TBname: " + det->name + ".");
  }
  if (det->name[3] == '4' || det->name[3] == '5') {
    isDownstream = true;
  }

  rot = new CLHEP::HepRotation;
  rot->rotateZ(-2.5 * CLHEP::deg);
  rotTube_1 = new CLHEP::HepRotation;
  rotTube_1->rotateX(90.0 * CLHEP::deg);
  rotTube_2 = new CLHEP::HepRotation;
  rotTube_2->rotateY(90.0 * CLHEP::deg);

  setPosition(det->position);
}

void T4SIPlane::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
	if(isDownstream) {
		tubs.push_back(new G4Tubs("mother_box", 0.0, 9.1 * CLHEP::cm,  1.0 * CLHEP::cm, 0., 360.*CLHEP::deg));
		log.push_back(new G4LogicalVolume(tubs.back(), materials->air_noOptical, "mother_log"));
	} else {
		box.push_back(new G4Box("mother_box", 25.0 * CLHEP::cm, 25.0 * CLHEP::cm, 1.0 * CLHEP::cm));
		log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
	}
  new G4PVPlacement(rot, positionVector + G4ThreeVector(0, 0, 0.8775 * CLHEP::cm * zMult), log.back(), "SI_air", world_log, 0, 0, checkOverlap);

  // silicon in mother
  box.push_back(new G4Box("si_box", 3.5 * CLHEP::cm, 2.5 * CLHEP::cm, 0.0075 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Si, "si_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8775 * CLHEP::cm * zMult), log.back(), "SI_si", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId)); // X or U

  log.push_back(new G4LogicalVolume(box.back(), materials->Si, "si_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.8925 * CLHEP::cm * zMult), log.back(), "SI_si_2", log.at(motherIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(new T4SensitiveDetector(namePlane2, 0, TGEANT::HIT, firstDetectorId + 1)); // V or Y

  // L-Boards
  // G10 in mother
  box.push_back(new G4Box("g10_box", 5.0 * CLHEP::cm, 1.5 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(1.5 * CLHEP::cm * zMult, -4.0 * CLHEP::cm, -0.82 * CLHEP::cm * zMult), log.back(), "SI_g10", log.at(motherIndex), 0, 0, checkOverlap);

  // G10_2 in mother
  unsigned int g10_2Index = log.size();
  box.push_back(new G4Box("g10_2_box", 1.5 * CLHEP::cm, 5.5 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_2_log"));
  new G4PVPlacement(0, G4ThreeVector(-5.0 * CLHEP::cm * zMult, 0, -0.82 * CLHEP::cm * zMult), log.back(), "SI_g10_2", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu in G10_2
  box.push_back(new G4Box("cu_box", 1.3 * CLHEP::cm, 3.65 * CLHEP::cm, 0.015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -0.65 * CLHEP::cm, 0), log.back(), "SI_cu", log.at(g10_2Index), 0, 0, checkOverlap);

  // G10_3 in mother
  unsigned int g10_3Index = log.size();
  box.push_back(new G4Box("g10_3_box", 6.5 * CLHEP::cm, 1.5 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 4.0 * CLHEP::cm, -0.95 * CLHEP::cm * zMult), log.back(), "SI_g10_3", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu_2 in G10_3
  box.push_back(new G4Box("cu_2_box", 4.5 * CLHEP::cm, 1.3 * CLHEP::cm, 0.015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_2_log"));
  new G4PVPlacement(0, G4ThreeVector(0.5 * CLHEP::cm * zMult, 0, 0), log.back(), "SI_cu_2", log.at(g10_3Index), 0, 0, checkOverlap);

  // G10_4 in mother
  box.push_back(new G4Box("g10_4_box", 1.5 * CLHEP::cm, 4.0 * CLHEP::cm, 0.05 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_4_log"));
  new G4PVPlacement(0, G4ThreeVector(5.0 * CLHEP::cm * zMult, -1.5 * CLHEP::cm, -0.95 * CLHEP::cm * zMult), log.back(), "SI_g10_4", log.at(motherIndex), 0, 0, checkOverlap);

  // APV
  // Si_3 in mother
  box.push_back(new G4Box("si_3_box", 0.9 * CLHEP::cm, 3.35 * CLHEP::cm, 0.015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Si, "si_3_log"));
  new G4PVPlacement(0, G4ThreeVector(-4.7 * CLHEP::cm * zMult, -0.85 * CLHEP::cm, -0.885 * CLHEP::cm * zMult), log.back(), "SI_si_3", log.at(motherIndex), 0, 0, checkOverlap);

  // Si_4 in mother
  box.push_back(new G4Box("si_4_box", 4.15 * CLHEP::cm, 0.9/*1.3*/ * CLHEP::cm, 0.015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Si, "si_4_log"));
  new G4PVPlacement(0, G4ThreeVector(0.65 * CLHEP::cm * zMult, 3.7 * CLHEP::cm, -0.885 * CLHEP::cm * zMult), log.back(), "SI_si_4", log.at(motherIndex), 0, 0, checkOverlap);

  // APV
  // Cu_3 in mother
  box.push_back(new G4Box("cu_3_box", 0.25 * CLHEP::cm, 3.35 * CLHEP::cm, 0.04 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_3_log"));
  new G4PVPlacement(0, G4ThreeVector(-5.25 * CLHEP::cm * zMult, -0.85 * CLHEP::cm, -0.73 * CLHEP::cm * zMult), log.back(), "SI_cu_3", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu_4 in mother
  box.push_back(new G4Box("cu_4_box", 4.15 * CLHEP::cm, 0.25 * CLHEP::cm, 0.04 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu, "cu_4_log"));
  new G4PVPlacement(0, G4ThreeVector(0.65 * CLHEP::cm * zMult, 4.25 * CLHEP::cm, -0.83 * CLHEP::cm * zMult), log.back(), "SI_cu_4", log.at(motherIndex), 0, 0, checkOverlap);

  // Capillaries
  // Cu_5 in mother
  tubs.push_back(new G4Tubs("cu_5_tubs", 0.065 * CLHEP::cm, 0.08 * CLHEP::cm,  5.0 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "cu_5_log"));
  new G4PVPlacement(rotTube_1, G4ThreeVector(-3.58 * CLHEP::cm * zMult, -2.0 * CLHEP::cm, -0.69 * CLHEP::cm * zMult), log.back(), "SI_cu_5", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu_6 in mother
  tubs.push_back(new G4Tubs("cu_6_tubs", 0.065 * CLHEP::cm, 0.08 * CLHEP::cm,  6.0 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "cu_6_log"));
  new G4PVPlacement(rotTube_2, G4ThreeVector(2.5 * CLHEP::cm * zMult, 2.58 * CLHEP::cm, -0.79 * CLHEP::cm * zMult), log.back(), "SI_cu_6", log.at(motherIndex), 0, 0, checkOverlap);

  // HV capacitor
  // Cu_7 in mother
  tubs.push_back(new G4Tubs("cu_7_tubs", 0, 0.75 * CLHEP::cm,  0.15 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->Cu, "cu_7_log"));
  new G4PVPlacement(0, G4ThreeVector(6.5 * CLHEP::cm * zMult, -1.5 * CLHEP::cm, -0.75 * CLHEP::cm * zMult), log.back(), "SI_cu_7", log.at(motherIndex), 0, 0, checkOverlap);

  if (isX && !isDownstream) {
    // G10_5 in mother
    unsigned int g10_5Index = log.size();
    box.push_back(new G4Box("g10_5_box", 25. * CLHEP::cm, 25. * CLHEP::cm, 0.4 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_5_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0.2 * CLHEP::cm * zMult), log.back(), "SI_g10_5", log.at(motherIndex), 0, 0, checkOverlap);

    // Air_2 in G10_5
    box.push_back(new G4Box("air_2_box", 3.5 * CLHEP::cm, 2.5 * CLHEP::cm, 0.4 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "air_2_log"));
    new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "SI_air_2", log.at(g10_5Index), 0, 0, checkOverlap);
  }

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->red);
}

void T4Silicon::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < siPlane.size(); i++)
    siPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4SIPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;

  // X or U
  wire.id = firstDetectorId;
  wire.tbName = det->name;
  wire.det = "SID2";
  if (!isX)
    wire.det = "SIU1";

  stringstream myStream;
  myStream << det->name[3];
  wire.unit = strToInt(myStream.str());
  wire.type = 21;

  wire.xSize = 7.000 * CLHEP::cm;
  wire.ySize = 5.000 * CLHEP::cm;
  wire.zSize = 0.015 * CLHEP::cm;

  wire.xCen = positionVector[0];
  wire.yCen = positionVector[1];
  wire.zCen = positionVector[2];

  wire.rotMatrix = TGEANT::ROT_025pDEG;
  wire.pitch = 0.005461 * CLHEP::cm;
  wire.nWires = 1280;
  wire.wireDist = -3.6060 * CLHEP::cm;
  wire.angle = -2.500;
  if (!isX) {
    wire.wireDist = -3.3880 * CLHEP::cm;
    wire.angle = 2.500;
  }

  wireDet.push_back(wire);

  // Y or V
  wire.id = firstDetectorId + 1;
  wire.tbName = namePlane2;
  wire.det = "SID1";
  if (!isX)
    wire.det = "SIU2";

  wire.zCen = positionVector[2] - 0.015 * CLHEP::cm;
  if (!isX)
    wire.zCen = positionVector[2] + 0.015 * CLHEP::cm;

  wire.pitch = 0.005170 * CLHEP::cm;
  wire.nWires = 1024;
  wire.wireDist = -2.6444 * CLHEP::cm;
  wire.angle = 87.500;
  if (!isX)
    wire.angle = -87.500;

  wireDet.push_back(wire);
}

T4Silicon::~T4Silicon(void)
{
  for (unsigned int i = 0; i < siPlane.size();i++)
    delete siPlane.at(i);
  siPlane.clear();
}

T4SIPlane::~T4SIPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  if (rot != NULL)
    delete rot;
  if (rotTube_1 != NULL)
    delete rotTube_1;
  if (rotTube_2 != NULL)
    delete rotTube_2;
}
