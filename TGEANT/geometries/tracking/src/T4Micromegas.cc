#include "T4Micromegas.hh"

void T4Micromegas::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 404;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getMicromegas()->size(); i++) {
    const T4SDetector* mm = &settingsFile->getStructManager()->getMicromegas()->at(i);

    if (mm->name[1] == 'M') {
      mmPlane.push_back(new T4MMPlane(mm, firstDetectorId));
      mmPlane.back()->construct(world_log);
      firstDetectorId += 3;
    } else if (mm->name[1] == 'P') {
      pmmPlane.push_back(new T4PixelMM(mm));
      pmmPlane.back()->construct(world_log);
    }
  }
}

T4MMPlane::T4MMPlane(const T4SDetector* _det, int _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;

  if (det->name[4] == 'X') {
    rot = NULL;
    zMult = 1.0;
  } else {
    rot = new CLHEP::HepRotation;
    if (det->name[4] == 'Y') {
      rot->rotateZ(-90.0 * CLHEP::deg);
      zMult = -1.0;
    } else if (det->name[4] == 'V') {
      rot->rotateZ(45.0 * CLHEP::deg);
      zMult = 1.0;
    } else if (det->name[4] == 'U') {
      rot->rotateZ(-45.0 * CLHEP::deg);
      zMult = -1.0;
    } else {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
          __FILE__,
          "T4MMPlane:: Unknown micromegas TBname: " + det->name
              + ". Treat it as X plane.");
    }
  }

  setPosition(det->position);

  dimensionMiddle[0] = 9.2068 * CLHEP::cm;
  dimensionMiddle[1] = 20. * CLHEP::cm;
  dimensionMiddle[2] = 0.25 * CLHEP::cm;

  dimensionOuterX = 5.3706 * CLHEP::cm;
}

void T4MMPlane::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", 70. * CLHEP::cm, 29. * CLHEP::cm, 1.134 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(rot, positionVector + G4ThreeVector(0, 0, -0.219 * CLHEP::cm * zMult), log.back(), "MM_air", world_log, 0, 0, checkOverlap);

  // CH2 in mother
  unsigned int ch2Index = log.size();
  box.push_back(new G4Box("ch2_box", 27.75 * CLHEP::cm, 27.75 * CLHEP::cm, 0.8 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->polyethylene, "ch2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.331 * CLHEP::cm * zMult), log.back(), "MM_ch2", log.at(motherIndex), 0, 0, checkOverlap);

  // Ne in CH2
  unsigned int neIndex = log.size();
  box.push_back(new G4Box("ne_box", 26. * CLHEP::cm, 26. * CLHEP::cm, 0.8 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "ne_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_ne", log.at(ch2Index), 0, 0, checkOverlap);

  // Cu/10 in Ne
  box.push_back(new G4Box("cu_box", 23. * CLHEP::cm, 23. * CLHEP::cm, 0.0025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_10, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -0.2975 * CLHEP::cm * zMult), log.back(), "MM_cu", log.at(neIndex), 0, 0, checkOverlap);

  // G10 in Ne
  unsigned int g10Index = log.size();
  box.push_back(new G4Box("g10_box", 23. * CLHEP::cm, 23. * CLHEP::cm, 0.25 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.55 * CLHEP::cm * zMult), log.back(), "MM_g10", log.at(neIndex), 0, 0, checkOverlap);

  // Ne in G10 (daughter #2)
  unsigned int Ne_d2Index = log.size();
  box.push_back(new G4Box("Ne_d2_box", 22. * CLHEP::cm, 22. * CLHEP::cm, 0.25 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "Ne_d2_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_Ne_d2", log.at(g10Index), 0, 0, checkOverlap);

  // Ne_sens in Ne_d2
  unsigned int Ne_sensIndex = log.size();
  box.push_back(new G4Box("Ne_sens_box", dimensionMiddle[0], dimensionMiddle[1], dimensionMiddle[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "Ne_sens_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_Ne_sens", log.at(Ne_d2Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId));

  // dead zone hole in Ne_sens
  tubs.push_back(new G4Tubs("Ne_dead_tubs", 0, 2.5 * CLHEP::cm,  0.25 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->neon, "Ne_dead_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_dead_zone", log.at(Ne_sensIndex), 0, 0, checkOverlap);

  // Ne_sens in Ne_d2
  box.push_back(new G4Box("Ne_sens_box", dimensionOuterX, dimensionMiddle[1], dimensionMiddle[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "Ne_sens_log"));
  new G4PVPlacement(0, G4ThreeVector(-14.5774 * CLHEP::cm, 0, 0), log.back(), "MM_Ne_sens", log.at(Ne_d2Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(det->name, 1, TGEANT::HIT, firstDetectorId + 1));

  // Ne_sens in Ne_d2
  box.push_back(new G4Box("Ne_sens_box", dimensionOuterX, dimensionMiddle[1], dimensionMiddle[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "Ne_sens_log"));
  new G4PVPlacement(0, G4ThreeVector(14.5774 * CLHEP::cm, 0, 0), log.back(), "MM_Ne_sens", log.at(Ne_d2Index), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(det->name, 2, TGEANT::HIT, firstDetectorId + 2));

  // Support in mother
  box.push_back(new G4Box("support_box", 70. * CLHEP::cm, 29. * CLHEP::cm, 0.325 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->mmSupport, "support_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.809 * CLHEP::cm * zMult), log.back(), "MM_support", log.at(motherIndex), 0, 0, checkOverlap);

  // Vacuum in mother
  unsigned int vacuumIndex = log.size();
  box.push_back(new G4Box("vacuum_box", 70. * CLHEP::cm, 29. * CLHEP::cm, 0.005 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->vacuum_noOptical, "vacuum_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.474 * CLHEP::cm * zMult), log.back(), "MM_vacuum", log.at(motherIndex), 0, 0, checkOverlap);

  // Ne_d3 in vacuum
  unsigned int Ne_d3Index = log.size();
  box.push_back(new G4Box("Ne_d3_box", 20. * CLHEP::cm, 20. * CLHEP::cm, 0.005 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->neon, "Ne_d3_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_Ne_d3", log.at(vacuumIndex), 0, 0, checkOverlap);

  // Vacuum tube in Ne_d3
  tubs.push_back(new G4Tubs("Vacuum_tubs", 0, 2.5 * CLHEP::cm,  0.005 * CLHEP::cm, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->vacuum_noOptical, "Vacuum_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MM_vacuum_tubs", log.at(Ne_d3Index), 0, 0, checkOverlap);

  // Cu/10 in mother
  box.push_back(new G4Box("cu_box", 60. * CLHEP::cm, 20. * CLHEP::cm, 0.0025 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_10, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0.4815 * CLHEP::cm * zMult), log.back(), "MM_cu", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu/Kap in mother
  box.push_back(new G4Box("cu_box", 16.125 * CLHEP::cm, 25. * CLHEP::cm, 0.0015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(43.875 * CLHEP::cm, 0, 0.4675 * CLHEP::cm * zMult), log.back(), "MM_cu", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu/Kap in mother
  box.push_back(new G4Box("cu_box", 16.125 * CLHEP::cm, 25. * CLHEP::cm, 0.0015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(-43.875 * CLHEP::cm, 0, 0.4675 * CLHEP::cm * zMult), log.back(), "MM_cu", log.at(motherIndex), 0, 0, checkOverlap);

  // Cu/Kap in mother
  box.push_back(new G4Box("cu_box", 27.75 * CLHEP::cm, 27.75 * CLHEP::cm, 0.0015 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->Cu_Kap, "cu_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -1.1325 * CLHEP::cm * zMult), log.back(), "MM_cu", log.at(motherIndex), 0, 0, checkOverlap);

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->green);
}

void T4Micromegas::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < mmPlane.size(); i++)
    mmPlane.at(i)->getWireDetDat(wireDet, deadZone);
  for (unsigned int i = 0; i < pmmPlane.size(); i++)
    pmmPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4MMPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  T4SDeadZone dead;

  // const for all 3
  wire.tbName = dead.tbName = det->name;

  stringstream myStream;
  myStream << det->name[3];
  wire.unit = dead.unit = strToInt(myStream.str());
  wire.type = 27;
  wire.ySize = 2. * dimensionMiddle[1];
  wire.zSize = 2. * dimensionMiddle[2];
  wire.zCen = dead.zCen = positionVector.getZ();

  if (det->name[4] == 'X') {
    wire.rotMatrix = TGEANT::ROT_0DEG;
    dead.rotMatrix = (TGEANT::RoationMatrix) 12;
  } else if (det->name[4] == 'Y') {
    wire.rotMatrix = TGEANT::ROT_90pDEG;
    dead.rotMatrix = (TGEANT::RoationMatrix) 14;
  } else if (det->name[4] == 'V') {
    wire.rotMatrix = TGEANT::ROT_45nDEG;
    dead.rotMatrix = (TGEANT::RoationMatrix) 15;
  } else if (det->name[4] == 'U') {
    wire.rotMatrix = TGEANT::ROT_45pDEG;
    dead.rotMatrix = (TGEANT::RoationMatrix) 13;
  }

  wire.angle = 0;

  // variable
  // 1) middle
  wire.id = dead.id = firstDetectorId;
  wire.det = dead.det = ((string) "SM" + det->name[4] + "1");
  wire.xSize = 2. * dimensionMiddle[0];
  wire.xCen = dead.xCen = positionVector.getX();
  wire.yCen = dead.yCen = positionVector.getY();
  wire.nWires = 512;
  wire.pitch = 0.035964 * CLHEP::cm;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;

  dead.sh = 5;
  dead.xSize = 50.0;
  dead.ySize = 5.0;
  dead.zSize = 0.0;

  wireDet.push_back(wire);
  deadZone.push_back(dead);


  // 2) outer minus direction
  wire.id = firstDetectorId + 1;
  wire.det = ((string) "SM" + det->name[4] + "2");
  wire.xSize = 2. * dimensionOuterX;

  if (det->name[4] == 'X') {
    wire.xCen = positionVector.getX() - (dimensionMiddle[0] + dimensionOuterX);
    wire.yCen = positionVector.getY();
  } else if (det->name[4] == 'Y') {
    wire.yCen = positionVector.getY() - (dimensionMiddle[0] + dimensionOuterX);
    wire.xCen = positionVector.getX();
  } else if (det->name[4] == 'V') {
    wire.xCen = positionVector.getX() - ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
    wire.yCen = positionVector.getY() + ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
  } else if (det->name[4] == 'U') {
    wire.xCen = positionVector.getX() - ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
    wire.yCen = positionVector.getY() - ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
  }

  wire.nWires = 256;
  wire.pitch = 0.041958 * CLHEP::cm;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;


  wireDet.push_back(wire);


  // 2) outer plus direction
  wire.id = firstDetectorId + 2;
  wire.det = ((string) "SM" + det->name[4] + "3");
  wire.xSize = 2. * dimensionOuterX;

  if (det->name[4] == 'X') {
    wire.xCen = positionVector.getX() + (dimensionMiddle[0] + dimensionOuterX);
    wire.yCen = positionVector.getY();
  } else if (det->name[4] == 'Y') {
    wire.yCen = positionVector.getY() + (dimensionMiddle[0] + dimensionOuterX);
    wire.xCen = positionVector.getX();
  } else if (det->name[4] == 'V') {
    wire.xCen = positionVector.getX() + ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
    wire.yCen = positionVector.getY() - ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
  } else if (det->name[4] == 'U') {
    wire.xCen = positionVector.getX() + ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
    wire.yCen = positionVector.getY() + ((dimensionMiddle[0] + dimensionOuterX) / sqrt(2));
  }

  wire.nWires = 256;
  wire.pitch = 0.041958 * CLHEP::cm;
  wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;

  wireDet.push_back(wire);
}

T4Micromegas::~T4Micromegas(void)
{
  for (unsigned int i = 0; i < mmPlane.size();i++)
    delete mmPlane.at(i);
  mmPlane.clear();
  for (unsigned int i = 0; i < pmmPlane.size();i++)
    delete pmmPlane.at(i);
  pmmPlane.clear();
}

T4MMPlane::~T4MMPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  if (rot != NULL)
    delete rot;
}
