#include "T4MDTModule.hh"

T4MDTModule::T4MDTModule(void)
{
  nChambers = 8;
  plasticThickness = 1.0 * CLHEP::mm;
  steelThickness = 0.15 * CLHEP::mm;
  aluThickness = 0.6 * CLHEP::mm;
  wireThickness = 50.0 * CLHEP::micrometer;
  wireDistance = 10.0 * CLHEP::mm;

  plasticHousing[0] = 82.6 / 2. * CLHEP::mm;
  plasticHousing[1] = 422.0 / 2. * CLHEP::cm;
  plasticHousing[2] = 12.15 / 2. * CLHEP::mm;

  steelHousing[0] = plasticHousing[0] - plasticThickness;
  steelHousing[1] = plasticHousing[1] - plasticThickness;
  steelHousing[2] = steelThickness / 2;

  aluHousing[0] = steelHousing[0];
  aluHousing[1] = steelHousing[1];
  aluHousing[2] = plasticHousing[2] - plasticThickness - steelThickness / 2.;

  gasChamber[0] = (wireDistance - aluThickness) / 2.;
  gasChamber[1] = aluHousing[1] - aluThickness;
  gasChamber[2] = aluHousing[2] - aluThickness / 2.;

  rotationMatrix = NULL;
  wire_tubs = NULL;

  wireRotation = new CLHEP::HepRotation;
  wireRotation->rotateX(90.0 * CLHEP::deg);
}

void T4MDTModule::construct(G4LogicalVolume* world_log)
{
  unsigned int plasticHousingIndex = log.size();
  box.push_back(new G4Box("plasticHousing_box", plasticHousing[0],
      plasticHousing[1], plasticHousing[2]));
  log.push_back(new G4LogicalVolume(box.back(),
      materials->plastic, "plasticHousing_log"));
  new G4PVPlacement(rotationMatrix, positionVector, log.back(),
      "plasticHousing_phys", world_log, 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->silver);

  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;

  box.push_back(new G4Box("steelHousing_box", steelHousing[0],
      steelHousing[1], steelHousing[2]));
  log.push_back(new G4LogicalVolume(box.back(),
      materials->stainlessSteel, "steelHousing_log"));
  new G4PVPlacement(0,
      G4ThreeVector(0, 0,
          plasticHousing[2] - plasticThickness - steelThickness / 2.),
          log.back(), "steelHousing_phys", log.at(plasticHousingIndex), 0, 0,
      checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  unsigned int aluHousingIndex = log.size();
  box.push_back(new G4Box("aluHousing_box", aluHousing[0], aluHousing[1],
      aluHousing[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical,
      "aluHousing_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, -steelThickness / 2.), log.back(),
      "aluHousing_phys", log.at(plasticHousingIndex), 0, 0, checkOverlap);
  log.back()->SetVisAttributes(colour->invisible);

  wire_tubs = new G4Tubs("wire_tubs", 0.0 * CLHEP::mm, wireThickness / 2.,
      gasChamber[1], 0., 2. * M_PI);
  log.push_back(new G4LogicalVolume(wire_tubs, materials->gold, "wire_log"));
  log.back()->SetVisAttributes(colour->invisible);

  box.push_back(new G4Box("gasChamber_box", gasChamber[0], gasChamber[1],
      gasChamber[2]));

  for (G4int i = 0; i < nChambers; i++) {
    sens_log.push_back(new G4LogicalVolume(box.back(), materials->mdtGas,
        "gasChamber_log"));
    sens_log.back()->SetVisAttributes(colour->invisible);
    new G4PVPlacement(0,
        G4ThreeVector((-3.5 + i) * wireDistance, 0, aluThickness / 2.),
        sens_log.back(), "gasChamber_phys", log.at(aluHousingIndex), 0, 0,
        checkOverlap);
    new G4PVPlacement(wireRotation, G4ThreeVector(0, 0, 0), log.back(),
        "wire_phys", sens_log.back(), 0, 0, checkOverlap);
  }
}

void T4MDTModule::setFullLength(G4double length)
{
  plasticHousing[1] = length / 2;
  steelHousing[1] = plasticHousing[1] - plasticThickness;
  aluHousing[1] = steelHousing[1];
  gasChamber[1] = aluHousing[1] - aluThickness / 2.;
}

T4MDTModule::~T4MDTModule(void)
{
  if (wire_tubs != NULL)
    delete wire_tubs;
  delete wireRotation;

  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();

  for (unsigned int i = 0; i < sens_log.size();i++)
    delete sens_log.at(i);
  sens_log.clear();
}
