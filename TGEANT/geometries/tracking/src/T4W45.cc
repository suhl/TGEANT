#include "T4W45.hh"
#include "T4SGlobals.hh"

int T4W45Plane::counterDet[6] = { 1, 1, 1, 1, 1, 1 };

void T4W45::construct(G4LogicalVolume* world_log)
{
  int startDetectorId = 165;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getW45()->size(); i++) {
    const T4SDetector* w45 = &settingsFile->getStructManager()->getW45()->at(i);

    char c = w45->name[4];
    if (c == 'X') {
      w45Planes.push_back(new T4W45PlaneX(w45, startDetectorId));
    } else if (c == 'Y') {
      w45Planes.push_back(new T4W45PlaneY(w45, startDetectorId));
    } else if (c == 'U' || c == 'V') {
      w45Planes.push_back(new T4W45PlaneUV(w45, startDetectorId));
    } else {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__, "T4W45:: Unknown W45 plane name: " + w45->name + ".");
    }

    startDetectorId += 2;
  }

  for (unsigned int i = 0; i < w45Planes.size(); i++)
    w45Planes.at(i)->construct(world_log);
}

T4W45Plane::T4W45Plane(const T4SDetector* t4SDetector, G4int _detectorId)
{
  useMechanicalStructure = t4SDetector->useMechanicalStructure;
  detectorName[0] = t4SDetector->name + "1__";
  detectorName[1] = t4SDetector->name + "2__";
  detectorId[0] = _detectorId;
  detectorId[1] = _detectorId + 1;

  setPosition(t4SDetector->position);

  if (detectorName[0].at(3) == '4' || detectorName[0].at(3) == '5')
    deadDiameter = 50 * CLHEP::cm;
  else
    deadDiameter = 100 * CLHEP::cm;

  pitch = 4.0 * CLHEP::cm;
  sensWireDiameter = 20.0 * CLHEP::micrometer;
  fieldWireDiameter = 200.0 * CLHEP::micrometer;
  cathodeWireDiameter = 100.0 * CLHEP::micrometer;

  size[0] = (522.0 * CLHEP::cm + fieldWireDiameter) / 2;
  size[1] = (262.0 * CLHEP::cm + fieldWireDiameter) / 2;
  size[2] = 2.0 * CLHEP::cm / 2;

  nWires = 0;
  angle = 0;

  mylarHull_box = NULL;
  mylarHull_log = NULL;
  gasVolume_box = NULL;
  gasVolume_log = NULL;
  cathodeVolume_box = NULL;
  cathodeVolume_log = NULL;
  sdVolume_box = NULL;
  sdVolume_log[0] = NULL;
  sdVolume_log[1] = NULL;

  outerFrame_box = NULL;
  innerFrame_box = NULL;
  frame_sub = NULL;
  frame_log = NULL;

  sensWire = NULL;
  fieldWire = NULL;
  sensWire_log = NULL;
  fieldWire_log = NULL;
  sensWireRot = NULL;
}

void T4W45Plane::construct(G4LogicalVolume* world_log)
{
  if (useMechanicalStructure)
    constructFrame(world_log);

  mylarHull_box = new G4Box("mylarHull_box", size[0], size[1], 3 * size[2]);
  mylarHull_log = new G4LogicalVolume(mylarHull_box, materials->mylar,
      "mylarHull_log");
  mylarHull_log->SetVisAttributes(colour->mediumpurple);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 1.0 * CLHEP::cm),
      mylarHull_log, "mylarHull_phys", world_log, 0, 0, checkOverlap);

  G4double mylarThickness = 0.1 * CLHEP::mm;
  gasVolume_box = new G4Box("gasVolume_box", size[0], size[1],
      3 * size[2] - mylarThickness);
  gasVolume_log = new G4LogicalVolume(gasVolume_box, materials->w45Gas,
      "gasVolume_log");
  gasVolume_log->SetVisAttributes(colour->invisible);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), gasVolume_log, "gasVolume_phys",
      mylarHull_log, 0, 0, checkOverlap);

  sdVolume_box = new G4Box("sdVolume_box", size[0], size[1],
      size[2] - cathodeWireDiameter / 2.);

  for (int i = 0; i < 2; i++) {
    sdVolume_log[i] = new G4LogicalVolume(sdVolume_box, materials->w45Gas,
        "sdVolume_log");
    sdVolume_log[i]->SetVisAttributes(colour->invisible);
    G4double zPos = i == 0 ? -1.0 : 1.0;
    new G4PVPlacement(0, G4ThreeVector(0, 0, zPos * CLHEP::cm), sdVolume_log[i],
        detectorName[i], gasVolume_log, 0, 0, checkOverlap);

    T4SensitiveDetector* sens = new T4SensitiveDetector(detectorName[i], 0,
        TGEANT::HIT, detectorId[i]);
    sens->addDeadZoneTubs(positionVector, deadDiameter / 2.);
    sdVolume_log[i]->SetSensitiveDetector(sens);
  }

  cathodeVolume_box = new G4Box("cathodeVolume_box", size[0], size[1],
      cathodeWireDiameter / 2.);
  cathodeVolume_log = new G4LogicalVolume(cathodeVolume_box, materials->w45CathodeGas,
      "cathodeVolume_log");
  cathodeVolume_log->SetVisAttributes(colour->invisible);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), cathodeVolume_log,
      "W45_cathodeVolume", gasVolume_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, -2. * size[2]), cathodeVolume_log,
      "W45_cathodeVolume", gasVolume_log, 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 0, 2. * size[2]), cathodeVolume_log,
      "W45_cathodeVolume", gasVolume_log, 0, 0, checkOverlap);


  wireDist[0] = -1.0 * (nWires - 1) / 2 * pitch;
  wireDist[1] = wireDist[0] + pitch / 2.;

  // special case for DW01 and DW02
  if (detectorName[0].at(3) == '1' || detectorName[0].at(3) == '2') {
    if (detectorName[0].at(4) == 'X') {
      wireDist[1] = wireDist[0] - pitch / 2.;
    } else {
      wireDist[1] = wireDist[0];
      wireDist[0] -= pitch / 2.;
    }
  }

  if (!settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    constructWire();
}

T4W45PlaneX::T4W45PlaneX(const T4SDetector* t4SDetector, G4int _detectorId) :
    T4W45Plane(t4SDetector, _detectorId)
{
  angle = 0;
  nWires = 130;
}

void T4W45PlaneX::constructWire(void)
{
  sensWire = new G4Tubs("sensWireX_tubs", 0., sensWireDiameter / 2., size[1], 0, 2. * M_PI);
  fieldWire = new G4Tubs("fieldWireX_tubs", 0., fieldWireDiameter / 2., size[1], 0, 2. * M_PI);
  sensWire_log = new G4LogicalVolume(sensWire, materials->tungsten, "sensWireX_log");
  fieldWire_log = new G4LogicalVolume(fieldWire, materials->Be, "fieldWireX_log");
  sensWireRot = new G4RotationMatrix;
  sensWireRot->rotateX(90*CLHEP::deg);
  sensWire_log->SetVisAttributes(colour->cyan);
  fieldWire_log->SetVisAttributes(colour->magenta);

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < nWires; j++) {

      new G4PVPlacement(sensWireRot,
          G4ThreeVector(wireDist[i] + pitch * j, 0, 0), sensWire_log,
          "W45_wire_sensX", sdVolume_log[i], 0, 0, checkOverlap);

      new G4PVPlacement(sensWireRot,
          G4ThreeVector(wireDist[(i+1)%2] + pitch * j, 0, 0), fieldWire_log,
          "W45_wire_fieldX", sdVolume_log[i], 0, 0, checkOverlap);
    }
  }
}

T4W45PlaneY::T4W45PlaneY(const T4SDetector* t4SDetector, G4int _detectorId) :
    T4W45Plane(t4SDetector, _detectorId)
{
  angle = 90;
  nWires = 65;
}

void T4W45PlaneY::constructWire(void)
{
  sensWire = new G4Tubs("sensWireY_tubs", 0., sensWireDiameter / 2., size[0], 0, 2. * M_PI);
  fieldWire = new G4Tubs("fieldWireY_tubs", 0., fieldWireDiameter / 2., size[0], 0, 2. * M_PI);
  sensWire_log = new G4LogicalVolume(sensWire, materials->tungsten, "sensWireY_log");
  fieldWire_log = new G4LogicalVolume(fieldWire, materials->Be, "fieldWireY_log");
  sensWireRot = new G4RotationMatrix;
  sensWireRot->rotateY(90*CLHEP::deg);
  sensWire_log->SetVisAttributes(colour->cyan);
  fieldWire_log->SetVisAttributes(colour->magenta);

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < nWires; j++) {

      new G4PVPlacement(sensWireRot,
          G4ThreeVector(0, wireDist[i] + pitch * j, 0), sensWire_log,
          "W45_wire_sensY", sdVolume_log[i], 0, 0, checkOverlap);

      new G4PVPlacement(sensWireRot,
          G4ThreeVector(0, wireDist[(i+1)%2] + pitch * j, 0), fieldWire_log,
          "W45_wire_fieldY", sdVolume_log[i], 0, 0, checkOverlap);
    }
  }
}

T4W45PlaneUV::T4W45PlaneUV(const T4SDetector* t4SDetector, G4int _detectorId) :
    T4W45Plane(t4SDetector, _detectorId)
{
  char c = t4SDetector->name[4];
  if (c == 'U')
    angle = -30;
  else
    angle = 30;

  nWires = 144;
}

void T4W45PlaneUV::constructWire(void)
{
  sensWire = new G4Tubs("sensWireUV_tubs", 0., sensWireDiameter / 2.,
      size[1] / cos(angle * CLHEP::deg) - 9.1 * CLHEP::mm, 0, 2. * M_PI);
  fieldWire = new G4Tubs("fieldWireUV_tubs", 0., fieldWireDiameter / 2.,
      size[1] / cos(angle * CLHEP::deg) - 9.1 * CLHEP::mm, 0, 2. * M_PI);
  sensWire_log = new G4LogicalVolume(sensWire, materials->tungsten, "sensWireUV_log");
  fieldWire_log = new G4LogicalVolume(fieldWire, materials->Be, "fieldWireUV_log");

  sensWireRot = new G4RotationMatrix;
  sensWireRot->rotateX(90 * CLHEP::deg);
  sensWireRot->rotateY(angle * CLHEP::deg);
  sensWire_log->SetVisAttributes(colour->cyan);
  fieldWire_log->SetVisAttributes(colour->magenta);

  G4ThreeVector posSens, posField;
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < nWires; j++) {
      posSens = G4ThreeVector(
          (wireDist[i] + pitch * j) / cos(angle * CLHEP::deg), 0, 0);
      posField = G4ThreeVector(
          (wireDist[(i + 1) % 2] + pitch * j) / cos(angle * CLHEP::deg), 0, 0);

      if (j > 31 && j < 112) {

        new G4PVPlacement(sensWireRot, posSens, sensWire_log, "W45_wire_sensUV",
            sdVolume_log[i], 0, 0, checkOverlap);
        new G4PVPlacement(sensWireRot, posField, fieldWire_log,
            "W45_wire_fieldUV", sdVolume_log[i], 0, 0, checkOverlap);

      } else {
//if (j != 10)continue;
        //TODO intersection ist zu langsam!! loesung: draehte kuerzen und position berechnen

        intersection_solid.push_back(new G4IntersectionSolid("intersection", sdVolume_box, sensWire, sensWireRot, posSens));
        intersection_log.push_back(new G4LogicalVolume(intersection_solid.back(), materials->tungsten, "sensWireUV_log"));
        new G4PVPlacement(0, G4ThreeVector(0,0,0), intersection_log.back(), "W45_wire_sensUV",
            sdVolume_log[i], 0, 0, checkOverlap);
        intersection_log.back()->SetVisAttributes(colour->cyan);

        intersection_solid.push_back(new G4IntersectionSolid("intersection", sdVolume_box, fieldWire, sensWireRot, posField));
        intersection_log.push_back(new G4LogicalVolume(intersection_solid.back(), materials->Be, "sensWireUV_log"));
        new G4PVPlacement(0, G4ThreeVector(0,0,0), intersection_log.back(), "W45_wire_fieldUV",
            sdVolume_log[i], 0, 0, checkOverlap);
        intersection_log.back()->SetVisAttributes(colour->magenta);
      }
    }
  }
}

void T4W45Plane::constructFrame(G4LogicalVolume* world_log)
{
  G4double sizeX = 5820.0 * CLHEP::mm / 2;
  G4double sizeY = 3220.0 * CLHEP::mm / 2;
  G4double sizeZ = 6.0 * CLHEP::cm / 2;
  G4double thicknessX = sizeX - size[0];
  G4double thicknessY = sizeY - size[1];

  outerFrame_box = new G4Box("outerFrame_box", sizeX, sizeY, sizeZ);
  innerFrame_box = new G4Box("outerFrame_box", sizeX - thicknessX,
      sizeY - thicknessY, sizeZ + 1.0 * CLHEP::mm);
  frame_sub = new G4SubtractionSolid("frame_sub", outerFrame_box,
      innerFrame_box);
  frame_log = new G4LogicalVolume(frame_sub, materials->aluminium_noOptical,
      "frame_log");
  frame_log->SetVisAttributes(colour->silver);
  new G4PVPlacement(0, positionVector + G4ThreeVector(0, 0, 1.0 * CLHEP::cm),
      frame_log, "frame_phys", world_log, 0, 0, checkOverlap);
}

void T4W45::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < w45Planes.size(); i++)
    w45Planes.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4W45Plane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  // most values here are copied from
  // detectors.20130329.dvcs_2012_plus.dat
  wire1.id = dead1.id = detectorId[0];
  wire2.id = dead2.id = detectorId[1];
  wire1.tbName = dead1.tbName = detectorName[0];
  wire2.tbName = dead2.tbName = detectorName[1];

  wire1.det = dead1.det = getDetName(wire1.tbName);
  wire2.det = dead2.det = getDetName(wire2.tbName);
  wire1.unit = dead1.unit = 1;
  wire2.unit = dead2.unit = 1;
  wire1.type = wire2.type = 11;

  wire1.xSize = wire2.xSize = 2. * size[0];
  wire1.ySize = wire2.ySize = 2. * size[1];
  wire1.zSize = wire2.zSize = 2. * size[2];
  wire1.xCen = wire2.xCen = dead1.xCen = dead2.xCen = positionVector[0];
  wire1.yCen = wire2.yCen = dead1.yCen = dead2.yCen = positionVector[1];
  wire1.zCen = dead1.zCen = positionVector[2];
  wire2.zCen = dead2.zCen = positionVector[2] + 20;

  dead1.sh = dead2.sh = 5;
  dead1.xSize = dead2.xSize = deadDiameter;
  dead1.ySize = dead2.ySize = 2. * size[2];
  dead1.zSize = dead2.zSize = 0.0;
  dead1.rotMatrix = dead2.rotMatrix = (TGEANT::RoationMatrix) 12;

  wire1.rotMatrix = wire2.rotMatrix = TGEANT::ROT_0DEG;
  wire1.angle = wire2.angle = angle;

  wire1.nWires = wire2.nWires = nWires;
  wire1.pitch = wire2.pitch = pitch;
  wire1.wireDist = wireDist[0];
  wire2.wireDist = wireDist[1];

  wireDet.push_back(wire1);
  deadZone.push_back(dead1);
  wireDet.push_back(wire2);
  deadZone.push_back(dead2);
}

G4String T4W45Plane::getDetName(G4String tbName)
{
  char type = tbName[3];
  if (type == '1') {
    return (G4String) "DW" + type + intToStr(counterDet[0]++);
  } else if (type == '2') {
    return (G4String) "DW" + type + intToStr(counterDet[1]++);
  } else if (type == '3') {
    return (G4String) "DW" + type + intToStr(counterDet[2]++);
  } else if (type == '4') {
    return (G4String) "DW" + type + intToStr(counterDet[3]++);
  } else if (type == '5') {
    return (G4String) "DW" + type + intToStr(counterDet[4]++);
  } else if (type == '6') {
    return (G4String) "DW" + type + intToStr(counterDet[5]++);
  } else {
    return "XXXX";
  }
}

T4W45::~T4W45(void)
{
  for (unsigned int i = 0; i < w45Planes.size(); i++)
    delete w45Planes.at(i);
  w45Planes.clear();
}

T4W45Plane::~T4W45Plane(void)
{
  if (mylarHull_box != NULL)
    delete mylarHull_box;
  if (mylarHull_log != NULL)
    delete mylarHull_log;
  if (gasVolume_box != NULL)
    delete gasVolume_box;
  if (gasVolume_log != NULL)
    delete gasVolume_log;
  if (cathodeVolume_box != NULL)
    delete cathodeVolume_box;
  if (cathodeVolume_log != NULL)
    delete cathodeVolume_log;
  if (sdVolume_box != NULL)
    delete sdVolume_box;
  if (sdVolume_log[0] != NULL)
    delete sdVolume_log[0];
  if (sdVolume_log[1] != NULL)
    delete sdVolume_log[1];

  if (outerFrame_box != NULL)
    delete outerFrame_box;
  if (innerFrame_box != NULL)
    delete innerFrame_box;
  if (frame_sub != NULL)
    delete frame_sub;
  if (frame_log != NULL)
    delete frame_log;

  if (sensWire != NULL)
    delete sensWire;
  if (fieldWire != NULL)
    delete fieldWire;
  if (sensWire_log != NULL)
    delete sensWire_log;
  if (fieldWire_log != NULL)
    delete fieldWire_log;
  if (sensWireRot != NULL)
    delete sensWireRot;
}
