#include "StrawPlaneX.hh"

StrawPlaneX::StrawPlaneX(const T4SDetector* straw) :
    StrawPlane(straw)
{
  rotationTube->rotateX(90 * CLHEP::deg);

  strawLength = 3202.0 / 2 * CLHEP::mm;
  mylarSize[0] = 3232.0 / 2 * CLHEP::mm;
  mylarSize[1] = strawLength;
  holeLength = 194.0 / 2 * CLHEP::mm;

  nOuterWires = 96;
  nInnerLongWires = 95;
  nInnerShortWires = 32;

  // for mechanical structure
  carbonSize[0] = mylarSize[0];
  carbonSize[1] = 9.0 / 2 * CLHEP::mm;
}

StrawPlaneU::StrawPlaneU(const T4SDetector* straw) :
    StrawPlaneX(straw)
{
  rotationPlane->rotateZ(-10 * CLHEP::deg);
}

StrawPlaneV::StrawPlaneV(const T4SDetector* straw) :
    StrawPlaneX(straw)
{
  rotationPlane->rotateZ(10 * CLHEP::deg);
}

void StrawPlaneX::getWireDetDat(std::vector<T4SWireDetector>& wireDet, std::vector<T4SDeadZone>& deadZone)
{
  setDefault();
  setRotation();

  for (unsigned int i = 0; i < 2; i++) {
    wire_b[i].xSize = (2*nInnerLongWires + nInnerShortWires) * 2. * outerRadiusSmall;
    wire_a[i].xSize = wire_c[i].xSize = nOuterWires * 2. * outerRadiusBig;

    wire_a[i].ySize = wire_b[i].ySize = wire_c[i].ySize = 2. * strawLength;

    dead[i].xSize = nInnerShortWires * 2. * outerRadiusSmall;
    dead[i].ySize = 2. * holeLength;
    dead[i].sh = 1;

    wire_b[i].xCen = dead[i].xCen = positionVector[0] + posOffsetSmall[i];
    G4double xOffset = (nInnerShortWires/2. + nInnerLongWires) * 2. * outerRadiusSmall + nOuterWires * outerRadiusBig;
    G4double overlap = (outerRadiusBig - outerRadiusSmall);
    wire_a[i].xCen = positionVector[0] - xOffset - overlap + posOffsetSmall[i];
    wire_c[i].xCen = positionVector[0] + xOffset + posOffsetSmall[i];

    wire_a[i].yCen = wire_b[i].yCen = wire_c[i].yCen = dead[i].yCen = positionVector[1];

    wire_a[i].angle = wire_b[i].angle = wire_c[i].angle = 0;

    wireDet.push_back(wire_a[i]);
    wireDet.push_back(wire_b[i]);
    wireDet.push_back(wire_c[i]);
    deadZone.push_back(dead[i]);
  }
}

void StrawPlaneX::setRotation(void)
{
  for (unsigned int i = 0; i < 2; i++)
    wire_a[i].rotMatrix = wire_b[i].rotMatrix = wire_c[i].rotMatrix = dead[i]
        .rotMatrix = TGEANT::ROT_0DEG;
}

void StrawPlaneU::setRotation(void)
{
  for (unsigned int i = 0; i < 2; i++)
    wire_a[i].rotMatrix = wire_b[i].rotMatrix = wire_c[i].rotMatrix = dead[i]
        .rotMatrix = TGEANT::ROT_10pDEG;
}

void StrawPlaneV::setRotation(void)
{
  for (unsigned int i = 0; i < 2; i++)
    wire_a[i].rotMatrix = wire_b[i].rotMatrix = wire_c[i].rotMatrix = dead[i]
        .rotMatrix = TGEANT::ROT_10nDEG;
}
