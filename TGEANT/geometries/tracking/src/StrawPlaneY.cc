#include "StrawPlaneY.hh"

StrawPlaneY::StrawPlaneY(const T4SDetector* straw) :
    StrawPlane(straw)
{
  rotationTube->rotateY(90 * CLHEP::deg);

  strawLength = 3652.0 / 2 * CLHEP::mm;
  mylarSize[0] = strawLength;
  mylarSize[1] = 2427.0 / 2 * CLHEP::mm;
  holeLength = 194.0 / 2 * CLHEP::mm;

  nOuterWires = 64;
  nInnerLongWires = 80;
  nInnerShortWires = 32;

  // for mechanical structure
  carbonSize[0] = 9.0 / 2 * CLHEP::mm;
  carbonSize[1] = mylarSize[1];
}

void StrawPlaneY::getWireDetDat(std::vector<T4SWireDetector>& wireDet, std::vector<T4SDeadZone>& deadZone)
{
  setDefault();

  for (unsigned int i = 0; i < 2; i++) {
    wire_a[i].xSize = wire_b[i].xSize = wire_c[i].xSize = 2. * strawLength;

    wire_b[i].ySize = (2*nInnerLongWires + nInnerShortWires) * 2. * outerRadiusSmall;
    wire_a[i].ySize = wire_c[i].ySize = nOuterWires * 2. * outerRadiusBig;

    dead[i].xSize = 2. * holeLength;
    dead[i].ySize = nInnerShortWires * 2. * outerRadiusSmall;
    dead[i].sh = 1;

    wire_a[i].xCen = wire_b[i].xCen = wire_c[i].xCen = dead[i].xCen = positionVector[0];

    wire_b[i].yCen = dead[i].yCen = positionVector[1] + posOffsetSmall[i];
    G4double yOffset = (nInnerShortWires/2. + nInnerLongWires) * 2. * outerRadiusSmall + nOuterWires * outerRadiusBig;
    G4double overlap = (outerRadiusBig - outerRadiusSmall);
    wire_a[i].yCen = positionVector[1] - yOffset - overlap + posOffsetSmall[i];
    wire_c[i].yCen = positionVector[1] + yOffset + posOffsetSmall[i];

    wire_a[i].rotMatrix = wire_b[i].rotMatrix = wire_c[i].rotMatrix = dead[i].rotMatrix = TGEANT::ROT_0DEG;

    wire_a[i].angle = wire_b[i].angle = wire_c[i].angle = 90.;

    wireDet.push_back(wire_a[i]);
    wireDet.push_back(wire_b[i]);
    wireDet.push_back(wire_c[i]);
    deadZone.push_back(dead[i]);
  }
}
