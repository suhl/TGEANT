#include "DCPlane.hh"

DCPlane::DCPlane(void)
{
  calls = -1;

  mylarThickness = 25.0 / 2. * CLHEP::micrometer;
  graphiteThickness = 5.0 / 2. * CLHEP::micrometer;
  wireDistance = 8.0 * CLHEP::mm;
  layerDistance = 18.0 * CLHEP::mm;
  sdThickness = wireDistance / 2. - mylarThickness - 2. * graphiteThickness;
  deadDiameter = 30.0 * CLHEP::cm;

  shiftFromFirstPlaneToMiddle = 1.5 * layerDistance + 2. * wireDistance;

  bkLineThickness = 2.3 * CLHEP::cm; //In RD we can see a size of about 2.8 cm (3*pitch)
  bkLineUVShift = 6.0 * CLHEP::cm;
  bkOnJuraSide = false;

  plane_box = NULL;
  plane_log = NULL;
  mylar_box = NULL;
  mylar_log = NULL;
  graphite_box = NULL;
  graphite_log = NULL;
  sdX_box = NULL;
  sdY_box = NULL;
  sdU_para = NULL;
  sdV_para = NULL;

  xWire_pot = NULL;
  xWire_sens = NULL;
  yWire_pot = NULL;
  yWire_sens = NULL;
  uvWire_pot = NULL;
  uvWire_sens = NULL;

  xWire_pot_log = NULL;
  xWire_sens_log = NULL;
  yWire_pot_log = NULL;
  yWire_sens_log = NULL;
  uvWire_pot_log = NULL;
  uvWire_sens_log = NULL;

  xWire_rot = NULL;
  yWire_rot = NULL;
  uWire_rot = NULL;
  vWire_rot = NULL;

  firstDetectorId = 0;
  nWiresXY = 0;
  nWiresUV = 0;
  pitch = 0;
  angleUVPlane = 0;

  potWireDiameter = 100. * CLHEP::micrometer;
  sensWireDiameter = 20. * CLHEP::micrometer;
}

DC00Plane::DC00Plane(void)
{
  detIdentVec.push_back(detIdent("DC00Y1__", "SDB1", 1)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC00Y2__", "SDB2", 1)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC00X1__", "SDB3", 1)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC00X2__", "SDB4", 1)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC00U1__", "SDB5", 1)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC00U2__", "SDB6", 1)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC00V1__", "SDB7", 1)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC00V2__", "SDB8", 1)); wireDist.push_back(-614.25);
  firstDetectorId = 266; // 266-273

  planeDimension[0] = 180.0 / 2. * CLHEP::cm;
  planeDimension[1] = 127.0 / 2. * CLHEP::cm;
  planeDimension[2] = 11.9 / 2. * CLHEP::cm;

  sizeXPlane[0] = planeDimension[1]; // in COMGEANT this is 180cm, maybe we have to adjust this
  sizeXPlane[1] = planeDimension[1];
  sizeYPlane[0] = planeDimension[0];
  sizeYPlane[1] = planeDimension[1];
  angleUVPlane = 20.0 * CLHEP::deg;
  sizeUVPlane[0] = planeDimension[0] - tan(angleUVPlane) * planeDimension[1]; // old: 120.0 / 2. * CLHEP::cm / cos(angleUVPlane);
  sizeUVPlane[1] = planeDimension[1];

  nWiresXY = nWiresUV = 176;
  pitch = 7.0 * CLHEP::mm;
}

DC01Plane::DC01Plane(void)
{
  detIdentVec.push_back(detIdent("DC01Y1__", "SDB1", 2)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC01Y2__", "SDB2", 2)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC01X1__", "SDB3", 2)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC01X2__", "SDB4", 2)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC01U1__", "SDB5", 2)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC01U2__", "SDB6", 2)); wireDist.push_back(-614.25);
  detIdentVec.push_back(detIdent("DC01V1__", "SDB7", 2)); wireDist.push_back(-610.75);
  detIdentVec.push_back(detIdent("DC01V2__", "SDB8", 2)); wireDist.push_back(-614.25);
  firstDetectorId = 274; // 274-281

  planeDimension[0] = 180.0 / 2. * CLHEP::cm;
  planeDimension[1] = 127.0 / 2. * CLHEP::cm;
  planeDimension[2] = 11.9 / 2. * CLHEP::cm;

  sizeXPlane[0] = planeDimension[1]; // in COMGEANT this is 180cm, maybe we have to adjust this
  sizeXPlane[1] = planeDimension[1];
  sizeYPlane[0] = planeDimension[0];
  sizeYPlane[1] = planeDimension[1];
  angleUVPlane = 20.0 * CLHEP::deg;
  sizeUVPlane[0] = planeDimension[0] - tan(angleUVPlane) * planeDimension[1]; // old: 120.0 / 2. * CLHEP::cm / cos(angleUVPlane);
  sizeUVPlane[1] = planeDimension[1];

  nWiresXY = nWiresUV = 176;
  pitch = 7.0 * CLHEP::mm;
}

DC04Plane::DC04Plane(void)
{
  detIdentVec.push_back(detIdent("DC04U2__", "SDC1", 1)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC04U1__", "SDC2", 1)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC04V2__", "SDC3", 1)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC04V1__", "SDC4", 1)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC04X2__", "SDC5", 1)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC04X1__", "SDC6", 1)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC04Y2__", "SDC7", 1)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC04Y1__", "SDC8", 1)); wireDist.push_back(-1020);
  firstDetectorId = 282; // 282-289

  planeDimension[0] = 248.0 / 2. * CLHEP::cm;
  planeDimension[1] = 208.0 / 2. * CLHEP::cm;
  planeDimension[2] = 11.9 / 2. * CLHEP::cm;

  sizeXPlane[0] = planeDimension[1];//205.2 / 2. * CLHEP::cm; // value from dc04 paper
  sizeXPlane[1] = planeDimension[1];
  sizeYPlane[0] = planeDimension[0];
  sizeYPlane[1] = planeDimension[1];//205.2 / 2. * CLHEP::cm; // value from dc04 paper
  angleUVPlane = 10.0 * CLHEP::deg;
  sizeUVPlane[0] = planeDimension[0] - tan(angleUVPlane) * planeDimension[1];//205.2 / 2. * CLHEP::cm / cos(angleUVPlane);
  sizeUVPlane[1] = planeDimension[1];

  nWiresXY = nWiresUV = 256;
  pitch = 8.0 * CLHEP::mm;

  bkOnJuraSide = true;
}

DC05Plane::DC05Plane(void)
{
  detIdentVec.push_back(detIdent("DC05U2__", "SDC1", 2)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC05U1__", "SDC2", 2)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC05V2__", "SDC3", 2)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC05V1__", "SDC4", 2)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC05X2__", "SDC5", 2)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC05X1__", "SDC6", 2)); wireDist.push_back(-1020);
  detIdentVec.push_back(detIdent("DC05Y2__", "SDC7", 2)); wireDist.push_back(-1016);
  detIdentVec.push_back(detIdent("DC05Y1__", "SDC8", 2)); wireDist.push_back(-1020);
  firstDetectorId = 290; // 290-297

  planeDimension[0] = 248.0 / 2. * CLHEP::cm;
  planeDimension[1] = 208.0 / 2. * CLHEP::cm;
  planeDimension[2] = 11.9 / 2. * CLHEP::cm;

  sizeXPlane[0] = planeDimension[1];
  sizeXPlane[1] = planeDimension[1];
  sizeYPlane[0] = planeDimension[0];
  sizeYPlane[1] = planeDimension[1];
  angleUVPlane = 10.0 * CLHEP::deg;
  sizeUVPlane[0] = planeDimension[0] - tan(angleUVPlane) * planeDimension[1];
  sizeUVPlane[1] = planeDimension[1];

  nWiresXY = 256;
  nWiresUV = 256;
  pitch = 8.0 * CLHEP::mm;
  // rescale UV plane:
//  sizeUVPlane[0] *= nWiresUV / nWiresXY;

  bkOnJuraSide = true;
}

void DCPlane::construct(G4LogicalVolume* world_log)
{
  plane_box = new G4Box("plane_box", planeDimension[0], planeDimension[1],
      planeDimension[2]);
  plane_log = new G4LogicalVolume(plane_box, materials->dc4gas, "plane_log");
  plane_log->SetVisAttributes(colour->mediumpurple);

  mylar_box = new G4Box("mylar_box", planeDimension[0], planeDimension[1],
      mylarThickness);
  mylar_log = new G4LogicalVolume(mylar_box, materials->mylar, "mylar_log");
  mylar_log->SetVisAttributes(colour->invisible);

  graphite_box = new G4Box("graphite_box", planeDimension[0], planeDimension[1],
      graphiteThickness);
  graphite_log = new G4LogicalVolume(graphite_box, materials->graphit,
      "graphite_log");
  graphite_log->SetVisAttributes(colour->invisible);

  // placement of plane_box
  new G4PVPlacement(0,
      positionVector + G4ThreeVector(0, 0, shiftFromFirstPlaneToMiddle),
      plane_log, "plane_phys", world_log, 0, 0, checkOverlap);

  // placement of mylar foil (3 per double layer = 12 per plane)
  G4double zPosMylar[6];
  G4double distSdToMylar = sdThickness + 2. * graphiteThickness + mylarThickness;
  zPosMylar[0] = layerDistance / 2. - distSdToMylar;
  zPosMylar[1] = layerDistance / 2. + distSdToMylar;
  zPosMylar[2] = layerDistance / 2. + wireDistance + distSdToMylar;
  zPosMylar[3] = 1.5 * layerDistance + wireDistance - distSdToMylar;
  zPosMylar[4] = 1.5 * layerDistance + wireDistance + distSdToMylar;
  zPosMylar[5] = 1.5 * layerDistance + 2. * wireDistance + distSdToMylar;
  for (unsigned int i = 0; i < 6; i++) {
    for (unsigned int j = 0; j < 2; j++) {
      G4int factor = j == 0 ? 1 : -1;
      new G4PVPlacement(0, G4ThreeVector(0, 0, factor * zPosMylar[i]),
          mylar_log, "mylar_phys", plane_log, 0, 0, checkOverlap);
    }
  }

  // placement of graphite foil (4 per double layer = 16 per plane)
  G4double zPosGraphite[8];
  G4double distSdToGraphite = sdThickness + graphiteThickness;
  zPosGraphite[0] = layerDistance / 2. - distSdToGraphite;
  zPosGraphite[1] = layerDistance / 2. + distSdToGraphite;
  zPosGraphite[2] = layerDistance / 2. + wireDistance - distSdToGraphite;
  zPosGraphite[3] = layerDistance / 2. + wireDistance + distSdToGraphite;
  zPosGraphite[4] = 1.5 * layerDistance + wireDistance - distSdToGraphite;
  zPosGraphite[5] = 1.5 * layerDistance + wireDistance + distSdToGraphite;
  zPosGraphite[6] = 1.5 * layerDistance + 2. * wireDistance - distSdToGraphite;
  zPosGraphite[7] = 1.5 * layerDistance + 2. * wireDistance + distSdToGraphite;
  for (unsigned int i = 0; i < 8; i++) {
    for (unsigned int j = 0; j < 2; j++) {
      G4int factor = j == 0 ? 1 : -1;
      new G4PVPlacement(0, G4ThreeVector(0, 0, factor * zPosGraphite[i]),
          graphite_log, "graphite_phys", plane_log, 0, 0, checkOverlap);
    }
  }

  // placement of sensitive detectors (2 per double layer = 8 per plane)
  zPosSensitive[0] = layerDistance / 2.;
  zPosSensitive[1] = layerDistance / 2. + wireDistance;
  zPosSensitive[2] = 1.5 * layerDistance + wireDistance;
  zPosSensitive[3] = 1.5 * layerDistance + 2. * wireDistance;

  sdX_box = new G4Box("sdX_box", sizeXPlane[0], sizeXPlane[1], sdThickness);
  sdY_box = new G4Box("sdY_box", sizeYPlane[0], sizeYPlane[1], sdThickness);
  sdU_para = new G4Para("sdU_para", sizeUVPlane[0], sizeUVPlane[1], sdThickness,
      angleUVPlane, 0, 0);
  sdV_para = new G4Para("sdV_para", sizeUVPlane[0], sizeUVPlane[1], sdThickness,
      -angleUVPlane, 0, 0);

  xWire_pot = new G4Tubs("xWire_pot_tubs", 0., potWireDiameter / 2., planeDimension[1], 0, 2.*M_PI);
  xWire_sens = new G4Tubs("xWire_sens_tubs", 0., sensWireDiameter / 2., planeDimension[1], 0, 2.*M_PI);
  xWire_pot_log = new G4LogicalVolume(xWire_pot, materials->Be, "xWire_pot_log");
  xWire_sens_log = new G4LogicalVolume(xWire_sens, materials->tungsten, "xWire_sens_log");
  xWire_pot_log->SetVisAttributes(colour->cyan);
  xWire_sens_log->SetVisAttributes(colour->magenta);
  xWire_rot = new G4RotationMatrix;
  xWire_rot->rotateX(90*CLHEP::deg);

  yWire_pot = new G4Tubs("yWire_pot_tubs", 0., potWireDiameter / 2., planeDimension[0], 0, 2.*M_PI);
  yWire_sens = new G4Tubs("yWire_sens_tubs", 0., sensWireDiameter / 2., planeDimension[0], 0, 2.*M_PI);
  yWire_pot_log = new G4LogicalVolume(yWire_pot, materials->Be, "yWire_pot_log");
  yWire_sens_log = new G4LogicalVolume(yWire_sens, materials->tungsten, "yWire_sens_log");
  yWire_pot_log->SetVisAttributes(colour->cyan);
  yWire_sens_log->SetVisAttributes(colour->magenta);
  yWire_rot = new G4RotationMatrix;
  yWire_rot->rotateY(90*CLHEP::deg);

  uvWire_pot = new G4Tubs("yWire_pot_tubs", 0., potWireDiameter / 2., sizeUVPlane[1] / cos(angleUVPlane) - 0.1 * CLHEP::mm, 0, 2.*M_PI);
  uvWire_sens = new G4Tubs("yWire_sens_tubs", 0., sensWireDiameter / 2., sizeUVPlane[1] / cos(angleUVPlane) - 0.1 * CLHEP::mm, 0, 2.*M_PI);
  uvWire_pot_log = new G4LogicalVolume(uvWire_pot, materials->Be, "uvWire_pot_log");
  uvWire_sens_log = new G4LogicalVolume(uvWire_sens, materials->tungsten, "uvWire_sens_log");
  uvWire_pot_log->SetVisAttributes(colour->cyan);
  uvWire_sens_log->SetVisAttributes(colour->magenta);
  uWire_rot = new G4RotationMatrix;
  uWire_rot->rotateX(90*CLHEP::deg);
  uWire_rot->rotateY(-angleUVPlane);
  vWire_rot = new G4RotationMatrix;
  vWire_rot->rotateX(90*CLHEP::deg);
  vWire_rot->rotateY(angleUVPlane);

  for (unsigned int i = 0; i < detIdentVec.size(); i++) {
    buildPlane(detIdentVec.at(i).tbName);

    sensitive_log.at(i)->SetVisAttributes(colour->red);
    T4SensitiveDetector* sens = new T4SensitiveDetector(detIdentVec.at(i).tbName, 0, TGEANT::HIT,
            firstDetectorId + i);
    sens->addDeadZoneTubs(positionVector, deadDiameter / 2.);
    addBKLine(detIdentVec.at(i).tbName, sens);
    sensitive_log.at(i)->SetSensitiveDetector(sens);
  }
}

void DCPlane::buildPlane(G4String planeName)
{
  G4double zPos;
  switch (++calls) {
    case 0:
      zPos = -zPosSensitive[3];
      break;
    case 1:
      zPos = -zPosSensitive[2];
      break;
    case 2:
      zPos = -zPosSensitive[1];
      break;
    case 3:
      zPos = -zPosSensitive[0];
      break;
    case 4:
      zPos = zPosSensitive[0];
      break;
    case 5:
      zPos = zPosSensitive[1];
      break;
    case 6:
      zPos = zPosSensitive[2];
      break;
    case 7:
      zPos = zPosSensitive[3];
      break;
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__, __FILE__,
          "DCPlane::buildPlane: Only 8 planes allowed.");
      return;
  }

  G4LogicalVolume* wire_pot, *wire_sens;
  G4RotationMatrix* rot;
  G4ThreeVector wirePosStart, wireStep;
  int nWires;

  switch (planeName.at(4)) {
    case 'X':
      sensitive_log.push_back(
          new G4LogicalVolume(sdX_box, materials->dc4gas, "sdX_log"));
      wire_pot = xWire_pot_log;
      wire_sens = xWire_sens_log;
      rot = xWire_rot;
      wirePosStart = G4ThreeVector(wireDist.at(calls), 0, 0);
      wireStep = G4ThreeVector(pitch, 0, 0);
      nWires = nWiresXY;
      break;
    case 'Y':
      sensitive_log.push_back(
          new G4LogicalVolume(sdY_box, materials->dc4gas, "sdY_log"));
      wire_pot = yWire_pot_log;
      wire_sens = yWire_sens_log;
      rot = yWire_rot;
      wirePosStart = G4ThreeVector(0, wireDist.at(calls), 0);
      wireStep = G4ThreeVector(0, pitch, 0);
      nWires = nWiresXY;
      break;
    case 'U':
      sensitive_log.push_back(
          new G4LogicalVolume(sdU_para, materials->dc4gas, "sdU_log"));
      wire_pot = uvWire_pot_log;
      wire_sens = uvWire_sens_log;
      rot = uWire_rot;
      wirePosStart = G4ThreeVector(wireDist.at(calls) / cos(angleUVPlane), 0, 0);
      wireStep = G4ThreeVector(pitch / cos(angleUVPlane), 0, 0);
      nWires = nWiresUV;
      break;
    case 'V':
      sensitive_log.push_back(
          new G4LogicalVolume(sdV_para, materials->dc4gas, "sdV_log"));
      wire_pot = uvWire_pot_log;
      wire_sens = uvWire_sens_log;
      rot = vWire_rot;
      wirePosStart = G4ThreeVector(wireDist.at(calls) / cos(angleUVPlane), 0, 0);
      wireStep = G4ThreeVector(pitch / cos(angleUVPlane), 0, 0);
      nWires = nWiresUV;
      break;
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
          __FILE__, "DCPlane::buildPlane: Unknown plane type.");
      return;
  }

  new G4PVPlacement(0, G4ThreeVector(0, 0, zPos), sensitive_log.back(),
      planeName, plane_log, 0, 0, checkOverlap);

  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;

  G4ThreeVector localPos;
  for (int i = 0; i < nWires; i++) {
    localPos = wirePosStart + i * wireStep;
    new G4PVPlacement(rot, localPos, wire_sens,
        "DC_wire_sens", sensitive_log.back(), 0, 0, checkOverlap);
  }

  for (int i = 0; i < nWires + 1; i++) {
    localPos = wirePosStart + (-0.5 + i) * wireStep;
    new G4PVPlacement(rot, localPos, wire_pot,
        "DC_wire_pot", sensitive_log.back(), 0, 0, checkOverlap);
  }
}

void DCPlane::addBKLine(G4String planeName, T4SensitiveDetector* sens)
{
  G4double xPos(0), yPos(0), xSize(0), ySize(0);

  switch (planeName.at(4)) {
    case 'X':
      xPos = sizeXPlane[0] / 2.;
      if (!bkOnJuraSide)
        xPos *= -1.;
      xSize = sizeXPlane[0];
      ySize = bkLineThickness;
      break;
    case 'Y':
      yPos = -sizeYPlane[1] / 2.;
      xSize = bkLineThickness;
      ySize = sizeYPlane[1];
      break;
    case 'U':
      xPos = sizeUVPlane[0] / 2.;
      yPos = -bkLineUVShift;
      if (!bkOnJuraSide) {
        xPos *= -1.;
        yPos *= -1.;
      }
      xSize = sizeUVPlane[0];
      ySize = bkLineThickness;
      break;
    case 'V':
      xPos = -sizeUVPlane[0] / 2.;
      yPos = -bkLineUVShift;
      xSize = sizeUVPlane[0];
      ySize = bkLineThickness;
      break;
    default:
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
          __FILE__, "DCPlane::addBKLine: Unknown plane type.");
      return;
  }

  sens->addDeadZoneBox(positionVector + G4ThreeVector(xPos, yPos, 0), xSize / 2., ySize / 2.);
}

void DCPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  // most values here are copied from
  // detectors.20130329.dvcs_2012_plus.dat

  for (unsigned int i = 0; i < detIdentVec.size(); i++) {
    char c = detIdentVec.at(i).tbName.at(4);

    wire.id = dead.id = firstDetectorId + i;
    wire.tbName = dead.tbName = detIdentVec.at(i).tbName;
    wire.det = dead.det = detIdentVec.at(i).detName;
    wire.unit = dead.unit = detIdentVec.at(i).unit;
    wire.type = 11;

    wire.zSize = wireDistance;

    wire.xCen = dead.xCen = positionVector[0];
    wire.yCen = dead.yCen = positionVector[1];
    wire.zCen = dead.zCen = positionVector[2] + (int) (i + 1) / 2 * wireDistance
        + (int) i / 2 * layerDistance;
    wire.rotMatrix = TGEANT::ROT_0DEG;

    wire.pitch = pitch;
    wire.wireDist = wireDist.at(i);

    dead.sh = 5;
    dead.rotMatrix = (TGEANT::RoationMatrix) 12;
    dead.xSize = deadDiameter;
    dead.ySize = wireDistance;
    dead.zSize = 0;

    if (c == 'X') {
      wire.xSize = 2. * sizeXPlane[0];
      wire.ySize = 2. * sizeXPlane[1];
      wire.angle = 0;
      wire.nWires = nWiresXY;
    } else if (c == 'Y') {
      wire.xSize = 2. * sizeYPlane[0];
      wire.ySize = 2. * sizeYPlane[1];
      wire.angle = 90;
      wire.nWires = nWiresXY;
    } else if (c == 'U') {
      wire.xSize = 2.
          * (sizeUVPlane[0] + sizeUVPlane[1] * tan(angleUVPlane));
      wire.ySize = 2. * sizeUVPlane[1];
      wire.angle = -angleUVPlane / CLHEP::deg;
      wire.nWires = nWiresUV;
    } else if (c == 'V') {
      wire.xSize = 2
          * (sizeUVPlane[0] + sizeUVPlane[1] * tan(angleUVPlane));
      wire.ySize = 2. * sizeUVPlane[1];
      wire.angle = angleUVPlane / CLHEP::deg;
      wire.nWires = nWiresUV;
    }

    wireDet.push_back(wire);
    deadZone.push_back(dead);
  }
}

DC00Plane::~DC00Plane(void)
{
}
DC01Plane::~DC01Plane(void)
{
}
DC04Plane::~DC04Plane(void)
{
}
DC05Plane::~DC05Plane(void)
{
}

DCPlane::~DCPlane(void)
{
  if (plane_box != NULL)
    delete plane_box;
  if (plane_log != NULL)
    delete plane_log;
  if (mylar_box != NULL)
    delete mylar_box;
  if (mylar_log != NULL)
    delete mylar_log;
  if (graphite_box != NULL)
    delete graphite_box;
  if (graphite_log != NULL)
    delete graphite_log;
  if (sdX_box != NULL)
    delete sdX_box;
  if (sdY_box != NULL)
    delete sdY_box;
  if (sdU_para != NULL)
    delete sdU_para;
  if (sdV_para != NULL)
    delete sdV_para;

  if (xWire_pot != NULL)
    delete xWire_pot;
  if (xWire_sens != NULL)
    delete xWire_sens;
  if (yWire_pot != NULL)
    delete yWire_pot;
  if (yWire_sens != NULL)
    delete yWire_sens;
  if (uvWire_pot != NULL)
    delete uvWire_pot;
  if (uvWire_sens != NULL)
    delete uvWire_sens;

  if (xWire_pot_log != NULL)
    delete xWire_pot_log;
  if (xWire_sens_log != NULL)
    delete xWire_sens_log;
  if (yWire_pot_log != NULL)
    delete yWire_pot_log;
  if (yWire_sens_log != NULL)
    delete yWire_sens_log;
  if (uvWire_pot_log != NULL)
    delete uvWire_pot_log;
  if (uvWire_sens_log != NULL)
    delete uvWire_sens_log;

  if (xWire_rot != NULL)
    delete xWire_rot;
  if (yWire_rot != NULL)
    delete yWire_rot;
  if (uWire_rot != NULL)
    delete uWire_rot;
  if (vWire_rot != NULL)
    delete vWire_rot;

  for (unsigned int i = 0; i < sensitive_log.size(); i++)
    delete sensitive_log.at(i);
  sensitive_log.clear();
}
