#include "T4RichWall.hh"

void T4RichWall::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 462;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getRichWall()->size(); i++) {
    const T4SDetector* info = &settingsFile->getStructManager()->getRichWall()
        ->at(i);

    T4DetIdent ident;
    ident.tbName = info->name + "__";
    ident.detName = "RW" + info->name.substr(4, 2);
    ident.unit = strToInt(info->name.substr(3, 1));

    T4MDTPlane::PlaneTypeMDT planeType;
    if (info->name[4] == 'X')
      planeType = T4MDTPlane::DR_X;
    else
      planeType = T4MDTPlane::DR_Y;

    planes.push_back(new T4MDTPlane(ident, firstDetectorId++, planeType));
    planes.back()->setPosition(
        G4ThreeVector(info->position[0], info->position[1], info->position[2]));

    planes.back()->construct(world_log);
  }
}

void T4RichWall::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < planes.size(); i++)
    planes.at(i)->getWireDetDat(wireDet, deadZone);
}

T4RichWall::~T4RichWall(void)
{
  for (unsigned int i = 0; i < planes.size(); i++)
    delete planes.at(i);
  planes.clear();
}
