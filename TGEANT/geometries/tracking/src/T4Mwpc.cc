#include "T4Mwpc.hh"

void T4Mwpc::construct(G4LogicalVolume* world_log)
{
  int firstDetectorId = 501;

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getMWPC()->size(); i++) {
    const T4SDetector* det = &settingsFile->getStructManager()->getMWPC()->at(i);

    mwpcPlane.push_back(new T4MwpcPlane(det, firstDetectorId));
    mwpcPlane.back()->construct(world_log);
  }
}

T4MwpcPlane::T4MwpcPlane(const T4SDetector* _det, int& _firstDetectorId)
{
  det = _det;
  firstDetectorId = _firstDetectorId;

  namePlaneU = det->name;
  namePlaneU[4] = 'U';
  namePlaneV = det->name;
  namePlaneV[4] = 'V';
  namePlaneY = det->name;
  namePlaneY[4] = 'Y';

  // some default values (mostly for PA)
  angleUV = 10.14 * CLHEP::deg;
  zSize = 0.8 * CLHEP::cm;

  xSizeSD = 76.5 * CLHEP::cm;
  ySizeSD = 60. * CLHEP::cm;

  worldDimension[0] = 101. * CLHEP::cm;
  worldDimension[1] = 113. * CLHEP::cm;
  worldDimension[2] = 3.4 * CLHEP::cm;

  ySizeG10 = 72. * CLHEP::cm;
  xSizeGas = 89. * CLHEP::cm;
  zSupport_1 = 2.9 * CLHEP::cm;
  zSupport_2 = 2.65 * CLHEP::cm;
  ySizeG10_10 = 12.5 * CLHEP::cm;
  yPosG10_10 = 84.5 * CLHEP::cm;

  mwpcGasIndex = 0;
  motherShift = G4ThreeVector(0,0,0);
  xPosDeadZone = 0;

  if (det->name[1] == 'A') {
    nSd = 3;

    zPosX = 0;
    zPosY = 0;
    zPosU = -2.0 * zSize;
    zPosV = 2.0 * zSize;

    if (det->name[3] == '1' || det->name[3] == '2') {
      deadZoneRadius = 8.0 * CLHEP::cm;
    } else if (det->name[3] == '6') {
      deadZoneRadius = 11.0 * CLHEP::cm;
    } else {
      deadZoneRadius = 10.0 * CLHEP::cm;
    }

  } else if (det->name[1] == 'B') {
    if (det->name[4] == 'X') {
      nSd = 2;
      zPosX = -zSize;
      zPosU = zSize;
      motherShift = G4ThreeVector(0,0,zSize);
    } else if (det->name[4] == 'V') {
      nSd = 1;
      zPosV = zSize;
      motherShift = G4ThreeVector(0,0,-zSize);
    }

    deadZoneRadius = 11.0 * CLHEP::cm;
    xPosDeadZone = -30 * CLHEP::cm;
    ySizeSD = 46.5 * CLHEP::cm;
    ySizeG10 = 58.5 * CLHEP::cm;
    worldDimension[1] = 92.5 * CLHEP::cm;
    worldDimension[2] = 2.6 * CLHEP::cm;
    ySizeG10_10 = 6. * CLHEP::cm;
    yPosG10_10 = 66. * CLHEP::cm;
    zSupport_1 = 2.1 * CLHEP::cm;
    zSupport_2 = 1.85 * CLHEP::cm;

  } else if (det->name == "PS01X1__") {
    nSd = 4;
    motherShift = G4ThreeVector(0,0,-zSize);

    zPosX = zSize;
    zPosY = -3.0 * zSize;
    zPosU = -zSize;
    zPosV = 3.0 * zSize;

    worldDimension[0] = 101. * CLHEP::cm;
    worldDimension[1] = 113. * CLHEP::cm;
    worldDimension[2] = 4.2 * CLHEP::cm;

    ySizeG10 = 72. * CLHEP::cm;
    deadZoneRadius = 8.0 * CLHEP::cm;
    zSupport_1 = 3.7 * CLHEP::cm;
    zSupport_2 = 3.45 * CLHEP::cm;

  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4MwpcPlane:: Unknown MWPC TBname: " + det->name + ".");
  }

  _firstDetectorId += nSd;
  setPosition(det->position);
}

void T4MwpcPlane::construct(G4LogicalVolume* world_log)
{
  // air mother volume
  unsigned int motherIndex = log.size();
  box.push_back(new G4Box("mother_box", worldDimension[0], worldDimension[1], worldDimension[2]));
  log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "mother_log"));
  new G4PVPlacement(0, positionVector + motherShift, log.back(), "MWPC_air", world_log, 0, 0, checkOverlap);

  int nSd_ = nSd;
  if (nSd_ == 1)
    nSd_++;

  // G10 in mother
  unsigned int g10Index = log.size();
  box.push_back(new G4Box("g10_box", worldDimension[0], ySizeG10, zSize * nSd_));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10, "g10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MWPC_g10", log.at(motherIndex), 0, 0, checkOverlap);

  // mwpcGas in g10
  mwpcGasIndex = log.size();
  box.push_back(new G4Box("mwpcGas_box", xSizeGas, ySizeSD, zSize * nSd_));
  log.push_back(new G4LogicalVolume(box.back(), materials->mwpcGas, "mwpcGas_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MWPC_mwpcGas", log.at(g10Index), 0, 0, checkOverlap);

  switch (nSd) {
    case 4:
      buildY();
    case 3:
      buildX();
      buildU();
    case 1:
      buildV();
      break;
    case 2:
      buildX();
      buildU();
      break;
    default:
      break;
  }

  // deadZone in sens_n1, sens_n2 and sens_n3
  tubs.push_back(new G4Tubs("mwpcGas_tubs", 0, deadZoneRadius,  zSize, 0., 360.*CLHEP::deg));
  log.push_back(new G4LogicalVolume(tubs.back(), materials->mwpcGas, "mwpcGas_log"));
  for (unsigned int i = 0; i < sdIndex.size(); i++)
    new G4PVPlacement(0, G4ThreeVector(xPosDeadZone, 0, 0), log.back(), "MWPC_mwpcGas_deadZone", log.at(sdIndex.at(i)), 0, 0, checkOverlap);

  // G10_10 in mother
  box.push_back(new G4Box("g10_10_box", worldDimension[0], ySizeG10_10, zSize * nSd_));
  log.push_back(new G4LogicalVolume(box.back(), materials->g10_10, "g10_10_log"));
  new G4PVPlacement(0, G4ThreeVector(0, yPosG10_10, 0), log.back(), "MWPC_g10_10", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, -yPosG10_10, 0), log.back(), "MWPC_g10_10", log.at(motherIndex), 0, 0, checkOverlap);

  // Alu_1 in mother
  for (int i = 0; i < 4; i++) {
    unsigned int aluIndex = log.size();
    box.push_back(new G4Box("alu_1_box", 6. * CLHEP::cm, worldDimension[1], 0.5 * CLHEP::cm));
    log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_1_log"));

    G4double x = 95. * CLHEP::cm;
    G4double z = zSupport_1;
    if (i == 0) {
      x *= -1.;
      z *= -1.;
    } else if (i == 1) {
      z *= -1.;
    } else if (i == 2) {
      x *= -1.;
    }

    new G4PVPlacement(0, G4ThreeVector(x, 0, z), log.back(), "MWPC_alu_1", log.at(motherIndex), 0, 0, checkOverlap);

    if (det->name[1] == 'B') {
      box.push_back(new G4Box("alu_hole_box", 6. * CLHEP::cm, 21.5 * CLHEP::cm, 0.5 * CLHEP::cm));
      log.push_back(new G4LogicalVolume(box.back(), materials->air_noOptical, "alu_hole_log"));
      new G4PVPlacement(0, G4ThreeVector(0, 0, 0), log.back(), "MWPC_alu_hole", log.at(aluIndex), 0, 0, checkOverlap);
    }
  }

  // Alu_2 in mother
  box.push_back(new G4Box("alu_2_box", xSizeGas, 6. * CLHEP::cm, 0.25 * CLHEP::cm));
  log.push_back(new G4LogicalVolume(box.back(), materials->aluminium_noOptical, "alu_1_log"));
  new G4PVPlacement(0, G4ThreeVector(0, -66. * CLHEP::cm, -zSupport_2), log.back(), "MWPC_alu_5", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 66. * CLHEP::cm, -zSupport_2), log.back(), "MWPC_alu_6", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, -66. * CLHEP::cm, zSupport_2), log.back(), "MWPC_alu_7", log.at(motherIndex), 0, 0, checkOverlap);
  new G4PVPlacement(0, G4ThreeVector(0, 66. * CLHEP::cm, zSupport_2), log.back(), "MWPC_alu_8", log.at(motherIndex), 0, 0, checkOverlap);

  if (log.size() > 0)
    log.at(0)->SetVisAttributes(colour->invisible);
  for (unsigned int i = 1; i < log.size(); i++)
    log.at(i)->SetVisAttributes(colour->darkyellow);
}

void T4MwpcPlane::buildX()
{
  sdIndex.push_back(log.size());
  box.push_back(new G4Box("sens_X_box", xSizeSD, ySizeSD, zSize));
  log.push_back(new G4LogicalVolume(box.back(), materials->mwpcGas, "MWPC_sens_X_log"+det->name));
  new G4PVPlacement(0, G4ThreeVector(0, 0, zPosX), log.back(), "MWPC_sens_X", log.at(mwpcGasIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(det->name, 0, TGEANT::HIT, firstDetectorId + sdIndex.size() - 1));
}

void T4MwpcPlane::buildY()
{
  sdIndex.push_back(log.size());
  box.push_back(new G4Box("sens_Y_box", xSizeGas, 52.5 * CLHEP::cm, zSize));
  log.push_back(new G4LogicalVolume(box.back(), materials->mwpcGas, "MWPC_sens_Y_log"+namePlaneY));
  new G4PVPlacement(0, G4ThreeVector(0, 0, zPosY), log.back(), "MWPC_sens_Y", log.at(mwpcGasIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(namePlaneY, 0, TGEANT::HIT, firstDetectorId + sdIndex.size() - 1));
}

void T4MwpcPlane::buildU()
{
  sdIndex.push_back(log.size());
  para.push_back(new G4Para("sens_U_box", xSizeGas - ySizeSD * tan(angleUV), ySizeSD, zSize, -angleUV, 0, 0));
  log.push_back(new G4LogicalVolume(para.back(), materials->mwpcGas, "MWPC_sens_U_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, zPosU), log.back(), "MWPC_sens_U", log.at(mwpcGasIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(namePlaneU, 0, TGEANT::HIT, firstDetectorId + sdIndex.size() - 1));
}

void T4MwpcPlane::buildV()
{
  sdIndex.push_back(log.size());
  para.push_back(new G4Para("sens_V_box", xSizeGas - ySizeSD * tan(angleUV), ySizeSD, zSize, angleUV, 0, 0));
  log.push_back(new G4LogicalVolume(para.back(), materials->mwpcGas, "MWPC_sens_V_log"));
  new G4PVPlacement(0, G4ThreeVector(0, 0, zPosV), log.back(), "MWPC_sens_V", log.at(mwpcGasIndex), 0, 0, checkOverlap);
  log.back()->SetSensitiveDetector(
      new T4SensitiveDetector(namePlaneV, 0, TGEANT::HIT, firstDetectorId + sdIndex.size() - 1));
}

void T4Mwpc::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  for (unsigned int i = 0; i < mwpcPlane.size();i++)
    mwpcPlane.at(i)->getWireDetDat(wireDet, deadZone);
}

void T4MwpcPlane::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector wire;
  T4SDeadZone dead;
  wire.type = 1;
  wire.xSize = xSizeGas * 2.;
  wire.ySize = ySizeSD * 2.;
  wire.zSize = dead.ySize = zSize * 2.;
  wire.xCen = positionVector.getX();
  wire.yCen = dead.yCen = positionVector.getY();
  wire.rotMatrix = TGEANT::ROT_0DEG;
  wire.nWires = 760;
  dead.sh = 5;
  dead.xSize = deadZoneRadius * 2.;
  dead.zSize = 0;
  dead.rotMatrix = 12;
  dead.xCen = wire.xCen + xPosDeadZone;

  if (nSd == 1) {

    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = det->name;
    wire.det = dead.det = (string) "PB" + det->name[2] + det->name[3];
    wire.unit = dead.unit = 1;
    wire.zCen = dead.zCen = positionVector.getZ();
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = -10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

  } else if (nSd == 2) {

    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = det->name;
    wire.det = dead.det = (string) "PB" + det->name[2] + det->name[3];
    wire.unit = dead.unit = 1;
    wire.zCen = dead.zCen = positionVector.getZ();
    wire.pitch = 0.200000 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 0;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+1;
    wire.tbName = dead.tbName = namePlaneU;
    wire.unit = dead.unit = 2;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosU + motherShift.getZ();
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

  } else if (nSd == 3) { // PA

    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = det->name;
    wire.det = dead.det = (string) "PA" + det->name[2] + det->name[3];
    wire.unit = dead.unit = 1;
    wire.zCen = dead.zCen = positionVector.getZ();
    wire.pitch = 0.200000 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 0;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+1;
    wire.tbName = dead.tbName = namePlaneU;
    wire.unit = dead.unit = 2;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosU;
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+2;
    wire.tbName = dead.tbName = namePlaneV;
    wire.unit = dead.unit = 3;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosV;
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = -10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

  } else if (nSd == 4) {

    wire.id = dead.id = firstDetectorId;
    wire.tbName = dead.tbName = namePlaneY;
    wire.det = dead.det = (string) "PS" + det->name[2] + det->name[3];
    wire.unit = dead.unit = 1;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosY + motherShift.getZ();
    wire.nWires = 520;
    wire.pitch = 0.200000 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 90;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+1;
    wire.tbName = dead.tbName = det->name;
    wire.unit = dead.unit = 2;
    wire.zCen = dead.zCen = positionVector.getZ();
    wire.nWires = 760;
    wire.pitch = 0.200000 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 0;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+2;
    wire.tbName = dead.tbName = namePlaneU;
    wire.unit = dead.unit = 3;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosU + motherShift.getZ();
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = 10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);

    wire.id = dead.id = firstDetectorId+3;
    wire.tbName = dead.tbName = namePlaneV;
    wire.unit = dead.unit = 4;
    wire.zCen = dead.zCen = positionVector.getZ() + zPosV + motherShift.getZ();
    wire.pitch = 0.196880 * CLHEP::cm;
    wire.wireDist = -1.0 * (wire.nWires - 1) / 2. * wire.pitch;
    wire.angle = -10.140;
    wireDet.push_back(wire);
    deadZone.push_back(dead);
  }
}

T4Mwpc::~T4Mwpc(void)
{
  for (unsigned int i = 0; i < mwpcPlane.size();i++)
    delete mwpcPlane.at(i);
  mwpcPlane.clear();
}

T4MwpcPlane::~T4MwpcPlane(void)
{
  for (unsigned int i = 0; i < box.size();i++)
    delete box.at(i);
  box.clear();

  for (unsigned int i = 0; i < tubs.size();i++)
    delete tubs.at(i);
  tubs.clear();

  for (unsigned int i = 0; i < para.size();i++)
    delete para.at(i);
  para.clear();

  for (unsigned int i = 0; i < log.size();i++)
    delete log.at(i);
  log.clear();
}
