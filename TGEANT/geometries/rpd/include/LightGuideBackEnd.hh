#ifndef LIGHTGUIDEBACKEND_HH_
#define LIGHTGUIDEBACKEND_HH_

#include "T4Globals.hh"

#include "G4RotationMatrix.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4IntersectionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4LogicalBorderSurface.hh"

class LightGuideBackEnd
{
  public:
    LightGuideBackEnd(void);
    virtual ~LightGuideBackEnd(void);

    void setDimensions(G4double sci_x, G4double sci_y, G4double pmt_radius,
        G4double length, G4double lgRadius);
    void setRotation(const CLHEP::HepRotation& _rotationMatrix)
      {rotationMatrix = _rotationMatrix;};
    void setAlignment(TGEANT::Alignment _alignment)
      {alignment = _alignment;};
    virtual G4ThreeVector getPmtPosition(G4double pmtLenght) = 0;
    void useOpticalPhysics(void) {useOptical = true;}

    G4LogicalVolume* getVolume(void) {return plastic_log;}

  protected:
    G4bool useOptical;
    TGEANT::Alignment alignment;
    G4double sci_x;
    G4double sci_y;
    G4double pmt_radius;
    G4double length;
    G4double lgRadius;
    CLHEP::HepRotation rotationMatrix;
    CLHEP::HepRotation* rotationTubs;

    G4double rotZ;

    G4Trd* plastic_trd;
    G4Trd* alu_trd;
    G4Trd* airGap_trd;
    G4Trd* lightguide_trd;

    G4Cons* plastic_cons;
    G4Cons* alu_cons;
    G4Cons* airGap_cons;
    G4Cons* lightguide_cons;

    G4Tubs* plastic_tubs;
    G4Tubs* alu_tubs;
    G4Tubs* airGap_tubs;
    G4Tubs* lightguide_tubs;

    G4IntersectionSolid* plastic_intersection;
    G4IntersectionSolid* alu_intersection;
    G4IntersectionSolid* airGap_intersection;
    G4IntersectionSolid* lightguide_intersection;

    G4UnionSolid* plastic_union;
    G4UnionSolid* alu_union;
    G4UnionSolid* airGap_union;
    G4UnionSolid* lightguide_union;

    G4LogicalVolume* plastic_log;
    G4LogicalVolume* alu_log;
    G4LogicalVolume* airGap_log;
    G4LogicalVolume* lightguide_log;

    G4PVPlacement* plastic_phys;
    G4PVPlacement* alu_phys;
    G4PVPlacement* airGap_phys;
    G4PVPlacement* lightguide_phys;

    G4LogicalBorderSurface* plexiAir;
    G4LogicalBorderSurface* airPlexi;
    G4LogicalBorderSurface* airAl;
};

#endif /* LIGHTGUIDEBACKEND_HH_ */
