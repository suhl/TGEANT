#ifndef T4PHOTOMULTIPLIER_HH_
#define T4PHOTOMULTIPLIER_HH_

#include "T4BaseDetector.hh"

#include "G4RotationMatrix.hh"
#include "G4Tubs.hh"

class T4PhotoMultiplier : public T4BaseDetector
{
  public:
    T4PhotoMultiplier(void);
    virtual ~T4PhotoMultiplier(void);

    void construct(G4LogicalVolume*);

    void setAlignment(TGEANT::Alignment _alignment) {alignment = _alignment;}
    void setDimensions(G4double _length, G4double _radius) {length = _length; radius = _radius;}
    void setRotation(const CLHEP::HepRotation& _rotationMatrix) {rotationMatrix = _rotationMatrix;}
    void setPmtType(TGEANT::PmtType _pmtType) {pmtType = _pmtType; usePmtType();}
    void setSensitiveDetector(G4String _name, G4int _channelNo) {name = _name; channelNo = _channelNo;}
    void rotateX(G4double angle) {rotationMatrix.rotateX(angle);}
    void rotateY(G4double angle) {rotationMatrix.rotateY(angle);}
    void rotateZ(G4double angle) {rotationMatrix.rotateZ(angle);}

    G4double getPmtLength(TGEANT::PmtType);
    G4double getPmtLength(void) {return length;}
    G4double getPmtRadius(TGEANT::PmtType);
    G4double getTT(TGEANT::PmtType);
    G4double getTTS(TGEANT::PmtType);

    void useOpticalPhysics(void) {
      aluminiumMat = materials->aluminium_optical;
      vacuumMat = materials->vacuum_optical;
      glassMat = materials->glass_optical;
      bialkaliMat = materials->bialkali_optical;}

    G4LogicalVolume* getVolume(void) {return housing_log;}

  private:
    void usePmtType(void);

    TGEANT::Alignment alignment;
    TGEANT::PmtType pmtType;
    G4double length;
    G4double radius;
    G4double thickness;
    CLHEP::HepRotation rotationMatrix;
    G4String name;
    G4int channelNo;

    G4Material* aluminiumMat;
    G4Material* vacuumMat;
    G4Material* glassMat;
    G4Material* bialkaliMat;

    // CAMERA Ring B
    G4double lengthET9823B;
    G4double radiusET9823B;
    G4double thicknessET9823B;
    G4double TT_ET9823B;
    G4double TTS_ET9823B;

    // CAMERA Ring A
    G4double lengthR10533;
    G4double radiusR10533;
    G4double thicknessR10533;
    G4double TT_R10533;
    G4double TTS_R10533;

    // OLGA module
    G4double lengthXP2050;
    G4double radiusXP2050;
    G4double thicknessXP2050;
    // MAINZ module
    G4double lengthEMI9236KB;
    G4double radiusEMI9236KB;
    G4double thicknessEMI9236KB;

    G4Tubs* housing_tube;
    G4Tubs* vacuum_tube;
    G4Tubs* window_tube;
    G4Tubs* photocathode_tube;

    G4LogicalVolume* housing_log;
    G4LogicalVolume* vacuum_log;
    G4LogicalVolume* window_log;
    G4LogicalVolume* photocathode_log;

    G4VPhysicalVolume* housing_phys;
    G4VPhysicalVolume* vacuum_phys;
    G4VPhysicalVolume* window_phys;
    G4VPhysicalVolume* photocathode_phys;

    G4LogicalBorderSurface* vacAl;
    G4LogicalBorderSurface* windowAl;
    G4LogicalBorderSurface* bialkaliAl;
    G4LogicalBorderSurface* windowVac;
    G4LogicalBorderSurface* vacWindow;
    G4LogicalBorderSurface* vacBialkali;
};

#endif /* T4PHOTOMULTIPLIER_HH_ */
