#ifndef RINGAELEMENT_HH_
#define RINGAELEMENT_HH_

#include "T4BaseDetector.hh"
#include "RingElementBackEnd.hh"
#include "LightGuideRingA.hh"
#include "LightGuideRingB.hh"
#include "ScintillatorGenericTrap.hh"

class RingAElement : public T4BaseDetector, public RingElementBackEnd
{
  public:
    RingAElement(void);
    virtual ~RingAElement(void);

    void construct(G4LogicalVolume*);
    void setSlope(G4double _slope) {slope = _slope;}
    void setRotAngle(G4double _angle) {angle = _angle;}

  private:
    LightGuideRingB* lightGuide_up;
    LightGuideRingA* lightGuide_down;

    // ringA calibration variables
    G4double slope;
    G4double angle;
    CLHEP::HepRotation rotationMatrixUp;
    CLHEP::HepRotation rotationMatrixDn;
};

#endif /* RINGAELEMENT_HH_ */
