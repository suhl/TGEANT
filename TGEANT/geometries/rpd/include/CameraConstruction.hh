#ifndef CAMERACONSTRUCTION_HH_
#define CAMERACONSTRUCTION_HH_

#include "T4BaseDetector.hh"
#include "RingBElement.hh"
#include "RingAElement.hh"
#include "CameraAmbiente.hh"

class CameraConstruction : public T4BaseDetector
{
  public:
    CameraConstruction(void);
    virtual ~CameraConstruction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&, std::vector<T4SDeadZone>&);

    G4double getAirGap(void) {return airGap;}
    G4double getAluFoil(void) {return aluFoil;}
    G4double getPlasticFoil(void) {return plasticFoil;}
    G4double getLenghtA(void) {return lenghtA;}
    G4double getLenghtB(void) {return lenghtB;}
    G4double getRadiusA(void) {return radiusA;}
    G4double getRadiusB(void) {return radiusB;}

  private:
    void positioningClassic(void);
    void positioningCalibrationFile(void);
    T4SRPD* ringA;
    T4SRPD* ringB;
    G4int detectorId_A;
    G4int detectorId_B;
    G4String tbName_A;
    G4String tbName_B;

    const static G4int nRing = 24;

    G4double ringAvsBRotation;

    G4ThreeVector positionRingA;
    G4ThreeVector positionRingB;
    G4ThreeVector positionSciA[nRing];
    G4ThreeVector positionSciB[nRing];
    CLHEP::HepRotation* rotationSciA[nRing];
    CLHEP::HepRotation* rotationSciB[nRing];
    RingAElement* ringAElement[nRing];
    RingBElement* ringBElement[nRing];
    CameraAmbiente* cameraAmbiente;

    G4double lenghtA;
    G4double lenghtB;
    G4double radiusA;
    G4double radiusB;

    G4double airGap;
    G4double aluFoil;
    G4double plasticFoil;

    G4double slope_A[nRing];
    G4double rotAngle_A[nRing];
};

#endif /* CAMERACONSTRUCTION_HH_ */
