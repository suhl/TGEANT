#ifndef LIGHTGUIDERINGA_HH_
#define LIGHTGUIDERINGA_HH_

#include "T4BaseDetector.hh"
#include "LightGuideBackEnd.hh"

class LightGuideRingA : public T4BaseDetector, public LightGuideBackEnd
{
  public:
    LightGuideRingA(void);
    virtual ~LightGuideRingA(void) {};

    void construct(G4LogicalVolume*);

    void setAngle(G4double _angle)
      {angle = _angle;};
    G4ThreeVector getPmtPosition(G4double pmtLenght);

  private:
    G4double angle;
};

#endif /* LIGHTGUIDERINGA_HH_ */
