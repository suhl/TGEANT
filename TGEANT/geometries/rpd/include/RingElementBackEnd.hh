#ifndef RINGELEMENTBACKEND_HH_
#define RINGELEMENTBACKEND_HH_

#include "ScintillatorTrd.hh"
#include "T4PhotoMultiplier.hh"

#include "G4RotationMatrix.hh"

class RingElementBackEnd
{
  public:
    RingElementBackEnd(void);
    virtual ~RingElementBackEnd(void);

    void setRotation(const CLHEP::HepRotation& _rotationMatrix)
      {rotationMatrix = _rotationMatrix;}
    void setDetectorName(G4String _name)
      {name = _name;}
    void setChannelNo(G4int _channelNo)
      {channelNo = _channelNo;}
    void setDetectorId(G4int _detectorId)
      {detectorId = _detectorId;}
    void useOpticalPhysics(void)
      {useOptical = true;}

    G4double getLenghtX(void)
      {return sci_x_long;}
    G4double getLenghtY(void)
      {return sci_y;}

  protected:
    CLHEP::HepRotation rotationMatrix;
    G4String name;
    G4int channelNo;
    G4int detectorId;
    G4bool useOptical;

    G4double sci_x_short;
    G4double sci_x_long;
    G4double sci_y;
    G4double sci_z;
    G4double pmtRadius;
    G4double pmtLength;
    G4double outerLgRadius;
    G4double upperLgLength;

    ScintillatorTrd* scintillatorTrd;
    T4PhotoMultiplier* photoMultiplier_up;
    T4PhotoMultiplier* photoMultiplier_down;
};

#endif /* RINGELEMENTBACKEND_HH_ */
