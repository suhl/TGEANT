#ifndef SCINTILLATORTRD_HH_
#define SCINTILLATORTRD_HH_

#include "T4BaseDetector.hh"

#include "G4Trd.hh"
#include "G4Trap.hh"
#include "G4RotationMatrix.hh"

class ScintillatorTrd : public T4BaseDetector
{
  public:
    ScintillatorTrd(void);
    virtual ~ScintillatorTrd(void);

    void construct(G4LogicalVolume*);

    void setDimensions(G4double sci_x_short, G4double sci_x_long,
        G4double sci_y, G4double sci_z);
    void setName(G4String _name)
      {name = _name;}
    void setChannelNo(G4int _channelNo)
      {channelNo = _channelNo;}
    void setDetectorId(G4int _detectorId)
      {detectorId = _detectorId;}
    void setRotation(const CLHEP::HepRotation& _rotationMatrix)
      {rotationMatrix = _rotationMatrix;}
    void deactivateCap(void)
      {useCap = false;}
    void useOpticalPhysics(void) {
      bc408Mat = materials->bc408_optical;
      plexiglassMat = materials->plexiglass_optical;
      aluminiumMat = materials->aluminium_optical;
      airMat = materials->air_optical;}

    G4LogicalVolume* getVolume(void)
      {return sci_world_log;}

  protected:
    G4String name;
    G4int channelNo;
    G4int detectorId;
    G4bool useCap;
    void createSensitiveDetector(G4String name, G4int channelNo, G4int detectorId);

    G4double sci_x_short;
    G4double sci_x_long;
    G4double sci_y;
    G4double sci_z;
    CLHEP::HepRotation rotationMatrix;
    CLHEP::HepRotation* capRotation[4];

    G4Material* bc408Mat;
    G4Material* plexiglassMat;
    G4Material* aluminiumMat;
    G4Material* airMat;

    G4LogicalVolume* sci_world_log;
    G4LogicalVolume* plastic_log;
    G4LogicalVolume* housing_log;
    G4LogicalVolume* airGap_log;
    G4LogicalVolume* scintillator_log;

    G4PVPlacement* sci_world_phys;
    G4PVPlacement* plastic_phys;
    G4PVPlacement* housing_phys;
    G4PVPlacement* airGap_phys;
    G4PVPlacement* scintillator_phys;

    G4Trap* plastic_cap_trap[4];
    G4Trap* housing_cap_trap[4];
    G4Trap* airGap_cap_trap[4];

    G4LogicalVolume* plastic_cap_log[4];
    G4LogicalVolume* housing_cap_log[4];
    G4LogicalVolume* airGap_cap_log[4];

    G4PVPlacement* plastic_cap_phys[4];
    G4PVPlacement* housing_cap_phys[4];
    G4PVPlacement* airGap_cap_phys[4];

    G4LogicalBorderSurface* sciAir;
    G4LogicalBorderSurface* airSci;
    G4LogicalBorderSurface* airAl;
    G4LogicalBorderSurface* sciAirGap;
    G4LogicalBorderSurface* airGapSci;

    G4LogicalBorderSurface* sciAirCap[4];
    G4LogicalBorderSurface* airSciCap[4];
    G4LogicalBorderSurface* airAlCap[4];

  private:
    G4Trd* sci_world_trd;
    G4Trd* plastic_trd;
    G4Trd* housing_trd;
    G4Trd* airGap_trd;
    G4Trd* scintillator_trd;
};

#endif /* SCINTILLATORTRD_HH_ */
