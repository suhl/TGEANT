#ifndef RPDCONSTRUCTION_HH_
#define RPDCONSTRUCTION_HH_

#include "T4BaseDetector.hh"

#include "ScintillatorTrd.hh"
#include "LGFishtail.hh"
#include "LightGuideRingA.hh"
#include "T4PhotoMultiplier.hh"

class RPDElement;
class ConicalCryostat;

class RPDConstruction : public T4BaseDetector
{
  public:
    RPDConstruction(void);
    virtual ~RPDConstruction(void);

    void construct(G4LogicalVolume*);
    void getWireDetDat(std::vector<T4SWireDetector>&,
        std::vector<T4SDeadZone>&);

  private:
    T4SRPD* rpdA;
    T4SRPD* rpdB;
    G4int detectorId_A;
    G4int detectorId_B;
    G4String tbName_A;
    G4String tbName_B;

    const static G4int nRingA = 12;
    const static G4int nRingB = 24;

    RPDElement* ringA[nRingA];
    RPDElement* ringB[nRingB];

    G4double radiusA;
    G4double sciA_x;
    G4double sciA_y;
    G4double sciA_z;
    G4double lgLengthA;
    G4double pmtRadiusA;
    G4double pmtLenghtA;
    G4ThreeVector positionSciA[nRingA];
    G4RotationMatrix* rotationSciA[nRingA];

    G4double radiusB;
    G4double sciB_x;
    G4double sciB_y;
    G4double sciB_z;
    G4double lgLengthB;
    G4double pmtRadiusB;
    G4double pmtLenghtB;
    G4ThreeVector positionSciB[nRingB];
    G4RotationMatrix* rotationSciB[nRingB];

    G4double ambienteLength;
    G4double ambienteRadius;
    G4double dimensionAmbiente[5];
    G4Tubs* ambiente_tube;
    G4LogicalVolume* ambiente_log;
    G4VPhysicalVolume* ambiente_phys;

		ConicalCryostat* conicalCryostat;
};

class RPDElement
{
  public:
    RPDElement(void);
    virtual ~RPDElement(void);

    void constructA(const CLHEP::HepRotation&, G4ThreeVector positionVector,
        G4String name, G4int channelNo, G4int detectorId, G4double sci_x,
        G4double sci_y, G4double sci_z, G4double lgLenght, G4double pmtRadius,
        G4double pmtLenght, G4bool useOpticalPhysics, G4LogicalVolume*);

    void constructB(const CLHEP::HepRotation&, G4ThreeVector positionVector,
        G4String name, G4int channelNo, G4int detectorId, G4double sci_x,
        G4double sci_y, G4double sci_z, G4double lgLenght, G4double pmtRadius,
        G4double pmtLenght, G4bool useOpticalPhysics, G4LogicalVolume*);

  private:
    ScintillatorTrd* scintillatorTrd;
    LGFishtail* fishtail_up;
    LGFishtail* fishtail_down;
    LightGuideRingA* lgRingA_up;
    LightGuideRingA* lgRingA_down;
    T4PhotoMultiplier* photoMultiplier_up;
    T4PhotoMultiplier* photoMultiplier_down;
};

class ConicalCryostat : public T4BaseDetector
{
	public:
		ConicalCryostat(const G4ThreeVector rpdPosition);
		~ConicalCryostat(void);
		virtual void construct (G4LogicalVolume* world_log);

	private:
		G4ThreeVector rpdPos;

		G4double coneInnerRadius_up;
		G4double coneInnerRadius_down;
		G4double coneOuterRadius_up;
		G4double coneOuterRadius_down;
		G4double coneLength;

		G4double upstreamWindowRadius;
		G4double upstreamWindowThickness;
		G4double upstreamWindowPosition;
		G4double downstreamWindowRadius;
		G4double downstreamWindowThickness;
		G4double downstreamWindowPosition;

		G4Cons* cone;
		G4LogicalVolume* cone_log;
		G4VPhysicalVolume* cone_phys;

		G4Tubs* upstreamWindow;
		G4LogicalVolume* upstreamWindow_log;
		G4PVPlacement* upstreamWindow_phys;

		G4Tubs* downstreamWindow;
		G4LogicalVolume* downstreamWindow_log;
		G4PVPlacement* downstreamWindow_phys;
};

#endif /* RPDCONSTRUCTION_HH_ */
