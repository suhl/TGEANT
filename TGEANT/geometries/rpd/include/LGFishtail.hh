#ifndef LGFISHTAIL_HH_
#define LGFISHTAIL_HH_

#include "T4BaseDetector.hh"

#include "G4Trd.hh"
#include "G4Cons.hh"
#include "G4IntersectionSolid.hh"

class LGFishtail : public T4BaseDetector
{
  public:
    LGFishtail(void);
    virtual ~LGFishtail(void);

    void construct(G4LogicalVolume*);

    void setAlignment(TGEANT::Alignment);
    void setDimensions(G4double sci_x, G4double sci_y, G4double pmt_radius,
        G4double length, G4double airGap = 0.1 * CLHEP::mm,
        G4double aluFoil = 0.02 * CLHEP::mm,
        G4double plasticFoil = 0.20 * CLHEP::mm);

    void setRotation(const CLHEP::HepRotation&);
    G4ThreeVector getPmtPosition(G4double pmtLenght);
    void useOpticalPhysics(void) {
      plexiglassMat = materials->plexiglass_optical;
      aluminiumMat = materials->aluminium_optical;
      airMat = materials->air_optical;}

  private:
    G4double sci_x;
    G4double sci_y;
    G4double pmt_radius;
    G4double length;
    TGEANT::Alignment alignment;
    CLHEP::HepRotation rotationMatrix;

    G4Material* plexiglassMat;
    G4Material* aluminiumMat;
    G4Material* airMat;

    G4double airGap;
    G4double aluFoil;
    G4double plasticFoil;

    G4Trd* plastic_trd;
    G4Trd* housing_trd;
    G4Trd* airGap_trd;
    G4Trd* lightguide_trd;

    G4Cons* plastic_cons;
    G4Cons* housing_cons;
    G4Cons* airGap_cons;
    G4Cons* lightguide_cons;

    G4IntersectionSolid* plastic_intersection;
    G4IntersectionSolid* housing_intersection;
    G4IntersectionSolid* airGap_intersection;
    G4IntersectionSolid* lightguide_intersection;

    G4LogicalVolume* plastic_log;
    G4LogicalVolume* housing_log;
    G4LogicalVolume* airGap_log;
    G4LogicalVolume* lightguide_log;

    G4PVPlacement* plastic_phys;
    G4PVPlacement* housing_phys;
    G4PVPlacement* airGap_phys;
    G4PVPlacement* lightguide_phys;

    G4LogicalBorderSurface* plexiAir;
    G4LogicalBorderSurface* airPlexi;
    G4LogicalBorderSurface* airAl;
};

#endif /* LGFISHTAIL_HH_ */
