#ifndef CAMERAAMBIENTE_HH_
#define CAMERAAMBIENTE_HH_

#include "T4BaseDetector.hh"

#include "RingAElement.hh"
#include "RingBElement.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"

class CameraAmbiente : public T4BaseDetector
{
  public:
    CameraAmbiente(void);
    virtual ~CameraAmbiente(void);

    void buildA(G4bool _useA) {useA = _useA;}
    void buildB(G4bool _useB) {useB = _useB;}

    void setPositionA(G4ThreeVector _positionA) {positionA = _positionA;}
    void setPositionB(G4ThreeVector _positionB) {positionB = _positionB;}

    void construct(G4LogicalVolume*);

  private:
    G4bool useA;
    G4bool useB;

    G4ThreeVector positionA;
    G4ThreeVector positionB;

    G4double lenghtB_;
    G4double radiusB_;

    G4double groundHeight_;

    G4double dimensionSciHolderA_inner[3];
    G4ThreeVector positionSciHolderA[5];
    G4Tubs* sciHolderA_inner_tubs[2];
    G4LogicalVolume* sciHolderA_inner_log[2];

    G4double dimensionSciHolderA_outer[3];
    G4Tubs* sciHolderA_outer_tubs[5];
    G4LogicalVolume* sciHolderA_outer_log[5];

    G4double dimensionLgHolderAu[3];
    G4ThreeVector positionLgHolderAu;
    G4Tubs* lgHolderAu_tubs;
    G4LogicalVolume* lgHolderAu_log;

    G4double dimensionLgHolderAd[4][5];
    G4ThreeVector positionLgHolderAd[4];
    G4Cons* lgHolderAd_cons[4];
    G4LogicalVolume* lgHolderAd_log[4];

    G4double dimensionSciHolderB[3];
    G4ThreeVector positionSciHolderB[5];
    G4Tubs* sciHolderB_tubs[2];
    G4LogicalVolume* sciHolderB_log[2];

    G4double dimensionSciHolderB_2[3];
    G4Tubs* sciHolderB_2_tubs;
    G4LogicalVolume* sciHolderB_2_log;

    G4double dimensionLgHolderB[3];
    G4ThreeVector positionLgHolderB[2];
    G4Tubs* lgHolderB_tubs;
    G4LogicalVolume* lgHolderB_log;

    CLHEP::HepRotation* rotationCrossbarB[8];
    G4double dimensionCrossbarB[3];
    G4ThreeVector positionCrossbarB[8];
    G4Box* crossbarB_box;
    vector<G4SubtractionSolid*> crossbar_sub;
    G4LogicalVolume* crossbarB_log;

    G4double dimensionBoschX[3];
    G4ThreeVector positionBoschX[5];
    G4Box* boschX_box[5];
    G4LogicalVolume* boschX_log[5];

    G4double dimensionBoschY[3];
    G4ThreeVector positionBoschY[4];
    G4Box* boschY_box;
    G4LogicalVolume* boschY_log;

    G4double dimensionBoschZ[3];
    G4ThreeVector positionBoschZ[6];
    G4Box* boschZ_box[6];
    G4LogicalVolume* boschZ_log[6];

    G4ThreeVector positionAnglePiece[16];
    CLHEP::HepRotation* rotationAnglePiece[2];
    G4double dimensionBoschAnglePiece[3];
    G4double dimensionBoxAnglePiece[3];

    G4Box* boschAnglePiece_box;
    G4Box* boxAnglePiece_box;
    G4IntersectionSolid* anglePiece_intersection[2];
    G4LogicalVolume* anglePiece_log[2];

    G4ThreeVector positionAnglePieceBase[8];
    CLHEP::HepRotation* rotationAnglePieceBase[4];
    G4double dimensionBoschAnglePieceBase[3];
    G4double dimensionBoxAnglePieceBase[3];
    G4double dimensionBox2AnglePieceBase[3];

    G4Box* boschAnglePieceBase_box;
    G4Box* boxAnglePieceBase_box[2];
    G4IntersectionSolid* anglePieceBase_intersection[8];
    G4LogicalVolume* anglePieceBase_log[8];

    G4double dimensionLittleBox;
    G4ThreeVector positionLittleBox[8];
    G4Box* littleBox_box;
    G4LogicalVolume* littleBox_log;
};

#endif /* CAMERAAMBIENTE_HH_ */
