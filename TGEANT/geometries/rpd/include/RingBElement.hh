#ifndef RINGBELEMENT_HH_
#define RINGBELEMENT_HH_

#include "T4BaseDetector.hh"
#include "RingElementBackEnd.hh"
#include "LightGuideRingB.hh"

class RingBElement : public T4BaseDetector, public RingElementBackEnd
{
  public:
    RingBElement(void);
    virtual ~RingBElement(void);

    void construct(G4LogicalVolume*);

    void setDimensions(G4double sci_x_short, G4double sci_x_long, G4double sci_y,
                       G4double sci_z, G4double pmtRadius, G4double outerLgRadius,
                       G4double upperLgLenght, G4double pmtLenght, G4bool isSciTrd);
    void setLengthZ(G4double _sci_z) {sci_z = _sci_z;}

  private:
    G4bool isSciTrd;
    LightGuideRingB* lightGuide_up;
    LightGuideRingB* lightGuide_down;
};

#endif /* RINGBELEMENT_HH_ */
