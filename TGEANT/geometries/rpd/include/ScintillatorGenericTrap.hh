#ifndef SCINTILLATORGENERICTRAP_HH_
#define SCINTILLATORGENERICTRAP_HH_

#include "ScintillatorTrd.hh"

#include "G4GenericTrap.hh"

class ScintillatorGenericTrap : public ScintillatorTrd
{
  public:
    ScintillatorGenericTrap(void);
    virtual ~ScintillatorGenericTrap(void);

    void construct(G4LogicalVolume*);
    void setSlope(G4double _slope) {slope = _slope;}
    void setRotAngle(G4double _angle) {angle = _angle;}

  private:
    G4GenericTrap* sci_world_trap;
    G4GenericTrap* plastic_trap;
    G4GenericTrap* housing_trap;
    G4GenericTrap* airGap_trap;
    G4GenericTrap* scintillator_trap;

    G4double slope;
    G4double angle;
};

#endif /* SCINTILLATORGENERICTRAP_HH_ */
