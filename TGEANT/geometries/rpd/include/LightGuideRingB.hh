#ifndef LIGHTGUIDERINGB_HH_
#define LIGHTGUIDERINGB_HH_

#include "T4BaseDetector.hh"
#include "LightGuideBackEnd.hh"

class LightGuideRingB : public T4BaseDetector, public LightGuideBackEnd
{
  public:
    LightGuideRingB(void);
    virtual ~LightGuideRingB(void) {};

    void construct(G4LogicalVolume*);

    G4ThreeVector getPmtPosition(G4double pmtLenght);
};

#endif /* LIGHTGUIDERINGB_HH_ */
