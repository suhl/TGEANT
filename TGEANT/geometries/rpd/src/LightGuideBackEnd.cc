#include "LightGuideBackEnd.hh"

LightGuideBackEnd::LightGuideBackEnd(void)
{
  useOptical = false;
  sci_x = 0;
  sci_y = 0;
  pmt_radius = 0;
  length = 0;
  lgRadius = 0;
  alignment = TGEANT::UP;
  rotZ = 0;

  rotationTubs = NULL;

  plastic_trd = NULL;
  alu_trd = NULL;
  airGap_trd = NULL;
  lightguide_trd = NULL;

  plastic_cons = NULL;
  alu_cons = NULL;
  airGap_cons = NULL;
  lightguide_cons = NULL;

  plastic_tubs = NULL;
  alu_tubs = NULL;
  airGap_tubs = NULL;
  lightguide_tubs = NULL;

  plastic_intersection = NULL;
  alu_intersection = NULL;
  airGap_intersection = NULL;
  lightguide_intersection = NULL;

  plastic_union = NULL;
  alu_union = NULL;
  airGap_union = NULL;
  lightguide_union = NULL;

  plastic_log = NULL;
  alu_log = NULL;
  airGap_log = NULL;
  lightguide_log = NULL;

  plastic_phys = NULL;
  alu_phys = NULL;
  airGap_phys = NULL;
  lightguide_phys = NULL;

  plexiAir = NULL;
  airPlexi = NULL;
  airAl = NULL;
}

LightGuideBackEnd::~LightGuideBackEnd(void)
{
  if (rotationTubs != NULL)
    delete rotationTubs;

  if (plastic_trd != NULL)
    delete plastic_trd;
  if (alu_trd != NULL)
    delete alu_trd;
  if (airGap_trd != NULL)
    delete airGap_trd;
  if (lightguide_trd != NULL)
    delete lightguide_trd;

  if (plastic_cons != NULL)
    delete plastic_cons;
  if (alu_cons != NULL)
    delete alu_cons;
  if (airGap_cons != NULL)
    delete airGap_cons;
  if (lightguide_cons != NULL)
    delete lightguide_cons;

  if (plastic_tubs != NULL)
    delete plastic_tubs;
  if (alu_tubs != NULL)
    delete alu_tubs;
  if (airGap_tubs != NULL)
    delete airGap_tubs;
  if (lightguide_tubs != NULL)
    delete lightguide_tubs;

  if (plastic_intersection != NULL)
    delete plastic_intersection;
  if (alu_intersection != NULL)
    delete alu_intersection;
  if (airGap_intersection != NULL)
    delete airGap_intersection;
  if (lightguide_intersection != NULL)
    delete lightguide_intersection;

  if (plastic_union != NULL)
    delete plastic_union;
  if (alu_union != NULL)
    delete alu_union;
  if (airGap_union != NULL)
    delete airGap_union;
  if (lightguide_union != NULL)
    delete lightguide_union;

  if (plastic_log != NULL)
    delete plastic_log;
  if (alu_log != NULL)
    delete alu_log;
  if (airGap_log != NULL)
    delete airGap_log;
  if (lightguide_log != NULL)
    delete lightguide_log;

  if (plexiAir != NULL)
    delete plexiAir;
  if (airPlexi != NULL)
    delete airPlexi;
  if (airAl != NULL)
    delete airAl;
}

void LightGuideBackEnd::setDimensions(G4double _sci_x, G4double _sci_y,
    G4double _pmt_radius, G4double _length, G4double _lgRadius)
{
  sci_x = _sci_x;
  sci_y = _sci_y;
  pmt_radius = _pmt_radius;
  length = _length;
  lgRadius = _lgRadius;
}
