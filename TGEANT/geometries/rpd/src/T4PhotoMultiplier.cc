#include "T4PhotoMultiplier.hh"

T4PhotoMultiplier::T4PhotoMultiplier(void)
{
  aluminiumMat = materials->aluminium_noOptical;
  vacuumMat = materials->vacuum_noOptical;
  glassMat = materials->glass_noOptical;
  bialkaliMat = materials->bialkali_noOptical;

  name = "";
  channelNo = 0;
  length = 10.0 * CLHEP::cm;
  radius = 10.0 * CLHEP::cm;
  thickness = 1.0 * CLHEP::cm;
  alignment = TGEANT::UP;
  positionVector = G4ThreeVector(0, 0, 0);

  lengthET9823B = 25.9 / 2 * CLHEP::cm;
  radiusET9823B = 11.0 / 2 * CLHEP::cm;
  thicknessET9823B = 1.0 * CLHEP::cm;
  TT_ET9823B = 55.0; // in ns
  TTS_ET9823B = 2.4; // in ns

  lengthR10533 = 12.0 / 2 * CLHEP::cm;
  radiusR10533 = 4.6 / 2 * CLHEP::cm;
  thicknessR10533 = 0.25 * CLHEP::cm;
  TT_R10533 = 24.0; // in ns;
  TTS_R10533 = 0.28; // in ns;

  lengthXP2050 = 19.3 / 2 * CLHEP::cm;
  radiusXP2050 = 11.0 / 2 * CLHEP::cm;
  thicknessXP2050 = 1.0 * CLHEP::cm;

  lengthEMI9236KB = 14.0 / 2 * CLHEP::cm;
  radiusEMI9236KB = 5.4 / 2 * CLHEP::cm;
  thicknessEMI9236KB = 0.3 * CLHEP::cm;

  pmtType = TGEANT::PMTDUMMY;

  housing_tube = NULL;
  vacuum_tube = NULL;
  window_tube = NULL;
  photocathode_tube = NULL;

  housing_log = NULL;
  vacuum_log = NULL;
  window_log = NULL;
  photocathode_log = NULL;

  housing_phys = NULL;
  vacuum_phys = NULL;
  window_phys = NULL;
  photocathode_phys = NULL;

  vacAl = NULL;
  windowAl = NULL;
  bialkaliAl = NULL;
  windowVac = NULL;
  vacWindow = NULL;
  vacBialkali = NULL;
}

T4PhotoMultiplier::~T4PhotoMultiplier(void)
{
  if (housing_tube != NULL)
    delete housing_tube;
  if (vacuum_tube != NULL)
    delete vacuum_tube;
  if (window_tube != NULL)
    delete window_tube;
  if (photocathode_tube != NULL)
    delete photocathode_tube;

  if (housing_log != NULL)
    delete housing_log;
  if (vacuum_log != NULL)
    delete vacuum_log;
  if (window_log != NULL)
    delete window_log;
  if (photocathode_log != NULL)
    delete photocathode_log;

  if (vacAl != NULL)
    delete vacAl;
  if (windowAl != NULL)
    delete windowAl;
  if (bialkaliAl != NULL)
    delete bialkaliAl;
  if (windowVac != NULL)
    delete windowVac;
  if (vacWindow != NULL)
    delete vacWindow;
  if (vacBialkali != NULL)
    delete vacBialkali;
}

void T4PhotoMultiplier::construct(G4LogicalVolume* world_log)
{
  G4double dz_window = 0.5 * CLHEP::mm;
  G4double dz_photocathode = 0.5 * CLHEP::mm;

  G4double zPos_Vacuum, zPos_Window, zPos_photocathode;
  if (alignment == TGEANT::UP) {
    zPos_Vacuum = 2.0 * CLHEP::mm;
    zPos_Window = length - 3.0 * CLHEP::mm; //0.5mm air
    zPos_photocathode = length - 5.0 * CLHEP::mm; //1mm air gap
  } else if (alignment == TGEANT::DOWN) {
    zPos_Vacuum = -2.0 * CLHEP::mm;
    zPos_Window = -1. * length + 3.0 * CLHEP::mm;
    zPos_photocathode = -1. * length + 5.0 * CLHEP::mm;
  }

  housing_tube = new G4Tubs("housing_tube", 0, radius + thickness, length, 0,
      360.0 * CLHEP::deg);
  vacuum_tube = new G4Tubs("vacuum_tube", 0, radius, length - 2.0 * CLHEP::mm,
      0, 360.0 * CLHEP::deg);
  window_tube = new G4Tubs("window_tube", 0, radius, dz_window, 0,
      360.0 * CLHEP::deg);
  photocathode_tube = new G4Tubs("photocathode_tube", 0, radius,
      dz_photocathode, 0, 360.0 * CLHEP::deg);

  housing_log = new G4LogicalVolume(housing_tube, aluminiumMat, "housing_log");
  vacuum_log = new G4LogicalVolume(vacuum_tube, vacuumMat, "vacuum_log");
  window_log = new G4LogicalVolume(window_tube, glassMat, "window_log");
  photocathode_log = new G4LogicalVolume(photocathode_tube, bialkaliMat,
      "photocathode_log");

  window_log->SetVisAttributes(colour->blue);
  housing_log->SetVisAttributes(colour->green);

  housing_phys = new G4PVPlacement(&rotationMatrix, positionVector, housing_log,
      "pmt_phys", world_log, false, 0, checkOverlap);
  if (settingsFile->getStructManager()->getGeneral()->simplifiedGeometries)
    return;
  vacuum_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, zPos_Vacuum),
      vacuum_log, "vacuum_phys", housing_log, false, 0, false);
  window_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, zPos_Window),
      window_log, "window_phys", vacuum_log, false, 0, false);
  photocathode_phys = new G4PVPlacement(0,
      G4ThreeVector(0, 0, zPos_photocathode), photocathode_log,
      "photocathode_phys", vacuum_log, false, 0, false);

  if (settingsFile->isOpticalPhysicsActivated()) {
    vacAl = new G4LogicalBorderSurface("vacAl", vacuum_phys, housing_phys,
        materials->surfaceAluminium);
    windowAl = new G4LogicalBorderSurface("windowAl", window_phys, housing_phys,
        materials->surfaceAluminium);
    bialkaliAl = new G4LogicalBorderSurface("bialkaliAl", photocathode_phys,
        housing_phys, materials->surfaceAluminium);
    windowVac = new G4LogicalBorderSurface("windowVac", window_phys,
        vacuum_phys, materials->surfaceGlassVacuum);
    vacWindow = new G4LogicalBorderSurface("vacWindow", vacuum_phys,
        window_phys, materials->surfaceVacuumBialkali);
    vacBialkali = new G4LogicalBorderSurface("vacBialkali", vacuum_phys,
        photocathode_phys, materials->surfaceVacuumBialkali);
  }

  if (name != "")
    photocathode_log->SetSensitiveDetector(
        new T4SensitiveDetector(name, channelNo, TGEANT::PMT, pmtType));
}

void T4PhotoMultiplier::usePmtType(void)
{
  if (pmtType == TGEANT::ET9823B) {
    radius = radiusET9823B;
    length = lengthET9823B;
    thickness = thicknessET9823B;
  } else if (pmtType == TGEANT::R10533) {
    radius = radiusR10533;
    length = lengthR10533;
    thickness = thicknessR10533;
  } else if (pmtType == TGEANT::XP2050) {
    radius = radiusXP2050;
    length = lengthXP2050;
    thickness = thicknessXP2050;
  } else if (pmtType == TGEANT::EMI9236KB) {
    radius = radiusEMI9236KB;
    length = lengthEMI9236KB;
    thickness = thicknessEMI9236KB;
  }
}

G4double T4PhotoMultiplier::getPmtLength(TGEANT::PmtType _pmtType)
{
  if (_pmtType == TGEANT::ET9823B) {
    return lengthET9823B;
  } else if (_pmtType == TGEANT::R10533) {
    return lengthR10533;
  } else if (_pmtType == TGEANT::XP2050) {
    return lengthXP2050;
  } else if (_pmtType == TGEANT::EMI9236KB) {
    return lengthEMI9236KB;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "PhotoMultiplier::getPmtLength: Unknown PmtType. Return length 0.");
    return 0.0;
  }
}

G4double T4PhotoMultiplier::getPmtRadius(TGEANT::PmtType _pmtType)
{
  if (_pmtType == TGEANT::ET9823B) {
    return radiusET9823B;
  } else if (_pmtType == TGEANT::R10533) {
    return radiusR10533;
  } else if (_pmtType == TGEANT::XP2050) {
    return radiusXP2050;
  } else if (_pmtType == TGEANT::EMI9236KB) {
    return radiusEMI9236KB;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "PhotoMultiplier::getPmtRadius: Unknown PmtType. Return radius 0.");
    return 0.0;
  }
}

G4double T4PhotoMultiplier::getTT(TGEANT::PmtType _pmtType)
{
  if (_pmtType == TGEANT::PMTDUMMY) {
    return 0.0;
  } else if (_pmtType == TGEANT::ET9823B) {
    return TT_ET9823B;
  } else if (_pmtType == TGEANT::R10533) {
    return TT_R10533;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "PhotoMultiplier::getTT: Unknown PmtType or TT not set. Return time 0.");
    return 0.0;
  }
}

G4double T4PhotoMultiplier::getTTS(TGEANT::PmtType _pmtType)
{
  if (_pmtType == TGEANT::PMTDUMMY) {
    return 0.0;
  } else if (_pmtType == TGEANT::ET9823B) {
    return TTS_ET9823B;
  } else if (_pmtType == TGEANT::R10533) {
    return TTS_R10533;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "PhotoMultiplier::getTTS: Unknown PmtType or TTS not set. Return time 0.");
    return 0.0;
  }
}
