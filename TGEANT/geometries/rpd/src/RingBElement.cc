#include "RingBElement.hh"
#include "CameraConstruction.hh"
#include "T4PhotoMultiplier.hh"

RingBElement::RingBElement(void)
{
  name = "B";

  sci_x_short = 28.96 / 2 * CLHEP::cm;
  sci_x_long = 30.28 / 2 * CLHEP::cm;
  sci_y = 5.0 / 2 * CLHEP::cm;

  CameraConstruction camera;
  sci_z = camera.getLenghtB();

  T4PhotoMultiplier photoMultiplier;
  pmtRadius = photoMultiplier.getPmtRadius(TGEANT::ET9823B);
  pmtLength = photoMultiplier.getPmtLength(TGEANT::ET9823B);
  outerLgRadius = 28.0 * CLHEP::cm;
  upperLgLength = 30.0 / 2 * CLHEP::cm;

  isSciTrd = true;

  lightGuide_up = NULL;
  lightGuide_down = NULL;
}

RingBElement::~RingBElement(void)
{
  if (lightGuide_up != NULL)
    delete lightGuide_up;
  if (lightGuide_down != NULL)
    delete lightGuide_down;
}

void RingBElement::construct(G4LogicalVolume* world_log)
{
  scintillatorTrd = new ScintillatorTrd();
  scintillatorTrd->setDimensions(sci_x_short, sci_x_long, sci_y, sci_z);
  scintillatorTrd->setPosition(positionVector);
  scintillatorTrd->setRotation(rotationMatrix);
  scintillatorTrd->setName(name);
  scintillatorTrd->setChannelNo(channelNo);
  scintillatorTrd->setDetectorId(detectorId);
  if (useOptical)
    scintillatorTrd->useOpticalPhysics();
  if (!isSciTrd)
    scintillatorTrd->deactivateCap();
  scintillatorTrd->construct(world_log);
  regionManager->addToOpticalRegion(scintillatorTrd->getVolume());

  lightGuide_up = new LightGuideRingB();
  lightGuide_up->setPosition(positionVector + G4ThreeVector(0, 0, -sci_z));
  lightGuide_up->setDimensions(sci_x_short, sci_y, pmtRadius, upperLgLength,
      outerLgRadius);
  lightGuide_up->setAlignment(TGEANT::UP);
  lightGuide_up->setRotation(rotationMatrix);
  if (useOptical)
    lightGuide_up->useOpticalPhysics();
  lightGuide_up->construct(world_log);
  regionManager->addToOpticalRegion(lightGuide_up->getVolume());

  lightGuide_down = new LightGuideRingB();
  lightGuide_down->setPosition(positionVector + G4ThreeVector(0, 0, sci_z));
  lightGuide_down->setDimensions(sci_x_short, sci_y, pmtRadius, upperLgLength,
      outerLgRadius);
  lightGuide_down->setAlignment(TGEANT::DOWN);
  lightGuide_down->setRotation(rotationMatrix);
  if (useOptical)
    lightGuide_down->useOpticalPhysics();
  lightGuide_down->construct(world_log);
  regionManager->addToOpticalRegion(lightGuide_down->getVolume());

  photoMultiplier_up = new T4PhotoMultiplier();
  photoMultiplier_up->setPosition(lightGuide_up->getPmtPosition(pmtLength));
  photoMultiplier_up->setAlignment(TGEANT::UP);
  photoMultiplier_up->setPmtType(TGEANT::ET9823B);
  photoMultiplier_up->rotateZ(
      -(rotationMatrix.getPhi() + rotationMatrix.getPsi()));
  photoMultiplier_up->rotateX(-90.0 * CLHEP::deg);
  photoMultiplier_up->setSensitiveDetector(name + "u", channelNo);
  if (useOptical)
    photoMultiplier_up->useOpticalPhysics();
  photoMultiplier_up->construct(world_log);
  regionManager->addToOpticalRegion(photoMultiplier_up->getVolume());

  photoMultiplier_down = new T4PhotoMultiplier();
  photoMultiplier_down->setPosition(lightGuide_down->getPmtPosition(pmtLength));
  photoMultiplier_down->setAlignment(TGEANT::DOWN);
  photoMultiplier_down->setPmtType(TGEANT::ET9823B);
  photoMultiplier_down->rotateZ(
      -(rotationMatrix.getPhi() + rotationMatrix.getPsi()));
  photoMultiplier_down->rotateX(90.0 * CLHEP::deg);
  photoMultiplier_down->setSensitiveDetector(name + "d", channelNo);
  if (useOptical)
    photoMultiplier_down->useOpticalPhysics();
  photoMultiplier_down->construct(world_log);
  regionManager->addToOpticalRegion(photoMultiplier_down->getVolume());
}

void RingBElement::setDimensions(G4double _sci_x_short, G4double _sci_x_long,
    G4double _sci_y, G4double _sci_z, G4double _pmtRadius,
    G4double _outerLgRadius, G4double _upperLgLenght, G4double _pmtLenght,
    G4bool _isSciTrd)
{
  sci_x_short = _sci_x_short;
  sci_x_long = _sci_x_long;
  sci_y = _sci_y;
  sci_z = _sci_z;

  pmtRadius = _pmtRadius;
  outerLgRadius = _outerLgRadius;
  upperLgLength = _upperLgLenght;
  pmtLength = _pmtLenght;
  isSciTrd = _isSciTrd;
}
