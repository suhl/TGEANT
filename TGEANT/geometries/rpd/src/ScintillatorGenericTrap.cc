#include "ScintillatorGenericTrap.hh"
#include "CameraConstruction.hh"

ScintillatorGenericTrap::ScintillatorGenericTrap(void)
{
  sci_world_trap = NULL;
  plastic_trap = NULL;
  housing_trap = NULL;
  airGap_trap = NULL;
  scintillator_trap = NULL;

  slope = 0;
  angle = 0;
}

ScintillatorGenericTrap::~ScintillatorGenericTrap(void)
{
  if (sci_world_trap != NULL)
    delete sci_world_trap;
  if (plastic_trap != NULL)
    delete plastic_trap;
  if (housing_trap != NULL)
    delete housing_trap;
  if (airGap_trap != NULL)
    delete airGap_trap;
  if (scintillator_trap != NULL)
    delete scintillator_trap;
}

void ScintillatorGenericTrap::construct(G4LogicalVolume* world_log)
{
  CameraConstruction camera;
  G4double airGap = camera.getAirGap();
  G4double aluFoil = camera.getAluFoil();
  G4double plasticFoil = camera.getPlasticFoil();

  G4double dx_short[4] = { sci_x_short + airGap + aluFoil + plasticFoil,
      sci_x_short + airGap + aluFoil, sci_x_short + airGap, sci_x_short };

  G4double dx_long[4] = { sci_x_long + airGap + aluFoil + plasticFoil,
      sci_x_long + airGap + aluFoil, sci_x_long + airGap, sci_x_long };

  G4double dy[4] = { sci_y + airGap + aluFoil + plasticFoil, sci_y + airGap
      + aluFoil, sci_y + airGap, sci_y };

  G4double sciWorld_z = sci_z + airGap + aluFoil + plasticFoil;
  G4double phiWorld = slope * sciWorld_z / CLHEP::cm;
  G4double phiScint = slope * sci_z / CLHEP::cm;

  vector<G4TwoVector> point[8];
  // we need 5 interations because the first entry (sci_world) is longer in z direction than plastic
  for (int i = 0; i < 5; i++) {
    int ii = i;
    if (i != 0)
      ii -= 1;
    for (int j = 0; j < 2; j++) {
      point[4 * j].push_back(G4TwoVector(-dx_short[ii], -dy[ii]));
      point[4 * j + 1].push_back(G4TwoVector(-dx_long[ii], dy[ii]));
      point[4 * j + 2].push_back(G4TwoVector(dx_long[ii], dy[ii]));
      point[4 * j + 3].push_back(G4TwoVector(dx_short[ii], -dy[ii]));
    }
  }

  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 4; j++) {
      point[4 * i + j].at(0).rotate((1. - 2. * i) * phiWorld - angle);
      for (int k = 1; k < 5; k++)
        point[4 * i + j].at(k).rotate((1. - 2. * i) * phiScint - angle);
    }
  }

  G4TwoVector helperGlobal = G4TwoVector(positionVector.x(),
      positionVector.y());

  G4TwoVector helperUp = helperGlobal;
  helperUp.rotate(phiWorld);
  helperUp -= helperGlobal;
  G4TwoVector helperDn = helperGlobal;
  helperDn.rotate(-phiWorld);
  helperDn -= helperGlobal;

  vector<G4TwoVector> vector2Vec;
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperUp + point[j].at(0));
  for (int j = 4; j < 8; j++)
    vector2Vec.push_back(helperDn + point[j].at(0));

  sci_world_trap = new G4GenericTrap("sci_world_trap",
      sciWorld_z, vector2Vec);

  helperUp = helperGlobal;
  helperUp.rotate(phiScint);
  helperUp -= helperGlobal;
  helperDn = helperGlobal;
  helperDn.rotate(-phiScint);
  helperDn -= helperGlobal;

  vector2Vec.clear();
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperUp + point[j].at(1));
  for (int j = 4; j < 8; j++)
    vector2Vec.push_back(helperDn + point[j].at(1));
  plastic_trap = new G4GenericTrap("plastic_trd", sci_z, vector2Vec);

  vector2Vec.clear();
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperUp + point[j].at(2));
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperDn + point[4 + j].at(2));
  housing_trap = new G4GenericTrap("housing_trap", sci_z, vector2Vec);

  vector2Vec.clear();
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperUp + point[j].at(3));
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperDn + point[4 + j].at(3));
  airGap_trap = new G4GenericTrap("airGap_trap", sci_z, vector2Vec);

  vector2Vec.clear();
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperUp + point[j].at(4));
  for (int j = 0; j < 4; j++)
    vector2Vec.push_back(helperDn + point[4 + j].at(4));
  scintillator_trap = new G4GenericTrap("scintillator_trap", sci_z, vector2Vec);

  sci_world_log = new G4LogicalVolume(sci_world_trap, plexiglassMat,
      "sci_world_log", 0, 0, 0);
  plastic_log = new G4LogicalVolume(plastic_trap, materials->polypropylene,
      "plastic_log", 0, 0, 0);
  housing_log = new G4LogicalVolume(housing_trap, aluminiumMat, "housing_log",
      0, 0, 0);
  airGap_log = new G4LogicalVolume(airGap_trap, airMat, "airGap_log", 0, 0, 0);
  scintillator_log = new G4LogicalVolume(scintillator_trap, bc408Mat,
      "scintillator_log", 0, 0, 0);

  sci_world_phys = new G4PVPlacement(0, positionVector, sci_world_log,
      name + intToStr(channelNo), world_log, false, 0, checkOverlap);
  plastic_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), plastic_log,
      "plastic_phys", sci_world_log, false, 0, checkOverlap);
  housing_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), housing_log,
      "housing_phys", plastic_log, false, 0, checkOverlap);
  airGap_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log,
      "airGap_phys", housing_log, false, 0, checkOverlap);
  scintillator_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0),
      scintillator_log, "scintillator_phys", airGap_log, false, 0, checkOverlap);

  plastic_log->SetVisAttributes(colour->yellow);


  if (useCap) {
    // caps are only implemented for class ScintillatorTrd
  }


  if (settingsFile->isOpticalPhysicsActivated()) {
    sciAir = new G4LogicalBorderSurface("sciAir", scintillator_phys,
        airGap_phys, materials->surfaceBc408Air);
    airSci = new G4LogicalBorderSurface("airSci", airGap_phys,
        scintillator_phys, materials->surfaceAirBc408);
    airAl = new G4LogicalBorderSurface("airAl", airGap_phys, housing_phys,
        materials->surfaceAluminium);
    sciAirGap = new G4LogicalBorderSurface("sciPlexi", scintillator_phys,
        sci_world_phys, materials->surfaceBc408Plexiglass);
    airGapSci = new G4LogicalBorderSurface("sciPlexi", sci_world_phys,
        scintillator_phys, materials->surfaceAirPlexiglass);

    if (useCap) {
      // caps are only implemented for class ScintillatorTrd
    }
  }

  createSensitiveDetector(name, channelNo, detectorId);
}
