#include "CameraConstruction.hh"

CameraConstruction::CameraConstruction(void)
{
  airGap = 0.1 * CLHEP::mm;
  aluFoil = 0.02 * CLHEP::mm;
  plasticFoil = 0.20 * CLHEP::mm;

  lenghtA = 275.0 / 2. * CLHEP::cm;
  lenghtB = 360.0 / 2. * CLHEP::cm;

  radiusA = 25.90 * CLHEP::cm; // thickness sci in middle + gap + ~1mm spacing (6.55/2 + 0.09 + ~0.05)/tan(7.5deg)
  radiusB = 114.70 * CLHEP::cm; // thickness sci in middle + gap + 4mm spacing (14.81 + 0.09 + 0.2)/tan(7.5deg)

  ringAvsBRotation = 7.5 * CLHEP::deg;

  ringA = NULL;
  ringB = NULL;
  detectorId_A = 926;
  detectorId_B = 927;
  tbName_A = "CA01R1__";
  tbName_B = "CA01R2__";

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getRPD()->size(); i++) {
    if (settingsFile->getStructManager()->getRPD()->at(i).general.name
        == "CA01R1")
      ringA = &settingsFile->getStructManager()->getRPD()->at(i);
    else if (settingsFile->getStructManager()->getRPD()->at(i).general.name
        == "CA01R2")
      ringB = &settingsFile->getStructManager()->getRPD()->at(i);
  }

  for (G4int i = 0; i < nRing; i++) {
    rotationSciA[i] = NULL;
    rotationSciB[i] = NULL;
    ringAElement[i] = NULL;
    ringBElement[i] = NULL;
  }

  cameraAmbiente = NULL;
}

CameraConstruction::~CameraConstruction(void)
{
  for (G4int i = 0; i < nRing; i++) {
    if (rotationSciA[i] != NULL)
      delete rotationSciA[i];
    if (rotationSciB[i] != NULL)
      delete rotationSciB[i];
    if (ringAElement[i] != NULL)
      delete ringAElement[i];
    if (ringBElement[i] != NULL)
      delete ringBElement[i];
  }

  if (cameraAmbiente != NULL)
    delete cameraAmbiente;
}

void CameraConstruction::construct(G4LogicalVolume* world_log)
{
  if (ringA == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "CameraConstruction::construct: RingA information not found in settings file.");
    return;
  }
  if (ringB == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "Error in CameraConstruction: RingB information not found in settings file.");
    return;
  }

  if (ringA->useCalibrationFile || ringB->useCalibrationFile)
    positioningCalibrationFile();
  else
    positioningClassic();

  if (!settingsFile->isOpticalPhysicsActivated()) {
    ringA->useOptical = false;
    ringB->useOptical = false;
  }

  // ringA
  if (ringA->general.useDetector) {
    if (!ringA->useSingleSlab)
      for (G4int i = 0; i < nRing; i++) {
        if (ringA->general.useDetector) {
          ringAElement[i] = new RingAElement();
          ringAElement[i]->setPosition(positionSciA[i]);
          ringAElement[i]->setRotation(*rotationSciA[i]);
          ringAElement[i]->setDetectorName(tbName_A);
          ringAElement[i]->setChannelNo(i);
          ringAElement[i]->setDetectorId(detectorId_A);
          if (ringA->useOptical)
            ringAElement[i]->useOpticalPhysics();
          if (ringA->useCalibrationFile) {
            ringAElement[i]->setSlope(slope_A[i]);
            ringAElement[i]->setRotAngle(rotAngle_A[i]);
          }
          ringAElement[i]->construct(world_log);
        }
      }
    else {
      ringAElement[0] = new RingAElement();
      ringAElement[0]->setPosition(
          positionRingA + G4ThreeVector(0, radiusA, 0));
      ringAElement[0]->setRotation(*rotationSciB[0]);
      ringAElement[0]->setDetectorName(tbName_A);
      ringAElement[0]->setChannelNo(0);
      ringAElement[0]->setDetectorId(detectorId_A);
      if (ringA->useOptical)
        ringAElement[0]->useOpticalPhysics();
      ringAElement[0]->construct(world_log); // w/o AvsB rotation
    }
  }

  // ringB
  if (ringB->general.useDetector) {
    if (!ringB->useSingleSlab)
      for (G4int i = 0; i < nRing; i++) {
        if (ringB->general.useDetector) {
          ringBElement[i] = new RingBElement();
          ringBElement[i]->setPosition(positionSciB[i]);
          ringBElement[i]->setRotation(*rotationSciB[i]);
          ringBElement[i]->setDetectorName(tbName_B);
          ringBElement[i]->setChannelNo(i);
          ringBElement[i]->setDetectorId(detectorId_B);
          if (ringB->useOptical)
            ringBElement[i]->useOpticalPhysics();
          ringBElement[i]->construct(world_log);
        }
      }
    else {
      ringBElement[0] = new RingBElement();
      ringBElement[0]->setPosition(positionSciB[0]);
      ringBElement[0]->setRotation(*rotationSciB[0]);
      ringBElement[0]->setDetectorName(tbName_B);
      ringBElement[0]->setChannelNo(0);
      ringBElement[0]->setDetectorId(detectorId_B);
      if (ringB->useOptical)
        ringBElement[0]->useOpticalPhysics();
      ringBElement[0]->construct(world_log);
    }
  }

  if (ringA->general.useMechanicalStructure
      || ringB->general.useMechanicalStructure) {
    cameraAmbiente = new CameraAmbiente();
    cameraAmbiente->buildA(ringA->general.useMechanicalStructure);
    cameraAmbiente->buildB(ringB->general.useMechanicalStructure);
    cameraAmbiente->setPositionA(
        positionRingA + G4ThreeVector(0, 0, -lenghtA));
    cameraAmbiente->setPositionB(
        positionRingB + G4ThreeVector(0, 0, -lenghtB));
    cameraAmbiente->construct(world_log);
  }

  // "Ring C"
//  G4Tubs* ringC_trd = new G4Tubs("ringC", radiusB + 6.0*CLHEP::cm, radiusB + 6.1*CLHEP::cm, 360.0/2 * CLHEP::cm, 0, 360.0*CLHEP::deg);
//  G4LogicalVolume* ringC_log = new G4LogicalVolume(ringC_trd, materials->bc408, "ringC_log", 0, 0, 0);
//  new G4PVPlacement(0, positionRingB, ringC_log, "ringC_phys", world_log, false, 0, true);
//  ringC_log->SetVisAttributes(colour->cyan);
//  ringC_log->SetSensitiveDetector(new T4SensitiveDetector("C", 0, TGEANT::HIT, 666));
}

void CameraConstruction::positioningClassic(void)
{
  positionRingA = G4ThreeVector(ringA->general.position[0],
      ringA->general.position[1], ringA->general.position[2]);
  positionRingB = G4ThreeVector(ringB->general.position[0],
      ringB->general.position[1], ringB->general.position[2]);

  for (G4int i = 0; i < nRing; i++) {
    positionSciA[i] = G4ThreeVector(
        sin(M_PI / 12. * i + ringAvsBRotation) * radiusA,
        cos(M_PI / 12. * i + ringAvsBRotation) * radiusA, 0)
        + positionRingA;

    positionSciB[i] = G4ThreeVector(sin(M_PI / 12. * i) * radiusB,
        cos(M_PI / 12. * i) * radiusB, 0) + positionRingB;

    rotationSciA[i] = new CLHEP::HepRotation;
    rotationSciA[i]->rotateZ(15.0 * i * CLHEP::deg + ringAvsBRotation);
    rotationSciB[i] = new CLHEP::HepRotation;
    rotationSciB[i]->rotateZ(15.0 * i * CLHEP::deg);
  }
}

void CameraConstruction::positioningCalibrationFile(void)
{
  G4double rad_A[24] = { 24.92256896756771, 24.602107857971074,
      24.354782815653444, 24.20417644796845, 24.162590651108978,
      24.231439780561534, 24.404821559315163, 24.66306245880473,
      24.99920451634423, 25.370340026850574, 25.785376205587525,
      26.169565061970545, 26.528067915189332, 26.829516511365224,
      27.0568558009009, 27.196075331169528, 27.23790442518444,
      27.18314928880973, 27.02274061011354, 26.78150299339417,
      26.473564320134344, 26.104505068693552, 25.701599301862675,
      25.30290884580363 };
  G4double phi_offset_A[24] = { 2.1272972287976497, 1.86029647006288,
      1.5828326023270718, 1.2968243920908689, 1.0369480800877762,
      0.746994675145808, 0.47688864750979953, 0.20673190403279793,
      -0.07154299392070246, -0.33401207777737635, -0.6038135112473088,
      -0.8574635009698032, -1.1127818688821771, -1.359423594631018,
      -1.6207157928233593, -1.852280650323646, -2.1055062180418704,
      -2.3476438588632687, -2.5999791841455364, -2.864663930303707,
      -3.1058548529042183, 2.9324091326335537, 2.6623798970273134,
      2.3998173650981323 };
  G4double phi_slope_A[24] = { -0.00016945094934195314, -0.00015608542640536553,
      -0.000152613639284716, -0.00019819895536742473, -0.00011684416098134872,
      -0.00017593767292604272, -0.00014845885971775003, -0.00016871228493121342,
      -0.00020709173827011275, -0.0002249656966286202, -0.00020770258052656252,
      -0.00020892648860002834, -0.00021158940472255008, -0.00019229516973805482,
      -0.0002549023438975024, -0.00016284029049565412, -0.00020120477948226313,
      -0.00019080798961516567, -0.00015597584953316924, -0.00023479404548462025,
      -0.00019251047744329783, -0.0001249062688737478, -0.00016736626510246126,
      -0.0001541033549029647 };
  G4double z_mid_A[24] = { -175.5, -175.5, -175.5, -175.5, -175.5, -175.5,
      -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5,
      -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5, -175.5 };

  G4double rad_B[24] = { 112.04153065094975, 111.92893063730995,
      111.79078962387348, 111.63534529514551, 111.47543354734725,
      111.32189574671729, 111.18630544790929, 111.07441959323748,
      110.9980035634632, 110.95962536494196, 110.96234715591208,
      111.00826482454153, 111.0901827901783, 111.2054766841139,
      111.34187853000967, 111.50438750261208, 111.66086247073832,
      111.8117623449438, 111.94465022648697, 112.05922427642176,
      112.13396228607381, 112.17275191830242, 112.16969522793687,
      112.12580908093578 };

  // some of these phi values are corrected to avoid overlaps
  //  0: 2.214642501407261 => 2.216642501407261 (+2 mrad)
  // 10: -0.39840202361678656 => -0.39940202361678656 (-1 mrad)
  // 11: -0.6702320904934586 => -0.6692320904934586 (+1 mrad)
  // 14: -1.4486680062811534 => -1.4521680062811534 (-3.5 mrad)
  // 15: -1.723796573621456 => -1.721296573621456 (+2.5 mrad)
  // 16: -1.9810137879183791 => -1.9805137879183791 (+0.5 mrad)
  // 18: -2.49378690244551 => -2.49778690244551 (-4 mrad)
  // 19: -2.7650898711440086 => -2.7630898711440086 (+2 mrad)
  // 20: -3.0197724721726145 => -3.0207724721726145 (-1 mrad)
  // 21: 2.9961820570746895 => 3.0001820570746895 (+4 mrad)
  // 22: 2.7444645273406785 => 2.7424645273406785 (-2 mrad)

  G4double phi_offset_B[24] = { 2.216642501407261, 1.9585551069675227,
      1.6991881046047834, 1.43597995893505, 1.1731077422577143,
      0.9103214955144868, 0.6499066487506169, 0.38429449706115376,
      0.12366610260230151, -0.1391382822573528, -0.39940202361678656,
      -0.6692320904934586, -0.9296261595275807, -1.1923410074540657,
      -1.4521680062811534, -1.721296573621456, -1.9805137879183791, //14 15 16
      -2.239247374938591, -2.49778690244551, -2.7630898711440086, // 17 18 19
      -3.0207724721726145, 3.0001820570746895, 2.7424645273406785, //20 21 22
      2.4820172913986993 };
  G4double phi_slope_B[24] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0 };
  G4double z_mid_B[24] = { -133.7197216361436, -133.48549147035425,
      -135.57479811217937, -131.9557251482342, -134.28440281636117,
      -133.52357158680516, -133.65105217439688, -132.57348160693547,
      -133.75772613054056, -134.4800695365697, -134.02311711308158,
      -133.8328914893854, -131.65977100001865, -131.8928129533818,
      -133.28125922105525, -133.1659660214568, -132.76640912223914,
      -130.99517180130636, -133.1887516242474, -130.94234355883907,
      -132.78361711686676, -133.20601579207622, -132.96401865268794,
      -133.4397520180639 };

  G4double xA = -0.711537202539;
  G4double yA = -1.36369852142;
  G4double xB = -0.591011385529;
  G4double yB = 0.151805761225;

  G4double zA = 0;
  G4double zB = 0;
  for (G4int i = 0; i < nRing; i++) {
    zA += z_mid_A[i];
    zB += z_mid_B[i];
  }
  positionRingA = G4ThreeVector(xA * CLHEP::cm, yA * CLHEP::cm,
      zA / nRing * CLHEP::cm);
  positionRingB = G4ThreeVector(xB * CLHEP::cm, yB * CLHEP::cm,
      zB / nRing * CLHEP::cm);

  for (G4int i = 0; i < nRing; i++) {
    rad_A[i] += 0.2;
    G4double phiA = phi_offset_A[i] + z_mid_A[i] * phi_slope_A[i];
    positionSciA[i] = G4ThreeVector(rad_A[i] * CLHEP::cm * cos(phiA),
        rad_A[i] * CLHEP::cm * sin(phiA), z_mid_A[i] * CLHEP::cm);
    slope_A[i] = phi_slope_A[i];

    rad_B[i] += 2.5;
    G4double phiB = phi_offset_B[i] + z_mid_B[i] * phi_slope_B[i];
    positionSciB[i] = G4ThreeVector(rad_B[i] * CLHEP::cm * cos(phiB),
        rad_B[i] * CLHEP::cm * sin(phiB), z_mid_B[i] * CLHEP::cm);

    // rotation of scintillators w.r.t. the center of the ring, not the positioning reference point
    rotationSciA[i] = new CLHEP::HepRotation;
    rotAngle_A[i] = atan2(positionSciA[i].x() - positionRingA.x(),
        positionSciA[i].y() - positionRingA.y());
    rotationSciA[i]->rotateZ(rotAngle_A[i]);

    rotationSciB[i] = new CLHEP::HepRotation;
    rotationSciB[i]->rotateZ(
        atan2(positionSciB[i].x() - positionRingB.x(),
            positionSciB[i].y() - positionRingB.y()));
  }
}

void CameraConstruction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector rA, rB;

  rA.id = detectorId_A;
  rB.id = detectorId_B;
  rA.tbName = tbName_A;
  rB.tbName = tbName_B;
  rA.det = "CA01";
  rB.det = "CA02";
  rA.unit = rB.unit = 1;
  rA.type = rB.type = 90;
  rA.radLength = rB.radLength = 0;
  rA.xSize = rB.xSize = 0;
  rA.ySize = rB.ySize = 0;
  rA.zSize = rB.zSize = 0;

  rA.xCen = positionRingA[0];
  rA.yCen = positionRingA[1];
  rA.zCen = positionRingA[2];

  rB.xCen = positionRingB[0];
  rB.yCen = positionRingB[1];
  rB.zCen = positionRingB[2];

  rA.rotMatrix = rB.rotMatrix = 1;
  rA.wireDist = rB.wireDist = 0;
  rA.angle = rB.angle = 0;
  rA.nWires = rB.nWires = 0;
  rA.pitch = rB.pitch = 0;
  rA.effic = rB.effic = 0;
  rA.backgr = rB.backgr = 0;
  rA.tgate = rB.tgate = 0;
  rA.drVel = rB.drVel = 0;
  rA.t0 = rB.t0 = 0;
  rA.res2hit = rB.res2hit = 0;
  rA.space = rB.space = 0;
  rA.tslice = rB.tslice = 0;

  wireDet.push_back(rA);
  wireDet.push_back(rB);
}
