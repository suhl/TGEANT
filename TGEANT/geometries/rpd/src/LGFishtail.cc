#include "LGFishtail.hh"

LGFishtail::LGFishtail(void)
{
  sci_x = 20.0 / 2 * CLHEP::cm;
  sci_y = 1.0 / 2 * CLHEP::cm;
  pmt_radius = 3.0 * CLHEP::cm;
  length = 25.0 / 2 * CLHEP::cm;
  alignment = TGEANT::UP;
  positionVector = G4ThreeVector(0, 0, 0);

  plexiglassMat = materials->plexiglass_noOptical;
  aluminiumMat = materials->aluminium_noOptical;
  airMat = materials->air_noOptical;

  airGap = 0.1 * CLHEP::mm;
  aluFoil = 0.02 * CLHEP::mm;
  plasticFoil = 0.20 * CLHEP::mm;

  plastic_trd = NULL;
  housing_trd = NULL;
  airGap_trd = NULL;
  lightguide_trd = NULL;

  plastic_cons = NULL;
  housing_cons = NULL;
  airGap_cons = NULL;
  lightguide_cons = NULL;

  plastic_intersection = NULL;
  housing_intersection = NULL;
  airGap_intersection = NULL;
  lightguide_intersection = NULL;

  plastic_log = NULL;
  housing_log = NULL;
  airGap_log = NULL;
  lightguide_log = NULL;

  plastic_phys = NULL;
  housing_phys = NULL;
  airGap_phys = NULL;
  lightguide_phys = NULL;

  plexiAir = NULL;
  airPlexi = NULL;
  airAl = NULL;
}

LGFishtail::~LGFishtail(void)
{
  if (plastic_trd != NULL)
    delete plastic_trd;
  if (housing_trd != NULL)
    delete housing_trd;
  if (airGap_trd != NULL)
    delete airGap_trd;
  if (lightguide_trd != NULL)
    delete lightguide_trd;

  if (plastic_cons != NULL)
    delete plastic_cons;
  if (housing_cons != NULL)
    delete housing_cons;
  if (airGap_cons != NULL)
    delete airGap_cons;
  if (lightguide_cons != NULL)
    delete lightguide_cons;

  if (plastic_intersection != NULL)
    delete plastic_intersection;
  if (housing_intersection != NULL)
    delete housing_intersection;
  if (airGap_intersection != NULL)
    delete airGap_intersection;
  if (lightguide_intersection != NULL)
    delete lightguide_intersection;

  if (plastic_log != NULL)
    delete plastic_log;
  if (housing_log != NULL)
    delete housing_log;
  if (airGap_log != NULL)
    delete airGap_log;
  if (lightguide_log != NULL)
    delete lightguide_log;

  if (plexiAir != NULL)
    delete plexiAir;
  if (airPlexi != NULL)
    delete airPlexi;
  if (airAl != NULL)
    delete airAl;
}

void LGFishtail::construct(G4LogicalVolume* world_log)
{
  G4double dx[4] = { sci_x + airGap + aluFoil + plasticFoil, sci_x + airGap
      + aluFoil, sci_x + airGap, sci_x };
  G4double dy[4] = { sci_y + airGap + aluFoil + plasticFoil, sci_y + airGap
      + aluFoil, sci_y + airGap, sci_y };
  G4double dr[4] = { pmt_radius + airGap + aluFoil + plasticFoil, pmt_radius
      + airGap + aluFoil, pmt_radius + airGap, pmt_radius };

  if (alignment == TGEANT::DOWN) {
    positionVector += G4ThreeVector(0, 0, length);
  } else if (alignment == TGEANT::UP) {
    rotationMatrix.rotateY(180.0 * CLHEP::deg);
    positionVector += G4ThreeVector(0, 0, -length);
  }

  plastic_trd = new G4Trd("plastic_trd", dx[0], dr[0], dy[0], dr[0], length);
  housing_trd = new G4Trd("housing_trd", dx[1], dr[1], dy[1], dr[1], length);
  airGap_trd = new G4Trd("airGap_trd", dx[2], dr[2], dy[2], dr[2], length);
  lightguide_trd = new G4Trd("lightguide_trd", dx[3], dr[3], dy[3], dr[3],
      length);

  plastic_cons = new G4Cons("plastic_cons", 0,
      sqrt(dx[0] * dx[0] + dy[0] * dy[0]), 0, dr[0], length, 0.0 * CLHEP::deg,
      360.0 * CLHEP::deg);
  housing_cons = new G4Cons("housing_cons", 0,
      sqrt(dx[1] * dx[1] + dy[1] * dy[1]), 0, dr[1], length, 0.0 * CLHEP::deg,
      360.0 * CLHEP::deg);
  airGap_cons = new G4Cons("airGap_cons", 0,
      sqrt(dx[2] * dx[2] + dy[2] * dy[2]), 0, dr[2], length, 0.0 * CLHEP::deg,
      360.0 * CLHEP::deg);
  lightguide_cons = new G4Cons("lightguide_cons", 0,
      sqrt(dx[3] * dx[3] + dy[3] * dy[3]), 0, dr[3], length, 0.0 * CLHEP::deg,
      360.0 * CLHEP::deg);

  plastic_intersection = new G4IntersectionSolid("plastic_intersection",
      plastic_trd, plastic_cons);
  housing_intersection = new G4IntersectionSolid("housing_intersection",
      housing_trd, housing_cons);
  airGap_intersection = new G4IntersectionSolid("airGap_intersection",
      airGap_trd, airGap_cons);
  lightguide_intersection = new G4IntersectionSolid("lightguide_intersection",
      lightguide_trd, lightguide_cons);

  plastic_log = new G4LogicalVolume(plastic_intersection,
      materials->polypropylene, "housing_log");
  housing_log = new G4LogicalVolume(housing_intersection, aluminiumMat,
      "housing_log");
  airGap_log = new G4LogicalVolume(airGap_intersection, airMat,
      "airGap_log");
  lightguide_log = new G4LogicalVolume(lightguide_intersection,
      plexiglassMat, "lightguide_log");

  plastic_phys = new G4PVPlacement(&rotationMatrix, positionVector, plastic_log,
      "plastic_phys", world_log, false, 0, false);
  housing_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), housing_log,
      "housing_phys", plastic_log, false, 0, checkOverlap);
  airGap_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log,
      "airGap_phys", housing_log, false, 0, checkOverlap);
  lightguide_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), lightguide_log,
      "lightguide_phys", airGap_log, false, 0, checkOverlap);

  housing_log->SetVisAttributes(colour->white);

  if (settingsFile->isOpticalPhysicsActivated()) {
    plexiAir = new G4LogicalBorderSurface("plexiAir", lightguide_phys,
        airGap_phys, materials->surfacePlexiglassAir);
    airPlexi = new G4LogicalBorderSurface("airPlexi", airGap_phys,
        lightguide_phys, materials->surfaceAirPlexiglass);
    airAl = new G4LogicalBorderSurface("airAl", airGap_phys, housing_phys,
        materials->surfaceAluminium);
  }
}

void LGFishtail::setAlignment(TGEANT::Alignment _alignment)
{
  alignment = _alignment;
}

void LGFishtail::setDimensions(G4double _sci_x, G4double _sci_y,
    G4double _pmt_radius, G4double _length, G4double _airGap, G4double _aluFoil,
    G4double _plasticFoil)
{
  sci_x = _sci_x;
  sci_y = _sci_y;
  pmt_radius = _pmt_radius;
  length = _length;
  airGap = _airGap;
  aluFoil = _aluFoil;
  plasticFoil = _plasticFoil;
}

void LGFishtail::setRotation(const CLHEP::HepRotation& input)
{
  rotationMatrix = input;
}

G4ThreeVector LGFishtail::getPmtPosition(G4double _length)
{
  if (plastic_trd == NULL)
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__, "LGFishtail::getPmtPosition: LightGuideRingB not constructed!");

  if (alignment == TGEANT::DOWN) {
    return (positionVector + G4ThreeVector(0, 0, length + _length));
  } else {
    return (positionVector + G4ThreeVector(0, 0, -length - _length));
  }
}
