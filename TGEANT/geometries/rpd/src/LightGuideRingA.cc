#include "LightGuideRingA.hh"
#include "CameraConstruction.hh"

LightGuideRingA::LightGuideRingA(void)
{
  sci_x = 6.5 / 2 * CLHEP::cm;
  sci_y = 0.4 / 2 * CLHEP::cm;
  pmt_radius = 1.0 * CLHEP::cm;
  length = 86.0 / 2 * CLHEP::cm;
  lgRadius = 27.4 * CLHEP::cm;
  alignment = TGEANT::DOWN;
  rotZ = 0;

  angle = 44.6 * CLHEP::deg;

  positionVector = G4ThreeVector(0, 0, 0);
}

void LightGuideRingA::construct(G4LogicalVolume* world_log)
{
  rotZ = rotationMatrix.getPhi() + rotationMatrix.getPsi();

  CameraConstruction camera;
  G4double airGap = camera.getAirGap();
  G4double aluFoil = camera.getAluFoil();
  G4double plasticFoil = camera.getPlasticFoil();

  G4double dx[4] = { sci_x + airGap + aluFoil + plasticFoil, sci_x + airGap
      + aluFoil, sci_x + airGap, sci_x };
  G4double dy[4] = { sci_y + airGap + aluFoil + plasticFoil, sci_y + airGap
      + aluFoil, sci_y + airGap, sci_y };
  G4double dr[4] = { pmt_radius + airGap + aluFoil + plasticFoil, pmt_radius
      + airGap + aluFoil, pmt_radius + airGap, pmt_radius };
  G4double outerRadius[4] = { lgRadius, lgRadius - plasticFoil, lgRadius
      - plasticFoil - aluFoil, lgRadius - plasticFoil - aluFoil - airGap };
  G4double innerRadius[4] = { outerRadius[0] - 2. * dy[0], outerRadius[1]
      - 2. * dy[1], outerRadius[2] - 2. * dy[2], outerRadius[3] - 2. * dy[3] };

  rotationTubs = new CLHEP::HepRotation;
  rotationTubs->rotateY(90.0 * CLHEP::deg);
  rotationTubs->rotateX(90.0 * CLHEP::deg - angle);
  rotationTubs->rotateY(180.0 * CLHEP::deg);

  rotationMatrix.rotateY(90.0 * CLHEP::deg);
  rotationMatrix.rotateZ(90.0 * CLHEP::deg);
  if (alignment == TGEANT::UP)
    rotationMatrix.rotateX(180.0 * CLHEP::deg);
  G4ThreeVector translationVector = G4ThreeVector(0, outerRadius[0] - dy[0], 0);
  translationVector.rotateZ(rotZ);
  positionVector += translationVector;

  plastic_trd = new G4Trd("plastic_trd", dr[0], dx[0], dr[0], dy[0], length);
  alu_trd = new G4Trd("alu_trd", dr[1], dx[1], dr[1], dy[1], length);
  airGap_trd = new G4Trd("airGap_trd", dr[2], dx[2], dr[2], dy[2], length);
  lightguide_trd = new G4Trd("lightguide_trd", dr[3], dx[3], dr[3], dy[3],
      length);

  plastic_cons = new G4Cons("plastic_cons", 0, dr[0], 0,
      sqrt(dx[0] * dx[0] + dy[0] * dy[0]), length, 0., 2. * M_PI);
  alu_cons = new G4Cons("alu_cons", 0, dr[1], 0,
      sqrt(dx[1] * dx[1] + dy[1] * dy[1]), length, 0., 2. * M_PI);
  airGap_cons = new G4Cons("alu_cons", 0, dr[2], 0,
      sqrt(dx[2] * dx[2] + dy[2] * dy[2]), length, 0., 2. * M_PI);
  lightguide_cons = new G4Cons("alu_cons", 0, dr[3], 0,
      sqrt(dx[3] * dx[3] + dy[3] * dy[3]), length, 0., 2. * M_PI);

  plastic_tubs = new G4Tubs("plastic_tubs", innerRadius[0], outerRadius[0],
      dx[0], 0., angle);
  alu_tubs = new G4Tubs("alu_tubs", innerRadius[1], outerRadius[1],
      dx[1], 0., angle);
  airGap_tubs = new G4Tubs("airGap_tubs", innerRadius[2], outerRadius[2], dx[2],
      0., angle);
  lightguide_tubs = new G4Tubs("lightguide_tubs", innerRadius[3],
      outerRadius[3], dx[3], 0., angle);

  plastic_intersection = new G4IntersectionSolid("plastic_intersection",
      plastic_cons, plastic_trd);
  alu_intersection = new G4IntersectionSolid("alu_intersection",
      alu_cons, alu_trd);
  airGap_intersection = new G4IntersectionSolid("airGap_intersection",
      airGap_cons, airGap_trd);
  lightguide_intersection = new G4IntersectionSolid("lightguide_intersection",
      lightguide_cons, lightguide_trd);

  plastic_union = new G4UnionSolid("plastic_union", plastic_tubs,
      plastic_intersection, rotationTubs,
      G4ThreeVector(outerRadius[0] - dy[0], length, 0).rotateZ(angle));
  alu_union = new G4UnionSolid("alu_union", alu_tubs,
      alu_intersection, rotationTubs,
      G4ThreeVector(outerRadius[0] - dy[0], length, 0).rotateZ(angle));
  airGap_union = new G4UnionSolid("airGap_union", airGap_tubs,
      airGap_intersection, rotationTubs,
      G4ThreeVector(outerRadius[0] - dy[0], length, 0).rotateZ(angle));
  lightguide_union = new G4UnionSolid("lightguide_union", lightguide_tubs,
      lightguide_intersection, rotationTubs,
      G4ThreeVector(outerRadius[0] - dy[0], length, 0).rotateZ(angle));

  G4Material* aluminiumMat;
  G4Material* airMat;
  G4Material* plexiglassMat;
  if (useOptical) {
    aluminiumMat = materials->aluminium_optical;
    airMat = materials->air_optical;
    plexiglassMat = materials->plexiglass_optical;
  } else {
    aluminiumMat = materials->aluminium_noOptical;
    airMat = materials->air_noOptical;
    plexiglassMat = materials->plexiglass_noOptical;
  }

  plastic_log = new G4LogicalVolume(plastic_union, materials->polypropylene,
      "plastic_log");
  alu_log = new G4LogicalVolume(alu_union, aluminiumMat, "alu_log");
  airGap_log = new G4LogicalVolume(airGap_union, airMat, "airGap_log");
  lightguide_log = new G4LogicalVolume(lightguide_union, plexiglassMat,
      "lightguide_log");

  plastic_phys = new G4PVPlacement(&rotationMatrix, positionVector, plastic_log,
      "plastic_phys", world_log, false, 0, false);
  alu_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), alu_log,
      "lightguideA_phys", plastic_log, false, 0, checkOverlap);
  airGap_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log,
      "airGap_phys", alu_log, false, 0, false);
  lightguide_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), lightguide_log,
      "lightguide_phys", airGap_log, false, 0, false);

  plastic_log->SetVisAttributes(colour->white);

  if (settingsFile->isOpticalPhysicsActivated()) {
    plexiAir = new G4LogicalBorderSurface("plexiAir", lightguide_phys,
        airGap_phys, materials->surfacePlexiglassAir);
    airPlexi = new G4LogicalBorderSurface("airPlexi", airGap_phys,
        lightguide_phys, materials->surfaceAirPlexiglass);
    airAl = new G4LogicalBorderSurface("airAl", airGap_phys, alu_phys,
        materials->surfaceAluminium);
  }
}

G4ThreeVector LightGuideRingA::getPmtPosition(G4double _length)
{
  if (plastic_trd == NULL)
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "LightGuideRingA::getPmtPosition: Can't return PMT position. LightGuideRingA is not constructed.");

  CameraConstruction camera;
  G4double airGap = camera.getAirGap();
  G4double aluFoil = camera.getAluFoil();
  G4double plasticFoil = camera.getPlasticFoil();
  if (alignment == TGEANT::UP) {
    return (positionVector
        + G4ThreeVector(0, -lgRadius + sci_y + airGap + aluFoil + plasticFoil,
            -2. * length - _length).rotateX(angle).rotateZ(rotZ));
  } else {
    return (positionVector
        + G4ThreeVector(0, -lgRadius + sci_y + airGap + aluFoil + plasticFoil,
            2. * length + _length).rotateX(-angle).rotateZ(rotZ));
  }
}
