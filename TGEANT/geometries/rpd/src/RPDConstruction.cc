#include "RPDConstruction.hh"

RPDConstruction::RPDConstruction(void)
{
  rpdA = NULL;
  rpdB = NULL;
  detectorId_A = 926;
  detectorId_B = 927;
  tbName_A = "RP01R1__";
  tbName_B = "RP01R2__";

  for (unsigned int i = 0;
      i < settingsFile->getStructManager()->getRPD()->size(); i++) {
    if (settingsFile->getStructManager()->getRPD()->at(i).general.name
        == "RP01R1")
      rpdA = &settingsFile->getStructManager()->getRPD()->at(i);
    else if (settingsFile->getStructManager()->getRPD()->at(i).general.name
        == "RP01R2")
      rpdB = &settingsFile->getStructManager()->getRPD()->at(i);
  }

  radiusA = 12.0 * CLHEP::cm;
  sciA_x = 6.0 / 2 * CLHEP::cm;
  sciA_y = 0.5 / 2 * CLHEP::cm;
  sciA_z = 50.0 / 2 * CLHEP::cm;
  lgLengthA = 27.0 / 2 * CLHEP::cm;
  pmtRadiusA = 3.0 * CLHEP::cm;
  pmtLenghtA = 34.7 / 2 * CLHEP::cm;

  radiusB = 77.5 * CLHEP::cm;
  sciB_x = 20.0 / 2 * CLHEP::cm;
  sciB_y = 1.0 / 2 * CLHEP::cm;
  sciB_z = 106.0 / 2 * CLHEP::cm;
  lgLengthB = 25.0 / 2 * CLHEP::cm;
  pmtRadiusB = 4.0 * CLHEP::cm;
  pmtLenghtB = 35.0 / 2 * CLHEP::cm;

  ambienteLength = 190.0 / 2 * CLHEP::cm;
  ambienteRadius = 83.0 * CLHEP::cm;

  dimensionAmbiente[0] = ambienteRadius;
  dimensionAmbiente[1] = ambienteRadius + 3.0 * CLHEP::mm;
  dimensionAmbiente[2] = ambienteLength;
  dimensionAmbiente[3] = 0.0 * CLHEP::deg;
  dimensionAmbiente[4] = 360.0 * CLHEP::deg;

  for (int i = 0; i < nRingA; i++) {
    ringA[i] = NULL;
    rotationSciA[i] = NULL;
  }

  for (int i = 0; i < nRingB; i++) {
    ringB[i] = NULL;
    rotationSciB[i] = NULL;
  }

  ambiente_tube = NULL;
  ambiente_log = NULL;
  ambiente_phys = NULL;

	conicalCryostat = NULL;
}

RPDConstruction::~RPDConstruction(void)
{
  for (int i = 0; i < nRingA; i++) {
    if (ringA[i] != NULL)
      delete ringA[i];
    if (rotationSciA[i] != NULL)
      delete rotationSciA[i];
  }

  for (int i = 0; i < nRingB; i++) {
    if (ringB[i] != NULL)
      delete ringB[i];
    if (rotationSciB[i] != NULL)
      delete rotationSciB[i];
  }

  if (ambiente_tube != NULL)
    delete ambiente_tube;
  if (ambiente_log != NULL)
    delete ambiente_log;
  if (ambiente_phys != NULL)
    delete ambiente_phys;

	if(conicalCryostat != NULL)
		delete conicalCryostat;
}

void RPDConstruction::construct(G4LogicalVolume* world_log)
{
  if (rpdA == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "RPDConstruction::construct: RingA information not found in settings file.");
    return;
  }
  if (rpdB == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        "RPDConstruction::construct: RingB information not found in settings file.");
    return;
  }

  G4ThreeVector positionRingA = G4ThreeVector(rpdA->general.position[0],
      rpdA->general.position[1], rpdA->general.position[2]);
  G4ThreeVector positionRingB = G4ThreeVector(rpdB->general.position[0],
      rpdB->general.position[1], rpdB->general.position[2]);

  // ringA
  if (rpdA->general.useDetector) {
    if (!rpdA->useSingleSlab)
      for (G4int i = 0; i < nRingA; i++) {
        positionSciA[i] = G4ThreeVector(
            sin(M_PI / 6. * i) * radiusA,
            cos(M_PI / 6. * i) * radiusA, 0) + positionRingA;

        rotationSciA[i] = new CLHEP::HepRotation;
        rotationSciA[i]->rotateZ(30.0 * i * CLHEP::deg);

        ringA[i] = new RPDElement();
        ringA[i]->constructA(*rotationSciA[i], positionSciA[i], tbName_A, i,
            detectorId_A, sciA_x, sciA_y, sciA_z, lgLengthA, pmtRadiusA,
            pmtLenghtA, rpdA->useOptical, world_log);
      }
    else {
      ringA[0] = new RPDElement();
      rotationSciA[0] = new CLHEP::HepRotation;
      ringA[0]->constructA(*rotationSciA[0], positionSciA[0], tbName_A, 0,
          detectorId_A, sciA_x, sciA_y, sciA_z, lgLengthA, pmtRadiusA,
          pmtLenghtA, rpdA->useOptical, world_log);
    }
    if(rpdA->useConicalCryostat) {
      conicalCryostat = new ConicalCryostat(positionRingB);
      conicalCryostat->construct(world_log);
    }
  }

  // ringB
  if (rpdB->general.useDetector) {
    if (!rpdB->useSingleSlab)
      for (G4int i = 0; i < nRingB; i++) {
        positionSciB[i] = G4ThreeVector(
            sin(M_PI / 12. * i) * radiusB,
            cos(M_PI / 12. * i) * radiusB, 0) + positionRingB;

        rotationSciB[i] = new CLHEP::HepRotation;
        rotationSciB[i]->rotateZ(15.0 * i * CLHEP::deg);

        ringB[i] = new RPDElement();
        ringB[i]->constructB(*rotationSciB[i], positionSciB[i], tbName_B, i,
            detectorId_B, sciB_x, sciB_y, sciB_z, lgLengthB, pmtRadiusB,
            pmtLenghtB, rpdB->useOptical, world_log);
      }
    else {
      ringB[0] = new RPDElement();
      rotationSciB[0] = new CLHEP::HepRotation;
      ringB[0]->constructB(*rotationSciB[0], positionSciB[0], tbName_B, 0,
          detectorId_B, sciB_x, sciB_y, sciB_z, lgLengthB, pmtRadiusB,
          pmtLenghtB, rpdB->useOptical, world_log);
    }
  }

  if (rpdB->general.useMechanicalStructure) {
    ambiente_tube = new G4Tubs("ambiente_tube", dimensionAmbiente[0],
        dimensionAmbiente[1], dimensionAmbiente[2], dimensionAmbiente[3],
        dimensionAmbiente[4]);

    ambiente_log = new G4LogicalVolume(ambiente_tube,
        materials->aluminium_noOptical, "ambiente_log", 0, 0, 0);
    ambiente_phys = new G4PVPlacement(0, positionRingB, ambiente_log,
        "ambiente_phys", world_log, false, 0, checkOverlap);
    ambiente_log->SetVisAttributes(colour->blue);
  }
}

//=============================================================================

RPDElement::RPDElement(void)
{
  scintillatorTrd = NULL;
  fishtail_up = NULL;
  fishtail_down = NULL;
  lgRingA_up = NULL;
  lgRingA_down = NULL;
  photoMultiplier_up = NULL;
  photoMultiplier_down = NULL;
}

RPDElement::~RPDElement(void)
{
  if (scintillatorTrd != NULL)
    delete scintillatorTrd;
  if (fishtail_up != NULL)
    delete fishtail_up;
  if (fishtail_down != NULL)
    delete fishtail_down;
  if (fishtail_down != NULL)
    delete lgRingA_up;
  if (lgRingA_down != NULL)
    delete lgRingA_down;
  if (photoMultiplier_up != NULL)
    delete photoMultiplier_up;
  if (photoMultiplier_down != NULL)
    delete photoMultiplier_down;
}

void RPDElement::constructA(const CLHEP::HepRotation& rotationMatrix,
    G4ThreeVector positionVector, G4String name, G4int channelNo,
    G4int detectorId, G4double sci_x, G4double sci_y, G4double sci_z,
    G4double lgLength, G4double pmtRadius, G4double pmtLength,
    G4bool useOpticalPhysics, G4LogicalVolume* world_log)
{
  scintillatorTrd = new ScintillatorTrd();
  scintillatorTrd->setDimensions(sci_x, sci_x, sci_y, sci_z);
  scintillatorTrd->setPosition(positionVector);
  scintillatorTrd->setRotation(rotationMatrix);
  scintillatorTrd->setName(name);
  scintillatorTrd->setChannelNo(channelNo);
  scintillatorTrd->setDetectorId(detectorId);
  if (useOpticalPhysics)
    scintillatorTrd->useOpticalPhysics();
  scintillatorTrd->deactivateCap();
  scintillatorTrd->construct(world_log);

  G4double angle = 15.0 * CLHEP::deg;
  lgRingA_down = new LightGuideRingA();
  lgRingA_down->setPosition(positionVector + G4ThreeVector(0, 0, sci_z));
  lgRingA_down->setRotation(rotationMatrix);
  lgRingA_down->setDimensions(sci_x, sci_y, pmtRadius, lgLength,
      5.0 * CLHEP::cm);
  lgRingA_down->setAngle(angle);
  lgRingA_down->setAlignment(TGEANT::DOWN);
  if (useOpticalPhysics)
    lgRingA_down->useOpticalPhysics();
  lgRingA_down->construct(world_log);

  lgRingA_up = new LightGuideRingA();
  lgRingA_up->setPosition(positionVector + G4ThreeVector(0, 0, -sci_z));
  lgRingA_up->setRotation(rotationMatrix);
  lgRingA_up->setDimensions(sci_x, sci_y, pmtRadius, lgLength, 5.0 * CLHEP::cm);
  lgRingA_up->setAngle(angle);
  lgRingA_up->setAlignment(TGEANT::UP);
  if (useOpticalPhysics)
    lgRingA_up->useOpticalPhysics();
  lgRingA_up->construct(world_log);

  photoMultiplier_up = new T4PhotoMultiplier();
  photoMultiplier_up->setPosition(lgRingA_up->getPmtPosition(pmtLength));
  photoMultiplier_up->setDimensions(pmtLength, pmtRadius);
  photoMultiplier_up->setAlignment(TGEANT::UP);
  photoMultiplier_up->rotateZ(
      -(rotationMatrix.getPhi() + rotationMatrix.getPsi()));
  photoMultiplier_up->rotateX(-angle);
  photoMultiplier_up->setSensitiveDetector(name + "u", channelNo);
  if (useOpticalPhysics)
    photoMultiplier_up->useOpticalPhysics();
  photoMultiplier_up->construct(world_log);

  photoMultiplier_down = new T4PhotoMultiplier();
  photoMultiplier_down->setPosition(lgRingA_down->getPmtPosition(pmtLength));
  photoMultiplier_down->setDimensions(pmtLength, pmtRadius);
  photoMultiplier_down->setAlignment(TGEANT::DOWN);
  photoMultiplier_down->rotateZ(
      -(rotationMatrix.getPhi() + rotationMatrix.getPsi()));
  photoMultiplier_down->rotateX(angle);
  photoMultiplier_down->setSensitiveDetector(name + "d", channelNo);
  if (useOpticalPhysics)
    photoMultiplier_down->useOpticalPhysics();
  photoMultiplier_down->construct(world_log);
}

void RPDElement::constructB(const CLHEP::HepRotation& rotationMatrix,
    G4ThreeVector positionVector, G4String name, G4int channelNo,
    G4int detectorId, G4double sci_x, G4double sci_y, G4double sci_z,
    G4double lgLenght, G4double pmtRadius, G4double pmtLenght,
    G4bool useOpticalPhysics, G4LogicalVolume* world_log)
{
  scintillatorTrd = new ScintillatorTrd();
  scintillatorTrd->setDimensions(sci_x, sci_x, sci_y, sci_z);
  scintillatorTrd->setPosition(positionVector);
  scintillatorTrd->setRotation(rotationMatrix);
  scintillatorTrd->setName(name);
  scintillatorTrd->setChannelNo(channelNo);
  scintillatorTrd->setDetectorId(detectorId);
  if (useOpticalPhysics)
    scintillatorTrd->useOpticalPhysics();
  scintillatorTrd->deactivateCap();
  scintillatorTrd->construct(world_log);

  fishtail_up = new LGFishtail();
  fishtail_up->setPosition(positionVector + G4ThreeVector(0, 0, -sci_z));
  fishtail_up->setDimensions(sci_x, sci_y, pmtRadius, lgLenght);
  fishtail_up->setAlignment(TGEANT::UP);
  fishtail_up->setRotation(rotationMatrix);
  if (useOpticalPhysics)
    fishtail_up->useOpticalPhysics();
  fishtail_up->construct(world_log);

  fishtail_down = new LGFishtail();
  fishtail_down->setPosition(positionVector + G4ThreeVector(0, 0, sci_z));
  fishtail_down->setDimensions(sci_x, sci_y, pmtRadius, lgLenght);
  fishtail_down->setAlignment(TGEANT::DOWN);
  fishtail_down->setRotation(rotationMatrix);
  if (useOpticalPhysics)
    fishtail_down->useOpticalPhysics();
  fishtail_down->construct(world_log);

  photoMultiplier_up = new T4PhotoMultiplier();
  photoMultiplier_up->setPosition(fishtail_up->getPmtPosition(pmtLenght));
  photoMultiplier_up->setDimensions(pmtLenght, pmtRadius);
  photoMultiplier_up->setAlignment(TGEANT::UP);
  photoMultiplier_up->setSensitiveDetector(name + "u", channelNo);
  if (useOpticalPhysics)
    photoMultiplier_up->useOpticalPhysics();
  photoMultiplier_up->construct(world_log);

  photoMultiplier_down = new T4PhotoMultiplier();
  photoMultiplier_down->setPosition(fishtail_down->getPmtPosition(pmtLenght));
  photoMultiplier_down->setDimensions(pmtLenght, pmtRadius);
  photoMultiplier_down->setAlignment(TGEANT::DOWN);
  photoMultiplier_down->setSensitiveDetector(name + "d", channelNo);
  if (useOpticalPhysics)
    photoMultiplier_down->useOpticalPhysics();
  photoMultiplier_down->construct(world_log);
}

void RPDConstruction::getWireDetDat(std::vector<T4SWireDetector>& wireDet,
    std::vector<T4SDeadZone>& deadZone)
{
  T4SWireDetector rA, rB;

  rA.id = detectorId_A;
  rB.id = detectorId_B;
  rA.tbName = tbName_A;
  rB.tbName = tbName_B;
  rA.det = "RP01";
  rB.det = "RP02";
  rA.unit = rB.unit = 1;
  rA.type = rB.type = 90;
  rA.radLength = 50;
  rB.radLength = 106;

  rA.xSize = 108.1;
  rA.ySize = 889;
  rA.zSize = 40.0;
  rB.xSize = 60;
  rB.ySize = 1142;
  rB.zSize = 99.1;

  rA.xCen = rpdA->general.position[0];
  rA.yCen = rpdA->general.position[1];
  rA.zCen = rpdA->general.position[2];

  rB.xCen = rpdB->general.position[0];
  rB.yCen = rpdB->general.position[1];
  rB.zCen = rpdB->general.position[2];

  rA.rotMatrix = rB.rotMatrix = 1;
  rA.wireDist = rB.wireDist = 0.0;
  rA.angle = rB.angle = 0.0;
  rA.nWires = 12;
  rB.nWires = 24;
  rA.pitch = 318;
  rB.pitch = 280;
  rA.effic = rB.effic = 0.0;
  rA.backgr = rB.backgr = 0.0;
  rA.tgate = rB.tgate = 0.0;
  rA.drVel = rB.drVel = 0.0;
  rA.t0 = rB.t0 = 0.0;
  rA.res2hit = rB.res2hit = 0.0;
  rA.space = rB.space = 0.0;
  rA.tslice = rB.tslice = 0.0;

  wireDet.push_back(rA);
  wireDet.push_back(rB);
}

ConicalCryostat::ConicalCryostat(const G4ThreeVector rpdPosition)
{
	rpdPos = rpdPosition;
  coneInnerRadius_up = 9.3 * CLHEP::cm;
  coneInnerRadius_down  = 17.2 * CLHEP::cm;
  coneOuterRadius_up = 10.6 * CLHEP::cm;
  coneOuterRadius_down  = 18.5 * CLHEP::cm;
  coneLength = 22.55 * CLHEP::cm;

  upstreamWindowRadius = 9.555 * CLHEP::cm;
  upstreamWindowThickness = 0.01 * CLHEP::cm;
	upstreamWindowPosition = 20.545 * CLHEP::cm;
  downstreamWindowRadius = 16.95 * CLHEP::cm;
  downstreamWindowThickness = 0.01 * CLHEP::cm;
	downstreamWindowPosition = 62.745 * CLHEP::cm;

	cone = NULL;
	cone_log = NULL;
	cone_phys = NULL;

	upstreamWindow = NULL;
	upstreamWindow_log = NULL;
	upstreamWindow_phys = NULL;

	downstreamWindow = NULL;
	downstreamWindow_log = NULL;
	downstreamWindow_phys = NULL;
}

ConicalCryostat::~ConicalCryostat(void) {
	if(cone != NULL)
		delete cone;
	if(cone_log != NULL)
		delete cone_log;
	if(cone_phys != NULL)
		delete cone_phys;
	if(upstreamWindow != NULL)
		delete upstreamWindow;
	if(upstreamWindow_log != NULL)
		delete upstreamWindow_log;
	if(upstreamWindow_phys != NULL)
		delete upstreamWindow_phys;
	if(downstreamWindow != NULL)
		delete downstreamWindow;
	if(downstreamWindow_log != NULL)
		delete downstreamWindow_log;
	if(downstreamWindow_phys != NULL)
		delete downstreamWindow_phys;
}

void ConicalCryostat::construct(G4LogicalVolume* world_log)
{
  cone = new G4Cons("cone", coneInnerRadius_up, coneOuterRadius_up,
                            coneInnerRadius_down, coneOuterRadius_down,
                            coneLength, 0, 360.0 * CLHEP::deg);
  cone_log = new G4LogicalVolume(cone, materials->aluminium_noOptical, "cone_log", 0, 0, 0);
  cone_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 416 * CLHEP::mm) + rpdPos,
                                cone_log, "cone_log", world_log, false, 0, checkOverlap);
	cone_log->SetVisAttributes(colour->red);
  upstreamWindow = new G4Tubs("upstreamWindow", 0.0, upstreamWindowRadius, upstreamWindowThickness,
                              0, 360.0 * CLHEP::deg);
  upstreamWindow_log = new G4LogicalVolume(upstreamWindow, materials->mylar, "upstreamWindow_log", 0, 0, 0);
  upstreamWindow_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, upstreamWindowPosition) + rpdPos,
																					upstreamWindow_log, "upstreamWindow_log", world_log,
																					false, 0, checkOverlap);
	upstreamWindow_log->SetVisAttributes(colour->red);
  downstreamWindow = new G4Tubs("downstreamWindow", 0.0, downstreamWindowRadius, downstreamWindowThickness,
                              0, 360.0 * CLHEP::deg);
  downstreamWindow_log = new G4LogicalVolume(downstreamWindow, materials->mylar, "downstreamWindow_log", 0, 0, 0);
  downstreamWindow_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, downstreamWindowPosition) + rpdPos,
																					downstreamWindow_log, "downstreamWindow_log", world_log,
																					false, 0, checkOverlap);
	downstreamWindow_log->SetVisAttributes(colour->red);
}
