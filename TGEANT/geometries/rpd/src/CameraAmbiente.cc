#include "CameraAmbiente.hh"
#include "CameraConstruction.hh"

CameraAmbiente::CameraAmbiente(void)
{
  groundHeight_ = -170.0 * CLHEP::cm;

  useA = false;
  useB = false;

  CameraConstruction camera;
  RingBElement ringB;
  RingAElement ringA;
  G4double radiusA = camera.getRadiusA();
  G4double ringA_y = ringA.getLenghtY() + camera.getAirGap()
      + camera.getAluFoil() + camera.getPlasticFoil();
  G4double ringA_X = ringA.getLenghtX();
  lenghtB_ = camera.getLenghtB();
  radiusB_ = camera.getRadiusB();
  G4double ringB_y = ringB.getLenghtY() + camera.getAirGap()
      + camera.getAluFoil() + camera.getPlasticFoil();
  G4double ringB_X = ringB.getLenghtX();

  dimensionSciHolderA_inner[0] = 22.0 * CLHEP::cm; // rmin, from Nicoles drawing
  dimensionSciHolderA_inner[1] = dimensionSciHolderA_inner[0] + 2.0 * CLHEP::cm; // rmax, 3cm for inner rings, 2cm for outer
  dimensionSciHolderA_inner[2] = 2.0 / 2 * CLHEP::cm;

  dimensionSciHolderA_outer[0] = sqrt(
      (radiusA / CLHEP::cm + ringA_y / CLHEP::cm)
          * (radiusA / CLHEP::cm + ringA_y / CLHEP::cm)
          + ringA_X / CLHEP::cm * ringA_X / CLHEP::cm) * CLHEP::cm
      + 0.3 * CLHEP::cm;
  dimensionSciHolderA_outer[1] = dimensionSciHolderA_outer[0] + 0.3 * CLHEP::cm; // 3mm thickness
  dimensionSciHolderA_outer[2] = dimensionSciHolderA_inner[2];

  dimensionLgHolderAu[0] = radiusA + ringA_y + 10.0 * CLHEP::cm;
  dimensionLgHolderAu[1] = dimensionLgHolderAu[0] + 45.0 * CLHEP::cm;
  dimensionLgHolderAu[2] = 2.0 / 2 * CLHEP::cm;

  dimensionLgHolderAd[0][0] = 4.5 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[0][1] = 6.5 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[0][2] = 8.9 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[0][3] = 10.9 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[0][4] = 5.0 / 2 * CLHEP::cm;

  dimensionLgHolderAd[1][0] = 23.5 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[1][1] = 25.5 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[1][2] = 28.1 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[1][3] = 30.1 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[1][4] = 5.0 / 2 * CLHEP::cm;

  dimensionLgHolderAd[2][0] = 43.0 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[2][1] = 44.0 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[2][2] = 46.6 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[2][3] = 48.6 * CLHEP::cm + 7.1 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[2][4] = 5.0 / 2 * CLHEP::cm;

  dimensionLgHolderAd[3][0] = 60.0 * CLHEP::cm + 5.5 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[3][1] = 62.0 * CLHEP::cm + 5.5 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[3][2] = 69.2 * CLHEP::cm + 5.5 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[3][3] = 71.2 * CLHEP::cm + 5.5 * CLHEP::cm + radiusA;
  dimensionLgHolderAd[3][4] = 10.0 / 2 * CLHEP::cm;

  dimensionSciHolderB[0] = sqrt(
      (radiusB_ / CLHEP::cm + ringB_y / CLHEP::cm)
          * (radiusB_ / CLHEP::cm + ringB_y / CLHEP::cm)
          + ringB_X / CLHEP::cm * ringB_X / CLHEP::cm) * CLHEP::cm
      + 0.2 * CLHEP::cm;
  dimensionSciHolderB[1] = dimensionSciHolderB[0] + 7.3 * CLHEP::cm;
  dimensionSciHolderB[2] = 2.0 / 2 * CLHEP::cm;

  dimensionSciHolderB_2[0] = dimensionSciHolderB[1];
  dimensionSciHolderB_2[1] = dimensionSciHolderB_2[0] + 0.7 * CLHEP::cm;
  dimensionSciHolderB_2[2] = 6.0 / 2 * CLHEP::cm;

  dimensionLgHolderB[0] = radiusB_ + ringB_y + 3.7 * CLHEP::cm;
  dimensionLgHolderB[1] = dimensionLgHolderB[0] + 65.0 * CLHEP::cm;
  dimensionLgHolderB[2] = 2.0 / 2 * CLHEP::cm;

  dimensionCrossbarB[0] = 20.0 / 2 * CLHEP::cm;
  dimensionCrossbarB[1] = 8.0 / 2 * CLHEP::cm;
  dimensionCrossbarB[2] = lenghtB_;

  dimensionBoschX[0] = 261.0 / 2 * CLHEP::cm;
  dimensionBoschX[1] = 12.0 / 2 * CLHEP::cm;
  dimensionBoschX[2] = 12.0 / 2 * CLHEP::cm;

  dimensionBoschY[0] = 10.0 / 2 * CLHEP::cm;
  dimensionBoschY[1] = 100.0 / 2 * CLHEP::cm;
  dimensionBoschY[2] = 10.0 / 2 * CLHEP::cm;

  dimensionBoschZ[0] = 10.0 / 2 * CLHEP::cm;
  dimensionBoschZ[1] = 10.0 / 2 * CLHEP::cm;
  dimensionBoschZ[2] = 227.0 / 2 * CLHEP::cm;

  dimensionBoschAnglePiece[0] = 8.0 / 2 * CLHEP::cm;
  dimensionBoschAnglePiece[1] = 8.0 / 2 * CLHEP::cm;
  dimensionBoschAnglePiece[2] = 90.0 / 2 * CLHEP::cm;

  dimensionBoxAnglePiece[0] = 8.0 / 2 * CLHEP::cm;
  dimensionBoxAnglePiece[1] = 33.0 / 2 * CLHEP::cm;
  dimensionBoxAnglePiece[2] = 57.0 / 2 * CLHEP::cm;

  dimensionBoschAnglePieceBase[0] = 8.0 / 2 * CLHEP::cm;
  dimensionBoschAnglePieceBase[1] = 150.0 / 2 * CLHEP::cm;
  dimensionBoschAnglePieceBase[2] = 8.0 / 2 * CLHEP::cm;

  dimensionBoxAnglePieceBase[0] = 8.0 / 2 * CLHEP::cm;
  dimensionBoxAnglePieceBase[1] = 94.0 / 2 * CLHEP::cm;
  dimensionBoxAnglePieceBase[2] = 94.0 / 2 * CLHEP::cm;

  dimensionBox2AnglePieceBase[0] = 79.0 / 2 * CLHEP::cm;
  dimensionBox2AnglePieceBase[1] = 79.0 / 2 * CLHEP::cm;
  dimensionBox2AnglePieceBase[2] = 8.0 / 2 * CLHEP::cm;

  dimensionLittleBox = 8.0 / 2 * CLHEP::cm;

  lgHolderAu_tubs = NULL;
  lgHolderAu_log = NULL;
  lgHolderB_tubs = NULL;
  lgHolderB_log = NULL;

  for (unsigned int i = 0; i < 2; i++) {
    sciHolderA_inner_tubs[i] = NULL;
    sciHolderA_inner_log[i] = NULL;
    rotationAnglePiece[i] = NULL;
    sciHolderB_tubs[i] = NULL;
    sciHolderB_log[i] = NULL;
    boxAnglePieceBase_box[i] = NULL;
    anglePiece_intersection[i] = NULL;
    anglePiece_log[i] = NULL;
  }

  sciHolderB_2_tubs = NULL;
  sciHolderB_2_log = NULL;
  boschY_box = NULL;
  boschY_log = NULL;

  for (unsigned int i = 0; i < 4; i++) {
    lgHolderAd_cons[i] = NULL;
    lgHolderAd_log[i] = NULL;
    rotationAnglePieceBase[i] = NULL;
  }

  for (unsigned int i = 0; i < 5; i++) {
    sciHolderA_outer_tubs[i] = NULL;
    sciHolderA_outer_log[i] = NULL;
    boschX_box[i] = NULL;
    boschX_log[i] = NULL;
  }

  for (unsigned int i = 0; i < 6; i++) {
    boschZ_box[i] = NULL;
    boschZ_log[i] = NULL;
  }

  crossbarB_box = NULL;
  crossbarB_log = NULL;

  for (unsigned int i = 0; i < 8; i++) {
    rotationCrossbarB[i] = NULL;
    anglePieceBase_intersection[i] = NULL;
    anglePieceBase_log[i] = NULL;
  }

  boschAnglePieceBase_box = NULL;
  littleBox_box = NULL;
  littleBox_log = NULL;
  boschAnglePiece_box = NULL;
  boxAnglePiece_box = NULL;
}

CameraAmbiente::~CameraAmbiente(void)
{
  if (lgHolderAu_tubs != NULL)
    delete lgHolderAu_tubs;
  if (lgHolderAu_log != NULL)
    delete lgHolderAu_log;

  if (lgHolderB_tubs != NULL)
    delete lgHolderB_tubs;
  if (lgHolderB_log != NULL)
    delete lgHolderB_log;

  for (G4int i = 0; i < 2; i++) {
    if (sciHolderA_inner_tubs[i] != NULL)
      delete sciHolderA_inner_tubs[i];
    if (sciHolderA_inner_log[i] != NULL)
      delete sciHolderA_inner_log[i];
    if (rotationAnglePiece[i] != NULL)
      delete rotationAnglePiece[i];
    if (sciHolderB_tubs[i] != NULL)
      delete sciHolderB_tubs[i];
    if (sciHolderB_log[i] != NULL)
      delete sciHolderB_log[i];
    if (boxAnglePieceBase_box[i] != NULL)
      delete boxAnglePieceBase_box[i];
    if (anglePiece_intersection[i] != NULL)
      delete anglePiece_intersection[i];
    if (anglePiece_log[i] != NULL)
      delete anglePiece_log[i];
  }

  if (sciHolderB_2_tubs != NULL)
    delete sciHolderB_2_tubs;
  if (sciHolderB_2_log != NULL)
    delete sciHolderB_2_log;
  if (boschY_box != NULL)
    delete boschY_box;
  if (boschY_log != NULL)
    delete boschY_log;

  for (G4int i = 0; i < 4; i++) {
    if (lgHolderAd_cons[i] != NULL)
      delete lgHolderAd_cons[i];
    if (lgHolderAd_log[i] != NULL)
      delete lgHolderAd_log[i];
    if (rotationAnglePieceBase[i] != NULL)
      delete rotationAnglePieceBase[i];
  }

  for (G4int i = 0; i < 5; i++) {
    if (sciHolderA_outer_tubs[i] != NULL)
      delete sciHolderA_outer_tubs[i];
    if (sciHolderA_outer_log[i] != NULL)
      delete sciHolderA_outer_log[i];
    if (boschX_box[i] != NULL)
      delete boschX_box[i];
    if (boschX_log[i] != NULL)
      delete boschX_log[i];
  }

  for (G4int i = 0; i < 6; i++) {
    if (boschZ_box[i] != NULL)
      delete boschZ_box[i];
    if (boschZ_log[i] != NULL)
      delete boschZ_log[i];
  }

  if (crossbarB_box != NULL)
    delete crossbarB_box;
  if (crossbarB_log != NULL)
    delete crossbarB_log;
  for (unsigned int i = 0; i < crossbar_sub.size(); i++) {
    delete crossbar_sub.at(i);
  }
  crossbar_sub.clear();

  for (G4int i = 0; i < 8; i++) {
    if (rotationCrossbarB[i] != NULL)
      delete rotationCrossbarB[i];
    if (anglePieceBase_intersection[i] != NULL)
      delete anglePieceBase_intersection[i];
    if (anglePieceBase_log[i] != NULL)
      delete anglePieceBase_log[i];
  }

  if (boschAnglePieceBase_box != NULL)
    delete boschAnglePieceBase_box;
  if (littleBox_box != NULL)
    delete littleBox_box;
  if (littleBox_log != NULL)
    delete littleBox_log;
  if (boschAnglePiece_box != NULL)
    delete boschAnglePiece_box;
  if (boxAnglePiece_box != NULL)
    delete boxAnglePiece_box;
}

void CameraAmbiente::construct(G4LogicalVolume* world_log)
{
  checkOverlap = T4SettingsFile::getInstance()->getStructManager()->getGeneral()
      ->checkOverlap;

  if (useA) {
    // sciHolderA
    // distance from z=0 to outer side of PMT_Au: (28+3)cm
    // measured distance from outer side of PMT_Au to end of first ring: 24 cm
    // => -7cm - width/2 = -8cm
    // other lengths also measured at experiment
    positionSciHolderA[0] = positionA
        + G4ThreeVector(0, 0, -8.0 * CLHEP::cm);
    positionSciHolderA[1] = positionSciHolderA[0]
        + G4ThreeVector(0, 0, 73.5 * CLHEP::cm);
    positionSciHolderA[2] = positionSciHolderA[0]
        + G4ThreeVector(0, 0, 139.0 * CLHEP::cm);
    positionSciHolderA[3] = positionSciHolderA[0]
        + G4ThreeVector(0, 0, 210.0 * CLHEP::cm);
    positionSciHolderA[4] = positionSciHolderA[0]
        + G4ThreeVector(0, 0, 283.7 * CLHEP::cm);

    for (G4int i = 0; i < 2; i++) {
      sciHolderA_inner_tubs[i] = new G4Tubs("sciHolderA_inner_tubs",
          dimensionSciHolderA_inner[0], dimensionSciHolderA_inner[1] + 1.0 * CLHEP::cm * i,
          dimensionSciHolderA_inner[2], 0, 2. * M_PI);
      sciHolderA_inner_log[i] = new G4LogicalVolume(sciHolderA_inner_tubs[i],
          materials->airexC70, "sciHolderA_inner_log");
      sciHolderA_inner_log[i]->SetVisAttributes(colour->gray);
    }

    for (G4int i = 0; i < 5; i++) {
      new G4PVPlacement(0, positionSciHolderA[i], sciHolderA_inner_log[(i == 0 || i == 4)],
          "sciHolderA_inner_phys", world_log, false, 0, checkOverlap);

      G4double helper = 0;
      if (i == 0)
        helper = 1.6 * CLHEP::cm; // because of upstream lightguide
      else if (i == 4)
        helper = 0.1 * CLHEP::cm; // because of downstream lightguide
      else
        helper = 0;
      sciHolderA_outer_tubs[i] = new G4Tubs("sciHolderA_outer_tubs",
          dimensionSciHolderA_outer[0] + helper,
          dimensionSciHolderA_outer[1] + helper, dimensionSciHolderA_outer[2],
          0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
      sciHolderA_outer_log[i] = new G4LogicalVolume(sciHolderA_outer_tubs[i],
          materials->plastic, "sciHolderA_outer_log");
      new G4PVPlacement(0, positionSciHolderA[i], sciHolderA_outer_log[i],
          "sciHolderA_outer_phys", world_log, false, 0, checkOverlap);
      sciHolderA_outer_log[i]->SetVisAttributes(colour->gray);
    }

    // lgHolderAu
    positionLgHolderAu = positionA
        + G4ThreeVector(0, 0, -20.0 * CLHEP::cm);
    lgHolderAu_tubs = new G4Tubs("lgHolderAu_tubs", dimensionLgHolderAu[0],
        dimensionLgHolderAu[1], dimensionLgHolderAu[2], 0.0 * CLHEP::deg,
        360.0 * CLHEP::deg);
    lgHolderAu_log = new G4LogicalVolume(lgHolderAu_tubs, materials->aluminium_noOptical,
        "lgHolderAu_log");
    new G4PVPlacement(0, positionLgHolderAu, lgHolderAu_log, "lgHolderAu_phys",
        world_log, false, 0, checkOverlap);
    lgHolderAu_log->SetVisAttributes(colour->gray);

    /* NOT USED
     // lgHolderAd
     positionLgHolderAd[0] = positionVector_ + G4ThreeVector(0, 0, lenghtA_ + 30.0 * CLHEP::cm);
     positionLgHolderAd[1] = positionVector_ + G4ThreeVector(0, 0, lenghtA_ + 50.0 * CLHEP::cm);
     positionLgHolderAd[2] = positionVector_ + G4ThreeVector(0, 0, lenghtA_ + 70.0 * CLHEP::cm);
     positionLgHolderAd[3] = positionVector_ + G4ThreeVector(0, 0, lenghtA_ + 94.0 * CLHEP::cm);

     for (G4int i = 0; i < 4; i++) {
     lgHolderAd_cons[i] = new G4Cons("lgHolderAd_cons", dimensionLgHolderAd[i][0],
     dimensionLgHolderAd[i][1], dimensionLgHolderAd[i][2],
     dimensionLgHolderAd[i][3], dimensionLgHolderAd[i][4],
     0.0 * CLHEP::deg, 360.0 * CLHEP::deg);
     lgHolderAd_log[i] = new G4LogicalVolume(lgHolderAd_cons[i], plastic,
     "lgHolderAd_log", 0, 0, 0);
     lgHolderAd_phys[i] = new G4PVPlacement(0, positionLgHolderAd[i], lgHolderAd_log[i],
     "lgHolderAd_phys", world_log, false, 0, checkOverlap);
     lgHolderAd_log[i]->SetVisAttributes(colour->gray);
     }
     */
  }

  if (useB) {
    // sciHolderB
    positionSciHolderB[0] = G4ThreeVector(0, 0, 23.0 * CLHEP::cm);
    positionSciHolderB[1] = G4ThreeVector(0, 0, 65.0 * CLHEP::cm);
    positionSciHolderB[2] = G4ThreeVector(0, 0, 179.0 * CLHEP::cm);
    positionSciHolderB[3] = G4ThreeVector(0, 0, 293.0 * CLHEP::cm);
    positionSciHolderB[4] = G4ThreeVector(0, 0, 335.0 * CLHEP::cm);

    for (G4int i = 0; i < 2; i++) {
      sciHolderB_tubs[i] = new G4Tubs("sciHolderB_tubs",
          dimensionSciHolderB[0], dimensionSciHolderB[1] + 0.7 * CLHEP::cm * i, dimensionSciHolderB[2],
          0, 2. * M_PI);
      sciHolderB_log[i] = new G4LogicalVolume(sciHolderB_tubs[i],
          materials->aluminium_noOptical, "sciHolderB_log");
      sciHolderB_log[i]->SetVisAttributes(colour->gray);
    }

    for (G4int i = 0; i < 5; i++) {
      new G4PVPlacement(0, positionB + positionSciHolderB[i], sciHolderB_log[(i == 0 || i == 4)],
          "sciHolderB_phys", world_log, false, 0, checkOverlap);
    }

    sciHolderB_2_tubs = new G4Tubs("sciHolderB_2_tubs",
        dimensionSciHolderB_2[0], dimensionSciHolderB_2[1],
        dimensionSciHolderB_2[2], 0, 2. * M_PI);
    sciHolderB_2_log = new G4LogicalVolume(sciHolderB_2_tubs,
        materials->aluminium_noOptical, "sciHolderB_2_log");
    sciHolderB_2_log->SetVisAttributes(colour->gray);

    for (G4int i = 0; i < 3; i++) {
      new G4PVPlacement(0, positionB + positionSciHolderB[i+1], sciHolderB_2_log,
          "sciHolderB_2_phys", world_log, false, 0, checkOverlap);
    }

    // lgHolderB
    positionLgHolderB[0] = positionB
        + G4ThreeVector(0, 0, -10.0 * CLHEP::cm);
    positionLgHolderB[1] = positionB
        + G4ThreeVector(0, 0, lenghtB_ * 2. + 10.0 * CLHEP::cm);

    lgHolderB_tubs = new G4Tubs("lgHolderB_tubs", dimensionLgHolderB[0],
        dimensionLgHolderB[1], dimensionLgHolderB[2], 0, 2. * M_PI);
    lgHolderB_log = new G4LogicalVolume(lgHolderB_tubs,
        materials->aluminium_noOptical, "lgHolderB_log");
    lgHolderB_log->SetVisAttributes(colour->gray);

    for (G4int i = 0; i < 2; i++) {
      new G4PVPlacement(0, positionLgHolderB[i], lgHolderB_log,
          "lgHolderB_phys", world_log, false, 0, checkOverlap);
    }

    // crossbarB
    crossbarB_box = new G4Box("crossbarB_box", dimensionCrossbarB[0],
        dimensionCrossbarB[1], dimensionCrossbarB[2]);

    crossbar_sub.push_back(
        new G4SubtractionSolid("crossbar_sub", crossbarB_box,
            sciHolderB_tubs[1], 0,
            G4ThreeVector(0, -dimensionSciHolderB[0] - dimensionCrossbarB[1],
                -lenghtB_) + positionSciHolderB[0]));
    for (int i = 1; i < 5; i++) {
      crossbar_sub.push_back(
          new G4SubtractionSolid("crossbar_sub", crossbar_sub.back(),
              sciHolderB_tubs[(i == 4)], 0,
              G4ThreeVector(0, -dimensionSciHolderB[0] - dimensionCrossbarB[1],
                  -lenghtB_) + positionSciHolderB[i]));
    }

    for (int i = 1; i < 4; i++) {
      crossbar_sub.push_back(
          new G4SubtractionSolid("crossbar_sub", crossbar_sub.back(),
              sciHolderB_2_tubs, 0,
              G4ThreeVector(0, -dimensionSciHolderB[0] - dimensionCrossbarB[1],
                  -lenghtB_) + positionSciHolderB[i]));
    }

    crossbarB_log = new G4LogicalVolume(crossbar_sub.back(),
        materials->aluminium_noOptical, "crossbarB_log");
    crossbarB_log->SetVisAttributes(colour->gray);

    for (G4int i = 0; i < 8; i++) {
      rotationCrossbarB[i] = new CLHEP::HepRotation;
      rotationCrossbarB[i]->rotateZ(22.5 * CLHEP::deg + 45.0 * i * CLHEP::deg);

      positionCrossbarB[i] = G4ThreeVector(
          sin(22.5 * CLHEP::deg + M_PI / 4 * i)
              * (dimensionSciHolderB[0] + dimensionCrossbarB[1]),
          cos(22.5 * CLHEP::deg + M_PI / 4 * i)
              * (dimensionSciHolderB[0] + dimensionCrossbarB[1]), lenghtB_)
          + positionB;

      new G4PVPlacement(rotationCrossbarB[i], positionCrossbarB[i],
          crossbarB_log, "crossbarB_phys", world_log, false, 0, checkOverlap);
    }

    // anglePiece
    rotationAnglePiece[0] = new CLHEP::HepRotation;
    rotationAnglePiece[0]->rotateX(30.0 * CLHEP::deg);
    rotationAnglePiece[1] = new CLHEP::HepRotation;
    rotationAnglePiece[1]->rotateX(-30.0 * CLHEP::deg);

    boschAnglePiece_box = new G4Box("boschAnglePiece_box",
        dimensionBoschAnglePiece[0], dimensionBoschAnglePiece[1],
        dimensionBoschAnglePiece[2]);
    boxAnglePiece_box = new G4Box("boxAnglePiece_box",
        dimensionBoxAnglePiece[0], dimensionBoxAnglePiece[1],
        dimensionBoxAnglePiece[2]);

    for (int i = 0; i < 2; i++) {
      anglePiece_intersection[i] = new G4IntersectionSolid(
          "anglePiece_intersection", boxAnglePiece_box, boschAnglePiece_box,
          rotationAnglePiece[i],
          G4ThreeVector(0, -dimensionBoschAnglePiece[1], 0));
      anglePiece_log[i] = new G4LogicalVolume(anglePiece_intersection[i],
          materials->aluminium_noOptical, "anglePiece_log");
      anglePiece_log[i]->SetVisAttributes(colour->gray);
    }

    for (G4int i = 0; i < 16; i++) {
      if (i < 8) {
        positionAnglePiece[i] = G4ThreeVector(
            sin(22.5 * CLHEP::deg + M_PI / 4 * i)
                * (dimensionSciHolderB[0] + 2. * dimensionCrossbarB[1]
                    + dimensionBoxAnglePiece[1]),
            cos(22.5 * CLHEP::deg + M_PI / 4 * i)
                * (dimensionSciHolderB[0] + 2. * dimensionCrossbarB[1]
                    + dimensionBoxAnglePiece[1]),
            positionLgHolderB[1].z() - dimensionLgHolderB[2]
                - dimensionBoxAnglePiece[2]);
        new G4PVPlacement(rotationCrossbarB[i], positionAnglePiece[i],
            anglePiece_log[0], "anglePiece_phys", world_log, false, 0,
            checkOverlap);
      } else {
        positionAnglePiece[i] = G4ThreeVector(
            sin(22.5 * CLHEP::deg + M_PI / 4 * i)
                * (dimensionSciHolderB[0] + 2. * dimensionCrossbarB[1]
                    + dimensionBoxAnglePiece[1]),
            cos(22.5 * CLHEP::deg + M_PI / 4 * i)
                * (dimensionSciHolderB[0] + 2. * dimensionCrossbarB[1]
                    + dimensionBoxAnglePiece[1]),
            positionLgHolderB[0].z() + dimensionLgHolderB[2]
                + dimensionBoxAnglePiece[2]);
        new G4PVPlacement(rotationCrossbarB[i - 8], positionAnglePiece[i],
            anglePiece_log[1], "anglePiece_phys", world_log, false, 0,
            checkOverlap);
      }
    }

    // bosch profile
    positionBoschX[0] = positionB
        + G4ThreeVector(0, groundHeight_ + dimensionBoschX[1],
            60.5 * CLHEP::cm);
    positionBoschX[1] = positionB
        + G4ThreeVector(0, groundHeight_ + dimensionBoschX[1],
            299.5 * CLHEP::cm);
    positionBoschX[2] = positionB
        + G4ThreeVector(0,
            groundHeight_ + 20.0 * CLHEP::cm + dimensionBoschX[1],
            60.5 * CLHEP::cm);
    positionBoschX[3] = positionB
        + G4ThreeVector(0,
            groundHeight_ + 20.0 * CLHEP::cm + dimensionBoschX[1],
            299.5 * CLHEP::cm);
    positionBoschX[4] = positionB
        + G4ThreeVector(0, groundHeight_ + dimensionBoschX[1], lenghtB_);

    for (G4int i = 0; i < 5; i++) {
      G4double extraLength = 0;
      G4double extraLength_2 = 0;
      if (i < 2)
        extraLength = 22.0 * CLHEP::cm;
      if (i == 4) {
        extraLength = -60.5 * CLHEP::cm;
        extraLength_2 = -1.0 * CLHEP::cm;
      }
      boschX_box[i] = new G4Box("boschX_box", dimensionBoschX[0] + extraLength,
          dimensionBoschX[1] + extraLength_2,
          dimensionBoschX[2] + extraLength_2);
      boschX_log[i] = new G4LogicalVolume(boschX_box[i],
          materials->stainlessSteel, "boschX_log");
      new G4PVPlacement(0, positionBoschX[i], boschX_log[i], "boschX_phys",
          world_log, false, 0, checkOverlap);
      boschX_log[i]->SetVisAttributes(colour->gray);
    }

    positionBoschY[0] = positionB
        + G4ThreeVector(135.5 * CLHEP::cm,
            groundHeight_ + 12.0 * CLHEP::cm + dimensionBoschY[1],
            60.5 * CLHEP::cm);
    positionBoschY[1] = positionB
        + G4ThreeVector(-135.5 * CLHEP::cm,
            groundHeight_ + 12.0 * CLHEP::cm + dimensionBoschY[1],
            60.5 * CLHEP::cm);
    positionBoschY[2] = positionB
        + G4ThreeVector(135.5 * CLHEP::cm,
            groundHeight_ + 12.0 * CLHEP::cm + dimensionBoschY[1],
            299.5 * CLHEP::cm);
    positionBoschY[3] = positionB
        + G4ThreeVector(-135.5 * CLHEP::cm,
            groundHeight_ + 12.0 * CLHEP::cm + dimensionBoschY[1],
            299.5 * CLHEP::cm);

    boschY_box = new G4Box("boschY_box", dimensionBoschY[0],
        dimensionBoschY[1], dimensionBoschY[2]);
    boschY_log = new G4LogicalVolume(boschY_box,
        materials->stainlessSteel, "boschY_log");
    boschY_log->SetVisAttributes(colour->gray);

    for (G4int i = 0; i < 4; i++) {
      new G4PVPlacement(0, positionBoschY[i], boschY_log, "boschY_phys",
          world_log, false, 0, checkOverlap);
    }

    positionBoschZ[0] = positionB
        + G4ThreeVector(70.0 * CLHEP::cm + dimensionBoschZ[0],
            groundHeight_ + 1.0 * CLHEP::cm + dimensionBoschZ[1], lenghtB_);
    positionBoschZ[1] = positionB
        + G4ThreeVector(-70.0 * CLHEP::cm - dimensionBoschZ[0],
            groundHeight_ + 1.0 * CLHEP::cm + dimensionBoschZ[1], lenghtB_);
    positionBoschZ[2] = positionB
        + G4ThreeVector(131.0 * CLHEP::cm + dimensionBoschZ[0],
            groundHeight_ + 1.0 * CLHEP::cm + dimensionBoschZ[1], lenghtB_);
    positionBoschZ[3] = positionB
        + G4ThreeVector(-131.0 * CLHEP::cm - dimensionBoschZ[0],
            groundHeight_ + 1.0 * CLHEP::cm + dimensionBoschZ[1], lenghtB_);

    positionBoschZ[4] = positionB
        + G4ThreeVector(5.0 * CLHEP::cm + dimensionBoschZ[0],
            groundHeight_ + 20.0 * CLHEP::cm + dimensionBoschZ[1],
            lenghtB_);
    positionBoschZ[5] = positionB
        + G4ThreeVector(-5.0 * CLHEP::cm - dimensionBoschZ[0],
            groundHeight_ + 20.0 * CLHEP::cm + dimensionBoschZ[1],
            lenghtB_);

    for (G4int i = 0; i < 6; i++) {
      boschZ_box[i] = new G4Box("boschZ_box", dimensionBoschZ[0],
          dimensionBoschZ[1], dimensionBoschZ[2]);
      boschZ_log[i] = new G4LogicalVolume(boschZ_box[i],
          materials->stainlessSteel, "boschZ_log");
      new G4PVPlacement(0, positionBoschZ[i], boschZ_log[i], "boschZ_phys",
          world_log, false, 0, checkOverlap);
      boschZ_log[i]->SetVisAttributes(colour->gray);
    }

    // anglePieceBase
    rotationAnglePieceBase[0] = new CLHEP::HepRotation;
    rotationAnglePieceBase[0]->rotateX(45.0 * CLHEP::deg);
    rotationAnglePieceBase[1] = new CLHEP::HepRotation;
    rotationAnglePieceBase[1]->rotateX(-45.0 * CLHEP::deg);
    rotationAnglePieceBase[2] = new CLHEP::HepRotation;
    rotationAnglePieceBase[2]->rotateZ(45.0 * CLHEP::deg);
    rotationAnglePieceBase[3] = new CLHEP::HepRotation;
    rotationAnglePieceBase[3]->rotateZ(-45.0 * CLHEP::deg);

    positionAnglePieceBase[0] = positionBoschY[0]
        + G4ThreeVector(0, -dimensionBoschY[1] + dimensionBoxAnglePieceBase[1],
            dimensionBoschY[2] + dimensionBoxAnglePieceBase[2]);
    positionAnglePieceBase[1] = positionBoschY[1]
        + G4ThreeVector(0, -dimensionBoschY[1] + dimensionBoxAnglePieceBase[1],
            dimensionBoschY[2] + dimensionBoxAnglePieceBase[2]);
    positionAnglePieceBase[2] = positionBoschY[2]
        + G4ThreeVector(0, -dimensionBoschY[1] + dimensionBoxAnglePieceBase[1],
            -dimensionBoschY[2] - dimensionBoxAnglePieceBase[2]);
    positionAnglePieceBase[3] = positionBoschY[3]
        + G4ThreeVector(0, -dimensionBoschY[1] + dimensionBoxAnglePieceBase[1],
            -dimensionBoschY[2] - dimensionBoxAnglePieceBase[2]);
    positionAnglePieceBase[4] = positionBoschY[0]
        + G4ThreeVector(-dimensionBoschY[0] - dimensionBox2AnglePieceBase[0],
            8.0 * CLHEP::cm + 2. * dimensionBoschX[1]
                + dimensionBox2AnglePieceBase[1] - dimensionBoschY[1], 0);
    positionAnglePieceBase[5] = positionBoschY[2]
        + G4ThreeVector(-dimensionBoschY[0] - dimensionBox2AnglePieceBase[0],
            8.0 * CLHEP::cm + 2. * dimensionBoschX[1]
                + dimensionBox2AnglePieceBase[1] - dimensionBoschY[1], 0);
    positionAnglePieceBase[6] = positionBoschY[1]
        + G4ThreeVector(dimensionBoschY[0] + dimensionBox2AnglePieceBase[0],
            8.0 * CLHEP::cm + 2. * dimensionBoschX[1]
                + dimensionBox2AnglePieceBase[1] - dimensionBoschY[1], 0);
    positionAnglePieceBase[7] = positionBoschY[3]
        + G4ThreeVector(dimensionBoschY[0] + dimensionBox2AnglePieceBase[0],
            8.0 * CLHEP::cm + 2. * dimensionBoschX[1]
                + dimensionBox2AnglePieceBase[1] - dimensionBoschY[1], 0);

    boschAnglePieceBase_box = new G4Box("boschAnglePieceBase_box",
        dimensionBoschAnglePieceBase[0], dimensionBoschAnglePieceBase[1],
        dimensionBoschAnglePieceBase[2]);
    boxAnglePieceBase_box[0] = new G4Box("boschAnglePieceBase_box",
        dimensionBoxAnglePieceBase[0], dimensionBoxAnglePieceBase[1],
        dimensionBoxAnglePieceBase[2]);
    boxAnglePieceBase_box[1] = new G4Box("boschAnglePieceBase_box",
        dimensionBox2AnglePieceBase[0], dimensionBox2AnglePieceBase[1],
        dimensionBox2AnglePieceBase[2]);

    for (G4int i = 0; i < 8; i++) {
      G4int k = (0 == i % 2) ? i / 2 : (i - 1) / 2;
      anglePieceBase_intersection[i] = new G4IntersectionSolid(
          "anglePieceBase_intersection", boxAnglePieceBase_box[(i >= 4)],
          boschAnglePieceBase_box, rotationAnglePieceBase[k],
          G4ThreeVector(0, -dimensionBoschAnglePieceBase[0], 0));
      anglePieceBase_log[i] = new G4LogicalVolume(
          anglePieceBase_intersection[i], materials->stainlessSteel,
          "anglePieceBase_log");
      new G4PVPlacement(0, positionAnglePieceBase[i], anglePieceBase_log[i],
          "anglePieceBase_phys", world_log, false, 0, checkOverlap);
      anglePieceBase_log[i]->SetVisAttributes(colour->gray);
    }

    // littleBox
    positionLittleBox[0] = positionBoschX[0]
        + G4ThreeVector(0, dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[1] = positionBoschX[0]
        + G4ThreeVector(53.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[2] = positionBoschX[0]
        + G4ThreeVector(-53.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[3] = positionBoschX[1]
        + G4ThreeVector(0, dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[4] = positionBoschX[1]
        + G4ThreeVector(53.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[5] = positionBoschX[1]
        + G4ThreeVector(-53.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[6] = positionBoschX[4]
        + G4ThreeVector(9.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);
    positionLittleBox[7] = positionBoschX[4]
        + G4ThreeVector(-9.0 * CLHEP::cm,
            dimensionBoschX[1] + dimensionLittleBox, 0);

    littleBox_box = new G4Box("littleBox_box", dimensionLittleBox,
        dimensionLittleBox, dimensionLittleBox);
    littleBox_log = new G4LogicalVolume(littleBox_box,
        materials->stainlessSteel, "littleBox_log");
    littleBox_log->SetVisAttributes(colour->gray);

    for (G4int i = 0; i < 8; i++) {
      new G4PVPlacement(0, positionLittleBox[i], littleBox_log,
          "littleBox_phys", world_log, false, 0, checkOverlap);
    }
  }
}
