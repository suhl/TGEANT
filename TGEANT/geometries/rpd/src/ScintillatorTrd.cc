#include "ScintillatorTrd.hh"
#include "CameraConstruction.hh"

ScintillatorTrd::ScintillatorTrd(void)
{
  positionVector = G4ThreeVector(0, 0, 1. * CLHEP::m);
  sci_x_short = 28.96 / 2. * CLHEP::cm;
  sci_x_long = 30.28 / 2. * CLHEP::cm;
  sci_y = 5.0 / 2. * CLHEP::cm;
  sci_z = 360.0 / 2. * CLHEP::cm;

  bc408Mat = materials->bc408_noOptical;
  plexiglassMat = materials->plexiglass_noOptical;
  aluminiumMat = materials->aluminium_noOptical;
  airMat = materials->air_noOptical;

  useCap = true;
  channelNo = 0;

  sci_world_trd = NULL;
  plastic_trd = NULL;
  housing_trd = NULL;
  airGap_trd = NULL;
  scintillator_trd = NULL;

  sci_world_log = NULL;
  plastic_log = NULL;
  housing_log = NULL;
  airGap_log = NULL;
  scintillator_log = NULL;

  sci_world_phys = NULL;
  plastic_phys = NULL;
  housing_phys = NULL;
  airGap_phys = NULL;
  scintillator_phys = NULL;

  sciAir = NULL;
  airSci = NULL;
  airAl = NULL;
  sciAirGap = NULL;
  airGapSci = NULL;

  for (unsigned int i = 0; i < 4; i++) {
    capRotation[i] = NULL;

    plastic_cap_trap[i] = NULL;
    housing_cap_trap[i] = NULL;
    airGap_cap_trap[i] = NULL;

    plastic_cap_log[i] = NULL;
    housing_cap_log[i] = NULL;
    airGap_cap_log[i] = NULL;

    plastic_cap_phys[i] = NULL;
    housing_cap_phys[i] = NULL;
    airGap_cap_phys[i] = NULL;

    sciAirCap[i] = NULL;
    airSciCap[i] = NULL;
    airAlCap[i] = NULL;
  }

  detectorId = 0;
}

ScintillatorTrd::~ScintillatorTrd(void)
{
  if (sci_world_trd != NULL)
    delete sci_world_trd;
  if (plastic_trd != NULL)
    delete plastic_trd;
  if (housing_trd != NULL)
    delete housing_trd;
  if (airGap_trd != NULL)
    delete airGap_trd;
  if (scintillator_trd != NULL)
    delete scintillator_trd;

  if (sci_world_log != NULL)
    delete sci_world_log;
  if (plastic_log != NULL)
    delete plastic_log;
  if (housing_log != NULL)
    delete housing_log;
  if (airGap_log != NULL)
    delete airGap_log;
  if (scintillator_log != NULL)
    delete scintillator_log;

  if (sciAir != NULL)
    delete sciAir;
  if (airSci != NULL)
    delete airSci;
  if (airAl != NULL)
    delete airAl;
  if (sciAirGap != NULL)
    delete sciAirGap;
  if (airGapSci != NULL)
    delete airGapSci;

  for (unsigned int i = 0; i < 4; i++) {
    if (capRotation[i] != NULL)
      delete capRotation[i];

    if (plastic_cap_trap[i] != NULL)
      delete plastic_cap_trap[i];
    if (housing_cap_trap[i] != NULL)
      delete housing_cap_trap[i];
    if (airGap_cap_trap[i] != NULL)
      delete airGap_cap_trap[i];

    if (plastic_cap_log[i] != NULL)
      delete plastic_cap_log[i];
    if (housing_cap_log[i] != NULL)
      delete housing_cap_log[i];
    if (airGap_cap_log[i] != NULL)
      delete airGap_cap_log[i];

    if (sciAirCap[i] != NULL)
      delete sciAirCap[i];
    if (airSciCap[i] != NULL)
      delete airSciCap[i];
    if (airAlCap[i] != NULL)
      delete airAlCap[i];
  }
}

void ScintillatorTrd::construct(G4LogicalVolume* world_log)
{
  rotationMatrix.rotateX(90.0 * CLHEP::deg);

  CameraConstruction camera;
  G4double airGap = camera.getAirGap();
  G4double aluFoil = camera.getAluFoil();
  G4double plasticFoil = camera.getPlasticFoil();

  G4double dx_short[4] = { sci_x_short + airGap + aluFoil + plasticFoil,
      sci_x_short + airGap + aluFoil, sci_x_short + airGap, sci_x_short };

  G4double dx_long[4] = { sci_x_long + airGap + aluFoil + plasticFoil,
      sci_x_long + airGap + aluFoil, sci_x_long + airGap, sci_x_long };

  G4double dy[4] = { sci_y + airGap + aluFoil + plasticFoil, sci_y + airGap
      + aluFoil, sci_y + airGap, sci_y };

  sci_world_trd = new G4Trd("sci_world_trd", dx_short[0], dx_long[0],
      sci_z + airGap + aluFoil + plasticFoil,
      sci_z + airGap + aluFoil + plasticFoil, dy[0]);
  plastic_trd = new G4Trd("plastic_trap", dx_short[0], dx_long[0], sci_z, sci_z,
      dy[0]);
  housing_trd = new G4Trd("housing_trap", dx_short[1], dx_long[1], sci_z, sci_z,
      dy[1]);
  airGap_trd = new G4Trd("airGap_trap", dx_short[2], dx_long[2], sci_z, sci_z,
      dy[2]);
  scintillator_trd = new G4Trd("scintillator_trd", dx_short[3], dx_long[3],
      sci_z, sci_z, dy[3]);

  sci_world_log = new G4LogicalVolume(sci_world_trd, plexiglassMat,
      "sci_world_log", 0, 0, 0);
  plastic_log = new G4LogicalVolume(plastic_trd, materials->polypropylene,
      "plastic_log", 0, 0, 0);
  housing_log = new G4LogicalVolume(housing_trd, aluminiumMat,
      "housing_log", 0, 0, 0);
  airGap_log = new G4LogicalVolume(airGap_trd, airMat, "airGap_log", 0,
      0, 0);
  scintillator_log = new G4LogicalVolume(scintillator_trd, bc408Mat,
      "scintillator_log", 0, 0, 0);

  sci_world_phys = new G4PVPlacement(&rotationMatrix, positionVector,
      sci_world_log, name + intToStr(channelNo), world_log, false, 0, checkOverlap);
  plastic_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), plastic_log,
      "plastic_phys", sci_world_log, false, 0, false);
  housing_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), housing_log,
      "housing_phys", plastic_log, false, 0, false);
  airGap_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0), airGap_log,
      "airGap_phys", housing_log, false, 0, false);
  scintillator_phys = new G4PVPlacement(0, G4ThreeVector(0, 0, 0),
      scintillator_log, "scintillator_phys", airGap_log, false, 0,
      false);

  plastic_log->SetVisAttributes(colour->yellow);

  if (useCap) {
    G4double cap_dx_long[3];
    G4double cap_dx_short[3];
    G4double cap_dz[3] = { airGap + aluFoil + plasticFoil, airGap + aluFoil,
        airGap };
    for (G4int i = 0; i < 3; i++) {
      cap_dx_long[i] = dx_long[i] - sci_x_short;
      cap_dx_short[i] = dx_short[i] - sci_x_short;
    }

    for (G4int i = 0; i < 4; i++) {
      plastic_cap_trap[i] = new G4Trap("plastic_cap_trap", cap_dz[0], 2. * dy[0],
          cap_dx_long[0], cap_dx_short[0]);
      housing_cap_trap[i] = new G4Trap("housing_cap_trap", cap_dz[1], 2. * dy[1],
          cap_dx_long[1], cap_dx_short[1]);
      airGap_cap_trap[i] = new G4Trap("airGap_cap_trap", cap_dz[2], 2. * dy[2],
          cap_dx_long[2], cap_dx_short[2]);

      plastic_cap_log[i] = new G4LogicalVolume(plastic_cap_trap[i],
          materials->polypropylene, "plastic_cap_log", 0, 0, 0);
      housing_cap_log[i] = new G4LogicalVolume(housing_cap_trap[i],
          aluminiumMat, "housing_cap_log", 0, 0, 0);
      airGap_cap_log[i] = new G4LogicalVolume(airGap_cap_trap[i],
          airMat, "airGap_cap_log", 0, 0, 0);
    }

    G4ThreeVector plastic_cap_position[4];
    plastic_cap_position[0] = G4ThreeVector(
        sci_x_short + (cap_dx_long[0] + cap_dx_short[0]) / 4.,
        sci_z + plasticFoil / 2. + aluFoil / 2. + airGap / 2., 0);
    plastic_cap_position[1] = G4ThreeVector(
        -sci_x_short - (cap_dx_long[0] + cap_dx_short[0]) / 4,
        sci_z + plasticFoil / 2. + aluFoil / 2. + airGap / 2., 0);
    plastic_cap_position[2] = G4ThreeVector(
        sci_x_short + (cap_dx_long[0] + cap_dx_short[0]) / 4.,
        -sci_z - plasticFoil / 2. - aluFoil / 2. - airGap / 2., 0);
    plastic_cap_position[3] = G4ThreeVector(
        -sci_x_short - (cap_dx_long[0] + cap_dx_short[0]) / 4.,
        -sci_z - plasticFoil / 2. - aluFoil / 2. - airGap / 2., 0);

    G4ThreeVector housing_cap_position[4];
    housing_cap_position[0] = G4ThreeVector(-plasticFoil / 2., 0,
        -plasticFoil / 2.);
    housing_cap_position[1] = G4ThreeVector(-plasticFoil / 2., 0,
        plasticFoil / 2.);
    housing_cap_position[2] = G4ThreeVector(-plasticFoil / 2., 0,
        plasticFoil / 2.);
    housing_cap_position[3] = G4ThreeVector(-plasticFoil / 2., 0,
        -plasticFoil / 2.);

    G4ThreeVector airGap_cap_position[4];
    airGap_cap_position[0] = G4ThreeVector(-aluFoil / 2., 0, -aluFoil / 2.);
    airGap_cap_position[1] = G4ThreeVector(-aluFoil / 2., 0, aluFoil / 2.);
    airGap_cap_position[2] = G4ThreeVector(-aluFoil / 2., 0, aluFoil / 2.);
    airGap_cap_position[3] = G4ThreeVector(-aluFoil / 2., 0, -aluFoil / 2.);

    for (G4int i = 0; i < 4; i++) {
      capRotation[i] = new CLHEP::HepRotation;
      capRotation[i]->rotateX(90.0 * CLHEP::deg);
    }
    capRotation[1]->rotateY(180.0 * CLHEP::deg);
    capRotation[3]->rotateY(180.0 * CLHEP::deg);

    for (G4int i = 0; i < 4; i++) {
      plastic_cap_phys[i] = new G4PVPlacement(capRotation[i],
          plastic_cap_position[i], plastic_cap_log[i], "plastic_cap_phys",
          sci_world_log, false, 0, checkOverlap);
      housing_cap_phys[i] = new G4PVPlacement(0, housing_cap_position[i],
          housing_cap_log[i], "housing_cap_phys", plastic_cap_log[i], false, 0,
          false);
      airGap_cap_phys[i] = new G4PVPlacement(0, airGap_cap_position[i],
          airGap_cap_log[i], "airGap_cap_phys", housing_cap_log[i], false, 0,
          false);

      plastic_cap_log[i]->SetVisAttributes(colour->blue);
    }
  }

  if (settingsFile->isOpticalPhysicsActivated()) {
    sciAir = new G4LogicalBorderSurface("sciAir", scintillator_phys,
        airGap_phys, materials->surfaceBc408Air);
    airSci = new G4LogicalBorderSurface("airSci", airGap_phys,
        scintillator_phys, materials->surfaceAirBc408);
    airAl = new G4LogicalBorderSurface("airAl", airGap_phys, housing_phys,
        materials->surfaceAluminium);
    sciAirGap = new G4LogicalBorderSurface("sciPlexi", scintillator_phys,
        sci_world_phys, materials->surfaceBc408Plexiglass);
    airGapSci = new G4LogicalBorderSurface("sciPlexi", sci_world_phys,
        scintillator_phys, materials->surfaceAirPlexiglass);

    if (useCap) {
      for (G4int i = 0; i < 4; i++) {
        sciAirCap[i] = new G4LogicalBorderSurface("sciAirCap",
            scintillator_phys, airGap_cap_phys[i], materials->surfaceBc408Air);
        airSciCap[i] = new G4LogicalBorderSurface("airSciCap",
            airGap_cap_phys[i], scintillator_phys, materials->surfaceAirBc408);
        airAlCap[i] = new G4LogicalBorderSurface("airAlCap", airGap_cap_phys[i],
            housing_cap_phys[i], materials->surfaceAluminium);
      }
    }
  }

  createSensitiveDetector(name, channelNo, detectorId);
}

void ScintillatorTrd::setDimensions(G4double _sci_x_short, G4double _sci_x_long,
    G4double _sci_y, G4double _sci_z)
{
  sci_x_short = _sci_x_short;
  sci_x_long = _sci_x_long;
  sci_y = _sci_y;
  sci_z = _sci_z;
}

void ScintillatorTrd::createSensitiveDetector(G4String name, G4int channelNo,
    G4int detectorId)
{
  scintillator_log->SetSensitiveDetector(
      new T4SensitiveDetector(name, channelNo, TGEANT::HIT, detectorId));
}

