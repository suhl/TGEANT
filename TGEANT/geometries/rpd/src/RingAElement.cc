#include "RingAElement.hh"
#include "CameraConstruction.hh"

RingAElement::RingAElement(void)
{
  name = "A";

  sci_x_short = 6.5 / 2 * CLHEP::cm;
  sci_x_long = 6.6 / 2 * CLHEP::cm; // angle: 7.5 deg
  sci_y = 0.4 / 2 * CLHEP::cm;

  CameraConstruction camera;
  sci_z = camera.getLenghtA();

  T4PhotoMultiplier photoMultiplier;
  pmtRadius = photoMultiplier.getPmtRadius(TGEANT::R10533);
  pmtLength = photoMultiplier.getPmtLength(TGEANT::R10533);
  outerLgRadius = 28.0 * CLHEP::cm;
  upperLgLength = 10.0 / 2 * CLHEP::cm;

  lightGuide_up = NULL;
  lightGuide_down = NULL;

  angle = 0;
  slope = 0;
}

RingAElement::~RingAElement(void)
{
  if (lightGuide_up != NULL) delete lightGuide_up;
  if (lightGuide_down != NULL) delete lightGuide_down;
}

void RingAElement::construct(G4LogicalVolume* world_log)
{
  G4bool useCalibration = (slope != 0);
  if (!useCalibration) {
    scintillatorTrd = new ScintillatorTrd();
  } else {
    scintillatorTrd = new ScintillatorGenericTrap();
    ScintillatorGenericTrap* tmp = (ScintillatorGenericTrap*) scintillatorTrd;
    tmp->setSlope(slope);
    tmp->setRotAngle(angle);
  }
  scintillatorTrd->setRotation(rotationMatrix);
  scintillatorTrd->setDimensions(sci_x_short, sci_x_long, sci_y, sci_z);
  scintillatorTrd->setPosition(positionVector);
  scintillatorTrd->setName(name);
  scintillatorTrd->setChannelNo(channelNo);
  scintillatorTrd->setDetectorId(detectorId);
  if (useOptical)
    scintillatorTrd->useOpticalPhysics();
  scintillatorTrd->construct(world_log);
  regionManager->addToOpticalRegion(scintillatorTrd->getVolume());

  G4TwoVector helperGlobal = G4TwoVector(positionVector.x(),
      positionVector.y());
  G4TwoVector helperUp = helperGlobal;
  G4TwoVector helperDn = helperGlobal;

  if (useCalibration) {
    G4double phi = slope * sci_z / CLHEP::cm;
    rotationMatrixUp.rotateZ(angle - phi);
    rotationMatrixDn.rotateZ(angle + phi);
    helperUp.rotate(phi);
    helperDn.rotate(-phi);
  } else {
    rotationMatrixUp = rotationMatrix;
    rotationMatrixDn = rotationMatrix;
  }
  helperUp -= helperGlobal;
  helperDn -= helperGlobal;

  lightGuide_up = new LightGuideRingB();
  lightGuide_up->setPosition(positionVector + G4ThreeVector(helperUp.x(), helperUp.y(), -sci_z));
  lightGuide_up->setDimensions(sci_x_short, sci_y, pmtRadius, upperLgLength,
      outerLgRadius);
  lightGuide_up->setAlignment(TGEANT::UP);
  lightGuide_up->setRotation(rotationMatrixUp);
  if (useOptical)
    lightGuide_up->useOpticalPhysics();
  lightGuide_up->construct(world_log);
  regionManager->addToOpticalRegion(lightGuide_up->getVolume());

  G4double lgangle = 43.0 * CLHEP::deg;
  lightGuide_down = new LightGuideRingA();
  lightGuide_down->setPosition(positionVector + G4ThreeVector(helperDn.x(), helperDn.y(), sci_z));
  lightGuide_down->setRotation(rotationMatrixDn);
  lightGuide_down->setAngle(lgangle);
  if (useOptical)
    lightGuide_down->useOpticalPhysics();
  lightGuide_down->construct(world_log);
  regionManager->addToOpticalRegion(lightGuide_down->getVolume());

  photoMultiplier_up = new T4PhotoMultiplier();
  photoMultiplier_up->setPosition(lightGuide_up->getPmtPosition(pmtLength));
  photoMultiplier_up->setAlignment(TGEANT::UP);
  photoMultiplier_up->setPmtType(TGEANT::R10533);
  photoMultiplier_up->rotateZ(
      -(rotationMatrixUp.getPhi() + rotationMatrixUp.getPsi()));
  photoMultiplier_up->rotateX(-90.0 * CLHEP::deg);
  photoMultiplier_up->setSensitiveDetector(name + "u", channelNo);
  if (useOptical)
    photoMultiplier_up->useOpticalPhysics();
  photoMultiplier_up->construct(world_log);
  regionManager->addToOpticalRegion(photoMultiplier_up->getVolume());

  photoMultiplier_down = new T4PhotoMultiplier();
  photoMultiplier_down->setPosition(lightGuide_down->getPmtPosition(pmtLength));
  photoMultiplier_down->setAlignment(TGEANT::DOWN);
  photoMultiplier_down->setPmtType(TGEANT::R10533);
  photoMultiplier_down->rotateZ(
      -(rotationMatrixDn.getPhi() + rotationMatrixDn.getPsi()));
  photoMultiplier_down->rotateX(lgangle);
  photoMultiplier_down->setSensitiveDetector(name + "d", channelNo);
  if (useOptical)
    photoMultiplier_down->useOpticalPhysics();
  photoMultiplier_down->construct(world_log);
  regionManager->addToOpticalRegion(photoMultiplier_down->getVolume());
}
