#include "RingElementBackEnd.hh"

RingElementBackEnd::RingElementBackEnd(void)
{
  name = "RingBackEnd";
  channelNo = 0;
  useOptical = false;

  sci_x_short = 0;
  sci_x_long = 0;
  sci_y = 0;
  sci_z = 0;
  pmtRadius = 0;
  pmtLength = 0;
  outerLgRadius = 0;
  upperLgLength = 0;

  scintillatorTrd = NULL;
  photoMultiplier_up = NULL;
  photoMultiplier_down = NULL;

  detectorId = 0;
}

RingElementBackEnd::~RingElementBackEnd(void)
{
  if (scintillatorTrd != NULL) delete scintillatorTrd;
  if (photoMultiplier_up != NULL) delete photoMultiplier_up;
  if (photoMultiplier_down != NULL) delete photoMultiplier_down;
}
