#!/usr/bin/python3


import sys, os, argparse

def main():
  path = os.path.dirname(os.path.realpath(__file__))
  

  tgeantPath = path.replace("/bin","")

  parser = argparse.ArgumentParser(description='Return TGEANT installation details')
  parser.add_argument("-I","--include",action="store_true", help="Returns the includes")
  parser.add_argument("-L","--libraries",action="store_true", help="Returns the libaries")
  parser.add_argument("-P","--prefix",action="store_true", help="Returns the libaries")
  
  args = parser.parse_args()
  
  f = open(tgeantPath+"/resources/xercesPath", 'r')
  
  xercesDir=f.read()
  #print(xercesDir)
  includeLine="-I"+xercesDir+"/include"
  librariesList="-L"+tgeantPath+"/lib -lT4Event -lT4Settings "+"-L"+xercesDir+"/lib -lxerces-c"

  if args.include:
    print(includeLine)
    
  if args.libraries:
    print(librariesList)
  
  if args.prefix:
    print(tgeantPath)
    

if __name__ == "__main__":
  main()
