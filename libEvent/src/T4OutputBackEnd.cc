#include "T4OutputBackEnd.hh"

#include "T4SMessenger.hh"
#include "T4SGlobals.hh"

void T4OutputBackEnd::checkFile(void)
{
  bool good = true;
  while (true) {
    if (!fileExists(saveFileName))
      break;

    saveFileName += "_";
    good = false;
  }

  if (!good)
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__, "T4OutputBackEnd::checkFile: File already exists!");
}
