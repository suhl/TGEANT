#include "T4OutputASCII.hh"
#include "T4SMessenger.hh"
#include "T4SGlobals.hh"


T4OutputASCII::T4OutputASCII(std::string saveFileName_)
{
  file_load = NULL;
  saveFileName = saveFileName_ + ".tgeant";

  checkFile();
  file_save.open(saveFileName.c_str());
  if (!file_save.good()) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
    __FILE__, "T4OutputASCII: Output file has not been created!");
    return;
  }
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4OutputASCII: Output file '" + saveFileName + "' has been created.");

  time_t rawtime;
  time(&rawtime);

  std::string hostName = "unknown";
  if (getenv("HOSTNAME") != NULL)
    hostName = getenv("HOSTNAME");
  file_save << "*TGEANT ASCII OUTPUT FILE*" << std::endl << "*TIME "
      << ctime(&rawtime) << "*HOST " << hostName << std::endl;

  // legend:
  file_save
      << "*TGEANT HIT: DetName, DetId, Ch, trackId, particleId, particleEnergy, time, beta, eDep, hitPos, primHit, lastHit, momentum"
      << std::endl;
  file_save
      << "*TGEANT RICH: cathodeNumber, photonHitPosition, photonProductionPosition, momentumMotherParticle, photonEnergy, time, xPadPosition, yPadPosition, cerenkovAngle"
      << std::endl;

  eventNo = 0;

  
  
  saveFileNameOriginal = saveFileName;
}

void T4OutputASCII::close(void)
{
  file_save.close();
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4OutputASCII::close: Output saved as " + saveFileName);

  if (useSplitting) {
    std::string commandCh = (string) "chmod 775 " + saveFileName;
    system(commandCh.c_str());

    std::string command = (string) "echo " + saveFileName + " >> "+saveFileNameOriginal+".lst";
    if (useSplitPiping)
      system(command.c_str());
  }
}

void T4OutputASCII::streamReset(void)
{
  file_load->seekg(0);
}

bool T4OutputASCII::streamLoad(std::string fileName)
{
  if (streamT4Event == NULL)
    streamT4Event = new T4Event;
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Load " + fileName);
  if (file_load == NULL)
    file_load = new igzstream;
  file_load->open(fileName.c_str());
  if (!file_load->good()) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4OutputASCII::streamLoad: Unknown file name: \""+fileName+"\"");
    return false;
  }
  else
    return true;
}

bool T4OutputASCII::streamGetNextEvent(void)
{
  using namespace std;

  streamT4Event->clear();
  std::string keyword;
  bool isEvent = false;

  while (file_load->good()) {
    *file_load >> keyword;
    if (keyword == "#EVENT") {
      isEvent = true;
      *file_load >> keyword; // this is the event number
      loadASCII_EventData(streamT4Event);
    }

    if (keyword == "#BEAMDATA")
      loadASCII_BeamData(&streamT4Event->beamData);

    // all tracking detectors
    if (keyword == "#TRACKING")
      loadASCII_Hit(&streamT4Event->tracking);

    // all trigger detectors
    if (keyword == "#TRIGGER")
      loadASCII_Hit(&streamT4Event->trigger);

    // all calorimeter detectors
    if (keyword == "#CALORIMETER")
      loadASCII_Hit(&streamT4Event->calorimeter);

    // rich detector
    if (keyword == "#RICH")
      loadASCII_Rich(&streamT4Event->rich);

    // all pmt
    if (keyword == "#PMT")
      loadASCII_Pmt(&streamT4Event->pmt);

    if (isEvent && keyword == "#EVENT_END")
      return true;
  }
  return false;
}

void T4OutputASCII::load(std::string fileName, std::vector<T4Event>* t4Run)
{
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Load " + fileName);
  if (file_load == NULL)
    file_load = new igzstream;
  file_load->open(fileName.c_str());
  if (!file_load->good()) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4OutputASCII::load: Unknown file name.");
    return;
  }

  T4Event t4Event;
  std::string keyword;
  bool isEvent = false;

  while (file_load->good()) {
    *file_load >> keyword;

    if (keyword == "#EVENT") {
      isEvent = true;
      *file_load >> keyword; // this is the event number
      loadASCII_EventData(&t4Event);
    }

    if (keyword == "#BEAMDATA")
      loadASCII_BeamData(&t4Event.beamData);

    // all tracking detectors
    if (keyword == "#TRACKING")
      loadASCII_Hit(&t4Event.tracking);

    // all trigger detectors
    if (keyword == "#TRIGGER")
      loadASCII_Hit(&t4Event.trigger);

    // all calorimeter detectors
    if (keyword == "#CALORIMETER")
      loadASCII_Hit(&t4Event.calorimeter);

    // rich detector
    if (keyword == "#RICH")
      loadASCII_Rich(&t4Event.rich);

    // all pmt
    if (keyword == "#PMT")
      loadASCII_Pmt(&t4Event.pmt);

    if (isEvent && keyword == "#EVENT_END") {
      t4Run->push_back(t4Event);

      // clear all vectors:
      t4Event.clear();
      isEvent = false;
    }
  }

  file_load->close();
}

void T4OutputASCII::save(void)
{
  if (useSplitting) {
    static int entry = 0;
    if (eventNo % eventPerChunk == 0 && eventNo != 0) {
      entry++;
      close();

      std::stringstream sstr;
      sstr << entry;
      saveFileName = saveFileNameOriginal + sstr.str();

      file_save.open(saveFileName.c_str());
      if (!file_save.good()) {
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
        __FILE__, "T4OutputASCII: FIFO output file has not been created!");
        return;
      }
      T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
          "T4OutputASCII: Output file '" + saveFileName
              + "' has been created.");

      time_t rawtime;
      time(&rawtime);

      std::string hostName = "unknown";
      if (getenv("HOSTNAME") != NULL)
        hostName = getenv("HOSTNAME");
      file_save << "*TGEANT ASCII OUTPUT FILE*" << std::endl << "*TIME "
          << ctime(&rawtime) << "*HOST " << hostName << std::endl;

      // legend:
      file_save
          << "*TGEANT HIT: DetName, DetId, Ch, trackId, particleId, particleEnergy, time, beta, eDep, hitPos, primHit, lastHit, momentum"
          << std::endl;
      file_save
          << "*TGEANT RICH: cathodeNumber, photonHitPosition, photonProductionPosition, momentumMotherParticle, photonEnergy, time, xPadPosition, yPadPosition, cerenkovAngle"
          << std::endl;
    }
  }

  file_save << std::setprecision(9);
  saveASCII_EventData(t4Event);
  eventNo++;
  saveASCII_BeamData(&t4Event->beamData);

  // all tracking detectors
  if (t4Event->tracking.size() > 0) {
    file_save << "#TRACKING";
    saveASCII_Hit(&t4Event->tracking);
  }

  // all trigger detectors
  if (t4Event->trigger.size() > 0) {
    file_save << "#TRIGGER";
    saveASCII_Hit(&t4Event->trigger);
  }

  // all calorimeter detectors
  if (t4Event->calorimeter.size() > 0) {
    file_save << "#CALORIMETER";
    saveASCII_Hit(&t4Event->calorimeter);
  }

  // rich detector
  if (t4Event->rich.size() > 0) {
    file_save << "#RICH";
    saveASCII_Rich(&t4Event->rich);
  }

  // all pmt
  if (t4Event->pmt.size() > 0) {
    file_save << "#PMT";
    saveASCII_Pmt(&t4Event->pmt);
  }

  file_save << "#EVENT_END" << std::endl;
}

void T4OutputASCII::saveASCII_EventData(T4Event* input)
{
  file_save << "#EVENT" << "\t" << eventNo << "\t" << input->processingTime
      << "\t" << input->trigMask << std::endl;
}

void T4OutputASCII::loadASCII_EventData(T4Event* input)
{
  *file_load >> input->processingTime >> input->trigMask;
}

void T4OutputASCII::saveASCII_BeamData(T4BeamData* t4BeamData)
{
  file_save << "#BEAMDATA" << "\t" << t4BeamData->vertexPosition[0] << "\t"
      << t4BeamData->vertexPosition[1] << "\t"
      << t4BeamData->vertexPosition[2] << "\t" << t4BeamData->vertexTime
      << "\t" << t4BeamData->generator << "\t" << t4BeamData->aux << "\t"
      << t4BeamData->x_bj << "\t" << t4BeamData->y << "\t" << t4BeamData->w2
      << "\t" << t4BeamData->q2 << "\t" << t4BeamData->nu << "\t";

  for (unsigned int i = 0; i < 20; i++)
    file_save << t4BeamData->uservar[i] << "\t";
  // !PYTHIA
  if (t4BeamData->generator != 1) {
    for (unsigned int i = 0; i < 40; i++)
      file_save << t4BeamData->lst[i] << "\t";
    for (unsigned int i = 0; i < 30; i++)
      file_save << t4BeamData->parl[i] << "\t";
    for (unsigned int i = 0; i < 14; i++)
      file_save << t4BeamData->cut[i] << "\t";
  }
  file_save << std::endl;

  file_save << t4BeamData->nBeamParticle << "\t";
  for (unsigned int i = 0; i < t4BeamData->beamParticles.size(); i++) {
    for (unsigned int j = 0; j < 5; j++)
      file_save << t4BeamData->beamParticles.at(i).k[j] << "\t";
    for (unsigned int j = 0; j < 5; j++)
      file_save << t4BeamData->beamParticles.at(i).p[j] << "\t";

    if (i % 10 == 0 && i != 0)
      file_save << std::endl;
  }
  file_save << std::endl;

  file_save << t4BeamData->nTrajectories << "\t";
  for (unsigned int i = 0; i < t4BeamData->trajectories.size(); i++) {
    file_save << t4BeamData->trajectories.at(i).trackId << "\t"
        << t4BeamData->trajectories.at(i).parentId << "\t"
        << t4BeamData->trajectories.at(i).particleId << "\t";
    for (unsigned int j = 0; j < 3; j++)
      file_save << t4BeamData->trajectories.at(i).position[j] << "\t";
    file_save << t4BeamData->trajectories.at(i).time << "\t";
    for (unsigned int j = 0; j < 3; j++)
      file_save << t4BeamData->trajectories.at(i).momentum[j] << "\t";

    if (i % 10 == 0 && i != 0)
      file_save << std::endl;
  }
  file_save << std::endl;

  // PYTHIA
  if (t4BeamData->generator == 1) {
    file_save << t4BeamData->pysubs.msel << "\t" << t4BeamData->pysubs.mselpd
        << "\t";
    for (unsigned int i = 0; i < 500; i++)
      file_save << t4BeamData->pysubs.msub[i] << "\t";
    file_save << std::endl;
    for (unsigned int i = 0; i < 2; i++)
      for (unsigned int j = 0; j < 80; j++)
        file_save << t4BeamData->pysubs.kfin[i][j] << "\t";
    for (unsigned int i = 0; i < 200; i++)
      file_save << t4BeamData->pysubs.ckin[i] << "\t";
    file_save << std::endl;

    for (unsigned int i = 0; i < 200; i++)
      file_save << t4BeamData->pypars.mstp[i] << "\t";
    file_save << std::endl;
    for (unsigned int i = 0; i < 200; i++)
      file_save << t4BeamData->pypars.parp[i] << "\t";
    file_save << std::endl;
    for (unsigned int i = 0; i < 200; i++)
      file_save << t4BeamData->pypars.msti[i] << "\t";
    file_save << std::endl;
    for (unsigned int i = 0; i < 200; i++)
      file_save << t4BeamData->pypars.pari[i] << "\t";
    file_save << std::endl;
  }
}

void T4OutputASCII::loadASCII_BeamData(T4BeamData* input)
{
  double dx, dy, dz;
  *file_load >> dx >> dy >> dz >> input->vertexTime >> input->generator
      >> input->aux >> input->x_bj >> input->y >> input->w2 >> input->q2
      >> input->nu;

  setThreeVector<double>(input->vertexPosition,dx,dy,dz);
  
  for (unsigned int i = 0; i < 20; i++)
    *file_load >> input->uservar[i];
  // !PYTHIA
  if (input->generator != 1) {
    for (unsigned int i = 0; i < 40; i++)
      *file_load >> input->lst[i];
    for (unsigned int i = 0; i < 30; i++)
      *file_load >> input->parl[i];
    for (unsigned int i = 0; i < 14; i++)
      *file_load >> input->cut[i];
  }

  *file_load >> input->nBeamParticle;
  for (unsigned int i = 0; i < input->nBeamParticle; i++) {
    T4BeamParticle beamParticle;
    for (unsigned int j = 0; j < 5; j++)
      *file_load >> beamParticle.k[j];
    for (unsigned int j = 0; j < 5; j++)
      *file_load >> beamParticle.p[j];
    input->beamParticles.push_back(beamParticle);
  }

  *file_load >> input->nTrajectories;
  for (unsigned int i = 0; i < input->nTrajectories; i++) {
    T4Trajectory t4Trajectory;
    *file_load >> t4Trajectory.trackId >> t4Trajectory.parentId
        >> t4Trajectory.particleId;
    for (unsigned int j = 0; j < 3; j++)
      *file_load >> t4Trajectory.position[j];
    *file_load >> t4Trajectory.time;
    for (unsigned int j = 0; j < 3; j++)
      *file_load >> t4Trajectory.momentum[j];
    input->trajectories.push_back(t4Trajectory);
  }

  // PYTHIA
  if (input->generator == 1) {
    *file_load >> input->pysubs.msel >> input->pysubs.mselpd;
    for (unsigned int i = 0; i < 500; i++)
      *file_load >> input->pysubs.msub[i];
    for (unsigned int i = 0; i < 2; i++)
      for (unsigned int j = 0; j < 80; j++)
        *file_load >> input->pysubs.kfin[i][j];
    for (unsigned int i = 0; i < 200; i++)
      *file_load >> input->pysubs.ckin[i];

    for (unsigned int i = 0; i < 200; i++)
      *file_load >> input->pypars.mstp[i];
    for (unsigned int i = 0; i < 200; i++)
      *file_load >> input->pypars.parp[i];
    for (unsigned int i = 0; i < 200; i++)
      *file_load >> input->pypars.msti[i];
    for (unsigned int i = 0; i < 200; i++)
      *file_load >> input->pypars.pari[i];
  }
}

void T4OutputASCII::saveASCII_Hit(std::vector<T4HitData>* input)
{
  file_save << "\t" << input->size() << std::endl;
  for (unsigned int i = 0; i < input->size(); i++) {
    file_save << input->at(i).detectorName << "\t" << input->at(i).detectorId
        << "\t" << input->at(i).channelNo << "\t" << input->at(i).trackId
        << "\t" << input->at(i).particleId << "\t"
        << input->at(i).particleEnergy << "\t" << input->at(i).time << "\t"
        << input->at(i).beta << "\t" << input->at(i).energyDeposit << "\t"
        << input->at(i).hitPosition[0] << "\t" << input->at(i).hitPosition[1]
        << "\t" << input->at(i).hitPosition[2] << "\t"
        << input->at(i).primaryHitPosition[0] << "\t"
        << input->at(i).primaryHitPosition[1] << "\t"
        << input->at(i).primaryHitPosition[2] << "\t"
        << input->at(i).lastHitPosition[0] << "\t"
        << input->at(i).lastHitPosition[1] << "\t"
        << input->at(i).lastHitPosition[2] << "\t" << input->at(i).momentum[0]
        << "\t" << input->at(i).momentum[1] << "\t"
        << input->at(i).momentum[2] << std::endl;
  }
}

void T4OutputASCII::loadASCII_Hit(std::vector<T4HitData>* input)
{
  int nChannel;
  *file_load >> nChannel;
  for (int i = 0; i < nChannel; i++) {
    double hitX, hitY, hitZ, primX, primY, primZ, lastX, lastY, lastZ,
        momentumX, momentumY, momentumZ;
    T4HitData hitData;
    *file_load >> hitData.detectorName >> hitData.detectorId
        >> hitData.channelNo >> hitData.trackId >> hitData.particleId
        >> hitData.particleEnergy >> hitData.time >> hitData.beta
        >> hitData.energyDeposit >> hitX >> hitY >> hitZ >> primX >> primY
        >> primZ >> lastX >> lastY >> lastZ >> momentumX >> momentumY
        >> momentumZ;
	
    setThreeVector<double>(hitData.hitPosition,hitX,hitY,hitZ);
   setThreeVector<double>(hitData.primaryHitPosition,primX,primY,primZ);
   setThreeVector<double>(hitData.lastHitPosition,lastX,lastY,lastZ);
   
   setThreeVector<double>(hitData.momentum,momentumX,momentumY,momentumZ);
  
    input->push_back(hitData);
  }
}

void T4OutputASCII::saveASCII_Pmt(std::vector<T4PmtData>* input)
{
  file_save << "\t" << input->size() << std::endl;
  for (unsigned int i = 0; i < input->size(); i++) {
    file_save << input->at(i).detectorName << "\t" << input->at(i).detectorId
        << "\t" << input->at(i).channelNo << "\t" << input->at(i).randomTime
        << "\t" << input->at(i).firstEventTime << "\t" << input->at(i).nsPerBin
        << "\t" << input->at(i).windowSize << "\t";
    for (unsigned int j = 0; j < input->at(i).digits.size(); j++) {
      file_save << input->at(i).digits.at(j) << "\t";
    }
    file_save << std::endl;
  }
}

void T4OutputASCII::loadASCII_Pmt(std::vector<T4PmtData>* input)
{
  int nChannel;
  *file_load >> nChannel;
  for (int i = 0; i < nChannel; i++) {
    T4PmtData pmtData;
    *file_load >> pmtData.detectorName >> pmtData.detectorId
        >> pmtData.channelNo >> pmtData.randomTime >> pmtData.firstEventTime
        >> pmtData.nsPerBin >> pmtData.windowSize;
    for (int j = 0; j < pmtData.windowSize / pmtData.nsPerBin; j++) {
      int digit = 0;
      *file_load >> digit;
      pmtData.digits.push_back(digit);
    }
    input->push_back(pmtData);
  }
}

void T4OutputASCII::saveASCII_Rich(std::vector<T4RichData>* input)
{
  file_save << "\t" << input->size() << std::endl;
  for (unsigned int i = 0; i < input->size(); i++) {
    file_save << input->at(i).detectorName << "\t" << input->at(i).detectorId
        << "\t" << input->at(i).photonHitPosition[0] << "\t"
        << input->at(i).photonHitPosition[1] << "\t"
        << input->at(i).photonHitPosition[2] << "\t"
        << input->at(i).photonProductionPosition[0] << "\t"
        << input->at(i).photonProductionPosition[1] << "\t"
        << input->at(i).photonProductionPosition[2] << "\t"
        << input->at(i).momentumMotherParticle[0] << "\t"
        << input->at(i).momentumMotherParticle[1] << "\t"
        << input->at(i).momentumMotherParticle[2] << "\t"
        << input->at(i).parentTrackId << "\t"
        << input->at(i).photonEnergy << "\t" << input->at(i).time << "\t"
        << input->at(i).xPadPosition << "\t" << input->at(i).yPadPosition
        << "\t" << input->at(i).cerenkovAngle;
    file_save << std::endl;
  }
}

void T4OutputASCII::loadASCII_Rich(std::vector<T4RichData>* input)
{
  int nChannel;
  *file_load >> nChannel;
  for (int i = 0; i < nChannel; i++) {
    double hitX, hitY, hitZ, prodX, prodY, prodZ, motherX, motherY, motherZ;
    T4RichData richData;
    *file_load >> richData.detectorName >> richData.detectorId >> hitX
        >> hitY >> hitZ >> prodX >> prodY >> prodZ >> motherX >> motherY
        >> motherZ >> richData.parentTrackId >> richData.photonEnergy >> richData.time
        >> richData.xPadPosition >> richData.yPadPosition
        >> richData.cerenkovAngle;
	setThreeVector<double>(richData.photonHitPosition,hitX,hitY,hitZ);
	setThreeVector<double>(richData.photonProductionPosition,prodX,prodY,prodZ);
	setThreeVector<double>(richData.momentumMotherParticle,motherX,motherY,motherZ);
	
	
    input->push_back(richData);
  }
}

void T4OutputASCII::savePerformance(double loadTime, double totalRunTime,
    long int startSeed)
{
  file_save << "#PROCESSING_TIME" << "\t" << loadTime << "\t" << totalRunTime
      << "\t" << startSeed << std::endl;
}
