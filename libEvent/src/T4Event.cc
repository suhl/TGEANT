#include "T4Event.hh"
#include <iostream>
#include <fstream>
#include <sstream>


#include "T4SMessenger.hh"
#include "T4SGlobals.hh"


void T4effic2Dline::loadFromBlob(string _fileName)
{
  ifstream inFile(_fileName.c_str());
  if (!inFile.good())
    return;
  string line;
  while(getline(inFile,line)){
    blob+=line;
  }
  convertBlob();
  

}


void T4effic2Dline::saveToBlob(string _fileName)
{
  ofstream outFile(_fileName.c_str());
  if (!outFile.good())
    return;
  
  for (int i = 0; i < binsX; i++){
    for (int j = 0; j < binsY; i++){
      outFile << dataArray[i][j]<< " ";
    }
    outFile << "<br />" << endl;
  }
  outFile << flush;
  outFile.close();
}


void T4effic2Dline::convertBlob() {
  //alloc mem
  dataArray = new double*[binsX];
  for (int i = 0; i < binsX; i++)
    dataArray[i] = new double[binsY];

  binWidthX = (endX - startX) / (double) binsX;
  binWidthY = (endY - startY) / (double) binsY;
  
  
  std::vector<std::string> blobExplode = explodeStringCustomDelim(blob,"<br />");
//   cout << "explodeSize" << blobExplode.size() << endl;
  
  if (blobExplode.size() != binsX)
    T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"Malformed efficiency value table in detector %s year %s: BinsX %i, explodeSize %u\n",tbname.c_str(),year.c_str(),binsX,blobExplode.size());
      
  for (int x = 0; x < binsX;x++){
    std::vector<std::string> yList = explodeString(blobExplode.at(x));
    if (yList.size() != binsY)
      T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"Malformed efficiency value table in detector %s year %s: BinsY %i, explodeSize %u\n",tbname.c_str(),year.c_str(),binsY,yList.size());
    
    for (int y = 0; y < binsY;y++){
      dataArray[x][y] = strToDouble(yList.at(y));
    }
  }

}


void T4effic2Dline::loadFromTGA(std::string _fileName)
{
  //TGA grayscale header for uncompressed targa gfx
  uint16_t header[9];
  FILE* outPutImage = fopen(_fileName.c_str(),"rb");
  if (outPutImage == NULL){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Could not open file %s for reading binary!\n",_fileName.c_str());
    return;
  }
  printf("opened the file \n");
  //write the binary header
  fread(header,1,sizeof(header),outPutImage);
  int nBinsX = header[6];
  int nBinsY = header[7];
  
  printf("read header, binsize: %i %i \n",nBinsX,nBinsY);
  float dataVal[nBinsX][nBinsY];
  for (int i = 0; i < nBinsY; i++)
   for (int a = 0; a < nBinsX; a++){     
     uint8_t myVal;
     uint8_t myAlpha;
     fread(&myVal,sizeof(uint8_t),1,outPutImage); 
     fread(&myAlpha,sizeof(uint8_t),1,outPutImage); 
     
     float retVal;
     
     retVal = (float) myVal;
     
     
     retVal /= (pow(2.,8.)-1.);
     if (static_cast<uint16_t>(myAlpha)==0){
       dataVal[a][i] = -1.0;
    }
    else
      dataVal[a][i] = retVal;

   }
   
  
  T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Read image %s -- output commencing!!\n",_fileName.c_str());
  
  string wait;
  cin >> wait;
  
  stringstream theBlob;
    for ( int x = 0; x < nBinsX; x++ ) {
        for ( int y = 0; y < nBinsY; y++ ) {
            theBlob << dataVal[x][y] << " ";
            
        }
        theBlob << "<br />";
    }
    
    cout << theBlob.str() << endl;
  
}



void T4effic2Dline::saveToTGA(std::string _fileName)
{
    //TGA grayscale header for uncompressed targa gfx
    uint16_t header[9]= {0, 3, 0, 0, 0, 0, static_cast<uint16_t>(binsX),static_cast<uint16_t>(binsY) , 16};

    FILE* outPutImage = fopen(_fileName.c_str(),"wb");
    if (outPutImage == NULL) {
        T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Could not open file %s for writing binary!\n",_fileName.c_str());
        return;
    }

    //write the binary header
    fwrite(header,sizeof(char),sizeof(header),outPutImage);

    //finally write the array
    for (int i = 0; i < binsY; i++)
        for (int a = 0; a < binsX; a++) {
            double colour = dataArray[a][i];
            if (colour != -1.0) {
                colour *= pow(2.,8.)-1.;
//        cout << "colour " << colour << endl;
                uint8_t colourInt = (uint8_t) colour;
//        if (colourInt != 0)
//         cout << static_cast<uint16_t>(colourInt)<< endl;

                fwrite(&colourInt,sizeof(uint8_t),1,outPutImage);
                //write this standard number with full alpha channel
                colourInt = 255;
                fwrite(&colourInt,sizeof(uint8_t),1,outPutImage);
            }
            else {
                uint8_t colourInt = 0;
                fwrite(&colourInt,sizeof(uint8_t),1,outPutImage);
                //write this standard number with full alpha channel
                colourInt = 0;
                fwrite(&colourInt,sizeof(uint8_t),1,outPutImage);
            }
        }
    fclose(outPutImage);
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Wrote image %s!\n",_fileName.c_str());
}


double T4effic2Dline::getEffic ( double _x, double _y ) {
//    cout << "wanting effic for det" << tbname << " _x, _y " << _x << " " << _y << endl;
  if (binWidthX == 0 || binWidthY == 0)
    return dataArray[0][0];
 
  if (_x < startX || _x > endX){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__, 
					       " Problem in detector map for det %s! Inconsistent Efficiency-Alignment or detector dimension! Wanting x=%f but detector goes from %f to %f\n",
					       tbname.c_str(),_x,startX,endX);
    return efficMean;
  }
  if (_y < startY || _y > endY){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__, 
					       " Problem in detector map for det %s! Inconsistent Efficiency-Alignment or detector dimension! Wanting y=%f but detector goes from %f to %f\n",
					       tbname.c_str(),_y,startY,endY);
    return efficMean;
  }
  
  int actBinX = (_x-startX) / binWidthX;
  int actBinY = (_y-startY) / binWidthY;

//   cout << "actbins " << actBinX << " " << actBinY << endl;
//   cout << binsX << " " << binsY << " "  << dataArray[actBinX][actBinY] << endl;
  
  double retVal = dataArray[actBinX][actBinY];
  if (retVal == -1.0)
    return efficMean;
  else
    return retVal;
}



void T4Event::mergeEvent ( T4Event* _toMerge, double timingOffset ) {
  int oldBeamParticles = beamData.nBeamParticle;
  //merging the beamparticles is easy
  beamData.nBeamParticle += _toMerge->beamData.nBeamParticle;
  //now we change daughter and parent numbers to fit the new ones
  for (unsigned int i = 0; i < _toMerge->beamData.nBeamParticle; i ++){
    T4BeamParticle myPart = _toMerge->beamData.beamParticles.at(i);
    myPart.k[2] += oldBeamParticles+1;
    myPart.k[3] += oldBeamParticles+1;
    myPart.k[4] += oldBeamParticles+1;
    beamData.beamParticles.push_back(myPart);
  }
  
  //now merge the trajectories together - therefore we need a timing offset and the highest trajectory id
  int highestTrackId = 0;
  for (unsigned int i = 0 ; i< beamData.trajectories.size(); i++)
    if (beamData.trajectories.at(i).trackId > highestTrackId)
      highestTrackId = beamData.trajectories.at(i).trackId;

  beamData.nTrajectories += _toMerge->beamData.nTrajectories;
  for (unsigned int i = 0; i < _toMerge->beamData.trajectories.size(); i++){
    T4Trajectory myTraj = _toMerge->beamData.trajectories.at(i);
    myTraj.parentId += highestTrackId+1;
    myTraj.trackId += highestTrackId+1;
    myTraj.time += timingOffset;
    beamData.trajectories.push_back(myTraj);
  }
  
  //finally merge the hits together, there we need to make sure we change all the trackIds and timings
  mergeHitData(&tracking,&_toMerge->tracking,highestTrackId+1,timingOffset);
  mergeHitData(&trigger,&_toMerge->trigger,highestTrackId+1,timingOffset);
  mergeCaloHitData(&calorimeter,&_toMerge->calorimeter,highestTrackId+1,timingOffset);
  //maybe one should add the rich data here
  //but since this function is purely for adding muon halo to events,
  //we do not do it here now -- maybe it would be interesting to do this because we 
  //could check for the efficiency in reconstruction of 2 interesting vertices in one trigger time window (true pileup)
  
}

void T4Event::mergeHitData ( std::vector<T4HitData>* _into, std::vector<T4HitData>* _toMerge, int trajectoryOffset, double timeOffset ) {
  for (unsigned int i = 0; i < _toMerge->size(); i++){
    T4HitData theHit = _toMerge->at(i);
    theHit.time += timeOffset;
    theHit.trackId += trajectoryOffset;
    _into->push_back(theHit);
  }
}


void T4Event::mergeCaloHitData ( std::vector<T4HitData>* _into, std::vector<T4HitData>* _toMerge, int trajectoryOffset, double timeOffset ) {
  for (unsigned int i = 0; i < _toMerge->size(); i++){
    T4HitData theHit = _toMerge->at(i);
    theHit.time += timeOffset;
    theHit.trackId += trajectoryOffset;
    bool found_already = false;
    for (int i = 0; i < _into->size(); i++)
      if (_into->at(i).channelNo == theHit.channelNo){
	_into->at(i).energyDeposit+=theHit.energyDeposit;
        found_already = true;
	break;
      }
    if (!found_already)
      _into->push_back(theHit);
  }
}

void T4Trajectory::printDebug(){  
  T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Traj: TrackID %i, ParentID %i, ParticleID %i, Position (%.2f,%.2f,%.2f), time %.2f, momentum (%.2f,%.2f,%.2f)\n",trackId,parentId,particleId,position[0],position[1],position[2],time,momentum[0],momentum[1],momentum[2]);
}


void T4BeamData::setDefault(void)
{
  memset(&vertexPosition[0],0,sizeof(double)*3);
  vertexTime = 0;
  generator = 0;
  aux = 0;
  x_bj = 0;
  y = 0;
  w2 = 0;
  q2 = 0;
  nu = 0;

  memset(&uservar[2], 0, sizeof (float) * 18); // not [0] and [1], see T4BeamBackend.cc
  memset(&lst[0], 0, sizeof (int) * 40);
  memset(&parl[0], 0, sizeof (float) * 30);
  memset(&cut[0], 0, sizeof (float) * 14);

  nBeamParticle = 0;
  beamParticles.clear();
  nTrajectories = 0;
  trajectories.clear();

  pysubs.msel = 0;
  pysubs.mselpd = 0;
  memset(&pysubs.msub[0], 0, sizeof (int) * 200);
  memset(&pysubs.kfin[0][0], 0, sizeof (int) * 80*2);
  memset(&pysubs.ckin[0], 0, sizeof (float) * 200);

  memset(&pypars.mstp[0], 0, sizeof (int) * 200);
  memset(&pypars.parp[0], 0, sizeof (float) * 200);
  memset(&pypars.msti[0], 0, sizeof (int) * 200);
  memset(&pypars.pari[0], 0, sizeof (float) * 200);
}

T4Event::T4Event(void)
{
  clear();
  beamData.uservar[0] = 0;
  beamData.uservar[1] = 0;
}

void T4Event::clear(void)
{
  beamData.setDefault();
  tracking.clear();
  trigger.clear();
  calorimeter.clear();
  rich.clear();
  pmt.clear();
  trigMask = 0;
  processingTime = 0;
}

void T4HitData::printData()
{
  T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"HitData of det %s, id %i channel %i\n",detectorName.c_str(),detectorId,channelNo);
  T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"  particleInfo pid %i E %.3f t %.3f beta %.3f Edep %.3f TrackID %i\n",particleId,particleEnergy,time,beta,energyDeposit,trackId);
  T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"  Hitpos (%.2f,%.2f,%.2f) PrimHitpos (%.2f,%.2f,%.2f) LastHitpos (%.2f,%.2f,%.2f) Momentum (%.2f,%.2f,%.2f)\n",hitPosition[0],hitPosition[1],hitPosition[2],primaryHitPosition[0],primaryHitPosition[1],primaryHitPosition[2],lastHitPosition[0],lastHitPosition[1],lastHitPosition[2],momentum[0],momentum[1],momentum[2]);
}


void T4Event::printEventInfo(bool _verbose)
{
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"T4Event::printEventInfo:");
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of tracking hits:    "+uintToStr(tracking.size()));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of trigger hits:     "+uintToStr(trigger.size()));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of calorimeter hits: "+uintToStr(calorimeter.size()));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of rich hits:        "+uintToStr(rich.size()));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of pmt hits:         "+uintToStr(pmt.size()));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Processing time:            "+doubleToStrFP(processingTime)+" s");
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Number of trajectories:     "+uintToStr(beamData.nTrajectories));
  if (_verbose){
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"#### TRACKING:\n");
    for (unsigned int i = 0; i < tracking.size(); i++)
      tracking.at(i).printData();
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"#### TRIGGER:\n");
    for (unsigned int i = 0; i < trigger.size(); i++)
      trigger.at(i).printData();
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"#### calorimeter:\n");
    for (unsigned int i = 0; i < calorimeter.size(); i++)
      calorimeter.at(i).printData();
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"#### TRAJECTORIES:\n");
    for (int i = 0; i < beamData.nTrajectories; i++)
      beamData.trajectories.at(i).printDebug();
    
    
  }
}

void T4Event::printBeamData(void)
{
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"T4Event::printBeamData:");
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Vertex:     (" + doubleToStrFP(beamData.vertexTime) + ", "
          + doubleToStrFP(beamData.vertexPosition[0]) + ", "
          + doubleToStrFP(beamData.vertexPosition[1]) + ", "
          + doubleToStrFP(beamData.vertexPosition[2]) + ")");
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"Generator:  "+intToStr(beamData.generator));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"x_bj:       "+doubleToStrFP(beamData.x_bj));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"y:          "+doubleToStrFP(beamData.y));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"q2:         "+doubleToStrFP(beamData.q2));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"w2:         "+doubleToStrFP(beamData.w2));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"nu:         "+doubleToStrFP(beamData.nu));
  T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"#particles: "+uintToStr(beamData.nBeamParticle));
  
  for (unsigned int i = 0; i < beamData.nBeamParticle; i++) {
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"# "+uintToStr(i)+":");
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"k[K-Code, particleId, origin, daughter1, daughter2]: ");
    stringstream myStream;
    for (unsigned int j = 0; j < 5; j++) myStream << beamData.beamParticles.at(i).k[j] << " ";
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,myStream.str());
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"p[momentum_x, momentum_y, momentum_z, energy, mass]: ");
    
    myStream.clear();
    for (unsigned int j = 0; j < 5; j++) myStream << beamData.beamParticles.at(i).p[j] << " ";
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,myStream.str());
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__,"aux: " + intToStr(beamData.beamParticles.at(i).aux));
  }
}
