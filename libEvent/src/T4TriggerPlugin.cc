#include "T4TriggerPlugin.hh"

T4TriggerPlugin::T4TriggerPlugin(void)
{
  memset(ixMatrix_up, 0, sizeof(double) * 32 * 32);
  memset(ixMatrix_dn, 0, sizeof(double) * 32 * 32);
  memset(lxMatrix, 0, sizeof(double) * 32 * 32);
  memset(mxMatrix_up, 0, sizeof(double) * 32 * 32);
  memset(mxMatrix_dn, 0, sizeof(double) * 32 * 32);
  memset(myMatrix, 0, sizeof(double) * 32 * 32);
  memset(oyMatrix, 0, sizeof(double) * 32 * 32);
  memset(lastMatrix_Y1, 0, sizeof(double) * 32 * 32);
  memset(lastMatrix_Y2, 0, sizeof(double) * 32 * 32);
  
  getTriggerRand = &getOne;

  calo = false;
  purecalo = false;
  rpd = false;

  timePileUp = 0;
  timeOT = 0;
  timeLT = 0;
  timeMT = 0;
  timeIT = 0;
  timeLAST = 0;
  timeVI = 0;
  timeVO = 0;

  t4Event = NULL;
  parserLAST = NULL;
}

void T4TriggerPlugin::doHodoLogic(void)
{
  // reset
  inner.clear();
  middleX.clear();
  middleY.clear();
  ladder.clear();
  outer.clear();
  last.clear();
  vetoInner.clear();
  vetoOuter.clear();

  // first we loop over all hits and search for all channels in the hodoscopes

  map<const T4HitData*, double> h3o, h4o, h4iu, h4id, h5iu, h5id,
      h4l, h5l, h4mxu, h4mxd, h5mxu, h5mxd, h4my, h5my, h1, h2_y1, h2_y2;

  double beamStartPosition = 0;
  if (t4Event->beamData.trajectories.size() > 0)
    beamStartPosition = t4Event->beamData.trajectories.at(0).position[2];
  
  for (unsigned int i = 0; i < t4Event->trigger.size(); i++) {
    string detectorName = t4Event->trigger.at(i).detectorName;
    
    // safety check: channel number between 0-31
    if (t4Event->trigger.at(i).channelNo < 0
        || t4Event->trigger.at(i).channelNo > 31)
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
      __FILE__,
          "T4TriggerPlugin::doHodoLogic: We expect a hodoscope channel number between 0 and 31 but found '"
              + intToStr(t4Event->trigger.at(i).channelNo) + "' for "
              + detectorName + "! Please check this detector!");


    double tRed = getTRed(&t4Event->trigger.at(i), beamStartPosition);

    // Vetos
    if (detectorName[0] == 'V') {
      if (abs(t4Event->trigger.at(i).particleId) != 13)
	continue;
      if (detectorName == "VI01P1__" || detectorName == "VI02X1__")
	vetoInner.push_back(tRed);
      else if (detectorName == "VO01X1__")
	vetoOuter.push_back(tRed);
      else
	T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
        __FILE__,
            "T4TriggerPlugin::doHodoLogic: Detector name '"
                + (string) detectorName
                + "' is not included into the veto logic.");
	
      continue;
    }

    // Hodoscopes
    if (abs(tRed) > timePileUp * CLHEP::ns)
      continue;

    if (detectorName == "HO03Y1_m")
      h3o[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HO04Y1_m" || detectorName == "HO04Y2_m")
      h4o[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HI04X1_u")
      h4iu[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HI04X1_d")
      h4id[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HI05X1_u")
      h5iu[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HI05X1_d")
      h5id[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HL04X1_m")
      h4l[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HL05X1_m")
      h5l[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM04X1_u")
      h4mxu[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM04X1_d")
      h4mxd[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM05X1_u")
      h5mxu[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM05X1_d")
      h5mxd[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM04Y1_d" || detectorName == "HM04Y1_u")
      h4my[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HM05Y1_d" || detectorName == "HM05Y1_u")
      h5my[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HG01Y1__")
      h1[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HG02Y1__")
      h2_y1[&t4Event->trigger.at(i)] = tRed;
    else if (detectorName == "HG02Y2__")
      h2_y2[&t4Event->trigger.at(i)] = tRed;
    else {
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
      __FILE__,
	  "T4TriggerPlugin::doHodoLogic: Detector name '"
	      + (string) detectorName
	      + "' is not included into the trigger logic.");
    }
  }

  map<const T4HitData*, double>::iterator it1, it2;
  vector<double> hid, hiu, hl, ho, hmxd, hmxu, hmy, hlast;

  // matrix = a[i][j]
  // for all hodos: j = channelH5, i = 31 - channelH4 (except H3O)

  // hid
  for (it1 = h4id.begin(); it1 != h4id.end(); it1++)
    for (it2 = h5id.begin(); it2 != h5id.end(); it2++)
      if (getTriggerRand() <= ixMatrix_dn[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeIT)
        hid.push_back((it1->second + it2->second) / 2.);

  // hiu
  for (it1 = h4iu.begin(); it1 != h4iu.end(); it1++)
    for (it2 = h5iu.begin(); it2 != h5iu.end(); it2++)
      if (getTriggerRand() <= ixMatrix_up[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeIT)
        hiu.push_back((it1->second + it2->second) / 2.);

  // hl
  for (it1 = h4l.begin(); it1 != h4l.end(); it1++)
    for (it2 = h5l.begin(); it2 != h5l.end(); it2++)
      if (getTriggerRand() <= lxMatrix[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeLT)
        hl.push_back((it1->second + it2->second) / 2.);

  // hmxd
  for (it1 = h4mxd.begin(); it1 != h4mxd.end(); it1++)
    for (it2 = h5mxd.begin(); it2 != h5mxd.end(); it2++)
      if (getTriggerRand() <= mxMatrix_dn[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeMT)
        hmxd.push_back((it1->second + it2->second) / 2.);

  // hmxu
  for (it1 = h4mxu.begin(); it1 != h4mxu.end(); it1++)
    for (it2 = h5mxu.begin(); it2 != h5mxu.end(); it2++)
      if (getTriggerRand() <= mxMatrix_up[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeMT)
        hmxu.push_back((it1->second + it2->second) / 2.);

  // hmy
  for (it1 = h4my.begin(); it1 != h4my.end(); it1++)
    for (it2 = h5my.begin(); it2 != h5my.end(); it2++)
      if (getTriggerRand() <= myMatrix[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeMT)
        hmy.push_back((it1->second + it2->second) / 2.);

  // ho
  for (it1 = h3o.begin(); it1 != h3o.end(); it1++)
    for (it2 = h4o.begin(); it2 != h4o.end(); it2++) {
      // 2012 mode with 16 channels
      int matrixCh;
      if (it1->first->channelNo < 16)
        matrixCh = 15 - it1->first->channelNo;

      // two new scintillators for 2014+
      else if (it1->first->channelNo == 16)
        matrixCh = 17;
      else if (it1->first->channelNo == 17)
        matrixCh = 16;
      else {
        T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
            __FILE__,
            "T4TriggerPlugin::doHodoLogic: Unknown channel number ("
                + intToStr(it1->first->channelNo) + ") for HO3!");
      }

      if (getTriggerRand() <= oyMatrix[matrixCh][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeOT)
        ho.push_back((it1->second + it2->second) / 2.);
    }




  // hlast
  cout << "ECCOMI 1" << endl;
  for (it1 = h1.begin(); it1 != h1.end(); it1++){
    for (it2 = h2_y1.begin(); it2 != h2_y1.end(); it2++) {
      if (getTriggerRand() <= lastMatrix_Y1[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeLAST)
        hlast.push_back((it1->second + it2->second) / 2.);
    }
    for (it2 = h2_y2.begin(); it2 != h2_y2.end(); it2++) {
      if (getTriggerRand() <= lastMatrix_Y2[31 - it1->first->channelNo][it2->first->channelNo]
          && fabs(it1->second - it2->second) < timeLAST)
        hlast.push_back((it1->second + it2->second) / 2.);
    }
  }
  cout << "ECCOMI 2" << endl;

  // final trigger logic
  inner.insert(inner.end(), hid.begin(), hid.end());
  inner.insert(inner.end(), hiu.begin(), hiu.end());

  ladder.insert(ladder.end(), hl.begin(), hl.end());

  outer.insert(outer.begin(), ho.begin(), ho.end());

  middleX.insert(middleX.begin(), hmxd.begin(), hmxd.end());
  middleX.insert(middleX.begin(), hmxu.begin(), hmxu.end());
  middleY.insert(middleY.begin(), hmy.begin(), hmy.end());

  last.insert(last.begin(),hlast.begin(),hlast.end());
}

bool T4TriggerPlugin::checkTriggerCoincidence(vector<double>& trig1,
    vector<double>& trig2, int& trigMask, double timing, int bit, bool checkVeto)
{
  bool tmp = false;
  for (unsigned int i = 0; i < trig1.size(); i++) {
    for (unsigned int j = 0; j < trig2.size(); j++) {
      if (fabs(trig1.at(i) - trig2.at(j)) < timing) {
	if (checkVeto) {
	  vector<double> v;
	  v.push_back((trig1.at(i) + trig2.at(j)) / 2.);
	  tmp = checkVetoCoincidence(v, trigMask, bit);
	} else {
	  trigMask |= 1 << bit;
	  tmp = true;
	}
	if (tmp)
	  return true;
      }
    }
  }
  return false;
}

bool T4TriggerPlugin::checkVetoCoincidence(vector<double>& trig, int& trigMask, int bit)
{
  for (unsigned int i = 0; i < trig.size(); i++) {
    bool isVeto = false;
    for (unsigned int j = 0; j < vetoInner.size() && !isVeto; j++)
      if (vetoInner.at(j) < trig.at(i) && trig.at(i) < vetoInner.at(j) + timeVI)
	isVeto = true;
    for (unsigned int j = 0; j < vetoOuter.size() && !isVeto; j++)
      if (vetoOuter.at(j) < trig.at(i) && trig.at(i) < vetoOuter.at(j) + timeVO)
	isVeto = true;

    if (!isVeto) {
      trigMask |= 1 << bit;
      return true;
    }
  }
  return false;
}

double T4TriggerPlugin::getTRed(T4HitData* hit, double beamStartPosition)
{
  return sqrt(
      pow(hit->hitPosition[0], 2) + pow(hit->hitPosition[1], 2)
          + pow(hit->hitPosition[2] - beamStartPosition, 2)) * CLHEP::mm
      / (/*hit->beta **/ CLHEP::c_light) - hit->time;
}

void T4TriggerPlugin::doCaloLogic(double threshold_hcal1,
    double threshold_hcal2, double threshold_purecalo, double threshold_ecal1)
{
  calo = false;
  purecalo = false;

  // check for pure calo trigger threshold, it should be bigger than hcal1 and hcal2 threshold
  bool usePureTrigger = true;
  if (threshold_purecalo == -1)
    usePureTrigger = false;
  // set the pure calo trigger threshold to the maximum calo threshold (to avoid problems with the following code)
  if (threshold_purecalo < threshold_hcal1)
    threshold_purecalo = threshold_hcal1;
  if (threshold_purecalo < threshold_hcal2)
    threshold_purecalo = threshold_hcal2;
  if (threshold_purecalo < threshold_ecal1)
    threshold_purecalo = threshold_ecal1;

  std::vector<CaloTriggerInfo> hcal1;
  std::vector<CaloTriggerInfo> hcal2;
  std::vector<CaloTriggerInfo> ecal1;
  double hcal1_integral = 0.;
  double hcal2_integral = 0.;
  double ecal1_integral = 0.;

  std::vector<CaloTriggerInfo>* cal;
  double* integral;
  double* threshold;

  CaloTriggerInfo info;
  for (unsigned int i = 0; i < t4Event->calorimeter.size(); i++) {
    if (t4Event->calorimeter.at(i).detectorName == "HCAL1") {
      cal = &hcal1;
      integral = &hcal1_integral;
      threshold = &threshold_hcal1;
    } else if (t4Event->calorimeter.at(i).detectorName == "HCAL2") {
      cal = &hcal2;
      integral = &hcal2_integral;
      threshold = &threshold_hcal2;
    } else if (t4Event->calorimeter.at(i).detectorName == "ECAL1" && threshold_ecal1 != -1) {
      cal = &ecal1;
      integral = &ecal1_integral;
      threshold = &threshold_ecal1;
    } else 
      continue;

    info.energyDeposit = t4Event->calorimeter.at(i).energyDeposit;
    if (info.energyDeposit > threshold_purecalo) {
      calo = true;
      purecalo = usePureTrigger;
      // we have enough energy in this calo module, we no longer need to look further
      return;
    }
    if (info.energyDeposit > (*threshold))
      calo = true;

    info.x = t4Event->calorimeter.at(i).hitPosition[0];
    info.y = t4Event->calorimeter.at(i).hitPosition[1];
    info.index = i;
    (*cal).push_back(info);
    (*integral) += info.energyDeposit;
  }
  
  // Before we check HCAL1 and HCAL2 for pure calo trigger, 
  // we check ECAL1 for a normal calo trigger if we don't have one already.
  // No pure calo trigger for ECAL1.
  if (!calo) {
    if (ecal1_integral > threshold_ecal1) {
      std::sort(ecal1.begin(), ecal1.end());
      for (unsigned int i = 0; i < ecal1.size(); i++) {
	double clusterEnergy = 0;
	for (unsigned int j = 0; j < ecal1.size(); j++) {
	  // distance should be smaller than 2 * sqrt(2) * 4 cm = ~radius of 4x4 modules
	  if (ecal1.at(j).getDist(ecal1.at(i)) < 11.31 * CLHEP::mm)
	    clusterEnergy += ecal1.at(j).energyDeposit;
	}
	if (clusterEnergy > threshold_ecal1) {
	  calo = true;
	  break;
	}
      }
    }
  }

  bool pure1_possible =
      ((hcal1_integral > threshold_purecalo) && usePureTrigger);
  bool pure2_possible =
      ((hcal2_integral > threshold_purecalo) && usePureTrigger);

  // pure calo trigger is not possible and we have already a calo trigger,
  // we no longer need to look further
  if (!pure1_possible && !pure2_possible && calo)
    return;

  // we start with HCAL1, if the full energy deposit is enough for a trigger
  if (hcal1_integral > threshold_hcal1) {
    std::sort(hcal1.begin(), hcal1.end());
    for (unsigned int i = 0; i < hcal1.size(); i++) {
      double clusterEnergy = 0;
      for (unsigned int j = 0; j < hcal1.size(); j++) {
        // distance should be smaller than 2 * sqrt(2) * 15 cm = ~radius of 4x4 modules
        if (hcal1.at(j).getDist(hcal1.at(i)) < 424.26 * CLHEP::mm)
          clusterEnergy += hcal1.at(j).energyDeposit;
      }
      if (clusterEnergy > threshold_purecalo) {
        calo = true;
        purecalo = usePureTrigger;
        return;
      }
      if (clusterEnergy > threshold_hcal1)
        calo = true;

      if (!pure1_possible && !pure2_possible && calo)
        return;
    }
  }

  if (hcal2_integral > threshold_hcal2) {
    std::sort(hcal2.begin(), hcal2.end());
    for (unsigned int i = 0; i < hcal2.size(); i++) {
      double clusterEnergy = 0;
      for (unsigned int j = i; j < hcal2.size(); j++) {
        // distance should be smaller than 2 * sqrt(2) * 20 cm = ~radius of 4x4 modules
        if (hcal2.at(j).getDist(hcal2.at(i)) < 565.67 * CLHEP::mm)
          clusterEnergy += hcal2.at(j).energyDeposit;
      }
      if (clusterEnergy > threshold_purecalo) {
        calo = true;
        purecalo = usePureTrigger;
        return;
      }
      if (clusterEnergy > threshold_hcal2)
        calo = true;

      if (!pure1_possible && !pure2_possible && calo)
        return;
    }
  }
}

void T4TriggerPlugin::readMatrixFile(std::string fileName, double a[32][32])
{
  ifstream matrixFile(fileName.c_str());

  vector<bool> entriesVector;
  bool val;

  if (matrixFile.is_open()) {
    while (matrixFile.good()) {
      matrixFile >> val;
      entriesVector.push_back(val);
    }
    matrixFile.close();
  } else {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4TriggerPlugin::readMatrixFile: Matrix file '" + fileName
            + "' not found.");
    return;
  }

  if (entriesVector.size() < 32 * 32)
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4TriggerPlugin::readMatrixFile: Matrix file '" + fileName
            + "' may be corrupted, matrix isn't in 32x32 format.");
  else {
    for (unsigned int i = 0; i < 32; i++)
      for (unsigned int j = 0; j < 32; j++)
        a[i][j] = entriesVector.at(i * 32 + j);
  }
}

void T4TriggerPlugin::readLASTFile(string fileName, double a[32][32])
{
  string pathToFile = fileName;

  try {
    XMLPlatformUtils::Initialize();
  } catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    T4SMessenger::getInstance()->setupStream(T4SFatalError, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4TriggerPlugin::readLASTFile: "
        << message;
    T4SMessenger::getInstance()->endStream();
    XMLString::release(&message);
  }

  parserLAST = new XercesDOMParser;
  parserLAST->setValidationScheme(XercesDOMParser::Val_Always);
  parserLAST->setDoNamespaces(false);
  parserLAST->setDoSchema(false);
  parserLAST->setValidationConstraintFatal(false);

  const char* settN = "general.settings";
  const char* matN = "use_matrix";

  XMLCh* settingsNode = XMLString::transcode(settN);
  XMLCh* matrixInUseElement = XMLString::transcode(matN);

  memset(a, 0, sizeof(double) * 32 * 32);

  parserLAST->parse(XMLString::transcode(pathToFile.c_str()));

  //DOMDocument festlegen
  DOMDocument* document = parserLAST->getDocument();

  if (document == NULL) {
    T4SMessenger::getInstance()->setupStream(T4SFatalError, __LINE__, __FILE__);
    T4SMessenger::getReference()
        << "T4TriggerPlugin::readLASTFile: Can not find file '" << pathToFile
        << "'. Last trigger inactive!";
    return;
  }

  //das unterste Element raussuchen, hoffentlich ist es eine map!
  DOMElement* rootNode = document->getDocumentElement();

  std::string matrixInUse;

  DOMElement* settingsParent = dynamic_cast<DOMElement*>(rootNode
      ->getElementsByTagName(settingsNode)->item(0));
  DOMElement* useMatrixChild = dynamic_cast<DOMElement*>(settingsParent
      ->getElementsByTagName(matrixInUseElement)->item(0));
  if (useMatrixChild)
    matrixInUse = XMLString::transcode(useMatrixChild->getTextContent());

  std::string finalMatrixString = "matrix.";
  finalMatrixString.append(matrixInUse);
  XMLCh* finalMatrixName = XMLString::transcode(finalMatrixString.c_str());

  DOMElement* matrixParent = dynamic_cast<DOMElement*>(rootNode
      ->getElementsByTagName(finalMatrixName)->item(0));

  DOMNodeList* nodeList = matrixParent->getChildNodes();
  const XMLSize_t nodeCount = nodeList->getLength();
  std::vector<std::string> finalMatrix;

  int counterOuter = 0;
  for (XMLSize_t i = 0; i < nodeCount; ++i) {
    int counter = 0;
    std::string line = XMLString::transcode(
        nodeList->item(i)->getTextContent());
    if (line.size() < 32)
      continue;
    finalMatrix.push_back(line);
    for (unsigned int inner = 0; inner < line.size(); inner++) {
      if (line.at(inner) == '1') {
        a[counterOuter][counter] = true;
        counter++;
      } else if (line.at(inner) == '0')
        counter++;
    }
    counterOuter++;
  }
}

void T4TriggerPlugin::setEffic(std::list<double> entries, double a[32][32], bool isRow, int chOffset)
{
  std::list<double>::iterator it;
  int channel;
  double effic;
  for (it = entries.begin(); it != entries.end(); it++) {
      channel = (int) (*it);
      it++;
      if (it == entries.end()) {
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
            "T4TriggerPlugin::setEffic: Uneven number or arguments for trigger efficiencies! Please check the list of arguments.");
        break;
      }
      effic = (*it);
      if (effic > 1)
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
            "T4TriggerPlugin::setEffic: Efficiency > 1! Please check the list of arguments.");

      if (isRow)
        channel = chOffset - channel;
      else
        channel += chOffset;

      if (channel < 0 || channel > 31)
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
            "T4TriggerPlugin::setEffic: Channel number out of range 0-31! Please check the list of arguments.");

      setEffic(channel, effic, a, isRow);
  }
}

void T4TriggerPlugin::setEffic(int channel, double effic, double a[32][32], bool isRow)
{
  for (unsigned int i = 0; i < 32; i++) {
    if (isRow)
      a[channel][i] *= effic;
    else
      a[i][channel] *= effic;
  }
}
