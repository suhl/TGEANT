#include "T4TriggerDVCS2016.hh"

T4TriggerDVCS2016::T4TriggerDVCS2016(void)
{
  timePileUp = 10;

  timeOT = 8;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 15;
}

int T4TriggerDVCS2016::getTriggerMask(void)
{
  int trigMask = 0;
  doHodoLogic();
  doCameraLogic();

  // 2016: if middle X trigger active, this line:
  checkTriggerCoincidence(middleX, middleY, trigMask, timeMT, TMIDDLE, true);
  // if not, use this line:
  //checkVetoCoincidence(middleY, trigMask, TMIDDLE);
  
  checkVetoCoincidence(ladder, trigMask, TLADDER);
  checkVetoCoincidence(outer, trigMask, TOUTER);
  checkVetoCoincidence(last, trigMask, TLAST);

  if (rpd)
    trigMask |= 1 << TCAMERA;

  if (trigMask != 0) {
    T4SMessenger::getReference().setupStream(T4SVerboseMore, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4Trigger2012::getTriggerMask: Active triggers are:";
    if (((1 << TCAMERA) & trigMask) > 0) T4SMessenger::getReference() << " TCAMERA";
    if (((1 << TMIDDLE) & trigMask) > 0) T4SMessenger::getReference() << " TMIDDLE";
    if (((1 << TLADDER) & trigMask) > 0) T4SMessenger::getReference() << " TLADDER";
    if (((1 << TOUTER) & trigMask) > 0) T4SMessenger::getReference() << " TOUTER";
    if (((1 << TLAST) & trigMask) > 0) T4SMessenger::getReference() << " TLAST";
    T4SMessenger::getReference().endStream();
  }

  return trigMask;
}
