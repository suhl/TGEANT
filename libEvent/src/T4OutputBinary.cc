#include "T4OutputBinary.hh"

#include "T4SMessenger.hh"

char T4OutputBinary::header_main[4] = { ( char ) 222, ( char ) 173, ( char ) 190, ( char ) 239};
char T4OutputBinary::header_datablock[4] =  { ( char ) 218, ( char ) 187, ( char ) 173, ( char ) 0};
char T4OutputBinary::header_subblock[4] = { ( char ) 202, ( char ) 254, ( char ) 186, ( char ) 190};




void T4OutputBinary::close ( void ) {
    T4OutputBackEnd::close();
    putHeader ( header_subblock );
    for ( int i = 0; i < static_cast<int> ( startVector.size() ); i++ )
        putInt64 ( startVector.at ( i ) );
    putInt64 ( static_cast<int> ( startVector.size() ) );

    file_save.close();
    T4SMessenger::getInstance()->printMessage ( T4SNotice, __LINE__, __FILE__,
            "T4OutputBinary::close: Output saved as " + saveFileName );


}


void T4OutputBinary::save ( void ) {
    T4OutputBackEnd::save();

    startVector.push_back ( file_save.tellp() );

    //first write the generic header block
    putHeader ( header_main );

    //each section gets a subblock for start - so if some stays empty we know how many there were
    putHeader ( header_subblock );

    //then write all the data of the event
    saveBIN_BeamData ( &t4Event->beamData );

    //each section gets a subblock for start - so if some stays empty we know how many there were
    putHeader ( header_subblock );

    // all tracking detectors
//   if ( t4Event->tracking.size() > 0 ) {
    saveBIN_Hit ( &t4Event->tracking );
//   }

    //each section gets a subblock for start - so if some stays empty we know how many there were
    putHeader ( header_subblock );
    // all trigger detectors
//   if ( t4Event->trigger.size() > 0 ) {
    saveBIN_Hit ( &t4Event->trigger );
//   }

    //each section gets a subblock for start - so if some stays empty we know how many there were
    putHeader ( header_subblock );
    // all calorimeter detectors
//   if ( t4Event->calorimeter.size() > 0 ) {
    saveBIN_Hit ( &t4Event->calorimeter );
//   }

    //finally write header block again to finalize event
    putHeader ( header_main );





}



bool T4OutputBinary::checkHeader ( char* _headerToCheck ) {
    char binsRead[4];
    file_load->read ( binsRead, sizeof ( char ) * 4 );
    /*printf("check: %x %x %x %x\n",_headerToCheck[0],_headerToCheck[1],_headerToCheck[2],_headerToCheck[3]);
    printf("is   : %x %x %x %x \n",binsRead[0],binsRead[1],binsRead[2],binsRead[3]);*/
    for ( int i = 0; i < 4; i++ )
        if ( _headerToCheck[i] != binsRead[i] )
            return false;

    return true;
}



void T4OutputBinary::savePerformance ( double loadTime, double totalRunTime, long int startSeed ) {
    cout << "savePerformance is a stub!" << endl;
}

bool T4OutputBinary::streamGetNextEvent ( void ) {

    streamT4Event->clear();
//     cout << "loading event " << indexState << " at position " << indexVector.at(indexState) << endl;

    if ( indexState == indexVector.size() )
        indexState = 0;

    file_load->seekg ( indexVector.at ( indexState ) );
    indexState++;


//   cout << "indexState " << indexState << endl;

    //find a valid event
    while ( !checkHeader ( header_main ) ) {
        cout << "skipped a bad header event!" << endl;
        file_load->seekg ( indexVector.at ( indexState ) );
        indexState++;
        if ( indexState == indexVector.size() )
            indexState = 0;
    }

//     cout << "header seems okay! checking for subblock-header!" << endl;
    if ( !checkHeader ( header_subblock ) ) {
        cout << "subblock header corrupt - checking next event!" << endl;
        return streamGetNextEvent();
    }
//     cout << "subblock header okay - reading beamdata now!" << endl;
    loadBIN_BeamData ( &streamT4Event->beamData );

    if ( !checkHeader ( header_subblock ) ) {
        cout << "subblock header corrupt - checking next event!" << endl;
        return streamGetNextEvent();
    }
//     cout << "subblock header okay - reading tracking hits now!" << endl;

    loadBIN_Hit ( &streamT4Event->tracking );

    if ( !checkHeader ( header_subblock ) ) {
        cout << "subblock header corrupt - checking next event!" << endl;
        return streamGetNextEvent();
    }
//    cout << "subblock header okay - reading trigger hits now!" << endl;
    loadBIN_Hit ( &streamT4Event->trigger );

    if ( !checkHeader ( header_subblock ) ) {
        cout << "subblock header corrupt - checking next event!" << endl;
        return streamGetNextEvent();
    }
//    cout << "subblock header okay - reading calo hits now!" << endl;
    loadBIN_Hit ( &streamT4Event->calorimeter );

    if ( !checkHeader ( header_main ) ) {
        cout << "end footer corrupt - checking next event!" << endl;
        return streamGetNextEvent();
    }
//     cout << "event read successfully!" << endl;
    return true;
}

bool T4OutputBinary::streamLoad ( string fileName ) {
//     T4OutputBackEnd::streamLoad ( fileName );

    if ( streamT4Event == NULL )
        streamT4Event = new T4Event;
    T4SMessenger::getInstance()->printMessage ( T4SNotice, __LINE__, __FILE__,
            "Load " + fileName );
    if ( file_load == NULL )
        file_load = new fstream ( fileName.c_str(), ios::in | ios::binary );
    if ( !file_load->good() ) {
        T4SMessenger::getInstance()->printMessage ( T4SFatalError, __LINE__,
                __FILE__, "T4OutputBinary::streamLoad: Unknown file name." );
        return false;
    }
    //first we rewind the file to start from behind and read the index-table
    file_load->seekg ( 0, ios::end );
    //then we rewind just enough to read an int64
    int64_t fileSize = file_load->tellg();
    file_load->seekg ( fileSize - sizeof ( int64_t ) );
    int64_t entries;
    file_load->read ( ( char* ) &entries, sizeof ( int64_t ) );
    T4SMessenger::getInstance()->printfMessage ( T4SNotice, __LINE__, __FILE__, "Loading %lli entries in the table\n", entries );
    //rewind the file again now to the beginning of the indexing list
    file_load->seekg ( fileSize - sizeof ( int64_t ) * ( entries + 1 ) );
    for ( int64_t tableRead = 0; tableRead < entries; tableRead++ ) {
        int64_t entryAdress;
        file_load->read ( ( char* ) &entryAdress, sizeof ( int64_t ) );
        indexVector.push_back ( entryAdress );
        T4SMessenger::getInstance()->printfMessage ( T4SVerboseMore, __LINE__, __FILE__, "Read index %lli to: %lli \n", tableRead, entryAdress );
    }
    random_shuffle ( indexVector.begin(), indexVector.end() );
    indexState = 0;

    return true;
}

void T4OutputBinary::streamReset ( void ) {
    indexState = 0;
}

T4OutputBinary::T4OutputBinary ( string _saveFileName ) : T4OutputBackEnd() {
    saveFileName = _saveFileName + ".Tbin";

    checkFile();
    file_save.open ( saveFileName.c_str(), ios::out | ios::binary );

    saveFileNameOriginal = saveFileName;

    if ( !file_save.good() ) {
        T4SMessenger::getInstance()->printMessage ( T4SFatalError, __LINE__,
                __FILE__, "T4OutputBinary: Output file has not been created!" );
        return;
    }
    T4SMessenger::getInstance()->printMessage ( T4SNotice, __LINE__, __FILE__,
            "T4OutputBinary: Output file '" + saveFileName + "' has been created." );

}


void T4OutputBinary::loadBIN_BeamData ( T4BeamData* _myBeamData ) {
    //check if we have a datablock here
    //if not - rewind!
    if ( !checkHeader ( header_datablock ) ) {
        cout << "no valid beamdata header found!" << endl;
        int64_t pos = file_load->tellg();
        file_load->seekg ( pos - sizeof ( char ) * 4 );
        return;
    }
    //read number of beamParticles
    int nBeamParticle;
    file_load->read ( ( char* ) &nBeamParticle, sizeof ( int ) );
    _myBeamData->nBeamParticle = nBeamParticle;
//     cout << "reading " << nBeamParticle << " beamParticles " << endl;
    for ( int i = 0; i < nBeamParticle; i++ ) {
        T4BeamParticle myPart;
        for ( int number = 0; number < 5; number++ ) {
            file_load->read ( ( char* ) &myPart.k[number], sizeof ( int ) );
            file_load->read ( ( char* ) &myPart.p[number], sizeof ( float ) );
        }
        file_load->read ( ( char* ) &myPart.aux, sizeof ( int ) );
        _myBeamData->beamParticles.push_back ( myPart );
    }
    int nTraj;
    file_load->read ( ( char* ) &nTraj, sizeof ( int ) );
//     cout << "reading " << nTraj << " trajectories " << endl;
    _myBeamData->nTrajectories = nTraj;

    for ( int i = 0; i < nTraj; i++ ) {
        T4Trajectory myTraj;
        int trackID,parentID,particleID;
        float time,position[3],momentum[3];
        file_load->read ( ( char* ) &trackID, sizeof ( int ) );
        file_load->read ( ( char* ) &parentID, sizeof ( int ) );
        file_load->read ( ( char* ) &particleID, sizeof ( int ) );
        file_load->read ( ( char* ) &time, sizeof ( float ) );
        for ( int number = 0; number < 3; number++ ) {
            file_load->read ( ( char* ) &(position[number]), sizeof ( float ) );
            file_load->read ( ( char* ) &(momentum[number]), sizeof ( float ) );
            myTraj.position[number] = position[number];
            myTraj.momentum[number] = momentum[number];

        }
//     printf("%i %i %i %.2f\n",trackID,parentID,particleID,time);
        myTraj.parentId = parentID;
        myTraj.particleId = particleID;
        myTraj.time = time;
        myTraj.trackId = trackID;
//      myTraj.printDebug();
        _myBeamData->trajectories.push_back ( myTraj );
    }

    if ( !checkHeader ( header_datablock ) ) {
        cout << "!!! no valid beamdata footer block found!!" << endl;
        return;
    }



}



void T4OutputBinary::loadBIN_Hit ( vector< T4HitData >* _myHitData ) {
//check if we have a datablock here
    //if not - rewind!
    if ( !checkHeader ( header_datablock ) ) {
        cout << "no valid beamdata header found!" << endl;
        int64_t pos = file_load->tellg();
        file_load->seekg ( pos - sizeof ( char ) * 4 );
        return;
    }
    int nHits;
    file_load->read ( ( char* ) &nHits, sizeof ( int ) );
//   cout << " found valid hit block! contained hits: "<< nHits << endl;



    for ( int i = 0; i < nHits; i++ ) {
        T4HitData myHit;
        int strSize;
        file_load->read ( ( char* ) &strSize, sizeof ( int ) );
        char buf[strSize+1];
	memset (buf,0,strSize);
        file_load->read ( buf, strSize );
	//add zero termination to string roflcopter
	//8-byte-strings without termination behave very very strange!
	buf[strSize] = '\0';
        myHit.detectorName = buf;
        file_load->read ( ( char* ) &myHit.detectorId, sizeof ( int ) );
        file_load->read ( ( char* ) &myHit.channelNo, sizeof ( int ) );
        file_load->read ( ( char* ) &myHit.trackId, sizeof ( int ) );
        file_load->read ( ( char* ) &myHit.particleId, sizeof ( int ) );
        file_load->read ( ( char* ) &myHit.particleEnergy, sizeof ( double ) );
        file_load->read ( ( char* ) &myHit.time, sizeof ( double ) );
        file_load->read ( ( char* ) &myHit.beta, sizeof ( double ) );
        file_load->read ( ( char* ) &myHit.energyDeposit, sizeof ( double ) );
        for ( int number = 0; number < 3; number++ ) {
            file_load->read ( ( char* ) &myHit.hitPosition[number], sizeof ( double ) );
            file_load->read ( ( char* ) &myHit.primaryHitPosition[number], sizeof ( double ) );
            file_load->read ( ( char* ) &myHit.lastHitPosition[number], sizeof ( double ) );
            file_load->read ( ( char* ) &myHit.momentum[number], sizeof ( double ) );
        }
        _myHitData->push_back ( myHit );
    }
    if ( !checkHeader ( header_datablock ) ) {
        cout << "Problems at end of hit datablock!!!!!!" << endl;
    }


}



void T4OutputBinary::saveBIN_BeamData ( T4BeamData* _myBeamData ) {
    //write datablock header
    putHeader ( header_datablock );
    //write number of beamparticles and the beamparticles
    putInt ( _myBeamData->nBeamParticle );
    for ( unsigned int i = 0; i < _myBeamData->nBeamParticle; i++ ) {
        for ( int number = 0; number < 5; number++ ) {
            putInt ( _myBeamData->beamParticles.at ( i ).k[number] );
            putFloat ( _myBeamData->beamParticles.at ( i ).p[number] );
        }
        putInt ( _myBeamData->beamParticles.at ( i ).aux );
    }

    //write number of trajectories and the trajectories
    putInt ( _myBeamData->nTrajectories );
    for ( unsigned int i = 0; i < _myBeamData->nTrajectories; i++ ) {
        putInt ( _myBeamData->trajectories.at ( i ).trackId );
        putInt ( _myBeamData->trajectories.at ( i ).parentId );
        putInt ( _myBeamData->trajectories.at ( i ).particleId );
        putFloat ( _myBeamData->trajectories.at ( i ).time );
        for ( int number = 0; number < 3; number++ ) {
            putFloat ( _myBeamData->trajectories.at ( i ).position[number] );
            putFloat ( _myBeamData->trajectories.at ( i ).momentum[number] );
        }
    }


    //write end of datablock
    putHeader ( header_datablock );
}

void T4OutputBinary::saveBIN_Hit ( vector< T4HitData >* _myHitdata ) {
    //write datablock header
    putHeader ( header_datablock );
    putInt ( static_cast<int> ( _myHitdata->size() ) );
    for ( unsigned int i = 0; i < _myHitdata->size(); i++ ) {
        T4HitData* currentHit = &_myHitdata->at ( i );
        putInt ( currentHit->detectorName.length() );
        file_save.write ( currentHit->detectorName.c_str(), sizeof ( char ) * currentHit->detectorName.length() );
        putInt ( currentHit->detectorId );
        putInt ( currentHit->channelNo );
        putInt ( currentHit->trackId );
        putInt ( currentHit->particleId );
        putDouble ( currentHit->particleEnergy );
        putDouble ( currentHit->time );
        putDouble ( currentHit->beta );
        putDouble ( currentHit->energyDeposit );
        for ( int number = 0; number < 3; number++ ) {
            putDouble ( currentHit->hitPosition[number] );
            putDouble ( currentHit->primaryHitPosition[number] );
            putDouble ( currentHit->lastHitPosition[number] );
            putDouble ( currentHit->momentum[number] );
        }
    }
    //write end of datablock
    putHeader ( header_datablock );
}


void T4OutputBinary::putFloat ( float _f ) {
    file_save.write ( ( char* ) &_f, sizeof ( float ) );
}

void T4OutputBinary::putInt ( int32_t _i ) {
    file_save.write ( ( char* ) &_i, sizeof ( int32_t ) );
}

void T4OutputBinary::putInt64 ( int64_t _i ) {
    file_save.write ( ( char* ) &_i, sizeof ( int64_t ) );
}

void T4OutputBinary::putDouble ( double _d ) {
    file_save.write ( ( char* ) &_d, sizeof ( double ) );
}


void T4OutputBinary::load ( string fileName, vector< T4Event >* t4Run ) {
    T4OutputBackEnd::load ( fileName, t4Run );
}




