#include "T4CGImport.hh"
#include <T4SMessenger.hh>



void T4CGImport::initOutput ( string _outFile ) {
  if ( myOutput != NULL ) {
    T4SMessenger::getInstance()->printfMessage ( T4SWarning, __LINE__, __FILE__, "Output already initialized!!!!!\n" );
    myOutput->close();
    delete myOutput;
  }
  myOutput = new T4OutputASCII ( _outFile );
}


void T4CGImport::saveAllEvents() {
  if ( myOutput == NULL )
    T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Output not initialized!!!!!\n" );

  for ( unsigned int i = 0; i < eventList.size(); i++ ) {
    myOutput->setEvent ( & ( eventList.at ( i ) ) );
    myOutput->save();
  }
  myOutput->close();
}


void T4CGImport::saveLastEvent() {
  if ( myOutput == NULL )
    T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Output not initialized!!!!!\n" );
  myOutput->setEvent ( &eventList.back() );
  myOutput->save();
}
void T4CGImport::closeFile() {
  if ( myOutput == NULL )
    T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Output not initialized!!!!!\n" );
  myOutput->close();
  delete myOutput;
  myOutput = NULL;

}

void T4CGImport::readEvent() {
  if ( myOutput == NULL )
    T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "STUBBBB!!!!!\n" );
}


void T4CGImport::readHits ( int* iq, int* lq, float* q, T4colink_struct* colink ) {
  T4Event& myEvent = eventList.back();
  int johit = colink->johit;
  if ( johit <= 0 ) return;

  int savehits = 0;

  int ntraject = iq[johit + 1];   // number of trajectories

  for ( int it = 1; it <= ntraject; it++ ) {
    int johtj = lq[johit - it];
    int track = iq[johtj + 1];    // Geant Track number
    int nhits = iq[johtj + 2];    // Number of hits


//     list<CsMCTrack*>::iterator It;  // find out my CsMCTrack...
//     for ( It = _tracks.begin();
//           It != _tracks.end() && ( *It )->getGnum() != track; It++ );

    for ( int ih = 1; ih <= nhits; ih++ ) {
      T4HitData newHit;
      //      savehits ++;
      int johth = lq[johtj - ih];

      int ndat = iq[johth - 1]  ;

      // number of words per hit
      // 19 for 'det' and 'sla' - hodoscopes will appear soon
      // 4 for calorimeters
      // 20 for RICH

      if ( ndat == 19 ) { // trackers (can be hodoscopes also - if 41 < detc < 50 )

        double xmm = q[johth + 1] * 10.; // Centre of Tr. in sens vol (MRS) (mm)
        double ymm = q[johth + 2] * 10.;
        double zmm = q[johth + 3] * 10.;
//         CsGeant3::_geaRef2CsRefVec ( xmm, ymm, zmm );
        double uid = q[johth + 4] * 10.; // Entrance point in sens vol (DRS) (mm)
        double vid = q[johth + 5] * 10.;
        double wid = q[johth + 6] * 10.;
//         CsGeant3::_geaRef2CsRefVec ( uid, vid, wid );
        double uod = q[johth + 7] * 10.; // Exit point from sens vol (DRS) (mm)
        double vod = q[johth + 8] * 10.;
        double wod = q[johth + 9] * 10.;
//         CsGeant3::_geaRef2CsRefVec ( uod, vod, wod );
        double elos = q[johth + 10];     // total energy lost
        double eion = q[johth + 11];     // energy lost by ionisation
        double p   = q[johth + 12];      // momentum (GeV)
        int    tk0 = int ( q[johth + 13] ); // 0=this, N=Other Geant Track
        double time = q[johth + 14];     // DeltaT from time zero (ns?)
        int    detc = int ( q[johth + 15] ); // detector code
        int    detn = int ( q[johth + 16] ); // detector number
        double cxm = q[johth + 17];      // cos of trk at entrance point (MRS)
        double cym = q[johth + 18];
        double czm = q[johth + 19];
//         CsGeant3::_geaRef2CsRefVec ( cxm, cym, czm );


        newHit.primaryHitPosition[0] = wid; newHit.primaryHitPosition[1] = uid; newHit.primaryHitPosition[2] = vid;
        newHit.lastHitPosition[0] = wod; newHit.lastHitPosition[1] = uod; newHit.lastHitPosition[2] = vod;
        newHit.hitPosition[0] = zmm; newHit.hitPosition[1] = xmm; newHit.hitPosition[2] = ymm;
        newHit.energyDeposit = elos; newHit.momentum[0] = p; newHit.detectorId = detn; newHit.time = time;
        myEvent.tracking.push_back ( newHit );

      }
      else if ( ndat == 4 ) {
        T4HitData caloHit;

        // ******************** CALORIMETERS ********************

//  CsCalorimeter::CalorimeterMCData d;
        caloHit.energyDeposit  = q[johth + 1];          // dE (GeV)

        double dummy = int ( q[johth + 2] ); // 0=original track entered cell ,
        // N=Other Geant Track
        //(N=particle type)

        //double  time_c  = q[johth+3];  // t-t0 (ns)
        caloHit.time       = q[johth + 3];     // t-t0 (ns)
        caloHit.detectorId = int ( q[johth + 4] ); //cell ID = matrix ID + module number
        // I can put these two numbers
        // separetly, if needed
        myEvent.calorimeter.push_back ( caloHit );
      }
//       else if(ndat==20){
// //
// // RICH data
// //
//  double cher_xm=q[johth+1]*10.; // X of phot. detect. point MRS [mm]
//  double cher_ym=q[johth+2]*10.; // Y
//  double cher_zm=q[johth+3]*10.; // Z
//  CsGeant3::_geaRef2CsRefVec(cher_xm, cher_ym, cher_zm );
//
//  double cher_yd=q[johth+4]*10.; // Y photon detection point, DRS
//  double cher_zd=q[johth+5]*10.; // Z
//
//  double cher_xp=q[johth+6]*10.; // X point of production MRS [mm]
//  double cher_yp=q[johth+7]*10.; // Y
//  double cher_zp=q[johth+8]*10.; // Z
//  CsGeant3::_geaRef2CsRefVec(cher_xp, cher_yp, cher_zp );
//
//  double cher_mpx=q[johth+9];  // Px of mother particle
//  double cher_mpy=q[johth+10]; // Py
//  double cher_mpz=q[johth+11]; // Pz
//  CsGeant3::_geaRef2CsRefVec(cher_mpx, cher_mpy, cher_mpz );
//
//  double cher_eph=q[johth+12]; // photon energy (eV)
//
//  int cher_im=int(q[johth+13]); // if generated by product = IPART
//
//  double cher_tim=q[johth+14]; // t-t0 [ns], should be around zero for particles
//                               // from the main vertex, non-zero for pile-up
//
//  int cher_ityp=int(q[johth+15]); // detector type * 1000
//  int cher_id  =int(q[johth+16]); // detector ID = 900+cathode number
// // int detn = (cher_id/100)*100;   // detector ID
//  int detn = 900;   // detector ID
//  int cher_cathode  =int(q[johth+16])-detn; // cathode number
//
//  double cher_xr=q[johth+17]*10.; // X point of reflection MRS [cm]
//  double cher_yr=q[johth+18]*10.; // Y
//  double cher_zr=q[johth+19]*10.; // Z
//  CsGeant3::_geaRef2CsRefVec(cher_xr, cher_yr, cher_zr );
//
//  double cher_ang=q[johth+20]; // cher angle, [rad]
//
//  savehits ++;
//  // set particle number using PDG convention
//  if( cher_im != 0 ) cher_im = CsMCParticle( cher_im ).getNumber();
//
//  CsRICH1Detector* rich = CsGeom::Instance()->getRich1Detector();
//  if( rich == NULL ) {
//    CsErrLog::mes( elError,
//     "Hit associated to a non esisting detector, please check detectors.dat content" );
//    continue;
//  }
//
//  CsMCHit* hit = new CsMCRICH1Hit(
//          cher_xm, cher_ym, cher_zm,
//          cher_yd, cher_zd,
//          cher_xp, cher_yp, cher_zp,
//          cher_xr, cher_yr, cher_zr,
//          cher_eph, cher_tim, cher_ang,
//          Hep3Vector( cher_mpx, cher_mpy,
//                cher_mpz ),
//          *(*It),
//          cher_im, cher_cathode, *rich );
//  _hits.push_back( hit );
//  rich->addMCHit( *hit );
//       }
    }
  }

  // sort hits
//   _hits.sort( _sortMCHits() );
//
//   list<CsMCHit*>::iterator Ih;
//   for( Ih=_hits.begin(); Ih!=_hits.end(); Ih++ ) {
//     CsMCTrack* mytrack = (*Ih)->getMCTrack();
//     mytrack->addMCHit( *(*Ih) );   // add the hit to its track
//   }
//
//   return savehits;
}


void T4CGImport::readKine ( int* iq, int* lq, float* q, T4gclink_struct* gclink ) {
  vertices.clear();
  tracks.clear();

  T4Event& myEvent = eventList.back();

  int jkine   = gclink->jkine;
  int jvertx  = gclink->jvertx;
  if ( jkine  <= 0 ) return;
  if ( jvertx <= 0 ) return;

  double x, y, z, t;
  int nvertx = iq[jvertx + 1]; // number of MC vertices
  int iv, jv;
  //CsMCVertex** vtxPtr = new CsMCVertex*[nvertx];
  map< int, T4VertexType*, less<int> > mv;

  // Loop over vertices...
  for ( iv = 1; iv <= nvertx; iv++ ) {
    jv = lq[jvertx - iv]; if ( jv <= 0 ) continue;
    //rotation done here included
    x  = q[jv + 3] * 10.;                 // vertex coordinates (mm)
    y  = q[jv + 1] * 10.;
    z  = q[jv + 2] * 10.;
    t  = q[jv + 4];                       // Time of flight (hope in us)
    // add a vertex (with no inTrack) to the list;
    // inTrack will be set later

    /*CsMCVertex* vertex = new CsMCVertex ( iv, x, y, z, t );
    _vertices.push_back ( vertex );
    */ //vtxPtr[iv-1] = vertex;
    T4VertexType newVertex;
    newVertex.position[0] = x; newVertex.position[1] = y; newVertex.position[2] = z;
    newVertex.time = t; newVertex.ID = iv + 1;
    vertices.push_back ( newVertex );
    mv[iv - 1] = &vertices.back();
  }

  double px, py, pz; int ip;
  int ntrack = iq[jkine + 1]; // number of MC tracks
  int it, jk;
  //CsMCTrack** trkPtr = new CsMCTrack*[ntrack];
  map< int, T4TrackType*, less<int> > mt;
  // Loop over tracks...
  for ( it = 1; it <= ntrack; it++ ) {
    jk = lq[jkine - it]; if ( jk <= 0 ) continue;
    ip = int ( q[jk + 5] ); // particle nr in JPART
    px = q[jk + 1];      // Particle momentum (GeV)
    py = q[jk + 2];      //
    pz = q[jk + 3];      //

    int ovx = int ( q[jk + 6] ); // origin vertex

    //CsMCTrack* track = new CsMCTrack( it, px, py, pz, CsMCParticle( ip ),
    //              *vtxPtr[ovx-1] );
//     CsMCTrack* track = new CsMCTrack ( it, px, py, pz, CsMCParticle ( ip ),
//                                        *mv[ovx - 1] );
//     _tracks.push_back ( track );
    //trkPtr[it-1] = track;
    T4TrackType newTrack;
    newTrack.momentum[0] = pz; newTrack.momentum[1] = px; newTrack.momentum[2] = py;
    newTrack.particleId = ip;
    newTrack.vtx = ovx;
    newTrack.ID = it;
    tracks.push_back ( newTrack );

    mt[it - 1] = &tracks.back();

    //vtxPtr[ovx-1]->addOutTrack( *track );
    mv[ovx - 1]->outgoingTrackId[mv[ovx - 1]->outgoingTrackNum] = newTrack.ID; // add out track to vertex
    mv[ovx - 1]->outgoingTrackNum++;


    T4Trajectory newTraj;
    newTraj.trackId = newTrack.ID;
    newTraj.position[0] = mv[ovx - 1]->position[0];
    newTraj.position[1] = mv[ovx - 1]->position[1];
    newTraj.position[2] = mv[ovx - 1]->position[2];
    newTraj.momentum[0] = newTrack.momentum[0];
    newTraj.momentum[1] = newTrack.momentum[1];
    newTraj.momentum[2] = newTrack.momentum[2];
    newTraj.time = newTrack.time;
    newTraj.particleId = newTrack.particleId;
    myEvent.beamData.trajectories.push_back ( newTraj );



  }

  // Loop again on tracks to set:
  //   - vertices inTrack
  //   - tracks outTrack list
  for ( it = 1; it <= ntrack; it++ ) {
    jk = lq[jkine - it]; if ( jk <= 0 ) continue;
    int nev = int ( q[jk + 7] ); // n end vertices
    if ( nev > 0 ) {
      for ( int i = 1; i <= nev; i++ ) {
        iv = int ( q[jk + 7 + i] );
        for ( int trackLoop = 0; trackLoop < mv[iv - 1]->outgoingTrackNum; trackLoop++ )
          mt[mv[iv - 1]->outgoingTrackId[trackLoop]]->parentId = it - 1;
        //vtxPtr[iv-1]->setInTrack( *trkPtr[it-1] );
//          mv[iv - 1]->setInTrack ( *mt[it - 1] ); // Set inTracks in vertices
        jv = lq[jvertx - iv]; if ( jv <= 0 ) continue;
        int nok = int ( q[jv + 7] ); // n out tracks
        //trkPtr[it-1]->addOutVertex( *vtxPtr[iv-1] );
//          mt[it - 1]->addOutVertex ( *mv[iv - 1] ); //Add outVertex in track
//          for ( int j = 1; j <= nok; j++ ) {
//            int ot = int ( q[jv + 7 + j] );
        //trkPtr[it-1]->addOutTrack( *trkPtr[ot-1] );
//            mt[it - 1]->addOutTrack ( *mt[ot - 1] );
//          }
      }
    }
  }

  //delete [] vtxPtr;
  //delete [] trkPtr;
//   return ntrack;
  myEvent.beamData.nTrajectories = myEvent.beamData.trajectories.size();
}


void T4CGImport::readLund ( int* iq, int* lq, float* q, T4gclink_struct* gclink ) {
  int jhead = gclink->jhead;
  if ( jhead <= 0 ) return;

  int jhaux = lq[jhead - 1];
  if ( jhaux <= 0 ) return;

  int jtlnd = lq[jhaux - 1];
  if ( jtlnd <= 0 ) return;
  int tlndsize = iq[jtlnd - 1];

  T4Event myEvent;
  int np = iq[jtlnd - 2];

  if ( tlndsize == 27 ) { // Old COMGEANT TLND banks size


    myEvent.beamData.x_bj  = q[jtlnd + 1];
    myEvent.beamData.y  = q[jtlnd + 2];
    myEvent.beamData.w2 = q[jtlnd + 3];
    myEvent.beamData.q2 = q[jtlnd + 4];
    myEvent.beamData.nu  = q[jtlnd + 5];
    for ( int i = 6; i <= 17; i++ ) myEvent.beamData.lst[i + 14] = int ( q[jtlnd + i] );
    for ( int i = 18; i <= 27; i++ ) myEvent.beamData.parl[i + 2] = q[jtlnd + i];

    int jplnd;
    T4BeamParticle part;
    for ( int i = 1; i <= np; i++ ) {
      jplnd = lq[jtlnd - i];
      for ( int j = 1; j <= 5; j++ ) {
        part.k[j - 1] = int ( q[jplnd + j] );
      }

      part.p[0] = q[jplnd + 7]; //
      part.p[1] = q[jplnd + 8]; // NB: Rotation from COMG ref. to Coral ref.
      part.p[2] = q[jplnd + 6]; //
      part.p[3] = q[jplnd + 9];
      part.p[4] = q[jplnd + 10];

      // at the moment there's no V vector on data
      //for( int j=11; j<=15; j++ ) {
      //  part.v[j-11] = q[jplnd+j];
      //}
//       for ( int i = 0; i < 5; i++ ) part. v[i] = 0.0;
      //part.lu2kine = int(q[jplnd+16]);
      part.aux = int ( q[jplnd + 11] );

      myEvent.beamData.beamParticles.push_back ( part );

    }

    int jrlnd = lq[jtlnd];
    if ( jrlnd != 0 ) {
      int rlndsize = iq[jrlnd - 1];

      if ( rlndsize == 56 || rlndsize == 79 ) { // Check on RLND bank size
        myEvent.beamData.generator = int ( q[jrlnd + 1] );
        for ( int i = 2; i <= 15; i++ ) myEvent.beamData.cut[i - 2]   = q[jrlnd + i];
        for ( int i = 16; i <= 35; i++ ) myEvent.beamData.lst[i - 16]  = int ( q[jrlnd + i] );
        for ( int i = 36; i <= 37; i++ ) myEvent.beamData.lst[i - 3]   = int ( q[jrlnd + i] );
        for ( int i = 38; i <= 46; i++ ) myEvent.beamData.parl[i - 38] = q[jrlnd + i];
        for ( int i = 47; i <= 56; i++ ) myEvent.beamData.parl[i - 37] = q[jrlnd + i];
//         if ( rlndsize == 79 ) {
//           for ( int i = 57; i <= 62; i++ ) myEvent.beamData.parhfl[i - 57] = q[jrlnd + i];
//           for ( int i = 63; i <= 70; i++ ) myEvent.beamData.cuthfl[i - 63] = q[jrlnd + i];
//           for ( int i = 71; i <= 74; i++ ) myEvent.beamData.lsthfl[i - 71] = int ( q[jrlnd + i] );
//           for ( int i = 75; i <= 79; i++ ) myEvent.beamData.lsthfl[i - 70] = int ( q[jrlnd + i] );
//         }
      }
      else {
        T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Wrong RLND bank size: %d, expected: 56 or 79",
            rlndsize );
      }
    }
  }

  else if ( tlndsize == 48 ) { // New COMGEANT TLND banks size (Lepto/Aroma)


    myEvent.beamData.x_bj  = q[jtlnd + 2];
    myEvent.beamData.y  = q[jtlnd + 3];
    myEvent.beamData.w2 = q[jtlnd + 4];
    myEvent.beamData.q2 = q[jtlnd + 5];
    myEvent.beamData.nu  = q[jtlnd + 6];
    for ( int i = 7; i <= 26; i++ )  myEvent.beamData.uservar[i - 7] = q[jtlnd + i];
    for ( int i = 27; i <= 38; i++ ) myEvent.beamData.lst[i - 7]     = int ( q[jtlnd + i] );
    for ( int i = 39; i <= 48; i++ ) myEvent.beamData.parl[i - 19]   = q[jtlnd + i];

    int jplnd;
    T4BeamParticle part;
    for ( int i = 1; i <= np; i++ ) {
      jplnd = lq[jtlnd - i];
      for ( int j = 1; j <= 5; j++ ) {
        part.k[j - 1] = int ( q[jplnd + j] );
      }
      part.p[0] = q[jplnd + 7]; //
      part.p[1] = q[jplnd + 8]; // NB: Rotation from COMG ref. to Coral ref.
      part.p[2] = q[jplnd + 6]; //
      part.p[3] = q[jplnd + 9];
      part.p[4] = q[jplnd + 10];
      part.aux = int ( q[jplnd + 11] );
      myEvent.beamData.beamParticles.push_back ( part );
    }

    int jrlnd = lq[jtlnd];
    if ( jrlnd != 0 ) {
      int rlndsize = iq[jrlnd - 1];

      if ( rlndsize == 63 || rlndsize == 91 ) { // Check on RLND bank size
        myEvent.beamData.generator = int ( q[jrlnd + 1] );
        for ( int i = 2; i <= 15; i++ )  myEvent.beamData.cut[i - 2]   = q[jrlnd + i];
        for ( int i = 16; i <= 35; i++ ) myEvent.beamData.lst[i - 16]  = int ( q[jrlnd + i] );
        for ( int i = 36; i <= 43; i++ ) myEvent.beamData.lst[i - 4]   = int ( q[jrlnd + i] );
        for ( int i = 44; i <= 63; i++ ) myEvent.beamData.parl[i - 44] = q[jrlnd + i];
//  if( rlndsize == 91 ) {
//    for( int i=64; i<=73; i++ ) myEvent.beamData.parhfl[i-64] = q[jrlnd+i];
//    for( int i=74; i<=81; i++ ) myEvent.beamData.cuthfl[i-74] = q[jrlnd+i];
//    for( int i=82; i<=91; i++ ) myEvent.beamData.lsthfl[i-82] = int(q[jrlnd+i]);
      }
    }
    else {
      int rlndsize = iq[jrlnd - 1];
      T4SMessenger::getInstance()->printfMessage ( T4SFatalError, __LINE__, __FILE__, "Wrong RLND bank size: %d, expected: 63 or 91",
          rlndsize );

    }
  }


  else if ( tlndsize == 826 ) { // New COMGEANT TLND banks size (Pythia)


    myEvent.beamData.x_bj  = q[jtlnd + 2];
    myEvent.beamData.y  = q[jtlnd + 3];
    myEvent.beamData.w2 = q[jtlnd + 4];
    myEvent.beamData.q2 = q[jtlnd + 5];
    myEvent.beamData.nu  = q[jtlnd + 6];
    for ( int i =  7; i <= 26; i++ ) myEvent.beamData.uservar[i - 7] = q[jtlnd + i];
    for ( int i = 27; i <= 226; i++ ) myEvent.beamData.pypars.mstp[i - 27]      = int ( q[jtlnd + i] );
    for ( int i = 227; i <= 426; i++ ) myEvent.beamData.pypars.parp[i - 227]     = q[jtlnd + i];
    for ( int i = 427; i <= 626; i++ ) myEvent.beamData.pypars.msti[i - 427]     = int ( q[jtlnd + i] );
    for ( int i = 627; i <= 826; i++ ) myEvent.beamData.pypars.pari[i - 627]     = q[jtlnd + i];

    int jplnd;
    T4BeamParticle part;
    for ( int i = 1; i <= np; i++ ) {
      jplnd = lq[jtlnd - i];
      for ( int j = 1; j <= 5; j++ ) {
        part.k[j - 1] = int ( q[jplnd + j] );
      }
      part.p[0] = q[jplnd + 7]; //
      part.p[1] = q[jplnd + 8]; // NB: Rotation from COMG ref. to Coral ref.
      part.p[2] = q[jplnd + 6]; //
      part.p[3] = q[jplnd + 9];
      part.p[4] = q[jplnd + 10];
      part.aux = int ( q[jplnd + 11] );
      myEvent.beamData.beamParticles.push_back ( part );
    }

    int jrlnd = lq[jtlnd];
    if ( jrlnd != 0 ) {
      int rlndsize = iq[jrlnd - 1];

    }
  }
  eventList.push_back ( myEvent );
  return;
}




