#include "T4TriggerSIDIS2011.hh"

T4TriggerSIDIS2011::T4TriggerSIDIS2011(void)
{
  timePileUp = 10;

  timeIT = 8;
  timeOT = 8;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 15;
  timeVI = 0;
  timeVO = 0;
}

int T4TriggerSIDIS2011::getTriggerMask(void)
{
  int trigMask = 0;
  doHodoLogic();
  
  // "The CM and the CT were both used with the low threshold." (Trigger note 2016)
  // In 2007 and 2011, ECAL1 was included in the calo mix trigger.
  doCaloLogic(5.4 * CLHEP::GeV, 5.4 * CLHEP::GeV, 5.4 * CLHEP::GeV, 5.4 * CLHEP::GeV);
 
  if (inner.size() > 0 && calo)
    trigMask |= 1 << TINNER;
  
  // TINCLMT = middle trigger + veto
  // TMIDDLE = middle trigger + veto + calo
  // btw, don't change the order in the following if condition!!
  if (checkTriggerCoincidence(middleX, middleY, trigMask, timeMT, TINCLMT, true) && calo)
    trigMask |= 1 << TMIDDLE;  
  
  checkVetoCoincidence(ladder, trigMask, TLADDER);
  checkVetoCoincidence(outer, trigMask, TOUTER);
  if (calo) 
    checkVetoCoincidence(last, trigMask, TLAST);

  if (purecalo)
    trigMask |= 1 << TCALO;

  if (trigMask != 0) {
    T4SMessenger::getReference().setupStream(T4SVerboseMore, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4TriggerSIDIS2011::getTriggerMask: Active triggers are:";
    if (((1 << TINNER) & trigMask) > 0) T4SMessenger::getReference() << " TINNER";
    if (((1 << TMIDDLE) & trigMask) > 0) T4SMessenger::getReference() << " TMIDDLE";
    if (((1 << TLADDER) & trigMask) > 0) T4SMessenger::getReference() << " TLADDER";
    if (((1 << TOUTER) & trigMask) > 0) T4SMessenger::getReference() << " TOUTER";
    if (((1 << TCALO) & trigMask) > 0) T4SMessenger::getReference() << " TCALO";
    if (((1 << TINCLMT) & trigMask) > 0) T4SMessenger::getReference() << " TINCLMT";
    if (((1 << TLAST) & trigMask) > 0) T4SMessenger::getReference() << " TLAST";
    T4SMessenger::getReference().endStream();
  }

  return trigMask;
}

bool T4TriggerSIDIS2011::hasTriggered(int _trigMask)
{
  return _trigMask > 0;
}
