#include "T4TriggerHadron2008.hh"

T4TriggerHadron2008::T4TriggerHadron2008(void)
{
  timePileUp = 10;

  timeIT = 8;
  timeOT = 8;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 15;
}

int T4TriggerHadron2008::getTriggerMask(void)
{
  int trigMask = 0;

  // TODO: Include the trigger logic. Examples: all other years.

  return trigMask;
}

bool T4TriggerHadron2008::hasTriggered(int _trigMask)
{
  return _trigMask > 0;
}
