#include "T4TriggerPrimakoff2012.hh"

T4TriggerPrimakoff2012::T4TriggerPrimakoff2012(double threshPrim1_ = 60,
                        double threshPrim2_ = 30, double threshPrim2Veto_ = 18,
                        double threshPrim3_ = 40)
{
  threshPrim1 = threshPrim1_;
  threshPrim2 = threshPrim2_;
  threshPrim2Veto = threshPrim2Veto_;
  threshPrim3 = threshPrim3_;

  timePileUp = 10;
  timeBT = 4; // time for FI01X and FI01Y coincidence
  timeABT = 4; // time BT and BK03 coincidence
  timeVeto = 12.5;

  aBT = false;
}

int T4TriggerPrimakoff2012::getTriggerMask(void)
{
  int trigMask = 0;
  doBKLogic();
  doRPDLogic();
  doECALLogic();

  if (bt.size() > 0)
    trigMask |= 1 << TBT;
  if (abt.size() > 0)
    trigMask |= 1 << TABT;
  if (abt.size() > 0 && veto.size() == 0 && (PrimTrig&1))
    trigMask |= 1 << TPRIM1;
  if (abt.size() > 0 && veto.size() == 0 && (PrimTrig&2))
    trigMask |= 1 << TPRIM2;
  if (abt.size() > 0 && veto.size() == 0 && (PrimTrig&4))
    trigMask |= 1 << TPRIM3;
  if (abt.size() > 0 && veto.size() == 0 && bk.size() == 0 )
    trigMask |= 1 << TMINBIAS;
  if (abt.size() > 0 && veto.size() == 0 && bk.size() == 0 && rpd)
    trigMask |= 1 << TDT0;
  if (abt.size() > 0 && veto.size() == 0)
    trigMask |= 1 << TMINBIASWOBK;

  if (trigMask != 0) {
    T4SMessenger::getReference().setupStream(T4SVerboseMore, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4Trigger2014DY::getTriggerMask: Active triggers are:";
    if (((1 << TDT0) & trigMask) > 0) T4SMessenger::getReference() << " TDT0: aBT && !(Veto) && !(BK) && RPD";
    if (((1 << TPRIM1) & trigMask) > 0) T4SMessenger::getReference() << " TPRIM1: aBT && !(Veto) && ECAL2SumTh1";
    if (((1 << TPRIM2) & trigMask) > 0) T4SMessenger::getReference() << " TPRIM2: aBT && !(Veto) && ECAL2Cluster";
    if (((1 << TPRIM3) & trigMask) > 0) T4SMessenger::getReference() << " TPRIM3: aBT && !(Veto) && ECAL2SumTh2";
    if (((1 << TMINBIAS) & trigMask) > 0) T4SMessenger::getReference() << " TMINBIAS: aBT && !(Veto) && !(BK)";
    if (((1 << TBT) & trigMask) > 0) T4SMessenger::getReference() << " TBT: Beam trigger";
    if (((1 << TABT) & trigMask) > 0) T4SMessenger::getReference() << " TABT: altern. Beam trigger";
    if (((1 << TMINBIASWOBK) & trigMask) > 0) T4SMessenger::getReference() << " TMINBIASWOBK: aBT && !(Veto)";

    T4SMessenger::getReference().endStream();
  }

  return trigMask;
}

bool T4TriggerPrimakoff2012::hasTriggered(int _trigMask)
{
  return _trigMask > 0;
}

void T4TriggerPrimakoff2012::doBKLogic(void)
{
  double beamStartPosition = 0;
  if (t4Event->beamData.trajectories.size() > 0)
    beamStartPosition = t4Event->beamData.trajectories.at(0).position[2];

  // FI01
  vector<double> fi01x, fi01y;
  for (unsigned int i = 0; i < t4Event->tracking.size(); i++) {
    string detectorName = t4Event->tracking.at(i).detectorName;
    if (detectorName == "FI01X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timePileUp * CLHEP::ns)
        fi01x.push_back(tRed);
    } else if (detectorName == "FI01Y1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timePileUp * CLHEP::ns)
        fi01y.push_back(tRed);
    }
  }

  // loop on trigger and vetos
  vector<double> hk01, hk02, hk03, sandwichVeto, vi01, vo01, vi02;
  for (unsigned int i = 0; i < t4Event->trigger.size(); i++) {
    string detectorName = t4Event->trigger.at(i).detectorName;
    if (detectorName == "HK01X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        hk01.push_back(tRed);
    } else if (detectorName == "HK02X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        hk02.push_back(tRed);
    } else if (detectorName == "HK03X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeABT * CLHEP::ns)
        hk03.push_back(tRed);
    } else if (detectorName.substr(0, 2) == "HH") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        sandwichVeto.push_back(tRed);
    } else if (detectorName == "VI01P1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        vi01.push_back(tRed);
    } else if (detectorName == "VO01X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        vo01.push_back(tRed);
    } else if (detectorName == "VI02X1__") {
      double tRed = getTRed(&t4Event->tracking.at(i), beamStartPosition);
      if (abs(tRed) <= timeVeto * CLHEP::ns)
        vi02.push_back(tRed);
    }
  }

  // doing the combinations now...
  vector<double>::iterator it1, it2;

  // combination for: beam trigger
  for (it1 = fi01x.begin(); it1 != fi01x.end(); it1++)
    for (it2 = fi01y.begin(); it2 != fi01y.end(); it2++)
      if (fabs(*it1 - *it2) < timeBT)
        bt.push_back((*it1 + *it2) / 2.);


  // combination for: altern. Beam trigger
  for (it1 = fi01x.begin(); it1 != fi01x.end(); it1++)
    for (it2 = hk03.begin(); it2 != hk03.end(); it2++)
      if (fabs(*it1 - *it2) < timeABT)
        abt.push_back((*it1 + *it2) / 2.);

  for(it1 = hk01.begin(); it1 != hk01.end(); it1++)
    for(it2 = hk02.begin(); it2 != hk02.end(); it2++)
      if (fabs(*it1 - *it2) < timeVeto)
        bk.push_back((*it1 + *it2) / 2.);

  // combination for: veto
  veto.insert(veto.begin(), sandwichVeto.begin(), sandwichVeto.end());
  veto.insert(veto.begin(), vi01.begin(), vi01.end());
  veto.insert(veto.begin(), vo01.begin(), vo01.end());
  veto.insert(veto.begin(), vi02.begin(), vi02.end());
}

void T4TriggerPrimakoff2012::doRPDLogic(void)
{
  rpd = false;

  vector<int> rpdHitsA;
  vector<int> rpdHitsB;

  for (unsigned int i = 0; i < t4Event->tracking.size(); i++) {
    if (t4Event->tracking.at(i).detectorId == 626) // ringA
      rpdHitsA.push_back(t4Event->tracking.at(i).channelNo);
    else if (t4Event->tracking.at(i).detectorId == 627) // ringA
      rpdHitsB.push_back(t4Event->tracking.at(i).channelNo);
  }

  int chA, chB;
  // now do the camera-trigger
  for (unsigned int i = 0; i < rpdHitsA.size(); i++) {
    chA = rpdHitsA.at(i);
    chB = 2*chA-1;
    if (chB == -1) chB = 23;
    if (std::find(rpdHitsB.begin(), rpdHitsB.end(), 2*chA) != rpdHitsB.end()
        || std::find(rpdHitsB.begin(), rpdHitsB.end(), 2*chA+1) != rpdHitsB.end()
        || std::find(rpdHitsB.begin(), rpdHitsB.end(), chB) != rpdHitsB.end()) {
      // We can actually return here, as the trigger fired already.
      // We cannot fire twice.
      rpd = true;
      return;
    }
  }
}

void T4TriggerPrimakoff2012::doECALLogic(void) {
  PrimTrig = 0;
  ECAL2Trigger ecalTrigger(t4Event, this);
  double sum = ecalTrigger.doSum();
  bool clusterTrig = ecalTrigger.doMaxCluster(threshPrim2, threshPrim2Veto);
  if(sum > threshPrim1)
    PrimTrig |= 1;
  if(clusterTrig)
    PrimTrig |= 2;
  if(sum > threshPrim3)
    PrimTrig |= 4;
}

T4TriggerPrimakoff2012::ECAL2Trigger::ECAL2Trigger(const T4Event* t4Event, T4TriggerPrimakoff2012* parent) {
  double beamStartPosition = 0;
  if (t4Event->beamData.trajectories.size() > 0)
    beamStartPosition = t4Event->beamData.trajectories.at(0).position[2];
  std::vector<T4HitData> vCalo = t4Event->calorimeter;

  for (unsigned int i = 0; i < vCalo.size(); i++) {
    string detectorName = vCalo.at(i).detectorName;
    if (vCalo.at(i).hitPosition[2] > 30000 && vCalo.at(i).hitPosition[2] < 34000) { //selects ECAL2
      hitsX.push_back((vCalo.at(i).lastHitPosition[0] - 19.15) / 38.3 + 32);
      hitsY.push_back((vCalo.at(i).lastHitPosition[1] - 19.15) / 38.3 + 24);
      hitsT.push_back(parent->getTRed(&vCalo.at(i), beamStartPosition));
      hitsE.push_back(vCalo.at(i).energyDeposit);
    }
  }
}

double T4TriggerPrimakoff2012::ECAL2Trigger::doSum(void) {
  sum=0;
  for(int i = 0; i < hitsX.size(); ++i) {
    int x = hitsX[i];
    int y = hitsY[i];
    if(x>=25 &&x <=36 && y>=18 && y<=29 &&  !(x>=34 && y>=22 && y<=25)) {
      if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
        sum += hitsE[i];
    }
  }
}
 
bool T4TriggerPrimakoff2012::ECAL2Trigger::doMaxCluster(const double thresholdCluster, const double thresholdVeto) {
  double sumLeftBand = 0;
  double sumRightBand = 0;
  double max = 0;
  int imax = -1;
  double vetoSum = 0;
  for(int i = 0; i < hitsX.size(); ++i) {
    int x = hitsX[i];
    int y = hitsY[i];
    if(x >= 24 && x <= 39 && y >= 12 && y <= 35 && !( x>= 34 &&  y >= 18 && y <= 29)) {
      if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
        if(hitsE[i] > max) {
          max = hitsE[i];
          imax = i;
        }
    }
    if(x >= 20 && x <= 23 && y >= 14 && y <= 33) {
      if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
        sumLeftBand += hitsE[i];
    }
    if(x >= 40 && x <= 43) {
      if(((y >= 12 && y <= 17) || (y >= 30 && y <= 35)) || x >= 42 && (y == 18 || y == 19 ||y == 28 || y == 29) ){
        if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
          sumRightBand += hitsE[i];
      }
    }
    if(x <= 33 || (x >= 34 && x <= 47 && (y >= 30 || y <= 17)) || (x >= 48 && (y <= 19 || y >= 28))){
      if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
        vetoSum += hitsE[i];
    }
  }

  double clusterSum = 0;
  for(int i = 0; i < hitsX.size(); ++i) {
    int x = hitsX[i];
    int y = hitsY[i];
    if(hitsT[i] < 12.5 && hitsE[i] < 1 * CLHEP::GeV)
      if(x >= hitsX[imax] - 2 && x <= hitsX[imax] + 2 && y >= hitsY[imax] - 2 && y <= hitsY[imax] + 2)
        if( x < 24 || x > 39)
          continue;
        clusterSum += hitsE[i];
  }

  if(imax == -1 || (clusterSum < thresholdCluster && hitsX[imax] != 24 && hitsX[imax] != 39)) {
   if(sumLeftBand > sumRightBand)
     clusterSum = sumLeftBand;
   else
     clusterSum = sumRightBand;
  }
  else if(hitsX[imax] == 24)
    clusterSum += sumLeftBand;
  else if(hitsX[imax] == 39)
    clusterSum += sumRightBand;


  if(sumLeftBand > clusterSum)
    clusterSum = sumLeftBand;
  if(sumRightBand > clusterSum)
    clusterSum = sumRightBand;

  if(clusterSum > thresholdCluster && (vetoSum - clusterSum) < thresholdVeto)
    return true;
  return false;
}
