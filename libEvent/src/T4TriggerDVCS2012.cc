#include "T4TriggerDVCS2012.hh"

T4TriggerDVCS2012::T4TriggerDVCS2012(void)
{
  timePileUp = 10;

  timeOT = 8;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 15;
  timeVI = 0;
  timeVO = 0;
}

int T4TriggerDVCS2012::getTriggerMask(void)
{
  int trigMask = 0;
  doHodoLogic();
  doCameraLogic();

  // for 2012 inner and calo was not active, look here: Prescaler setup for run 107999 (elog)

  checkTriggerCoincidence(middleX, middleY, trigMask, timeMT, TMIDDLE, true);
  checkVetoCoincidence(ladder, trigMask, TLADDER);
  checkVetoCoincidence(outer, trigMask, TOUTER);
  checkVetoCoincidence(last, trigMask, TLAST);

//   if (ladder.size() > 0)
//     trigMask |= 1 << TLADDER;
//   if (outer.size() > 0)
//     trigMask |= 1 << TOUTER;
//   if (last.size() > 0)
//     trigMask |= 1 << TLAST;
  if (rpd)
    trigMask |= 1 << TCAMERA;

  if (trigMask != 0) {
    T4SMessenger::getReference().setupStream(T4SVerboseMore, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4Trigger2012::getTriggerMask: Active triggers are:";
    if (((1 << TCAMERA) & trigMask) > 0) T4SMessenger::getReference() << " TCAMERA";
    if (((1 << TMIDDLE) & trigMask) > 0) T4SMessenger::getReference() << " TMIDDLE";
    if (((1 << TLADDER) & trigMask) > 0) T4SMessenger::getReference() << " TLADDER";
    if (((1 << TOUTER) & trigMask) > 0) T4SMessenger::getReference() << " TOUTER";
    if (((1 << TLAST) & trigMask) > 0) T4SMessenger::getReference() << " TLAST";
    T4SMessenger::getReference().endStream();
  }

  return trigMask;
}

bool T4TriggerDVCS2012::hasTriggered(int _trigMask)
{
  // special for 2012: return false if camera trigger only
  return _trigMask > 1;
}

void T4TriggerDVCS2012::doCameraLogic(void)
{
  rpd = false;

  vector<int> cameraHitsA;
  vector<int> cameraHitsB;

  for (unsigned int i = 0; i < t4Event->tracking.size(); i++) {
    if (t4Event->tracking.at(i).detectorId == 626) // ringA
      cameraHitsA.push_back(t4Event->tracking.at(i).channelNo);
    else if (t4Event->tracking.at(i).detectorId == 627) // ringA
      cameraHitsB.push_back(t4Event->tracking.at(i).channelNo);
  }

  int ID, IDP;
  // now do the camera-trigger
  for (unsigned int i = 0; i < cameraHitsA.size(); i++) {
    ID = cameraHitsA.at(i);
    IDP = ID + 1;
    if (IDP == 24)
      IDP = 0;
    if (std::find(cameraHitsB.begin(), cameraHitsB.end(), ID)
        != cameraHitsB.end()
        || std::find(cameraHitsB.begin(), cameraHitsB.end(), IDP)
            != cameraHitsB.end()) {
      // We can actually return here, as the trigger fired already.
      // We cannot fire twice.
      rpd = true;
      return;
    }
  }
}
