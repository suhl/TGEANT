#include "T4TriggerDY2015.hh"

T4TriggerDY2015::T4TriggerDY2015(void)
{
  timePileUp = 4;

  timeOT = 6;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 10;

  timeDiMuon = 5;
}
