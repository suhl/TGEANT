#include "T4PileUpAddition.hh"
#include "T4SMessenger.hh"

T4PileUpAddition* T4PileUpAddition::single = NULL;

T4PileUpAddition* T4PileUpAddition::getInstance ( void )
{
    if (single == NULL) {
        single = new T4PileUpAddition();
    }
    return single;
}

int T4PileUpAddition::initialize ( string _fileName, double _rate, double _tgate, T4Event* writeTo, long int _seed, bool _useCache , string _cacheFolder )
{
  
    time_t now;
    localtime(&now);
    myRandom = new CLHEP::HepJamesRandom(now);
    
    cachingActive = _useCache;
    srand(_seed);
    //get file names from fileList
    ifstream myIn;
    myIn.open(_fileName.c_str(),ifstream::in);
    vector<string> myFileList;
    if(!myIn.good())
        T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"Filelist %s not found!\n",_fileName.c_str());
    while (!myIn.eof()) {
        string line;
        getline(myIn,line);
        if (line.size() > 0) {
            myFileList.push_back(line);
        }
    }
    std::random_shuffle(myFileList.begin(),myFileList.end());
    chosenFile = myFileList.front();

    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Chose file %s from your FileList!\n",chosenFile.c_str());


    if (_useCache) {
        int source = open(chosenFile.c_str(), O_RDONLY, 0);
        string fileTo = _cacheFolder +"/"+intToStr(_seed)+".copy.Tbin";
        int dest = open(fileTo.c_str(), O_WRONLY | O_CREAT , 0777);
        struct stat stat_source;
        fstat(source, &stat_source);
	T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Caching file to: %s\n",fileTo.c_str());
        sendfile(dest, source, 0, stat_source.st_size);
        close(source);
        close(dest);
        chosenFile = fileTo;
    }


    myBinaryFile = new T4OutputBinary();
    myBinaryFile->streamLoad(chosenFile);
    myEventPointerBin = myBinaryFile->streamGetEventPointer();
    myEventPointerOut = writeTo;
    myRandom->setSeed(_seed);
    rate = _rate;
    tgate = _tgate;
}

void T4PileUpAddition::cleanUpCache()
{
    if(cachingActive){
      unlink(chosenFile.c_str());
      T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Deleted file %s!\n",chosenFile.c_str());
 }
}


T4PileUpAddition::T4PileUpAddition ( void )
{
    cachingActive = false;
    myBinaryFile = NULL;
}

T4PileUpAddition::~T4PileUpAddition ( void )
{
    
    delete myBinaryFile;
    delete myRandom;
}

bool T4PileUpAddition::addPileUp()
{
    //check if active via nullpointer
    if(myBinaryFile == NULL)
        return false;

    //generate number of pileups
    //rate is in events / ns, time is -tgate < t < tgate so total number in mean is
    //rate * 2 * tgate)-1 because the main beam is already 1 muon.
    //rate is just muon flux here
    int nPileUp = CLHEP::RandPoisson::shoot(myRandom,tgate*2*rate-1);

    for (int i = 0; i < nPileUp; i ++) {
        //throw the timing offset
        double time = CLHEP::RandFlat::shoot(myRandom,-tgate,tgate);
        //get next event myEventPointerBin
        myBinaryFile->streamGetNextEvent();

        myEventPointerOut->mergeEvent(myEventPointerBin,time);
    }
    return true;

}
