#include "T4TriggerDY2014.hh"

T4TriggerDY2014::T4TriggerDY2014(void)
{
  timePileUp = 4;

  timeOT = 6;
  timeLT = 6;
  timeMT = 4;
  timeLAST = 10;

  timeDiMuon = 5;
}

int T4TriggerDY2014::getTriggerMask(void)
{
  int trigMask = 0;
  doHodoLogic();

  // in 2014 only MT_Y (with horizontal strips) was activated

  checkTriggerCoincidence(middleY, last, trigMask, timeDiMuon, TMIDDLELAST);

  if (middleY.size() > 0)
    trigMask |= 1 << TMIDDLE;

  checkTriggerCoincidence(outer, last, trigMask, timeDiMuon, TOUTERLAST);

  if (outer.size() > 0)
    trigMask |= 1 << TOUTER;

  checkDoubleTrigger(last, trigMask, TLASTDOUBLE);

  if (last.size() > 0)
    trigMask |= 1 << TLAST;

//  checkDoubleTrigger(outer, trigMask, TOUTERDOUBLE);
//  checkDoubleTrigger(middleY, trigMask, TMIDDLEDOUBLE);
//  checkTriggerCoincidence(middleY, outer, trigMask, timeDiMuon, TMIDDLEOUTER);

  if (trigMask != 0) {
    T4SMessenger::getReference().setupStream(T4SVerboseMore, __LINE__, __FILE__);
    T4SMessenger::getReference() << "T4Trigger2014DY::getTriggerMask: Active triggers are:";
    if (((1 << TMIDDLELAST) & trigMask) > 0) T4SMessenger::getReference() << " TMIDDLELAST";
    if (((1 << TMIDDLE) & trigMask) > 0) T4SMessenger::getReference() << " TMIDDLE";
    if (((1 << TOUTERLAST) & trigMask) > 0) T4SMessenger::getReference() << " TOUTERLAST";
    if (((1 << TOUTER) & trigMask) > 0) T4SMessenger::getReference() << " TOUTER";
    if (((1 << TLASTDOUBLE) & trigMask) > 0) T4SMessenger::getReference() << " TLASTDOUBLE";
    if (((1 << TLAST) & trigMask) > 0) T4SMessenger::getReference() << " TLAST";
    T4SMessenger::getReference().endStream();
  }

  return trigMask;
}

bool T4TriggerDY2014::hasTriggered(int _trigMask)
{
  return _trigMask > 0;
}

void T4TriggerDY2014::checkDoubleTrigger(vector<double>& trig, int& trigMask, int bit)
{
  bool tmp = false;
  for (unsigned int i = 0; i < trig.size(); i++) {
    for (unsigned int j = i + 1; j < trig.size(); j++) {
      if (fabs(trig.at(i) - trig.at(j)) < timeDiMuon) {
        trigMask |= 1 << bit;
        tmp = true;
        break;
      }
    }
    if (tmp)
      break;
  }
}
