#include "T4EfficDatabase.hh"
#ifdef USE_SQLITE

map<string,T4effic2Dline*> T4EfficDatabase::cache;
vector<T4effic2Dline*> T4EfficDatabase::cacheVec;


int T4EfficDatabase::callback ( void *data, int argc, char **argv, char **azColName ) {
    int i;
    T4effic2Dline* newLine = new T4effic2Dline;
    newLine->tbname = argv[1];
    newLine->detname = argv[2];
    newLine->unit = argv[3];
    newLine->year = argv[4];
    newLine->binsX = strToInt ( argv[5] );
    newLine->binsY = strToInt ( argv[6] );
    newLine->startX = strToDouble ( argv[7] );
    newLine->startY = strToDouble ( argv[8] );
    newLine->endX = strToDouble ( argv[9] );
    newLine->endY = strToDouble ( argv[10] );
    newLine->efficMean = strToDouble ( argv[11] );
    newLine->blob = argv[12];
    newLine->convertBlob();

    string indexName = newLine->tbname+newLine->detname+newLine->unit;
    //check if this detector already exists in the map, if yes check which has more bins and use the one that does!
//   cout << "nameIndex for det: " << indexName<< endl;
    std::map<std::string,T4effic2Dline*>::iterator effIt;
    effIt = T4EfficDatabase::cache.find(indexName);
    if (effIt != T4EfficDatabase::cache.end()) {
        int myBins = newLine->binsX * newLine->binsY;
        int theirBins = effIt->second->binsX * effIt->second->binsY;
        if (theirBins < myBins) {
            cache[indexName] = newLine;
            printf("T4EfficDatabase: Already had detector %s from year %s with %i bins. Using year %s with %i bins instead!\n",effIt->second->tbname.c_str(),effIt->second->year.c_str(),theirBins,newLine->year.c_str(),myBins);
        }
    }
    else
        cache[indexName] = newLine;


    if ( data != NULL )
        cacheVec.push_back ( newLine );


//     printf("detector %s added to cache!\n",argv[1]);
    return 0;
}

T4EfficDatabase::~T4EfficDatabase()
{
    if(good) sqlite3_close ( db );
    for (unsigned int i = 0; i < cacheVec.size(); i++)
        delete cacheVec.at(i);
//   map<string,T4effic2Dline*>::iterator myIter = cache.begin();
//   while (myIter != cache.end()){
//     ++myIter;
//     delete myIter->second;
//   }
}


T4EfficDatabase::T4EfficDatabase ( string _dbFile ) {

    zErrMsg = 0;
    rc = sqlite3_open ( _dbFile.c_str(), &db );
    if ( rc )
        good = false;
    else
        good = true;
}

bool T4EfficDatabase::cacheDetector ( string _tbname, string _detname, string _unit,string _year, bool vecCache, bool anyYear ) {

//   cout << "want cache for: " << _tbname << " " << _detname << " " << _unit << " " << _year << " anyyear " << anyYear<< endl;
    string sql = ( string ) "SELECT * FROM efficiencies WHERE TBNAME=\"" + _tbname + "\" AND DETNAME=\"" + _detname + "\" AND UNIT=" + _unit;
    if (!anyYear)
        sql += + " AND YEAR=\""+_year+"\";";
    else
        sql += ";";
    const char* data = "UseVector";
    if ( !vecCache )
        data = NULL;
    rc = sqlite3_exec ( db, sql.c_str(), callback, ( void* ) data, &zErrMsg );
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
        return false;
    }
    return true;
}


bool T4EfficDatabase::cacheAllYear(string _year)
{
    string sql = ( string ) "SELECT * FROM efficiencies WHERE YEAR=\""+_year+"\";";
    const char* data = "UseVector";

    rc = sqlite3_exec ( db, sql.c_str(), callback, ( void* ) data, &zErrMsg );
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
        return false;
    }
    return true;

}



#endif

