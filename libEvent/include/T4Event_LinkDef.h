#include "config.hh"

#ifdef USE_ROOT

#include <vector>
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class T4BeamParticle;
#pragma link C++ class std::vector<T4BeamParticle>+;

#pragma link C++ class T4Trajectory;
#pragma link C++ class std::vector<T4Trajectory>+;

#pragma link C++ class T4PySubs;
#pragma link C++ class T4PyPars;
#pragma link C++ class T4BeamData;

#pragma link C++ class T4HitData;
#pragma link C++ class std::vector<T4HitData>+;

#pragma link C++ class T4PmtData;
#pragma link C++ class std::vector<T4PmtData>+;

#pragma link C++ class T4RichData;
#pragma link C++ class std::vector<T4RichData>+;

#pragma link C++ class T4Event;

#endif

#endif

