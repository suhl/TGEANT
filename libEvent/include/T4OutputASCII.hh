#ifndef T4OUTPUTASCII_HH_
#define T4OUTPUTASCII_HH_

#include <time.h>
#include <iomanip> 
#include <sys/file.h>
#include "gzstream.hh"
#include "T4OutputBackEnd.hh"


/*! \class  T4OutputASCII
 *  \brief An implementation of a zipped ASCII output format for TGEANT
 * 
 *  This class implements a simple but very stable zipped output format. 
 *  In principle its only ASCII but it is gzipped on the fly.
 *  The created files can just be unzipped and are then human-readable.
 *  Also notable is, that this format is actually streamable!
 * 
 */

class T4OutputASCII : public T4OutputBackEnd
{
  public:
    /*! \brief the void constructor - use with care! */
    T4OutputASCII(void) {eventNo = 0; file_load = NULL; useSplitting = false;}
    /*! \brief the default constructor for saving events */
    T4OutputASCII(std::string); // standard constructor for saving events
    /*! \brief the destructor */
    virtual ~T4OutputASCII(void) {if (file_load != NULL) delete file_load;}

    /*! \brief the non-streaming load-function. Loads every event from a file into the vector of T4Event.
     *  \param fileName the name of the file to read
     *  \param t4Run 
     *  This functions loads the contents of the whole file from the first param into the vector of T4Event, the second param.
     *  Be careful with this function! While it allows to split network-load and cpu-load it also uses the RAM heavily!
     */
    void load(std::string fileName, std::vector<T4Event>* t4Run);
    /*! \brief saves the event from pointer to the file */
    void save(void);
    /*! \brief this saves performance information into the file */
    void savePerformance(double loadTime, double totalRunTime,
        long int startSeed);
    /*! \brief closes the file */
    void close(void);
    
    /*! \brief opens a file for reading in streaming-mode
     *  \param fileName the file to read from
     *  \return true if file successful
     *  This function just opens the file and keeps a handle on it.
     *  It will continuously load from this file whenever an event is requested!
     *  This is the commonly used and wanted way to use this class, as RAM usage is minimized. 
     *  On the other hand one has to live with a continous file access.
     */
    bool streamLoad(std::string fileName);
    
     /*! \brief closes stream-mode */
    void streamClose(void) {file_load->close(); delete file_load; file_load = NULL;}
    
    /*! \brief get the next event from the file into the pointer streamT4Event 
     *  \return true if the next event was ready, false if not or EOF.
     */
    bool streamGetNextEvent(void);
    
    /*! \brief resets the stream to begin all-over */
    virtual void streamReset(void);
    
    /*! \brief return number of the events in the file in total 
     * 
     *  This function returns the total number of events to stream.
     *  For discreet root-files for example this is possible.
     *  This implemenation cannot do this, as it is just not known how long the file really will be.
     *  Therefore we just return -1 and the user has to check the return value of streamGetNextEvent()!
     */
    int streamGetEventNumber(void) {return -1;}
    
    /*! \brief gets the pointer to the raw file device for output */
    inline ogzstream* getRawOutput(void){return &file_save;};
    
    /*! \brief gets the pointer to the raw file device for input */
    inline igzstream* getRawInput(void){return file_load;};
    
    
  private:
    ogzstream file_save;
    igzstream* file_load;

    std::string saveFileNameOriginal;
   
    void loadASCII_EventData(T4Event*);
    void loadASCII_BeamData(T4BeamData*);
    void loadASCII_Hit(std::vector<T4HitData>*);
    void loadASCII_Pmt(std::vector<T4PmtData>*);
    void loadASCII_Rich(std::vector<T4RichData>*);

    void saveASCII_EventData(T4Event*);
    void saveASCII_BeamData(T4BeamData*);
    void saveASCII_Hit(std::vector<T4HitData>*);
    void saveASCII_Pmt(std::vector<T4PmtData>*);
    void saveASCII_Rich(std::vector<T4RichData>*);

    unsigned int eventNo;
};

#endif /* T4OUTPUTASCII_HH_ */
