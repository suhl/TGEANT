#ifndef T4TRIGGER2014DY_HH
#define T4TRIGGER2014DY_HH

#include "T4TriggerPlugin.hh"
/*!
 * \brief Implementation for the 2014 DY trigger.
 */

// trigger defines - what triggerbit is for what trigger?
#define TMIDDLELAST 0
#define TMIDDLE 1
#define TOUTERLAST 2
#define TOUTER 3
// #define TCALO 4
// #define TVETO 5
// #define THALO 6
// #define TBEAM 7
#define TLASTDOUBLE 8
#define TLAST 9

// ???????
//#define TOUTERDOUBLE 4
//#define TMIDDLEDOUBLE 5
//#define TMIDDLEOUTER 6


class T4TriggerDY2014 : public T4TriggerPlugin
{
  public:
    T4TriggerDY2014(void);
    virtual ~T4TriggerDY2014(void) {}

    /*! \brief returns the triggermask*/
    virtual int getTriggerMask(void);

    /*! \brief checks if the event has triggered at all*/
    virtual bool hasTriggered(int _trigMask);

    /*! \brief returns a human-readable name of the plugin*/
    virtual string getPluginName(void) {return "2014-DY";}

  protected:
    double timeDiMuon;
    void checkDoubleTrigger(vector<double>&, int& trigMask, int bit);
};

#endif
