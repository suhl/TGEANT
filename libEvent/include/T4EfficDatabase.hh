#ifndef T4EFFICDATABASE_HH
#define T4EFFICDATABASE_HH
#include "config.hh"

#ifdef USE_SQLITE
#include <map>
#include <vector>
#include <string>

#include "T4SGlobals.hh"
#include "T4Event.hh"

#include "sqlite3.h"


class T4EfficDatabase{
public:
  T4EfficDatabase(string _dbFile);
  ~T4EfficDatabase();
  inline bool isGood(){return good;};
  bool cacheDetector(string _tbname, string _detname, string _unit,string _year,bool vecCache = false,bool anyYear=false);
  bool cacheAllYear(string _year);
  static vector<T4effic2Dline*> cacheVec;
  static map<string,T4effic2Dline*> cache;
  static int callback ( void *data, int argc, char **argv, char **azColName );
private:
  char *zErrMsg;
  int  rc;
  bool good;
  sqlite3 *db;
};




#endif

#endif


