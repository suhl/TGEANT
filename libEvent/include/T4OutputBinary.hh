#ifndef T4OUTPUTBINARY_HH_
#define T4OUTPUTBINARY_HH_

#include <time.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <algorithm> 


using namespace std;


#include "T4OutputBackEnd.hh"
/*! \class  T4OutputBinary
 *  \brief An implementation of a binary output/input file -- USE ONLY FOR muon-halo!!!
 *
 *  This class implements a simple binary file format for use only in the muon halo.
 *  It implements the streamload function  as a random-access function to add random halo muons to an otherwise final event.
 *
 */

class T4OutputBinary : public T4OutputBackEnd {
public:
  /*! \brief the void constructor - use with care! */
  T4OutputBinary ( void ) {
    eventNo = 0;
    file_load = NULL;
    useSplitting = false;
  }
  /*! \brief the default constructor for saving events */
  T4OutputBinary ( std::string ); // standard constructor for saving events
  /*! \brief the destructor */
  virtual ~T4OutputBinary ( void ) {
    if ( file_load != NULL ) delete file_load;
  }

  /*! \brief the non-streaming load-function. Loads every event from a file into the vector of T4Event.
   *  \param fileName the name of the file to read
   *  \param t4Run
   *  This functions loads the contents of the whole file from the first param into the vector of T4Event, the second param.
   *  Be careful with this function! While it allows to split network-load and cpu-load it also uses the RAM heavily!
   */
  void load ( std::string fileName, std::vector<T4Event>* t4Run );
  /*! \brief saves the event from pointer to the file */
  void save ( void );
  /*! \brief this saves performance information into the file */
  void savePerformance ( double loadTime, double totalRunTime,
                         long int startSeed );
  /*! \brief closes the file */
  void close ( void );

  /*! \brief opens a file for reading in streaming-mode
   *  \param fileName the file to read from
   *  \return true if file successful
   *  This function just opens the file and keeps a handle on it.
   *  It will continuously load from this file whenever an event is requested!
   *  This is the commonly used and wanted way to use this class, as RAM usage is minimized.
   *  On the other hand one has to live with a continous file access.
   */
  bool streamLoad ( std::string fileName );

  /*! \brief closes stream-mode */
  void streamClose ( void ) {
    file_load->close();
    delete file_load;
    file_load = NULL;
  }

  /*! \brief get the next event from the file into the pointer streamT4Event
   *  \return true if the next event was ready, false if not or EOF.
   *  Special care is needed here! This is a random-access-iterator-stream!!!! It will give you a random event from the binary sample!
   */
  bool streamGetNextEvent ( void );

  /*! \brief resets the stream to begin all-over */
  virtual void streamReset ( void );

  /*! \brief return number of the events in the file in total
   *
   *  This function returns the total number of events to stream.
   *  For discreet root-files for example this is possible.
   */
  int streamGetEventNumber ( void ) {
    return -1;
  }



private:
  fstream file_save;
  fstream* file_load;

  std::string saveFileNameOriginal;

  void loadBIN_BeamData ( T4BeamData* );
  void loadBIN_Hit ( std::vector<T4HitData>* );

  void saveBIN_BeamData ( T4BeamData* );
  void saveBIN_Hit ( std::vector<T4HitData>* );
  
  inline void putHeader(char* _array){for (int i = 0; i < 4; i++)file_save.write(&_array[i],sizeof(char));};


  static char header_main[4] ;
  static char header_datablock[4] ;
  static char header_subblock[4]  ;

  void putFloat ( float _f );
  void putInt ( int32_t _i );
  void putInt64 ( int64_t _i );
  void putDouble ( double _f );
  unsigned int eventNo;
  
  bool checkHeader(char* _headerToCheck);
  
  vector<int64_t> startVector;
  vector<int64_t> indexVector;
  int indexState;
};




#endif
