#ifndef T4TRIGGERPRIMAKOFF2012_HH
#define T4TRIGGERPRIMAKOFF2012_HH

#include "T4TriggerPlugin.hh"
/*!
 * \brief Implementation for the 2012 Primakoff trigger.
 *
 * 01  DT0 aBT && !(Veto) && RPD
 * 08  BT  Beam trigger
 * 09  aBT altern. Beam trigger
 *
 * http://wwwcompass.cern.ch/twiki/bin/view/Trigger/TriggerMask
 */

// trigger defines - what triggerbit is for what trigger?
#define TDT0 0
#define TPRIM1 1
#define TPRIM2 2
#define TPRIM3 3
#define TMINBIAS 4
#define TBT 7
#define TABT 8
#define TMINBIASWOBK 9

class T4TriggerPrimakoff2012 : public T4TriggerPlugin
{
  public:
    T4TriggerPrimakoff2012(double, double, double, double);
    virtual ~T4TriggerPrimakoff2012(void) {}

    /*! \brief returns the triggermask*/
    int getTriggerMask(void);

    /*! \brief checks if the event has triggered at all*/
    bool hasTriggered(int _trigMask);

    /*! \brief returns a human-readable name of the plugin*/
    string getPluginName(void) {return "2012-Primakoff";}

  private:
    bool aBT;
    double timeBT, timeABT, timeVeto;
    vector<double> bt, abt, veto, bk;
    int PrimTrig;
    double threshPrim1, threshPrim2, threshPrim2Veto, threshPrim3;
    void doBKLogic(void);

    void doRPDLogic(void);

    void doECALLogic(void);

    class ECAL2Trigger {
      public:
        ECAL2Trigger(const T4Event* t4Event, T4TriggerPrimakoff2012* parent);
        ~ECAL2Trigger() {}
        double doSum(void);
        bool doMaxCluster(const double thresholdCluster, const double thresholdVeto);
      private:
        double sum;
        std::vector<int> hitsX;
        std::vector<int> hitsY;
        std::vector<int> hitsT;
        std::vector<int> hitsE;
    };
    friend class ECAL2Trigger;
};


#endif
