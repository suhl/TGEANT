#ifndef T4TRIGGER2012_HH
#define T4TRIGGER2012_HH

#include "T4TriggerPlugin.hh"
/*!
 * \brief Implementation for the 2012 MuonTrigger (DVCS)
 */

// trigger defines - what triggerbit is for what trigger?
#define TCAMERA 0
#define TMIDDLE 1
#define TLADDER 2
#define TOUTER 3
#define TLAST 9

class T4TriggerDVCS2012 : public T4TriggerPlugin
{
  public:
    T4TriggerDVCS2012(void);
    virtual ~T4TriggerDVCS2012(void) {}

    /*! \brief returns the triggermask*/
    virtual int getTriggerMask(void);

    /*! \brief checks if the event has triggered at all*/
    virtual bool hasTriggered(int _trigMask);

    /*! \brief returns a human-readable name of the plugin*/
    virtual string getPluginName(void) {return "2012-DVCS";}

  protected:
    /*! \brief Checks for a hit in the camera detector. */
    void doCameraLogic(void);
};


#endif
