#ifndef T4TRIGGER2015_HH
#define T4TRIGGER2015_HH

#include "T4TriggerDY2014.hh"
/*!
 * \brief Implementation for the 2015 trigger (DY)
 */

// trigger defines - what triggerbit is for what trigger?
#define TMIDDLELAST 0
#define TMIDDLE 1
#define TOUTERLAST 2
#define TOUTER 3
#define TLASTDOUBLE 8
#define TLAST 9

class T4TriggerDY2015 : public T4TriggerDY2014
{
  public:
    T4TriggerDY2015(void);
    virtual ~T4TriggerDY2015(void) {}

    /*! \brief returns a human-readable name of the plugin*/
    string getPluginName(void) {return "2015-DY";}
};

#endif
