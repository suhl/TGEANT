#ifndef T4TRIGGER2016_HH
#define T4TRIGGER2016_HH

#include "T4TriggerDVCS2012.hh"
/*!
 * \brief Implementation for the 2016 MuonTrigger (DVCS)
 */

// trigger defines - what triggerbit is for what trigger?
#define TCAMERA 0
#define TMIDDLE 1
#define TLADDER 2
#define TOUTER 3
#define TLAST 9

class T4TriggerDVCS2016 : public T4TriggerDVCS2012
{
  public:
    T4TriggerDVCS2016(void);
    virtual ~T4TriggerDVCS2016(void) {}

    /*! \brief returns a human-readable name of the plugin*/
    string getPluginName(void) {return "2016-DVCS";}
    
    /*! \brief returns the triggermask*/
    int getTriggerMask(void);
};

#endif
