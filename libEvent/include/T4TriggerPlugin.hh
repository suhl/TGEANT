#ifndef T4TRIGGERPLUGIN_HH
#define T4TRIGGERPLUGIN_HH

#include <string>
#include <vector>
#include <cmath>
#include <algorithm>
#include <map>
#include <fstream>
#include <list>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "T4Event.hh"
#include "T4SMessenger.hh"
#include "T4SGlobals.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

using namespace xercesc;
using namespace std;

inline double getOne(void) {return 1;}

class T4TriggerPlugin
{
  public:
    T4TriggerPlugin(void);
    virtual ~T4TriggerPlugin(void) {}

    virtual int getTriggerMask(void) = 0;
    virtual bool hasTriggered(int _trigMask) = 0;
    virtual string getPluginName(void) = 0;

    void setEventPointer(T4Event* _newEvent) {t4Event = _newEvent;}

    void setRandomFunction(double (*_inFunc)(void)) {getTriggerRand =_inFunc;}

    void loadInnerX(string path) {readMatrixFile(path, ixMatrix_up);
      std::copy(&ixMatrix_up[0][0], &ixMatrix_up[31][31], &ixMatrix_dn[0][0]);}
    void loadLadderX(string path) {readMatrixFile(path, lxMatrix);}
    void loadMiddleX(string path) {readMatrixFile(path, mxMatrix_up);
      std::copy(&mxMatrix_up[0][0], &mxMatrix_up[31][31], &mxMatrix_dn[0][0]);}
    void loadMiddleY(string path) {readMatrixFile(path, myMatrix);}
    void loadOuterY(string path) {readMatrixFile(path, oyMatrix);}
    void loadLast(string path)
    {
      if (path.substr(path.size() - 3, 3) == "xml")
        readLASTFile(path, lastMatrix_Y1);
      else
        readMatrixFile(path, lastMatrix_Y1);
      std::copy(&lastMatrix_Y1[0][0], &lastMatrix_Y1[31][31],&lastMatrix_Y2[0][0]);
    }

    void setEfficI4X_up(std::list<double> entries) {setEffic(entries, ixMatrix_up, true, 31);}
    void setEfficI5X_up(std::list<double> entries) {setEffic(entries, ixMatrix_up, false);}
    void setEfficI4X_dn(std::list<double> entries) {setEffic(entries, ixMatrix_dn, true, 31);}
    void setEfficI5X_dn(std::list<double> entries) {setEffic(entries, ixMatrix_dn, false);}

    void setEfficL4X(std::list<double> entries) {setEffic(entries, lxMatrix, true, 31);}
    void setEfficL5X(std::list<double> entries) {setEffic(entries, lxMatrix, false);}

    void setEfficO3Y(std::list<double> entries) {setEffic(entries, oyMatrix, true, 15);}
    void setEfficO4Y1(std::list<double> entries) {setEffic(entries, oyMatrix, false, 16);}
    void setEfficO4Y2(std::list<double> entries) {setEffic(entries, oyMatrix, false);}

    void setEfficM4X_up(std::list<double> entries) {setEffic(entries, mxMatrix_up, true, 31);}
    void setEfficM5X_up(std::list<double> entries) {setEffic(entries, mxMatrix_up, false);}
    void setEfficM4X_dn(std::list<double> entries) {setEffic(entries, mxMatrix_dn, true, 31);}
    void setEfficM5X_dn(std::list<double> entries) {setEffic(entries, mxMatrix_dn, false);}

    void setEfficM4Y(std::list<double> entries) {setEffic(entries, myMatrix, true, 31);}
    void setEfficM5Y(std::list<double> entries) {setEffic(entries, myMatrix, false);}

    void setEfficLast1(std::list<double> entries) {setEffic(entries, lastMatrix_Y1, true, 31);
						   setEffic(entries, lastMatrix_Y2, true, 31);}
    void setEfficLast2(std::list<double> entries) {setEffic(entries, lastMatrix_Y1, false);
						   setEffic(entries, lastMatrix_Y2, false);}
    void setEfficLast2_Y1(std::list<double> entries) {setEffic(entries, lastMatrix_Y1, false);}
    void setEfficLast2_Y2(std::list<double> entries) {setEffic(entries, lastMatrix_Y2, false);}

    void setTimePileUp(double time) {timePileUp = time;}
    void setTimeOT(double time) {timeOT = time;}
    void setTimeLT(double time) {timeLT = time;}
    void setTimeMT(double time) {timeMT = time;}
    void setTimeIT(double time) {timeIT = time;}
    void setTimeLAST(double time) {timeLAST = time;}
    void setTimeVI(double time) {timeVI = time;}
    void setTimeVO(double time) {timeVO = time;}


  protected:
    /*! \brief Parse the tracking hits and check for hodoscope hits using the trigger matrices */
    void doHodoLogic(void);

    /*! \brief Checks for a hit in the hcals and fills the two calo bools. 
     * Lower threshold: 3 times the most probable energy loss of a muon = 3*1.8 GeV = 5.4 GeV.
     * The second threshold is used for a standalone calorimetric trigger at 16.2 GeV 
     * equivalent to 9 times the typival muon response. (But not in all years!)
     */
    void doCaloLogic(double threshold_hcal1, double threshold_hcal2,
        double threshold_purecalo = -1, double threshold_ecal1 = -1);

    /*! \brief Checks for a coincidence of two trigger. */
    bool checkTriggerCoincidence(vector<double>& trig1, vector<double>& trig2, int& trigMask, double timing, int bit, bool checkVeto = false);
    
    /*! \brief Checks for a coincidence with the vetos. */
    bool checkVetoCoincidence(vector<double>& trig, int& trigMask, int bit);

    /*! \brief Calculates the reduced time */
    double getTRed(T4HitData* hit, double beamStartPosition);

    vector<double> inner, middleX, middleY, ladder, outer, last;
    vector<double> vetoInner, vetoOuter;
    
    bool rpd,calo,purecalo ;
    /*! \brief Trigger matrices. */
    double ixMatrix_up[32][32];
    double ixMatrix_dn[32][32];
    double lxMatrix[32][32];
    double mxMatrix_up[32][32];
    double mxMatrix_dn[32][32];
    double myMatrix[32][32];
    double oyMatrix[32][32];
    double lastMatrix_Y1[32][32];
    double lastMatrix_Y2[32][32];
    
    /*! \brief this is the time-difference of the muon-start at the start point that is seperable. If the incident muon starts in +- of this time to the incident muon, it can trigger */
    double timePileUp;
    /*! \brief reduced time difference for two hodoscope hits */
    double timeOT, timeLT, timeMT, timeIT, timeLAST;
    /*! \brief Veto time window to reject triggers */
    double timeVI, timeVO;
    
    /*! \brief pointer on the function for the random number generator - used for the efficiencies in TGEANT and CORAL
     * In TGEANT it should be set on the 1. returner-function above, because we do not want any efficiencies in here.
     * In CORAL it should be set to the CsRandom::getFlat function.
     */
    double (*getTriggerRand)(void);

    T4Event* t4Event;

  private:
    XercesDOMParser* parserLAST;

    void readMatrixFile(string fileName, double[32][32]);
    void readLASTFile(string fileName, double[32][32]);
    
    void setEffic(std::list<double> entries, double[32][32], bool isRow, int chOffset = 0);
    void setEffic(int channel, double effic, double[32][32], bool isRow);

    class CaloTriggerInfo
    {
      public:
        double energyDeposit;
        double x;
        double y;
        int index;
        bool operator< (const CaloTriggerInfo &rhs) const {return energyDeposit < rhs.energyDeposit;}
        double getDist (const CaloTriggerInfo &rhs) const {return sqrt( pow((x-rhs.x),2.)+pow((y-rhs.y),2.));}
    };
};

#endif
