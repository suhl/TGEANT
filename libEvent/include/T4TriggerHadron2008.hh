#ifndef T4TRIGGER2008_HH
#define T4TRIGGER2008_HH

#include "T4TriggerPlugin.hh"
/*!
 * \brief Implementation for the 2008 Hadron Trigger
 */

// trigger defines - what triggerbit is for what trigger?
#define TINNER 0
#define TMIDDLE 1
#define TLADDER 2
#define TOUTER 3
#define TCALO 4
#define TINCLMT 8
#define TLAST 9

class T4TriggerHadron2008 : public T4TriggerPlugin
{
  public:
    T4TriggerHadron2008(void);
    virtual ~T4TriggerHadron2008(void) {}

    /*! \brief returns the triggermask*/
    virtual int getTriggerMask(void);

    /*! \brief checks if the event has triggered at all*/
    virtual bool hasTriggered(int _trigMask);

    /*! \brief returns a human-readable name of the plugin*/
    virtual string getPluginName(void) {return "2008-Hadron";}
};


#endif
