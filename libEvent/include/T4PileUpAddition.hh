#ifndef T4PileUpAddition_HH_
#define T4PileUpAddition_HH_


#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <ctime>
#include <algorithm> 


#include "T4SGlobals.hh"
#include "T4Event.hh"
#include "T4OutputBinary.hh"

#include <CLHEP/Random/JamesRandom.h>
#include <CLHEP/Random/RandPoisson.h>
#include <CLHEP/Random/RandFlat.h>

#include <sys/sendfile.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>



/*! \class T4PileUpAddition
 *  \brief This file adds pileup from a binfile to a given T4Event pointer
 */
class T4PileUpAddition {
public:
  /*! \brief standard destructor */
  ~T4PileUpAddition ( void );

  /*! \brief gets the instance */
  static T4PileUpAddition* getInstance ( void );
  
  /*! \brief initializes the singleton with parameters filename to read pileup from, mean pileup per event, timegate for pileup and T4Event pointer to add pileup to*/
  int initialize(string _fileName, double _rate, double _tgate, T4Event* writeTo, long int _seed, bool _useCache = false, string _cacheFolder = "");
  
  inline void setEvent(T4Event* writeTo){myEventPointerOut = writeTo;}
  inline void setSeed(long int _seed){myRandom->setSeed(_seed);}
  
  
  /*! \brief adds the pileup by throwing a number of events and reading them and adding them */
  bool addPileUp();
  
  /*! \brief clean up the cache */
  void cleanUpCache();
  
private:
  /*! \brief our instance */
  static T4PileUpAddition* single;
  /*! \brief standard constructor */
  T4PileUpAddition ( void );
  
  string fileName;
  
  T4OutputBackEnd* myBinaryFile;
  T4Event* myEventPointerBin;
  T4Event* myEventPointerOut;
  
  CLHEP::HepJamesRandom* myRandom;
  
  double tgate;
  double rate;
  string chosenFile;
  bool cachingActive;
  
};









#endif

