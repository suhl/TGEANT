#ifndef T4OUTPUTBACKEND_HH_
#define T4OUTPUTBACKEND_HH_

#include <iostream>
#include <ostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "T4Event.hh"

/*! \class T4OutputBackEnd
 *  \brief A base class for the TGEANT output system
 *  This class contains all the necessary functions for the TGEANT-input-output system.
 *  It features streamed loading of big files, saving events in fixed-size-chunks and much more.
 *  Note that not all the features make sense for all the implementations!
 */
class T4OutputBackEnd
{
  public:
    /*! \brief standard constructor */
	T4OutputBackEnd(void) {
		t4Event = NULL;
		streamT4Event = NULL;
		useSplitting = false;
		useSplitPiping = false;
		eventPerChunk = 0;}
    /*! \brief standard destructor */
    virtual ~T4OutputBackEnd(void) {if (streamT4Event != NULL) delete streamT4Event;}

    /*! \brief load all the events from a file into a vector 
    */
    virtual void load(std::string, std::vector<T4Event>*) {}
    
    /*! \brief opens a file for reading in streaming-mode */
    virtual bool streamLoad(std::string) { return true; }
    
    /*! \brief closes stream-mode */
    virtual void streamClose(void) {}
    
    /*! \brief sets the event splitting variables */
	void setChunkSplitting(bool _useSplitting, int _eventsPerChunk,
			bool _useSplitPiping) {
		useSplitting = _useSplitting;
		eventPerChunk = _eventsPerChunk;
		useSplitPiping = _useSplitPiping;}
    
    
    /*! \brief returns the event-pointer for stream-reading */
    inline T4Event* streamGetEventPointer(void) {
      if (streamT4Event == NULL) streamT4Event = new T4Event;
      return streamT4Event;}
    
    /*! \brief get the next event from the file into the pointer streamT4Event */
    virtual bool streamGetNextEvent(void) {return false;}
    
    /*! \brief resets the stream to begin all-over */
    virtual void streamReset(void) {}
    
    /*! \brief return number of the events in the file in total */
    virtual int streamGetEventNumber(void) {return 0;}
    
    /*! \brief saves the event from the t4Event pointer to the file */
    virtual void save(void) {}
    /*! \brief saves the performance of the TGEANT run 
     *  \param loadTime the time needed for loading
     *  \param totalRunTime the total execution time of TGEANT in the beam-on-phase
     *  \param startSeed the seed used for starting this TGEANT run 
     *  This function saves some performance information into the outfile.
     *  It may be useful to find eventuell problems on the batch-system where some hosts are slow.
     */
    virtual void savePerformance(double loadTime, double totalRunTime,
        long int startSeed) {
      loadTime = 0;
      totalRunTime = 0;
      startSeed = 0;}

      /*! \brief closes the output file
       */
    virtual void close(void) {}

    /*! \brief sets the event pointer to save
     *  \param t4Event_ the new T4Event pointer from which the data will be copied to the outfile
     */ 
    void setEvent(T4Event* t4Event_) {t4Event = t4Event_;}

  protected:
    bool useSplitting;
    bool useSplitPiping;
    int eventPerChunk;
    T4Event* t4Event;
    T4Event* streamT4Event;
    std::string saveFileName;

    /*! \brief checks if the savefile already exists */
    void checkFile(void);
};

#endif /* T4OUTPUTBACKEND_HH_ */
