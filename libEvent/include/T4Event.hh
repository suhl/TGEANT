#ifndef T4EVENT_HH_
#define T4EVENT_HH_

#include "config.hh"


#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
#include <cmath>
#include <stdint.h>

/*! \class T4BeamParticle
 *  \brief a Lujet like structure
 */
class T4BeamParticle {
  public:
    /*! \brief status-codes and tree-information
     * k[0] = K-Code
     * k[1] = particle ID (Geant4/PDG)
     * k[2] = origin line number
     * k[3] = daughter particles begin line number
     * k[4] = daughter particles end line number
     */
    int k[5];     
    /*! \brief kinematic information of the particle
     * p[0,1,2,3] = px,py,pz,E [GeV] = lorentzvector
     * p[4] invariant Mass [GeV]
     */
    float p[5]; 
    /*! \brief This is an auxiliary number not used in TGEANT
     */
    int aux;

    T4BeamParticle(void) {};
    ~T4BeamParticle(void) {};
};

/*! \class T4Trajectory
 *  \brief A class holding the information of a simulated trajectory inside the simulation
 */
class T4Trajectory {
  public:
    /*! \brief prints the debug info of the trajectories */
    void printDebug();
    /*! \brief the trackid of this trajectory */
    int trackId;
    /*! \brief the trackid of the parent track of this trajectory */
    int parentId;
    /*! \brief Geant4 PDG particle ID */
    int particleId;
    /*! \brief position of start of the trajectory [mm] */
    float position[3];
    float time;
    /*! \brief momentum at the start of the trajectory [MeV] */
    float momentum[3];

    T4Trajectory(void) {};
    ~T4Trajectory(void) {};
};
/*! \brief the Pythia6 generator settings struct 
 * For a detailed description of the generator settings
 * please see the pythia6 manual at
 * http://arxiv.org/abs/hep-ph/0603175
 */
class T4PySubs {
  public:
    int msel;                   
    int mselpd;                
    int msub[500];  	
    int kfin[2][80];  	
    float ckin[200];  				 

    T4PySubs(void) {};
    ~T4PySubs(void) {};
  
};
/*! \brief the Pythia6 generator parameters struct 
 * For a detailed description of the generator parameters
 * please see the pythia6 manual at
 * http://arxiv.org/abs/hep-ph/0603175
 */
class T4PyPars {
  public:
    int mstp[200];                   
    float parp[200];                
    int msti[200];                   
    float pari[200];  				 

    T4PyPars(void) {};
    ~T4PyPars(void) {};
  
};
/*! \class T4BeamData
 *  This class contains the generator information.
 *  Take care when using the values here, as these may have different units than 
 *  used in the rest of TGEANT!
 */
    
class T4BeamData {
  public:
    /*! \brief vertex position in mm */
    double vertexPosition[3];
    /*! \brief time at which the vertex happens */
    double vertexTime;
    /*! \brief The generator code that was used */
    int generator;
    
    double aux;
    /*! \brief bjorken x*/
    float x_bj;
    
    /*! \brief transmitted energy fraction y=(E-E')/E*/
    float y;
    /*! \brief invariant mass square of gamma* p system */
    float w2;
    /*! \brief invariant momentum transfer square */
    float q2;
    /*! \brief absolute energy transfer nu=E-E' */
    float nu;
    /*! \brief the uservars of the event generator */
    float uservar[20];
    /*! \brief the lst vars of the event generator */
    int lst[40];
    /*! \brief the parl data of the event generator */
    float parl[30];
    /*! \brief the cut data of the event generator */
    float cut[14];
    /*! \brief number of lujets */
    unsigned int nBeamParticle;
    /*! \brief list of lujets - here called T4BeamParticle 
     * Take special care of the units here, they generator dependant!
     */
    std::vector<T4BeamParticle> beamParticles;
    /*! \brief number of trajectories
     */
    unsigned int nTrajectories;
    /*! \brief Trajectory container
     * These are the four-vectors of particles at the vertex of there creation.
     * Contains also secondary vertices. Units here are in TGEANT standard.
     */
    std::vector<T4Trajectory> trajectories;
    T4PySubs pysubs;
    T4PyPars pypars;

    T4BeamData(void) {};
    ~T4BeamData(void) {};
    void setDefault(void);
};
/*! \class T4HitData 
 *  \brief This class contains the information about in hit in a detector
 *  It is used for calorimeters, trackers, triggers
 *  Note that for the usage in different detector-types, some values have different meanings!
 */
    
class T4HitData {
  public:
    /*! \brief prints this hitdata */
    void printData();
    /*! \brief name of the detector that registered the hit */
    std::string detectorName;
    /*! \brief ID of the detector that registered the hit */
    int detectorId;
    /*! \brief Channel number in which the hit was registered */
    int channelNo;
    /*! \brief The id number of the MC track that generated the hit */
    int trackId;
    /*! \brief Geant4 PDG particle ID */
    int particleId;
    /*! \brief total particle energy in [MeV] */
    double particleEnergy;
    /*! \brief absolute time in ns */
    double time;
    /*! \brief Beta of the particle at the hit position */
    double beta;
    /*! \brief Energydeposit in [MeV]*/
    double energyDeposit;
    /*! \brief hitPosition in MRS [mm] */
    double hitPosition[3];       // (MRS)
    /*! \brief first hit position in MRS [mm] */
    double primaryHitPosition[3]; // (MRS)
    /*! \brief last hit position in MRS  [mm] (except in calo use)
     *  In case of usage in calorimeters this value is the DRS position of the struck cell
     */
    double lastHitPosition[3];    // (MRS)
    /*! \brief momentum at the time of the hit [MeV] */
    double momentum[3];

    T4HitData(void) {};
    ~T4HitData(void) {};
    
  
};

/*! \brief Class that holds information about a det line from detectors.dat regarding 2dim efficiency maps
 */
class T4effic2Dline {
public:
  T4effic2Dline(void){dataArray = NULL;};
  ~T4effic2Dline(void){if (dataArray!= NULL) for (int i =0; i < binsX;i++)delete[] dataArray[i];};
  /*! \brief converts a concenated hist-in-str to a 2d double array */
  void convertBlob();
  /*! \brief returns the value in the array corresponding to the position */
  double getEffic(double _x, double _y);
  
  /*! \brief save the TH2D as a TGA file to edit with a GFX editor */
  void saveToTGA(std::string _fileName);
  /*! \brief load the dataArray from TGA*/
  static void loadFromTGA(std::string _fileName);
  
  void saveToBlob(std::string _fileName);
  void loadFromBlob(std::string _fileName);
  
  std::string tbname,detname,unit,year;
  int binsX,binsY;
  double startX,startY,endX,endY,efficMean;
  double binWidthX,binWidthY;
  double** dataArray;
  std::string blob;
};


class T4PmtData {
  public:
    std::string detectorName;
    int detectorId;
    int channelNo;
    double randomTime;
    double firstEventTime;
    int nsPerBin;
    int windowSize;
    std::vector<int> digits;

    T4PmtData(void) {};
    ~T4PmtData(void) {};
  
};
/*! \class T4RichData 
 *  \brief holds the information about a single hit in the RICH detector
 */
    
class T4RichData {
  public:
    std::string detectorName;
    int detectorId;
    /*! \brief hitPosition in MRS */
    double photonHitPosition[3];        // (MRS)
    /*! \brief Position of photon production in MRS */
    double photonProductionPosition[3]; // (MRS)
    /*! \brief momentum of the mother particle that generated the photon */
    double momentumMotherParticle[3];
    /*! \brief track id of the parent track */
    int parentTrackId;
    /*! \brief energy of the cherenkov photon */
    double photonEnergy;
    /*! \brief hit time of the photon */
    double time;
    /*! \brief pad Position x in the DRS*/
    double xPadPosition;             // (DRS)
    /*! \brief pad Position y in the DRS */
    double yPadPosition;             // (DRS)
    /*! \brief opening angle of the cherenkov cone */
    double cerenkovAngle;

    T4RichData(void) {};
    ~T4RichData(void) {};
  
};
/*! \class T4Event
 * \brief This class holds all the MC data for a single event processed by TGEANT
 */
class T4Event
{
  public:
    
    /*! \brief merges another event with a timing offset into this one */
    void mergeEvent(T4Event* _toMerge, double timingOffset);
    /*! \brief holds the generator information */
    T4BeamData beamData;                //|| don't change comments
    /*! \brief holds the hits from the tracking detectors */
    std::vector<T4HitData> tracking;    //||
    /*! \brief holds the hits from trigger hodoscopes */
    std::vector<T4HitData> trigger;     //||
    /*! \brief holds the hit information from calorimeter modules 
     *  This is integrated per channel-id. So only one hit per channel id can occur
     */
    std::vector<T4HitData> calorimeter; //||
    /*! \brief holds the hit information from the rich */
    std::vector<T4RichData> rich;       //||
    /*! \brief contains PMT information - only valid with optical physics enabled */
    std::vector<T4PmtData> pmt;         //||
    /*! \brief the triggermask */
    int trigMask;					    //||
    /*! \brief The time this event took to generate */
    double processingTime;            //||

    /*! \brief Clears the datastructs */
    void clear(void);
    /*! \brief prints the event info */
    void printEventInfo(bool _verbose=false);
    /*! \brief prints the generator information */
    void printBeamData(void);
    
    T4Event(void);
    ~T4Event(void) {};
private:
    void mergeHitData(std::vector<T4HitData>* _into, std::vector<T4HitData>* _toMerge,int trajectoryOffset, double timeOffset);
    void mergeCaloHitData(std::vector<T4HitData>* _into, std::vector<T4HitData>* _toMerge,int trajectoryOffset, double timeOffset);
};

#endif /* T4EVENT_HH_ */
