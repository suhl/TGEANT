#include "toolbox.hh"

class Vertex_Studies : ToolboxPlugin
{
  public:
    Vertex_Studies(void);
    ~Vertex_Studies(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH1D* positionVertex_z;
    TH2D* positionVertex_xy;

    TH1D* beamEnergy_vertex;
    TH1D* energyLoss_origin_vertex;
    TH2D* energyLoss_origin_vertexVSz;
    
};

static Vertex_Studies* vertex_Studies = new Vertex_Studies();

Vertex_Studies::Vertex_Studies(void)
{
  myName = "Vertex_Studies";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string Vertex_Studies::getDescription(void)
{
  std::string description = "Toolbox example to get vertex position using beamData and trajectory access. \n";
  description += "We also plot the energy loss of the beam particle between the starting point upstream of the target (e.g. -9m)\n";
  description += "and the vertex point. From the 2D plot we can extract the energy loss parameters that can be used in the target code, see:\n";
  description += "setELossParams(__offset__ * CLHEP::MeV, __slope__ * CLHEP::MeV / CLHEP::mm);";
  return description;
}

void Vertex_Studies::beginOfEvents(void)
{
  // place to initialize the histograms
  positionVertex_z = new TH1D("positionVertex_z", "vertex z position", 300, -9000, 5000);
  positionVertex_z->GetXaxis()->SetTitle("z position [mm]");

  positionVertex_xy = new TH2D("positionVertex_xy", "vertex x/y position", 60, -30, 30, 60, -30, 30);
  positionVertex_xy->GetXaxis()->SetTitle("x position [mm]");
  positionVertex_xy->GetYaxis()->SetTitle("y position [mm]");

  beamEnergy_vertex = new TH1D("positionVertex_z", "energy of beam particle at vertex", 800, 150, 200);
  beamEnergy_vertex->GetXaxis()->SetTitle("beam energy [GeV]");

  energyLoss_origin_vertex = new TH1D("energyLoss_origin_vertex", "energy loss of beam particle from z=-9m to vertex", 200, 0, 10);
  energyLoss_origin_vertex->GetXaxis()->SetTitle("energy loss [GeV]");
  
  energyLoss_origin_vertexVSz = new TH2D("energyLoss_origin_vertex", "energy loss of beam particle from z=-9m to vertex (2D)",900,-3000,1000,800,0,2);
  
}

bool Vertex_Studies::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  // TGEANT unit system is equal to Geant4: mm, ns and MeV
  // But the unit system of an event generator output (lst, cut, uservar...) is not changed in TGEANT!

  // We get the vertex position directly from beamData:
  positionVertex_z->Fill(event->beamData.vertexPosition[2]);
  positionVertex_xy->Fill(event->beamData.vertexPosition[0], event->beamData.vertexPosition[1]);

  // All particles from the primary vertex can be accessed via beamData.beamParticles vector.
  // The primary particle is at first position (but we can also loop over all beamParticles to
  // search another particle, e.g. the scattered beam particle).
  // We are just taking the first energy in the following code: the incoming beam particle.
  
  double beamEnergy;
//   for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
  if (event->beamData.beamParticles.size() == 0)
    return true;
  
    T4BeamParticle* particle = &event->beamData.beamParticles.at(0);
    // LEPTO format and Pythia k-code documentation can be found on TGEANT homepage!
//     if (particle->k[0] == 21 && particle->k[1] == -211) {
      // this is the primary beam particle (pi-)
      beamEnergy = particle->p[3]; // this is in GeV (event generator convention)!
      beamEnergy_vertex->Fill(beamEnergy);
//       break; // we are finished...
//     }
//   }

  // Finally we want to know the initial beam energy.
  // We can access such information via the trajectories. For each particle in TGEANT (including
  // secondaries) a new trajectory is saved. The position and momentum are the values at the
  // production point of the particle.
  // The primary particle is again at first position, but as example we loop over all and search for
  // its properties.
  for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
    T4Trajectory* trajectory = &event->beamData.trajectories.at(i);
    if (trajectory->parentId == 0) {
      // this check is enough for the primary particle, but we can also check the id...
      // since this code should work for pions and muon (+/-) we take the abs(particleId) to get the mass
      double energy;
      
      if (abs(trajectory->particleId) == 211) {
        energy = sqrt(
            pow(trajectory->momentum[0], 2) + pow(trajectory->momentum[1], 2)
                + pow(trajectory->momentum[2], 2)
                + pow(135. /*MeV*/, 2));
      } else if (abs(trajectory->particleId) == 13) {
        energy = sqrt(
            pow(trajectory->momentum[0], 2) + pow(trajectory->momentum[1], 2)
                + pow(trajectory->momentum[2], 2)
                + pow(105. /*MeV*/, 2));
      } else {
	cout << "Something wrong: Particle ID '" << trajectory->particleId << "' not defined here!" << endl;
	energy = 0;
      }
      
      energyLoss_origin_vertex->Fill(energy / 1000. /*GeV*/ - beamEnergy);
      energyLoss_origin_vertexVSz->Fill(event->beamData.vertexPosition[2],energy / 1000. - beamEnergy);
    }
  }

  return true;
}

void Vertex_Studies::endOfEvents(void)
{
  // place to write the histograms
  positionVertex_z->Write();
  positionVertex_xy->Write();

  beamEnergy_vertex->Write();
  energyLoss_origin_vertex->Write();
  
  energyLoss_origin_vertexVSz->Write();
  
}
