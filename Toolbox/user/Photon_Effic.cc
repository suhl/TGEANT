#include "toolbox.hh"

class Photon_Effic : ToolboxPlugin
{
  public:
    Photon_Effic(void);
    ~Photon_Effic(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_Generator[5][5];
    TH2D* hist_ECAL1[5][5];

    TH1D* photonAbs;

    void extrapToECAL1(double& x, double& y, double* position, double* momentum);
};

static Photon_Effic* photon_Effic = new Photon_Effic();

Photon_Effic::Photon_Effic(void)
{
  myName = "Photon_Effic";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string Photon_Effic::getDescription(void)
{
  std::string description = "Photon_Effic for DVCS";
  return description;
}

void Photon_Effic::beginOfEvents(void)
{
  // place to initialize the histograms
  string title_energy[] = {"0.7 GeV < E_#gamma < 1 GeV",
                           "1 GeV < E_#gamma < 5 GeV",
                           "5 GeV < E_#gamma < 10 GeV",
                           "10 GeV < E_#gamma < 30 GeV",
                           "30 GeV < E_#gamma < 60 GeV"};

  string title_vertex[] = {"Vertex pos UP-UP",
                           "Vertex pos UP",
                           "Vertex pos CENTER",
                           "Vertex pos DN",
                           "Vertex pos DN-DN"};

  const Int_t Nx = 134;
  const Int_t Ny = 98;

  Double_t BinsX[Nx + 1];
  Double_t BinsY[Ny + 1];
  BinsX[0] = -4. * 30.085 - 13. * 13.7 - 8. * 14.3 - 12. * 3.83
      - 30 * 3.83 / 3.0;
  BinsY[0] = -6. * 30.085 - 6 * 13.7 - 13. * 7.5 - 6. * 3.83 - 18 * 3.83 / 3.0;

  for (Int_t i_x = 0; i_x < Nx; i_x++) {
    if (fabs(i_x - Nx / 2 + 0.05) <= 30)
      BinsX[i_x + 1] = BinsX[i_x] + 3.83 / 3.0;
    else if (fabs(i_x - Nx / 2 + 0.05) <= 42)
      BinsX[i_x + 1] = BinsX[i_x] + 3.83;
    else if (fabs(i_x - Nx / 2 + 0.05) <= 50)
      BinsX[i_x + 1] = BinsX[i_x] + 14.3;
    else if (fabs(i_x - Nx / 2 + 0.05) <= 63)
      BinsX[i_x + 1] = BinsX[i_x] + 13.7;
    else
      BinsX[i_x + 1] = BinsX[i_x] + 30.085;
  }

  for (Int_t i_y = 0; i_y < Ny; i_y++) {
    if (fabs(i_y - Ny / 2 + 0.05) <= 18)
      BinsY[i_y + 1] = BinsY[i_y] + 3.83 / 3.0;
    else if (fabs(i_y - Ny / 2 + 0.05) <= 24)
      BinsY[i_y + 1] = BinsY[i_y] + 3.83;
    else if (fabs(i_y - Ny / 2 + 0.05) <= 37)
      BinsY[i_y + 1] = BinsY[i_y] + 7.5;
    else if (fabs(i_y - Ny / 2 + 0.05) <= 43)
      BinsY[i_y + 1] = BinsY[i_y] + 13.7;
    else
      BinsY[i_y + 1] = BinsY[i_y] + 30.085;
  }

  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
//      hist_Generator[i][j] = new TH2D(((string)"hist_Generator["+intToStr(i)+"]["+intToStr(j)+ "]").c_str()
//          , (title_energy[i] + " -- " + title_vertex[j]).c_str(), 800, -400, 400, 600, -300, 300);
//      hist_ECAL1[i][j] = new TH2D(((string)"hist_ECAL1["+intToStr(i)+"]["+intToStr(j)+ "]").c_str()
//          , (title_energy[i] + " -- " + title_vertex[j]).c_str(), 800, -400, 400, 600, -300, 300);

      hist_Generator[i][j] = new TH2D(((string)"hist_Generator["+intToStr(i)+"]["+intToStr(j)+ "]").c_str(),
          (title_energy[i] + " -- " + title_vertex[j]).c_str(), Nx, BinsX, Ny, BinsY);
      hist_ECAL1[i][j] = new TH2D(((string)"hist_ECAL1["+intToStr(i)+"]["+intToStr(j)+ "]").c_str(),
          (title_energy[i] + " -- " + title_vertex[j]).c_str(), Nx, BinsX, Ny, BinsY);
    }
  }

  photonAbs=new TH1D("photonAbs","photonAbs",5000,-10000,40000);
}

bool Photon_Effic::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  double weight = 1;
//  double weight = event->beamData.uservar[15];
//  if (event->beamData.uservar[2] <= 0 || weight <= 0 || event->beamData.uservar[16] <= 0)
//      return false;

//  // kine cuts
//  if (event->beamData.q2 < 1 || event->beamData.q2 > 20)
//    return false;
//  if (event->beamData.x_bj < 0.005 || event->beamData.x_bj > 0.27)
//    return false;
//  if (event->beamData.y < 0.05 || event->beamData.y > 0.9)
//    return false;
cout<<"new event!"<<endl;
  for (unsigned int bp = 0; bp < event->beamData.beamParticles.size(); bp++) {

    double photonEnergy = 0;
    T4BeamParticle* part = &event->beamData.beamParticles.at(bp);
     // photon
     if (part->k[0] == 1 && part->k[1] == 22) {
       photonEnergy = part->p[3];
     } else {
       continue;
     }
cout<<"photon  "<<flush;
     double vertexPosition[3];
     double momentum[3];
     int photonId;
     bool photonTrajFound = false;

     for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
       if (event->beamData.trajectories.at(i).parentId == 1 &&
           event->beamData.trajectories.at(i).particleId == 22) {
        photonId = event->beamData.trajectories.at(i).trackId;
        vertexPosition[0] = event->beamData.trajectories.at(i).position[0] / 10.;
        vertexPosition[1] = event->beamData.trajectories.at(i).position[1] / 10.;
        vertexPosition[2] = event->beamData.trajectories.at(i).position[2] / 10.;
        momentum[0] = event->beamData.trajectories.at(i).momentum[0];
        momentum[1] = event->beamData.trajectories.at(i).momentum[1];
        momentum[2] = event->beamData.trajectories.at(i).momentum[2];

        if (abs(momentum[0] / 1000. - part->p[0]) < 10E-3
            && abs(momentum[1] / 1000. - part->p[1]) < 10E-3
            && abs(momentum[2] / 1000. - part->p[2]) < 10E-3) {
          photonTrajFound = true;
          cout << "passt  "<<flush;
          break;
        }
      }
    }

    if (!photonTrajFound)
      continue;

    int index_E = -1;
    int index_V = -1;
    // target length: -311.2 -- -71.2 == 240
    // 48 cm per cell
    if (vertexPosition[2] >= -311.2 && vertexPosition[2] < -311.2 + 48)
      index_V = 0;
    else if (vertexPosition[2] >= -311.2 + 48 && vertexPosition[2] < -311.2 + 48 * 2)
      index_V = 1;
    else if (vertexPosition[2] >= -311.2 + 48 * 2 && vertexPosition[2] < -311.2 + 48 * 3)
      index_V = 2;
    else if (vertexPosition[2] >= -311.2 + 48 * 3 && vertexPosition[2] < -311.2 + 48 * 4)
      index_V = 3;
    else if (vertexPosition[2] >= -311.2 + 48 * 4 && vertexPosition[2] < -311.2 + 48 * 5)
      index_V = 4;
    else {
      cout<<"vertex cut"<<endl;
      return false;
    }

    if (photonEnergy >= 0.7 && photonEnergy < 1)
      index_E = 0;
    else if (photonEnergy >= 1 && photonEnergy < 5)
      index_E = 1;
    else if (photonEnergy >= 5 && photonEnergy < 10)
      index_E = 2;
    else if (photonEnergy >= 10 && photonEnergy < 30)
      index_E = 3;
    else if (photonEnergy >= 30 && photonEnergy < 60)
      index_E = 4;
    else {
      cout << "e cut"<<endl;
      continue;
    }

    double x,y;

    // fill denominator:
    extrapToECAL1(x, y, vertexPosition, momentum);
    hist_Generator[index_E][index_V]->Fill(x, y, weight);
    cout<<"fillGen  "<<flush;
    // check the cluster
    bool ecal0_decay = false;
    bool ecal1_decay = false;
    bool ecal2_decay = false;

    vector<int> daughters;
    for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
      if (event->beamData.trajectories.at(i).parentId == photonId) {
        double z = event->beamData.trajectories.at(i).position[2];
        if (z > 10000 && z < 11500)
          ecal1_decay = true;
        else if (z > 32500 && z < 34000)
          ecal2_decay = true;
        else if (z > 850 && z < 950)
          ecal0_decay = true;

//        if(abs(y)<20 && x>-38 && x<-25)
          photonAbs->Fill(z);
          cout<<z<<" "<<flush;

        if (ecal0_decay || ecal1_decay || ecal2_decay)
          daughters.push_back(event->beamData.trajectories.at(i).trackId);
      }
    }
    daughters.push_back(photonId);
    cout<<"daughters="<<daughters.size()<<"  ("<<photonId<<")  "<<endl;

    bool ecal0 = false;
    bool ecal1 = false;
    bool ecal2 = false;
    bool clusterFound = false;
    for (unsigned int hits = 0; hits < event->calorimeter.size(); hits++) {
      T4HitData* hit = &event->calorimeter.at(hits);
      cout<< "hit: " << hit->trackId<<"  "<<hit->particleId<<endl;

  //    if (abs(hit->energyDeposit/1000.-photonEnergy) < 0.1 * photonEnergy) {
        for (unsigned int i = 0; i < daughters.size(); i++) {
          if (hit->trackId == daughters.at(i)) {
            clusterFound = true;
            cout<<"found cluster"<<endl;
            if (hit->primaryHitPosition[2] > 20000)
              ecal2 = true;
            else if (hit->primaryHitPosition[2] > 10000)
              ecal1 = true;
            else
              ecal0 = true;

            break;
          }
        }
  //    }

      if (clusterFound)
        break;
    }

    cout << "Fill ECAL? " << ecal0 << " " << ecal0_decay << " " << ecal1 << " "
        << ecal1_decay << " " << ecal2 << " " << ecal2_decay
        << " " << photonEnergy << "    "<<x << "    "<<y

        << endl;
    if ((ecal0 && ecal0_decay) || (ecal1 && ecal1_decay) || (ecal2 && ecal2_decay))
      hist_ECAL1[index_E][index_V]->Fill(x, y, weight);
  }
cout<<"finished"<<endl;
  return true;
}

void Photon_Effic::endOfEvents(void)
{
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      cout << "Energy=" << i << " Vertex=" << j << "  Effic: "
          << hist_ECAL1[i][j]->GetSumOfWeights() << " / "
          << hist_Generator[i][j]->GetSumOfWeights() << " = "
          << hist_ECAL1[i][j]->GetSumOfWeights()
              / hist_Generator[i][j]->GetSumOfWeights() << endl;

      hist_ECAL1[i][j]->Write();
      hist_Generator[i][j]->Write();
    }
  }
  photonAbs->Write();
}

void Photon_Effic::extrapToECAL1(double& x, double& y, double* position,
    double* momentum)
{
  double zPos_E1 = 1107.4;

  double zDiff = zPos_E1 - position[2];
  double ticks = zDiff / momentum[2];

  x = position[0] + ticks * momentum[0];
  y = position[1] + ticks * momentum[1];
}
