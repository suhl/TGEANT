#include "toolbox.hh"

#include "T4TriggerDVCS2016.hh"
#include "T4TriggerDVCS2012.hh"

class HO03_Dimension : ToolboxPlugin
{
  public:
    HO03_Dimension(void);
    ~HO03_Dimension(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_H03[7][3];
    TH2D* hist_H04[7][3];
    
    TH2D* kine_map[7][3];

    T4TriggerPlugin* trigger_org;
    T4TriggerPlugin* trigger_one;
    T4TriggerPlugin* trigger_mod;
};

static HO03_Dimension* hO03_Dimension = new HO03_Dimension();

HO03_Dimension::HO03_Dimension(void)
{
  myName = "HO03_Dimension";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string HO03_Dimension::getDescription(void)
{
  std::string description = "Study of HO03 modifications (and other triggers) for the 2016 DVCS run.";
  return description;
}

void HO03_Dimension::beginOfEvents(void)
{
  // place to initialize the histograms
  int xBin_3 = 300 * 4;
  double xMin_3 = -150;
  double xMax_3 = 150;
  int yBin_3 = 200 * 4;
  double yMin_3 = -100;
  double yMax_3 = 100;

  int xBin_4 = 600 * 4;
  double xMin_4 = -300;
  double xMax_4 = 300;
  int yBin_4 = 300 * 4;
  double yMin_4 = -150;
  double yMax_4 = 150;

  string title[] = {" - all events",
                    " - MT",
                    " - LT",
                    " - LAST",
                    " - !OT",
                    " - OT",
                    " - no trigger at all"};

  string addon[] = {"",
                    " (trigger matrix modified)",
                    " (trigger matrix 1)"};

  for (int i = 0; i < 7; i++) {
    for (int j = 0; j < 3; j++) {
      hist_H03[i][j] = new TH2D(((string) "hist_H03_" + intToStr(i) + intToStr(j)).c_str(),
          ((string) "HO03" + title[i] + addon[j]).c_str(),
          xBin_3, xMin_3, xMax_3, yBin_3, yMin_3, yMax_3);

      hist_H04[i][j] = new TH2D(((string) "hist_H04_" + intToStr(i) + intToStr(j)).c_str(),
          ((string) "HO04" + title[i] + addon[j]).c_str(),
          xBin_4, xMin_4, xMax_4, yBin_4, yMin_4, yMax_4);
      
      kine_map[i][j] = new TH2D(((string) "hist_kine_" + intToStr(i) + intToStr(j)).c_str(),
          ((string) "KINE" + title[i] + addon[j]).c_str(),
          2000, 0, 1, 2000, 0.5, 22);
    }
  }

  trigger_org = new T4TriggerDVCS2012();
  trigger_one = new T4TriggerDVCS2012();
  trigger_mod = new T4TriggerDVCS2016();

  if (getenv("TGEANT") == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__, __FILE__,
        "T4SettingsFile: $TGEANT not set! Please set $TGEANT to the TGEANT installation directory.");
  }

  std::string pathToTGEANT = getenv("TGEANT");

  trigger_org->loadInnerX(pathToTGEANT + "/resources/triggerMatrix/inner_x.mtx");
  trigger_org->loadLadderX(pathToTGEANT + "/resources/triggerMatrix/ladder_x.mtx");
  trigger_org->loadMiddleX(pathToTGEANT + "/resources/triggerMatrix/middle_x.mtx");
  trigger_org->loadMiddleY(pathToTGEANT + "/resources/triggerMatrix/middle_y.mtx");
  trigger_org->loadOuterY(pathToTGEANT + "/resources/triggerMatrix/outer_y.mtx");
  trigger_org->loadLast(pathToTGEANT + "/resources/triggerMatrix/last_dvcs2012.xml");
  
  trigger_one->loadInnerX(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");
  trigger_one->loadLadderX(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");
  trigger_one->loadMiddleX(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");
  trigger_one->loadMiddleY(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");
  trigger_one->loadOuterY(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");
  trigger_one->loadLast(pathToTGEANT + "/resources/triggerMatrix/matrix_one.mtx");

  trigger_mod->loadInnerX(pathToTGEANT + "/resources/triggerMatrix/inner_x.mtx");
  trigger_mod->loadLadderX(pathToTGEANT + "/resources/triggerMatrix/ladder_x_2016mod.mtx");
  trigger_mod->loadMiddleX(pathToTGEANT + "/resources/triggerMatrix/middle_x_2016mod.mtx");
  trigger_mod->loadMiddleY(pathToTGEANT + "/resources/triggerMatrix/middle_y.mtx");
  trigger_mod->loadOuterY(pathToTGEANT + "/resources/triggerMatrix/outer_y_2015.mtx");
  trigger_mod->loadLast(pathToTGEANT + "/resources/triggerMatrix/last_dy2015.mtx");
}

bool HO03_Dimension::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  if (event->beamData.generator == 3) {
    double weight = event->beamData.uservar[15];

    if (event->beamData.uservar[2] <= 0 || weight <= 0 || event->beamData.uservar[16] <= 0)
      return false;

    // kine cuts
  //  if (event->beamData.q2 < 0.1 || event->beamData.q2 > 20)
  //    return false;
    if (event->beamData.q2 < 1. || event->beamData.q2 > 20)
      return false;
   if (event->beamData.x_bj < 0.005 || event->beamData.x_bj > 0.27)
     return false;
    if (event->beamData.y < 0.05 || event->beamData.y > 0.9)
      return false;


    int triggerMask[3];
//     triggerMask[0] = event->trigMask;

    trigger_org->setEventPointer(event);
    trigger_one->setEventPointer(event);
    trigger_mod->setEventPointer(event);
  
    triggerMask[0] = trigger_org->getTriggerMask();
    triggerMask[2] = trigger_one->getTriggerMask();
    triggerMask[1] = trigger_mod->getTriggerMask();

    bool middle[3], ladder[3], outer[3], last[3];

    for (int i = 0; i < 3; i++) {
      middle[i] = (((1 << 1) & triggerMask[i]) > 0);
      ladder[i] = (((1 << 2) & triggerMask[i]) > 0);
      outer[i] = (((1 << 3) & triggerMask[i]) > 0);
      last[i] = (((1 << 9) & triggerMask[i]) > 0);
    }

    bool h3Hit = false;
    T4HitData* hit3;

    bool h4Hit = false;
    T4HitData* hit4;

    int scatMuId = 0;
    for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
      if (event->beamData.trajectories.at(i).parentId == 1
          && event->beamData.trajectories.at(i).particleId == -13/*22*/) {
        scatMuId = event->beamData.trajectories.at(i).trackId;
        break;
      }
    }

    for (unsigned int hits = 0; hits < event->tracking.size(); hits++) {
      T4HitData* hit = &event->tracking.at(hits);
      if (hit->detectorId == 6666 && hit->trackId == scatMuId) {
        if (hit->channelNo == 0) {
          hit3 = hit;
	  if (h3Hit) return false;
          h3Hit = true;
        } else if (hit->channelNo == 1) {
          hit4 = hit;
	  if (h4Hit) return false;
          h4Hit = true;
        }
      }
    }

    if (!(h3Hit && h4Hit))
      return false;
    
    for (int i = 0; i < 3; i++) {
      hist_H03[0][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
      hist_H04[0][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
      kine_map[0][i]->Fill(event->beamData.y, event->beamData.q2, weight);

      if (middle[i]) {
        hist_H03[1][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[1][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[1][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }

      if (ladder[i]) {
        hist_H03[2][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[2][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[2][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }

      if (last[i]) {
        hist_H03[3][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[3][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[3][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }

      if (!middle[i] && !ladder[i] && !last[i]) {
        hist_H03[4][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[4][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[4][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }

      if (outer[i]) {
        hist_H03[5][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[5][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[5][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }

      if (!middle[i] && !ladder[i] && !last[i] && !outer[i]) {
        hist_H03[6][i]->Fill(hit3->hitPosition[0]/10., hit3->hitPosition[1]/10., weight);
        hist_H04[6][i]->Fill(hit4->hitPosition[0]/10., hit4->hitPosition[1]/10., weight);
	kine_map[6][i]->Fill(event->beamData.y, event->beamData.q2, weight);
      }
    }

  } else if (event->beamData.generator == 0) {
    for (unsigned int hits = 0; hits < event->tracking.size(); hits++) {
       T4HitData* hit = &event->tracking.at(hits);
       if (hit->detectorId == 6666 && hit->particleId == -13) {
         if (hit->channelNo == 0) {
           hist_H03[0][0]->Fill(hit->hitPosition[0]/10., hit->hitPosition[1]/10.);
         } else if (hit->channelNo == 1) {
           hist_H04[0][0]->Fill(hit->hitPosition[0]/10., hit->hitPosition[1]/10.);

         }
       }
     }
  } else {
    cout << "Unknown generator! Expected HEPGen or BeamOnly..." << endl;
  }

  return true;
}

void HO03_Dimension::endOfEvents(void)
{
  // place to write the histograms
  for (int i = 0; i < 7; i++) {
    for (int j = 0; j < 3; j++) {
      hist_H03[i][j]->Write();
      hist_H04[i][j]->Write();
      kine_map[i][j]->Write();
    }
  }
}
