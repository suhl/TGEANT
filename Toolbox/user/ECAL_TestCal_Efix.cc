#include "toolbox.hh"

class ECAL_TestCal_Efix : ToolboxPlugin
{
  public:
    ECAL_TestCal_Efix(void);
    ~ECAL_TestCal_Efix(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_hits;
    TH1D* hist_clusterSize;
    TH1D* hist_energy;
    TH1D* hist_time;

    TH1D* hist_clusterSize_threshold;


    unsigned int eventCounter;
    int cells;
};

static ECAL_TestCal_Efix* eCAL_TestCal_Efix = new ECAL_TestCal_Efix();

ECAL_TestCal_Efix::ECAL_TestCal_Efix(void)
{
  myName = "ECAL_TestCal_Efix";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string ECAL_TestCal_Efix::getDescription(void)
{
  std::string description = "TestCal setup with 40GeV electon beam.";
  return description;
}

void ECAL_TestCal_Efix::beginOfEvents(void)
{
  cells = 11;

  // place to initialize the histograms
  hist_hits = new TH2D("cell_map","average energy deposit in each cell", cells, 0, cells, cells, 0, cells);
  hist_clusterSize = new TH1D("cluster_size", "number of cells", 500, 0, 500);
  hist_clusterSize_threshold = new TH1D("hist_clusterSize_threshold", "number of cells", 500, 0, 500);

  hist_energy = new TH1D("energy", "energy of cluster", 1000, 0, 100);

  eventCounter = 0;
  hist_time = new TH1D("processing_time", "time for one event", 800, 0, 200);

}

bool ECAL_TestCal_Efix::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  hist_clusterSize->Fill(event->calorimeter.size());
  eventCounter++;

  double energy = 0;
  int counter = 0;

//  double eScale = 0.92;
  double eScale = 1.;


  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    T4HitData* hit = &event->calorimeter.at(i);
    if (hit->energyDeposit*eScale < 200)
      continue;
    int channel = hit->channelNo - 7778;

    int xBin = channel%cells + 1;
    int yBin = (int) floor((double)channel/cells) + 1;

    hist_hits->SetBinContent(xBin, yBin, hist_hits->GetBinContent(xBin, yBin) + hit->energyDeposit*eScale/1000.);
    energy += hit->energyDeposit*eScale;

//    if (hit->energyDeposit > 200)
      counter++;

  }
  hist_clusterSize_threshold->Fill(counter);

  hist_energy->Fill(energy/1000.);
  hist_time->Fill(event->processingTime);

  return true;
}

void ECAL_TestCal_Efix::endOfEvents(void)
{
  // place to write the histograms
  hist_hits->Scale(1./eventCounter);
  hist_clusterSize->Scale(1./eventCounter);
  hist_clusterSize_threshold->Scale(1./eventCounter);
  hist_energy->Scale(1./eventCounter);
  hist_time->Scale(1./eventCounter);

  hist_hits->Write();
  hist_clusterSize->Write();
  hist_clusterSize_threshold->Write();
  hist_energy->Write();
  hist_time->Write();

  TH1D* profx = hist_hits->ProjectionX(); profx->Write();
  TH1D* profy = hist_hits->ProjectionY(); profy->Write();

  TH1D* hist_profileX = new TH1D("profile_x", "energy profile x", cells, 0, cells);
  TH1D* hist_profileY = new TH1D("profile_y", "energy profile y", cells, 0, cells);

  for (int i = 1; i <= cells; i++) {
    hist_profileX->SetBinContent(i, hist_profileX->GetBinContent(i-1) + profx->GetBinContent(i));
    hist_profileY->SetBinContent(i, hist_profileY->GetBinContent(i-1) + profy->GetBinContent(i));
  }
  hist_profileX->Write();
  hist_profileY->Write();
}
