#include "toolbox.hh"

class HadronicInteractionPoint : ToolboxPlugin
{
  public:
    HadronicInteractionPoint(void);
    ~HadronicInteractionPoint(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH1D* positionInteraction_z;
};

static HadronicInteractionPoint* hadronicInteractionPoint = new HadronicInteractionPoint();

HadronicInteractionPoint::HadronicInteractionPoint(void)
{
  myName = "HadronicInteractionPoint";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string HadronicInteractionPoint::getDescription(void)
{
  std::string description = "Toolbox example to get hadronic interaction point of pion beam. (For DY setup.)\n";
  description += "Generator: GEANT";
  return description;
}

void HadronicInteractionPoint::beginOfEvents(void)
{
  // place to initialize the histograms
  positionInteraction_z = new TH1D("positionInteraction_z", "pion beam z position interaction", 340, -2700, 700);
  positionInteraction_z->GetXaxis()->SetTitle("z position [mm]");
}

bool HadronicInteractionPoint::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  double zPos = -9000 /*mm*/;

  if (event->beamData.generator == 0) {
    for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
      T4Trajectory* trajectory = &event->beamData.trajectories.at(i);
      if (trajectory->parentId == 1 && trajectory->position[2] > zPos) {
        zPos = trajectory->position[2];
      }
    }
    positionInteraction_z->Fill(zPos);
  } else {
    positionInteraction_z->Fill(event->beamData.vertexPosition[2]);
  }

  return true;
}

void HadronicInteractionPoint::endOfEvents(void)
{
  // place to write the histograms
  positionInteraction_z->Write();
}
