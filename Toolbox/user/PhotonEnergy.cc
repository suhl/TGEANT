#include "toolbox.hh"

class PhotonEnergy : ToolboxPlugin
{
  public:
    PhotonEnergy(void);
    ~PhotonEnergy(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    static const int nCalo = 3;
    TH1D* histEnergyEcal[nCalo];
    TH1D* histEnergyDifference[nCalo];
    TH2D* histEnergyDifference2D[nCalo];
};

static PhotonEnergy* photonEnergy = new PhotonEnergy();

PhotonEnergy::PhotonEnergy(void)
{
  myName = "PhotonEnergy";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string PhotonEnergy::getDescription(void)
{
  std::string description = "This is an example for the usage of the toolbox.\n";
  description +=
      "We are plotting the energy of a single DVCS/BH photon in all calorimeters\n";
  description +=
      "and looking at the difference to the photon energy on generator level.";
  return description;
}

void PhotonEnergy::beginOfEvents(void)
{
  // place to initialize the histograms
  string name[] = { "ECAL0", "ECAL1", "ECAL2" };
  for (int i = 0; i < nCalo; i++) {
    histEnergyEcal[i] = new TH1D((name[i] + "_photonEnergy").c_str(),
        (name[i] + " photon cluster energy (all) - TGEANT level").c_str(), 200,
        0, 200);
    histEnergyEcal[i]->SetXTitle("E_{#gamma, cluster} [GeV]");

    histEnergyDifference[i] = new TH1D((name[i] + "_energyDifference").c_str(),
        (name[i] + " energy generator - energy cluster (all) - TGEANT level")
            .c_str(), 400, -200, 200);
    histEnergyDifference[i]->SetXTitle(
        "E_{#gamma, generator} - E_{#gamma, cluster} [GeV]");

    histEnergyDifference2D[i] =
        new TH2D((name[i] + "_energyDifference2D").c_str(),
            (name[i]
                + " correlation between energy generator and energy cluster (all) - TGEANT level")
                .c_str(), 200, 0, 200, 200, 0, 200);
    histEnergyDifference2D[i]->SetXTitle("E_{#gamma, generator} [GeV]");
    histEnergyDifference2D[i]->SetYTitle("E_{#gamma, cluster} [GeV]");
  }
}

bool PhotonEnergy::processEvent(T4Event* event)
{
  // kick out DD
  if (event->beamData.beamParticles.size() != 6)
    return false;

  double mcWeight = event->beamData.uservar[2];

  T4BeamParticle* photon;
  bool photonFound = false;
  for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
    photon = &event->beamData.beamParticles.at(i);
    if (photon->k[1] == 22) {
      photonFound = true;
      break;
    }
  }
  if (!photonFound)
    return false;

  double* vertexPos = event->beamData.vertexPosition;

  ////////////////////////
  // sum up calo energy
  double energyEcal0 = 0;
  double energyEcal1 = 0;
  double energyEcal2 = 0;
  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    T4HitData* hit = &event->calorimeter.at(i);

    if (hit->detectorId >= 3000 && hit->detectorId < 3800) { // hcal
      continue;
    } else if (hit->detectorId >= 2000 && hit->detectorId < 3000) {
      energyEcal0 += hit->energyDeposit / 1000.;
    } else if (hit->detectorId >= 6900 && hit->detectorId < 8700) {
      energyEcal1 += hit->energyDeposit / 1000.;
    } else if (hit->detectorId >= 3800 && hit->detectorId < 6900) {
      energyEcal2 += hit->energyDeposit / 1000.;
    } else
      cout << "Error: Unknown module id!" << endl;
  }

  // all in cm now!!
  double factor;
  double x;
  double y;
  double photonEnergy = photon->p[3];

  // ECAL0
  factor = (91.600 - vertexPos[2] / 10.) / photon->p[2];
  x = vertexPos[0] / 10. + factor * photon->p[0];
  y = vertexPos[1] / 10. + factor * photon->p[1];

  if ((fabs(x) >= 42 && fabs(x) <= 66 && fabs(y) <= 30) || // cmtx line 1+2
      (fabs(x) <= 54 && y >= -54 && y <= -30) || // cmtx line 3
      (fabs(x) <= 54 && y >= 31.2 && y <= 55.2)) { // cmtx line 4
    histEnergyEcal[0]->Fill(photonEnergy, mcWeight);
    histEnergyDifference[0]->Fill(photonEnergy - energyEcal0, mcWeight);
    histEnergyDifference2D[0]->Fill(photonEnergy, energyEcal0, mcWeight);
    return true;
  }

  // ECAL1
  factor = (1107.400 - vertexPos[2] / 10.) / photon->p[2];
  x = vertexPos[0] / 10. + factor * photon->p[0];
  y = vertexPos[1] / 10. + factor * photon->p[1];
  if ((fabs(x) <= 198.66 && fabs(y) <= 143) && //complete module
      !(fabs(x) <= 38.3 && fabs(y) <= 22.98)) { // except hole
    histEnergyEcal[1]->Fill(photonEnergy, mcWeight);
    histEnergyDifference[1]->Fill(photonEnergy - energyEcal1, mcWeight);
    histEnergyDifference2D[1]->Fill(photonEnergy, energyEcal1, mcWeight);
    return true;
  }

  // ECAL2
  factor = (3325.200 - vertexPos[2] / 10.) / photon->p[2];
  x = vertexPos[0] / 10. + factor * photon->p[0];
  y = vertexPos[1] / 10. + factor * photon->p[1];
  if ((fabs(x) <= 122.56 && fabs(y) <= 91.92) && //complete module
      !(x <= 38.3 && x >= 0 && fabs(y) <= 19.15)) { // except hole
    histEnergyEcal[2]->Fill(photonEnergy, mcWeight);
    histEnergyDifference[2]->Fill(photonEnergy - energyEcal2, mcWeight);
    histEnergyDifference2D[2]->Fill(photonEnergy, energyEcal2, mcWeight);
    return true;
  }

  // track not successful extrapolated to one of the ECALs:
  return false;
}

void PhotonEnergy::endOfEvents(void)
{
  for (int i = 0; i < nCalo; i++) {
    histEnergyEcal[i]->Write();
    histEnergyDifference[i]->Write();
    histEnergyDifference2D[i]->Write();
  }
}
