#include "toolbox.hh"

class HM_Position : ToolboxPlugin
{
  public:
    HM_Position(void);
    ~HM_Position(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_H4MY[2];
    TH2D* hist_H4MX[2];
    
    TH2D* hist_H5MY[2];
    TH2D* hist_H5MX[2];
};

static HM_Position* hm_position = new HM_Position();

HM_Position::HM_Position(void)
{
  myName = "HM_Position";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string HM_Position::getDescription(void)
{
  std::string description = "Position of MT hodoscopes.";
  return description;
}

void HM_Position::beginOfEvents(void)
{
  // place to initialize the histograms
  hist_H4MY[0] = new TH2D("H4MY-all", "H4MY all MC hits", 1600, -100, 1500, 1600, -800, 800);
  hist_H4MX[0] = new TH2D("H4MX-all", "H4MX all MC hits", 1600, -100, 1500, 1600, -800, 800); 
  hist_H4MY[1] = new TH2D("H4MY-MT", "H4MY all MC hits with MT", 1600, -100, 1500, 1600, -800, 800);
  hist_H4MX[1] = new TH2D("H4MX-MT", "H4MX all MC hits with MT", 1600, -100, 1500, 1600, -800, 800);
  
  hist_H5MY[0] = new TH2D("H5MY-all", "H5MY all MC hits", 2100, -100, 2000, 1600, -800, 800);
  hist_H5MX[0] = new TH2D("H5MX-all", "H5MX all MC hits", 2100, -100, 2000, 1600, -800, 800);
  hist_H5MY[1] = new TH2D("H5MY-MT", "H5MY all MC hits with MT", 2100, -100, 2000, 1600, -800, 800);
  hist_H5MX[1] = new TH2D("H5MX-MT", "H5MX all MC hits with MT", 2100, -100, 2000, 1600, -800, 800);
}

bool HM_Position::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  // in case of HEPGen, let's take the DVCS weight
  double weight = 1.;
  if (event->beamData.generator == 3)
    weight = event->beamData.uservar[15];

  int triggerMask = event->trigMask;
  bool middle = (((1 << 1) & triggerMask) > 0);
  bool ladder = (((1 << 2) & triggerMask) > 0);
  bool outer = (((1 << 3) & triggerMask) > 0);
  bool last = (((1 << 9) & triggerMask) > 0);
  
  for (unsigned int hits = 0; hits < event->trigger.size(); hits++) {
    T4HitData* hit = &event->trigger.at(hits);
    double x = hit->hitPosition[0];
    double y = hit->hitPosition[1];
    
    if (hit->detectorName.substr(0,5) == "HM04Y") {
      hist_H4MY[0]->Fill(x, y, weight);
      if (middle)
	hist_H4MY[1]->Fill(x, y, weight);
      
    } else if (hit->detectorName.substr(0,5) == "HM04X") {
      hist_H4MX[0]->Fill(x, y, weight);
      if (middle)
	hist_H4MX[1]->Fill(x, y, weight);
      
    } else if (hit->detectorName.substr(0,5) == "HM05Y") {
      hist_H5MY[0]->Fill(x, y, weight);
      if (middle)
	hist_H5MY[1]->Fill(x, y, weight);
      
    } else if (hit->detectorName.substr(0,5) == "HM05X") {
      hist_H5MX[0]->Fill(x, y, weight);
      if (middle)
	hist_H5MX[1]->Fill(x, y, weight);
    }
  }
  
  return true;
}

void HM_Position::endOfEvents(void)
{
  // place to write the histograms
  for (int i = 0; i < 2; i++) {
    hist_H4MY[i]->Write();
    hist_H4MX[i]->Write();
    
    hist_H5MY[i]->Write();
    hist_H5MX[i]->Write();
  }
}
