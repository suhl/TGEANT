#include "toolbox.hh"
#include "T4OutputBinary.hh"

#include <ctime>

class ConvertToBinary : ToolboxPlugin
{
  public:
    ConvertToBinary(void);
    ~ConvertToBinary(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    T4OutputBinary* binOutput;
    
};

static ConvertToBinary* convertToBinary = new ConvertToBinary();

ConvertToBinary::ConvertToBinary(void)
{
  myName = "ConvertToBinary";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string ConvertToBinary::getDescription(void)
{
  std::string description = "Converter for ascii to binary\n";
  description +=
      "We convert a whole event to binary output - output file is written to env($BINFILE)\n";
  return description;
}

void ConvertToBinary::beginOfEvents(void)
{
  std::string binFile;
  if (getenv("BINFILE") != NULL)
    binFile= getenv("BINFILE");
  else
    T4SMessenger::getInstance()->printfMessage(T4SFatalError,__LINE__,__FILE__,"Environmental $BINFILE not set!\n");

  // place to initialize the histograms
  binOutput = new T4OutputBinary(binFile);
}

bool ConvertToBinary::processEvent(T4Event* event)
{
  binOutput->setEvent(event);
  binOutput->save();
  return false;
}

void ConvertToBinary::endOfEvents(void)
{
  binOutput->close();
}
