#include "toolbox.hh"

class FI03_Photon : ToolboxPlugin
{
  public:
    FI03_Photon(void);
    ~FI03_Photon(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_DUMMY[3];
    TH2D* hist_ECAL1;
    TH2D* hist_ECAL2;

    double counter[2];
};

static FI03_Photon* fI03_Photon = new FI03_Photon();

FI03_Photon::FI03_Photon(void)
{
  myName = "FI03_Photon";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string FI03_Photon::getDescription(void)
{
  std::string description = "FI03_Photon for HO04 hits";
  return description;
}

void FI03_Photon::beginOfEvents(void)
{
  // place to initialize the histograms
  int bins = 200;
  hist_DUMMY[0] = new TH2D("hist_DUMMY[0]", "DUMMY - UPstream of FI03", 675, -135, 135, 350, -70, 70);
  hist_DUMMY[1] = new TH2D("hist_DUMMY[1]", "DUMMY - DNstream of FI03", 675, -135, 135, 350, -70, 70);

  hist_ECAL1 = new TH2D("hist_ECAL1[0]", "ECAL1 - UPstream of FI03", bins, -40, 40, bins, -40, 40);
  hist_ECAL2 = new TH2D("hist_ECAL2[0]", "ECAL2 - UPstream of FI03", bins, -40, 40, bins, -40, 40);

  counter[0] = 0;
  counter[1] = 0;
}

bool FI03_Photon::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  double weight = 1.;
  int photonId=1;
  bool clusterFound = true;
  bool ecal1 = false;
  bool ecal2 = false;

  if (event->beamData.generator == 3) {

    weight = event->beamData.uservar[15];
    if (event->beamData.uservar[2] <= 0 || weight <= 0 || event->beamData.uservar[16] <= 0)
        return false;
    // kine cuts
    if (event->beamData.q2 < 1 || event->beamData.q2 > 20)
      return false;
    if (event->beamData.x_bj < 0.005 || event->beamData.x_bj > 0.27)
      return false;
    if (event->beamData.y < 0.05 || event->beamData.y > 0.9)
      return false;

    for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
      if (event->beamData.trajectories.at(i).parentId == 1 &&
          event->beamData.trajectories.at(i).particleId == 22) {
        photonId = event->beamData.trajectories.at(i).trackId;
        break;
      }
    }

    double photonEnergy = 0;
    for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
      T4BeamParticle* part = &event->beamData.beamParticles.at(i);
       // photon
       if (part->k[0] == 1 && part->k[1] == 22) {
         photonEnergy = part->p[3];
         break;
       }
    }

    vector<int> daughters;
    for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
      if (event->beamData.trajectories.at(i).parentId == photonId) {
        double z = event->beamData.trajectories.at(i).position[2];
        if ((z > 10000 && z < 11500) || (z > 32500 && z < 34000))
          daughters.push_back(event->beamData.trajectories.at(i).trackId);
      }
    }
    daughters.push_back(photonId);

    clusterFound = false;
    for (unsigned int hits = 0; hits < event->calorimeter.size(); hits++) {
      T4HitData* hit = &event->calorimeter.at(hits);
      for (unsigned int i = 0; i < daughters.size(); i++) {
        if (hit->trackId == daughters.at(i) && abs(hit->energyDeposit/1000.-photonEnergy) < 0.1 * photonEnergy) {
          clusterFound = true;
          if (hit->primaryHitPosition[2] > 20000)
            ecal2 = true;
          else if (hit->primaryHitPosition[2] > 10000)
            ecal1 = true;
          break;
        }
      }
      if (clusterFound)
        break;
    }



  }

  bool upHit = false;
  T4HitData* hitUp;

  bool dnHit = false;
//  T4HitData* hitDn;

  for (unsigned int hits = 0; hits < event->tracking.size(); hits++) {
    T4HitData* hit = &event->tracking.at(hits);
    if (hit->detectorId == 6666 && hit->particleId == 22 && hit->trackId == photonId) {
      if (hit->channelNo == 0) {
        upHit = true;
        hitUp = hit;
      } else if (hit->channelNo == 1) {
        dnHit = true;
//        hitDn = hit;
      }

      if (upHit && dnHit)
        break;
    }
  }

  if (upHit) {
    hist_DUMMY[0]->Fill(hitUp->hitPosition[0]/10., hitUp->hitPosition[1]/10., weight);
    counter[0] += weight;
  }

  if (upHit && dnHit && clusterFound) {
    hist_DUMMY[1]->Fill(hitUp->hitPosition[0]/10., hitUp->hitPosition[1]/10., weight);
    counter[1] += weight;

    if (ecal1)
      hist_ECAL1->Fill(hitUp->hitPosition[0]/10., hitUp->hitPosition[1]/10., weight);
    if (ecal2)
      hist_ECAL2->Fill(hitUp->hitPosition[0]/10., hitUp->hitPosition[1]/10., weight);
  }

  return true;
}

void FI03_Photon::endOfEvents(void)
{
  // place to write the histograms
  hist_DUMMY[2] = (TH2D*) hist_DUMMY[1]->Clone();
  hist_DUMMY[2]->Divide(hist_DUMMY[0]);
  hist_DUMMY[2]->SetName("ratio");
  hist_DUMMY[2]->SetTitle("Ratio DN/UP");

  for (int i = 0; i < 3; i++) {
    hist_DUMMY[i]->Write();
  }
  hist_ECAL1->Write();
  hist_ECAL2->Write();

  cout<<"Overall efficiency         : " << counter[1]/counter[0] <<endl;

}
