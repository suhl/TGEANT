#include "toolbox.hh"
#include "TH3D.h"
#include "TVector3.h"
#include "TLorentzVector.h"

struct koloCluster
{
    int mainIndex;
    double Energy;
    double position[3];
    vector<int> usedCells;
};

class ECAL_Clustering : ToolboxPlugin
{
  public:
    ECAL_Clustering(void);
    virtual ~ECAL_Clustering(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event *event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    void meanSum(TVector3& sum, double* _vec, double _fac);

    static const int nCalo = 3;

    vector<TH1 *> histList;
    vector<koloCluster> clusterList;

    vector<int> usedCells;

    TH2D *histHitPosition[3];
    TH1D *clusterSize;
    TH2D *clusterMeanPosition;
    TH1D *eDiff;
    TH1D *pi0mass;
    TH1D *clusterSizeKolosov;
    TH2D *clusterMeanKolosov;
    TH2D *pi0MassEDep;
    TH1D *eDiffKolosov;
    TH3D *vertexPosHist;
};

static ECAL_Clustering* ecal_Clustering = new ECAL_Clustering();

ECAL_Clustering::ECAL_Clustering(void) :
    ToolboxPlugin()
{
  myName = "ECAL_Clustering";
  pluginList::getInstance()->activated_classes.push_back(this);
}

string ECAL_Clustering::getDescription(void)
{
  return "A simple ToolboxPlugin for generating kolosov-style clusters, and saving their size. Then making some histograms.";
}

void ECAL_Clustering::beginOfEvents(void)
{
  // place to initialize the histograms
  clusterSize = new TH1D("clusterSizeOnTGEANTLVL", "clusterSizeOnTGEANTLVL",
      100, 0, 100);
  clusterMeanPosition = new TH2D("clusterMean", "clusterMean", 3000, -3000,
      3000, 3000, -3000, 3000);
  eDiff = new TH1D("eDiff", "eDiff", 2000, -20, 20);
  clusterSizeKolosov = new TH1D("clusterSizeOnTGEANTLVLKOLOSOV",
      "clusterSizeOnTGEANTLVLKOLOVOS", 100, 0, 100);
  clusterMeanKolosov = new TH2D("clusterMeanKolosov", "clusterMeanKolosov",
      3000, -3000, 3000, 3000, -3000, 3000);
  eDiffKolosov = new TH1D("eDiffKolosov", "eDiffKolosov", 2000, -20, 20);
  vertexPosHist = new TH3D("VertexPosition", "VertexPosition", 200, -4000, 0,
      50, -50, 50, 50, -50, 50);
  pi0mass = new TH1D("pi0Mass", "pi0Mass", 1200, -.2, 1.0);
  histHitPosition[0] = new TH2D("ECAL0_hitPos", "ECAL0 hit position", 800, -200,
      200, 600, -150, 150);
  histHitPosition[1] = new TH2D("ECAL1_hitPos", "ECAL1 hit position", 800, -200,
      200, 600, -150, 150);
  histHitPosition[2] = new TH2D("ECAL2_hitPos", "ECAL2 hit position", 800, -200,
      200, 600, -150, 150);
  
  pi0MassEDep = new TH2D("Pi0-mass energy dep","Pi0-mass energy dep",160,0,80,200,-0.05,0.05);

  histList.push_back(clusterSize);
  histList.push_back(clusterMeanPosition);
  histList.push_back(eDiff);
  histList.push_back(clusterSizeKolosov);
  histList.push_back(clusterMeanKolosov);
  histList.push_back(eDiffKolosov);
  histList.push_back(vertexPosHist);
  histList.push_back(pi0mass);
  histList.push_back(pi0MassEDep);
  histList.push_back(histHitPosition[0]);
  histList.push_back(histHitPosition[1]);
  histList.push_back(histHitPosition[2]);
}

bool ECAL_Clustering::processEvent(T4Event *event)
{
  clusterList.clear();
  usedCells.clear();

  //additional MC information
//     T4BeamParticle *photon;
//     bool photonFound = false;
//     for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
//         photon = &event->beamData.beamParticles.at(i);
//         if (photon->k[1] == 22) {
//             photonFound = true;
//             break;
//         }
//     }

  TVector3 vertexPos = TVector3(event->beamData.vertexPosition[0],
      event->beamData.vertexPosition[1], event->beamData.vertexPosition[2]);
//     TVector3 photonMom = TVector3(photon->p[0], photon->p[1], photon->p[2]);

//     if (vertexPos.Z() < -3261)
//         printf("Kappe holen!!\n");
//
//     if (vertexPos.Z() > -646)
//         printf("Andere Kappe \n");
  //fill the vertex pos into a simple 3d histo for conveniance
  vertexPosHist->Fill(vertexPos.Z(), vertexPos.X(), vertexPos.Y());
//     printf("%f %f %f \n",vertexPos.X(), vertexPos.Y(),vertexPos.Z());

//     if (!photonFound) {
//         return false;
//     }

//     return false;

  //now the calo stuff
  int maxCell;
  double maxEnergy = 0.0;
  double sumEnergy = 0.0;
  //build the sum and get the max cell for kolosov style clustering
  for (int i = 0; i < event->calorimeter.size(); i++) {
    sumEnergy += event->calorimeter.at(i).energyDeposit;
    if (event->calorimeter.at(i).energyDeposit > maxEnergy) {
      maxEnergy = event->calorimeter.at(i).energyDeposit;
      maxCell = i;
    }
  }

  //simple approximation: get all the cells energy and do the mean of the position weighted with the elos relative to the summed eloss

  TVector3 meanPos = TVector3(0.0, 0.0, 0.0);

  for (int i = 0; i < event->calorimeter.size(); i++)
    meanSum(meanPos, event->calorimeter.at(i).hitPosition,
        event->calorimeter.at(i).energyDeposit / sumEnergy);

  while (event->calorimeter.size() > usedCells.size()) {
    koloCluster myCluster;
    double localMaximum = 0.0;
    int localMaxCell = 0;
    for (int i = 0; i < event->calorimeter.size(); i++) {
      if (std::find(usedCells.begin(), usedCells.end(), i) != usedCells.end())
        continue;
      if (event->calorimeter.at(i).energyDeposit > localMaximum) {
        localMaximum = event->calorimeter.at(i).energyDeposit;
        localMaxCell = i;
      }
    }
    myCluster.Energy = 0.0;
    myCluster.mainIndex = localMaxCell;
    double radiusKolosov;
    //hit in ECAL0
    if (event->calorimeter.at(localMaxCell).hitPosition[2] < 1200)
      radiusKolosov = 80;
    else if (event->calorimeter.at(localMaxCell).hitPosition[2] > 1200
        && event->calorimeter.at(localMaxCell).hitPosition[2] < 15000)
      radiusKolosov = 150; //ECAL1
    else
      radiusKolosov = 80; //ECAL2

    //take one order more on threshold energy
    if (localMaximum / 1000. > 4)
      radiusKolosov *= 1.5;

    // now kolosov style
    TVector3 kolosovMax = TVector3(
        event->calorimeter.at(localMaxCell).hitPosition[0],
        event->calorimeter.at(localMaxCell).hitPosition[1],
        event->calorimeter.at(localMaxCell).hitPosition[2]);
    TVector3 kolosovMean = TVector3(0.0, 0.0, 0.0);

    for (int i = 0; i < event->calorimeter.size(); i++) {
      if (std::find(usedCells.begin(), usedCells.end(), i) != usedCells.end())
        continue;

      TVector3 hitPos = TVector3(event->calorimeter.at(i).hitPosition[0],
          event->calorimeter.at(i).hitPosition[1],
          event->calorimeter.at(i).hitPosition[2]);

      if ((hitPos - kolosovMax).Mag() < radiusKolosov) {
        myCluster.Energy += event->calorimeter.at(i).energyDeposit;
        usedCells.push_back(i);
        myCluster.usedCells.push_back(i);
      }
    }
    //then do the weighted mean position
    for (int i = 0; i < myCluster.usedCells.size(); i++) {
      if ((event->calorimeter.at(myCluster.usedCells.at(i)).hitPosition
          - kolosovMax).Mag() < radiusKolosov)
        meanSum(kolosovMean,
            event->calorimeter.at(myCluster.usedCells.at(i)).hitPosition,
            event->calorimeter.at(myCluster.usedCells.at(i)).energyDeposit
                / myCluster.Energy);
    }

    myCluster.position[0] = kolosovMean.X();
    myCluster.position[1] = kolosovMean.Y();
    myCluster.position[2] = kolosovMean.Z();

    clusterList.push_back(myCluster);

  }

//     printf("*** Kolosov-Clustering method results: %u clusters!\n",clusterList.size());
//     for (unsigned int i = 0; i < clusterList.size(); i++){
//       printf("Cluster %u: Pos: %f %f %f, Energy: %f, Cells: %u\n",i,clusterList.at(i).position[0],clusterList.at(i).position[1],clusterList.at(i).position[2],clusterList.at(i).Energy,clusterList.at(i).usedCells.size() );
//     }

  TLorentzVector firstGamma, secondGamma;
  if (clusterList.size() > 1)
    for (unsigned int i = 0; i < clusterList.size() - 1; i++) {
      if (clusterList.at(i).Energy / 1000. < 4)
        continue;

      TVector3 firstGammaDirection;
      firstGammaDirection.SetXYZ(clusterList.at(i).position[0] - vertexPos.X(),
          clusterList.at(i).position[1] - vertexPos.Y(),
          clusterList.at(i).position[2] - vertexPos.Z());
      firstGammaDirection.SetMag(clusterList.at(i).Energy / 1000.);
      firstGamma.SetVect(firstGammaDirection);
      firstGamma.SetE(clusterList.at(i).Energy / 1000.);
      for (unsigned int a = i + 1; a < clusterList.size(); a++) {
        if (clusterList.at(a).Energy / 1000. < 4)
          continue;
        TVector3 secondGammaDirection;

        secondGammaDirection.SetXYZ(
            clusterList.at(a).position[0] - vertexPos.X(),
            clusterList.at(a).position[1] - vertexPos.Y(),
            clusterList.at(a).position[2] - vertexPos.Z());
        secondGammaDirection.SetMag(clusterList.at(a).Energy / 1000.);
        secondGamma.SetVect(secondGammaDirection);
        secondGamma.SetE(clusterList.at(a).Energy / 1000.);
        TLorentzVector piZero = firstGamma + secondGamma;
        pi0mass->Fill(piZero.M());
	pi0MassEDep->Fill(piZero.E(),piZero.M()-134.9766/1000.);
      }
    }

  for (unsigned int i = 0; i < clusterList.size(); i++) {
    clusterSizeKolosov->Fill(clusterList.at(i).usedCells.size());
    clusterMeanKolosov->Fill(clusterList.at(i).position[0],
        clusterList.at(i).position[1]);

//         printf("%f\n",clusterList.at(i).position[2]);
    if (clusterList.at(i).position[2] < 1200)
      histHitPosition[0]->Fill(clusterList.at(i).position[0] / 10.,
          clusterList.at(i).position[1] / 10.);
    else if (clusterList.at(i).position[2] > 1200
        && clusterList.at(i).position[2] < 15000)
      histHitPosition[1]->Fill(clusterList.at(i).position[0] / 10.,
          clusterList.at(i).position[1] / 10.);
    else
      histHitPosition[2]->Fill(clusterList.at(i).position[0] / 10.,
          clusterList.at(i).position[1] / 10.);
  }

  return true;
}

void ECAL_Clustering::endOfEvents(void)
{
  for (int i = 0; i < histList.size(); i++)
    histList.at(i)->Write();
}

void ECAL_Clustering::meanSum(TVector3& sum, double* _vec, double _fac)
{
  sum += _fac * TVector3(_vec[0], _vec[1], _vec[2]);
}
