#include "toolbox.hh"
#include "TROOT.h"
#include "TF1.h"

class ECAL_Calibration : public ToolboxPlugin
{
  public:
    ECAL_Calibration(void) {}
    ~ECAL_Calibration(void) {}

    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH1D* histEnergyDiff_Gams[2];
    TH1D* histEnergyDiff_GamsRH[2];
    TH1D* histEnergyDiff_Shashlik[2];
    TH1D* histEnergyDiff_Mainz[2];
    TH1D* histEnergyDiff_Olga[2];
    TH1D* histEnergyDiff_Ecal0[2];

    TH2D* histEnergyDiff_Gams_2D[2];
    TH2D* histEnergyDiff_GamsRH_2D[2];
    TH2D* histEnergyDiff_Shashlik_2D[2];
    TH2D* histEnergyDiff_Mainz_2D[2];
    TH2D* histEnergyDiff_Olga_2D[2];
    TH2D* histEnergyDiff_Ecal0_2D[2];

    vector<TH1D*> th1d;
    vector<TH2D*> th2d;
    vector<TH2D*> th2d_noSave;

  protected:
    int bins;
    double xmin;
    double xmax;
    string mode;
};

void ECAL_Calibration::beginOfEvents(void)
{
  // place to initialize the histograms
  string name = mode;
  for (unsigned int i = 0; i < 2; i++) {
    if (i == 1)
      name = "corrected " + mode;
    histEnergyDiff_Gams[i] = new TH1D(("Gams_eDiff_" + name).c_str(), ("Gams: energy generator - energy cluster " + name).c_str(), 350, -10, 60);
    th1d.push_back(histEnergyDiff_Gams[i]);
    histEnergyDiff_GamsRH[i] = new TH1D(("GamsRH_eDiff_" + name).c_str(), ("GamsRH: energy generator - energy cluster " + name).c_str(), 350, -10, 60);
    th1d.push_back(histEnergyDiff_GamsRH[i]);
    histEnergyDiff_Shashlik[i] = new TH1D(("Shashlik_eDiff_" + name).c_str(), ("Shashlik: energy generator - energy cluster " + name).c_str(), 200, -10, 10);
    th1d.push_back(histEnergyDiff_Shashlik[i]);
    histEnergyDiff_Mainz[i] = new TH1D(("Mainz_eDiff_" + name).c_str(), ("Mainz: energy generator - energy cluster " + name).c_str(), 200, -10, 10);
    th1d.push_back(histEnergyDiff_Mainz[i]);
    histEnergyDiff_Olga[i] = new TH1D(("Olga_eDiff_" + name).c_str(), ("Olga: energy generator - energy cluster " + name).c_str(), 250, -10, 40);
    th1d.push_back(histEnergyDiff_Olga[i]);
    histEnergyDiff_Ecal0[i] = new TH1D(("Ecal0_eDiff_" + name).c_str(), ("Ecal0: energy generator - energy cluster " + name).c_str(), 250, -10, 40);
    th1d.push_back(histEnergyDiff_Ecal0[i]);
  }

  for (unsigned int i = 0; i < th1d.size(); i++) {
    if (mode == "gamma")
      th1d.at(i)->SetXTitle("E_{#gamma, generator} - E_{#gamma, cluster} [GeV]");
    else
      th1d.at(i)->SetXTitle("E_{e, generator} - E_{e, cluster} [GeV]");
  }

  histEnergyDiff_Gams_2D[0] = new TH2D(("Gams_2D_" + mode).c_str(), ("Gams" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_Gams_2D[0]);
  histEnergyDiff_GamsRH_2D[0] = new TH2D(("GamsRH_2D_" + mode).c_str(), ("GamsRH" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_GamsRH_2D[0]);
  histEnergyDiff_Shashlik_2D[0] = new TH2D(("Shashlik_2D_" + mode).c_str(), ("Shashlik" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_Shashlik_2D[0]);
  histEnergyDiff_Mainz_2D[0] = new TH2D(("Mainz_2D_" + mode).c_str(), ("Mainz" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_Mainz_2D[0]);
  histEnergyDiff_Olga_2D[0] = new TH2D(("Olga_2D_" + mode).c_str(), ("Olga" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_Olga_2D[0]);
  histEnergyDiff_Ecal0_2D[0] = new TH2D(("Ecal0_2D_" + mode).c_str(), ("Ecal0" + mode).c_str(), bins, xmin, xmax, bins, xmin, xmax);
  th2d.push_back(histEnergyDiff_Ecal0_2D[0]);

  for (unsigned int i = 0; i < th2d.size(); i++) {
    if (mode == "gamma") {
      th2d.at(i)->SetXTitle("E_{#gamma, generator} [GeV]");
      th2d.at(i)->SetYTitle("E_{#gamma, cluster} [GeV]");
    } else {
      th2d.at(i)->SetXTitle("E_{e, generator} [GeV]");
      th2d.at(i)->SetYTitle("E_{e, cluster} [GeV]");
    }
  }

  histEnergyDiff_Gams_2D[1] = new TH2D(("Gams_2D_helper_" + mode).c_str(), ("Gams" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_Gams_2D[1]);
  histEnergyDiff_GamsRH_2D[1] = new TH2D(("GamsRH_2D_helper_" + mode).c_str(), ("GamsRH" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_GamsRH_2D[1]);
  histEnergyDiff_Shashlik_2D[1] = new TH2D(("Shashlik_2D_helper_" + mode).c_str(), ("Shashlik" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_Shashlik_2D[1]);
  histEnergyDiff_Mainz_2D[1] = new TH2D(("Mainz_2D_helper_" + mode).c_str(), ("Mainz" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_Mainz_2D[1]);
  histEnergyDiff_Olga_2D[1] = new TH2D(("Olga_2D_helper_" + mode).c_str(), ("Olga" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_Olga_2D[1]);
  histEnergyDiff_Ecal0_2D[1] = new TH2D(("Ecal0_2D_helper_" + mode).c_str(), ("Ecal0" + mode).c_str(), bins, xmin, xmax, bins, -xmax, -xmin);
  th2d_noSave.push_back(histEnergyDiff_Ecal0_2D[1]);
}

bool ECAL_Calibration::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  double generatorEnergy = 0;
  if (event->beamData.beamParticles.size() == 1)
    generatorEnergy = event->beamData.beamParticles.at(0).p[3];
  else
    return false;

  // search for the module with maximum energy deposit
  double clusterEnergy = 0;
  unsigned int cellEntry;
  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    T4HitData* caloHit = &event->calorimeter.at(i);

    if (caloHit->energyDeposit > clusterEnergy) {
      clusterEnergy = caloHit->energyDeposit;
      cellEntry = i;
    }
  }

  if (clusterEnergy == 0)
    return false;

  // simple clustering...
//  double x0 = event->calorimeter.at(cellEntry).hitPosition[0];
//  double y0 = event->calorimeter.at(cellEntry).hitPosition[1];
  string moduleName = (string) event->calorimeter.at(cellEntry).detectorName;

//  double moduleSize;
//  if (moduleName == "MAINZ")
//    moduleSize = 80;//75
//  else if (moduleName == "OLGA")
//    moduleSize = 150;//143
//  else
//    moduleSize = 120;

  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    if (i == cellEntry)
      continue;

//    double x1 = event->calorimeter.at(i).hitPosition[0];
//    double y1 = event->calorimeter.at(i).hitPosition[1];

    //neighbor cluster?
//    if (fabs(x0 - x1) < moduleSize && fabs(y0 - y1) < moduleSize)
        clusterEnergy += event->calorimeter.at(i).energyDeposit;
  }

  double difference = generatorEnergy - clusterEnergy/1000.;

  if (moduleName == "GAMS") {
    histEnergyDiff_Gams[0]->Fill(difference);
    histEnergyDiff_Gams[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9469);
    histEnergyDiff_Gams_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_Gams_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  } else if (moduleName == "GAMSRH") {
    histEnergyDiff_GamsRH[0]->Fill(difference);
    histEnergyDiff_GamsRH[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9451);
    histEnergyDiff_GamsRH_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_GamsRH_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  } else if (moduleName == "SHASHLIK") {
    histEnergyDiff_Shashlik[0]->Fill(difference);
    histEnergyDiff_Shashlik[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9689);
    histEnergyDiff_Shashlik_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_Shashlik_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  } else if (moduleName == "MAINZ") {
    histEnergyDiff_Mainz[0]->Fill(difference);
    histEnergyDiff_Mainz[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9944);
    histEnergyDiff_Mainz_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_Mainz_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  } else if (moduleName == "OLGA") {
    histEnergyDiff_Olga[0]->Fill(difference);
    histEnergyDiff_Olga[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9843);
    histEnergyDiff_Olga_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_Olga_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  } else if (moduleName == "ECAL0") {
    histEnergyDiff_Ecal0[0]->Fill(difference);
    histEnergyDiff_Ecal0[1]->Fill(generatorEnergy - clusterEnergy/1000./0.9688);
    histEnergyDiff_Ecal0_2D[0]->Fill(generatorEnergy, clusterEnergy/1000.);
    histEnergyDiff_Ecal0_2D[1]->Fill(generatorEnergy, -clusterEnergy/1000.);
  }

  return true;
}

void ECAL_Calibration::endOfEvents(void)
{
  // place to save the histograms
  for (unsigned int i = 0; i < th1d.size(); i++)
    th1d.at(i)->Write();

  if (th2d.size() != th2d_noSave.size())
    cout<< "ERROR!! Size of th2d != th2d_noSave!!"<<endl;

  for (unsigned int i = 0; i < th2d.size(); i++) {
    th2d.at(i)->Write();

    // possion fit for x slices
    TAxis* axis = th2d_noSave.at(i)->GetYaxis();
    TF1* tf1 = new TF1("landau", "landau", axis->GetXmin(), axis->GetXmax());
    TObjArray* array = new TObjArray();
    th2d_noSave.at(i)->FitSlicesY(tf1, 0, -1, 0, "QNR", array);

    TF1* fit = new TF1("P1", "pol1", xmin + 1.0, xmax - 5.0 /*30,50*/);
    TH1D* profile = (TH1D*) array->At(1);
    profile->Scale(-1.0);
    profile->Fit(fit, "RQ");
    profile->SetTitle(((string) th2d.at(i)->GetName() + "    slope = " + doubleToStrFP(fit->GetParameter(1))).c_str());
    profile->SetName(((string) th2d.at(i)->GetName() + "_fit.root").c_str());
    profile->Write();

    delete tf1;
    delete array;
  }
}

class ECAL_Calibration_Electron : ECAL_Calibration
{
  public:
    ECAL_Calibration_Electron(void) {
      myName = "ECAL_Calibration_Electron";
      pluginList::getInstance()->activated_classes.push_back(this);
      bins = 840 / 4;
      xmin = 0;
      xmax = 84;
      mode = "electron";
    }
    ~ECAL_Calibration_Electron(void) {}

    std::string getDescription(void)
    {
      std::string description = "ECAL Module Calibration with electron: Gams, GamsRH, Shashlik, Mainz, Olga and Ecal0.";
      return description;
    }
};

class ECAL_Calibration_Gamma : ECAL_Calibration
{
  public:
    ECAL_Calibration_Gamma(void) {
      myName = "ECAL_Calibration_Gamma";
      pluginList::getInstance()->activated_classes.push_back(this);
      bins = 840 / 4;
      xmin = 0;
      xmax = 84;
      mode = "gamma";
    }
    ~ECAL_Calibration_Gamma(void) {}

    std::string getDescription(void)
    {
      std::string description = "ECAL Module Calibration with gammas: Gams, GamsRH, Shashlik, Mainz, Olga and Ecal0.";
      return description;
    }
};

static ECAL_Calibration_Electron* eCAL_Calibration_Electron = new ECAL_Calibration_Electron();
static ECAL_Calibration_Gamma* eCAL_Calibration_Gamma = new ECAL_Calibration_Gamma();
