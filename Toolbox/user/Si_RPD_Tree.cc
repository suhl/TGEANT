#include "toolbox.hh"
#include "TTree.h"

class Si_RPD_Tree : ToolboxPlugin
{
  public:
    Si_RPD_Tree(void);
    ~Si_RPD_Tree(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TTree* tree_good;
    TTree* tree_bad;
    
    bool photonDetected;
    bool isA;
    TVector3* posA;
    TVector3* momentumA;
    double timeA;
    double particleEnergyA;
    double betaA;
    double energyDepositA;
    double phiA;
    
    bool isB;
    TVector3* posB;
    TVector3* momentumB;
    double timeB;
    double particleEnergyB;
    double betaB;
    double energyDepositB;
    double phiB;
    
    TVector3* vertexPos;
    TVector3* vertexMomentum;
    double protonMomentum;
    double t_mandelstam;
    
    TVector3* protonVectorMeasured;
    double thetaMeasured;
    
    double weightDVCS;
    double weightBH;
    double weightSum;
};

static Si_RPD_Tree* si_RPD_Tree = new Si_RPD_Tree();

Si_RPD_Tree::Si_RPD_Tree(void)
{
  myName = "Si_RPD_Tree";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string Si_RPD_Tree::getDescription(void)
{
  std::string description = "Analysis user event for Si_RPD_Tree.";
  return description;
}

void Si_RPD_Tree::beginOfEvents(void)
{
  // place to initialize the histograms
  tree_good = new TTree("Si_RPD_Tree", "Si_RPD_Tree");
  tree_bad = new TTree("Si_RPD_Tree_noReco", "Si_RPD_Tree_noReco");

  posA = new TVector3();
  momentumA = new TVector3();
  posB = new TVector3();
  momentumB = new TVector3();
  vertexPos = new TVector3();
  vertexMomentum = new TVector3();
  protonVectorMeasured = new TVector3();
  
  TTree* tree = tree_good;
  for (int i = 0; i < 2; i++) {
    tree->Branch("photonDetected", &photonDetected);
    tree->Branch("isA", &isA);
    tree->Branch("posA", &posA);
    tree->Branch("momentumA", &momentumA);
    tree->Branch("timeA", &timeA);
    tree->Branch("particleEnergyA", &particleEnergyA);
    tree->Branch("betaA", &betaA);
    tree->Branch("energyDepositA", &energyDepositA);
    tree->Branch("phiA", &phiA);
    
    tree->Branch("isB", &isB);
    tree->Branch("posB", &posB);
    tree->Branch("momentumB", &momentumB);
    tree->Branch("timeB", &timeB);
    tree->Branch("particleEnergyB", &particleEnergyB);
    tree->Branch("betaB", &betaB);
    tree->Branch("energyDepositB", &energyDepositB);
    tree->Branch("phiB", &phiB);
    
    tree->Branch("vertexPos", &vertexPos);
    tree->Branch("vertexMomentum", &vertexMomentum);
    tree->Branch("protonMomentum", &protonMomentum);
    tree->Branch("t_mandelstam", &t_mandelstam);
    
    tree->Branch("protonVectorMeasured", &protonVectorMeasured);
    tree->Branch("thetaMeasured", &thetaMeasured);
    
    tree->Branch("weightDVCS", &weightDVCS);
    tree->Branch("weightBH", &weightBH);
    tree->Branch("weightSum", &weightSum);
    
    tree = tree_bad;
  }
}

bool Si_RPD_Tree::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated) 
  
  // kinematic cuts
  if (event->beamData.y <= 0.05 || event->beamData.y >= 0.9)
    return false;
  if (event->beamData.q2 <= 1 || event->beamData.q2 >= 20)
    return false;
  if (event->beamData.x_bj >= 0.27 || event->beamData.x_bj <= 0.005)
    return false;
  
  weightSum = event->beamData.uservar[2];
  weightDVCS = event->beamData.uservar[15];
  weightBH = event->beamData.uservar[16];
  
  // trajectory
  int photonTrackId = -1;
  int protonTrackId = -1;
  
  for (unsigned int i = 0; i < event->beamData.nTrajectories; i++) {
    if (event->beamData.trajectories.at(i).parentId <= 1 &&
        fabs(event->beamData.trajectories.at(i).position[0] - event->beamData.vertexPosition[0]) < 10E-3 &&
        fabs(event->beamData.trajectories.at(i).position[1] - event->beamData.vertexPosition[1]) < 10E-3 &&
        fabs(event->beamData.trajectories.at(i).position[2] - event->beamData.vertexPosition[2]) < 10E-3) {
      
      if (event->beamData.trajectories.at(i).particleId == 2212) {
	protonTrackId = event->beamData.trajectories.at(i).trackId;
      } else if (event->beamData.trajectories.at(i).particleId == 22) {
	photonTrackId = event->beamData.trajectories.at(i).trackId;
      }
    }
    if (protonTrackId != -1 && photonTrackId != -1)
      break;
  }
  
  if (protonTrackId == -1 || photonTrackId == -1) {
    cout << "ERROR! Proton/Photon trajectory not found, skip event!" << endl;
    return false;
  }
  
  // beamData
  T4BeamParticle* protonPart = NULL;
  for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
    T4BeamParticle* part = &event->beamData.beamParticles.at(i);
    
    // proton
    if (part->k[0] == 1 && part->k[1] == 2212) {
      if (protonPart == NULL)
        protonPart = part;
      else {
        cout << "ERROR! Found proton beamParticle twice, skip event!" << endl;
        return false;
      }
    }
  }
  
  if (protonPart == NULL) {
    cout << "ERROR! Proton or photon beamParticle not found, skip event!" << endl;
    return false;
  }
  
  vertexPos->SetXYZ(event->beamData.vertexPosition[0], event->beamData.vertexPosition[1], event->beamData.vertexPosition[2]);
  vertexMomentum->SetXYZ(protonPart->p[0], protonPart->p[1], protonPart->p[2]);
  protonMomentum = sqrt(pow(protonPart->p[0], 2)+pow(protonPart->p[1], 2)+pow(protonPart->p[2], 2));
  t_mandelstam = (pow(protonPart->p[3] - protonPart->p[4],2) - pow(protonMomentum,2)) * -1.;
  
  if (t_mandelstam >= 0.64) //0.06 < t_release < 0.64
    return false;

  
  T4HitData* hitA = NULL;
  T4HitData* hitB = NULL;
  photonDetected = false;
  for (unsigned int i = 0; i < event->tracking.size(); i++) {
    T4HitData* hit = &event->tracking.at(i);
    
    // proton
    if (hit->detectorId == 2222 && hit->particleId == 2212 && hit->trackId == protonTrackId) {
      if (hit->channelNo == 0 && hitA == NULL) {
	hitA = hit;
      } else if (hit->channelNo == 1 && hitB == NULL){
	hitB = hit;
      } else {
	cout << "something wrong! " << endl;
	return false;
      }
    }
    
    // photon
    if (hit->detectorId == 6666 && hit->particleId == 22 && hit->trackId == photonTrackId) {
      photonDetected = true;
    }
  }
  
//CAMERA:
//   for (unsigned int i = 0; i < event->tracking.size(); i++) {
//     if (event->tracking.at(i).detectorName[0] == 'C' && event->tracking.at(i).particleId == 2212 && event->tracking.at(i).trackId == protonTrackId) {
//       if (event->tracking.at(i).detectorId == 926 && e0 == -1) {
// 	e0 = event->tracking.at(i).energyDeposit;
//       } else if (event->tracking.at(i).detectorId == 927 && e1 == -1){
// 	e1 = event->tracking.at(i).energyDeposit;
//       } else {
// 	cout << "something wrong! " << event->tracking.at(i).channelNo << " " <<e0<<" " <<e1  << endl;
// 	return false;
//       }
//     }
//   }
  
  isA = (hitA != NULL);
  isB = (hitB != NULL);
  
  if (isA) {
    posA->SetXYZ(hitA->hitPosition[0], hitA->hitPosition[1], hitA->hitPosition[2]);
    momentumA->SetXYZ(hitA->momentum[0], hitA->momentum[1], hitA->momentum[2]);
    timeA = hitA->time;
    particleEnergyA = hitA->particleEnergy;
    betaA = hitA->beta;
    energyDepositA = hitA->energyDeposit;
    phiA = TVector3(hitA->hitPosition[0]-event->beamData.vertexPosition[0], 
		    hitA->hitPosition[1]-event->beamData.vertexPosition[1], 
		    hitA->hitPosition[2]-event->beamData.vertexPosition[2]).Phi()/M_PI*180.;
  }
  
  if (isB) {
    posB->SetXYZ(hitB->hitPosition[0], hitB->hitPosition[1], hitB->hitPosition[2]);
    momentumB->SetXYZ(hitB->momentum[0], hitB->momentum[1], hitB->momentum[2]);
    timeB = hitB->time;
    particleEnergyB = hitB->particleEnergy;
    betaB = hitB->beta;
    energyDepositB = hitB->energyDeposit;
    phiB = TVector3(hitB->hitPosition[0]-event->beamData.vertexPosition[0], 
		    hitB->hitPosition[1]-event->beamData.vertexPosition[1], 
		    hitB->hitPosition[2]-event->beamData.vertexPosition[2]).Phi()/M_PI*180.;
  }

  if (isA && isB) {
    protonVectorMeasured->SetXYZ(hitB->hitPosition[0]-hitA->hitPosition[0], 
				 hitB->hitPosition[1]-hitA->hitPosition[1], 
				 hitB->hitPosition[2]-hitA->hitPosition[2]);
    thetaMeasured = protonVectorMeasured->Theta()/M_PI*180.;
    tree_good->Fill();
  } else {
    protonVectorMeasured->SetXYZ(0, 0, 0);
    thetaMeasured = 0;
    tree_bad->Fill();
  }
  
  return true;
}

void Si_RPD_Tree::endOfEvents(void)
{
  tree_good->Write();
  tree_bad->Write();
}
