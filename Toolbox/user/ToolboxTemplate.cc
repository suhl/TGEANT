#include "toolbox.hh"

/*! \class ToolboxTemplate
 *  \brief A template for your own implementation
 *  You can just create a copy of this file, replace 'ToolboxTemplate' with your own classname and start programming!
 */
class ToolboxTemplate : ToolboxPlugin
{
  public:
    ToolboxTemplate(void);
    ~ToolboxTemplate(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
};

static ToolboxTemplate* toolboxTemplate = new ToolboxTemplate();

ToolboxTemplate::ToolboxTemplate(void)
{
  myName = "ToolboxTemplate";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string ToolboxTemplate::getDescription(void)
{
  std::string description = "Toolbox template to add own user functions.";
  return description;
}

void ToolboxTemplate::beginOfEvents(void)
{
  // place to initialize the histograms

}

bool ToolboxTemplate::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  return true;
}

void ToolboxTemplate::endOfEvents(void)
{
  // place to write the histograms

}
