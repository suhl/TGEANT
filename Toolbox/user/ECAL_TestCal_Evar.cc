#include "toolbox.hh"

class ECAL_TestCal_Evar : ToolboxPlugin
{
  public:
    ECAL_TestCal_Evar(void);
    ~ECAL_TestCal_Evar(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_energy_vs_size;
    TH1D* hist_clusterSize;
    TH1D* hist_energy;
    TH1D* hist_time;

    TH2D* hist_clustE_vs_genE;


    unsigned int eventCounter;
};

static ECAL_TestCal_Evar* eCAL_TestCal_Evar = new ECAL_TestCal_Evar();

ECAL_TestCal_Evar::ECAL_TestCal_Evar(void)
{
  myName = "ECAL_TestCal_Evar";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string ECAL_TestCal_Evar::getDescription(void)
{
  std::string description = "TestCal setup with 40GeV electon beam.";
  return description;
}

void ECAL_TestCal_Evar::beginOfEvents(void)
{
  // place to initialize the histograms
  hist_energy_vs_size = new TH2D("energy_vs_size","cluster energy vs size", 440, 0, 110, 500, 0, 500);
  hist_energy_vs_size->SetXTitle("cluster energy");
  hist_energy_vs_size->SetYTitle("cluster size");
  hist_clusterSize = new TH1D("cluster_size", "number of cells", 500, 0, 500);
  hist_energy = new TH1D("energy", "energy of cluster", 440, 0, 110);

  eventCounter = 0;
  hist_time = new TH1D("processing_time", "time for one event", 800, 0, 200);


  hist_clustE_vs_genE = new TH2D("clustE_vs_genE","cluster energy vs generator energy", 440, 0, 110, 440, 0, 110);
  hist_clustE_vs_genE->SetXTitle("cluster energy");
  hist_clustE_vs_genE->SetYTitle("generator energy");
}

bool ECAL_TestCal_Evar::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  eventCounter++;
  int counter = 0;

  double energy = 0;
  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    T4HitData* hit = &event->calorimeter.at(i);
    if (hit->energyDeposit < 200)
      continue;

    energy += hit->energyDeposit;
    counter++;
  }

  hist_clusterSize->Fill(counter);
  hist_energy_vs_size->Fill(energy/1000., counter);
  hist_clustE_vs_genE->Fill(energy/1000., event->beamData.trajectories.at(0).momentum[2]/1000.);

  hist_energy->Fill(energy/1000.);
  hist_time->Fill(event->processingTime);

  return true;
}

void ECAL_TestCal_Evar::endOfEvents(void)
{
  // place to write the histograms
  hist_energy_vs_size->Scale(1./eventCounter);
  hist_clustE_vs_genE->Scale(1./eventCounter);
  hist_clusterSize->Scale(1./eventCounter);
  hist_energy->Scale(1./eventCounter);
  hist_time->Scale(1./eventCounter);

  hist_energy_vs_size->Write();
  hist_clustE_vs_genE->Write();
  hist_clusterSize->Write();
  hist_energy->Write();
  hist_time->Write();

//  TH1D* profx = hist_hits->ProjectionX(); profx->Write();
//  TH1D* profy = hist_hits->ProjectionY(); profy->Write();
//
//  TH1D* hist_profileX = new TH1D("profile_x", "energy profile x", cells, 0, cells);
//  TH1D* hist_profileY = new TH1D("profile_y", "energy profile y", cells, 0, cells);
//
//  for (int i = 1; i <= cells; i++) {
//    hist_profileX->SetBinContent(i, hist_profileX->GetBinContent(i-1) + profx->GetBinContent(i));
//    hist_profileY->SetBinContent(i, hist_profileY->GetBinContent(i-1) + profy->GetBinContent(i));
//  }
//  hist_profileX->Write();
//  hist_profileY->Write();
}
