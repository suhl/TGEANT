#include "toolbox.hh"

class Cosmics_RingA : ToolboxPlugin
{
  public:
    Cosmics_RingA(void);
    ~Cosmics_RingA(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    unsigned int sumUp[7][2];
    unsigned int sumDn[7][2];
    unsigned int counter[7][2];
    unsigned int events[2];
};

static Cosmics_RingA* cosmics_RingA = new Cosmics_RingA();

Cosmics_RingA::Cosmics_RingA(void)
{
  myName = "Cosmics_RingA";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string Cosmics_RingA::getDescription(void)
{
  std::string description = "Toolbox user event for the analysis of the CAMERA Ring A test setup using cosmics and optical physics. ";
  description += "(Used by a bachelor student from Freiburg.)";
  return description;
}

void Cosmics_RingA::beginOfEvents(void)
{
  // place to initialize the histograms

}

bool Cosmics_RingA::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  int group = -1;
  int posZ = event->beamData.trajectories.at(0).position[2];

  int p = 0;
  if (event->beamData.beamParticles.at(0).k[1] == 11)
    p = 1;

  switch (posZ) {
    case -900:
      group = 0;
      break;
    case -600:
      group = 1;
      break;
    case -300:
      group = 2;
      break;
    case 0:
      group = 3;
      break;
    case 300:
      group = 4;
      break;
    case 600:
      group = 5;
      break;
    case 900:
      group = 6;
      break;
    default:
      break;
  }

  if (group == -1) {
    cout << "group error"<<endl;
    return false;
  }

  if (event->pmt.size() == 2) {

    for (int i=0; i < 2; i++) {
      if (event->pmt.at(i).detectorName == "CA01R1__u") {
        sumUp[group][p] += event->pmt.at(i).digits.at(0);
      } else {
        sumDn[group][p] += event->pmt.at(i).digits.at(0);
      }
    }
    counter[group][p]++;

  } else {
//    cout<<"keine photonen?!"<<endl;
  }

  events[p]++;
  return true;
}

void Cosmics_RingA::endOfEvents(void)
{
  // place to write the histograms

  cout << "RESULTS muon:" << endl;
  int sumM = 0;
  for (int i = 0; i < 7; i++) {
    cout << i << ": " << (double) sumUp[i][0] / counter[i][0] << "    " << (double) sumDn[i][0] / counter[i][0] << endl;
    sumM += counter[i][0];
  }
  cout << "Successful muons: "<< sumM << " / " << events[0] << endl;

  cout << "RESULTS electron:" << endl;
  int sumE = 0;
  for (int i = 0; i < 7; i++) {
    cout << i << ": " << (double) sumUp[i][1] / counter[i][1] << "    " << (double) sumDn[i][1] / counter[i][1] << endl;
    sumE += counter[i][1];
  }
  cout << "Successful electrons: "<< sumE << " / " << events[1] << endl;
}
