#include "toolbox.hh"

#include "TVector3.h"

class PolGPD_Analysis : ToolboxPlugin
{
public:
    PolGPD_Analysis(void);
    ~PolGPD_Analysis(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

private:
    // place to declare some histograms
    TH1D* protonEnergyVertex[2];
    TH1D* protonEnergyDetector[2];
    TH1D* protonEnergyDetector_both[2];

    TH1D* protonTVertex[2];
    TH1D* protonTDetector[2];
    TH1D* protonTDetector_both[2];

    TH1D* photonEnergyVertex;
    TH1D* photonEnergyDetector;
    TH1D* photonEnergyDetector_both;

    TH1D* zPositionVertex;
    TH2D* xyPositionVertex;

    TH2D* protonEnergy2D;
    TH2D* photonEnergy2D;

    TH1D* photonTheta;
    TH1D* photonThetaDetector;
    TH1D* photonThetaDetector_both;
    TH1D* photonZDetector;
    TH1D* photonZDetector_both;

    TH1D* protonPhi;
    TH1D* protonPhiDetector;
    TH1D* protonPhiDetector_both;
    
    TH1D* kine_qsq;
    TH1D* kine_xbj;
    TH1D* kine_y;

    TH1D* results;

    bool detectedProton;
    bool detectedPhoton;

    void makeRatioPlot(TH1* h1, TH1* h2, TH1* h3, string name, string title, int rebin = 1, bool setLogy = false, TH1* h1_forRatio = NULL, TH1* h2_forRatio = NULL, TH1* h3_forRatio = NULL);
    double calcX(TH1*, double value);
    double calcMean(TH1* ratio, TH1* h1, TH1* h2, double minimum = 0);
};

static PolGPD_Analysis* polGPD_Analysis = new PolGPD_Analysis();

PolGPD_Analysis::PolGPD_Analysis(void)
{
  myName = "PolGPD_Analysis";
  pluginList::getInstance()->activated_classes.push_back(this);
}

void PolGPD_Analysis::beginOfEvents(void)
{
  // place to initialize the histograms

  protonEnergyVertex[0] = new TH1D("protonEnergyVertex[0]", "proton energy vertex", 500, 0., 1.5);
  protonEnergyVertex[1] = new TH1D("protonEnergyVertex[1]", "proton energy vertex", 500, -5, 0.176);
  protonEnergyVertex[0]->SetXTitle("proton momentum [GeV/c]");
  protonEnergyVertex[1]->SetXTitle("proton momentum [GeV/c]");
  binLogX(protonEnergyVertex[1]);

  protonEnergyDetector[0] = new TH1D("protonEnergyDetector[0]", "proton energy detector", 500, 0., 1.5);
  protonEnergyDetector[1] = new TH1D("protonEnergyDetector[1]", "proton energy detector", 500, -5, 0.176);
  protonEnergyDetector[0]->SetXTitle("proton momentum [GeV/c]");
  protonEnergyDetector[1]->SetXTitle("proton momentum [GeV/c]");
  binLogX(protonEnergyDetector[1]);

  protonEnergyDetector_both[0] = new TH1D("protonEnergyDetector_both[0]", "proton energy detector (#gamma detected)", 500, 0., 1.5);
  protonEnergyDetector_both[1] = new TH1D("protonEnergyDetector_both[1]", "proton energy detector (#gamma detected)", 500, -5, 0.176);
  protonEnergyDetector_both[0]->SetXTitle("proton momentum [GeV/c]");
  protonEnergyDetector_both[1]->SetXTitle("proton momentum [GeV/c]");
  binLogX(protonEnergyDetector_both[1]);


  protonTVertex[0] = new TH1D("protonTVertex[0]", "proton -t vertex", 500, 0., 1.);
  protonTVertex[1] = new TH1D("protonTVertex[1]", "proton -t vertex", 500, -5, 0);
  protonTVertex[0]->SetXTitle("-t [(GeV/c)^{2}]");
  protonTVertex[1]->SetXTitle("-t [(GeV/c)^{2}]");
  binLogX(protonTVertex[1]);

  protonTDetector[0] = new TH1D("protonTDetector[0]", "proton -t detector", 500, 0., 1.);
  protonTDetector[1] = new TH1D("protonTDetector[1]", "proton -t detector", 500, -5, 0);
  protonTDetector[0]->SetXTitle("-t [(GeV/c)^{2}]");
  protonTDetector[1]->SetXTitle("-t [(GeV/c)^{2}]");
  binLogX(protonTDetector[1]);

  protonTDetector_both[0] = new TH1D("protonTDetector_both[0]", "proton -t detector (#gamma detected)", 500, 0., 1.);
  protonTDetector_both[1] = new TH1D("protonTDetector_both[1]", "proton -t detector (#gamma detected)", 500, -5, 0);
  protonTDetector_both[0]->SetXTitle("-t [(GeV/c)^{2}]");
  protonTDetector_both[1]->SetXTitle("-t [(GeV/c)^{2}]");
  binLogX(protonTDetector_both[1]);


  photonEnergyVertex = new TH1D("photonEnergyVertex", "#gamma energy vertex", 100, 0, 180);
  photonEnergyVertex->SetXTitle("#gamma energy [GeV]");
  photonEnergyDetector = new TH1D("photonEnergyDetector", "#gamma energy detector", 100, 0, 180);
  photonEnergyDetector->SetXTitle("#gamma energy [GeV]");
  photonEnergyDetector_both = new TH1D("photonEnergyDetector_both", "#gamma energy detector (proton detected)", 100, 0, 180);
  photonEnergyDetector_both->SetXTitle("#gamma energy [GeV]");


  zPositionVertex = new TH1D("zPositionVertex", "z position vertex", 100, -700, 700);
  zPositionVertex->SetXTitle("z position vertex [mm]");
  xyPositionVertex = new TH2D("xyPositionVertex", "x/y position vertex", 600, -30, 30, 600, -30, 30);
  xyPositionVertex->SetXTitle("x position vertex [mm]");
  xyPositionVertex->SetYTitle("y position vertex [mm]");


  protonEnergy2D = new TH2D("protonEnergy2D", "proton momentum generator vs. detector", 600, 0., 1.2, 600, 0., 1.2);
  protonEnergy2D->SetXTitle("proton momentum generator [GeV/c]");
  protonEnergy2D->SetYTitle("proton momentum detector [GeV/c]");

  photonEnergy2D = new TH2D("photonEnergy2D", "#gamma energy generator vs. detector", 1000, 0, 200, 1000, 0, 200);
  photonEnergy2D->SetXTitle("#gamma energy generator [GeV]");
  photonEnergy2D->SetYTitle("#gamma energy detector [GeV]");

  photonTheta = new TH1D("photonTheta", "#gamma #theta angle", 100, 0, 0.300);
  photonTheta->SetXTitle("#theta [rad]");
  photonThetaDetector = new TH1D("photonThetaDetector", "#gamma #theta angle (detected)", 100, 0, 0.300);
  photonThetaDetector->SetXTitle("#theta [rad]");
  photonThetaDetector_both = new TH1D("photonThetaDetector_both", "#gamma #theta angle (detected w/ proton)", 100, 0, 0.300);
  photonThetaDetector_both->SetXTitle("#theta [rad]");
  photonZDetector = new TH1D("photonZDetector", "#gamma z position (detected)", 100, -700, 700);
  photonZDetector->SetXTitle("z position [mm]");
  photonZDetector_both = new TH1D("photonZDetector_both", "#gamma z position (detected w/ proton)", 100, -700, 700);
  photonZDetector_both->SetXTitle("z position [mm]");

  protonPhi = new TH1D("protonPhi", "proton #phi angle", 60, -M_PI, M_PI);
  protonPhi->SetXTitle("#phi [rad]");
  protonPhiDetector = new TH1D("protonPhiDetector", "proton #phi angle (detected)", 60, -M_PI, M_PI);
  protonPhiDetector->SetXTitle("#phi [rad]");
  protonPhiDetector_both = new TH1D("protonPhiDetector_both", "proton #phi angle (detected w/ #gamma)", 60, -M_PI, M_PI);
  protonPhiDetector_both->SetXTitle("#phi [rad]");
  
  kine_qsq = new TH1D("kine_qsq", "Q^{2}", 202, 0.8, 25.);
  kine_qsq->SetXTitle("Q^{2} [(GeV/c)^{2}]");
  binLogX_Special(kine_qsq, 1., 20.);
  
  kine_xbj = new TH1D("kine_xbj", "x_{Bj}", 102, 0.001, 1.);
  kine_xbj->SetXTitle("x_{Bj}");
  binLogX_Special(kine_xbj, 0.005, 0.27);
  
  kine_y = new TH1D("kine_y", "y", 100, 0, 1);
  kine_y->SetXTitle("y");
  
  
  results = new TH1D("results", "results(1-39) and setup(40-69)", 70, 0, 70);
  results->GetXaxis()->SetLabelSize(0.04);

  // results
  results->GetXaxis()->SetBinLabel(1, "photon mean prob. (w/o proton)");
  results->GetXaxis()->SetBinLabel(2, "proton mean prob. (w/o photon)");
  results->GetXaxis()->SetBinLabel(3, "photon & proton mean prob.");

  results->GetXaxis()->SetBinLabel(4, "E_{proton} prob > 10% (w/o photon)");
  results->GetXaxis()->SetBinLabel(5, "E_{proton} prob > 20% (w/o photon)");
  results->GetXaxis()->SetBinLabel(6, "E_{proton} prob > 30% (w/o photon)");
  results->GetXaxis()->SetBinLabel(7, "E_{proton} prob > 40% (w/o photon)");

  results->GetXaxis()->SetBinLabel(8, "E_{proton} prob > 10% (w/ photon)");
  results->GetXaxis()->SetBinLabel(9, "E_{proton} prob > 20% (w/ photon)");
  results->GetXaxis()->SetBinLabel(10, "E_{proton} prob > 30% (w/ photon)");
  results->GetXaxis()->SetBinLabel(11, "E_{proton} prob > 40% (w/ photon)");

  results->GetXaxis()->SetBinLabel(12, "t_min");
  results->GetXaxis()->SetBinLabel(13, "t_min mean proton & photon prob.");

  // setup
  // target
  results->GetXaxis()->SetBinLabel(40, "target_cryostat_radius");
  results->GetXaxis()->SetBinLabel(41, "target_cryostat_length");
  results->GetXaxis()->SetBinLabel(42, "target_cryostat_thickness");
  results->GetXaxis()->SetBinLabel(43, "target_aluminium_radius");
  results->GetXaxis()->SetBinLabel(44, "target_aluminium_length");
  results->GetXaxis()->SetBinLabel(45, "target_aluminium_thickness");
  results->GetXaxis()->SetBinLabel(46, "target_coils_radius");
  results->GetXaxis()->SetBinLabel(47, "target_coils_length");
  results->GetXaxis()->SetBinLabel(48, "target_coils_thickness");
  results->GetXaxis()->SetBinLabel(49, "target_mylar_window_thickness");
  results->GetXaxis()->SetBinLabel(50, "target_length");
  results->GetXaxis()->SetBinLabel(51, "target_lhe_radius");
  results->GetXaxis()->SetBinLabel(52, "target_nh3_radius");
  results->GetXaxis()->SetBinLabel(53, "target_fiber_thickness");
  results->GetXaxis()->SetBinLabel(54, "target_kevlar_thickness");
  // micromegas
  results->GetXaxis()->SetBinLabel(55, "mm_length");
  results->GetXaxis()->SetBinLabel(56, "mm_radius");
  results->GetXaxis()->SetBinLabel(57, "mm_layer_thickness");
  results->GetXaxis()->SetBinLabel(58, "mm_layer_distance");
  results->GetXaxis()->SetBinLabel(59, "mm_zPosOffset");
  // ring B
  results->GetXaxis()->SetBinLabel(60, "ringB_length");
  results->GetXaxis()->SetBinLabel(61, "ringB_radius");
  results->GetXaxis()->SetBinLabel(62, "ringB_zPosOffset");
//  results->GetXaxis()->SetBinLabel(63, "");
//  results->GetXaxis()->SetBinLabel(64, "");
//  results->GetXaxis()->SetBinLabel(65, "");
//  results->GetXaxis()->SetBinLabel(66, "");
//  results->GetXaxis()->SetBinLabel(67, "");
//  results->GetXaxis()->SetBinLabel(68, "");
//  results->GetXaxis()->SetBinLabel(69, "");
}

std::string PolGPD_Analysis::getDescription(void)
{
  std::string description = "PolGPD_Analysis user event.";
  return description;
}

bool PolGPD_Analysis::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  static bool first(true);
  // save setup
  if (first) {
    for (int i = 0; i < 30; i++)
      results->SetBinContent(40 + i, event->beamData.parl[i]);
    first = false;
  }

  // kinematic cuts
  if (event->beamData.y <= 0.05 || event->beamData.y >= 0.9)
    return false;
  if (event->beamData.q2 <= 1 || event->beamData.q2 >= 20)
    return false;
  if (event->beamData.x_bj >= 0.27 || event->beamData.x_bj <= 0.005)
    return false;

  T4BeamParticle* photonPart = NULL;
  T4BeamParticle* protonPart = NULL;
  detectedProton = false;
  detectedPhoton = false;

  double weight = event->beamData.uservar[15];
  if (weight < 0) {
    cout << "ERROR! Negative weight, skip event!" << endl;
    return false;
  }

  for (unsigned int i = 0; i < event->beamData.beamParticles.size(); i++) {
    T4BeamParticle* part = &event->beamData.beamParticles.at(i);

    // photon
    if (part->k[0] == 1 && part->k[1] == 22) {
      if (photonPart == NULL)
        photonPart = part;
      else {
        cout << "ERROR! Found photon beamParticle twice, skip event!" << endl;
        return false;
      }
    }

    // proton
    if (part->k[0] == 1 && part->k[1] == 2212) {
      if (protonPart == NULL)
        protonPart = part;
      else {
        cout << "ERROR! Found proton beamParticle twice, skip event!" << endl;
        return false;
      }
    }
  }

  if (photonPart == NULL || protonPart == NULL) {
    cout << "ERROR! Proton or photon beamParticle not found, skip event!" << endl;
    return false;
  }

  double protonMomentum = sqrt(pow(protonPart->p[0], 2)+pow(protonPart->p[1], 2)+pow(protonPart->p[2], 2));
  double t_mandelstam = pow(protonPart->p[3] - protonPart->p[4],2) - pow(protonMomentum,2);
  t_mandelstam *= -1;

  if (t_mandelstam >= 0.64) //0.06 < t_release < 0.64
    return false;
  

//cout<<"t="<<t_mandelstam<<endl;
  protonEnergyVertex[0]->Fill(protonMomentum, weight);
  protonEnergyVertex[1]->Fill(protonMomentum, weight);
  protonTVertex[0]->Fill(t_mandelstam, weight);
  protonTVertex[1]->Fill(t_mandelstam, weight);
  photonEnergyVertex->Fill(photonPart->p[3], weight);
  zPositionVertex->Fill(event->beamData.vertexPosition[2], weight);
  xyPositionVertex->Fill(event->beamData.vertexPosition[0], event->beamData.vertexPosition[1], weight);

  TVector3 photonMomentum = TVector3(photonPart->p[0], photonPart->p[1], photonPart->p[2]);
  photonTheta->Fill(photonMomentum.Theta(), weight);

  TVector3 protonMomentumVec = TVector3(protonPart->p[0], protonPart->p[1], protonPart->p[2]);
  protonPhi->Fill(photonMomentum.Phi(), weight);

  int photonTrackId = 0;
  int protonTrackId = 0;

  for (unsigned int i = 0; i < event->beamData.trajectories.size(); i++) {
    T4Trajectory* tra = &event->beamData.trajectories.at(i);

    // photon
    if (tra->parentId == 1 && tra->particleId == 22) {
      if (photonTrackId == 0)
        photonTrackId = tra->trackId;
      else {// we dont need this: the first found particle is from the prim vertex...
//        cout << "ERROR! Found photon trajectory twice, skip event!" << endl;
//        return false;
      }
    }

    // proton
    if (tra->parentId == 1 && tra->particleId == 2212) {
      if (protonTrackId == 0)
        protonTrackId = tra->trackId;
      else {
//        cout << "ERROR! Found proton trajectory twice, skip event!" << endl;
//        return false;
      }
    }
  }

  if (photonTrackId == 0 || protonTrackId == 0) {
    cout << "ERROR! Proton or photon trajectory not found, skip event!" << endl;
    return false;
  }


  for (unsigned int i = 0; i < event->tracking.size(); i++) {
    T4HitData* hit = &event->tracking.at(i);

    // photon
    if (hit->detectorId == 6666 && hit->particleId == 22) {
      if (hit->trackId == photonTrackId) {
        detectedPhoton = true;
        photonEnergyDetector->Fill(photonPart->p[3], weight);
        photonEnergy2D->Fill(photonPart->p[3], hit->particleEnergy / 1000., weight);
        photonThetaDetector->Fill(photonMomentum.Theta(), weight);
        photonZDetector->Fill(event->beamData.vertexPosition[2], weight);
      }
    }

    // proton
//     if (hit->detectorId == 929 && hit->particleId == 2212) {
    if ((hit->detectorId == 927 ||(hit->detectorId == 2222 && hit->channelNo == 1)) && hit->particleId == 2212) {
      if (hit->trackId == protonTrackId) {
        detectedProton = true;
        protonEnergyDetector[0]->Fill(protonMomentum, weight);
        protonEnergyDetector[1]->Fill(protonMomentum, weight);
        protonTDetector[0]->Fill(t_mandelstam, weight);
        protonTDetector[1]->Fill(t_mandelstam, weight);
        protonEnergy2D->Fill(protonMomentum, sqrt(pow(hit->particleEnergy,2) - pow(938.27,2)) / 1000., weight);
        protonPhiDetector->Fill(photonMomentum.Phi(), weight);
      }
    }
  }

  if (detectedPhoton && detectedProton) {
    photonEnergyDetector_both->Fill(photonPart->p[3], weight);
    protonEnergyDetector_both[0]->Fill(protonMomentum, weight);
    protonEnergyDetector_both[1]->Fill(protonMomentum, weight);
    protonTDetector_both[0]->Fill(t_mandelstam, weight);
    protonTDetector_both[1]->Fill(t_mandelstam, weight);
    photonThetaDetector_both->Fill(photonMomentum.Theta(), weight);
    photonZDetector_both->Fill(event->beamData.vertexPosition[2], weight);
    protonPhiDetector_both->Fill(photonMomentum.Phi(), weight);
    
    kine_qsq->Fill(event->beamData.q2, weight);
    kine_xbj->Fill(event->beamData.x_bj, weight);
    kine_y->Fill(event->beamData.y, weight);
  }

  return true;
}

void PolGPD_Analysis::endOfEvents(void)
{
  // place to save the histograms
  protonEnergyVertex[0]->Write();
  protonEnergyDetector[0]->Write();
  protonEnergyDetector_both[0]->Write();
  protonEnergyVertex[1]->Write();
  protonEnergyDetector[1]->Write();
  protonEnergyDetector_both[1]->Write();

  protonTVertex[0]->Write();
  protonTDetector[0]->Write();
  protonTDetector_both[0]->Write();
  protonTVertex[1]->Write();
  protonTDetector[1]->Write();
  protonTDetector_both[1]->Write();

  photonEnergyVertex->Write();
  photonEnergyDetector->Write();
  photonEnergyDetector_both->Write();

  zPositionVertex->Write();
  xyPositionVertex->Write();

  protonEnergy2D->Write();
  photonEnergy2D->Write();

  photonTheta->Write();
  photonThetaDetector->Write();
  photonThetaDetector_both->Write();
  photonZDetector->Write();
  photonZDetector_both->Write();

  protonPhi->Write();
  protonPhiDetector->Write();
  protonPhiDetector_both->Write();
  
  kine_qsq->Write();
  kine_xbj->Write();
  kine_y->Write();

  makeRatioPlot(photonEnergyVertex, photonEnergyDetector, photonEnergyDetector_both, "photon_detection_probability", "#gamma detection probability", 2);
  makeRatioPlot(protonEnergyVertex[0], protonEnergyDetector[0], protonEnergyDetector_both[0], "proton_detection_probability", "proton detection probability", 2, true, protonEnergyVertex[1], protonEnergyDetector[1], protonEnergyDetector_both[1]);
  makeRatioPlot(protonTVertex[0], protonTDetector[0], protonTDetector_both[0], "proton_detection_probability_t", "proton detection probability (t)", 2, true, protonTVertex[1], protonTDetector[1], protonTDetector_both[1]);


  makeRatioPlot(zPositionVertex, photonZDetector, photonZDetector_both, "photon_zEff", "#gamma z eff.", 4);
  makeRatioPlot(photonTheta, photonThetaDetector, photonThetaDetector_both, "photon_ThetaEff", "photon #theta eff.", 4, true);
  makeRatioPlot(photonTheta, photonThetaDetector, NULL, "photon_ThetaEff_single", "photon #theta eff.", 4, true);
  makeRatioPlot(protonPhi, protonPhiDetector, protonPhiDetector_both, "proton_PhiEff", "proton #phi eff.", 2);

  results->Write();
}

void PolGPD_Analysis::makeRatioPlot(TH1* h1, TH1* h2, TH1* h3, string name, string title, int rebin, bool setLogy, TH1* h1_forRatio, TH1* h2_forRatio, TH1* h3_forRatio)
{
  if (h1_forRatio == NULL)
    h1_forRatio = h1;
  if (h2_forRatio == NULL)
    h2_forRatio = h2;
  if (h3_forRatio == NULL)
    h3_forRatio = h3;

//  TCanvas* canvas1 = new TCanvas(name.c_str(), title.c_str(), 1200, 800 * 2);
  TCanvas* canvas1 = new TCanvas(name.c_str(), title.c_str(), 1000, 1200);
  TPad* pad1 = new TPad("pad1", "pad1", 0, 0.5, 1, 1);
  pad1->SetBottomMargin(0.02);
  pad1->Draw();

  TPad* pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.5);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.15);
  pad2->Draw();

  pad1->SetGridx();
  pad1->SetGridy();
  pad2->SetGridx();
  pad2->SetGridy();

  pad1->cd();
  if (setLogy)
    pad1->SetLogy();
  h1->SetTitle(title.c_str());
  h1->SetLineWidth(5);
  h1_forRatio->SetLineWidth(5);
  if (!setLogy)
    h1->GetYaxis()->SetRangeUser(0., 1.1 * h1->GetMaximum());
  h1->Draw("");
  h2->SetFillColor(2);
  h2->SetFillStyle(3004);
  h2->SetLineColor(2);
  h2->SetLineWidth(5);
  h2_forRatio->SetLineColor(2);
  h2_forRatio->SetLineWidth(5);
  h2->Draw("same");
  if (h3 != NULL) {
    h3->SetFillColor(4);
    h3->SetFillStyle(3005);
    h3->SetLineColor(4);
    h3->SetLineWidth(5);
    h3_forRatio->SetLineColor(4);
    h3_forRatio->SetLineWidth(5);
    h3->Draw("same");
  }

  h1->GetXaxis()->SetLabelSize(0.0);
  h1->SetStats(0);

  TLegend leg(0.65,0.70,0.90,0.90);
  leg.AddEntry(h1, h1->GetTitle(), "f");
  leg.AddEntry(h2, h2->GetTitle(), "f");
  if (h3 != NULL) leg.AddEntry(h3, h3->GetTitle(), "f");
//  leg.SetFillStyle(0);
  leg.Draw("same");

  pad2->cd();

  TH1D* ratio1 = (TH1D*) h2_forRatio->Clone();
  ratio1->Rebin(rebin);
  TH1D* h2Clone = (TH1D*) h1_forRatio->Clone();
  h2Clone->Rebin(rebin);
  ratio1->Sumw2();
  h2Clone->Sumw2();
  ratio1->SetMarkerColor(2);
  ratio1->SetMarkerStyle(21);
  ratio1->SetStats(0);
  ratio1->Divide(h2Clone);
  ratio1->GetYaxis()->SetRangeUser(0, 1);
//  ratio1->GetXaxis()->SetTitle("energy generator [GeV]");
  ratio1->SetTitle("");
  ratio1->GetXaxis()->SetTitleOffset(1);
  ratio1->Draw("Y");

  TH1D* ratio2;
  if (h3 != NULL) {
    ratio2 = (TH1D*) h3_forRatio->Clone();
    ratio2->Rebin(rebin);
    TH1D* h3Clone = (TH1D*) h1_forRatio->Clone();
    h3Clone->Rebin(rebin);
    ratio2->Sumw2();
    h3Clone->Sumw2();
    ratio2->SetMarkerColor(4);
    ratio2->SetMarkerStyle(21);
    ratio2->Divide(h3Clone);
    ratio2->Draw("same");

    TLegend leg2(0.70,0.85,0.90,1.00);
    leg2.AddEntry(ratio1, "single", "p");
    leg2.AddEntry(ratio2, "combined", "p");
  //  leg2.SetFillStyle(0);
    leg2.Draw("same");
  }

  if (name == "proton_detection_probability" && h3 != NULL) {
    results->SetBinContent(4, calcX(ratio1, 0.10));
    results->SetBinContent(5, calcX(ratio1, 0.20));
    results->SetBinContent(6, calcX(ratio1, 0.30));
    results->SetBinContent(7, calcX(ratio1, 0.40));
    results->SetBinContent(2, calcMean(ratio1, h1_forRatio, h2_forRatio));

    results->SetBinContent(8, calcX(ratio2, 0.10));
    results->SetBinContent(9, calcX(ratio2, 0.20));
    results->SetBinContent(10, calcX(ratio2, 0.30));
    results->SetBinContent(11, calcX(ratio2, 0.40));

    double meanBoth = calcMean(ratio2, h1_forRatio, h3_forRatio);
    if (results->GetBinContent(3) == 0)
      results->SetBinContent(3, meanBoth);
    else if (results->GetBinContent(3) != meanBoth)
      cout << "TOOLBOX ERROR PolGPD_Analysis: meanBoth_proton != meanBoth_photon!!"
          << meanBoth << " vs. " << results->GetBinContent(3) <<endl;

  } else if (name == "proton_detection_probability_t" && h3 != NULL) {
    double minimum = calcX(ratio2, 0.20);
    results->SetBinContent(12, minimum);
    double tmin_mean = calcMean(ratio2, h1_forRatio, h3_forRatio, minimum);
    results->SetBinContent(13, tmin_mean);

    calcMean(ratio1, h1_forRatio, h2_forRatio);
    calcMean(ratio2, h1_forRatio, h3_forRatio);

    cout<<"tmin = "<< minimum << "  prob = "<< tmin_mean<<endl;
  } else if (name == "photon_detection_probability" && h3 != NULL) {
    results->SetBinContent(1, calcMean(ratio1, h1_forRatio, h2_forRatio));

    double meanBoth = calcMean(ratio2, h1_forRatio, h3_forRatio);
    if (results->GetBinContent(3) == 0)
      results->SetBinContent(3, meanBoth);
    else if (results->GetBinContent(3) != meanBoth)
      cout << "TOOLBOX ERROR PolGPD_Analysis: meanBoth_photon != meanBoth_proton!!"
          << meanBoth << " vs. " << results->GetBinContent(3) << endl;
  } else if (name == "photon_ThetaEff" || name == "photon_ThetaEff_single") {
    calcMean(ratio1, h1_forRatio, h2_forRatio);
    if(h3 != NULL) calcMean(ratio2, h1_forRatio, h3_forRatio);
  }

  canvas1->Write();
  string name222 = (string) canvas1->GetName() + ".png";
  canvas1->SaveAs((name222).c_str());
  delete canvas1;
}

double PolGPD_Analysis::calcX(TH1* h, double value)
{
  int bin(-1);
  for (int i = 1; i <= h->GetNbinsX()+1; i++) {
    if (h->GetBinContent(i) > value) {
      bin = i;
      break;
    }
  }

  if (bin < 1)
    return 0;

  double x1 = h->GetBinCenter(bin-1);
  double x2 = h->GetBinCenter(bin);
  double y1 = h->GetBinContent(bin-1);
  double y2 = h->GetBinContent(bin);

  double ratio = (value - y1) / (y2 - y1);
  return (x1 + ratio * (x2 - x1));
}

double PolGPD_Analysis::calcMean(TH1* ratio, TH1* h1, TH1* h2, double minimum)
{
  int startBin(1);

  if (minimum != 0) {
    for (int i = startBin; i <= h1->GetNbinsX()+1; i++) {
      if (h1->GetBinCenter(i) >= minimum) {
        startBin = i;
        break;
      }
    }
  }

  double int1(0), int2(0);
  for (int i = startBin; i <= h1->GetNbinsX()+1; i++) {
    int1 += h1->GetBinContent(i);
    int2 += h2->GetBinContent(i);
  }

  double mean = int2 / int1;

  if (startBin == 1)
    minimum = h1->GetXaxis()->GetBinLowEdge(1);

  TLine* line = new TLine(minimum, mean, ratio->GetXaxis()->GetBinUpEdge(ratio->GetNbinsX()), mean);
  if (startBin == 1)
    line->SetLineColor(ratio->GetMarkerColor());
  else
    line->SetLineColor(1);
  line->SetLineWidth(4);
  line->Draw("same");

  if (startBin != 1) {
    TLine* line2 = new TLine(minimum, 0, minimum, mean);
    line2->SetLineColor(1);
    line2->SetLineWidth(4);
    line2->Draw("same");
  }

  return mean;
}
