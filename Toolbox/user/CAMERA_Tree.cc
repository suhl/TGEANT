#include "toolbox.hh"
#include "TTree.h"

class CAMERA_Tree : ToolboxPlugin
{
  public:
    CAMERA_Tree(void);
    ~CAMERA_Tree(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    TTree* tree;
    TVector3* posA;
    TVector3* posB;
    double timeA;
    double timeB;
    double energyA;
    double energyB;
};

static CAMERA_Tree* camera_Tree = new CAMERA_Tree();

CAMERA_Tree::CAMERA_Tree(void)
{
  myName = "CAMERA_Tree";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string CAMERA_Tree::getDescription(void)
{
  std::string description = "Fills a ROOT tree with CAMERA information from the proton track.";
  return description;
}

void CAMERA_Tree::beginOfEvents(void)
{
  // place to initialize the histograms
  tree = new TTree("CAMERA", "CAMERA");
  posA = new TVector3();
  posB = new TVector3();
  tree->Branch("posA", &posA);
  tree->Branch("posB", &posB);
  tree->Branch("timeA", &timeA);
  tree->Branch("timeB", &timeB);
  tree->Branch("energyA", &energyA);
  tree->Branch("energyB", &energyB);
}

bool CAMERA_Tree::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)

  if (event->beamData.beamParticles.size() != 6*2)
    return false;

  // search proton track:
  bool proton_found = false;
  int proton_track;
  for (unsigned int i = 0; i < event->beamData.nTrajectories; i++) {
    T4Trajectory* track = &event->beamData.trajectories.at(i);
    if (track->parentId == 1 && track->particleId == 2212) {
      proton_track = track->trackId;
      proton_found = true;
    }
  }
  if (!proton_found)
    return false;


  timeA = 0.;
  timeB = 0.;
  energyA = 0.;
  energyB = 0.;
  posA->SetXYZ(0,0,0);
  posB->SetXYZ(0,0,0);

  for (unsigned int i = 0; i < event->tracking.size(); i++) {
    T4HitData* hit = &event->tracking.at(i);
    if (hit->detectorId == 926 && hit->particleId == 2212 && hit->trackId == proton_track) { //RingA
      timeA = hit->time;
      energyA = hit->energyDeposit;
      posA->SetXYZ(hit->primaryHitPosition[0], hit->primaryHitPosition[1], hit->primaryHitPosition[2]);
    } else if (hit->detectorId == 927 && hit->particleId == 2212 && hit->trackId == proton_track && hit->channelNo == 0) { //RingB
      timeB = hit->time;
      energyB = hit->energyDeposit;
      posB->SetXYZ(hit->primaryHitPosition[0], hit->primaryHitPosition[1], hit->primaryHitPosition[2]);
    }
  }

  tree->Fill();

  return true;
}

void CAMERA_Tree::endOfEvents(void)
{
  // place to write the histograms
  tree->Write();
}
