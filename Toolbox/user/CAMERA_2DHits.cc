#include "toolbox.hh"

class CAMERA_2DHits : ToolboxPlugin
{
  public:
    CAMERA_2DHits(void);
    ~CAMERA_2DHits(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    TH2D* cameraHistALL;
    TH2D* cameraHistProton;
    TH2D* cameraHistElectron;
};

static CAMERA_2DHits* camera_2DHits = new CAMERA_2DHits();

CAMERA_2DHits::CAMERA_2DHits(void)
{
  myName = "CAMERA_2DHits";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string CAMERA_2DHits::getDescription(void)
{
  std::string description = "This is an example for the usage of the toolbox.\n";
  description += "We are plotting a 2D hit map for CAMERA RingA and RingB.";
  return description;
}

void CAMERA_2DHits::beginOfEvents(void)
{
  // place to initialize the histograms
  cameraHistALL = new TH2D("cameraHistALL",
      "ALL: X and Y Hit position in CAMERA", 2600, -130, 130, 2600, -130, 130);
  cameraHistALL->GetXaxis()->SetTitle("X hit position [cm]");
  cameraHistALL->GetYaxis()->SetTitle("Y hit position [cm]");
  cameraHistProton = new TH2D("cameraHistProton",
      "PROTON: X and Y Hit position in CAMERA", 2600, -130, 130, 2600, -130,
      130);
  cameraHistProton->GetXaxis()->SetTitle("X hit position [cm]");
  cameraHistProton->GetYaxis()->SetTitle("Y hit position [cm]");
  cameraHistElectron = new TH2D("cameraHistElectron",
      "ELECTRON: X and Y Hit position in CAMERA", 2600, -130, 130, 2600, -130,
      130);
  cameraHistElectron->GetXaxis()->SetTitle("X hit position [cm]");
  cameraHistElectron->GetYaxis()->SetTitle("Y hit position [cm]");
}

bool CAMERA_2DHits::processEvent(T4Event* event)
{
  for (unsigned int eventNo = 0; eventNo < event->tracking.size(); eventNo++) {
    T4HitData* hit = &event->tracking.at(eventNo);

    if (hit->detectorName[0] != 'C')
      continue;

    cameraHistALL->Fill(hit->primaryHitPosition[0] / 10.,
        hit->primaryHitPosition[1] / 10.);
    if (hit->particleId == 2212)
      cameraHistProton->Fill(hit->primaryHitPosition[0] / 10.,
          hit->primaryHitPosition[1] / 10.);
    else if (hit->particleId == 11)
      cameraHistElectron->Fill(hit->primaryHitPosition[0] / 10.,
          hit->primaryHitPosition[1] / 10.);
  }

  return true;
}

void CAMERA_2DHits::endOfEvents(void)
{
  cameraHistALL->Write();
  cameraHistProton->Write();
  cameraHistElectron->Write();
}
