#include "toolbox.hh"

class ECAL_TestCal_EnergyEffic : ToolboxPlugin
{
  public:
    ECAL_TestCal_EnergyEffic(void);
    ~ECAL_TestCal_EnergyEffic(void) {}

    std::string getDescription(void);
    bool processEvent(T4Event* event);
    void endOfEvents(void);
    void beginOfEvents(void);

  private:
    // place to declare some histograms
    TH2D* hist_energy_cell;
    TH2D* hist_energy_hull;
    TH2D* hist_energy_world;
    TH2D* hist_energy_back;
    
    TH2D* hist_energy_sum;
    TH2D* hist_energy_missing;
    
    int cells;
};

static ECAL_TestCal_EnergyEffic* eCAL_TestCal_EnergyEffic = new ECAL_TestCal_EnergyEffic();

ECAL_TestCal_EnergyEffic::ECAL_TestCal_EnergyEffic(void)
{
  myName = "ECAL_TestCal_EnergyEffic";
  pluginList::getInstance()->activated_classes.push_back(this);
}

std::string ECAL_TestCal_EnergyEffic::getDescription(void)
{
  std::string description = "TestCal setup to check for the energy within the cells and in their outer hull.";
  return description;
}

void ECAL_TestCal_EnergyEffic::beginOfEvents(void)
{
  cells = 11;

  int bins = 400;
  // place to initialize the histograms
  hist_energy_cell = new TH2D("energy_cell", "energy in cells", bins, 0, 200, bins, 0, 200);
  hist_energy_hull = new TH2D("energy_hull", "energy in hull", bins, 0, 200, bins, 0, 20);
  hist_energy_world = new TH2D("energy_world", "energy in world", bins, 0, 200, bins, 0, 20);
  hist_energy_back = new TH2D("energy_back", "energy behind calo", bins, 0, 200, bins, 0, 200);
  
  hist_energy_sum = new TH2D("energy_sum", "energy sum", bins, 0, 200, bins, 0, 200);
  hist_energy_missing = new TH2D("energy_missing", "energy sum", bins, 0, 200, bins, 0, 200);
}

bool ECAL_TestCal_EnergyEffic::processEvent(T4Event* event)
{
  // this function is called for each event
  // return true if this event should be saved, false if not
  // (only if -o is activated)
  
  double energy_gen = event->beamData.beamParticles.at(0).p[3];

  double energy_cell = 0;
  double energy_hull = 0;
  double energy_world = 0;
  double energy_back = 0;

  for (unsigned int i = 0; i < event->calorimeter.size(); i++) {
    T4HitData* hit = &event->calorimeter.at(i);
    if (hit->detectorName == "HULL") {
      energy_hull += hit->energyDeposit/1000.;
    } else if (hit->detectorName == "WORLD") {
      energy_world += hit->energyDeposit/1000.;
    } else if (hit->detectorName == "BACK") {
      energy_back += hit->energyDeposit/1000.;
    } else {
      energy_cell += hit->energyDeposit/1000.;
    }
  }
  hist_energy_cell->Fill(energy_gen, energy_cell);
  hist_energy_hull->Fill(energy_gen, energy_hull);
  hist_energy_world->Fill(energy_gen, energy_world);
  hist_energy_back->Fill(energy_gen, energy_back);
  
  hist_energy_sum->Fill(energy_gen, energy_cell + energy_hull + energy_world + energy_back);
  hist_energy_missing->Fill(energy_gen, energy_gen - energy_cell);
  
  return true;
}

void ECAL_TestCal_EnergyEffic::endOfEvents(void)
{
  // place to write the histograms
  hist_energy_cell->Write();
  hist_energy_hull->Write();
  hist_energy_world->Write();
  hist_energy_back->Write();

  hist_energy_sum->Write();  
  hist_energy_missing->Write();
}
