#include "toolbox.hh"

pluginList* pluginList::instance = NULL;
using namespace std;

bool terminatedOnce,interruptedOnce;

void printLogo(void);
void interruptHandler(int sig);
void do_fast_merge(std::vector<std::string> _fileList,string _outputName);

void terminationHandler(int sig);

std::string checkArguments(std::vector<std::string>& arguments,
    std::string _keyWord)
{
  std::string retVal;
  std::vector<std::string>::iterator myIter;
  myIter = std::find(arguments.begin(), arguments.end(), _keyWord);
  if (myIter != arguments.end()) {
    if (myIter + 1 == arguments.end()) {
      printf("Error! '%s' needs an argument! \n", _keyWord.c_str());
      exit(-1);
    }
    retVal = *(myIter + 1);
    arguments.erase(myIter, myIter + 2);
    printf("found value for keyword %s to: %s\n", _keyWord.c_str(),
        retVal.c_str());
    return retVal;
  } else
    return "NA";
}

void listActivatedClasses()
{
  printf("Listing activated_classes!\n\n");
  printf("-------------------------------------\n");
  for (unsigned int i = 0;
      i < pluginList::getInstance()->activated_classes.size(); i++) {
    cout << i << ": "
        << pluginList::getInstance()->activated_classes.at(i)->getName() << " "
        << endl
        << pluginList::getInstance()->activated_classes.at(i)->getDescription()
        << endl;
    cout << "--------------------------------" << endl;
  }
  printf("-------------------------------------\n");
}

int main(int argc, char **argv)
{
  /* This means: either I dont know that you  want, or I see you need help:
   * toolbox -h
   * toolbox --help
   * or insufficient parameters all lead here
   */
  terminatedOnce = false;
  interruptedOnce = false;
  
  signal(SIGINT, interruptHandler);
  signal(SIGTERM, terminationHandler);
  

  std::vector<std::string> arguments(argv + 1, argv + argc);
  printLogo();

  if (argc == 2) {
    if (strcmp(argv[1], "--list-classes") == 0) {
      listActivatedClasses();
      return 0;
    }
  }

  if (argc < 4
      || (argc > 1
          && (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0))) {
    printf("Usage: %s [input] [options] \n\n\n", argv[0]);
    printf("Input:  [single-file.tgeant] \n");
    printf("Input:  -l [file list] \n\n");
    printf("Option: -u [name of user function]\n");
    printf("Option: -o [/path/to/outFile]\n");
    printf("Option: -s [number of events per output file] (split mode)\n");
    printf("Option: -n [number of events to process]\n");
    printf("Option: -h [name of output file (not used in each plugin)]\n\n");
    printf("Option: -m [fast-merge-mode, needs -l,-o, cannot use -u,-s,-n]\n");

    return -1;
  }

  //we will look this up in a second!
  string fileListName = "NA";
  vector<string> fileList;

  string outputName = "NA";
  string extName = "NA";
  string className = "NA";
  int eventsToSave = -1;
  int eventsToProcess = -1;
  bool mergemode  = (std::find(arguments.begin(), arguments.end(), "-m") != arguments.end());

  ToolboxPlugin* activePlugin = NULL;

  //parse the input parameters
  fileListName = checkArguments(arguments, "-l");
  className = checkArguments(arguments, "-u");
  outputName = checkArguments(arguments, "-o");
  extName = checkArguments(arguments, "-h");
  string splitEvents = checkArguments(arguments, "-s");
  if (splitEvents != "NA")
    eventsToSave = strToInt(splitEvents);
  string processEvents = checkArguments(arguments, "-n");
  if (processEvents == "NA")
    processEvents = checkArguments(arguments, "-N");
  if (processEvents != "NA")
    eventsToProcess = strToInt(processEvents);
  
  if ((outputName=="NA" ||fileListName == "NA") && mergemode ){
    printf("Error! fast-merge-mode cannot be used without fileList and output!");
    exit(-1);
  }
  


  //if we removed everything known and there was no filelist, assume the last argument is the input file!
  if (fileListName == "NA")
    fileList.push_back(arguments.back());
  else {
    ifstream list(fileListName.c_str(), std::ios::in);
    if (!list.is_open() || list.eof()) {
      printf("Error! List empty or non-existant! \n");
      return -1;
    }
    while (list.good()) {
      string tmp;
      getline(list, tmp);
      if (tmp != "")
        fileList.push_back(tmp);
    }
  }
  
  if (mergemode){
    do_fast_merge(fileList,outputName);
    exit(0);
  }
  if (eventsToSave != -1 && outputName == "NA") {
    printf(
        "Error! Cannot split output files without output file specified! Use -o ! \n");
    return -1;
  }

  //if not mergeMode then its usermode, select active plugin to choose!
  if (className != "NA") {
    for (unsigned int i = 0;
        i < pluginList::getInstance()->activated_classes.size(); i++) {
      if (pluginList::getInstance()->activated_classes.at(i)->getName()
          == className)
        activePlugin = pluginList::getInstance()->activated_classes.at(i);
    }
    if (activePlugin == NULL) {
      printf("Error! Detected PluginMode but could not find Plugin: %s \n",
          className.c_str());
      listActivatedClasses();

      unsigned int size = pluginList::getInstance()->activated_classes.size();
      printf("Choose a plugin index [0-%u]: ", size - 1);
      unsigned int index;
      std::cin >> index;

      while (std::cin.fail() || index >= size) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        printf("Unexpected input! Enter a number between 0 and %u: ", size - 1);
        std::cin >> index;
      }
      activePlugin = pluginList::getInstance()->activated_classes.at(index);
    }
  }

  if (className != "NA") {
    activePlugin->createOutFile(extName);
    activePlugin->beginOfEvents();

    printf("UserPlugin loading!\n");
    cout << activePlugin->getName() << endl;
    printf("-------------------------------------------------\n");
    cout << activePlugin->getDescription() << endl;
    printf("-------------------------------------------------\n");
  }

  printf("Toolbox will process the following files: \n");
  for (unsigned int i = 0; i < fileList.size(); i++)
    printf("File number %i: %s \n", i, fileList.at(i).c_str());
  if (eventsToProcess > 0)
    printf("Toolbox will process %i events.\n", eventsToProcess);

  T4OutputBackEnd* load = new T4OutputASCII();
  T4OutputBackEnd* save;

  int fileNo = 0;
  if (outputName != "NA") {
    string fullOutputName = outputName + "_" + intToStr(fileNo);
    save = new T4OutputASCII(fullOutputName);
    save->setEvent(load->streamGetEventPointer());
    fileNo++;
  }

  int eventCounter = 0;
  int totalCounter = 0;
  bool enoughEvents = false;

  unsigned int i;
  for (i = 0; i < fileList.size(); i++) {
    if (interruptedOnce || terminatedOnce)
	break;
    std::string tgeantFile = fileList.at(i);
    if (tgeantFile.find(".tgeant") == std::string::npos) {
      std::cerr
          << "Error in openTGEANTFile: Unknown TGEANT file! Use ASCII (.tgeant) format."
          << std::endl;
      continue;
    }
    load->streamLoad(tgeantFile);

    while (load->streamGetNextEvent() && !enoughEvents) {
      if (interruptedOnce || terminatedOnce)
	break;
      if (eventsToSave != -1 && eventCounter == eventsToSave) {
        save->close();
        string fullOutputName = outputName;// + "_" + intToStr(fileNo);
        save = new T4OutputASCII(fullOutputName);
        save->setEvent(load->streamGetEventPointer());
        fileNo++;
        eventCounter = 0;
      }

      ////////// ACTIVE CODE HERE!!!!!!
      if (className == "NA") {
        if (outputName != "NA")
          save->save();
      } else if (activePlugin->processEvent(load->streamGetEventPointer())) {
        if (outputName != "NA")
          save->save();
      }
      ////////// FINISHED ACTIVE CODE HERE!!!!!!!!!
      eventCounter++;
      totalCounter++;

      enoughEvents = (totalCounter == eventsToProcess && eventsToProcess > 0);
    }
    load->streamClose();
    if (enoughEvents)
      break;
  }
  if (outputName != "NA")
    save->close();
  if (className != "NA")
    activePlugin->endOfEvents();

  delete activePlugin;
  
  cout << "--- TOOLBOXOSAURUS finished ---" << endl;
  cout << "Number of processed Files:  " << i << "/" << fileList.size() << endl;
  cout << "Number of processed Events: " << totalCounter << endl;
  cout << "-------------------------------" << endl;
}

void printLogo(void)
{
  cout << "                          .       ." << endl;
  cout << "                         / `.   .' \\" << endl;
  cout << "                 .---.  <    > <    >  .---." << endl;
  cout << "                 |    \\  \\ - ~ ~ - /  /    |" << endl;
  cout << "                  ~-..-~             ~-..-~" << endl;
  cout << "              \\~~~\\.'      E   A         `./~~~/" << endl;
  cout << "    .-~~^-.    \\__/     G          N       \\__/" << endl;
  cout << "  .'  O    \\     /   T           /   T   \\  \\" << endl;
  cout << " (_____,    `._.'               |         }  \\/~~~/" << endl;
  cout << "  `----.          /       }     |        /    \\__/" << endl;
  cout << "        `-.      |       /      |       /      `. ,~~|" << endl;
  cout << "            ~-.__|      /_ - ~ ^|      /- _      `..-'   f: f:"
      << endl;
  cout << "                 |     /        |     /     ~-.     `-. _||_||_"
      << endl;
  cout << "  TOOLBOXOSAURUS |_____|        |_____|         ~ - . _ _ _ _ _>"
      << endl;
  cout << endl;
}


void interruptHandler(int sig)
{
  printf("CTRL+C found\n");
  if (interruptedOnce){
    printf("CTRL+C found twice, killing with force!\n");
    exit(-1);
  }
  else {
    printf("Trying graceful shutdown! Please wait a few seconds!.....\n");
    interruptedOnce = true;
  }
}

void terminationHandler(int sig)
{
  if (terminatedOnce){
   printf("Termination requested twice!\n");
   exit(-1);
  }
  else {
    printf(
        "Termination has been requested! Stopping and trying to save some data!\n");
    terminatedOnce = true;
  }
}

void do_fast_merge ( vector< string > _fileList, string _outputName ) {
  ogzstream* output;
  igzstream* input;
  input = new igzstream;
  output = new ogzstream;
  output->open(_outputName.c_str());
  printf("Ultra fast-merge-mode actived! concenating %u files!\n",_fileList.size());
  for (unsigned int i = 0; i < _fileList.size();i++){
    if (interruptedOnce || terminatedOnce)
	break;
    
    printf("Adding file number %i: %s \n",i,_fileList.at(i).c_str());
    input->open(_fileList.at(i).c_str());
    if(!input->good()){
      printf("Warning! File was broken! Taking next one! \n");
      continue;
    }
    (*output)<<input->rdbuf(); 
    output->flush();
    input->close();
    input->clear();
  }
  printf("All files merged successfully (hopefully :D) \n");
  output->flush();
  output->close();
  
}

