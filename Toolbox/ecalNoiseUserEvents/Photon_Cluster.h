#ifndef USER_PHOTON_CLUSTER_H_
#define USER_PHOTON_CLUSTER_H_

#include "PaEvent.h"
#include "PaVertex.h"
#include "Phast.h"
#include "PaAlgo.h"

class Photon_Cluster {
  public:
    long long UniqueEvNum;
    double vertex[3];
    double momentum[3];

    int caloNumber;
    double clusterPos[3];
    double energy;
    double eCellSum;
    int clusterSize;
    int nCells;
    int cellNumbers[100];
    double cellEnergy[100];
    int iCell;
    double iCell_xc;
    double iCell_yc;
    double time;

    Photon_Cluster(void) {};
    ~Photon_Cluster(void) {};

    void fillCluster(const PaCaloClus* c) {
      caloNumber = c->iCalorim();
      clusterPos[0] = c->X();
      clusterPos[1] = c->Y();
      clusterPos[2] = c->Z();
      time=c->Time();
      energy = c->E();
      eCellSum = c->ECellSum();
      clusterSize = c->Size();
      nCells = (int) c->vCellNumber().size();
      for (int i = 0; i < 100; i++) {
        if (i < nCells) {
          cellNumbers[i] = c->vCellNumber().at(i);
          cellEnergy[i] = c->vCellEnergy().at(i);
        } else {
          cellNumbers[i] = 0;
          cellEnergy[i] = 0;
        }
      }
      iCell = c->iCell(iCell_xc, iCell_yc);
    }
};



#endif /* USER_PHOTON_CLUSTER_H_ */
