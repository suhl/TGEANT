
#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TH3D.h"
#include "TTree.h"
#include "TProfile.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "TMath.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaParticle.h"
#include "PaAlgo.h"
#include "G3part.h"
#include "PaMCvertex.h"
#include "PaMCtrack.h"
#include "Photon_Cluster.h"

TH1D* mc__h[100];
TH2D* MC_HistTwo[100];
TH3D* MC_HistThree[100];


/*** ECAL NOISE ANALYSIS  -- MC COMPARISON***/
void UserEvent5 ( PaEvent& e )      // shortcut e = PaEvent
{

    vector<LUJET> lujets;
    int nLujet;
    double eMcCluster=-1;
    e.MCgen(nLujet,lujets);
    for(int i = 0; i < lujets.size();i++){
      //find photon entry
      if (lujets.at(i).k[1] == 22){
	eMcCluster = lujets.at(i).p[3];
	break;
      }
    }
//     cout << "found photon energy: " << eMcCluster << endl;
    
    
    vector<int> ecal[3];
    double threshold[3];
    int counter;
    static TTree* tree;
    static Photon_Cluster* photon_cluster;

    const double M_mu = G3partMass[5];  // muon   mass
    const double M_pi = G3partMass[8];  // pion   mass
    const double M_Pr = G3partMass[14]; // proton mass
    const double M_e  = G3partMass[2];  // electron mass



    static float refpos0 = 0.;
    const PaSetup& setup = PaSetup::Ref();

    static bool first ( true );
    if ( first ) {
        counter = 0;
        mc__h[0] = new TH1D ( "MC_ECAL0energy","MC_ECAL0 -- energy",300,0,9 );
        mc__h[1] = new TH1D ( "MC_ECAL1energy","MC_ECAL1 -- energy",300,0,9 );
        mc__h[2] = new TH1D ( "MC_ECAL2energy","MC_ECAL2 -- energy",300,0,9 );

        mc__h[3] = new TH1D ( "MC_ECAL0time","MC_ECAL0 -- time",120,-60,60 );
        mc__h[4] = new TH1D ( "MC_ECAL1time","MC_ECAL1 -- time",120,-60,60 );
        mc__h[5] = new TH1D ( "MC_ECAL2time","MC_ECAL2 -- time",120,-60,60 );

        mc__h[6] = new TH1D ( "MC_ECAL0Cells","MC_ECAL0 -- hit cells",20,0,20 );
        mc__h[7] = new TH1D ( "MC_ECAL1Cells","MC_ECAL1 -- hit cells",20,0,20 );
        mc__h[8] = new TH1D ( "MC_ECAL2Cells","MC_ECAL2 -- hit cells",20,0,20 );

        mc__h[9] = new TH1D ( "MC_ECAL0ClusterSize","MC_ECAL0 -- ClusterSize",10,0,10 );
        mc__h[10] = new TH1D ( "MC_ECAL1ClusterSize","MC_ECAL1 -- ClusterSize",10,0,10 );
        mc__h[11] = new TH1D ( "MC_ECAL2ClusterSize","MC_ECAL2 -- ClusterSize",10,0,10 );



        mc__h[25] = new TH1D ( "TriggerCounter","TriggerCounter -1 = all, 1 = RT",10,-5,5 );


        MC_HistTwo[0] = new TH2D ( "MC_ECAL0 distr","MC_ECAL0 distr",200,-200,200,200,-200,200 );
        MC_HistTwo[1] = new TH2D ( "MC_ECAL1 distr","MC_ECAL1 distr",800,-400,400,200,-200,200 );
        MC_HistTwo[2] = new TH2D ( "MC_ECAL2 distr","MC_ECAL2 distr",200,-200,200,200,-200,200 );

        MC_HistTwo[3] = new TH2D ( "MC_ECAL0_modnum_vs_e","MC_ECAL0module_number vs energy",200,0,10,510,2000,2510 );
        MC_HistTwo[4] = new TH2D ( "MC_ECAL1_modnum_vs_e","MC_ECAL1module_number vs energy",200,0,10,1725,6900,8625 );
        MC_HistTwo[5] = new TH2D ( "MC_ECAL2_modnum_vs_e","MC_ECAL2module_number vs energy",200,0,10,3003,3800,6803 );


//         MC_HistTwo[1] = new TH2D("MC_ECAL1 distr","MC_ECAL1 distr",800,-400,400,200,-200,200);
//         MC_HistTwo[2] = new TH2D("MC_ECAL2 distr","MC_ECAL2 distr",200,-200,200,200,-200,200);
//



        MC_HistThree[0] = new TH3D ( "MC_ECAL0 distr vs e","MC_ECAL0 distr vs e",200,-200,200,200,-200,200,200,0,10 );
        MC_HistThree[1] = new TH3D ( "MC_ECAL1 distr vs e","MC_ECAL1 distr vs e",800,-400,400,200,-200,200,200,0,10 );
        MC_HistThree[2] = new TH3D ( "MC_ECAL2 distr vs e","MC_ECAL2 distr vs e",200,-200,200,200,-200,200,200,0,10 );
        MC_HistThree[3] = new TH3D ( "MC_ECAL0 distr vs t","MC_ECAL0 distr vs t",200,-200,200,200,-200,200,120,-60,60 );
        MC_HistThree[4] = new TH3D ( "MC_ECAL1 distr vs t","MC_ECAL1 distr vs t",800,-400,400,200,-200,200,120,-60,60 );
        MC_HistThree[5] = new TH3D ( "MC_ECAL2 distr vs t","MC_ECAL2 distr vs t",200,-200,200,200,-200,200,120,-60,60 );


        threshold[0] = 4.;
        threshold[1] = 5.;
        threshold[2] = 10.;

        first=false;
        cout << endl << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << "-------------------------------- UserEvent5 called ----------------------------" << endl;
        cout << "Ecal Noise Analysis -- COMPARISON FOR MC" << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << endl << endl;
        first = false;

        tree = new TTree ( "CLUSTER", "Ecal noise cluster" );
        photon_cluster = new Photon_Cluster();
        tree->Branch ( "UniqueEvNum", &photon_cluster->UniqueEvNum, "UniqueEvNum/L" );
        tree->Branch ( "vertex", &photon_cluster->vertex, "vertex[3]/D" );
        tree->Branch ( "momentum", &photon_cluster->momentum, "momentum[3]/D" );
        tree->Branch ( "caloNumber", &photon_cluster->caloNumber, "caloNumber/I" );
        tree->Branch ( "clusterPos", &photon_cluster->clusterPos, "clusterPos[3]/D" );
        tree->Branch ( "energy", &photon_cluster->energy, "energy/D" );
        tree->Branch ( "eCellSum", &photon_cluster->eCellSum, "eCellSum/D" );
        tree->Branch ( "clusterSize", &photon_cluster->clusterSize, "clusterSize/I" );
        tree->Branch ( "nCells", &photon_cluster->nCells, "nCells/I" );
        tree->Branch ( "cellNumbers", &photon_cluster->cellNumbers, "cellNumbers[100]/I" );
        tree->Branch ( "cellEnergy", &photon_cluster->cellEnergy, "cellEnergy[100]/D" );
        tree->Branch ( "iCell", &photon_cluster->iCell, "iCell/I" );
        tree->Branch ( "iCell_xc", &photon_cluster->iCell_xc, "iCell_xc/D" );
        tree->Branch ( "iCell_yc", &photon_cluster->iCell_yc, "iCell_yc/D" );

        photon_cluster->vertex[0] = 0;
        photon_cluster->vertex[1] = 0;
        photon_cluster->vertex[2] = 0;
        photon_cluster->momentum[0] = 0;
        photon_cluster->momentum[1] = 0;
        photon_cluster->momentum[2] = 0;


    }

    photon_cluster->UniqueEvNum = e.UniqueEvNum();
    e.TagToSave();
    counter++;
    ecal[0].clear();
    ecal[1].clear();
    ecal[2].clear();

    if ( e.NCaloClus() == 0 ) {
        return;
    }

    for ( int i = 0; i < e.NCaloClus(); i++ ) { // loop over calo clusters
        const PaCaloClus& cl = e.vCaloClus ( i );
        int myCalo = cl.iCalorim();

        if ( myCalo != 0 && myCalo != 1 && myCalo != 2 ) {
            continue;
        }

        ecal[myCalo].push_back ( i );
    }

    for ( unsigned int i = 0; i < 3; i++ ) {
        for ( unsigned int j = 0; j < ecal[i].size(); j++ ) {
            const PaCaloClus& cl = e.vCaloClus ( ecal[i].at ( j ) );
            bool cl_charged=false;
            for ( int pars = 0; pars < cl.NParticles(); pars++ ) {
                if ( e.vParticle ( cl.iParticle ( pars ) ).iTrack() != -1 ) {
                    cl_charged = true;
                    break;
                }
            }

            if ( cl_charged ) {
                continue;
            }
            if ( cl.vCellNumber().size() != 1 || cl.Size() != 1 ) {
                continue;
            }
            
            //clean out the mc clusters
//             if (fabs(cl.E() - eMcCluster) < 5.0)
// 	      continue;
            if (cl.E() > 10 || fabs(cl.Time()) < 2.0)
	      continue;

            photon_cluster->fillCluster ( &cl );
            tree->Fill();

            mc__h[i]->Fill ( cl.E() );
            mc__h[3+i]->Fill ( cl.Time() );
            mc__h[6+i]->Fill ( cl.vCellNumber().size() );
            mc__h[9+i]->Fill ( cl.Size() );
            MC_HistTwo[i]->Fill ( cl.X(),cl.Y() );
            MC_HistThree[i]->Fill ( cl.X(),cl.Y(),cl.E() );
            MC_HistThree[i+3]->Fill ( cl.X(),cl.Y(),cl.Time() );
            double x=cl.X();
            double y=cl.Y();
            MC_HistTwo[i+3]->Fill ( cl.E(),cl.iCell ( x,y ) );
            mc__h[25]->Fill ( i+6 );
        }
    }

}





