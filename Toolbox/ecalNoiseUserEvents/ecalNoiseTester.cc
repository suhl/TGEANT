#include <iostream>

#include "T4CaloNoise.hh"

#include "TH1D.h"
#include "TFile.h"
TH1D* caloEnergy[3];

int main (int argc, char** argv){
  if (argc < 3){
    printf("Too few arguments! Use like %s inputCDFs outputTree.root \n",argv[0]);
    return -1;
  }
  
  char buffer[200];
  T4CaloNoise::getInstance()->loadCDFs(argv[1]);
  T4CaloNoise::getInstance()->setActive();
  T4CaloNoise::getInstance()->setRate(20.0);
  for (int i = 0; i < 3; i++){
    sprintf(buffer,"energy_ecal_%i",i);
    caloEnergy[i] = new TH1D(buffer,buffer,200,0,10);
  }
//   #pragma omp parallel
  for (int i = 0; i <10;i++){
    double e,t;
    int cell,ecal;
//     if (i%100000 == 0)
//       printf("At event %i \n",i);
    T4CaloNoise::getInstance()->makeANoise(cell,ecal,e,t);
    printf("%i: %f\n",i,e);
    caloEnergy[ecal]->Fill(e);
  }
  
  TFile* myFile = new TFile(argv[2],"RECREATE");
  for (int i = 0; i < 3; i++)
    caloEnergy[i]->Write();
  
  myFile->Save();
  myFile->Close();
  return 0;
}