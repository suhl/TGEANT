#!/usr/bin/python3
import argparse
import os,sys,ROOT,math
import numpy

def main():
  parser = argparse.ArgumentParser(description='Smoothing a single histogram')
  parser.add_argument("InputROOTfile")
  parser.add_argument("histogramName")
  parser.add_argument("outputFile")
  parser.add_argument("-s","--use_smoothing",action="store_true", help="Makes holes and low sens regions more homogenious")
  parser.add_argument("-S","--use_super_smoothing",action="store_true", help="needs -s to be set!")
  
  args=parser.parse_args()
  
  if args.use_super_smoothing and not args.use_smoothing:
    print("Cannot use super smoothing without smoothing!")
    sys.exit(0)
  
  f1 = ROOT.TFile(args.InputROOTfile,'read')
  print("Opening root-file: "+args.InputROOTfile)
  
  print("Trying to get the histogram!");
  
  if f1 is None:
    print("could not open file!!!")
    sys.exit(0)
  
  
  effic = f1.Get(args.histogramName)
  if effic is None:
    print("could not open the Histogram!!!: "+args.histogramName)
    sys.exit(0)
  
  efficSmooth = effic.Clone()
  
  if args.use_smoothing:
  #smooth unilluminated areas
    print("smoothing")
    edgeVal = 1./math.sqrt(2)
    outerEdge = 1./(2*math.sqrt(2))
    almostOuter = 1./(math.sqrt(5))
    smoothKernel = [
                    [outerEdge,almostOuter,0.5,almostOuter,outerEdge],
                    [almostOuter,edgeVal,1.0,edgeVal,almostOuter],
                    [0.5,1.0,0.0,1.0,0.5],
                    [almostOuter,edgeVal,1.0,edgeVal,almostOuter],
                    [outerEdge,almostOuter,0.5,almostOuter,outerEdge]
                    ]
    
    for x in range(3,effic.GetNbinsX()-4):
      for y in range(3,effic.GetNbinsY()-4):
        if effic.GetBinContent(x,y) == -1.0:
          #print("empty")
          nextvalues=[]
          mean=0.0
          det=0.0
          for xnext in range (x-2,x+3):
            for ynext in range(y-2,y+3):
              if xnext == x and ynext == y:
                continue
              if effic.GetBinContent(xnext,ynext) != -1.0:
                nextvalues.append(effic.GetBinContent(xnext,ynext))
                mean+=effic.GetBinContent(xnext,ynext)*smoothKernel[xnext-x+2][ynext-y+2]
                det+=smoothKernel[xnext-x+2][ynext-y+2]
          if det > 6.0:
            mean /= det
            efficSmooth.SetBinContent(x,y,mean)
  efficSuperSmooth = efficSmooth.Clone()
  if args.use_super_smoothing:    
    
    print("SuperSmoothing")
    edgeVal = 1./math.sqrt(2)
    smoothKernel = [[edgeVal,1,edgeVal],
                  [1,2.,1],
                  [edgeVal,1,edgeVal]]
    for x in range(2,effic.GetNbinsX()-2):
      for y in range(2,effic.GetNbinsY()-2):
        mean=0.0
        det=0.0
        nextvalues=[]
        for xnext in range (x-1,x+2):
          for ynext in range(y-1,y+2):
            if efficSmooth.GetBinContent(xnext,ynext) != -1.0:
              if not ( xnext == x and ynext == y):
                  nextvalues.append(efficSmooth.GetBinContent(xnext,ynext))
                
              mean+=efficSmooth.GetBinContent(xnext,ynext)*smoothKernel[xnext-x+1][ynext-y+1]
              det+=smoothKernel[xnext-x+1][ynext-y+1]
        if det > 0.0 and len(nextvalues) > 4:
          mean /= det
          arr = numpy.array(nextvalues)
          stdDev = numpy.std(arr, axis=0)
          if stdDev < 0.2 and math.fabs( efficSmooth.GetBinContent(x,y) - mean) > 0.2:
              efficSuperSmooth.SetBinContent(x,y,mean)
    newMean = 0.0
    counter = 0
    for x in range(1,efficSuperSmooth.GetNbinsX()):
      for y in range(1,efficSuperSmooth.GetNbinsY()):
        if efficSuperSmooth.GetBinContent(x,y) > 0.0:
          newMean+=efficSuperSmooth.GetBinContent(x,y)
          counter+=1
    if counter > 0:
      newMean /= counter
      efficSuperSmooth.SetTitle(efficSuperSmooth.GetTitle()+" |newEffic:"+str(newMean))
      print("NewMean="+str(newMean))
      print(efficSuperSmooth.GetTitle())
    
    
  f2 = ROOT.TFile(args.outputFile,'recreate')
  if f2 is None:
    print("Could not open outputfile for writing: "+args.outputFile)
    sys.exit(0)
  if args.use_smoothing:
    if args.use_super_smoothing:
      efficSuperSmooth.Write()
      #newMean.Write()
      #fftHist = efficSuperSmooth.Clone()
      #fftHist = efficSuperSmooth.FFT(fftHist,"MAG R2C ES")
      #fftHist.Write()
    else:
      efficSmooth.Write()
  else:
    effic.Write()
  f2.Flush
  f2.Close()
  print("All done! Wrote output to: "+args.outputFile)


if __name__ == "__main__":
  main()

