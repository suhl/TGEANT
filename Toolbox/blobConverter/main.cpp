#include <iostream>
#include <stdio.h>
#include <cstring>
#include <cmath>
#include <fstream>
#include <sstream>
#include "TH2D.h"

#include "lodepng.h"
using namespace std;

int main(int argc, char **argv) {

    if (argc < 2) {
        printf("Too few arguments! Use: %s [image] \n",argv[0]);
	printf("Uses Lodepng! http://lodev.org/lodepng/ \n");
        return -1;
    }

    cout << argv[1] << endl;

    std::vector<unsigned char> image; //the raw pixels
    unsigned width, height;

    //decode
    unsigned error = lodepng::decode(image, width, height, argv[1],LCT_GREY_ALPHA);

    //if there's an error, display it
    if(error) std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;

    cout << " width height length: " << width << " "<< height << " " << image.size() << endl;

    TH2D* myHist = new TH2D("TROLOLOL","TROLOLOL",width,0,width,height,0,height);

    for (unsigned int i = 0; i < height; i++) {
        for (unsigned int j = 0; j < width; j++) {
            int pixelval = static_cast<int>(image.at(2.*(width*i+j)));
            int alphaVal = static_cast<int>(image.at(2.*(width*i+j)+1));

            float val = static_cast<float>(pixelval)/(pow(2.,8.)-1.);
            if (alphaVal == 0)
                val = -1;
            myHist->SetBinContent(j+1,(height-i)+1,val);
        }
    }

    int nbinX, nbinY;
    nbinX = myHist->GetNbinsX();
    nbinY = myHist->GetNbinsY();

    stringstream theBlob;
    double addition = 0.0;
    int meanCount = 0;
    for ( int x = 1; x <= nbinX; x++ ) {
        for ( int y = 1; y <= nbinY; y++ ) {
            theBlob << myHist->GetBinContent ( x, y ) << " ";
            if ( myHist->GetBinContent ( x, y ) > 0 ) {
                meanCount++;
                addition += myHist->GetBinContent ( x, y );
            }
        }
        theBlob << "<br />";
    }
    addition /= ( double ) meanCount;

    
    
    string filenameNew = basename(argv[1]);
    filenameNew += ".txt";

    ofstream myOutAscii(filenameNew.c_str(),ofstream::out);
    
    myOutAscii << theBlob.str();
    myOutAscii<< flush;
    myOutAscii.close();
    

    return 0;
}
