
#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TProfile.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "TMath.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaParticle.h"
#include "PaAlgo.h"
#include "G3part.h"
#include "PaMCvertex.h"
#include "PaMCtrack.h"


/* UserEvent for BeamFile extraction! */

void UserEvent3(PaEvent& e)         // shortcut e = PaEvent
{
  
  static TTree* tree(NULL);

  const double M_mu = G3partMass[5];  // muon   mass
  const double M_pi = G3partMass[8];  // pion   mass
  const double M_Pr = G3partMass[14]; // proton mass
  const double M_e  = G3partMass[2];  // electron mass
  
  static int   Run;     // Run number
  static float Xprim;   // X coordinate of primary vertex
  static float Yprim;   // Y coordinate of primary vertex
  static float Zprim;   // Z coordinate of primary vertex
  static float Rprim;   // R coordinate of primary vertex
  static int   Nout;    // Number of tracks in primary vertex
  static float Ch2prim; // Chi2 of primary vertex
  static float Ybj, Ybj_MC;       // Y
  static float W, W_MC;       // W2
  static float Xf;      // Xf
  static float Q2, Q2_MC;      // Q2
  static float Xbj, Xbj_MC ;     // Xbjorken
  static float ScAngle ;// scattering angle, mrad

  W=Xf=Q2=Xbj=Ybj=-10000.; // some defaults
  W_MC=Q2_MC=Xbj_MC=Ybj_MC=-10000.; // some defaults
  ScAngle=-1000; // some defaults

  static int physbits[32];
  for (int ib=0 ; ib<32 ; ib++) physbits[ib]=0;
  physbits[0]=1;  // ICT
  physbits[1]=1;  // MCT
  physbits[2]=1;  // LCT
  physbits[3]=1;  // OT
  physbits[4]=1;  // CT
  physbits[7]=1;  // beam
  physbits[8]=1;  // MT/2
  physbits[10]=1; // J/Psi
  physbits[11]=1; // random

  static float refpos0 = 0.;

  static TH1D* h1[200];
  static TH2D* h2[60];
  static TProfile* p[10];

  const PaSetup& setup = PaSetup::Ref();
  
  static bool first(true);
  if(first){                         // histograms and Ntupes booking block

    h1[0]  = new TH1D("usr3_h0","P beam       ",                       100,   50., 250.);
    h1[1]  = new TH1D("usr3_h1","X beams (cm) ",                       100,   -5., 5.);
    h1[2]  = new TH1D("usr3_h2","Y beams (cm) ",                       100,   -5., 5.);

    h1[3]  = new TH1D("usr3_h3","X slope beams (mrad)",                100,   -10., 10.);
    h1[4]  = new TH1D("usr3_h4","Y slope beams (mrad)",                100,   -10., 10.);


    h1[5]  = new TH1D("usr3_h5","time beams (ns) ",                    100,   -50., 50.);

    h1[6]  = new TH1D("usr3_h6","nhits beams         ",                20,   0., 20.);
    h1[7]  = new TH1D("usr3_h7","nhits FI beams      ",                20,   0., 20.);
    h1[8]  = new TH1D("usr3_h8","chi2/ndf beams      ",               100,   0., 20.);
    h1[9]  = new TH1D("usr3_h9","Prob beams          ",                50,   0., 1.);

   // Double_t Prob(Double_t chi2,Int_t ndf)

    h1[11]  = new TH1D("usr3_h11","X beams (cm) noIT ",                       100,   -5., 5.);
    h1[12]  = new TH1D("usr3_h12","Y beams (cm) noIT",                       100,   -5., 5.);
    h1[13]  = new TH1D("usr3_h13","X slope beams (mrad) noIT",                100,   -10., 10.);
    h1[14]  = new TH1D("usr3_h14","Y slope beams (mrad) noIT",                100,   -10., 10.);

    h1[21]  = new TH1D("usr3_h21","X beams (cm) noILT",                       100,   -5., 5.);
    h1[22]  = new TH1D("usr3_h22","Y beams (cm) noILT",                       100,   -5., 5.);
    h1[23]  = new TH1D("usr3_h23","X slope beams (mrad) noILT",                100,   -10., 10.);
    h1[24]  = new TH1D("usr3_h24","Y slope beams (mrad) noILT",                100,   -10., 10.);

    h1[30]  = new TH1D("usr3_h30","P beam       AngSgt2.0",                       100,   50., 250.);
    h1[31]  = new TH1D("usr3_h31","X beams (cm) AngSgt2.0",                       100,   -5., 5.);
    h1[32]  = new TH1D("usr3_h32","Y beams (cm) AngSgt2.0",                       100,   -5., 5.);
    h1[33]  = new TH1D("usr3_h33","X slope beams (mrad) AngSgt2.0",                100,   -10., 10.);
    h1[34]  = new TH1D("usr3_h34","Y slope beams (mrad) AngSgt2.0",                100,   -10., 10.);

    h1[40]  = new TH1D("usr3_h40","P beam       AngSgt3.0",                       100,   50., 250.);
    h1[41]  = new TH1D("usr3_h41","X beams (cm) AngSgt3.0",                       100,   -5., 5.);
    h1[42]  = new TH1D("usr3_h42","Y beams (cm) AngSgt3.0",                       100,   -5., 5.);
    h1[43]  = new TH1D("usr3_h43","X slope beams (mrad) AngSgt3.0",                100,   -10., 10.);
    h1[44]  = new TH1D("usr3_h44","Y slope beams (mrad) AngSgt3.0",                100,   -10., 10.);

    h1[50]  = new TH1D("usr3_h50","P beam       AngSgt4.0",                       100,   50., 250.);
    h1[51]  = new TH1D("usr3_h51","X beams (cm) AngSgt4.0",                       100,   -5., 5.);
    h1[52]  = new TH1D("usr3_h52","Y beams (cm) AngSgt4.0",                       100,   -5., 5.);
    h1[53]  = new TH1D("usr3_h53","X slope beams (mrad) AngSgt4.0",                100,   -10., 10.);
    h1[54]  = new TH1D("usr3_h54","Y slope beams (mrad) AngSgt4.0",                100,   -10., 10.);

    h1[60]  = new TH1D("usr3_h60","P beam       IT",                       100,   50., 250.);
    h1[61]  = new TH1D("usr3_h61","X beams (cm) IT ",                       100,   -5., 5.);
    h1[62]  = new TH1D("usr3_h62","Y beams (cm) IT",                       100,   -5., 5.);
    h1[63]  = new TH1D("usr3_h63","X slope beams (mrad) IT",                100,   -10., 10.);
    h1[64]  = new TH1D("usr3_h64","Y slope beams (mrad) IT",                100,   -10., 10.);

    h1[70]  = new TH1D("usr3_h70","P beam       LT",                       100,   50., 250.);
    h1[71]  = new TH1D("usr3_h71","X beams (cm) LT ",                       100,   -5., 5.);
    h1[72]  = new TH1D("usr3_h72","Y beams (cm) LT",                       100,   -5., 5.);
    h1[73]  = new TH1D("usr3_h73","X slope beams (mrad) LT",                100,   -10., 10.);
    h1[74]  = new TH1D("usr3_h74","Y slope beams (mrad) LT",                100,   -10., 10.);

    h1[80]  = new TH1D("usr3_h80","P beam       MT",                       100,   50., 250.);
    h1[81]  = new TH1D("usr3_h81","X beams (cm) MT ",                       100,   -5., 5.);
    h1[82]  = new TH1D("usr3_h82","Y beams (cm) MT",                       100,   -5., 5.);
    h1[83]  = new TH1D("usr3_h83","X slope beams (mrad) MT",                100,   -10., 10.);
    h1[84]  = new TH1D("usr3_h84","Y slope beams (mrad) MT",                100,   -10., 10.);

    h1[90]  = new TH1D("usr3_h90","P beam       OT",                       100,   50., 250.);
    h1[91]  = new TH1D("usr3_h91","X beams (cm) OT ",                       100,   -5., 5.);
    h1[92]  = new TH1D("usr3_h92","Y beams (cm) OT",                       100,   -5., 5.);
    h1[93]  = new TH1D("usr3_h93","X slope beams (mrad) OT",                100,   -10., 10.);
    h1[94]  = new TH1D("usr3_h94","Y slope beams (mrad) OT",                100,   -10., 10.);

    h1[100]  = new TH1D("usr3_h100","P beam       BT",                       100,   50., 250.);
    h1[101]  = new TH1D("usr3_h101","X beams (cm) BT ",                       100,   -5., 5.);
    h1[102]  = new TH1D("usr3_h102","Y beams (cm) BT",                       100,   -5., 5.);
    h1[103]  = new TH1D("usr3_h103","X slope beams (mrad) BT",                100,   -10., 10.);
    h1[104]  = new TH1D("usr3_h104","Y slope beams (mrad) BT",                100,   -10., 10.);

    h1[110]  = new TH1D("usr3_h110","P beam       RT",                       100,   50., 250.);
    h1[111]  = new TH1D("usr3_h111","X beams (cm) RT ",                       100,   -5., 5.);
    h1[112]  = new TH1D("usr3_h112","Y beams (cm) RT",                        100,   -5., 5.);
    h1[113]  = new TH1D("usr3_h113","X slope beams (mrad) RT",                100,   -10., 10.);
    h1[114]  = new TH1D("usr3_h114","Y slope beams (mrad) RT",                100,   -10., 10.);

    h1[120]  = new TH1D("usr3_h120","P beam       CT",                       100,   50., 250.);
    h1[121]  = new TH1D("usr3_h121","X beams (cm) CT ",                       100,   -5., 5.);
    h1[122]  = new TH1D("usr3_h122","Y beams (cm) CT",                        100,   -5., 5.);
    h1[123]  = new TH1D("usr3_h123","X slope beams (mrad) CT",                100,   -10., 10.);
    h1[124]  = new TH1D("usr3_h124","Y slope beams (mrad) CT",                100,   -10., 10.);
    

    h1[130]  = new TH1D("usr3_h130","P beam       Q2gt1.0",                       100,   50., 250.);
    h1[131]  = new TH1D("usr3_h131","X beams (cm) Q2gt1.0",                       100,   -5., 5.);
    h1[132]  = new TH1D("usr3_h132","Y beams (cm) Q2gt1.0",                       100,   -5., 5.);
    h1[133]  = new TH1D("usr3_h133","X slope beams (mrad) Q2gt1.0",                100,   -10., 10.);
    h1[134]  = new TH1D("usr3_h134","Y slope beams (mrad) Q2gt1.0",                100,   -10., 10.);


    p[0]   = new TProfile("p_00","P vs scang",    51 , -1, 50, 100., 220.);
    p[1]   = new TProfile("p_01","X vs scang",    51 , -1, 50, -4., 4.);
    p[2]   = new TProfile("p_02","Y vs scang",    51 , -1, 50, -4., 4.);
    p[3]   = new TProfile("p_03","xslope vs scang",    51 ,-1, 50, -4., 4.);
    p[4]   = new TProfile("p_04","yslope vs scang",    51 , -1, 50, -3., 3.);





    first=false;
  } // end of histogram booking
  

  int TMaskRaw = e.TrigMask();

  int TMask = 0;
  int tbits[32];
  int nset=0;
  for (int ib=0 ; ib<32 ; ib++){
    tbits[ib]=(TMaskRaw>>ib)&1;
    if( tbits[ib]!=0 && physbits[ib]!=0 ) {
      nset+=1;
      TMask+=pow(2.,ib);
    }
  }




  bool ICT = false;
  bool LCT = false;
  bool MCT = false;
  bool MT = false;
  bool OT = false;
  bool pCT = false;
  bool BT = false;
  bool RT = false;

  if( (TMask>>0)&1 ) ICT = true;
  if( (TMask>>1)&1 ) MCT = true;
  if( (TMask>>2)&1 ) LCT = true;
  if( (TMask>>3)&1 )  OT = true;
  if( (TMask>>7)&1 )  BT = true;
  if( (TMask>>8)&1 )  MT = true;  
  if( (TMask>>11)&1)  RT = true;
  if(  TMask==16   ) pCT = true;  





  for(int iv = 0; iv < e.NVertex(); iv++){  // loop over reconstructed vertices

    const PaVertex& v = e.vVertex(iv);   // "v" now is "synonym" for "vertex # iv"

    // select vertices with beam and mu'
    //    if(! v.IsPrimary()) continue;        // skip not primary vertex.
    if(iv != e.iBestPrimaryVertex())    continue; //  skip not best primary vertex.

    int imu0 = v.InParticle();           // get beam      mu index
    int imu1 = v.iMuPrim();              // get scattered mu index
    if( imu1 == -1 && TMask==16 ) imu1 = PaAlgo::GetMW1ScatMuon(e);
    if(imu0 == -1)    continue;          // skip if there is no beam (same as "if not primary")
    if(imu1 == -1)    continue;          // skip if there is no mu'

    // get mu, mu' Lorentz vectors at primary vertex
    const PaParticle& Mu0   = e.vParticle(imu0);    // it's beam muon
    const PaParticle& Mu1   = e.vParticle(imu1);    // it's scattered muon
    int itMu1               = Mu1.iTrack();         // mu' track number
    const PaTrack& trkMu1   = e.vTrack(itMu1);      // mu' track
    const PaTPar& ParamMu0  = Mu0.ParInVtx(iv);     // fitted mu  parameters in the primary vertex
    const PaTPar& ParamMu1  = Mu1.ParInVtx(iv);     // fitted mu' parameters in the primary vertex
    TLorentzVector LzVecMu0 = ParamMu0.LzVec(M_mu); // calculate beam      mu Lorentz vector
    TLorentzVector LzVecMu1 = ParamMu1.LzVec(M_mu); // calculate scattered mu Lorentz vector
    TLorentzVector LzVecGam = LzVecMu0 - LzVecMu1; // gamma*

    // calculate some kinematic variables
    Ybj   = (LzVecMu0.E()-LzVecMu1.E())/LzVecMu0.E();
    Q2  =      PaAlgo::Q2 (LzVecMu0, LzVecMu1);
    Xbj =      PaAlgo::xbj(LzVecMu0, LzVecMu1);
    W   = sqrt(PaAlgo::W2 (LzVecMu0, LzVecMu1));
    TVector3 Mu0Vect = LzVecMu0.Vect();
    TVector3 Mu1Vect = LzVecMu1.Vect();
    ScAngle = Mu0Vect.Angle(Mu1Vect)*1000.;

  }

  //  if(Q2<0.) return;

  for(int it=0; it<e.NTrack();it++){

    //    cout<<" it" <<it<<endl;
    const PaTrack& t = e.vTrack(it);
    if(t.NTPar()==0) continue;
    const PaTPar & par = t.vTPar(0);

    float bmom     = 0.;
    float bQ       = 0.;
    if(par.HasMom()) {
      bmom = par.Mom();
      bQ =  par.Q();
    }


    float firstZ   = par(0);
    float lastZ   = t.ZLast();
    if( firstZ > 0. ) continue; // ne beam 

    int   nhits   = t.NHits ();
    float chi2tot = t.Chi2tot();
    float chi2ndf = chi2tot/float(nhits);

    if( chi2ndf>4. ) continue;

    double bprob = TMath::Prob( chi2tot,nhits );

    float time    = t.MeanTime();                                
//    float etime   = t.SigTime();
//    if(etime<=0.) etime = 100000.;

    int nhitFI = t.NHitsFoundInDetect("FI");
    //    if(nhitFI<3) continue;

    bool isAbeam=false;

    int ipart =	t.iParticle();
    if( ipart>=0 ){
      const PaParticle& p = e.vParticle(ipart);
      isAbeam = p.IsBeam();
    } 

    PaTPar par0;
    bool yes ;

    yes=par.Extrap(refpos0, par0);

    //    float firstZ   = par0(0); 
    float X    = par0(1);
    float Y    = par0(2);
    float dXdZ = par0(3);
    float dYdZ = par0(4);
    float Qp       = par0(5);

    float xslope = atan(dXdZ)*1000.; 
    float yslope = atan(dYdZ)*1000.;


    if(bmom>140.&&bmom<180.) {

      if(isAbeam){

    h1[0]->Fill(bmom);
    h1[1]->Fill(X);
    h1[2]->Fill(Y);
    h1[3]->Fill(xslope);
    h1[4]->Fill(yslope);

    if(!pCT) h1[5]->Fill(time);
    h1[6]->Fill(float(nhits));
    h1[7]->Fill(float(nhitFI));    

    h1[8]->Fill(chi2ndf);
    h1[9]->Fill(bprob);

    if(!ICT&&!pCT) {

    h1[11]->Fill(X);
    h1[12]->Fill(Y);
    h1[13]->Fill(xslope);
    h1[14]->Fill(yslope);

    }
    if(!ICT&&!LCT&&!pCT){

    h1[21]->Fill(X);
    h1[22]->Fill(Y);
    h1[23]->Fill(xslope);
    h1[24]->Fill(yslope);

    }

    if(ScAngle>2.){

    h1[30]->Fill(bmom);
    h1[31]->Fill(X);
    h1[32]->Fill(Y);
    h1[33]->Fill(xslope);
    h1[34]->Fill(yslope);

    }    

    if(Q2>1.){

    h1[130]->Fill(bmom);
    h1[131]->Fill(X);
    h1[132]->Fill(Y);
    h1[133]->Fill(xslope);
    h1[134]->Fill(yslope);

    }  


    if(ScAngle>3.){
    h1[40]->Fill(bmom);
    h1[41]->Fill(X);
    h1[42]->Fill(Y);
    h1[43]->Fill(xslope);
    h1[44]->Fill(yslope);

    }

    if(ScAngle>4.){

    h1[50]->Fill(bmom);
    h1[51]->Fill(X);
    h1[52]->Fill(Y);
    h1[53]->Fill(xslope);
    h1[54]->Fill(yslope);

    }


     p[0]->Fill(ScAngle,bmom);     
     p[1]->Fill(ScAngle,X);
     p[2]->Fill(ScAngle,Y);
     p[3]->Fill(ScAngle,xslope);
     p[4]->Fill(ScAngle,yslope);



    if(ICT){

    h1[60]->Fill(bmom);
    h1[61]->Fill(X);
    h1[62]->Fill(Y);
    h1[63]->Fill(xslope);
    h1[64]->Fill(yslope);

    }


    if(LCT){

    h1[70]->Fill(bmom);
    h1[71]->Fill(X);
    h1[72]->Fill(Y);
    h1[73]->Fill(xslope);
    h1[74]->Fill(yslope);

    }

    if(MT || MCT){

    h1[80]->Fill(bmom);
    h1[81]->Fill(X);
    h1[82]->Fill(Y);
    h1[83]->Fill(xslope);
    h1[84]->Fill(yslope);

    }

    if(OT){

    h1[90]->Fill(bmom);
    h1[91]->Fill(X);
    h1[92]->Fill(Y);
    h1[93]->Fill(xslope);
    h1[94]->Fill(yslope);

    }

    if(pCT){

    h1[120]->Fill(bmom);
    h1[121]->Fill(X);
    h1[122]->Fill(Y);
    h1[123]->Fill(xslope);
    h1[124]->Fill(yslope);

    }


      }  //isAbeam
     

    if(BT){

    h1[100]->Fill(bmom);
    h1[101]->Fill(X);
    h1[102]->Fill(Y);
    h1[103]->Fill(xslope);
    h1[104]->Fill(yslope);

    }

    if(RT){

    h1[110]->Fill(bmom);
    h1[111]->Fill(X);
    h1[112]->Fill(Y);
    h1[113]->Fill(xslope);
    h1[114]->Fill(yslope);

    }


    } // bmom


  }

}

