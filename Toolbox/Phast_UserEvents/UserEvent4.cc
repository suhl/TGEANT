
#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TH3D.h"
#include "TTree.h"
#include "TProfile.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "TMath.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaParticle.h"
#include "PaAlgo.h"
#include "G3part.h"
#include "PaMCvertex.h"
#include "PaMCtrack.h"
#include "Photon_Cluster.h"

vector<int> ecal[3];
TH1D* h[100];
TH2D* HistTwo[100];
TH3D* HistThree[100];

double threshold[3];
int counter;

/*** ECAL NOISE ANALYSIS ***/
void UserEvent4 ( PaEvent& e )      // shortcut e = PaEvent
{
    static TTree* tree;
    static Photon_Cluster* photon_cluster;

    const double M_mu = G3partMass[5];  // muon   mass
    const double M_pi = G3partMass[8];  // pion   mass
    const double M_Pr = G3partMass[14]; // proton mass
    const double M_e  = G3partMass[2];  // electron mass


    static int physbits[32];
    for ( int ib=0 ; ib<32 ; ib++ ) {
        physbits[ib]=0;
    }
    physbits[0]=1;  // ICT
    physbits[1]=1;  // MCT
    physbits[2]=1;  // LCT
    physbits[3]=1;  // OT
    physbits[4]=1;  // CT
    physbits[7]=1;  // beam
    physbits[8]=1;  // MT/2
    physbits[10]=1; // J/Psi
    physbits[11]=1; // random
    static float refpos0 = 0.;
    const PaSetup& setup = PaSetup::Ref();

    static bool first ( true );
    if ( first ) {
        counter = 0;
        h[0] = new TH1D ( "ECAL0energy","ECAL0 -- energy",300,0,9 );
        h[1] = new TH1D ( "ECAL1energy","ECAL1 -- energy",300,0,9 );
        h[2] = new TH1D ( "ECAL2energy","ECAL2 -- energy",300,0,9 );

        h[3] = new TH1D ( "ECAL0time","ECAL0 -- time",120,-60,60 );
        h[4] = new TH1D ( "ECAL1time","ECAL1 -- time",120,-60,60 );
        h[5] = new TH1D ( "ECAL2time","ECAL2 -- time",120,-60,60 );

        h[6] = new TH1D ( "ECAL0Cells","ECAL0 -- hit cells",20,0,20 );
        h[7] = new TH1D ( "ECAL1Cells","ECAL1 -- hit cells",20,0,20 );
        h[8] = new TH1D ( "ECAL2Cells","ECAL2 -- hit cells",20,0,20 );

        h[9] = new TH1D ( "ECAL0ClusterSize","ECAL0 -- ClusterSize",10,0,10 );
        h[10] = new TH1D ( "ECAL1ClusterSize","ECAL1 -- ClusterSize",10,0,10 );
        h[11] = new TH1D ( "ECAL2ClusterSize","ECAL2 -- ClusterSize",10,0,10 );



        h[25] = new TH1D ( "TriggerCounter","TriggerCounter -1 = all, 1 = RT",10,-5,5 );


        HistTwo[0] = new TH2D ( "ECAL0 distr","ECAL0 distr",200,-200,200,200,-200,200 );
        HistTwo[1] = new TH2D ( "ECAL1 distr","ECAL1 distr",800,-400,400,200,-200,200 );
        HistTwo[2] = new TH2D ( "ECAL2 distr","ECAL2 distr",200,-200,200,200,-200,200 );

        HistTwo[3] = new TH2D ( "ECAL0_modnum_vs_e","ECAL0module_number vs energy",200,0,10,510,2000,2510 );
        HistTwo[4] = new TH2D ( "ECAL1_modnum_vs_e","ECAL1module_number vs energy",200,0,10,1725,6900,8625 );
        HistTwo[5] = new TH2D ( "ECAL2_modnum_vs_e","ECAL2module_number vs energy",200,0,10,3003,3800,6803 );


//         HistTwo[1] = new TH2D("ECAL1 distr","ECAL1 distr",800,-400,400,200,-200,200);
//         HistTwo[2] = new TH2D("ECAL2 distr","ECAL2 distr",200,-200,200,200,-200,200);
//



        HistThree[0] = new TH3D ( "ECAL0 distr vs e","ECAL0 distr vs e",200,-200,200,200,-200,200,200,0,10 );
        HistThree[1] = new TH3D ( "ECAL1 distr vs e","ECAL1 distr vs e",800,-400,400,200,-200,200,200,0,10 );
        HistThree[2] = new TH3D ( "ECAL2 distr vs e","ECAL2 distr vs e",200,-200,200,200,-200,200,200,0,10 );
        HistThree[3] = new TH3D ( "ECAL0 distr vs t","ECAL0 distr vs t",200,-200,200,200,-200,200,120,-60,60 );
        HistThree[4] = new TH3D ( "ECAL1 distr vs t","ECAL1 distr vs t",800,-400,400,200,-200,200,120,-60,60 );
        HistThree[5] = new TH3D ( "ECAL2 distr vs t","ECAL2 distr vs t",200,-200,200,200,-200,200,120,-60,60 );


        threshold[0] = 4.;
        threshold[1] = 5.;
        threshold[2] = 10.;

        first=false;
        cout << endl << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << "-------------------------------- UserEvent4 called ----------------------------" << endl;
        cout << "Ecal Noise Analysis" << endl;
        cout << "-------------------------------------------------------------------------------" << endl;
        cout << endl << endl;
        first = false;

        tree = new TTree ( "CLUSTER", "Ecal noise cluster" );
        photon_cluster = new Photon_Cluster();
        tree->Branch ( "UniqueEvNum", &photon_cluster->UniqueEvNum, "UniqueEvNum/L" );
        tree->Branch ( "vertex", &photon_cluster->vertex, "vertex[3]/D" );
        tree->Branch ( "momentum", &photon_cluster->momentum, "momentum[3]/D" );
        tree->Branch ( "caloNumber", &photon_cluster->caloNumber, "caloNumber/I" );
        tree->Branch ( "clusterPos", &photon_cluster->clusterPos, "clusterPos[3]/D" );
        tree->Branch ( "energy", &photon_cluster->energy, "energy/D" );
        tree->Branch ( "eCellSum", &photon_cluster->eCellSum, "eCellSum/D" );
        tree->Branch ( "clusterSize", &photon_cluster->clusterSize, "clusterSize/I" );
        tree->Branch ( "nCells", &photon_cluster->nCells, "nCells/I" );
        tree->Branch ( "cellNumbers", &photon_cluster->cellNumbers, "cellNumbers[100]/I" );
        tree->Branch ( "cellEnergy", &photon_cluster->cellEnergy, "cellEnergy[100]/D" );
        tree->Branch ( "iCell", &photon_cluster->iCell, "iCell/I" );
        tree->Branch ( "iCell_xc", &photon_cluster->iCell_xc, "iCell_xc/D" );
        tree->Branch ( "iCell_yc", &photon_cluster->iCell_yc, "iCell_yc/D" );

        photon_cluster->vertex[0] = 0;
        photon_cluster->vertex[1] = 0;
        photon_cluster->vertex[2] = 0;
        photon_cluster->momentum[0] = 0;
        photon_cluster->momentum[1] = 0;
        photon_cluster->momentum[2] = 0;


    } // end of histogram booking


    int TMaskRaw = e.TrigMask();

    int TMask = 0;
    int tbits[32];
    int nset=0;
    for ( int ib=0 ; ib<32 ; ib++ ) {
        tbits[ib]= ( TMaskRaw>>ib ) &1;
// 	cout << " bet set: " << ib << " to " << tbits[ib] << endl;
        if ( tbits[ib]!=0 && physbits[ib]!=0 ) {
            nset+=1;
            TMask+=pow ( 2.,ib );
        }
    }

    bool RT = false;

    h[25]->Fill ( -1 );

    if ( ( TMask>>10 ) &1 ) {
        RT = true;
    }

    if ( !RT ) {
        return;
    }

    if ( RT ) {
        h[25]->Fill ( 1 );
//         for ( int part = 0; part < e.NParticle(); part++ ) {
//             if ( e.vParticle ( part ).IsBeam() ) {
//                 return;
//             }
//         }
        h[25]->Fill ( 2 );
    }


    if ( RT ) {
        photon_cluster->UniqueEvNum = e.UniqueEvNum();
        e.TagToSave();
//       cout << "RandomTrigger " << endl;
        counter++;
        ecal[0].clear();
        ecal[1].clear();
        ecal[2].clear();

        if ( e.NCaloClus() == 0 ) {
            return;
        }

        for ( int i = 0; i < e.NCaloClus(); i++ ) { // loop over calo clusters
            const PaCaloClus& cl = e.vCaloClus ( i );
            int myCalo = cl.iCalorim();

            if ( myCalo != 0 && myCalo != 1 && myCalo != 2 ) {
                continue;
            }

            ecal[myCalo].push_back ( i );
// 	    cout << "clust0r" <<endl;
        }

        for ( unsigned int i = 0; i < 3; i++ ) {
            for ( unsigned int j = 0; j < ecal[i].size(); j++ ) {
                const PaCaloClus& cl = e.vCaloClus ( ecal[i].at ( j ) );
// 		cout << "loopclus" << endl;


                bool cl_charged=false;
                for ( int pars = 0; pars < cl.NParticles(); pars++ ) {
                    if ( e.vParticle ( cl.iParticle ( pars ) ).iTrack() != -1 ) {
                        cl_charged = true;
                        break;
                    }
                }

                if ( cl_charged ) {
                    continue;
                }
                if ( cl.vCellNumber().size() != 1 || cl.Size() != 1 ) {
                    continue;
                }
                bool leaked = false;
		
                photon_cluster->fillCluster ( &cl );
//                 //check for leak in hcal
                 if ( i == 2 && cl.E() > 1.5 ) {
                     for ( int hc = 0; hc < e.NCaloClus(); hc++ ) { // loop over calo clusters
                         const PaCaloClus& cl2 = e.vCaloClus ( hc );
                         int myHCalo = cl2.iCalorim();
 // 			cout <<myHCalo << " name " << PaSetup::Ref().Calorimeter(myHCalo).Name() << endl;
                         if ( myHCalo != 4 ) {
                             continue;
                         }
                         if ( cl2.E() > 10 ) {
                             continue;
                         }
 			if (cl2.E() < 0.5)
 			  continue;
 			if (cl2.E() < cl.E())
 			  continue;
                        double r = sqrt ( ( pow ( cl.X() - cl2.X(),2.0 ) +pow ( cl.Y() - cl2.Y(),2.0 ) ) );
                         if ( r < 40 ) {
                             leaked = true;
			     photon_cluster->setLeaked(cl2.E());
//  			    cout << "removed leaked cluster " << cl.E() << " against energy: " << cl2.E() <<endl;
                         }
 
                     }
                 }
//                  if ( leaked ) {
//                     continue;
//  we do not want to not keep it, just for information - removing leaking doesnt work anyway		   
//                  }

                tree->Fill();

// 		cout << " fill"  <<endl;
                h[i]->Fill ( cl.E() );
                h[3+i]->Fill ( cl.Time() );
                h[6+i]->Fill ( cl.vCellNumber().size() );
                h[9+i]->Fill ( cl.Size() );
                HistTwo[i]->Fill ( cl.X(),cl.Y() );
                HistThree[i]->Fill ( cl.X(),cl.Y(),cl.E() );
                HistThree[i+3]->Fill ( cl.X(),cl.Y(),cl.Time() );
                double x=cl.X();
                double y=cl.Y();
                HistTwo[i+3]->Fill ( cl.E(),cl.iCell ( x,y ) );
                h[25]->Fill ( i+6 );
            }
        }

    }



}
