#ifndef TOOLBOX_HH
#define TOOLBOX_HH

#include <iostream>
#include <string>
#include <vector>
#include <map>


#include <csignal>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>

#include "T4OutputASCII.hh"
#include "T4Event.hh"
#include "T4SGlobals.hh"

#include "T4SStructManager.hh"
#include "T4SMessenger.hh"

// basic root includes
#include "TCanvas.h"
#include "TGraph.h"
#include "TH2D.h"
#include "TH1D.h"
#include "TFile.h"
#include "TStyle.h"
#include "TMath.h"
#include "TLegend.h"
#include "TLine.h"
#include "TTree.h"
#include "TBranch.h" 
#include "TLorentzVector.h" 

/*! \class ToolboxPlugin
 *  \brief The base class for toolbox analysis plugins.
 *  This class is the mother class of all analysis plugins for the TGEANT-Toolbox.
 *  In order to create your own, you need to derive from this class and reimplement the virtuals
 */
class ToolboxPlugin
{
public:
   /*! \brief The default constructor
    * Also sets some default styles for ROOT histograms
    */
    ToolboxPlugin(void)
    {
        gStyle->SetLabelSize(0.06, "xy");
        gStyle->SetTitleSize(0.06, "xy");
        gStyle->SetTitleOffset(0.75, "xy");
        gStyle->SetLabelOffset(0., "X");
        gStyle->SetLabelOffset(0.01, "Y");

        gStyle->SetHistLineColor(1);
        gStyle->SetHistLineWidth(2);
        gStyle->SetHistFillColor(5);
        gStyle->SetHistFillStyle(1001);
    }

    virtual ~ToolboxPlugin(void) {
      outFile->Flush();
      outFile->Close();
    }
    /*! \brief returns the plugin name for the list of available plugins */
    std::string getName() {
        return myName;
    }
    /*! \brief returns the plugin description 
     * You need to implement this! Do describe your plugin in one or two sentences here
     */
    virtual std::string getDescription(void)=0;
    
    /*! \brief Gets called on each event  
     * This function gets called by the toolbox each event - similar to the function "UserEvent(PaEvent &e)" of PHAST
     * You need to implement this!
     */
    virtual bool processEvent(T4Event* event)=0;
    /*! \brief This function gets called once at the end of all events.
     * You need to implement this!
     * Maybe save your histograms or stuff or error calculation or whatever. Its the pendant of "UserJobEnd"
     */
    virtual void endOfEvents(void) {}
    /*! \brief This function is called once before the event loop begins
     * You need to implement this!
     * This is the place to create histograms and initialize stuff 
     */
    virtual void beginOfEvents(void) {}

    /*! \brief Creates a TFile to save all histograms
     * This is called when the user runs the Toolbox with '-h /path/to/outfile.root' option
     */
    void createOutFile(std::string name) {
      // create a ROOT TFile to save all histograms
      // the output path can be specified with the '-h /path/to/outfile.root' option
      if (name == "NA")
        outPath = myName + ".root";
      else
        outPath = name;

      outFile = new TFile(outPath.c_str(), "recreate");
    }

protected:
    std::string myName;
    

    TFile* outFile;
    string outPath;

    /*! \brief Bins a histogram logarithmic
     */ 
    static void binLogX(TH1* h)
    {
        TAxis *axis = h->GetXaxis();
        int bins = axis->GetNbins();

        Axis_t from = axis->GetXmin();
        Axis_t to = axis->GetXmax();
        Axis_t width = (to - from) / bins;
        Axis_t *new_bins = new Axis_t[bins + 1];

        for (int i = 0; i <= bins; i++) {
            new_bins[i] = TMath::Power(10, from + i * width);
        }
        axis->Set(bins, new_bins);
        delete new_bins;
    }
    
    static void binLogX_Special(TH1* h, double min, double max)
    {
        TAxis *axis = h->GetXaxis();
        int bins = axis->GetNbins();

        Axis_t from = TMath::Log10(min);
        Axis_t to = TMath::Log10(max);
        Axis_t width = (to - from) / (bins-2);
        Axis_t *new_bins = new Axis_t[bins + 1];

	new_bins[0] = axis->GetXmin();
        for (int i = 0; i <= bins-2; i++) {
            new_bins[i+1] = TMath::Power(10, from + i * width);
        }
        new_bins[bins] = axis->GetXmax();
        axis->Set(bins, new_bins);
        delete new_bins;
    }
    
    static void binSquaredQ2(TH1* h)
    {
      TAxis *axis = h->GetXaxis();
      int bins = axis->GetNbins();
      double max = axis->GetXmax();
      Axis_t *new_bins = new Axis_t[bins + 1];

      for (int i = 0; i <= bins; i++) {
        new_bins[i] = max * 1e-2 * pow(10., 2. * (1. / bins) * i);
      }
      axis->Set(bins, new_bins);
      delete new_bins;
    }

    void saveHist(TH1* hist)
    {
      hist->SaveAs(((string) hist->GetName() + ".root").c_str());
    }
};


class pluginList
{
public:
    ~pluginList() {};
    static pluginList* getInstance() {
        if (instance == NULL)
            instance = new pluginList();
        return instance;

    };
    std::vector<ToolboxPlugin*> activated_classes;


private:
    static pluginList* instance;
    pluginList() {
    };



};




#endif
