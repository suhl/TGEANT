#include <iostream>

#include "T4SStructManager.hh"
#include "T4SCalorimeter.hh"
#include "T4SEfficiency.hh"



int main(int argc,char** argv){
  
  if (argc < 4){
    printf("Usage: %s [detDat] [newCaloCalib.xml] [newEffic.xml] \n",argv[0]);
    return -1;
  }
  
  T4SStructManager myMan;
  T4SCalorimeter myCaloCalib(&myMan);
  
  printf("Setting default calo calibration values from dvcs2012\n");
  string oldCalib = getenv("TGEANT");
  oldCalib+= "/resources/dvcs_2012/calorimeter_dvcs2012.xml";
  myCaloCalib.readXML(oldCalib);
  printf("Reading Calos from DetDat %s\n",argv[1]);
  myCaloCalib.importCalosFromDetDat(argv[1]);  
  printf("Writing Calo-XML %s\n",argv[2]);
  myCaloCalib.writeXML(argv[2]);
  
  printf("Making a new effic!\n");
  T4SEfficiency myEffic(&myMan);
  myEffic.importEfficiencyFromDetDat(argv[1]);
  printf("Writing effic.xml: %s\n",argv[3]);
  myEffic.writeXML(argv[3]); 
}