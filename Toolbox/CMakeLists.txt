message("---------------------------")
message("Preparing Toolbox binary")
message("---------------------------")

find_package (Xerces REQUIRED) 
find_package (CLHEP REQUIRED)
find_package (ROOT REQUIRED)

if (${TGEANT_ENABLE_SQLITE} MATCHES "YES")
  find_package (SQLite3 REQUIRED)
  set(USE_SQLITE 1)
  include_directories(${SQLITE3_INCLUDE_DIRS})
endif(${TGEANT_ENABLE_SQLITE} MATCHES "YES")

include_directories (${XERCESC_INCLUDE})
include_directories (${CLHEP_INCLUDE_DIRS})
include_directories (${ROOT_INCLUDE_DIR})
include_directories (${CMAKE_SOURCE_DIR}/TGEANT/general/include)
include_directories (${CMAKE_SOURCE_DIR}/libSettings/include)
include_directories (${CMAKE_SOURCE_DIR}/libEvent/include)
include_directories (${CMAKE_SOURCE_DIR}/Toolbox/)
if (${USE_HEPGEN})
  include_directories(${CMAKE_SOURCE_DIR}/HEPGenPlusPlus/libhepgen/)
endif(${USE_HEPGEN})
# Get all source files
FILE(GLOB_RECURSE TOOLBOX_SRC
    "${CMAKE_SOURCE_DIR}/Toolbox/user/*.cc"
)

# Remove cmake auto generated stuff from makefile
string(REGEX REPLACE "CMakeFiles/[^;]+;?" "" TOOLBOX_SRC "${TOOLBOX_SRC}") 


#Make efficiencyDBManager
if (USE_SQLITE)
  add_executable(efficienciesDB efficienciesDB.cc)
  TARGET_LINK_LIBRARIES(efficienciesDB T4Event T4Settings ${SQLITE3_LIBRARIES} ${ROOT_LIBRARIES})
  install (TARGETS efficienciesDB RUNTIME DESTINATION bin)
  install (FILES dbsplitter.py DESTINATION bin)
  install (FILES dbcreate.py DESTINATION bin)
  install (FILES efficConverter.py DESTINATION bin)
endif(USE_SQLITE)


# Make Toolbox
add_executable(toolbox toolbox.cc ${TOOLBOX_SRC})

TARGET_LINK_LIBRARIES(toolbox T4Settings)
TARGET_LINK_LIBRARIES(toolbox ${XERCESC_LIBRARY})
TARGET_LINK_LIBRARIES(toolbox T4Event)
TARGET_LINK_LIBRARIES(toolbox ${ROOT_LIBRARIES})
TARGET_LINK_LIBRARIES(toolbox ${CLHEP_LIBRARIES})
if (${USE_HEPGEN})
  TARGET_LINK_LIBRARIES(toolbox hepgen)
endif(${USE_HEPGEN})


# Make the geometry pre loader
add_executable(geomPreLoad geomPreLoad.cc)
TARGET_LINK_LIBRARIES(geomPreLoad ${ROOT_LIBRARIES} Geom T4Settings)

# Make detDatImport
add_executable(detDatImport detDatImport.cc)
TARGET_LINK_LIBRARIES(detDatImport T4Settings)
TARGET_LINK_LIBRARIES(detDatImport ${XERCESC_LIBRARY})
TARGET_LINK_LIBRARIES(detDatImport T4Event)

# Make ecalNoiseAnal
add_executable(ecalNoiseAnal ecalNoiseTool.cpp)
TARGET_LINK_LIBRARIES(ecalNoiseAnal ${ROOT_LIBRARIES})

# Make ecalNoisePostProcessing
add_executable(ecalNoisePostProc ecalNoiseIntegrator.cpp)
TARGET_LINK_LIBRARIES(ecalNoisePostProc ${ROOT_LIBRARIES} T4Settings)



# Make install
install (TARGETS toolbox RUNTIME DESTINATION bin)
install (TARGETS geomPreLoad RUNTIME DESTINATION bin)
install (TARGETS detDatImport RUNTIME DESTINATION bin)
install (TARGETS ecalNoiseAnal RUNTIME DESTINATION bin)
install (TARGETS ecalNoisePostProc RUNTIME DESTINATION bin)


add_subdirectory(ecalNoiseUserEvents)