#!/usr/bin/python3
import sqlite3
import argparse


def main():
  parser = argparse.ArgumentParser(description='Splitter for sqlite databases for 2d efficiencies TGEANT by year')
  parser.add_argument("input_db")
  parser.add_argument("output_db")
  parser.add_argument("year_to_split")
  args=parser.parse_args()
  
  print("Running YearSplitter")
  
  conn = sqlite3.connect(args.input_db)
  connOut = sqlite3.connect(args.output_db)
  c = conn.cursor()
  cOut = connOut.cursor()

  sql_create="""CREATE TABLE \"efficiencies\" ( 
	`ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, \
	`TBNAME`	TEXT,
	`DETNAME`	TEXT,
	`UNIT`	INTEGER,
	`YEAR`	TEXT DEFAULT 0,
	`binsX`	INTEGER DEFAULT 0,
	`binsY`	INTEGER DEFAULT 0,
	`startX`	REAL DEFAULT 0.0,
	`startY`	REAL DEFAULT 0.0,
	`endX`	REAL,
	`endY`	REAL,
	`efficMean`	REAL,
	`efficValues`	TEXT
  )"""
  
  print("Creating new Database structure!")
  #We commit the creation first, the next sqls can be buffered, but this needs to be okay first!
  cOut.execute(sql_create)
  connOut.commit()
  
  #select the rows to insert into the new database
  c.execute('SELECT TBNAME,DETNAME,UNIT,YEAR,binsX,binsY,startX,startY,endX,endY,efficMean,efficValues FROM efficiencies WHERE year="'+args.year_to_split+'"')
  all_rows = c.fetchall()
  cOut.executemany("INSERT INTO efficiencies "  "(TBNAME,DETNAME,UNIT,YEAR,binsX,binsY,startX,startY,endX,endY,efficMean,efficValues) "  
              "VALUES (?,?,?,?,?,?,?,?,?,?,?,?);"
              ,all_rows)
  
  connOut.commit()
  connOut.close()


if __name__ == "__main__":
  main()

