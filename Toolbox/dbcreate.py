#!/usr/bin/python3
import sqlite3
import argparse


def main():
  parser = argparse.ArgumentParser(description='Creator for empty 2d effic databases')
  parser.add_argument("fileToWrite")
  args=parser.parse_args()
  
  print("Running DBCreator")
  
  conn = sqlite3.connect(args.fileToWrite)
  c = conn.cursor()

  sql_create="""CREATE TABLE \"efficiencies\" ( 
	`ID`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, \
	`TBNAME`	TEXT,
	`DETNAME`	TEXT,
	`UNIT`	INTEGER,
	`YEAR`	TEXT DEFAULT 0,
	`binsX`	INTEGER DEFAULT 0,
	`binsY`	INTEGER DEFAULT 0,
	`startX`	REAL DEFAULT 0.0,
	`startY`	REAL DEFAULT 0.0,
	`endX`	REAL,
	`endY`	REAL,
	`efficMean`	REAL,
	`efficValues`	TEXT
  )"""
  
  print("Creating new Database structure!")
  c.execute(sql_create)
  conn.commit()
  conn.close()
  print("Done!")

if __name__ == "__main__":
  main()

