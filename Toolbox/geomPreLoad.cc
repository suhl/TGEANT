#include <iostream>
#include <TRint.h>
#include <TROOT.h>
#include <TGeoManager.h>
#include <T4SMessenger.hh>

using namespace std;

int main (int argc, char **argv)
{ 
  
    //Make gROOT available
    //TRint *theApp = new TRint("ROOT example", &argc, argv);
    int major,minor,patchset;
    
    int scannedArgs=sscanf(gROOT->GetVersion(),"%d.%d/%d",&major,&minor,&patchset);
    if (scannedArgs < 3){
      T4SMessenger::getInstance()->printfMessage(T4SWarning,__LINE__,__FILE__,"Could not parse the ROOT version(%s)! Found only %i arguments!\n",gROOT->GetVersion(),scannedArgs);
    }
    T4SMessenger::getInstance()->printfMessage(T4SNotice,__LINE__,__FILE__,"Found ROOT version: %s, parsing to major: %i, minor %i, patchset: %i\n",gROOT->GetVersion(),major,minor,patchset);   

    if (major == 5 && minor==34 && (patchset> 15 && patchset < 25)){
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal,__LINE__,__FILE__,"You are using a ROOT version that has bugs in its GDML implementation (see: https://root.cern.ch/phpBB3/viewtopic.php?f=3&t=19271)");
      T4SMessenger::getInstance()->printMessage(T4SFatalError,__LINE__,__FILE__,"Please use ROOT 5.34/25 or newer!\n");
    }
    
    
  
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__, "TGEANT's Geometry-Pre-Loader! Usage: geomPreLoad [input.gdml] [output.root]");
    if (argc < 3)
        T4SMessenger::getInstance()->printMessage(T4SFatalError,__LINE__,__FILE__, "As I said.... use me like: geomPreLoad [input.gdml] [output.root] ");

    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__, "Importing the GDML now! Please be patient! Can take a few minutes!");
    TGeoManager* myGeo = new TGeoManager();
    myGeo->Import(argv[1]);
    T4SMessenger::getInstance()->printMessage(T4SNotice,__LINE__,__FILE__, "Import complete! Now saving it again!");
    myGeo->Export(argv[2]);
}
