// Std includes and cstring helpers
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <vector>

//TGEANT includes
#include "T4SGlobals.hh"
#include "T4SDetectorsDat.hh"
#include "T4Event.hh"
#include "T4EfficDatabase.hh"

//sqlite3 includes
#include <sqlite3.h>
#include <root/TFile.h>
#include <root/TH2D.h>

//ROOT includes
#include <TCanvas.h>
#include <TFile.h>
#include <TH2D.h>
#include <TH2.h>
#include <TH1.h>
#include <TStyle.h>

using namespace std;

int printOpt ( void );

void searchTBName ( bool withYear = false );
void searchYear();
void searchSimple();
void addEntry();
void addEntry ( string _tbname, string _detname, string _unitnumber, string _year, string _pathToHisto );
void createTGA ( string _tbname, string _detname, string _unitnumber, string _year, string _tbFile);
void importTGA ( string _tbname, string _detname, string _unitnumber, string _year, string _pathToTGA);
void importBlob (string _tbname, string _detname, string _unitnumber, string _year, string _pathToBlob);

void detDatImportSecret ( string _detDatPath, string _year );
void testImplementation();
void makeBlob();

void plotDB();

sqlite3 *db;
char *zErrMsg = 0;
int  rc;
string sql;
string dbName;

static int callback ( void *data, int argc, char **argv, char **azColName ) {
    int i;
    fprintf ( stderr, "%s: ", ( const char* ) data );
    for ( i = 0; i < argc; i++ ) {
        printf ( "%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL" );
    }
    printf ( "\n" );
    return 0;
}



int main ( int argc, char **argv ) {

    string dbname;

    cout << "argc = " << argc << endl;

    if ( argc < 2 ) {
        printf ( "Usage: %s [dbfile]\n", argv[0] );
        printf ( "Or direct ROOT Import: %s [dbfile] [TBNAME] [DETNAME] [UNIT] [YEAR] [/Path/to/ROOTHISTO.root]\n", argv[0] );
        printf ( "Or direct TGA Import: %s [dbfile] [TBNAME] [DETNAME] [UNIT] [YEAR] [/Path/to/TGA.tga] --tgaImport\n",argv[0]);
        printf ( "Or direct blob.txt Import: %s [dbfile] [TBNAME] [DETNAME] [UNIT] [YEAR] [/path/to/blob.txt --blobImport\n",argv[0]);
        printf ( "Or makeTGA: %s [dbfile] [TBNAME] [DETNAME] [UNIT] [YEAR] --tgaExport\n",argv[0]);
        printf ( "Or initialize with det.dat values: %s [dbfile] [/path/to/det.dat] [yearFlag]\n",argv[0]);
        exit ( 0 );
    }
    dbName = argv[1];


    /* Open database */
    rc = sqlite3_open ( argv[1], &db );
    if ( rc ) {
        fprintf ( stderr, "Can't open database: %s\n", sqlite3_errmsg ( db ) );
        exit ( 0 );
    }
    else {
        fprintf ( stdout, "Opened database successfully\n" );
    }


    if ( argc == 7 ) {
        if (strcmp(argv[argc-1],"--tgaExport")==0)
            createTGA(argv[2],argv[3],argv[4],argv[5],argv[1]);
        else

            addEntry ( string(argv[2]), string(argv[3]), string(argv[4]), string(argv[5]), string(argv[6]) );
        exit ( 0 );
    }
    if ( argc == 4 ) {
        string check;
        printf ( "Warning! Secret Det.dat importer feature activated! DO NOT USE THIS IF YOU DONT KNOW WHAT THIS IS! \n Enter 42 to continue!\n" );
        cin >> check;
        if ( check != "42" )
            exit ( 0 );
        detDatImportSecret ( argv[2], argv[3] );
        exit ( 0 );
    }

    if (argc == 8) {
        if (strcmp(argv[argc-1],"--tgaImport")==0)
            importTGA(argv[2],argv[3],argv[4],argv[5],argv[6]);
        else if (strcmp(argv[argc-1],"--blobImport")==0)
            importBlob(argv[2],argv[3],argv[4],argv[5],argv[6]);
        exit (0);
    }
    /* print menu and get option */
    int options = printOpt();
    while ( options != 5 ) {
        switch ( options ) {
        case 1:
            searchTBName();
            break;
        case 2:
            searchTBName ( true );
            break;
        case 3:
            searchYear();
            break;
        case 4:
            addEntry();
            break;
        case 5:
            break;
        case 6:
            searchSimple();
            break;
        case 7:
            testImplementation();
            break;
        case 8:
            makeBlob();
            break;
        case 9:
            plotDB();
            break;
        }
        options = printOpt();
    }

    sqlite3_close ( db );
}




int printOpt ( void ) {
    printf ( "-----------------Menu---------------\n" );
    printf ( "1. Search for TBNAME\n" );
    printf ( "2. Search for TBNAME and YEAR\n" );
    printf ( "3. Search for YEAR\n" );
    printf ( "4. Add a new Entry\n" );
    printf ( "5. Exit\n" );
    printf ( "6. Print detector without 2D-Efficiencies\n" );
    printf ( "7. Show an efficiency graphic of a detector\n" );
    printf ( "8. Make BLOB from TGA\n" );
    printf ( "9. Plot a whole database\n");
    int retVal;
    cin >> retVal;
    return retVal;
}

void searchTBName ( bool withYear ) {
    string tbname;
    printf ( "Enter TBName\n" );
    cin >> tbname;


    /* Create SQL statement */
    string sql = ( string ) "SELECT * FROM efficiencies WHERE TBNAME=\"" + tbname + "\"";
    if ( withYear ) {
        printf ( "Enter Year\n" );
        string year;
        cin >> year;
        sql += ( string ) " AND YEAR=" + year + "";
    }
    const char* data = "Callback for TBNAME query: ";


    /* Execute SQL statement */
    rc = sqlite3_exec ( db, sql.c_str(), callback, ( void* ) data, &zErrMsg );
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else {
        fprintf ( stdout, "Operation done successfully -- Results should be above!\n" );
    }

}

void addEntry() {
    string tbname, detname, unit, year, pathToHist;
    printf ( "Enter TBNAME\n" );
    cin >> tbname;
    printf ( "Enter DETNAME\n" );
    cin >> detname;
    printf ( "Enter UNIT number\n" );
    cin >> unit;
    printf ( "Enter YEAR\n" );
    cin >> year;
    printf ( "Path to ROOT histogram\n" );
    cin >> pathToHist;
    addEntry(tbname,detname,unit,year,pathToHist);
}

void searchYear() {
    printf ( "Enter Year\n" );
    string year;
    cin >> year;

    string sql = ( string ) "SELECT * FROM efficiencies WHERE YEAR=\n" + year + "\n";
    const char* data = "Result for YEAR: ";


    /* Execute SQL statement */
    rc = sqlite3_exec ( db, sql.c_str(), callback, ( void* ) data, &zErrMsg );
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else {
        fprintf ( stdout, "Operation done successfully -- Results should be above!\n" );
    }
}

void importBlob(string _tbname, string _detname, string _unitnumber, string _year, string _pathToBlob)
{
    ifstream file(_pathToBlob.c_str());
    stringstream theBlob;
    if ( file ) {
        theBlob << file.rdbuf();
        file.close();
    }
    else{
      cout << "Fatal, could not open file " << _pathToBlob << endl;
      return;
    }


//     printf ( "Would import now: binsx %i, binsy %i, minx %f, miny %f, maxx %f, maxy %f, TBNAME %s, DETNAME %s, UNIT %s, Year %s \n", nbinX, nbinY, minx, miny, maxx, maxy, _tbname.c_str(), _detname.c_str(), _unitnumber.c_str(), _year.c_str() );
    cout  << theBlob.str() << endl;
    char* buffer = new char[123456789];
    sprintf(buffer,"UPDATE efficiencies SET efficValues=\"%s\" WHERE \
                  TBNAME=\"%s\" AND DETNAME=\"%s\" AND UNIT=%s AND YEAR=\"%s\";",theBlob.str().c_str(),_tbname.c_str(),_detname.c_str(),_unitnumber.c_str(),_year.c_str()
           );

    rc = sqlite3_exec ( db, buffer, callback, 0, &zErrMsg );
    int changedRows = sqlite3_changes(db);
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else if (changedRows > 0) {
        fprintf ( stdout, "...success - updated %i rows! \n", changedRows);
    }
    else if (changedRows == 0) {
        fprintf ( stdout, "...error - no rows changed! Check your input (TBNAME, DETNAME, UNIT, YEAR)!! \n" );
    }
    delete[] buffer;

}


void importTGA(string _tbname, string _detname, string _unitnumber, string _year, string _pathToTGA)
{

    double meanVal = 0.0;
    int meanCount = 0;

    uint16_t header[9];
    FILE* outPutImage = fopen(_pathToTGA.c_str(),"rb");
    if (outPutImage == NULL) {
        T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Could not open file %s for reading binary!\n",_pathToTGA.c_str());
        return;
    }
    printf("opened the file \n");
    //write the binary header
    fread(header,1,sizeof(header),outPutImage);
    int nBinsX = header[6];
    int nBinsY = header[7];

    printf("read header, binsize: %i %i \n",nBinsX,nBinsY);
    float dataVal[nBinsX][nBinsY];
    for (int i = 0; i < nBinsY; i++)
        for (int a = 0; a < nBinsX; a++) {
            uint8_t myVal;
            uint8_t myAlpha;
            fread(&myVal,sizeof(uint8_t),1,outPutImage);
            fread(&myAlpha,sizeof(uint8_t),1,outPutImage);
            float retVal;
            retVal = (float) myVal;
            retVal /= (pow(2.,8.)-1.);
            if (static_cast<uint16_t>(myAlpha)==0 && retVal != 0.0) {
                dataVal[a][i] = -1.0;
            }
            else
                dataVal[a][i] = retVal;
        }

    for (int i = 0; i < nBinsX; i++)
        for(int a = 0; a < nBinsY; a++) {
            if (dataVal[i][a] > 0) {
                meanCount ++;
                meanVal += dataVal[i][a];
            }
        }


    printf("Sum of Bins %.4e, bins counted %i -- MeanEffic: %.3e\n",meanVal,meanCount,meanVal/meanCount);
    meanVal /= meanCount;


    stringstream theBlob;
    for ( int x = 0; x < nBinsX; x++ ) {
        for ( int y = 0; y < nBinsY; y++ ) {
            theBlob << dataVal[x][y] << " ";

        }
        theBlob << "<br />";
    }

//     cout << theBlob.str() << endl;


    int nbinX, nbinY;
    double minx, miny, maxx, maxy;
    nbinX = nBinsX;
    nbinY = nBinsY;

    printf ( "Would import now: binsx %i, binsy %i, minx %f, miny %f, maxx %f, maxy %f, TBNAME %s, DETNAME %s, UNIT %s, Year %s \n", nbinX, nbinY, minx, miny, maxx, maxy, _tbname.c_str(), _detname.c_str(), _unitnumber.c_str(), _year.c_str() );
    char* buffer = new char[123456789];
    sprintf(buffer,"UPDATE efficiencies SET binsX=%i, binsY=%i, efficMean=%f, efficValues=\"%s\" WHERE \
                  TBNAME=\"%s\" AND DETNAME=\"%s\" AND UNIT=%s AND YEAR=\"%s\";",nbinX,nbinY,meanVal,theBlob.str().c_str(),_tbname.c_str(),_detname.c_str(),_unitnumber.c_str(),_year.c_str()
           );

    rc = sqlite3_exec ( db, buffer, callback, 0, &zErrMsg );
    int changedRows = sqlite3_changes(db);
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else if (changedRows > 0) {
        fprintf ( stdout, "...success - updated %i rows! \n", changedRows);
    }
    else if (changedRows == 0) {
        fprintf ( stdout, "...error - no rows changed! Check your input (TBNAME, DETNAME, UNIT, YEAR)!! \n" );
    }
    delete[] buffer;

}


void addEntry ( string _tbname, string _detname, string _unitnumber, string _year, string _pathToHisto ) {
    TFile* newFile = new TFile ( _pathToHisto.c_str(), "READ" );
    if ( !newFile->IsOpen() ) {
        printf ( "Could not open ROOT-File %s for reading! Aborting import\n", _pathToHisto.c_str() );
        return;
    }
    printf("File %s opened,\n... loading histogram: %s!\n",_pathToHisto.c_str(),newFile->GetListOfKeys()->First()->GetName());
    //now get first histo from TFile

    TH2D* myHist;
    newFile->GetObject ( newFile->GetListOfKeys()->First()->GetName(), myHist );
    if ( myHist == NULL ) {
        printf ( "Could not convert first entry to TH2D! seems something wrong with the rootfile! Aborting!\n" );
        return;
    }
    int nbinX, nbinY;
    double minx, miny, maxx, maxy;
    nbinX = myHist->GetNbinsX();
    nbinY = myHist->GetNbinsY();
    minx = myHist->GetXaxis()->GetBinLowEdge ( 1 );
    miny = myHist->GetYaxis()->GetBinLowEdge ( 1 );
    maxx = myHist->GetXaxis()->GetBinUpEdge ( nbinX );
    maxy = myHist->GetYaxis()->GetBinUpEdge ( nbinY );

    stringstream theBlob;
    double addition = 0.0;
    int meanCount = 0;
    for ( int x = 1; x <= nbinX; x++ ) {
        for ( int y = 1; y <= nbinY; y++ ) {
            theBlob << myHist->GetBinContent ( x, y ) << " ";
            if ( myHist->GetBinContent ( x, y ) > 0 ) {
                meanCount++;
                addition += myHist->GetBinContent ( x, y );
            }
        }
        theBlob << "<br />";
//     cout << "<br />" << endl;
    }
    addition /= ( double ) meanCount;

    printf ( "Would import now: binsx %i, binsy %i, minx %f, miny %f, maxx %f, maxy %f, TBNAME %s, DETNAME %s, UNIT %s, Year %s \n", nbinX, nbinY, minx, miny, maxx, maxy, _tbname.c_str(), _detname.c_str(), _unitnumber.c_str(), _year.c_str() );
    printf ( "MeanEffic %f\n", addition );
    char* buffer = new char[123456789];
    sprintf(buffer,"UPDATE efficiencies SET binsX=%i, binsY=%i, startX=%f, startY=%f, endX=%f, endY=%f, efficMean=%f, efficValues=\"%s\" WHERE \
                  TBNAME=\"%s\" AND DETNAME=\"%s\" AND UNIT=%s AND YEAR=\"%s\";",nbinX,nbinY,minx,miny,maxx,maxy,addition,theBlob.str().c_str(),_tbname.c_str(),_detname.c_str(),_unitnumber.c_str(),_year.c_str()
           );
//   cout << "--------------------" << endl;
//   cout << theBlob.str() << endl;
//   cout << "--------------------" << endl;
//
    rc = sqlite3_exec ( db, buffer, callback, 0, &zErrMsg );
    int changedRows = sqlite3_changes(db);
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else if (changedRows > 0) {
        fprintf ( stdout, "...success - updated %i rows! \n", changedRows);
    }
    else if (changedRows == 0) {
        fprintf ( stdout, "...error - no rows changed! Check your input (TBNAME, DETNAME, UNIT, YEAR)!! \n" );
    }
    delete[] buffer;

}

void detDatImportSecret ( string _detDatPath, string _year ) {
    printf ( "Starting secret Det.dat importer mode!\n" );
    ifstream detDatFile ( _detDatPath.c_str() );
    //go over the lines and parse each one
    while ( !detDatFile.eof()  && detDatFile.is_open() ) {
        string line;
        getline ( detDatFile, line );
        vector<string> boomList = explodeString ( line );
        if (boomList.size() < 18)
            continue;
        //it is a detector!
        if ( boomList.at ( 0 ) != "det" )
            continue;
        string tbname = boomList.at ( DETDAT_TBNAME );
        string detname = boomList.at ( DETDAT_DET );
        string unit = boomList.at ( DETDAT_UNIT );
        string effic = boomList.at ( DETDAT_EFFIC );
        string blob = effic + " <br />";

        printf ( "Putting default information for detector %s %s %s: %s\n", tbname.c_str(), detname.c_str(), unit.c_str(), effic.c_str() );


        /* Create SQL statement */
        sql = "INSERT INTO efficiencies (TBNAME,DETNAME,UNIT,YEAR,binsX,binsY,startX,startY,endX,endY,efficMean,efficValues) "  \
              "VALUES (\"" + tbname + "\",\"" + detname + "\"," + unit + ",\"" + _year + "\"," + "1,1,-10000,-10000,10000,10000," + effic + ",\"" + blob + "\");";

// 	  cout << "sql: " << sql << endl;
        /* Execute SQL statement */
        rc = sqlite3_exec ( db, sql.c_str(), callback, 0, &zErrMsg );
        if ( rc != SQLITE_OK ) {
            fprintf ( stderr, "SQL error: %s\n", zErrMsg );
            sqlite3_free ( zErrMsg );
        }
        else {
            fprintf ( stdout, "...success\n" );
        }
    }
}

void searchSimple() {
    printf ( "Enter Year\n" );
    string year;
    cin >> year;

    sql = ( string ) "SELECT TBNAME,DETNAME,UNIT FROM efficiencies WHERE YEAR=\n" + year + (string) "\n AND binsX=1 AND binsY=1";
    const char* data = "Non-2d: \n";


    /* Execute SQL statement */
    rc = sqlite3_exec ( db, sql.c_str(), callback, ( void* ) data, &zErrMsg );
    if ( rc != SQLITE_OK ) {
        fprintf ( stderr, "SQL error: %s\n", zErrMsg );
        sqlite3_free ( zErrMsg );
    }
    else {
        fprintf ( stdout, "Operation done successfully -- Results should be above!\n" );
    }
}


void createTGA(string _tbname, string _detname, string _unitnumber, string _year, string _tbFile)
{
    T4EfficDatabase myDatabase(dbName);
    if (myDatabase.isGood())
        printf("db says file is good\n");
    else
        return;
    if (myDatabase.cacheDetector(_tbname,_detname,_unitnumber,_year))
        printf("Query Success");
    else
        return;
    string idString = _tbname+_detname+_unitnumber;
    printf("Detector in cache says: binx %i, biny %i, startx %f, starty %f\n",T4EfficDatabase::cache[idString]->binsX,T4EfficDatabase::cache[idString]->binsY,T4EfficDatabase::cache[idString]->startX,T4EfficDatabase::cache[idString]->startY);

    if (T4EfficDatabase::cache[idString] == NULL) {
        printf("detector not found!\n");
    }

    T4EfficDatabase::cache[idString]->saveToTGA(_tbname+".tga");
}


void testImplementation() {
    printf("Enter detector tbname,detname,unit and year and lets see what comes out!\n");
    string tbname,detname,unit,year;
    cin >> tbname;
    cin >> detname;
    cin >> unit;
    cin >> year;

    T4EfficDatabase myDatabase(dbName);
    if (myDatabase.isGood())
        printf("db says file is good\n");
    else
        return;
    if (myDatabase.cacheDetector(tbname,detname,unit,year))
        printf("Query Success");
    else
        return;
    string idString = tbname+detname+unit;
    printf("Detector in cache says: binx %i, biny %i, startx %f, starty %f\n",T4EfficDatabase::cache[idString]->binsX,T4EfficDatabase::cache[idString]->binsY,T4EfficDatabase::cache[idString]->startX,T4EfficDatabase::cache[idString]->startY);

    if (T4EfficDatabase::cache[idString] == NULL) {
        printf("detector not found!\n");
    }

    T4EfficDatabase::cache[idString]->saveToTGA("det.tga");


    TH2D* showDetHist = new TH2D(tbname.c_str(),tbname.c_str(),T4EfficDatabase::cache[idString]->binsX,T4EfficDatabase::cache[idString]->startX,T4EfficDatabase::cache[idString]->endX,T4EfficDatabase::cache[idString]->binsY,T4EfficDatabase::cache[idString]->startY,T4EfficDatabase::cache[idString]->endY);

    for (int i = 0; i < T4EfficDatabase::cache[idString]->binsX; i++)
        for(int a = 0; a < T4EfficDatabase::cache[idString]->binsY; a++) {
//             printf("bin %i %i -- effic %.3e\n",i,a,T4EfficDatabase::cache[idString]->dataArray[i][a]);
            showDetHist->SetBinContent(i+1,a+1,T4EfficDatabase::cache[idString]->dataArray[i][a]);
        }
    char buffer[100];
    sprintf(buffer,"Detector plot for detector %s det %s unit %s in year %s",tbname.c_str(),detname.c_str(),unit.c_str(),year.c_str());
    showDetHist->SetTitle(buffer);
    showDetHist->SetDrawOption("colz");
    showDetHist->SaveAs("detHist.C");
    showDetHist->SaveAs("detHist.root");
    system("root -l .x detHist.C");
    printf("detHist.png and detHist.root were created \n");
}

void makeBlob()
{
    cout << "enter path to TGA" << endl;
    string tgafile;
    cin >> tgafile;
    double meanVal = 0.0;
    int meanCount = 0;
//   T4effic2Dline::loadFromTGA(tgafile);

    uint16_t header[9];
    FILE* outPutImage = fopen(tgafile.c_str(),"rb");
    if (outPutImage == NULL) {
        T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Could not open file %s for reading binary!\n",tgafile.c_str());
        return;
    }
    printf("opened the file \n");
    //write the binary header
    fread(header,1,sizeof(header),outPutImage);
    int nBinsX = header[6];
    int nBinsY = header[7];
//   printf("Bin");

    printf("read header, binsize: %i %i \n",nBinsX,nBinsY);
    float dataVal[nBinsX][nBinsY];
    for (int i = 0; i < nBinsY; i++)
        for (int a = 0; a < nBinsX; a++) {

            uint8_t myVal;
            uint8_t myAlpha;
            fread(&myVal,sizeof(uint8_t),1,outPutImage);
            fread(&myAlpha,sizeof(uint8_t),1,outPutImage);

            float retVal;

            retVal = (float) myVal;


            retVal /= (pow(2.,8.)-1.);
            if (static_cast<uint16_t>(myAlpha)==0) {
                dataVal[a][i] = -1.0;
            }
            else
                dataVal[a][i] = retVal;


        }

    TH2D* showDetHist = new TH2D("TGAPLOT","TGAPLOT",nBinsX,-118.0,118.0,nBinsY,-325,325);

    for (int i = 0; i < nBinsX; i++)
        for(int a = 0; a < nBinsY; a++) {
            showDetHist->SetBinContent(i+1,a+1,dataVal[i][a]);
            meanCount ++;
            if (dataVal[i][a] != 0) {
                meanVal += dataVal[i][a];
            }
            else {
                meanVal += 0.932314;
            }
        }
    showDetHist->SetDrawOption("colz");
    showDetHist->SaveAs("detHist.C");
    showDetHist->SaveAs("detHist.root");
    system("root -l .x detHist.C");

    printf("Sum of Bins %.4e, bins counted %i -- MeanEffic: %.3e\n",meanVal,meanCount,meanVal/meanCount);
    cout << "printing blob for this hist!" << endl;
    string wait;
    cin >> wait;

    stringstream theBlob;
    for ( int x = 0; x < nBinsX; x++ ) {
        for ( int y = 0; y < nBinsY; y++ ) {
            theBlob << dataVal[x][y] << " ";

        }
        theBlob << "<br />";
    }

    cout << theBlob.str() << endl;


}

void plotDB()
{
    printf("Enter Year to plot!\n");
    string year;
    cin >> year;

    T4EfficDatabase myDatabase(dbName);
    if (myDatabase.isGood())
        printf("db says file is good\n");
    else
        return;
    if (myDatabase.cacheAllYear(year))
        printf("Query Success");
    else
        return;



    map<string,T4effic2Dline*>::iterator myIter;
    TFile* myOutFileRoot = new TFile("dbPlot.root","recreate");
    TCanvas* myCanvas= new TCanvas("mycanvas","mycanvas",1280,800);
    for (myIter =  T4EfficDatabase::cache.begin(); myIter!= T4EfficDatabase::cache.end(); ++myIter) {
//       string tbname = myIter->tbname

        TH2D* showDetHist = new TH2D(myIter->second->tbname.c_str(),myIter->second->tbname.c_str(),myIter->second->binsX,myIter->second->startX,myIter->second->endX,myIter->second->binsY,myIter->second->startY,myIter->second->endY);


        for (int i = 0; i < myIter->second->binsX; i++)
            for(int a = 0; a < myIter->second->binsY; a++) {
                showDetHist->SetBinContent(i+1,a+1,myIter->second->dataArray[i][a]);
            }


        char buffer[100];
        sprintf(buffer,"Detector %s %s %s  in year %s : MeanEffic: %f",myIter->second->tbname.c_str(),myIter->second->detname.c_str(),myIter->second->unit.c_str(),year.c_str(),myIter->second->efficMean);
        showDetHist->SetTitle(buffer);
        showDetHist->SetDrawOption("colz");
        gStyle->SetOptStat(0);
        showDetHist->GetXaxis()->SetTitle("x [cm]");
        showDetHist->GetYaxis()->SetTitle("y [cm]");
        myCanvas->Clear();
//         showDetHist->SetMinimum(-0.1);
        showDetHist->Draw("colz");
        myCanvas->Draw();
        string fileName=myIter->second->tbname+"_"+myIter->second->detname+"_"+myIter->second->unit+"_"+year+".png";
        myCanvas->SaveAs(fileName.c_str());
        showDetHist->Write();
        myOutFileRoot->Flush();
        delete showDetHist;
    }
    myOutFileRoot->Close();
    exit(0);
}





