// Std includes and cstring helpers
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <map>
#include <T4SGlobals.hh>



//ROOT includes
#include <TCanvas.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TIterator.h>
#include <TH2D.h>
#include <TH2.h>
#include <TH1.h>
#include <TTree.h>

using namespace std;


int main ( int argc, char **argv )
{
    if ( argc < 2 ) {
        printf ( "Builds the CDF-Table from the ecalNoiseTool's merged histograms\n" );
        printf ( "Usage: %s [inputTree.root] [OutputHistos.root]\n", argv[0] );
        exit ( 0 );
    }

    TFile* myInput = new TFile ( argv[1],"read" );

    vector<string> histVec;
    vector<TH1D*> cdfList;


    TH1D* allIntegrationHist = new TH1D ( "CellInt","CellInt",30000,0,30000 );
    TH1D* allCDF = new TH1D ( "CellCDF","CellCDF",30000,0,30000 );

    TList* keyListHist = gDirectory->GetListOfKeys();
    TIterator* histList = keyListHist->MakeIterator();
    TObjString *histname_obj;
    while ( histname_obj = ( TObjString * ) histList->Next() ) {
        string histoname = ( histname_obj->String() ).Data();
        histVec.push_back ( histoname );
    }

    for ( int i =0; i < histVec.size(); i++ ) {
        string actHist = histVec.at ( i );
        TH1D* actHistRoot = ( TH1D* ) myInput->Get ( actHist.c_str() );
        TH1D* saveThisToo = ( TH1D* ) actHistRoot->Clone();
        string newName = actHistRoot->GetName();
        string newTitle = actHistRoot->GetTitle();
        newName+="_CDF_";
        newTitle+="_CDF_";
        int UniqueNum;
        vector<string> afterExplode = explodeStringCustomDelim ( actHistRoot->GetTitle(),"_" );
        // should be like E_Cell_73_calo_1
        // therefore 5 elements after explode
        if ( afterExplode.size() < 5 ) {
            printf ( "Problem in title of histogram! This root-file is not cleanly created by ecalNoiseAnal tool: %s\n",actHistRoot->GetTitle() );
            exit ( -1 );
        } else {
            UniqueNum = 10000*strToInt ( afterExplode.back() ) +strToInt ( afterExplode.at ( 2 ) );
            allIntegrationHist->SetBinContent ( UniqueNum+1,actHistRoot->Integral() );
        }
        newName+=intToStr ( UniqueNum );
        newTitle+=intToStr ( UniqueNum );
        TH1D* cdf = new TH1D ( newName.c_str(),newTitle.c_str(),100.,0.,10. );
        double IntVal = 0.0;
        for ( int bin = 1; bin <= 100; bin++ ) {
            IntVal+=actHistRoot->GetBinContent ( bin );
            cdf->SetBinContent ( bin,IntVal );
        }
        cdf->Scale ( 1/IntVal );
        cdfList.push_back ( cdf );
    }


    TFile* myOutput = new TFile ( argv[2],"RECREATE" );



    double allInt = 0.0;
    for ( int bin = 1; bin <= 30000; bin++ ) {
        allInt+=allIntegrationHist->GetBinContent ( bin );
        allCDF->SetBinContent ( bin,allInt );
    }

    allCDF->Scale ( 1/allInt );
    allCDF->Write();
    allIntegrationHist->Write();
    for ( std::vector<TH1D*>::iterator it=cdfList.begin(); it!=cdfList.end(); ++it ) {
        ( *it )->Write();
    }


    myOutput->Write();
    myOutput->Close();
}
