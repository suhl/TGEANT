// Std includes and cstring helpers
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <map>




//ROOT includes
#include <TCanvas.h>
#include <TFile.h>
#include <TH2D.h>
#include <TH2.h>
#include <TH1.h>
#include <TTree.h>

using namespace std;


void calcRate ( Long64_t _nCluster, int _nTrigger )
{

    double rate = static_cast<double> ( _nCluster ) /static_cast<double> ( _nTrigger );

    printf ( "----- Final Rate-Analysis----------------------\n" );
    printf ( "--- Found %i Triggers\n",_nTrigger );
    printf ( "--- Found %lli Clusters \n",_nCluster );
    printf ( "--- Resulting Rate is %lf clusters / event\n",rate );
    printf ( "-----------------------------------------------\n" );

}


int main ( int argc, char **argv )
{
    if ( argc < 2 ) {
        printf ( "Prepares the modular noise histograms from the calorimeter tree\n" );
        printf ( "Usage: %s [inputTree.root] [OutputHistos.root] (--Rate-Only)\n", argv[0] );
        printf ( "--Rate-Only: Does not produce an output root-file but only calculates the rate of the given data set\n" );
        exit ( 0 );
    }

    TFile* myInput = new TFile ( argv[1],"read" );


    TTree* myTree = NULL;
    myTree = ( TTree* ) myInput->Get ( "CLUSTER" );

    double energy;
    int iCell;
    int caloNumber;
    map<string,TH1D*> cellMap;

    myTree->SetBranchAddress ( "energy",&energy );
    myTree->SetBranchAddress ( "iCell",&iCell );
    myTree->SetBranchAddress ( "caloNumber",&caloNumber );


    //read all entries and fill the histograms
    Long64_t nentries = myTree->GetEntries();
    char buffer[200];
    char buffer2[20];


    //now make the noise-Rate analysis

    //get the triggercount
    TH1D* Trigcount = ( TH1D* ) myInput->Get ( "TriggerCounter" );
    int nTrib = Trigcount->GetBinContent ( 8 );

    if ( argc == 4 && strcmp ( argv[3],"--Rate-Only" ) ==0 ) {
        calcRate ( nentries,nTrib );
        return 0;
    }

    for ( Long64_t i=0; i<nentries; i++ ) {
        if ( i % 10000 == 0 ) {
            printf ( "At Event %lli\n",i );
        }
        myTree->GetEntry ( i );
        sprintf ( buffer2,"%i_%i",caloNumber,iCell );
        if ( cellMap.find ( buffer2 ) == cellMap.end() ) {
            sprintf ( buffer,"E_Cell_%i_calo_%i",iCell,caloNumber );
            cellMap[buffer2] = new TH1D ( buffer,buffer,100,0.,10. );
        }
        cellMap[buffer2]->Fill ( energy );
    }
//   myInput->Close();

//   delete myTree;
//   delete myInput;

    printf ( "Saving Histograms" );

    TFile* myOutput = new TFile ( argv[2],"RECREATE" );
//   myOutput->set
    for ( std::map<string,TH1D*>::iterator it=cellMap.begin(); it!=cellMap.end(); ++it ) {
        it->second->Write();
        printf ( "Writing histogram: %s\n",it->second->GetTitle() );
    }
    myOutput->Write();
    myOutput->Close();

    calcRate ( nentries,nTrib );
    return 0;
}
