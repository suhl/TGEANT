#Find hepgen
#once done will define
#hepgen_FOUND
#hepgen_LIBRARIES



find_library(HEPGEN_LIBRARY hepgen)

set(hepgen_DEFAULT_MSG "Could NOT find hepgen.so!\n")

#Standard-Search
If(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")
  Set (hepgen_FOUND FALSE)
  message(STATUS "hepgen.so not found in system directories, checking environmentals!")
Else(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")
  Set (hepgen_FOUND TRUE)
  message(STATUS "hepgen.so found in system: " HEPGEN_LIBRARY)
EndIf(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")


#Search in Environmental-Variable HEPGEN
If(NOT EXISTS "$ENV{HEPGEN}")
  message("No HEPGEN-ENV is set!")
Set (hepgen_FOUND FALSE)
Else()
  find_library(HEPGEN_LIBRARY NAMES hepgen PATHS $ENV{HEPGEN}/lib)
    If(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")
      Set (hepgen_FOUND FALSE)
      message(STATUS "hepgen.so not found in env directory!") 
    Else(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")
      Set (hepgen_FOUND TRUE)
      Set (HEPGEN_LIB ${HEPGEN_LIBRARY})
      message(STATUS "hepgen.so found: " ${HEPGEN_LIBRARY})
      Set(HEPGEN_LIBDIR $ENV{HEPGEN}/lib)
      Set(LD_LIBRARY_PATH ${LD_LIBRARY_PATH} ${HEPGEN_LIBDIR})
      Set(hepgen_FOUND TRUE)
      message("-- HEPGEN-ENV is set, setting path to: " $ENV{HEPGEN}/lib)
    EndIf(${HEPGEN_LIBRARY} MATCHES "HEPGEN_LIBRARY-NOTFOUND")
EndIf()




# message(${LD_LIBRARY_PATH})





  
