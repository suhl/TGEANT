#Find primgen
#once done will define
#primgen_FOUND
#primgen_LIBRARIES



Set (primgen_FOUND FALSE)

#Search in Environmental-Variable PRIMGEN
If(NOT EXISTS "$ENV{PRIMGEN}")
  message(FATAL_ERROR "No PRIMGEN-ENV is set!")
Set (primgen_FOUND FALSE)
Else()
  find_path(PRIMGEN_INCLUDE_DIR primGen.h PATHS $ENV{PRIMGEN})
  If(PRIMGEN_INCLUDE_DIR MATCHES "PRIMGEN_INCLUDE_DIR-NOTFOUND")
      message(FATAL_ERROR "primGen.h not found in env directory!")
  EndIf(PRIMGEN_INCLUDE_DIR MATCHES "PRIMGEN_INCLUDE_DIR-NOTFOUND")

  find_library(PRIMGEN_LIBRARY NAMES PrimGen PATHS $ENV{PRIMGEN})
    If(${PRIMGEN_LIBRARY} MATCHES "PRIMGEN_LIBRARY-NOTFOUND")
      Set (primgen_FOUND FALSE)
      message(FATAL_ERROR "libPrimGen.so not found in env directory!")
    Else(${PRIMGEN_LIBRARY} MATCHES "PRIMGEN_LIBRARY-NOTFOUND")
      Set (primgen_FOUND TRUE)
      Set (PRIMGEN_LIB ${PRIMGEN_LIBRARY})
      message(STATUS "primgen.so found: " ${PRIMGEN_LIBRARY})
      Set(PRIMGEN_LIBDIR $ENV{PRIMGEN})
      Set(LD_LIBRARY_PATH ${LD_LIBRARY_PATH} ${PRIMGEN_LIBDIR})
      Set(primgen_FOUND TRUE)
      message("-- PRIMGEN-ENV is set, setting path to: " $ENV{PRIMGEN})
    EndIf(${PRIMGEN_LIBRARY} MATCHES "PRIMGEN_LIBRARY-NOTFOUND")
EndIf()


