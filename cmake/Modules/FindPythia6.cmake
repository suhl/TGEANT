#Find Pythia6

find_library(PYTHIA6_LIBRARY Pythia6)

set(Pythia6_DEFAULT_MSG "Could NOT find Pythia6.so!\n")

#Standard-Search
If(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")
  Set (Pythia6_FOUND FALSE)
  message(STATUS "Pythia6.so not found in system directories, checking environmentals!")
Else(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")
  Set (Pythia6_FOUND TRUE)
  message(STATUS "Pythia6.so found in system: " ${PYTHIA6_LIBRARY})
EndIf(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")


#Search in Environmental-Variable PYTHIA6
If(NOT EXISTS "$ENV{PYTHIA6}")
  message("No PYTHIA6-ENV is set!")
  Set(Pythia6_FOUND FALSE)
Else()
  find_library(PYTHIA6_LIBRARY Pythia6 PATH $ENV{PYTHIA6})
  If(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")
    Set (Pythia6_FOUND FALSE)
    message(STATUS "Pythia6.so not found in system directories!") 
  Else(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")
    Set (Pythia6_FOUND TRUE)
    Set (PYTHIA6_LIB ${PYTHIA6_LIBRARY})
    message(STATUS "Pythia6.so found: " ${PYTHIA6_LIBRARY})
    Set(PYTHIA_LIBDIR $ENV{PYTHIA6})
    Set(LD_LIBRARY_PATH ${LD_LIBRARY_PATH} ${PYTHIA6_LIBDIR})
    Set(Pythia6_FOUND TRUE)
    message("-- PYTHIA6-ENV is set, setting path to: " $ENV{PYTHIA6})
  EndIf(${PYTHIA6_LIBRARY} MATCHES "PYTHIA6_LIBRARY-NOTFOUND")
EndIf()



if (${TGEANT_EXTERNAL_LHAPDF} MATCHES "YES")
    message("Wanting external LHAPDF!")
    find_library(LHAPDF_LIBRARY LHAPDF PATH $ENV{LHAPDF})
    If(${LHAPDF_LIBRARY} MATCHES "LHAPDF_LIBRARY-NOTFOUND")
      Set (LHAPDF_FOUND FALSE)
      message(STATUS "libLHAPDF.so not found in system directories!") 
    Else(${LHAPDF_LIBRARY} MATCHES "LHAPDF_LIBRARY-NOTFOUND")
      Set (LHAPDF_FOUND TRUE)
      Set (LHAPDF_LIB ${LHAPDF_LIBRARY})
      message(STATUS "libLHAPDF.so found: " ${LHAPDF_LIBRARY})
      Set(LHAPDF_LIBDIR $ENV{LHAPDF})
      Set(LD_LIBRARY_PATH ${LD_LIBRARY_PATH} ${LHAPDF_LIBDIR})
      Set(LHAPDF_FOUND TRUE)
      message("-- LHAPDF-ENV is set, setting path to: " $ENV{LHAPDF})
    EndIf(${LHAPDF_LIBRARY} MATCHES "LHAPDF_LIBRARY-NOTFOUND")
EndIf(${TGEANT_EXTERNAL_LHAPDF} MATCHES "YES")

# message(${LD_LIBRARY_PATH})





  
