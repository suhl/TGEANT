#interface-building file
message("---------------------------")
message("Preparing Interface binary")
message("---------------------------")

find_package(Qt4 REQUIRED)
find_package(ROOT REQUIRED)
set(QT_USE_OPENGL TRUE)
find_package (Xerces REQUIRED) 

include_directories (${XERCESC_INCLUDE})


MACRO(HEADER_DIRECTORIES_INTERFACE return_list)
    FILE(GLOB_RECURSE new_list *.h)
    SET(dir_list "")
    FOREACH(file_path ${new_list})
        GET_FILENAME_COMPONENT(dir_path ${file_path} PATH)
        SET(dir_list ${dir_list} ${dir_path})
    ENDFOREACH()
    LIST(REMOVE_DUPLICATES dir_list)
    SET(${return_list} ${dir_list})
ENDMACRO()


##### Prepare Headers and files for Efficiencies Interface
QT4_WRAP_CPP(EFFICINTERFACE_HEADERS_MOC "${CMAKE_SOURCE_DIR}/Interface/include/T4IEfficiencies.hh")
QT4_WRAP_UI(EFFICINTERFACE_FORMS_MOC "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Efficiencies.ui")

##### Prepare Headers and files for Pythia Interface
QT4_WRAP_CPP(PYINTERFACE_HEADERS_MOC "${CMAKE_SOURCE_DIR}/Interface/include/T4IPythiaPreferences.hh")
QT4_WRAP_UI(PYINTERFACE_FORMS_MOC "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Pythia.ui")



HEADER_DIRECTORIES_INTERFACE(EXTRA_INCLUDES_INTERFACE)

include_directories (${PROJECT_SOURCE_DIR}/libSettings/include)
include_directories (${PROJECT_SOURCE_DIR}/Interface/)
include_directories (EXTRA_INCLUDES_INTERFACE)


#### Prepare sources, forms and headers for the Interface main exe

set(Interface_SRC
    "${CMAKE_SOURCE_DIR}/Interface/src/Interface.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4IEfficiencies.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4IGenerator.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4ITrigger.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4IPythiaPreferences.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4ISetInterface.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4ISetStructManager.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4ISetStructManager.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4ISettingsFile.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4IStartLocal.cc"
    "${CMAKE_SOURCE_DIR}/Interface/src/T4IMainMenu.cc"
)

set(Interface_HEADERS
    "${CMAKE_SOURCE_DIR}/Interface/include/T4IMainMenu.hh"
    "${CMAKE_SOURCE_DIR}/Interface/include/T4IGenerator.hh"
    "${CMAKE_SOURCE_DIR}/Interface/include/T4ITrigger.hh"
    "${CMAKE_SOURCE_DIR}/Interface/include/T4ISettingsFile.hh"
    "${CMAKE_SOURCE_DIR}/Interface/include/T4IStartLocal.hh"
)

set(Interface_FORMS
    "${CMAKE_SOURCE_DIR}/Interface/forms/MainWindow.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Cosmics.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-EcalCalib.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Generator-Hepgen.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Generator-Primakoff.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-PhysicsList.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-User.ui"
    "${CMAKE_SOURCE_DIR}/Interface/forms/Preferences-Trigger-Primakoff.ui"
)

QT4_WRAP_CPP(Interface_HEADERS_MOC ${Interface_HEADERS})
QT4_WRAP_UI(Interface_FORMS_HEADERS ${Interface_FORMS})

#### Include Directories for the QT
include_directories(${CMAKE_CURRENT_BINARY_DIR})
INCLUDE(${QT_USE_FILE})

ADD_DEFINITIONS(${QT_DEFINITIONS})



##### Main Interface
ADD_EXECUTABLE(Interface ${Interface_SRC} 
    ${Interface_HEADERS_MOC} 
    ${Interface_FORMS_HEADERS}
    ${Interface_HEADERS}
    ${EFFICINTERFACE_HEADERS_MOC} 
    ${EFFICINTERFACE_FORMS_MOC}
    ${PYINTERFACE_HEADERS_MOC} 
    ${PYINTERFACE_FORMS_MOC}
)

TARGET_LINK_LIBRARIES(Interface T4Settings)
TARGET_LINK_LIBRARIES(Interface ${QT_QTCORE_LIBRARY}  ${QT_QTGUI_LIBRARY})
TARGET_LINK_LIBRARIES(Interface ${QT_LIBRARIES} )


##### EfficInterface
add_executable(EfficInterface "${CMAKE_SOURCE_DIR}/Interface/src_effic/EfficInterface.cc"
				"${CMAKE_SOURCE_DIR}/Interface/src/T4IEfficiencies.cc"
				 ${EFFICINTERFACE_HEADERS_MOC} 
				 ${EFFICINTERFACE_FORMS_MOC}
    )

INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})
TARGET_LINK_LIBRARIES(EfficInterface T4Settings)
TARGET_LINK_LIBRARIES(EfficInterface ${QT_QTCORE_LIBRARY}  ${QT_QTGUI_LIBRARY})
TARGET_LINK_LIBRARIES(EfficInterface ${QT_LIBRARIES} )

##### PythiaInterface
add_executable(PythiaInterface "${CMAKE_SOURCE_DIR}/Interface/src_pythia/PythiaInterface.cc"
				"${CMAKE_SOURCE_DIR}/Interface/src/T4IPythiaPreferences.cc"
				 ${PYINTERFACE_HEADERS_MOC} 
				 ${PYINTERFACE_FORMS_MOC}
    )

INCLUDE(${QT_USE_FILE})
ADD_DEFINITIONS(${QT_DEFINITIONS})
TARGET_LINK_LIBRARIES(PythiaInterface T4Settings)
TARGET_LINK_LIBRARIES(PythiaInterface ${QT_QTCORE_LIBRARY}  ${QT_QTGUI_LIBRARY})
TARGET_LINK_LIBRARIES(PythiaInterface ${QT_LIBRARIES} )




##### Installation of the interface files
install (TARGETS EfficInterface RUNTIME DESTINATION bin)
install (TARGETS Interface RUNTIME DESTINATION bin)
install (TARGETS PythiaInterface RUNTIME DESTINATION bin)
