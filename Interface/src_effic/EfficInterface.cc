#include <iostream>
#include <qapplication.h>



#include "include/T4ISettingsFile.hh"
#include "include/T4IMainMenu.hh"
#include "include/T4IGenerator.hh"
#include "include/T4IStartLocal.hh"
#include "include/T4IEfficiencies.hh"


int main (int argc, char** argv){
   if (getenv("TGEANT") == NULL) {
    std::cerr << "$TGEANT is not set! Exit." << std::endl;
    std::cerr << "Please set $TGEANT to the TGEANT installation directory."
        << std::endl;
    exit(0);
  } else {
    std::cout << "Found $TGEANT: " << std::string(getenv("TGEANT")) << std::endl;
  }
  
  if (argc < 2){
    printf("Use me like: %s /PATH/TO/EFFIC.xml \n",argv[0]);
    return -1;
  }

  QApplication qApplication(argc, argv);

  T4SStructManager* structManager = new T4SStructManager;
  
  structManager->getExternal()->detectorEfficiency = argv[1];
  T4IEfficiencies* myGui = new T4IEfficiencies();
  myGui->setStructMan(structManager);
  
  myGui->show();
  
  return qApplication.exec();
}