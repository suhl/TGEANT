#include "include/T4IStartLocal.hh"

T4IStartLocal::T4IStartLocal(T4ISettingsFile* settingsFile_,
    Ui::MainWindow* uiMainWindow_)
{
  settingsFile = settingsFile_;
  uiMainWindow = uiMainWindow_;

  connect(uiMainWindow->pushButton_start, SIGNAL(clicked()), this,
      SLOT(buttonStart()));
}

void T4IStartLocal::run(void)
{
  system("$TGEANT/bin/TGEANT");
}

void T4IStartLocal::buttonStart(void)
{
  settingsFile->saveFile();
  start(QThread::HighPriority);
}
