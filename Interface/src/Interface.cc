#include <iostream>
#include "qapplication.h"

#include "include/T4ISettingsFile.hh"
#include "include/T4IMainMenu.hh"
#include "include/T4IGenerator.hh"
#include "include/T4IStartLocal.hh"
#include "include/T4IPythiaPreferences.hh"

int main(int argc, char* argv[])
{
  std::cout << "TGEANT Graphical User Interface started." << std::endl;
  if (getenv("TGEANT") == NULL) {
    std::cerr << "$TGEANT is not set! Exit." << std::endl;
    std::cerr << "Please set $TGEANT to the TGEANT installation directory."
        << std::endl;
    exit(0);
  } else {
    std::cout << "Found $TGEANT: " << std::string(getenv("TGEANT")) << std::endl;
  }

  QApplication qApplication(argc, argv);

  Ui::MainWindow uiMainWindow;
  Ui::PreferencesBeamUser uiUser;
  Ui::PreferencesBeamCosmics uiCosmics;
  Ui::PreferencesBeamEcalCalib uiEcalCalib;
  Ui::PreferencesGeneratorHEPGEN uiHEPGen;
  Ui::PreferencesGeneratorPrimGen uiPrimGen;
  Ui::PreferencesPhysicsList uiPhysicsList;
  Ui::PreferencesTriggerPrimakoff uiPrimTrigger;
  T4IPythiaGui uiPythia;

  T4ISettingsFile* settingsFile = new T4ISettingsFile(&uiMainWindow, &uiUser,
      &uiCosmics, &uiEcalCalib, &uiHEPGen, &uiPrimGen, &uiPhysicsList, &uiPrimTrigger);

  uiPythia.setStructMan(settingsFile->getT4SStructManager());

  T4IMainMenu* mainMenu = new T4IMainMenu(settingsFile, &uiMainWindow,
      &uiPhysicsList);
  new T4IGenerator(settingsFile, &uiMainWindow, &uiUser, &uiCosmics, &uiEcalCalib, 
      &uiHEPGen, &uiPrimGen, &uiPythia);
  new T4ITrigger(settingsFile, &uiMainWindow, &uiPrimTrigger);

  T4IStartLocal* startLocal = new T4IStartLocal(settingsFile, &uiMainWindow);

  mainMenu->show();
  startLocal->wait();

  return qApplication.exec();
}

