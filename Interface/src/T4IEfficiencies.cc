#include "include/T4IEfficiencies.hh"


void T4IEfficiencies::clearInterface(void)
{
    tableDetectors->clearContents();
    tableMagnets->clearContents();

}

void T4IEfficiencies::clearStructMan(void)
{
    myEffic->getDetList().clear();
    myEffic->getMagList().clear();
}

void T4IEfficiencies::readFile(void)
{
    string translated = translatePathEnvTGEANT(myStructMan->getExternal()->detectorEfficiency);
    myEffic->readXML(translated);
    setInterfaceFromStructMan();
}

void T4IEfficiencies::setInterfaceFromStructMan(void)
{
    clearInterface();
    tableDetectors->setRowCount(myEffic->getDetList().size());
    for (unsigned int i = 0; i < myEffic->getDetList().size(); i++) {
        tableDetectors->setItem(i,0,new QTableWidgetItem(QString::fromStdString(myEffic->getDetList().at(i).tbName)));
        tableDetectors->setItem(i,1,new QTableWidgetItem(QString::fromStdString(myEffic->getDetList().at(i).det)));
        tableDetectors->setItem(i,2,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).unit)));
        tableDetectors->setItem(i,3,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).radLength)));
        tableDetectors->setItem(i,4,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).effic)));
        tableDetectors->setItem(i,5,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).backgr)));
        tableDetectors->setItem(i,6,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).tgate)));
        tableDetectors->setItem(i,7,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).drVel)));
        tableDetectors->setItem(i,8,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).t0)));
        tableDetectors->setItem(i,9,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).res2hit)));
        tableDetectors->setItem(i,10,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).space)));
        tableDetectors->setItem(i,11,new QTableWidgetItem(QString::number(myEffic->getDetList().at(i).tslice)));

        Qt::ItemFlags flags = tableDetectors->item(i,0)->flags();
        flags |= Qt::ItemIsSelectable;
        flags &= ~Qt::ItemIsEditable;
        for (int flagCell = 0; flagCell < 12; flagCell++)
            tableDetectors->item(i,flagCell)->setFlags(flags);
    }
    tableDetectors->resizeColumnsToContents();

    tableMagnets->setRowCount(myEffic->getMagList().size());
    for (unsigned int i = 0; i < myEffic->getMagList().size(); i++) {
        tableMagnets->setItem(i,0,new QTableWidgetItem(QString::number(myEffic->getMagList().at(i).number)));
        tableMagnets->setItem(i,1,new QTableWidgetItem(QString::number(myEffic->getMagList().at(i).flag1)));
        tableMagnets->setItem(i,2,new QTableWidgetItem(QString::number(myEffic->getMagList().at(i).flag2)));
        tableMagnets->setItem(i,3,new QTableWidgetItem(QString::number(myEffic->getMagList().at(i).rot)));
	
	Qt::ItemFlags flags = tableMagnets->item(i,0)->flags();
        flags |= Qt::ItemIsSelectable;
        flags &= ~Qt::ItemIsEditable;
        for (int flagCell = 0; flagCell < 4; flagCell++)
            tableMagnets->item(i,flagCell)->setFlags(flags);
    }
    tableMagnets->resizeColumnsToContents();

}

void T4IEfficiencies::setStructMan(T4SStructManager* _structMan)
{
    myStructMan = _structMan;
    myEffic = new T4SEfficiency(_structMan);
}

void T4IEfficiencies::setStructManFromInterface(void)
{

}


void T4IEfficiencies::editDetEff(int _row, int _column)
{
    selDet = _row;

    lineEditTBName->setText(QString::fromStdString(myEffic->getDetList().at(_row).tbName));
    lineEditDet->setText(QString::fromStdString(myEffic->getDetList().at(_row).det));
    dsbRadLen->setValue(myEffic->getDetList().at(_row).radLength);
    dsbEffic->setValue(myEffic->getDetList().at(_row).effic);
    dsbBackgr->setValue(myEffic->getDetList().at(_row).backgr);
    dsbTGate->setValue(myEffic->getDetList().at(_row).tgate);
    dsbDrVel->setValue(myEffic->getDetList().at(_row).drVel);
    dsbT0->setValue(myEffic->getDetList().at(_row).t0);
    dsbRes2Hit->setValue(myEffic->getDetList().at(_row).res2hit);
    dsbSpace->setValue(myEffic->getDetList().at(_row).space);
    dsbTSlice->setValue(myEffic->getDetList().at(_row).tslice);
}



void T4IEfficiencies::editMagEff(int _row,  int _column)
{
    selMag = _row;
    spinBoxNumber->setValue(myEffic->getMagList().at(_row).number);
    dsbFlag1->setValue(myEffic->getMagList().at(_row).flag1);
    dsbFlag2->setValue(myEffic->getMagList().at(_row).flag2);
    spinBoxMagnetRot->setValue(myEffic->getMagList().at(_row).rot);
}




void T4IEfficiencies::showEvent(QShowEvent* event)
{
    readFile();
    QDialog::showEvent(event);
}

void T4IEfficiencies::writeFile(void)
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Save effic.xml"),envTGEANT+"/resources/",tr("Efficiency-Files (*.xml)"));
    if (fileName == "")
        return;
    setStructManFromInterface();
    myStructMan->getExternal()->detectorEfficiency = fileName.toStdString();
    writeOutStructMan();
    done(QDialog::Accepted);
}

void T4IEfficiencies::writeOutStructMan(void)
{
    myEffic->writeXML(myStructMan->getExternal()->detectorEfficiency);
}

T4IEfficiencies::T4IEfficiencies(QWidget* parent, Qt::WindowFlags f): QDialog(parent, f)
{
    setupUi(this);
    myEffic = NULL;

    std::string envTGEANT_str = std::string(getenv("TGEANT")) + "/";
    envTGEANT = QString::fromStdString(envTGEANT_str);

    //signal slot connects for the main menu
    connect(yarpButton,SIGNAL(clicked(bool)),this,SLOT(writeFile(void)));
    connect(narpButton,SIGNAL(clicked(bool)),this,SLOT(readFile(void)));
    connect(cancelButton,SIGNAL(clicked(bool)),this,SLOT(reject()));
    connect(importButton,SIGNAL(clicked(bool)),this,SLOT(importDetDat()));
    selDet = -1;
    selMag = -1;
    connect(tableDetectors, SIGNAL(cellClicked(int,int)), this, SLOT(editDetEff(int,int)));
    connect(pushButtonSave,SIGNAL(clicked(bool)), this,SLOT(saveDet()));
    connect(pushButtonDelete,SIGNAL(clicked(bool)), this,SLOT(deleteDet()));
    connect(pushButtonInsert,SIGNAL(clicked(bool)), this,SLOT(insertDet()));

    connect(tableMagnets, SIGNAL(cellClicked(int,int)), this, SLOT(editMagEff(int,int)));
    connect(pushButtonMagnetSave,SIGNAL(clicked(bool)), this,SLOT(saveMag()));
    connect(pushButtonMagnetDelete,SIGNAL(clicked(bool)), this,SLOT(deleteMag()));
    connect(pushButtonMagnetInsert,SIGNAL(clicked(bool)), this,SLOT(insertMag()));
}

void T4IEfficiencies::deleteDet(void)
{
    if (selDet == -1 || selDet > myEffic->getDetList().size())
        return;

    myEffic->getDetList().erase(myEffic->getDetList().begin()+selDet);

    setInterfaceFromStructMan();
}

void T4IEfficiencies::deleteMag(void)
{
    if (selMag == -1 || selMag > myEffic->getMagList().size())
        return;

    myEffic->getMagList().erase(myEffic->getMagList().begin()+selMag);
    setInterfaceFromStructMan();
}


void T4IEfficiencies::insertDet(void)
{
    T4SWireDetector newStruct;
    myEffic->getDetList().push_back(newStruct);
    selDet = tableDetectors->rowCount();
    saveDet();
}

void T4IEfficiencies::insertMag(void)
{
    T4SMagnetLine newStruct;
    myEffic->getMagList().push_back(newStruct);
    selMag = tableMagnets->rowCount();
    saveMag();
}


void T4IEfficiencies::saveDet(void)
{
    if (selDet == -1 || selDet > myEffic->getDetList().size())
        return;

    T4SWireDetector* saveDet = &myEffic->getDetList().at(selDet);
    saveDet->tbName = lineEditTBName->text().toStdString();
    saveDet->det = lineEditDet->text().toStdString();
    saveDet->unit = spinBoxUnit->value();
    saveDet->radLength = dsbRadLen->value();
    saveDet->effic = dsbEffic->value();
    saveDet->backgr = dsbBackgr->value();
    saveDet->tgate = dsbTGate->value();
    saveDet->drVel = dsbDrVel->value();
    saveDet->t0 = dsbT0->value();
    saveDet->res2hit = dsbRes2Hit->value();
    saveDet->space = dsbSpace->value();
    saveDet->tslice = dsbTSlice->value();

    setInterfaceFromStructMan();
}

void T4IEfficiencies::saveMag(void)
{
    if (selMag == -1 || selMag > myEffic->getMagList().size())
        return;

    T4SMagnetLine* saveDet = &myEffic->getMagList().at(selMag);
    saveDet->number = spinBoxNumber->value();
    saveDet->flag1 = dsbFlag1->value();
    saveDet->flag2 = dsbFlag2->value();
    saveDet->rot = spinBoxMagnetRot->value();
    
    setInterfaceFromStructMan();
}





void T4IEfficiencies::importDetDat(void)
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Import COMPASS-COMGEANT detector table"),envTGEANT+"/resources/",tr("detector table (*.dat)"));
    if (fileName == "")
        return;
    myEffic->importEfficiencyFromDetDat(fileName.toStdString());

    setInterfaceFromStructMan();
}


T4IEfficiencies::~T4IEfficiencies()
{

}
