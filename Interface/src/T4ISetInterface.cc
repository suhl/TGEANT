#include "include/T4ISettingsFile.hh"

void T4ISettingsFile::setInterface(void)
{
  setInterfaceGeneral();
  setInterfaceBeam();
  setInterfacePaths();

  setInterfaceCalorimeter();
  setInterfaceMW1();
  setInterfaceMW2();
  setInterfaceStraw();
  setInterfaceMicromegas();
  setInterfaceGem();
  setInterfaceSciFi();
  setInterfaceSilicon();
  setInterfaceMWPC();
  setInterfaceVeto();
  setInterfaceDC();
  setInterfaceW45();
  setInterfaceRichWall();
  setInterfaceRPD();
  setInterfaceMagnet();
  setInterfaceRICH();
  setInterfaceDetector();
  setInterfaceDetectorRes();
  setInterfacePolGPD();
  setInterfaceScifiTest();
  setInterfaceDUMMY();
}

void T4ISettingsFile::setInterfaceGeneral(void)
{
  T4SGeneral* general = structM->getGeneral();
  uiMainWindow->comboBox_PhysicsList_Details->setCurrentIndex(
      uiMainWindow->comboBox_PhysicsList_Details->findText(
          QString::fromStdString(general->physicsList)));
  if (general->physicsList == "FULL")
    setInterfaceSampling();

  uiMainWindow->checkBox_useGflash->setChecked(general->useGflash);
  uiMainWindow->lineEdit_runName->setText(
      QString::fromStdString(general->runName));
  uiMainWindow->lineEdit_seed->setText(QString::number(general->seed));

  if (general->useVisualization)
    uiMainWindow->radioButton_VisualizationEnabled->setChecked(true);
  else
    uiMainWindow->radioButton_VisualizationDisabled->setChecked(true);

  uiMainWindow->checkBox_RestoreFileFromSeed->setChecked(general->useSeed);

  uiMainWindow->lineEdit_OutputPath->setText(
      QString::fromStdString(general->outputPath));
  uiMainWindow->checkBox_saveASCII->setChecked(general->saveASCII);
  uiMainWindow->checkBox_saveBinary->setChecked(general->saveBinary);
  uiMainWindow->checkBox_saveDETDAT->setChecked(general->saveDetDat);
  uiMainWindow->checkBox_useTriggerLogic->setChecked(general->useTrigger);
  uiMainWindow->comboBox_triggerPlugin->setCurrentIndex(uiMainWindow->comboBox_triggerPlugin->findText(QString::fromStdString(general->triggerPlugin)));
  uiMainWindow->checkBox_ROOTGeometry_GDML->setChecked(general->exportGDML);
  uiMainWindow->checkBox_namingWithSeed->setChecked(general->namingWithSeed);
  uiMainWindow->spinBox_verboseLevel->setValue(general->verboseLevel);
  uiMainWindow->checkBox_usePerformanceMonitor->setChecked(
      general->usePerformanceMonitor);
  uiMainWindow->checkBox_checkOverlap->setChecked(general->checkOverlap);
  uiMainWindow->doubleSpinBox_CutsGlobal->setValue(general->productionCutsGlobal);
  uiMainWindow->doubleSpinBox_CutsGams->setValue(general->productionCutsGams);
  uiMainWindow->doubleSpinBox_CutsRHGams->setValue(general->productionCutsRHGams);
  uiMainWindow->doubleSpinBox_CutsMainz->setValue(general->productionCutsMainz);
  uiMainWindow->doubleSpinBox_CutsOlga->setValue(general->productionCutsOlga);
  uiMainWindow->doubleSpinBox_CutsShashlik->setValue(general->productionCutsShashlik);
  uiMainWindow->doubleSpinBox_CutsHcal->setValue(general->productionCutsHcal);
  uiMainWindow->checkBox_noSecondaries->setChecked(general->noSecondaries);
  uiMainWindow->comboBox_ColorTheme->setCurrentIndex(general->colorTheme);
  uiMainWindow->checkBox_simplifiedGeometries->setChecked(general->simplifiedGeometries);
  uiMainWindow->checkBox_noMemoryLimitation->setChecked(general->noMemoryLimitation);
  uiMainWindow->checkBox_UseSplitting->setChecked(general->useSplitting);
  uiMainWindow->spinBox_EventNum->setValue(general->eventsPerChunk);
  uiMainWindow->checkBox_PipeToFile->setChecked(general->useSplitPiping);
}

void T4ISettingsFile::setInterfacePaths(void)
{
  T4SExternalFiles* extPaths = structM->getExternal();
  uiMainWindow->lineEdit_PathToBeamfile->setText(
    QString::fromStdString(extPaths->beamFile));
  uiMainWindow->lineEdit_PathToBeamfile_2->setText(
    QString::fromStdString(extPaths->beamFileForAdditionalPileUp));
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText(
      QString::fromStdString(extPaths->localGeneratorFile));
  uiMainWindow->lineEdit_Path_CMTX->setText(
      QString::fromStdString(extPaths->calorimeterInfo));
  
  uiMainWindow->libHEPGen_pi0->setText( QString::fromStdString(extPaths->libHEPGen_Pi0));
  uiMainWindow->libHEPGen_pi0_transv->setText( QString::fromStdString(extPaths->libHEPGen_Pi0_transv));
  uiMainWindow->libHEPGen_rho->setText( QString::fromStdString(extPaths->libHEPGen_Rho));
  uiMainWindow->visualizationMacro->setText( QString::fromStdString(extPaths->visualizationMacro));
  uiMainWindow->detectorEfficiencyFile->setText( QString::fromStdString(extPaths->detectorEfficiency));
  uiMainWindow->mtxInnerX->setText( QString::fromStdString(extPaths->triggerMatrixInnerX));
  uiMainWindow->mtxLadderX->setText( QString::fromStdString(extPaths->triggerMatrixLadderX));
  uiMainWindow->mtxLAST->setText( QString::fromStdString(extPaths->triggerMatrixLAST));
  uiMainWindow->mtxMiddleX->setText( QString::fromStdString(extPaths->triggerMatrixMiddleX));
  uiMainWindow->mtxMiddleY->setText( QString::fromStdString(extPaths->triggerMatrixMiddleY));
  uiMainWindow->mtxOuterY->setText( QString::fromStdString(extPaths->triggerMatrixOuterY));
}

void T4ISettingsFile::setInterfaceBeam(void)
{
  T4SBeam* beam = structM->getBeam();
  uiMainWindow->spinBox_numParticlesPerRun->setValue(beam->numParticles);
  uiMainWindow->spinBox_beamParticleId->setValue(beam->beamParticle);

  if (beam->beamPlugin == "BeamOnly")
    uiMainWindow->radioButton_BeamOnly->setChecked(true);
  else if (beam->beamPlugin == "VisualizationMode")
    uiMainWindow->radioButton_VisualizationMode->setChecked(true);
  else if (beam->beamPlugin == "HEPGEN") {
    uiMainWindow->radioButton_HEPGen->setChecked(true);
    setInterfaceHEPGen();
  } else if (beam->beamPlugin == "PYTHIA") {
    uiMainWindow->radioButton_Pythia6->setChecked(true);
  } else if (beam->beamPlugin == "PYTHIA8") {
    uiMainWindow->radioButton_Pythia8->setChecked(true);
  } else if (beam->beamPlugin == "LEPTO") {
    uiMainWindow->radioButton_Lepto->setChecked(true);
  } else if (beam->beamPlugin == "ascii")
    uiMainWindow->radioButton_ascii->setChecked(true);
  else if (beam->beamPlugin == "Primakoff") {
    uiMainWindow->radioButton_Primakoff->setChecked(true);
    setInterfacePrimGen();
  }
  else {
    uiMainWindow->radioButton_SelectionMode->setChecked(true);
    uiMainWindow->comboBox_BeamMode->setCurrentIndex(
        uiMainWindow->comboBox_BeamMode->findText(
            QString::fromStdString(beam->beamPlugin)));

    QString currentText = uiMainWindow->comboBox_BeamMode->currentText();
    if (currentText == "User")
      setInterfaceUser();
    else if (currentText == "Cosmics")
      setInterfaceCosmic();
    else if (currentText == "EcalCalib")
      setInterfaceEcalCalib();
  }

  uiMainWindow->doubleSpinBox_beamEnergy->setValue(beam->beamEnergy / 1000); // MeV => GeV
  uiMainWindow->checkBox_useBeamfile->setChecked(beam->useBeamfile);
  uiMainWindow->comboBox_beamfileType->setCurrentIndex(
      uiMainWindow->comboBox_beamfileType->findText(
          QString::fromStdString(beam->beamFileType)));
  uiMainWindow->comboBox_beamfileBackend->setCurrentIndex(
      uiMainWindow->comboBox_beamfileBackend->findText(
          QString::fromStdString(beam->beamFileBackend)));
  uiMainWindow->doubleSpinBox_beamFileZConvention->setValue(beam->beamFileZConvention);
  uiMainWindow->doubleSpinBox_beamFileZConvention_2->setValue(beam->beamFileZConventionForAdditionalPileUp);
  uiMainWindow->doubleSpinBox_beamZStart->setValue(beam->beamZStart);

  uiMainWindow->checkBox_usePileUp->setChecked(beam->usePileUp);
  uiMainWindow->checkBox_usePileUp_2->setChecked(beam->useAdditionalPileUp);
  uiMainWindow->doubleSpinBox_beamFlux->setValue(beam->beamFlux);
  uiMainWindow->doubleSpinBox_beamFlux_2->setValue(beam->additionalPileUpFlux);
  uiMainWindow->doubleSpinBox_TimeGate->setValue(beam->timeGate);
  uiMainWindow->checkBox_useTargetExtrap->setChecked(beam->useTargetExtrap);
  uiMainWindow->doubleSpinBox_targetStepLimit->setValue(beam->targetStepLimit);
  uiMainWindow->checkBox_useHadronicInteractionCall->setChecked(beam->useHadronicInteractionEGCall);
}

void T4ISettingsFile::setInterfaceSampling(void)
{
  T4SSampling* sampling = structM->getSampling();
  uiPhysicsList->doubleSpinBox_GANDALFbaseline->setValue(sampling->baseline);
  uiPhysicsList->checkBox_GANDALFbaseline->setChecked(sampling->useBaseline);
  uiPhysicsList->spinBox_windowSizeGandalf->setValue(sampling->windowSize);
  uiPhysicsList->radioButton_GANDALF_1GHZ->setChecked(
      sampling->binsPerNanoSecond == 1);
}

void T4ISettingsFile::setInterfaceUser(void)
{
  T4SUser* user = structM->getUser();
  uiUser->doubleSpinBox_PosX->setValue(user->position[0] / 10); // mm => cm
  uiUser->doubleSpinBox_PosY->setValue(user->position[1] / 10);
  uiUser->doubleSpinBox_PosZ->setValue(user->position[2] / 10);
  uiUser->doubleSpinBox_Px->setValue(user->momentum[0]);
  uiUser->doubleSpinBox_Py->setValue(user->momentum[1]);
  uiUser->doubleSpinBox_Pz->setValue(user->momentum[2]);
  uiUser->checkBox_useRandomEnergy->setChecked(user->useRandomEnergy);
  uiUser->doubleSpinBox_RandomEnergyMin->setValue(user->minEnergy);
  uiUser->doubleSpinBox_RandomEnergyMax->setValue(user->maxEnergy);
}

void T4ISettingsFile::setInterfaceCosmic(void)
{
  T4SCosmic* cosmic = structM->getCosmic();
  uiCosmics->doubleSpinBox_AngleVariation->setValue(cosmic->angleVariation);
  uiCosmics->doubleSpinBoxVariationX->setValue(cosmic->variationX / 10); // mm => cm
  uiCosmics->doubleSpinBoxVariationZ->setValue(cosmic->variationZ / 10);
  uiCosmics->doubleSpinBox_PosX->setValue(cosmic->positionX / 10);
  uiCosmics->doubleSpinBox_PosY->setValue(cosmic->positionZ / 10);
}

void T4ISettingsFile::setInterfaceEcalCalib(void)
{
  T4SEcalCalib* ecalcalib = structM->getEcalCalib();
  uiEcalCalib->doubleSpinBox_PosZ->setValue(ecalcalib->positionZ / 10);
  uiEcalCalib->doubleSpinBox_VariationXMin->setValue(ecalcalib->positionXMin / 10);
  uiEcalCalib->doubleSpinBox_VariationXMax->setValue(ecalcalib->positionXMax / 10);
  uiEcalCalib->doubleSpinBox_VariationYMin->setValue(ecalcalib->positionYMin / 10);
  uiEcalCalib->doubleSpinBox_VariationYMax->setValue(ecalcalib->positionYMax / 10);
  uiEcalCalib->doubleSpinBox_RandomEnergyMin->setValue(ecalcalib->energyMin / 1000);
  uiEcalCalib->doubleSpinBox_RandomEnergyMax->setValue(ecalcalib->energyMax / 1000);
}


void T4ISettingsFile::setInterfaceHEPGen(void)
{
  T4SHEPGen* hepgen = structM->getHEPGen();
  uiHEPGen->comboBox_Target->setCurrentIndex(
      uiHEPGen->comboBox_Target->findText(
          QString::fromStdString(hepgen->target)));

  QString ivecm;
  switch (hepgen->IVECM) {
    case 0:
      ivecm = "0: DVCS";
      break;
    case 1:
      ivecm = "1: Pi0 -> yy";
      break;
    case 2:
      ivecm = "2: Rho0 -> Pi Pi";
      break;
    case 3:
      ivecm = "3: Phi -> K K";
      break;
    case 4:
      ivecm = "4: J/Psi -> e+ e- and mu+ mu-";
      break;
    case 6:
      ivecm = "6: Omega-> Pi Pi Pi";
      break;
    case 7:
      ivecm = "7: Rho+ -> Pi yy";
      break;
    case 8:
      ivecm = "8: Omega -> y Pi0";
      break;
    default:
      std::cerr
          << "Error in T4ISettingsFile::writeHEPGen(void). IVECM not identified."
          << std::endl;
      break;
  }
  uiHEPGen->comboBox_IVECM->setCurrentIndex(
      uiHEPGen->comboBox_IVECM->findText(ivecm));

  uiHEPGen->checkBox_diffractiveDissociation->setChecked(hepgen->LST == 1);
  uiHEPGen->doubleSpinBox_x_min->setValue(hepgen->CUT[0]);
  uiHEPGen->doubleSpinBox_x_max->setValue(hepgen->CUT[1]);
  uiHEPGen->doubleSpinBox_y_min->setValue(hepgen->CUT[2]);
  uiHEPGen->doubleSpinBox_y_max->setValue(hepgen->CUT[3]);
  uiHEPGen->doubleSpinBox_Q2_min->setValue(hepgen->CUT[4]);
  uiHEPGen->doubleSpinBox_Q2_max->setValue(hepgen->CUT[5]);
  uiHEPGen->doubleSpinBox_W2_min->setValue(hepgen->CUT[6]);
  uiHEPGen->doubleSpinBox_W2_max->setValue(hepgen->CUT[7]);
  uiHEPGen->doubleSpinBox_nu_min->setValue(hepgen->CUT[8]);
  uiHEPGen->doubleSpinBox_nu_max->setValue(hepgen->CUT[9]);
  uiHEPGen->doubleSpinBox_E_min->setValue(hepgen->CUT[10]);
  uiHEPGen->doubleSpinBox_E_max->setValue(hepgen->CUT[11]);
  uiHEPGen->doubleSpinBox_phi_min->setValue(hepgen->CUT[12]);
  uiHEPGen->doubleSpinBox_phi_max->setValue(hepgen->CUT[13]);

  uiHEPGen->doubleSpinBox_MACC->setValue(hepgen->THMAX);
  uiHEPGen->doubleSpinBox_ALFA->setValue(hepgen->alf);
  uiHEPGen->doubleSpinBox_TPAR_A->setValue(hepgen->atomas);
  uiHEPGen->doubleSpinBox_TPAR_probc->setValue(hepgen->probc);
  uiHEPGen->doubleSpinBox_TPAR_bcoh->setValue(hepgen->bcoh);
  uiHEPGen->doubleSpinBox_TPAR_bin->setValue(hepgen->bin);
  uiHEPGen->doubleSpinBox_BPAR_charge->setValue(hepgen->clept);
  uiHEPGen->doubleSpinBox_BPAR_polarisation->setValue(hepgen->slept);
  uiHEPGen->doubleSpinBox_DVCS_B0->setValue(hepgen->B0);
  uiHEPGen->doubleSpinBox_DVCS_xbj0->setValue(hepgen->xbj0);
  uiHEPGen->doubleSpinBox_DVCS_alphap->setValue(hepgen->alphap);
  uiHEPGen->doubleSpinBox_TLIM1->setValue(hepgen->TLIM[0]);
  uiHEPGen->doubleSpinBox_TLIM2->setValue(hepgen->TLIM[1]);
  uiHEPGen->checkBox_usePi_transv_table->setChecked(hepgen->usePi0_transv_table);
}

void T4ISettingsFile::setInterfacePrimGen(void)
{
  T4SPrimGen* primgen = structM->getPrimGen();
  std::string particle = primgen->particle;
  if(particle == "pion")
    uiPrimGen->comboBox_Particle->setCurrentIndex(0);
  else
    uiPrimGen->comboBox_Particle->setCurrentIndex(1);

  uiPrimGen->doubleSpinBox_Z->setValue(primgen->Z);
  uiPrimGen->doubleSpinBox_alpha_1->setValue(primgen->alpha_1);
  uiPrimGen->doubleSpinBox_alpha_2->setValue(primgen->alpha_2);
  uiPrimGen->doubleSpinBox_beta_1->setValue(primgen->beta_1);
  uiPrimGen->doubleSpinBox_beta_2->setValue(primgen->beta_2);
  uiPrimGen->doubleSpinBox_s_min->setValue(primgen->s_min);
  uiPrimGen->doubleSpinBox_s_max->setValue(primgen->s_max);
  uiPrimGen->doubleSpinBox_Q2_max->setValue(primgen->Q2_max);
  uiPrimGen->doubleSpinBox_Egamma_min->setValue(primgen->Egamma_min);
  uiPrimGen->doubleSpinBox_pbeam_min->setValue(primgen->pbeam_min);
  uiPrimGen->doubleSpinBox_pbeam_max->setValue(primgen->pbeam_max);
  uiPrimGen->CheckboxBorn->setChecked(primgen->born);
  uiPrimGen->CheckboxRho->setChecked(primgen->rhoContrib);
  uiPrimGen->CheckboxChiralLoops->setChecked(primgen->chiralLoops);
  uiPrimGen->Checkboxpolarizabilities->setChecked(primgen->polarizabilities);
  uiPrimGen->CheckboxRadCorr->setChecked(primgen->radcorr);
}

void T4ISettingsFile::setInterfacePrimTrigger(void)
{
  T4SPrimTrig* primtrig = structM->getPrimTrig();
  uiPrimTrig->doubleSpinBox_ThresholdPrim1->setValue(primtrig->threshPrim1);
  uiPrimTrig->doubleSpinBox_ThresholdPrim2_Maximum->setValue(primtrig->threshPrim2);
  uiPrimTrig->doubleSpinBox_ThresholdPrim3->setValue(primtrig->threshPrim3);
  uiPrimTrig->doubleSpinBox_ThresholdPrim2_Veto->setValue(primtrig->threshPrim2Veto);
}

void T4ISettingsFile::setInterfaceCalorimeter(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_ECAL0->setChecked(false);
  uiMainWindow->checkBox_ECAL1->setChecked(false);
  uiMainWindow->checkBox_ECAL2->setChecked(false);
  uiMainWindow->checkBox_HCAL1->setChecked(false);
  uiMainWindow->checkBox_HCAL2->setChecked(false);

  for (unsigned int i = 0; i < structM->getCalorimeter()->size(); i++) {
    const T4SDetector* calo = &structM->getCalorimeter()->at(i);
    if (calo->name == "ECAL0") {
      uiMainWindow->checkBox_ECAL0->setChecked(true);
      uiMainWindow->doubleSpinBox_PosECAL0X->setValue(calo->position[0] / 10); // mm => cm
      uiMainWindow->doubleSpinBox_PosECAL0Y->setValue(calo->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosECAL0Z->setValue(calo->position[2] / 10);
      uiMainWindow->checkBox_ECAL0_MechStructure->setChecked(
          calo->useMechanicalStructure);
    } else if (calo->name == "ECAL1") {
      uiMainWindow->checkBox_ECAL1->setChecked(true);
      uiMainWindow->doubleSpinBox_PosECAL1X->setValue(calo->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosECAL1Y->setValue(calo->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosECAL1Z->setValue(calo->position[2] / 10);
      uiMainWindow->checkBox_ECAL1_MechStructure->setChecked(
          calo->useMechanicalStructure);
    } else if (calo->name == "ECAL2") {
      uiMainWindow->checkBox_ECAL2->setChecked(true);
      uiMainWindow->doubleSpinBox_PosECAL2X->setValue(calo->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosECAL2Y->setValue(calo->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosECAL2Z->setValue(calo->position[2] / 10);
      uiMainWindow->checkBox_ECAL2_MechStructure->setChecked(
          calo->useMechanicalStructure);
    } else if (calo->name == "HCAL1") {
      uiMainWindow->checkBox_HCAL1->setChecked(true);
      uiMainWindow->doubleSpinBox_PosHCAL1X->setValue(calo->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosHCAL1Y->setValue(calo->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosHCAL1Z->setValue(calo->position[2] / 10);
      uiMainWindow->checkBox_HCAL1_MechStructure->setChecked(
          calo->useMechanicalStructure);
    } else if (calo->name == "HCAL2") {
      uiMainWindow->checkBox_HCAL2->setChecked(true);
      uiMainWindow->doubleSpinBox_PosHCAL2X->setValue(calo->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosHCAL2Y->setValue(calo->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosHCAL2Z->setValue(calo->position[2] / 10);
      uiMainWindow->checkBox_HCAL2_MechStructure->setChecked(
          calo->useMechanicalStructure);
    }
  }
}

void T4ISettingsFile::setInterfaceMW1(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_MW1->setChecked(false);

  T4SMW1* mw1 = structM->getMW1();
  if (!mw1->general.useDetector)
    return;

  uiMainWindow->checkBox_MW1->setChecked(true);
  uiMainWindow->doubleSpinBox_MW1_AbsorberX->setValue(
      mw1->general.position[0] / 10); // mm => cm
  uiMainWindow->doubleSpinBox_MW1_AbsorberY->setValue(
      mw1->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_MW1_AbsorberZ->setValue(
      mw1->general.position[2] / 10);

  uiMainWindow->checkBox_MW1_useAbsorber->setChecked(mw1->useAbsorber);
  uiMainWindow->checkBox_MA01X1->setChecked(mw1->useMA01X1);
  uiMainWindow->checkBox_MA01X3->setChecked(mw1->useMA01X3);
  uiMainWindow->checkBox_MA01Y1->setChecked(mw1->useMA01Y1);
  uiMainWindow->checkBox_MA01Y3->setChecked(mw1->useMA01Y3);
  uiMainWindow->checkBox_MA02X1->setChecked(mw1->useMA02X1);
  uiMainWindow->checkBox_MA02X3->setChecked(mw1->useMA02X3);
  uiMainWindow->checkBox_MA02Y1->setChecked(mw1->useMA02Y1);
  uiMainWindow->checkBox_MA02Y3->setChecked(mw1->useMA02Y3);

  uiMainWindow->doubleSpinBox_MA01X1_X->setValue(mw1->posMA01X1[0] / 10);
  uiMainWindow->doubleSpinBox_MA01X1_Y->setValue(mw1->posMA01X1[1] / 10);
  uiMainWindow->doubleSpinBox_MA01X1_Z->setValue(mw1->posMA01X1[2] / 10);

  uiMainWindow->doubleSpinBox_MA01X3_X->setValue(mw1->posMA01X3[0] / 10);
  uiMainWindow->doubleSpinBox_MA01X3_Y->setValue(mw1->posMA01X3[1] / 10);
  uiMainWindow->doubleSpinBox_MA01X3_Z->setValue(mw1->posMA01X3[2] / 10);

  uiMainWindow->doubleSpinBox_MA01Y1_X->setValue(mw1->posMA01Y1[0] / 10);
  uiMainWindow->doubleSpinBox_MA01Y1_Y->setValue(mw1->posMA01Y1[1] / 10);
  uiMainWindow->doubleSpinBox_MA01Y1_Z->setValue(mw1->posMA01Y1[2] / 10);

  uiMainWindow->doubleSpinBox_MA01Y3_X->setValue(mw1->posMA01Y3[0] / 10);
  uiMainWindow->doubleSpinBox_MA01Y3_Y->setValue(mw1->posMA01Y3[1] / 10);
  uiMainWindow->doubleSpinBox_MA01Y3_Z->setValue(mw1->posMA01Y3[2] / 10);

  uiMainWindow->doubleSpinBox_MA02X1_X->setValue(mw1->posMA02X1[0] / 10);
  uiMainWindow->doubleSpinBox_MA02X1_Y->setValue(mw1->posMA02X1[1] / 10);
  uiMainWindow->doubleSpinBox_MA02X1_Z->setValue(mw1->posMA02X1[2] / 10);

  uiMainWindow->doubleSpinBox_MA02X3_X->setValue(mw1->posMA02X3[0] / 10);
  uiMainWindow->doubleSpinBox_MA02X3_Y->setValue(mw1->posMA02X3[1] / 10);
  uiMainWindow->doubleSpinBox_MA02X3_Z->setValue(mw1->posMA02X3[2] / 10);

  uiMainWindow->doubleSpinBox_MA02Y1_X->setValue(mw1->posMA02Y1[0] / 10);
  uiMainWindow->doubleSpinBox_MA02Y1_Y->setValue(mw1->posMA02Y1[1] / 10);
  uiMainWindow->doubleSpinBox_MA02Y1_Z->setValue(mw1->posMA02Y1[2] / 10);

  uiMainWindow->doubleSpinBox_MA02Y3_X->setValue(mw1->posMA02Y3[0] / 10);
  uiMainWindow->doubleSpinBox_MA02Y3_Y->setValue(mw1->posMA02Y3[1] / 10);
  uiMainWindow->doubleSpinBox_MA02Y3_Z->setValue(mw1->posMA02Y3[2] / 10);
}

void T4ISettingsFile::setInterfaceMW2(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_MW2->setChecked(false);

  T4SMW2* mw2 = structM->getMW2();
  if (!mw2->general.useDetector)
    return;

  uiMainWindow->checkBox_MW2->setChecked(true);
  uiMainWindow->doubleSpinBox_PosMW2X->setValue(mw2->general.position[0] / 10); // mm => cm
  uiMainWindow->doubleSpinBox_PosMW2Y->setValue(mw2->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_PosMW2Z->setValue(mw2->general.position[2] / 10);

  uiMainWindow->checkBox_MW2_Absorber->setChecked(mw2->useAbsorber);
  uiMainWindow->checkBox_MB01X->setChecked(mw2->MB01X);
  uiMainWindow->checkBox_MB01Y->setChecked(mw2->MB01Y);
  uiMainWindow->checkBox_MB01V->setChecked(mw2->MB01V);
  uiMainWindow->checkBox_MB02X->setChecked(mw2->MB02X);
  uiMainWindow->checkBox_MB02Y->setChecked(mw2->MB02Y);
  uiMainWindow->checkBox_MB02V->setChecked(mw2->MB02V);

  uiMainWindow->doubleSpinBox_MB01X_X->setValue(mw2->positionMB01X[0] / 10);
  uiMainWindow->doubleSpinBox_MB01X_Y->setValue(mw2->positionMB01X[1] / 10);
  uiMainWindow->doubleSpinBox_MB01X_Z->setValue(mw2->positionMB01X[2] / 10);

  uiMainWindow->doubleSpinBox_MB01Y_X->setValue(mw2->positionMB01Y[0] / 10);
  uiMainWindow->doubleSpinBox_MB01Y_Y->setValue(mw2->positionMB01Y[1] / 10);
  uiMainWindow->doubleSpinBox_MB01Y_Z->setValue(mw2->positionMB01Y[2] / 10);

  uiMainWindow->doubleSpinBox_MB01V_X->setValue(mw2->positionMB01V[0] / 10);
  uiMainWindow->doubleSpinBox_MB01V_Y->setValue(mw2->positionMB01V[1] / 10);
  uiMainWindow->doubleSpinBox_MB01V_Z->setValue(mw2->positionMB01V[2] / 10);

  uiMainWindow->doubleSpinBox_MB02X_X->setValue(mw2->positionMB02X[0] / 10);
  uiMainWindow->doubleSpinBox_MB02X_Y->setValue(mw2->positionMB02X[1] / 10);
  uiMainWindow->doubleSpinBox_MB02X_Z->setValue(mw2->positionMB02X[2] / 10);

  uiMainWindow->doubleSpinBox_MB02Y_X->setValue(mw2->positionMB02Y[0] / 10);
  uiMainWindow->doubleSpinBox_MB02Y_Y->setValue(mw2->positionMB02Y[1] / 10);
  uiMainWindow->doubleSpinBox_MB02Y_Z->setValue(mw2->positionMB02Y[2] / 10);

  uiMainWindow->doubleSpinBox_MB02V_X->setValue(mw2->positionMB02V[0] / 10);
  uiMainWindow->doubleSpinBox_MB02V_Y->setValue(mw2->positionMB02V[1] / 10);
  uiMainWindow->doubleSpinBox_MB02V_Z->setValue(mw2->positionMB02V[2] / 10);
}

void T4ISettingsFile::setInterfaceStraw(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_ST02->setChecked(false);
  uiMainWindow->checkBox_ST02X1->setChecked(false);
  uiMainWindow->checkBox_ST02X2->setChecked(false);
  uiMainWindow->checkBox_ST02Y1->setChecked(false);
  uiMainWindow->checkBox_ST02Y2->setChecked(false);
  uiMainWindow->checkBox_ST02U1->setChecked(false);
  uiMainWindow->checkBox_ST02V1->setChecked(false);
  uiMainWindow->checkBox_ST03->setChecked(false);
  uiMainWindow->checkBox_ST03X1->setChecked(false);
  uiMainWindow->checkBox_ST03X2->setChecked(false);
  uiMainWindow->checkBox_ST03Y1->setChecked(false);
  uiMainWindow->checkBox_ST03Y2->setChecked(false);
  uiMainWindow->checkBox_ST03U1->setChecked(false);
  uiMainWindow->checkBox_ST03V1->setChecked(false);
  uiMainWindow->checkBox_ST05->setChecked(false);
  uiMainWindow->checkBox_ST05X1->setChecked(false);
  uiMainWindow->checkBox_ST05Y2->setChecked(false);
  uiMainWindow->checkBox_ST05U1->setChecked(false);

  for (unsigned int i = 0; i < structM->getStraw()->size(); i++) {
    const T4SDetector* straw = &structM->getStraw()->at(i);

    // ST02
    if (straw->name == "ST02X1") {
      uiMainWindow->checkBox_ST02X1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02X1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02X1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02X1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02X1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
    } else if (straw->name == "ST02X2") {
      uiMainWindow->checkBox_ST02X2->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02X2_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02X2_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02X2_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02X2_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
    } else if (straw->name == "ST02Y1") {
      uiMainWindow->checkBox_ST02Y1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02Y1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02Y1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02Y1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02Y1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
    } else if (straw->name == "ST02Y2") {
      uiMainWindow->checkBox_ST02Y2->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02Y2_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02Y2_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02Y2_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02Y2_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
    } else if (straw->name == "ST02U1") {
      uiMainWindow->checkBox_ST02U1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02U1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02U1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02U1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02U1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
    } else if (straw->name == "ST02V1") {
      uiMainWindow->checkBox_ST02V1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST02V1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST02V1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST02V1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST02V1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST02->setChecked(true);
      // ST03
    } else if (straw->name == "ST03X1") {
      uiMainWindow->checkBox_ST03X1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03X1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03X1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03X1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03X1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
    } else if (straw->name == "ST03X2") {
      uiMainWindow->checkBox_ST03X2->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03X2_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03X2_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03X2_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03X2_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
    } else if (straw->name == "ST03Y1") {
      uiMainWindow->checkBox_ST03Y1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03Y1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03Y1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03Y1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03Y1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
    } else if (straw->name == "ST03Y2") {
      uiMainWindow->checkBox_ST03Y2->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03Y2_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03Y2_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03Y2_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03Y2_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
    } else if (straw->name == "ST03U1") {
      uiMainWindow->checkBox_ST03U1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03U1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03U1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03U1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03U1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
    } else if (straw->name == "ST03V1") {
      uiMainWindow->checkBox_ST03V1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST03V1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST03V1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST03V1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST03V1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST03->setChecked(true);
      // ST05
    } else if (straw->name == "ST05X1") {
      uiMainWindow->checkBox_ST05X1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST05X1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST05X1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST05X1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST05X1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST05->setChecked(true);
    } else if (straw->name == "ST05Y2") {
      uiMainWindow->checkBox_ST05Y2->setChecked(true);
      uiMainWindow->doubleSpinBox_ST05Y2_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST05Y2_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST05Y2_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST05Y2_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST05->setChecked(true);
    } else if (straw->name == "ST05U1") {
      uiMainWindow->checkBox_ST05U1->setChecked(true);
      uiMainWindow->doubleSpinBox_ST05U1_X->setValue(straw->position[0] / 10);
      uiMainWindow->doubleSpinBox_ST05U1_Y->setValue(straw->position[1] / 10);
      uiMainWindow->doubleSpinBox_ST05U1_Z->setValue(straw->position[2] / 10);
      uiMainWindow->checkBox_ST05U1_mech->setChecked(
          straw->useMechanicalStructure);
      uiMainWindow->checkBox_ST05->setChecked(true);
    }
  }
}

void T4ISettingsFile::setInterfaceMicromegas(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_MM01->setChecked(false);
  uiMainWindow->checkBox_MM01X->setChecked(false);
  uiMainWindow->checkBox_MM01Y->setChecked(false);
  uiMainWindow->checkBox_MM01V->setChecked(false);
  uiMainWindow->checkBox_MM01U->setChecked(false);
  uiMainWindow->checkBox_MM02->setChecked(false);
  uiMainWindow->checkBox_MM02X->setChecked(false);
  uiMainWindow->checkBox_MM02Y->setChecked(false);
  uiMainWindow->checkBox_MM02V->setChecked(false);
  uiMainWindow->checkBox_MM02U->setChecked(false);
  uiMainWindow->checkBox_MM03->setChecked(false);
  uiMainWindow->checkBox_MM03X->setChecked(false);
  uiMainWindow->checkBox_MM03Y->setChecked(false);
  uiMainWindow->checkBox_MM03V->setChecked(false);
  uiMainWindow->checkBox_MM03U->setChecked(false);
  uiMainWindow->checkBox_MP00->setChecked(false);
  uiMainWindow->checkBox_MP00X->setChecked(false);
  uiMainWindow->checkBox_MP01->setChecked(false);
  uiMainWindow->checkBox_MP01X->setChecked(false);
  uiMainWindow->checkBox_MP01Y->setChecked(false);
  uiMainWindow->checkBox_MP01U->setChecked(false);
  uiMainWindow->checkBox_MP01V->setChecked(false);
  uiMainWindow->checkBox_MP02->setChecked(false);
  uiMainWindow->checkBox_MP02X->setChecked(false);
  uiMainWindow->checkBox_MP02Y->setChecked(false);
  uiMainWindow->checkBox_MP02U->setChecked(false);
  uiMainWindow->checkBox_MP02V->setChecked(false);
  uiMainWindow->checkBox_MP03->setChecked(false);
  uiMainWindow->checkBox_MP03X->setChecked(false);
  uiMainWindow->checkBox_MP03Y->setChecked(false);
  uiMainWindow->checkBox_MP03U->setChecked(false);
  uiMainWindow->checkBox_MP03V->setChecked(false);

  for (unsigned int i = 0; i < structM->getMicromegas()->size(); i++) {
    const T4SDetector* mm = &structM->getMicromegas()->at(i);
    if (mm->name == "MM01X1__") {
      uiMainWindow->checkBox_MM01->setChecked(true);
      uiMainWindow->checkBox_MM01X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM01X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM01X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM01X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM01Y1__") {
      uiMainWindow->checkBox_MM01->setChecked(true);
      uiMainWindow->checkBox_MM01Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM01Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM01Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM01Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM01V1__") {
      uiMainWindow->checkBox_MM01->setChecked(true);
      uiMainWindow->checkBox_MM01V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM01V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM01V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM01V_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM01U1__") {
      uiMainWindow->checkBox_MM01->setChecked(true);
      uiMainWindow->checkBox_MM01U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM01U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM01U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM01U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM02X1__") {
      uiMainWindow->checkBox_MM02->setChecked(true);
      uiMainWindow->checkBox_MM02X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM02X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM02X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM02X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM02Y1__") {
      uiMainWindow->checkBox_MM02->setChecked(true);
      uiMainWindow->checkBox_MM02Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM02Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM02Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM02Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM02V1__") {
      uiMainWindow->checkBox_MM02->setChecked(true);
      uiMainWindow->checkBox_MM02V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM02V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM02V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM02V_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM02U1__") {
      uiMainWindow->checkBox_MM02->setChecked(true);
      uiMainWindow->checkBox_MM02U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM02U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM02U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM02U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM03X1__") {
      uiMainWindow->checkBox_MM03->setChecked(true);
      uiMainWindow->checkBox_MM03X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM03X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM03X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM03X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM03Y1__") {
      uiMainWindow->checkBox_MM03->setChecked(true);
      uiMainWindow->checkBox_MM03Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM03Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM03Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM03Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM03V1__") {
      uiMainWindow->checkBox_MM03->setChecked(true);
      uiMainWindow->checkBox_MM03V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM03V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM03V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM03V_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MM03U1__") {
      uiMainWindow->checkBox_MM03->setChecked(true);
      uiMainWindow->checkBox_MM03U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMM03U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMM03U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMM03U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP00X1__") {
      uiMainWindow->checkBox_MP00->setChecked(true);
      uiMainWindow->checkBox_MP00X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP00X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP00X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP00X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP01X1__") {
      uiMainWindow->checkBox_MP01->setChecked(true);
      uiMainWindow->checkBox_MP01X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP01X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP01X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP01X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP01Y1__") {
      uiMainWindow->checkBox_MP01->setChecked(true);
      uiMainWindow->checkBox_MP01Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP01Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP01Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP01Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP01U1__") {
      uiMainWindow->checkBox_MP01->setChecked(true);
      uiMainWindow->checkBox_MP01U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP01U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP01U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP01U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP01V1__") {
      uiMainWindow->checkBox_MP01->setChecked(true);
      uiMainWindow->checkBox_MP01V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP01V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP01V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP01V_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP02X1__") {
      uiMainWindow->checkBox_MP02->setChecked(true);
      uiMainWindow->checkBox_MP02X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP02X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP02X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP02X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP02Y1__") {
      uiMainWindow->checkBox_MP02->setChecked(true);
      uiMainWindow->checkBox_MP02Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP02Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP02Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP02Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP02U1__") {
      uiMainWindow->checkBox_MP02->setChecked(true);
      uiMainWindow->checkBox_MP02U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP02U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP02U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP02U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP02V1__") {
      uiMainWindow->checkBox_MP02->setChecked(true);
      uiMainWindow->checkBox_MP02V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP02V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP02V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP02V_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP03X1__") {
      uiMainWindow->checkBox_MP03->setChecked(true);
      uiMainWindow->checkBox_MP03X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP03X_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP03X_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP03X_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP03Y1__") {
      uiMainWindow->checkBox_MP03->setChecked(true);
      uiMainWindow->checkBox_MP03Y->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP03Y_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP03Y_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP03Y_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP03U1__") {
      uiMainWindow->checkBox_MP03->setChecked(true);
      uiMainWindow->checkBox_MP03U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP03U_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP03U_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP03U_Z->setValue(mm->position[2] / 10);
    } else if (mm->name == "MP03V1__") {
      uiMainWindow->checkBox_MP03->setChecked(true);
      uiMainWindow->checkBox_MP03V->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMP03V_X->setValue(mm->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMP03V_Y->setValue(mm->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMP03V_Z->setValue(mm->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceGem(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_GEM->setChecked(false);
  uiMainWindow->checkBox_GM01X->setChecked(false);
  uiMainWindow->checkBox_GM01U->setChecked(false);
  uiMainWindow->checkBox_GM02X->setChecked(false);
  uiMainWindow->checkBox_GM02U->setChecked(false);
  uiMainWindow->checkBox_GM03X->setChecked(false);
  uiMainWindow->checkBox_GM03U->setChecked(false);
  uiMainWindow->checkBox_GM04X->setChecked(false);
  uiMainWindow->checkBox_GM04U->setChecked(false);
  uiMainWindow->checkBox_GM05X->setChecked(false);
  uiMainWindow->checkBox_GM05U->setChecked(false);
  uiMainWindow->checkBox_GM06X->setChecked(false);
  uiMainWindow->checkBox_GM06U->setChecked(false);
  uiMainWindow->checkBox_GM11X->setChecked(false);
  uiMainWindow->checkBox_GM11U->setChecked(false);

  uiMainWindow->checkBox_PGEM->setChecked(false);
  uiMainWindow->checkBox_GP01X->setChecked(false);
  uiMainWindow->checkBox_GP01U->setChecked(false);
  uiMainWindow->checkBox_GP02X->setChecked(false);
  uiMainWindow->checkBox_GP02U->setChecked(false);
  uiMainWindow->checkBox_GP03X->setChecked(false);
  uiMainWindow->checkBox_GP03U->setChecked(false);

  for (unsigned int i = 0; i < structM->getGem()->size(); i++) {
    const T4SDetector* gem = &structM->getGem()->at(i);
    if (gem->name == "GM01X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM01X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM01X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM01X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM01X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM01U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM01U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM01U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM01U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM01U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM02X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM02X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM02X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM02X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM02X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM02U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM02U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM02U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM02U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM02U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM03X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM03X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM03X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM03X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM03X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM03U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM03U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM03U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM03U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM03U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM04X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM04X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM04X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM04X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM04X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM04U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM04U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM04U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM04U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM04U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM05X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM05X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM05X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM05X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM05X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM05U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM05U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM05U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM05U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM05U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM06X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM06X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM06X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM06X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM06X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM06U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM06U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM06U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM06U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM06U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM07X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM07X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM07X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM07X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM07X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM07U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM07U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM07U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM07U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM07U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM08X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM08X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM08X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM08X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM08X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM08U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM08U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM08U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM08U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM08U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM09X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM09X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM09X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM09X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM09X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM09U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM09U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM09U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM09U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM09U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM10X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM10X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM10X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM10X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM10X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM10U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM10U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM10U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM10U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM10U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM11X1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM11X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM11X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM11X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM11X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GM11U1__") {
      uiMainWindow->checkBox_GEM->setChecked(true);
      uiMainWindow->checkBox_GM11U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGM11U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGM11U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGM11U_Z->setValue(gem->position[2] / 10);

    } else if (gem->name == "GP01X1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP01X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP01X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP01X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP01X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GP01U1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP01U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP01U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP01U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP01U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GP02X1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP02X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP02X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP02X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP02X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GP02U1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP02U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP02U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP02U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP02U_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GP03X1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP03X->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP03X_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP03X_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP03X_Z->setValue(gem->position[2] / 10);
    } else if (gem->name == "GP03U1__") {
      uiMainWindow->checkBox_PGEM->setChecked(true);
      uiMainWindow->checkBox_GP03U->setChecked(true);
      uiMainWindow->doubleSpinBox_PosGP03U_X->setValue(gem->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosGP03U_Y->setValue(gem->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosGP03U_Z->setValue(gem->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceSciFi(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_FI->setChecked(false);
  uiMainWindow->checkBox_FI01X->setChecked(false);
  uiMainWindow->checkBox_FI15X->setChecked(false);
  uiMainWindow->checkBox_FI02X->setChecked(false);
  uiMainWindow->checkBox_FI03X->setChecked(false);
  uiMainWindow->checkBox_FI35X->setChecked(false);
  uiMainWindow->checkBox_FI04X->setChecked(false);
  uiMainWindow->checkBox_FI05X->setChecked(false);
  uiMainWindow->checkBox_FI55U->setChecked(false);
  uiMainWindow->checkBox_FI06X->setChecked(false);
  uiMainWindow->checkBox_FI07X->setChecked(false);
  uiMainWindow->checkBox_FI08X->setChecked(false);

  for (unsigned int i = 0; i < structM->getSciFi()->size(); i++) {
    const T4SDetector* det = &structM->getSciFi()->at(i);
    if (det->name == "FI01X1__") {
      uiMainWindow->checkBox_FI01X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI01X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI01X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI01X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI15X1__") {
      uiMainWindow->checkBox_FI15X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI15X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI15X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI15X_Z->setValue(det->position[2] / 10);
      uiMainWindow->checkBox_FI15X_useU->setChecked(det->useMechanicalStructure);
    } else if (det->name == "FI02X1__") {
      uiMainWindow->checkBox_FI02X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI02X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI02X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI02X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI03X1__") {
      uiMainWindow->checkBox_FI03X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI03X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI03X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI03X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI35X1__") {
      uiMainWindow->checkBox_FI35X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI35X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI35X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI35X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI04X1__") {
      uiMainWindow->checkBox_FI04X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI04X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI04X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI04X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI05X1__") {
      uiMainWindow->checkBox_FI05X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI05X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI05X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI05X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI55U1__") {
      uiMainWindow->checkBox_FI55U->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI55U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI55U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI55U_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI06X1__") {
      uiMainWindow->checkBox_FI06X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI06X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI06X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI06X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI07X1__") {
      uiMainWindow->checkBox_FI07X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI07X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI07X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI07X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "FI08X1__") {
      uiMainWindow->checkBox_FI08X->setChecked(true);
      uiMainWindow->checkBox_FI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosFI08X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosFI08X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosFI08X_Z->setValue(det->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceSilicon(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_SI->setChecked(false);
  uiMainWindow->checkBox_SI01X->setChecked(false);
  uiMainWindow->checkBox_SI01U->setChecked(false);
  uiMainWindow->checkBox_SI02X->setChecked(false);
  uiMainWindow->checkBox_SI02U->setChecked(false);
  uiMainWindow->checkBox_SI03X->setChecked(false);
  uiMainWindow->checkBox_SI03U->setChecked(false);
  uiMainWindow->checkBox_SI04X->setChecked(false);
  uiMainWindow->checkBox_SI04U->setChecked(false);
  uiMainWindow->checkBox_SI05X->setChecked(false);
  uiMainWindow->checkBox_SI05U->setChecked(false);

  for (unsigned int i = 0; i < structM->getSilicon()->size(); i++) {
    const T4SDetector* det = &structM->getSilicon()->at(i);
    if (det->name == "SI01X1__") {
      uiMainWindow->checkBox_SI01X->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI01X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI01X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI01X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI01U1__") {
      uiMainWindow->checkBox_SI01U->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI01U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI01U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI01U_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI02X1__") {
      uiMainWindow->checkBox_SI02X->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI02X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI02X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI02X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI02U1__") {
      uiMainWindow->checkBox_SI02U->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI02U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI02U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI02U_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI03X1__") {
      uiMainWindow->checkBox_SI03X->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI03X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI03X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI03X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI03U1__") {
      uiMainWindow->checkBox_SI03U->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI03U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI03U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI03U_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI04X1__") {
      uiMainWindow->checkBox_SI04X->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI04X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI04X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI04X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI04U1__") {
      uiMainWindow->checkBox_SI04U->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI04U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI04U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI04U_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI05X1__") {
      uiMainWindow->checkBox_SI05X->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI05X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI05X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI05X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "SI05U1__") {
      uiMainWindow->checkBox_SI05U->setChecked(true);
      uiMainWindow->checkBox_SI->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSI05U_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSI05U_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSI05U_Z->setValue(det->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceMWPC(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_PA->setChecked(false);
  uiMainWindow->checkBox_PB->setChecked(false);
  uiMainWindow->checkBox_PS->setChecked(false);

  for (unsigned int i = 0; i < structM->getMWPC()->size(); i++) {
    const T4SDetector* det = &structM->getMWPC()->at(i);
    if (det->name == "PA01X1__") {
      uiMainWindow->checkBox_PA01X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA01X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA01X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA01X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA02X1__") {
      uiMainWindow->checkBox_PA02X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA02X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA02X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA02X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA03X1__") {
      uiMainWindow->checkBox_PA03X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA03X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA03X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA03X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA04X1__") {
      uiMainWindow->checkBox_PA04X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA04X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA04X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA04X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA05X1__") {
      uiMainWindow->checkBox_PA05X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA05X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA05X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA05X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA06X1__") {
      uiMainWindow->checkBox_PA06X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA06X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA06X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA06X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PA11X1__") {
      uiMainWindow->checkBox_PA11X->setChecked(true);
      uiMainWindow->checkBox_PA->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPA11X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPA11X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPA11X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB01X1__") {
      uiMainWindow->checkBox_PB01X->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB01X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB01X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB01X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB03X1__") {
      uiMainWindow->checkBox_PB03X->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB03X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB03X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB03X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB05X1__") {
      uiMainWindow->checkBox_PB05X->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB05X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB05X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB05X_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB02V1__") {
      uiMainWindow->checkBox_PB02V->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB02V_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB02V_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB02V_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB04V1__") {
      uiMainWindow->checkBox_PB04V->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB04V_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB04V_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB04V_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PB06V1__") {
      uiMainWindow->checkBox_PB06V->setChecked(true);
      uiMainWindow->checkBox_PB->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPB06V_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPB06V_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPB06V_Z->setValue(det->position[2] / 10);
    } else if (det->name == "PS01X1__") {
      uiMainWindow->checkBox_PS01X->setChecked(true);
      uiMainWindow->checkBox_PS->setChecked(true);
      uiMainWindow->doubleSpinBox_PosPS01X_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosPS01X_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosPS01X_Z->setValue(det->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceVeto(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_Veto->setChecked(false);

  for (unsigned int i = 0; i < structM->getVeto()->size(); i++) {
    const T4SDetector* det = &structM->getVeto()->at(i);
    if (det->name == "VI01P1__") {
      uiMainWindow->checkBox_VI01P1->setChecked(true);
      uiMainWindow->checkBox_Veto->setChecked(true);
      uiMainWindow->doubleSpinBox_VI01P1_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_VI01P1_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_VI01P1_Z->setValue(det->position[2] / 10);
    } else if (det->name == "VO01X1__") {
      uiMainWindow->checkBox_VO01X1->setChecked(true);
      uiMainWindow->checkBox_Veto->setChecked(true);
      uiMainWindow->doubleSpinBox_VO01X1_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_VO01X1_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_VO01X1_Z->setValue(det->position[2] / 10);
    } else if (det->name == "VI02X1__") {
      uiMainWindow->checkBox_VI02X1->setChecked(true);
      uiMainWindow->checkBox_Veto->setChecked(true);
      uiMainWindow->doubleSpinBox_VI02X1_X->setValue(det->position[0] / 10);
      uiMainWindow->doubleSpinBox_VI02X1_Y->setValue(det->position[1] / 10);
      uiMainWindow->doubleSpinBox_VI02X1_Z->setValue(det->position[2] / 10);
    }
  }
}

void T4ISettingsFile::setInterfaceDC(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_DC00->setChecked(false);
  uiMainWindow->checkBox_DC01->setChecked(false);
  uiMainWindow->checkBox_DC04->setChecked(false);
  uiMainWindow->checkBox_DC05->setChecked(false);

  for (unsigned int i = 0; i < structM->getDC()->size(); i++) {
    const T4SDetector* dc = &structM->getDC()->at(i);

    if (dc->name == "DC00") {
      uiMainWindow->checkBox_DC00->setChecked(true);
      uiMainWindow->doubleSpinBox_PosDC00X->setValue(dc->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosDC00Y->setValue(dc->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosDC00Z->setValue(dc->position[2] / 10);
      uiMainWindow->checkBox_DC00useMechStructure->setChecked(
          dc->useMechanicalStructure);
    } else if (dc->name == "DC01") {
      uiMainWindow->checkBox_DC01->setChecked(true);
      uiMainWindow->doubleSpinBox_PosDC01X->setValue(dc->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosDC01Y->setValue(dc->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosDC01Z->setValue(dc->position[2] / 10);
      uiMainWindow->checkBox_DC01useMechStructure->setChecked(
          dc->useMechanicalStructure);
    } else if (dc->name == "DC04") {
      uiMainWindow->checkBox_DC04->setChecked(true);
      uiMainWindow->doubleSpinBox_PosDC04X->setValue(dc->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosDC04Y->setValue(dc->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosDC04Z->setValue(dc->position[2] / 10);
      uiMainWindow->checkBox_DC04useMechStructure->setChecked(
          dc->useMechanicalStructure);
    } else if (dc->name == "DC05") {
      uiMainWindow->checkBox_DC05->setChecked(true);
      uiMainWindow->doubleSpinBox_PosDC05X->setValue(dc->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosDC05Y->setValue(dc->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosDC05Z->setValue(dc->position[2] / 10);
      uiMainWindow->checkBox_DC05useMechStructure->setChecked(
          dc->useMechanicalStructure);
    }
  }
}

void T4ISettingsFile::setInterfaceW45(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_W45->setChecked(false);
  uiMainWindow->checkBox_DW01X->setChecked(false);
  uiMainWindow->checkBox_DW01Y->setChecked(false);
  uiMainWindow->checkBox_DW02X->setChecked(false);
  uiMainWindow->checkBox_DW02Y->setChecked(false);
  uiMainWindow->checkBox_DW03V->setChecked(false);
  uiMainWindow->checkBox_DW03Y->setChecked(false);
  uiMainWindow->checkBox_DW04Y->setChecked(false);
  uiMainWindow->checkBox_DW04U->setChecked(false);
  uiMainWindow->checkBox_DW05X->setChecked(false);
  uiMainWindow->checkBox_DW05V->setChecked(false);
  uiMainWindow->checkBox_DW06U->setChecked(false);
  uiMainWindow->checkBox_DW06X->setChecked(false);

  for (unsigned int i = 0; i < structM->getW45()->size(); i++) {
    const T4SDetector* w45 = &structM->getW45()->at(i);

    if (w45->name == "DW01X") {
      uiMainWindow->checkBox_DW01X->setChecked(true);
      uiMainWindow->doubleSpinBox_DW01X_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW01X_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW01X_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW01X_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW01Y") {
      uiMainWindow->checkBox_DW01Y->setChecked(true);
      uiMainWindow->doubleSpinBox_DW01Y_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW01Y_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW01Y_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW01Y_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW02X") {
      uiMainWindow->checkBox_DW02X->setChecked(true);
      uiMainWindow->doubleSpinBox_DW02X_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW02X_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW02X_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW02X_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW02Y") {
      uiMainWindow->checkBox_DW02Y->setChecked(true);
      uiMainWindow->doubleSpinBox_DW02Y_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW02Y_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW02Y_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW02Y_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW03V") {
      uiMainWindow->checkBox_DW03V->setChecked(true);
      uiMainWindow->doubleSpinBox_DW03V_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW03V_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW03V_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW03V_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW03Y") {
      uiMainWindow->checkBox_DW03Y->setChecked(true);
      uiMainWindow->doubleSpinBox_DW03Y_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW03Y_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW03Y_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW03Y_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW04Y") {
      uiMainWindow->checkBox_DW04Y->setChecked(true);
      uiMainWindow->doubleSpinBox_DW04Y_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW04Y_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW04Y_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW04Y_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW04U") {
      uiMainWindow->checkBox_DW04U->setChecked(true);
      uiMainWindow->doubleSpinBox_DW04U_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW04U_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW04U_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW04U_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW05X") {
      uiMainWindow->checkBox_DW05X->setChecked(true);
      uiMainWindow->doubleSpinBox_DW05X_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW05X_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW05X_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW05X_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW05V") {
      uiMainWindow->checkBox_DW05V->setChecked(true);
      uiMainWindow->doubleSpinBox_DW05V_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW05V_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW05V_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW05V_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW06U") {
      uiMainWindow->checkBox_DW06U->setChecked(true);
      uiMainWindow->doubleSpinBox_DW06U_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW06U_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW06U_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW06U_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    } else if (w45->name == "DW06X") {
      uiMainWindow->checkBox_DW06X->setChecked(true);
      uiMainWindow->doubleSpinBox_DW06X_X->setValue(w45->position[0] / 10);
      uiMainWindow->doubleSpinBox_DW06X_Y->setValue(w45->position[1] / 10);
      uiMainWindow->doubleSpinBox_DW06X_Z->setValue(w45->position[2] / 10);
      uiMainWindow->checkBox_DW06X_mech->setChecked(
          w45->useMechanicalStructure);
      uiMainWindow->checkBox_W45->setChecked(true);
    }
  }
}

void T4ISettingsFile::setInterfaceRichWall(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_RichWall->setChecked(false);
  uiMainWindow->checkBox_DR01X1->setChecked(false);
  uiMainWindow->checkBox_DR01X2->setChecked(false);
  uiMainWindow->checkBox_DR01Y1->setChecked(false);
  uiMainWindow->checkBox_DR01Y2->setChecked(false);
  uiMainWindow->checkBox_DR02X1->setChecked(false);
  uiMainWindow->checkBox_DR02X2->setChecked(false);
  uiMainWindow->checkBox_DR02Y1->setChecked(false);
  uiMainWindow->checkBox_DR02Y2->setChecked(false);

  for (unsigned int i = 0; i < structM->getRichWall()->size(); i++) {
    const T4SDetector* richwall = &structM->getRichWall()->at(i);

    if (richwall->name == "DR01X1") {
      uiMainWindow->checkBox_DR01X1->setChecked(true);
      uiMainWindow->doubleSpinBox_DR01X1_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR01X1_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR01X1_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR01X2") {
      uiMainWindow->checkBox_DR01X2->setChecked(true);
      uiMainWindow->doubleSpinBox_DR01X2_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR01X2_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR01X2_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR01Y1") {
      uiMainWindow->checkBox_DR01Y1->setChecked(true);
      uiMainWindow->doubleSpinBox_DR01Y1_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR01Y1_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR01Y1_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR01Y2") {
      uiMainWindow->checkBox_DR01Y2->setChecked(true);
      uiMainWindow->doubleSpinBox_DR01Y2_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR01Y2_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR01Y2_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR02X1") {
      uiMainWindow->checkBox_DR02X1->setChecked(true);
      uiMainWindow->doubleSpinBox_DR02X1_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR02X1_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR02X1_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR02X2") {
      uiMainWindow->checkBox_DR02X2->setChecked(true);
      uiMainWindow->doubleSpinBox_DR02X2_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR02X2_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR02X2_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR02Y1") {
      uiMainWindow->checkBox_DR02Y1->setChecked(true);
      uiMainWindow->doubleSpinBox_DR02Y1_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR02Y1_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR02Y1_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    } else if (richwall->name == "DR02Y2") {
      uiMainWindow->checkBox_DR02Y2->setChecked(true);
      uiMainWindow->doubleSpinBox_DR02Y2_X->setValue(richwall->position[0] / 10);
      uiMainWindow->doubleSpinBox_DR02Y2_Y->setValue(richwall->position[1] / 10);
      uiMainWindow->doubleSpinBox_DR02Y2_Z->setValue(richwall->position[2] / 10);
      uiMainWindow->checkBox_RichWall->setChecked(true);
    }
  }
}

void T4ISettingsFile::setInterfaceRPD(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_CAMERA->setChecked(false);
  uiMainWindow->checkBox_RPD->setChecked(false);
  uiMainWindow->checkBox_recoil_RingA->setChecked(false);
  uiMainWindow->checkBox_recoil_RingB->setChecked(false);

  for (unsigned int i = 0; i < structM->getRPD()->size(); i++) {
    T4SRPD* rpd = &structM->getRPD()->at(i);
    if (!rpd->general.useDetector)
      continue;

    if (rpd->general.name == "CA01R1" || rpd->general.name == "RP01R1") {
      if (rpd->general.name == "CA01R1") {
        uiMainWindow->checkBox_CAMERA->setChecked(true);
        uiMainWindow->checkBox_recoil_Use_CalibrationFile->setChecked(rpd->useCalibrationFile);
        uiMainWindow->lineEdit_PathToRecoilCalibrationFile->setText(
            QString::fromStdString(rpd->calibrationFilePath));
      } else if (rpd->general.name == "RP01R1") {
        uiMainWindow->checkBox_RPD->setChecked(true);
        uiMainWindow->checkBox_recoil_RingA_ConicalCryostat->setChecked(rpd->useConicalCryostat);
      }

      uiMainWindow->checkBox_recoil_RingA->setChecked(true);
      uiMainWindow->doubleSpinBox_recoil_RingA_X->setValue(
          rpd->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_recoil_RingA_Y->setValue(
          rpd->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_recoil_RingA_Z->setValue(
          rpd->general.position[2] / 10);
      uiMainWindow->checkBox_recoil_RingA_Mech->setChecked(
          rpd->general.useMechanicalStructure);
      uiMainWindow->checkBox_recoil_RingA_Optical->setChecked(rpd->useOptical);
      uiMainWindow->checkBox_recoil_RingA_Slab->setChecked(rpd->useSingleSlab);

    } else if (rpd->general.name == "CA01R2" || rpd->general.name == "RP01R2") {
      if (rpd->general.name == "CA01R2") {
        uiMainWindow->checkBox_CAMERA->setChecked(true);
        uiMainWindow->checkBox_recoil_Use_CalibrationFile->setChecked(rpd->useCalibrationFile);
        uiMainWindow->lineEdit_PathToRecoilCalibrationFile->setText(
            QString::fromStdString(rpd->calibrationFilePath));
      } else if (rpd->general.name == "RP01R2")
        uiMainWindow->checkBox_RPD->setChecked(true);

      uiMainWindow->checkBox_recoil_RingB->setChecked(true);
      uiMainWindow->doubleSpinBox_recoil_RingB_X->setValue(
          rpd->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_recoil_RingB_Y->setValue(
          rpd->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_recoil_RingB_Z->setValue(
          rpd->general.position[2] / 10);
      uiMainWindow->checkBox_recoil_RingB_Mech->setChecked(
          rpd->general.useMechanicalStructure);
      uiMainWindow->checkBox_recoil_RingB_Optical->setChecked(rpd->useOptical);
      uiMainWindow->checkBox_recoil_RingB_Slab->setChecked(rpd->useSingleSlab);
    }
  }
}

void T4ISettingsFile::setInterfaceMagnet(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_SM1->setChecked(false);
  uiMainWindow->checkBox_SM2->setChecked(false);
  uiMainWindow->checkBox_DYTarget->setChecked(false);
  uiMainWindow->checkBox_PolTarget->setChecked(false);

  for (unsigned int i = 0; i < structM->getMagnet()->size(); i++) {
    T4SMagnet* magnet = &structM->getMagnet()->at(i);
    if (!magnet->general.useDetector)
      continue;

    if (magnet->general.name == "SM1") {
      uiMainWindow->checkBox_SM1->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSM1X->setValue(
          magnet->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSM1Y->setValue(
          magnet->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSM1Z->setValue(
          magnet->general.position[2] / 10);
      uiMainWindow->checkBox_SM1_Mech->setChecked(magnet->general.useMechanicalStructure);
      uiMainWindow->checkBox_SM1_Field->setChecked(magnet->useField);
      uiMainWindow->lineEdit_PathToSM1->setText(
          QString::fromStdString(magnet->fieldmapPath));
      uiMainWindow->doubleSpinBox_FieldScaleSM1->setValue(magnet->scaleFactor);
    } else if (magnet->general.name == "SM2") {
      uiMainWindow->checkBox_SM2->setChecked(true);
      uiMainWindow->doubleSpinBox_PosSM2X->setValue(
          magnet->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_PosSM2Y->setValue(
          magnet->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_PosSM2Z->setValue(
          magnet->general.position[2] / 10);
      uiMainWindow->checkBox_SM2_Mech->setChecked(magnet->general.useMechanicalStructure);
      uiMainWindow->checkBox_SM2_Field->setChecked(magnet->useField);
      uiMainWindow->lineEdit_PathToSM2->setText(
          QString::fromStdString(magnet->fieldmapPath));
      uiMainWindow->doubleSpinBox_FieldScaleSM2->setValue(magnet->scaleFactor);
      uiMainWindow->comboBox_SM2Field->setCurrentIndex(
          uiMainWindow->comboBox_SM2Field->findText(
              QString::fromStdString(intToStr(magnet->current))));

    } else if (magnet->general.name == "DYTARGET" || magnet->general.name == "POLTARGET") {
      if (magnet->general.name == "DYTARGET")
        uiMainWindow->checkBox_DYTarget->setChecked(true);
      else if (magnet->general.name == "POLTARGET")
        uiMainWindow->checkBox_PolTarget->setChecked(true);

      uiMainWindow->doubleSpinBox_PosTargetX->setValue(
          magnet->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_PosTargetY->setValue(
          magnet->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_PosTargetZ->setValue(
          magnet->general.position[2] / 10);
      uiMainWindow->checkBox_TargetMechStructure->setChecked(
          magnet->general.useMechanicalStructure);
      uiMainWindow->checkBox_Target_Field->setChecked(magnet->useField);
      uiMainWindow->lineEdit_PathToTargetMag->setText(
          QString::fromStdString(magnet->fieldmapPath));
      uiMainWindow->doubleSpinBox_FieldScaleTarget->setValue(magnet->scaleFactor);
    }
  }
}

void T4ISettingsFile::setInterfaceRICH(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_Rich->setChecked(false);

  T4SRICH* rich = structM->getRICH();
  if (!rich->general.useDetector)
    return;

  uiMainWindow->checkBox_Rich->setChecked(true);
  uiMainWindow->doubleSpinBox_PosRichX->setValue(
      rich->general.position[0] / 10);
  uiMainWindow->doubleSpinBox_PosRichY->setValue(
      rich->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_PosRichZ->setValue(
      rich->general.position[2] / 10);
  uiMainWindow->checkBox_Rich_OpticalPhysics->setChecked(
      rich->useOpticalPhysics);
  uiMainWindow->checkBox_RICH_housingVisible->setChecked(rich->visibleHousing);
  uiMainWindow->comboBox_richGas->setCurrentIndex(
      uiMainWindow->comboBox_richGas->findText(
          QString::fromStdString(rich->gas)));
}

void T4ISettingsFile::setInterfaceDetector(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_LH2->setChecked(false);
  uiMainWindow->checkBox_LH2Hadron->setChecked(false);
  uiMainWindow->checkBox_DYAbsorber->setChecked(false);
  uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
  uiMainWindow->checkBox_MF3->setChecked(false);
  uiMainWindow->checkBox_TestSetupTestCal->setChecked(false);
  uiMainWindow->checkBox_PolGPD_SiRPD->setChecked(false);

  uiMainWindow->checkBox_HO03->setChecked(false);
  uiMainWindow->checkBox_HI04X1_u->setChecked(false);
  uiMainWindow->checkBox_HI04X1_d->setChecked(false);
  uiMainWindow->checkBox_HM04Y1_u1->setChecked(false);
  uiMainWindow->checkBox_HM04Y1_u2->setChecked(false);
  uiMainWindow->checkBox_HM04Y1_d1->setChecked(false);
  uiMainWindow->checkBox_HM04Y1_d2->setChecked(false);
  uiMainWindow->checkBox_HM04X1_u->setChecked(false);
  uiMainWindow->checkBox_HM04X1_d->setChecked(false);
  uiMainWindow->checkBox_HL04X1_1->setChecked(false);
  uiMainWindow->checkBox_HL04X1_2->setChecked(false);
  uiMainWindow->checkBox_HL04X1_3->setChecked(false);
  uiMainWindow->checkBox_HL04X1_4->setChecked(false);
  uiMainWindow->checkBox_HO04->setChecked(false);
  uiMainWindow->checkBox_HI05X1_u->setChecked(false);
  uiMainWindow->checkBox_HI05X1_d->setChecked(false);
  uiMainWindow->checkBox_HM05Y1_u1->setChecked(false);
  uiMainWindow->checkBox_HM05Y1_u2->setChecked(false);
  uiMainWindow->checkBox_HM05Y1_d1->setChecked(false);
  uiMainWindow->checkBox_HM05Y1_d2->setChecked(false);
  uiMainWindow->checkBox_HM05X1_u->setChecked(false);
  uiMainWindow->checkBox_HM05X1_d->setChecked(false);
  uiMainWindow->checkBox_HL05X1_1->setChecked(false);
  uiMainWindow->checkBox_HL05X1_2->setChecked(false);
  uiMainWindow->checkBox_HL05X1_3->setChecked(false);
  uiMainWindow->checkBox_HL05X1_4->setChecked(false);
  uiMainWindow->checkBox_HK01->setChecked(false);
  uiMainWindow->checkBox_HK02->setChecked(false);
  uiMainWindow->checkBox_HK03->setChecked(false);
  uiMainWindow->checkBox_HH02->setChecked(false);
  uiMainWindow->checkBox_HH03->setChecked(false);

  uiMainWindow->checkBox_H4I->setChecked(false);
  uiMainWindow->checkBox_H4M->setChecked(false);
  uiMainWindow->checkBox_H4L->setChecked(false);
  uiMainWindow->checkBox_H5I->setChecked(false);
  uiMainWindow->checkBox_H5M->setChecked(false);
  uiMainWindow->checkBox_H5L->setChecked(false);
  uiMainWindow->checkBox_H3O->setChecked(false);
  uiMainWindow->checkBox_H4O->setChecked(false);
  uiMainWindow->checkBox_HK->setChecked(false);
  uiMainWindow->checkBox_SandwichVeto->setChecked(false);
  uiMainWindow->checkBox_Multiplicity->setChecked(false);
  uiMainWindow->checkBox_HH->setChecked(false);

  for (unsigned int i = 0; i < structM->getDetector()->size(); i++) {
    T4SDetector* detector = &structM->getDetector()->at(i);
    if (!detector->useDetector)
      continue;

    if (detector->name == "LH2" || detector->name == "LH2Hadron" || detector->name == "PRIMAKOFFTARGET"
       || detector->name == "WORKSHOPEXAMPLE") {
      if (detector->name == "LH2")
        uiMainWindow->checkBox_LH2->setChecked(true);
      else if (detector->name == "LH2Hadron")
        uiMainWindow->checkBox_LH2Hadron->setChecked(true);
      else if (detector->name == "PRIMAKOFFTARGET")
        uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(true);
      else if (detector->name == "WORKSHOPEXAMPLE")
        uiMainWindow->checkBox_WorkshopExample->setChecked(true);

      uiMainWindow->doubleSpinBox_PosTargetX->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosTargetY->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosTargetZ->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_TargetMechStructure->setChecked(
          detector->useMechanicalStructure);

    } else if (detector->name == "DYABSORBER") {
      uiMainWindow->checkBox_DYAbsorber->setChecked(true);
      uiMainWindow->doubleSpinBox_PosDYAbsorberX->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosDYAbsorberY->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosDYAbsorberZ->setValue(
          detector->position[2] / 10);
    } else if (detector->name == "MF3_BLOCK") {
      uiMainWindow->checkBox_MF3->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMF3_Block_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMF3_Block_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMF3_Block_Z->setValue(
          detector->position[2] / 10);
    } else if (detector->name == "MF3") {
      uiMainWindow->checkBox_MF3->setChecked(true);
      uiMainWindow->doubleSpinBox_PosMF3_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_PosMF3_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_PosMF3_Z->setValue(
          detector->position[2] / 10);
    } else if (detector->name == "TESTCAL") {
      uiMainWindow->checkBox_TestSetupTestCal->setChecked(true);
      uiMainWindow->doubleSpinBox_TESTCAL_PosX->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_TESTCAL_PosY->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_TESTCAL_PosZ->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_TESTCAL_MechanicalStructure->setChecked(
          detector->useMechanicalStructure);
    } else if (detector->name == "PolGPDSiRPD") {
      uiMainWindow->checkBox_PolGPD_SiRPD->setChecked(true);
      uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosX->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosY->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosZ->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_PolGPD_SiRPD_MechanicalStructure->setChecked(
          detector->useMechanicalStructure);
    } else if (detector->name == "HO03") {
      uiMainWindow->checkBox_HO03->setChecked(true);
      uiMainWindow->doubleSpinBox_HO03_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HO03_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HO03_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H3O->setChecked(true);
    } else if (detector->name == "HI04X1_u") {
      uiMainWindow->checkBox_HI04X1_u->setChecked(true);
      uiMainWindow->doubleSpinBox_HI04X1_u_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HI04X1_u_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HI04X1_u_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4I->setChecked(true);
    } else if (detector->name == "HI04X1_d") {
      uiMainWindow->checkBox_HI04X1_d->setChecked(true);
      uiMainWindow->doubleSpinBox_HI04X1_d_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HI04X1_d_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HI04X1_d_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4I->setChecked(true);
    } else if (detector->name == "HM04Y1_u1") {
      uiMainWindow->checkBox_HM04Y1_u1->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04Y1_u1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_u1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_u1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HM04Y1_u2") {
      uiMainWindow->checkBox_HM04Y1_u2->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04Y1_u2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_u2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_u2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HM04Y1_d1") {
      uiMainWindow->checkBox_HM04Y1_d1->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04Y1_d1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_d1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_d1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HM04Y1_d2") {
      uiMainWindow->checkBox_HM04Y1_d2->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04Y1_d2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_d2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04Y1_d2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HM04X1_u") {
      uiMainWindow->checkBox_HM04X1_u->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04X1_u_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04X1_u_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04X1_u_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HM04X1_d") {
      uiMainWindow->checkBox_HM04X1_d->setChecked(true);
      uiMainWindow->doubleSpinBox_HM04X1_d_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM04X1_d_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM04X1_d_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4M->setChecked(true);
    } else if (detector->name == "HL04X1_1") {
      uiMainWindow->checkBox_HL04X1_1->setChecked(true);
      uiMainWindow->doubleSpinBox_HL04X1_1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4L->setChecked(true);
    } else if (detector->name == "HL04X1_2") {
      uiMainWindow->checkBox_HL04X1_2->setChecked(true);
      uiMainWindow->doubleSpinBox_HL04X1_2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4L->setChecked(true);
    } else if (detector->name == "HL04X1_3") {
      uiMainWindow->checkBox_HL04X1_3->setChecked(true);
      uiMainWindow->doubleSpinBox_HL04X1_3_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_3_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_3_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4L->setChecked(true);
    } else if (detector->name == "HL04X1_4") {
      uiMainWindow->checkBox_HL04X1_4->setChecked(true);
      uiMainWindow->doubleSpinBox_HL04X1_4_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_4_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL04X1_4_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H4L->setChecked(true);
    } else if (detector->name == "HO04") {
      uiMainWindow->checkBox_HO04->setChecked(true);
      uiMainWindow->doubleSpinBox_HO04_X->setValue(detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HO04_Y->setValue(detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HO04_Z->setValue(detector->position[2] / 10);
      uiMainWindow->checkBox_H4O->setChecked(true);
    } else if (detector->name == "HI05X1_u") {
      uiMainWindow->checkBox_HI05X1_u->setChecked(true);
      uiMainWindow->doubleSpinBox_HI05X1_u_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HI05X1_u_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HI05X1_u_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5I->setChecked(true);
    } else if (detector->name == "HI05X1_d") {
      uiMainWindow->checkBox_HI05X1_d->setChecked(true);
      uiMainWindow->doubleSpinBox_HI05X1_d_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HI05X1_d_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HI05X1_d_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5I->setChecked(true);
    } else if (detector->name == "HM05Y1_u1") {
      uiMainWindow->checkBox_HM05Y1_u1->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05Y1_u1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_u1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_u1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HM05Y1_u2") {
      uiMainWindow->checkBox_HM05Y1_u2->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05Y1_u2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_u2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_u2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HM05Y1_d1") {
      uiMainWindow->checkBox_HM05Y1_d1->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05Y1_d1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_d1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_d1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HM05Y1_d2") {
      uiMainWindow->checkBox_HM05Y1_d2->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05Y1_d2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_d2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05Y1_d2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HM05X1_u") {
      uiMainWindow->checkBox_HM05X1_u->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05X1_u_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05X1_u_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05X1_u_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HM05X1_d") {
      uiMainWindow->checkBox_HM05X1_d->setChecked(true);
      uiMainWindow->doubleSpinBox_HM05X1_d_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HM05X1_d_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HM05X1_d_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5M->setChecked(true);
    } else if (detector->name == "HL05X1_1") {
      uiMainWindow->checkBox_HL05X1_1->setChecked(true);
      uiMainWindow->doubleSpinBox_HL05X1_1_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_1_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_1_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5L->setChecked(true);
    } else if (detector->name == "HL05X1_2") {
      uiMainWindow->checkBox_HL05X1_2->setChecked(true);
      uiMainWindow->doubleSpinBox_HL05X1_2_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_2_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_2_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5L->setChecked(true);
    } else if (detector->name == "HL05X1_3") {
      uiMainWindow->checkBox_HL05X1_3->setChecked(true);
      uiMainWindow->doubleSpinBox_HL05X1_3_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_3_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_3_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5L->setChecked(true);
    } else if (detector->name == "HL05X1_4") {
      uiMainWindow->checkBox_HL05X1_4->setChecked(true);
      uiMainWindow->doubleSpinBox_HL05X1_4_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_4_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HL05X1_4_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_H5L->setChecked(true);
    } else if (detector->name == "HK01X1__") {
      uiMainWindow->checkBox_HK01->setChecked(true);
      uiMainWindow->doubleSpinBox_HK01_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HK01_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HK01_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_HK->setChecked(true);
    } else if (detector->name == "HK02X1__") {
      uiMainWindow->checkBox_HK02->setChecked(true);
      uiMainWindow->doubleSpinBox_HK02_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HK02_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HK02_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_HK->setChecked(true);
    } else if (detector->name == "HK03X1__") {
      uiMainWindow->checkBox_HK03->setChecked(true);
      uiMainWindow->doubleSpinBox_HK03_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HK03_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HK03_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_HK->setChecked(true);
    } else if (detector->name == "SANDWICHVETO") {
      uiMainWindow->checkBox_SandwichVeto->setChecked(true);
      uiMainWindow->doubleSpinBox_SandwichVeto_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_SandwichVeto_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_SandwichVeto_Z->setValue(
         detector->position[2] / 10);
     } else if (detector->name == "MultiplicityCounter") {
      uiMainWindow->checkBox_Multiplicity->setChecked(true);
      uiMainWindow->doubleSpinBox_Multiplicity_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_Multiplicity_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_Multiplicity_Z->setValue(
          detector->position[2] / 10);
    } else if (detector->name == "HH02R1__") {
      uiMainWindow->checkBox_HH02->setChecked(true);
      uiMainWindow->doubleSpinBox_HH02_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HH02_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HH02_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_HH->setChecked(true);
    } else if (detector->name == "HH03R1__") {
      uiMainWindow->checkBox_HH03->setChecked(true);
      uiMainWindow->doubleSpinBox_HH03_X->setValue(
          detector->position[0] / 10);
      uiMainWindow->doubleSpinBox_HH03_Y->setValue(
          detector->position[1] / 10);
      uiMainWindow->doubleSpinBox_HH03_Z->setValue(
          detector->position[2] / 10);
      uiMainWindow->checkBox_HH->setChecked(true);
    }
  }
}

void T4ISettingsFile::setInterfaceDetectorRes(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_H1->setChecked(false);
  uiMainWindow->checkBox_H2->setChecked(false);
  // since setInterfaceVeto() is called first, checkBox_Veto was already set to false
  // and we dont need to do it here
//  uiMainWindow->checkBox_Veto->setChecked(false);

  for (unsigned int i = 0; i < structM->getDetectorRes()->size(); i++) {
    T4SDetectorRes* detectorRes = &structM->getDetectorRes()->at(i);
    if (!detectorRes->general.useDetector)
      continue;

    if (detectorRes->general.name == "HG01Y") {
      uiMainWindow->checkBox_H1->setChecked(true);
      uiMainWindow->doubleSpinBox_H1_X->setValue(
          detectorRes->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_H1_Y->setValue(
          detectorRes->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_H1_Z->setValue(
          detectorRes->general.position[2] / 10);
      uiMainWindow->checkBox_H1_MechStructure->setChecked(
          detectorRes->general.useMechanicalStructure);
      uiMainWindow->checkBox_H1_useOptical->setChecked(detectorRes->useOptical);
    } else if (detectorRes->general.name == "HG02Y") {
      uiMainWindow->checkBox_H2->setChecked(true);
      uiMainWindow->doubleSpinBox_H2_X->setValue(
          detectorRes->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_H2_Y->setValue(
          detectorRes->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_H2_Z->setValue(
          detectorRes->general.position[2] / 10);
      uiMainWindow->checkBox_H2_MechStructure->setChecked(
          detectorRes->general.useMechanicalStructure);
      uiMainWindow->checkBox_H2_useOptical->setChecked(detectorRes->useOptical);
    } else if (detectorRes->general.name == "VETO") {
      uiMainWindow->checkBox_Veto->setChecked(true);
      uiMainWindow->doubleSpinBox_Veto_unkn_X->setValue(
          detectorRes->general.position[0] / 10);
      uiMainWindow->doubleSpinBox_Veto_unkn_Y->setValue(
          detectorRes->general.position[1] / 10);
      uiMainWindow->doubleSpinBox_Veto_unkn_Z->setValue(
          detectorRes->general.position[2] / 10);
      uiMainWindow->checkBox_Veto_unkn_MechStructure->setChecked(
          detectorRes->general.useMechanicalStructure);
      uiMainWindow->checkBox_Veto_unkn_useOptical->setChecked(
          detectorRes->useOptical);
    }
  }
}

void T4ISettingsFile::setInterfacePolGPD(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_PolGPD->setChecked(false);

  T4SPolGPD* in = structM->getPolGPD();
  if (!in->general.useDetector)
    return;

  uiMainWindow->checkBox_PolGPD->setChecked(true);
  uiMainWindow->doubleSpinBox_PolGPD_PosX->setValue(
      in->general.position[0] / 10);
  uiMainWindow->doubleSpinBox_PolGPD_PosY->setValue(
      in->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_PolGPD_PosZ->setValue(
      in->general.position[2] / 10);

  // target
  uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_radius->setValue(in->target_cryostat_radius);
  uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_length->setValue(in->target_cryostat_length);
  uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_thickness->setValue(in->target_cryostat_thickness);

  uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_radius->setValue(in->target_aluminium_radius);
  uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_length->setValue(in->target_aluminium_length);
  uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_thickness->setValue(in->target_aluminium_thickness);

  uiMainWindow->doubleSpinBox_PolGPD_target_coils_radius->setValue(in->target_coils_radius);
  uiMainWindow->doubleSpinBox_PolGPD_target_coils_length->setValue(in->target_coils_length);
  uiMainWindow->doubleSpinBox_PolGPD_target_coils_thickness->setValue(in->target_coils_thickness);

  uiMainWindow->doubleSpinBox_PolGPD_target_mylar_window_thickness->setValue(in->target_mylar_window_thickness);

  uiMainWindow->doubleSpinBox_PolGPD_target_length->setValue(in->target_length);
  uiMainWindow->doubleSpinBox_PolGPD_target_lhe_radius->setValue(in->target_lhe_radius);
  uiMainWindow->doubleSpinBox_PolGPD_target_nh3_radius->setValue(in->target_nh3_radius);
  uiMainWindow->doubleSpinBox_PolGPD_target_fiber_thickness->setValue(in->target_fiber_thickness);
  uiMainWindow->doubleSpinBox_PolGPD_target_kevlar_thickness->setValue(in->target_kevlar_thickness);

  // micromegas
  uiMainWindow->doubleSpinBox_PolGPD_mm_length->setValue(in->mm_length);
  uiMainWindow->doubleSpinBox_PolGPD_mm_radius->setValue(in->mm_radius);
  uiMainWindow->doubleSpinBox_PolGPD_mm_layer_thickness->setValue(in->mm_layer_thickness);
  uiMainWindow->doubleSpinBox_PolGPD_mm_layer_distance->setValue(in->mm_layer_distance);
  uiMainWindow->doubleSpinBox_PolGPD_mm_zPosOffset->setValue(in->mm_zPosOffset);

  // ring B
  uiMainWindow->doubleSpinBox_PolGPD_ringB_length->setValue(in->ringB_length);
  uiMainWindow->doubleSpinBox_PolGPD_ringB_radius->setValue(in->ringB_radius);
  uiMainWindow->doubleSpinBox_PolGPD_ringB_zPosOffset->setValue(in->ringB_zPosOffset);
}

void T4ISettingsFile::setInterfaceScifiTest(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_SCIFI_TEST->setChecked(false);

  T4SSciFiTest* scifiTest = structM->getSciFiTest();
  if (!scifiTest->general.useDetector)
    return;

  uiMainWindow->checkBox_SCIFI_TEST->setChecked(true);
  uiMainWindow->doubleSpinBox_SCIFI_TEST_PosX->setValue(
      scifiTest->general.position[0] / 10);
  uiMainWindow->doubleSpinBox_SCIFI_TEST_PosY->setValue(
      scifiTest->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_SCIFI_TEST_PosZ->setValue(
      scifiTest->general.position[2] / 10);
  uiMainWindow->doubleSpinBox_SCIFI_TEST_Length->setValue(
      scifiTest->length / 10);
}

void T4ISettingsFile::setInterfaceDUMMY(void)
{
  // first we have to set all to false
  uiMainWindow->checkBox_TestSetupDummy->setChecked(false);

  if (structM->getDummies()->size() == 0)
    return;

  const T4SDummy* dummy = &structM->getDummies()->at(0);
  uiMainWindow->checkBox_TestSetupDummy->setChecked(true);
  uiMainWindow->doubleSpinBox_DUMMY_PosX->setValue(dummy->general.position[0] / 10);
  uiMainWindow->doubleSpinBox_DUMMY_PosY->setValue(dummy->general.position[1] / 10);
  uiMainWindow->doubleSpinBox_DUMMY_PosZ->setValue(dummy->general.position[2] / 10);
  uiMainWindow->checkBox_DUMMY_MechanicalStructure->setChecked(dummy->general.useMechanicalStructure);
  uiMainWindow->doubleSpinBox_DUMMY_SizeX->setValue(dummy->planeSize[0] / 10);
  uiMainWindow->doubleSpinBox_DUMMY_SizeY->setValue(dummy->planeSize[1] / 10);
  uiMainWindow->doubleSpinBox_DUMMY_SizeZ->setValue(dummy->planeSize[2] / 10);

  other_dummies.clear();
  for (unsigned int i = 1; i < structM->getDummies()->size(); i++) {
    other_dummies.push_back(T4SDummy());
    dummy = &structM->getDummies()->at(i);

    other_dummies.back().general.useDetector = dummy->general.useDetector;
    other_dummies.back().general.name = dummy->general.name;
    other_dummies.back().general.position[0] = dummy->general.position[0];
    other_dummies.back().general.position[1] = dummy->general.position[1];
    other_dummies.back().general.position[2] = dummy->general.position[2];
    other_dummies.back().general.useMechanicalStructure = dummy->general.useMechanicalStructure;
    other_dummies.back().planeSize[0] = dummy->planeSize[0];
    other_dummies.back().planeSize[1] = dummy->planeSize[1];
    other_dummies.back().planeSize[2] = dummy->planeSize[2];
  }
}

// OTHER TOPIC NOW: detectors.dat
void T4ISettingsFile::setDetDatAlignment(void)
{
  // we have to convert all dimensions from cm to mm
  // RPD
  if (t4SAlignment->getCA01R1()[3]) {
    uiMainWindow->doubleSpinBox_recoil_RingA_X->setValue(
        t4SAlignment->getCA01R1()[0] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingA_Y->setValue(
        t4SAlignment->getCA01R1()[1] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingA_Z->setValue(
        t4SAlignment->getCA01R1()[2] / 10);
    uiMainWindow->checkBox_recoil_RingA->setChecked(t4SAlignment->getCA01R1()[3]);
  } else if (t4SAlignment->getRP01R1()[3]) {
    uiMainWindow->doubleSpinBox_recoil_RingA_X->setValue(
        t4SAlignment->getRP01R1()[0] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingA_Y->setValue(
        t4SAlignment->getRP01R1()[1] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingA_Z->setValue(
        t4SAlignment->getRP01R1()[2] / 10);
    uiMainWindow->checkBox_recoil_RingA->setChecked(t4SAlignment->getRP01R1()[3]);
  }

  if (t4SAlignment->getCA01R2()[3]) {
    uiMainWindow->doubleSpinBox_recoil_RingB_X->setValue(
        t4SAlignment->getCA01R2()[0] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingB_Y->setValue(
        t4SAlignment->getCA01R2()[1] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingB_Z->setValue(
        t4SAlignment->getCA01R2()[2] / 10);
    uiMainWindow->checkBox_recoil_RingB->setChecked(t4SAlignment->getCA01R2()[3]);
  } else if (t4SAlignment->getRP01R2()[3]) {
    uiMainWindow->doubleSpinBox_recoil_RingB_X->setValue(
        t4SAlignment->getRP01R2()[0] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingB_Y->setValue(
        t4SAlignment->getRP01R2()[1] / 10);
    uiMainWindow->doubleSpinBox_recoil_RingB_Z->setValue(
        t4SAlignment->getRP01R2()[2] / 10);
    uiMainWindow->checkBox_recoil_RingB->setChecked(t4SAlignment->getRP01R2()[3]);
  }

  uiMainWindow->checkBox_CAMERA->setChecked((t4SAlignment->getCA01R1()[3] || t4SAlignment->getCA01R2()[3]));
  uiMainWindow->checkBox_RPD->setChecked((t4SAlignment->getRP01R1()[3] || t4SAlignment->getRP01R2()[3]));

  // DCs
  uiMainWindow->doubleSpinBox_PosDC00X->setValue(
      t4SAlignment->getDC00()[0] / 10);
  uiMainWindow->doubleSpinBox_PosDC00Y->setValue(
      t4SAlignment->getDC00()[1] / 10);
  uiMainWindow->doubleSpinBox_PosDC00Z->setValue(
      t4SAlignment->getDC00()[2] / 10);
  uiMainWindow->checkBox_DC00->setChecked(t4SAlignment->getDC00()[3]);

  uiMainWindow->doubleSpinBox_PosDC01X->setValue(
      t4SAlignment->getDC01()[0] / 10);
  uiMainWindow->doubleSpinBox_PosDC01Y->setValue(
      t4SAlignment->getDC01()[1] / 10);
  uiMainWindow->doubleSpinBox_PosDC01Z->setValue(
      t4SAlignment->getDC01()[2] / 10);
  uiMainWindow->checkBox_DC01->setChecked(t4SAlignment->getDC01()[3]);

  uiMainWindow->doubleSpinBox_PosDC04X->setValue(
      t4SAlignment->getDC04()[0] / 10);
  uiMainWindow->doubleSpinBox_PosDC04Y->setValue(
      t4SAlignment->getDC04()[1] / 10);
  uiMainWindow->doubleSpinBox_PosDC04Z->setValue(
      t4SAlignment->getDC04()[2] / 10);
  uiMainWindow->checkBox_DC04->setChecked(t4SAlignment->getDC04()[3]);

  uiMainWindow->doubleSpinBox_PosDC05X->setValue(
      t4SAlignment->getDC05()[0] / 10);
  uiMainWindow->doubleSpinBox_PosDC05Y->setValue(
      t4SAlignment->getDC05()[1] / 10);
  uiMainWindow->doubleSpinBox_PosDC05Z->setValue(
      t4SAlignment->getDC05()[2] / 10);
  uiMainWindow->checkBox_DC05->setChecked(t4SAlignment->getDC05()[3]);

  // MW1
  uiMainWindow->doubleSpinBox_MA01X1_X->setValue(
      t4SAlignment->getMA01X1()[0] / 10);
  uiMainWindow->doubleSpinBox_MA01X1_Y->setValue(
      t4SAlignment->getMA01X1()[1] / 10);
  uiMainWindow->doubleSpinBox_MA01X1_Z->setValue(
      t4SAlignment->getMA01X1()[2] / 10);
  uiMainWindow->checkBox_MA01X1->setChecked(t4SAlignment->getMA01X1()[3]);

  uiMainWindow->doubleSpinBox_MA01X3_X->setValue(
      t4SAlignment->getMA01X3()[0] / 10);
  uiMainWindow->doubleSpinBox_MA01X3_Y->setValue(
      t4SAlignment->getMA01X3()[1] / 10);
  uiMainWindow->doubleSpinBox_MA01X3_Z->setValue(
      t4SAlignment->getMA01X3()[2] / 10);
  uiMainWindow->checkBox_MA01X3->setChecked(t4SAlignment->getMA01X3()[3]);

  uiMainWindow->doubleSpinBox_MA01Y1_X->setValue(
      t4SAlignment->getMA01Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_MA01Y1_Y->setValue(
      t4SAlignment->getMA01Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_MA01Y1_Z->setValue(
      t4SAlignment->getMA01Y1()[2] / 10);
  uiMainWindow->checkBox_MA01Y1->setChecked(t4SAlignment->getMA01Y1()[3]);

  uiMainWindow->doubleSpinBox_MA01Y3_X->setValue(
      t4SAlignment->getMA01Y3()[0] / 10);
  uiMainWindow->doubleSpinBox_MA01Y3_Y->setValue(
      t4SAlignment->getMA01Y3()[1] / 10);
  uiMainWindow->doubleSpinBox_MA01Y3_Z->setValue(
      t4SAlignment->getMA01Y3()[2] / 10);
  uiMainWindow->checkBox_MA01Y3->setChecked(t4SAlignment->getMA01Y3()[3]);

  uiMainWindow->doubleSpinBox_MA02X1_X->setValue(
      t4SAlignment->getMA02X1()[0] / 10);
  uiMainWindow->doubleSpinBox_MA02X1_Y->setValue(
      t4SAlignment->getMA02X1()[1] / 10);
  uiMainWindow->doubleSpinBox_MA02X1_Z->setValue(
      t4SAlignment->getMA02X1()[2] / 10);
  uiMainWindow->checkBox_MA02X1->setChecked(t4SAlignment->getMA02X1()[3]);

  uiMainWindow->doubleSpinBox_MA02X3_X->setValue(
      t4SAlignment->getMA02X3()[0] / 10);
  uiMainWindow->doubleSpinBox_MA02X3_Y->setValue(
      t4SAlignment->getMA02X3()[1] / 10);
  uiMainWindow->doubleSpinBox_MA02X3_Z->setValue(
      t4SAlignment->getMA02X3()[2] / 10);
  uiMainWindow->checkBox_MA02X3->setChecked(t4SAlignment->getMA02X3()[3]);

  uiMainWindow->doubleSpinBox_MA02Y1_X->setValue(
      t4SAlignment->getMA02Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_MA02Y1_Y->setValue(
      t4SAlignment->getMA02Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_MA02Y1_Z->setValue(
      t4SAlignment->getMA02Y1()[2] / 10);
  uiMainWindow->checkBox_MA02Y1->setChecked(t4SAlignment->getMA02Y1()[3]);

  uiMainWindow->doubleSpinBox_MA02Y3_X->setValue(
      t4SAlignment->getMA02Y3()[0] / 10);
  uiMainWindow->doubleSpinBox_MA02Y3_Y->setValue(
      t4SAlignment->getMA02Y3()[1] / 10);
  uiMainWindow->doubleSpinBox_MA02Y3_Z->setValue(
      t4SAlignment->getMA02Y3()[2] / 10);
  uiMainWindow->checkBox_MA02Y3->setChecked(t4SAlignment->getMA02Y3()[3]);

  uiMainWindow->checkBox_MW1->setChecked(
      (t4SAlignment->getMA01X1()[3] || t4SAlignment->getMA01X3()[3]
          || t4SAlignment->getMA01Y1()[3] || t4SAlignment->getMA01Y3()[3]
          || t4SAlignment->getMA02X1()[3] || t4SAlignment->getMA02X3()[3]
          || t4SAlignment->getMA02Y1()[3] || t4SAlignment->getMA02Y3()[3]));

  // MW2
  uiMainWindow->doubleSpinBox_MB01X_X->setValue(
      t4SAlignment->getMB01X()[0] / 10);
  uiMainWindow->doubleSpinBox_MB01X_Y->setValue(
      t4SAlignment->getMB01X()[1] / 10);
  uiMainWindow->doubleSpinBox_MB01X_Z->setValue(
      t4SAlignment->getMB01X()[2] / 10);
  uiMainWindow->checkBox_MB01X->setChecked(t4SAlignment->getMB01X()[3]);

  uiMainWindow->doubleSpinBox_MB01Y_X->setValue(
      t4SAlignment->getMB01Y()[0] / 10);
  uiMainWindow->doubleSpinBox_MB01Y_Y->setValue(
      t4SAlignment->getMB01Y()[1] / 10);
  uiMainWindow->doubleSpinBox_MB01Y_Z->setValue(
      t4SAlignment->getMB01Y()[2] / 10);
  uiMainWindow->checkBox_MB01Y->setChecked(t4SAlignment->getMB01Y()[3]);

  uiMainWindow->doubleSpinBox_MB01V_X->setValue(
      t4SAlignment->getMB01V()[0] / 10);
  uiMainWindow->doubleSpinBox_MB01V_Y->setValue(
      t4SAlignment->getMB01V()[1] / 10);
  uiMainWindow->doubleSpinBox_MB01V_Z->setValue(
      t4SAlignment->getMB01V()[2] / 10);
  uiMainWindow->checkBox_MB01V->setChecked(t4SAlignment->getMB01V()[3]);

  uiMainWindow->doubleSpinBox_MB02X_X->setValue(
      t4SAlignment->getMB02X()[0] / 10);
  uiMainWindow->doubleSpinBox_MB02X_Y->setValue(
      t4SAlignment->getMB02X()[1] / 10);
  uiMainWindow->doubleSpinBox_MB02X_Z->setValue(
      t4SAlignment->getMB02X()[2] / 10);
  uiMainWindow->checkBox_MB02X->setChecked(t4SAlignment->getMB02X()[3]);

  uiMainWindow->doubleSpinBox_MB02Y_X->setValue(
      t4SAlignment->getMB02Y()[0] / 10);
  uiMainWindow->doubleSpinBox_MB02Y_Y->setValue(
      t4SAlignment->getMB02Y()[1] / 10);
  uiMainWindow->doubleSpinBox_MB02Y_Z->setValue(
      t4SAlignment->getMB02Y()[2] / 10);
  uiMainWindow->checkBox_MB02Y->setChecked(t4SAlignment->getMB02Y()[3]);

  uiMainWindow->doubleSpinBox_MB02V_X->setValue(
      t4SAlignment->getMB02V()[0] / 10);
  uiMainWindow->doubleSpinBox_MB02V_Y->setValue(
      t4SAlignment->getMB02V()[1] / 10);
  uiMainWindow->doubleSpinBox_MB02V_Z->setValue(
      t4SAlignment->getMB02V()[2] / 10);
  uiMainWindow->checkBox_MB02V->setChecked(t4SAlignment->getMB02V()[3]);

  uiMainWindow->checkBox_MW2->setChecked(
      (t4SAlignment->getMB01X()[3] || t4SAlignment->getMB01Y()[3]
          || t4SAlignment->getMB01V()[3] || t4SAlignment->getMB02X()[3]
          || t4SAlignment->getMB02Y()[3] || t4SAlignment->getMB02V()[3]));

  // W45
  uiMainWindow->doubleSpinBox_DW01X_X->setValue(
      t4SAlignment->getDW01X()[0] / 10);
  uiMainWindow->doubleSpinBox_DW01X_Y->setValue(
      t4SAlignment->getDW01X()[1] / 10);
  uiMainWindow->doubleSpinBox_DW01X_Z->setValue(
      t4SAlignment->getDW01X()[2] / 10);
  uiMainWindow->checkBox_DW01X->setChecked(t4SAlignment->getDW01X()[3]);

  uiMainWindow->doubleSpinBox_DW01Y_X->setValue(
      t4SAlignment->getDW01Y()[0] / 10);
  uiMainWindow->doubleSpinBox_DW01Y_Y->setValue(
      t4SAlignment->getDW01Y()[1] / 10);
  uiMainWindow->doubleSpinBox_DW01Y_Z->setValue(
      t4SAlignment->getDW01Y()[2] / 10);
  uiMainWindow->checkBox_DW01Y->setChecked(t4SAlignment->getDW01Y()[3]);

  uiMainWindow->doubleSpinBox_DW02X_X->setValue(
      t4SAlignment->getDW02X()[0] / 10);
  uiMainWindow->doubleSpinBox_DW02X_Y->setValue(
      t4SAlignment->getDW02X()[1] / 10);
  uiMainWindow->doubleSpinBox_DW02X_Z->setValue(
      t4SAlignment->getDW02X()[2] / 10);
  uiMainWindow->checkBox_DW02X->setChecked(t4SAlignment->getDW02X()[3]);

  uiMainWindow->doubleSpinBox_DW02Y_X->setValue(
      t4SAlignment->getDW02Y()[0] / 10);
  uiMainWindow->doubleSpinBox_DW02Y_Y->setValue(
      t4SAlignment->getDW02Y()[1] / 10);
  uiMainWindow->doubleSpinBox_DW02Y_Z->setValue(
      t4SAlignment->getDW02Y()[2] / 10);
  uiMainWindow->checkBox_DW02Y->setChecked(t4SAlignment->getDW02Y()[3]);

  uiMainWindow->doubleSpinBox_DW03V_X->setValue(
      t4SAlignment->getDW03V()[0] / 10);
  uiMainWindow->doubleSpinBox_DW03V_Y->setValue(
      t4SAlignment->getDW03V()[1] / 10);
  uiMainWindow->doubleSpinBox_DW03V_Z->setValue(
      t4SAlignment->getDW03V()[2] / 10);
  uiMainWindow->checkBox_DW03V->setChecked(t4SAlignment->getDW03V()[3]);

  uiMainWindow->doubleSpinBox_DW03Y_X->setValue(
      t4SAlignment->getDW03Y()[0] / 10);
  uiMainWindow->doubleSpinBox_DW03Y_Y->setValue(
      t4SAlignment->getDW03Y()[1] / 10);
  uiMainWindow->doubleSpinBox_DW03Y_Z->setValue(
      t4SAlignment->getDW03Y()[2] / 10);
  uiMainWindow->checkBox_DW03Y->setChecked(t4SAlignment->getDW03Y()[3]);

  uiMainWindow->doubleSpinBox_DW04Y_X->setValue(
      t4SAlignment->getDW04Y()[0] / 10);
  uiMainWindow->doubleSpinBox_DW04Y_Y->setValue(
      t4SAlignment->getDW04Y()[1] / 10);
  uiMainWindow->doubleSpinBox_DW04Y_Z->setValue(
      t4SAlignment->getDW04Y()[2] / 10);
  uiMainWindow->checkBox_DW04Y->setChecked(t4SAlignment->getDW04Y()[3]);

  uiMainWindow->doubleSpinBox_DW04U_X->setValue(
      t4SAlignment->getDW04U()[0] / 10);
  uiMainWindow->doubleSpinBox_DW04U_Y->setValue(
      t4SAlignment->getDW04U()[1] / 10);
  uiMainWindow->doubleSpinBox_DW04U_Z->setValue(
      t4SAlignment->getDW04U()[2] / 10);
  uiMainWindow->checkBox_DW04U->setChecked(t4SAlignment->getDW04U()[3]);

  uiMainWindow->doubleSpinBox_DW05X_X->setValue(
      t4SAlignment->getDW05X()[0] / 10);
  uiMainWindow->doubleSpinBox_DW05X_Y->setValue(
      t4SAlignment->getDW05X()[1] / 10);
  uiMainWindow->doubleSpinBox_DW05X_Z->setValue(
      t4SAlignment->getDW05X()[2] / 10);
  uiMainWindow->checkBox_DW05X->setChecked(t4SAlignment->getDW05X()[3]);

  uiMainWindow->doubleSpinBox_DW05V_X->setValue(
      t4SAlignment->getDW05V()[0] / 10);
  uiMainWindow->doubleSpinBox_DW05V_Y->setValue(
      t4SAlignment->getDW05V()[1] / 10);
  uiMainWindow->doubleSpinBox_DW05V_Z->setValue(
      t4SAlignment->getDW05V()[2] / 10);
  uiMainWindow->checkBox_DW05V->setChecked(t4SAlignment->getDW05V()[3]);

  uiMainWindow->doubleSpinBox_DW06U_X->setValue(
      t4SAlignment->getDW06U()[0] / 10);
  uiMainWindow->doubleSpinBox_DW06U_Y->setValue(
      t4SAlignment->getDW06U()[1] / 10);
  uiMainWindow->doubleSpinBox_DW06U_Z->setValue(
      t4SAlignment->getDW06U()[2] / 10);
  uiMainWindow->checkBox_DW06U->setChecked(t4SAlignment->getDW06U()[3]);

  uiMainWindow->doubleSpinBox_DW06X_X->setValue(
      t4SAlignment->getDW06X()[0] / 10);
  uiMainWindow->doubleSpinBox_DW06X_Y->setValue(
      t4SAlignment->getDW06X()[1] / 10);
  uiMainWindow->doubleSpinBox_DW06X_Z->setValue(
      t4SAlignment->getDW06X()[2] / 10);
  uiMainWindow->checkBox_DW06X->setChecked(t4SAlignment->getDW06X()[3]);

  uiMainWindow->checkBox_W45->setChecked(
      t4SAlignment->getDW01X()[3] || t4SAlignment->getDW01Y()[3]
          || t4SAlignment->getDW02X()[3] || t4SAlignment->getDW02Y()[3]
          || t4SAlignment->getDW03V()[3] || t4SAlignment->getDW03Y()[3]
          || t4SAlignment->getDW04Y()[3] || t4SAlignment->getDW04U()[3]
          || t4SAlignment->getDW05X()[3] || t4SAlignment->getDW05V()[3]
          || t4SAlignment->getDW06U()[3] || t4SAlignment->getDW06X()[3]);

  // RichWall
  uiMainWindow->doubleSpinBox_DR01X1_X->setValue(
      t4SAlignment->getDR01X1()[0] / 10);
  uiMainWindow->doubleSpinBox_DR01X1_Y->setValue(
      t4SAlignment->getDR01X1()[1] / 10);
  uiMainWindow->doubleSpinBox_DR01X1_Z->setValue(
      t4SAlignment->getDR01X1()[2] / 10);
  uiMainWindow->checkBox_DR01X1->setChecked(t4SAlignment->getDR01X1()[3]);

  uiMainWindow->doubleSpinBox_DR01X2_X->setValue(
      t4SAlignment->getDR01X2()[0] / 10);
  uiMainWindow->doubleSpinBox_DR01X2_Y->setValue(
      t4SAlignment->getDR01X2()[1] / 10);
  uiMainWindow->doubleSpinBox_DR01X2_Z->setValue(
      t4SAlignment->getDR01X2()[2] / 10);
  uiMainWindow->checkBox_DR01X2->setChecked(t4SAlignment->getDR01X2()[3]);

  uiMainWindow->doubleSpinBox_DR01Y1_X->setValue(
      t4SAlignment->getDR01Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_DR01Y1_Y->setValue(
      t4SAlignment->getDR01Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_DR01Y1_Z->setValue(
      t4SAlignment->getDR01Y1()[2] / 10);
  uiMainWindow->checkBox_DR01Y1->setChecked(t4SAlignment->getDR01Y1()[3]);

  uiMainWindow->doubleSpinBox_DR01Y2_X->setValue(
      t4SAlignment->getDR01Y2()[0] / 10);
  uiMainWindow->doubleSpinBox_DR01Y2_Y->setValue(
      t4SAlignment->getDR01Y2()[1] / 10);
  uiMainWindow->doubleSpinBox_DR01Y2_Z->setValue(
      t4SAlignment->getDR01Y2()[2] / 10);
  uiMainWindow->checkBox_DR01Y2->setChecked(t4SAlignment->getDR01Y2()[3]);

  uiMainWindow->doubleSpinBox_DR02X1_X->setValue(
      t4SAlignment->getDR02X1()[0] / 10);
  uiMainWindow->doubleSpinBox_DR02X1_Y->setValue(
      t4SAlignment->getDR02X1()[1] / 10);
  uiMainWindow->doubleSpinBox_DR02X1_Z->setValue(
      t4SAlignment->getDR02X1()[2] / 10);
  uiMainWindow->checkBox_DR02X1->setChecked(t4SAlignment->getDR02X1()[3]);

  uiMainWindow->doubleSpinBox_DR02X2_X->setValue(
      t4SAlignment->getDR02X2()[0] / 10);
  uiMainWindow->doubleSpinBox_DR02X2_Y->setValue(
      t4SAlignment->getDR02X2()[1] / 10);
  uiMainWindow->doubleSpinBox_DR02X2_Z->setValue(
      t4SAlignment->getDR02X2()[2] / 10);
  uiMainWindow->checkBox_DR02X2->setChecked(t4SAlignment->getDR02X2()[3]);

  uiMainWindow->doubleSpinBox_DR02Y1_X->setValue(
      t4SAlignment->getDR02Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_DR02Y1_Y->setValue(
      t4SAlignment->getDR02Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_DR02Y1_Z->setValue(
      t4SAlignment->getDR02Y1()[2] / 10);
  uiMainWindow->checkBox_DR02Y1->setChecked(t4SAlignment->getDR02Y1()[3]);

  uiMainWindow->doubleSpinBox_DR02Y2_X->setValue(
      t4SAlignment->getDR02Y2()[0] / 10);
  uiMainWindow->doubleSpinBox_DR02Y2_Y->setValue(
      t4SAlignment->getDR02Y2()[1] / 10);
  uiMainWindow->doubleSpinBox_DR02Y2_Z->setValue(
      t4SAlignment->getDR02Y2()[2] / 10);
  uiMainWindow->checkBox_DR02Y2->setChecked(t4SAlignment->getDR02Y2()[3]);

  uiMainWindow->checkBox_RichWall->setChecked(
      t4SAlignment->getDR01X1()[3] || t4SAlignment->getDR01X2()[3]
          || t4SAlignment->getDR01Y1()[3] || t4SAlignment->getDR01Y2()[3]
          || t4SAlignment->getDR02X1()[3] || t4SAlignment->getDR02X2()[3]
          || t4SAlignment->getDR02Y1()[3] || t4SAlignment->getDR02Y2()[3]);

  // H1 & H2
  uiMainWindow->doubleSpinBox_H1_X->setValue(t4SAlignment->getH1()[0] / 10);
  uiMainWindow->doubleSpinBox_H1_Y->setValue(t4SAlignment->getH1()[1] / 10);
  uiMainWindow->doubleSpinBox_H1_Z->setValue(t4SAlignment->getH1()[2] / 10);
  uiMainWindow->checkBox_H1->setChecked(t4SAlignment->getH1()[3]);

  uiMainWindow->doubleSpinBox_H2_X->setValue(t4SAlignment->getH2()[0] / 10);
  uiMainWindow->doubleSpinBox_H2_Y->setValue(t4SAlignment->getH2()[1] / 10);
  uiMainWindow->doubleSpinBox_H2_Z->setValue(t4SAlignment->getH2()[2] / 10);
  uiMainWindow->checkBox_H2->setChecked(t4SAlignment->getH2()[3]);

  // ST02
  uiMainWindow->doubleSpinBox_ST02X1_X->setValue(
      t4SAlignment->getST02X1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02X1_Y->setValue(
      t4SAlignment->getST02X1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02X1_Z->setValue(
      t4SAlignment->getST02X1()[2] / 10);
  uiMainWindow->checkBox_ST02X1->setChecked(t4SAlignment->getST02X1()[3]);

  uiMainWindow->doubleSpinBox_ST02X2_X->setValue(
      t4SAlignment->getST02X2()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02X2_Y->setValue(
      t4SAlignment->getST02X2()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02X2_Z->setValue(
      t4SAlignment->getST02X2()[2] / 10);
  uiMainWindow->checkBox_ST02X2->setChecked(t4SAlignment->getST02X2()[3]);

  uiMainWindow->doubleSpinBox_ST02Y1_X->setValue(
      t4SAlignment->getST02Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02Y1_Y->setValue(
      t4SAlignment->getST02Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02Y1_Z->setValue(
      t4SAlignment->getST02Y1()[2] / 10);
  uiMainWindow->checkBox_ST02Y1->setChecked(t4SAlignment->getST02Y1()[3]);

  uiMainWindow->doubleSpinBox_ST02Y2_X->setValue(
      t4SAlignment->getST02Y2()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02Y2_Y->setValue(
      t4SAlignment->getST02Y2()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02Y2_Z->setValue(
      t4SAlignment->getST02Y2()[2] / 10);
  uiMainWindow->checkBox_ST02Y2->setChecked(t4SAlignment->getST02Y2()[3]);

  uiMainWindow->doubleSpinBox_ST02U1_X->setValue(
      t4SAlignment->getST02U1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02U1_Y->setValue(
      t4SAlignment->getST02U1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02U1_Z->setValue(
      t4SAlignment->getST02U1()[2] / 10);
  uiMainWindow->checkBox_ST02U1->setChecked(t4SAlignment->getST02U1()[3]);

  uiMainWindow->doubleSpinBox_ST02V1_X->setValue(
      t4SAlignment->getST02V1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST02V1_Y->setValue(
      t4SAlignment->getST02V1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST02V1_Z->setValue(
      t4SAlignment->getST02V1()[2] / 10);
  uiMainWindow->checkBox_ST02V1->setChecked(t4SAlignment->getST02V1()[3]);

  uiMainWindow->checkBox_ST02->setChecked(
      t4SAlignment->getST02X1()[3] || t4SAlignment->getST02X2()[3]
          || t4SAlignment->getST02Y1()[3] || t4SAlignment->getST02Y2()[3]
          || t4SAlignment->getST02U1()[3] || t4SAlignment->getST02V1()[3]);

  // ST03
  uiMainWindow->doubleSpinBox_ST03X1_X->setValue(
      t4SAlignment->getST03X1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03X1_Y->setValue(
      t4SAlignment->getST03X1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03X1_Z->setValue(
      t4SAlignment->getST03X1()[2] / 10);
  uiMainWindow->checkBox_ST03X1->setChecked(t4SAlignment->getST03X1()[3]);

  uiMainWindow->doubleSpinBox_ST03X2_X->setValue(
      t4SAlignment->getST03X2()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03X2_Y->setValue(
      t4SAlignment->getST03X2()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03X2_Z->setValue(
      t4SAlignment->getST03X2()[2] / 10);
  uiMainWindow->checkBox_ST03X2->setChecked(t4SAlignment->getST03X2()[3]);

  uiMainWindow->doubleSpinBox_ST03Y1_X->setValue(
      t4SAlignment->getST03Y1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03Y1_Y->setValue(
      t4SAlignment->getST03Y1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03Y1_Z->setValue(
      t4SAlignment->getST03Y1()[2] / 10);
  uiMainWindow->checkBox_ST03Y1->setChecked(t4SAlignment->getST03Y1()[3]);

  uiMainWindow->doubleSpinBox_ST03Y2_X->setValue(
      t4SAlignment->getST03Y2()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03Y2_Y->setValue(
      t4SAlignment->getST03Y2()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03Y2_Z->setValue(
      t4SAlignment->getST03Y2()[2] / 10);
  uiMainWindow->checkBox_ST03Y2->setChecked(t4SAlignment->getST03Y2()[3]);

  uiMainWindow->doubleSpinBox_ST03U1_X->setValue(
      t4SAlignment->getST03U1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03U1_Y->setValue(
      t4SAlignment->getST03U1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03U1_Z->setValue(
      t4SAlignment->getST03U1()[2] / 10);
  uiMainWindow->checkBox_ST03U1->setChecked(t4SAlignment->getST03U1()[3]);

  uiMainWindow->doubleSpinBox_ST03V1_X->setValue(
      t4SAlignment->getST03V1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST03V1_Y->setValue(
      t4SAlignment->getST03V1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST03V1_Z->setValue(
      t4SAlignment->getST03V1()[2] / 10);
  uiMainWindow->checkBox_ST03V1->setChecked(t4SAlignment->getST03V1()[3]);

  uiMainWindow->checkBox_ST03->setChecked(
      t4SAlignment->getST03X1()[3] || t4SAlignment->getST03X2()[3]
          || t4SAlignment->getST03Y1()[3] || t4SAlignment->getST03Y2()[3]
          || t4SAlignment->getST03U1()[3] || t4SAlignment->getST03V1()[3]);

  // ST05
  uiMainWindow->doubleSpinBox_ST05X1_X->setValue(
      t4SAlignment->getST05X1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST05X1_Y->setValue(
      t4SAlignment->getST05X1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST05X1_Z->setValue(
      t4SAlignment->getST05X1()[2] / 10);
  uiMainWindow->checkBox_ST05X1->setChecked(t4SAlignment->getST05X1()[3]);

  uiMainWindow->doubleSpinBox_ST05Y2_X->setValue(
      t4SAlignment->getST05Y2()[0] / 10);
  uiMainWindow->doubleSpinBox_ST05Y2_Y->setValue(
      t4SAlignment->getST05Y2()[1] / 10);
  uiMainWindow->doubleSpinBox_ST05Y2_Z->setValue(
      t4SAlignment->getST05Y2()[2] / 10);
  uiMainWindow->checkBox_ST05Y2->setChecked(t4SAlignment->getST05Y2()[3]);

  uiMainWindow->doubleSpinBox_ST05U1_X->setValue(
      t4SAlignment->getST05U1()[0] / 10);
  uiMainWindow->doubleSpinBox_ST05U1_Y->setValue(
      t4SAlignment->getST05U1()[1] / 10);
  uiMainWindow->doubleSpinBox_ST05U1_Z->setValue(
      t4SAlignment->getST05U1()[2] / 10);
  uiMainWindow->checkBox_ST05U1->setChecked(t4SAlignment->getST05U1()[3]);

  uiMainWindow->checkBox_ST05->setChecked(
      t4SAlignment->getST05X1()[3] || t4SAlignment->getST05Y2()[3]
          || t4SAlignment->getST05U1()[3]);

  // MM
  uiMainWindow->doubleSpinBox_PosMM01X_X->setValue(
      t4SAlignment->getMMX(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM01X_Y->setValue(
      t4SAlignment->getMMX(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM01X_Z->setValue(
      t4SAlignment->getMMX(1)[2] / 10);
  uiMainWindow->checkBox_MM01X->setChecked(t4SAlignment->getMMX(1)[3]);

  uiMainWindow->doubleSpinBox_PosMM01Y_X->setValue(
      t4SAlignment->getMMY(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM01Y_Y->setValue(
      t4SAlignment->getMMY(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM01Y_Z->setValue(
      t4SAlignment->getMMY(1)[2] / 10);
  uiMainWindow->checkBox_MM01Y->setChecked(t4SAlignment->getMMY(1)[3]);

  uiMainWindow->doubleSpinBox_PosMM01V_X->setValue(
      t4SAlignment->getMMV(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM01V_Y->setValue(
      t4SAlignment->getMMV(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM01V_Z->setValue(
      t4SAlignment->getMMV(1)[2] / 10);
  uiMainWindow->checkBox_MM01V->setChecked(t4SAlignment->getMMV(1)[3]);

  uiMainWindow->doubleSpinBox_PosMM01U_X->setValue(
      t4SAlignment->getMMU(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM01U_Y->setValue(
      t4SAlignment->getMMU(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM01U_Z->setValue(
      t4SAlignment->getMMU(1)[2] / 10);
  uiMainWindow->checkBox_MM01U->setChecked(t4SAlignment->getMMU(1)[3]);

  uiMainWindow->checkBox_MM01->setChecked(
      t4SAlignment->getMMX(1)[3] || t4SAlignment->getMMY(1)[3]
          || t4SAlignment->getMMV(1)[3] || t4SAlignment->getMMU(1)[3]);

  uiMainWindow->doubleSpinBox_PosMM02X_X->setValue(
      t4SAlignment->getMMX(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM02X_Y->setValue(
      t4SAlignment->getMMX(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM02X_Z->setValue(
      t4SAlignment->getMMX(2)[2] / 10);
  uiMainWindow->checkBox_MM02X->setChecked(t4SAlignment->getMMX(2)[3]);

  uiMainWindow->doubleSpinBox_PosMM02Y_X->setValue(
      t4SAlignment->getMMY(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM02Y_Y->setValue(
      t4SAlignment->getMMY(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM02Y_Z->setValue(
      t4SAlignment->getMMY(2)[2] / 10);
  uiMainWindow->checkBox_MM02Y->setChecked(t4SAlignment->getMMY(2)[3]);

  uiMainWindow->doubleSpinBox_PosMM02V_X->setValue(
      t4SAlignment->getMMV(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM02V_Y->setValue(
      t4SAlignment->getMMV(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM02V_Z->setValue(
      t4SAlignment->getMMV(2)[2] / 10);
  uiMainWindow->checkBox_MM02V->setChecked(t4SAlignment->getMMV(2)[3]);

  uiMainWindow->doubleSpinBox_PosMM02U_X->setValue(
      t4SAlignment->getMMU(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM02U_Y->setValue(
      t4SAlignment->getMMU(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM02U_Z->setValue(
      t4SAlignment->getMMU(2)[2] / 10);
  uiMainWindow->checkBox_MM02U->setChecked(t4SAlignment->getMMU(2)[3]);

  uiMainWindow->checkBox_MM02->setChecked(
      t4SAlignment->getMMX(2)[3] || t4SAlignment->getMMY(2)[3]
          || t4SAlignment->getMMV(2)[3] || t4SAlignment->getMMU(2)[3]);

  uiMainWindow->doubleSpinBox_PosMM03X_X->setValue(
      t4SAlignment->getMMX(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM03X_Y->setValue(
      t4SAlignment->getMMX(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM03X_Z->setValue(
      t4SAlignment->getMMX(3)[2] / 10);
  uiMainWindow->checkBox_MM03X->setChecked(t4SAlignment->getMMX(3)[3]);

  uiMainWindow->doubleSpinBox_PosMM03Y_X->setValue(
      t4SAlignment->getMMY(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM03Y_Y->setValue(
      t4SAlignment->getMMY(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM03Y_Z->setValue(
      t4SAlignment->getMMY(3)[2] / 10);
  uiMainWindow->checkBox_MM03Y->setChecked(t4SAlignment->getMMY(3)[3]);

  uiMainWindow->doubleSpinBox_PosMM03V_X->setValue(
      t4SAlignment->getMMV(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM03V_Y->setValue(
      t4SAlignment->getMMV(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM03V_Z->setValue(
      t4SAlignment->getMMV(3)[2] / 10);
  uiMainWindow->checkBox_MM03V->setChecked(t4SAlignment->getMMV(3)[3]);

  uiMainWindow->doubleSpinBox_PosMM03U_X->setValue(
      t4SAlignment->getMMU(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMM03U_Y->setValue(
      t4SAlignment->getMMU(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMM03U_Z->setValue(
      t4SAlignment->getMMU(3)[2] / 10);
  uiMainWindow->checkBox_MM03U->setChecked(t4SAlignment->getMMU(3)[3]);

  uiMainWindow->checkBox_MM03->setChecked(
      t4SAlignment->getMMX(3)[3] || t4SAlignment->getMMY(3)[3]
          || t4SAlignment->getMMV(3)[3] || t4SAlignment->getMMU(3)[3]);

  uiMainWindow->doubleSpinBox_PosMP00X_X->setValue(
      t4SAlignment->getMPX(0)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP00X_Y->setValue(
      t4SAlignment->getMPX(0)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP00X_Z->setValue(
      t4SAlignment->getMPX(0)[2] / 10);
  uiMainWindow->checkBox_MP00X->setChecked(t4SAlignment->getMPX(0)[3]);

  uiMainWindow->checkBox_MP00->setChecked(t4SAlignment->getMPX(0)[3]);

  uiMainWindow->doubleSpinBox_PosMP01X_X->setValue(
      t4SAlignment->getMPX(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP01X_Y->setValue(
      t4SAlignment->getMPX(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP01X_Z->setValue(
      t4SAlignment->getMPX(1)[2] / 10);
  uiMainWindow->checkBox_MP01X->setChecked(t4SAlignment->getMPX(1)[3]);

  uiMainWindow->doubleSpinBox_PosMP01Y_X->setValue(
      t4SAlignment->getMPY(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP01Y_Y->setValue(
      t4SAlignment->getMPY(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP01Y_Z->setValue(
      t4SAlignment->getMPY(1)[2] / 10);
  uiMainWindow->checkBox_MP01Y->setChecked(t4SAlignment->getMPY(1)[3]);

  uiMainWindow->doubleSpinBox_PosMP01U_X->setValue(
      t4SAlignment->getMPU(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP01U_Y->setValue(
      t4SAlignment->getMPU(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP01U_Z->setValue(
      t4SAlignment->getMPU(1)[2] / 10);
  uiMainWindow->checkBox_MP01U->setChecked(t4SAlignment->getMPU(1)[3]);

  uiMainWindow->doubleSpinBox_PosMP01V_X->setValue(
      t4SAlignment->getMPV(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP01V_Y->setValue(
      t4SAlignment->getMPV(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP01V_Z->setValue(
      t4SAlignment->getMPV(1)[2] / 10);
  uiMainWindow->checkBox_MP01V->setChecked(t4SAlignment->getMPV(1)[3]);

  uiMainWindow->checkBox_MP01->setChecked(
      t4SAlignment->getMPX(1)[3] || t4SAlignment->getMPY(1)[3]
          || t4SAlignment->getMPU(1)[3] || t4SAlignment->getMPV(1)[3]);

  uiMainWindow->doubleSpinBox_PosMP02X_X->setValue(
      t4SAlignment->getMPX(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP02X_Y->setValue(
      t4SAlignment->getMPX(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP02X_Z->setValue(
      t4SAlignment->getMPX(2)[2] / 10);
  uiMainWindow->checkBox_MP02X->setChecked(t4SAlignment->getMPX(2)[3]);

  uiMainWindow->doubleSpinBox_PosMP02Y_X->setValue(
      t4SAlignment->getMPY(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP02Y_Y->setValue(
      t4SAlignment->getMPY(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP02Y_Z->setValue(
      t4SAlignment->getMPY(2)[2] / 10);
  uiMainWindow->checkBox_MP02Y->setChecked(t4SAlignment->getMPY(2)[3]);

  uiMainWindow->doubleSpinBox_PosMP02U_X->setValue(
      t4SAlignment->getMPU(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP02U_Y->setValue(
      t4SAlignment->getMPU(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP02U_Z->setValue(
      t4SAlignment->getMPU(2)[2] / 10);
  uiMainWindow->checkBox_MP02U->setChecked(t4SAlignment->getMPU(2)[3]);

  uiMainWindow->doubleSpinBox_PosMP02V_X->setValue(
      t4SAlignment->getMPV(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP02V_Y->setValue(
      t4SAlignment->getMPV(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP02V_Z->setValue(
      t4SAlignment->getMPV(2)[2] / 10);
  uiMainWindow->checkBox_MP02V->setChecked(t4SAlignment->getMPV(2)[3]);

  uiMainWindow->checkBox_MP02->setChecked(
      t4SAlignment->getMPX(2)[3] || t4SAlignment->getMPY(2)[3]
          || t4SAlignment->getMPU(2)[3] || t4SAlignment->getMPV(2)[3]);

  uiMainWindow->doubleSpinBox_PosMP03X_X->setValue(
      t4SAlignment->getMPX(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP03X_Y->setValue(
      t4SAlignment->getMPX(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP03X_Z->setValue(
      t4SAlignment->getMPX(3)[2] / 10);
  uiMainWindow->checkBox_MP03X->setChecked(t4SAlignment->getMPX(3)[3]);

  uiMainWindow->doubleSpinBox_PosMP03Y_X->setValue(
      t4SAlignment->getMPY(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP03Y_Y->setValue(
      t4SAlignment->getMPY(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP03Y_Z->setValue(
      t4SAlignment->getMPY(3)[2] / 10);
  uiMainWindow->checkBox_MP03Y->setChecked(t4SAlignment->getMPY(3)[3]);

  uiMainWindow->doubleSpinBox_PosMP03U_X->setValue(
      t4SAlignment->getMPU(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP03U_Y->setValue(
      t4SAlignment->getMPU(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP03U_Z->setValue(
      t4SAlignment->getMPU(3)[2] / 10);
  uiMainWindow->checkBox_MP03U->setChecked(t4SAlignment->getMPU(3)[3]);

  uiMainWindow->doubleSpinBox_PosMP03V_X->setValue(
      t4SAlignment->getMPV(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosMP03V_Y->setValue(
      t4SAlignment->getMPV(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosMP03V_Z->setValue(
      t4SAlignment->getMPV(3)[2] / 10);
  uiMainWindow->checkBox_MP03V->setChecked(t4SAlignment->getMPV(3)[3]);

  uiMainWindow->checkBox_MP03->setChecked(
      t4SAlignment->getMPX(3)[3] || t4SAlignment->getMPY(3)[3]
          || t4SAlignment->getMPU(3)[3] || t4SAlignment->getMPV(3)[3]);


  // GEM
  uiMainWindow->doubleSpinBox_PosGM01X_X->setValue(
      t4SAlignment->getGMX(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM01X_Y->setValue(
      t4SAlignment->getGMX(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM01X_Z->setValue(
      t4SAlignment->getGMX(1)[2] / 10);
  uiMainWindow->checkBox_GM01X->setChecked(t4SAlignment->getGMX(1)[3]);

  uiMainWindow->doubleSpinBox_PosGM01U_X->setValue(
      t4SAlignment->getGMU(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM01U_Y->setValue(
      t4SAlignment->getGMU(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM01U_Z->setValue(
      t4SAlignment->getGMU(1)[2] / 10);
  uiMainWindow->checkBox_GM01U->setChecked(t4SAlignment->getGMU(1)[3]);

  uiMainWindow->doubleSpinBox_PosGM02X_X->setValue(
      t4SAlignment->getGMX(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM02X_Y->setValue(
      t4SAlignment->getGMX(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM02X_Z->setValue(
      t4SAlignment->getGMX(2)[2] / 10);
  uiMainWindow->checkBox_GM02X->setChecked(t4SAlignment->getGMX(2)[3]);

  uiMainWindow->doubleSpinBox_PosGM02U_X->setValue(
      t4SAlignment->getGMU(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM02U_Y->setValue(
      t4SAlignment->getGMU(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM02U_Z->setValue(
      t4SAlignment->getGMU(2)[2] / 10);
  uiMainWindow->checkBox_GM02U->setChecked(t4SAlignment->getGMU(2)[3]);

  uiMainWindow->doubleSpinBox_PosGM03X_X->setValue(
      t4SAlignment->getGMX(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM03X_Y->setValue(
      t4SAlignment->getGMX(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM03X_Z->setValue(
      t4SAlignment->getGMX(3)[2] / 10);
  uiMainWindow->checkBox_GM03X->setChecked(t4SAlignment->getGMX(3)[3]);

  uiMainWindow->doubleSpinBox_PosGM03U_X->setValue(
      t4SAlignment->getGMU(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM03U_Y->setValue(
      t4SAlignment->getGMU(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM03U_Z->setValue(
      t4SAlignment->getGMU(3)[2] / 10);
  uiMainWindow->checkBox_GM03U->setChecked(t4SAlignment->getGMU(3)[3]);

  uiMainWindow->doubleSpinBox_PosGM04X_X->setValue(
      t4SAlignment->getGMX(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM04X_Y->setValue(
      t4SAlignment->getGMX(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM04X_Z->setValue(
      t4SAlignment->getGMX(4)[2] / 10);
  uiMainWindow->checkBox_GM04X->setChecked(t4SAlignment->getGMX(4)[3]);

  uiMainWindow->doubleSpinBox_PosGM04U_X->setValue(
      t4SAlignment->getGMU(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM04U_Y->setValue(
      t4SAlignment->getGMU(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM04U_Z->setValue(
      t4SAlignment->getGMU(4)[2] / 10);
  uiMainWindow->checkBox_GM04U->setChecked(t4SAlignment->getGMU(4)[3]);

  uiMainWindow->doubleSpinBox_PosGM05X_X->setValue(
      t4SAlignment->getGMX(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM05X_Y->setValue(
      t4SAlignment->getGMX(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM05X_Z->setValue(
      t4SAlignment->getGMX(5)[2] / 10);
  uiMainWindow->checkBox_GM05X->setChecked(t4SAlignment->getGMX(5)[3]);

  uiMainWindow->doubleSpinBox_PosGM05U_X->setValue(
      t4SAlignment->getGMU(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM05U_Y->setValue(
      t4SAlignment->getGMU(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM05U_Z->setValue(
      t4SAlignment->getGMU(5)[2] / 10);
  uiMainWindow->checkBox_GM05U->setChecked(t4SAlignment->getGMU(5)[3]);

  uiMainWindow->doubleSpinBox_PosGM06X_X->setValue(
      t4SAlignment->getGMX(6)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM06X_Y->setValue(
      t4SAlignment->getGMX(6)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM06X_Z->setValue(
      t4SAlignment->getGMX(6)[2] / 10);
  uiMainWindow->checkBox_GM06X->setChecked(t4SAlignment->getGMX(6)[3]);

  uiMainWindow->doubleSpinBox_PosGM06U_X->setValue(
      t4SAlignment->getGMU(6)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM06U_Y->setValue(
      t4SAlignment->getGMU(6)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM06U_Z->setValue(
      t4SAlignment->getGMU(6)[2] / 10);
  uiMainWindow->checkBox_GM06U->setChecked(t4SAlignment->getGMU(6)[3]);

  uiMainWindow->doubleSpinBox_PosGM07X_X->setValue(
      t4SAlignment->getGMX(7)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM07X_Y->setValue(
      t4SAlignment->getGMX(7)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM07X_Z->setValue(
      t4SAlignment->getGMX(7)[2] / 10);
  uiMainWindow->checkBox_GM07X->setChecked(t4SAlignment->getGMX(7)[3]);

  uiMainWindow->doubleSpinBox_PosGM07U_X->setValue(
      t4SAlignment->getGMU(7)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM07U_Y->setValue(
      t4SAlignment->getGMU(7)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM07U_Z->setValue(
      t4SAlignment->getGMU(7)[2] / 10);
  uiMainWindow->checkBox_GM07U->setChecked(t4SAlignment->getGMU(7)[3]);

  uiMainWindow->doubleSpinBox_PosGM08X_X->setValue(
      t4SAlignment->getGMX(8)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM08X_Y->setValue(
      t4SAlignment->getGMX(8)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM08X_Z->setValue(
      t4SAlignment->getGMX(8)[2] / 10);
  uiMainWindow->checkBox_GM08X->setChecked(t4SAlignment->getGMX(8)[3]);

  uiMainWindow->doubleSpinBox_PosGM08U_X->setValue(
      t4SAlignment->getGMU(8)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM08U_Y->setValue(
      t4SAlignment->getGMU(8)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM08U_Z->setValue(
      t4SAlignment->getGMU(8)[2] / 10);
  uiMainWindow->checkBox_GM08U->setChecked(t4SAlignment->getGMU(8)[3]);

  uiMainWindow->doubleSpinBox_PosGM09X_X->setValue(
      t4SAlignment->getGMX(9)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM09X_Y->setValue(
      t4SAlignment->getGMX(9)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM09X_Z->setValue(
      t4SAlignment->getGMX(9)[2] / 10);
  uiMainWindow->checkBox_GM09X->setChecked(t4SAlignment->getGMX(9)[3]);

  uiMainWindow->doubleSpinBox_PosGM09U_X->setValue(
      t4SAlignment->getGMU(9)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM09U_Y->setValue(
      t4SAlignment->getGMU(9)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM09U_Z->setValue(
      t4SAlignment->getGMU(9)[2] / 10);
  uiMainWindow->checkBox_GM09U->setChecked(t4SAlignment->getGMU(9)[3]);

  uiMainWindow->doubleSpinBox_PosGM10X_X->setValue(
      t4SAlignment->getGMX(10)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM10X_Y->setValue(
      t4SAlignment->getGMX(10)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM10X_Z->setValue(
      t4SAlignment->getGMX(10)[2] / 10);
  uiMainWindow->checkBox_GM10X->setChecked(t4SAlignment->getGMX(10)[3]);

  uiMainWindow->doubleSpinBox_PosGM10U_X->setValue(
      t4SAlignment->getGMU(10)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM10U_Y->setValue(
      t4SAlignment->getGMU(10)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM10U_Z->setValue(
      t4SAlignment->getGMU(10)[2] / 10);
  uiMainWindow->checkBox_GM10U->setChecked(t4SAlignment->getGMU(10)[3]);

  uiMainWindow->doubleSpinBox_PosGM11X_X->setValue(
      t4SAlignment->getGMX(11)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM11X_Y->setValue(
      t4SAlignment->getGMX(11)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM11X_Z->setValue(
      t4SAlignment->getGMX(11)[2] / 10);
  uiMainWindow->checkBox_GM11X->setChecked(t4SAlignment->getGMX(11)[3]);

  uiMainWindow->doubleSpinBox_PosGM11U_X->setValue(
      t4SAlignment->getGMU(11)[0] / 10);
  uiMainWindow->doubleSpinBox_PosGM11U_Y->setValue(
      t4SAlignment->getGMU(11)[1] / 10);
  uiMainWindow->doubleSpinBox_PosGM11U_Z->setValue(
      t4SAlignment->getGMU(11)[2] / 10);
  uiMainWindow->checkBox_GM11U->setChecked(t4SAlignment->getGMU(11)[3]);

  uiMainWindow->checkBox_GEM->setChecked(false);
  for (int i = 1; i <= 11; i++)
    if (t4SAlignment->getGMX(i)[3]) {
      uiMainWindow->checkBox_GEM->setChecked(true);
      break;
    }

  uiMainWindow->doubleSpinBox_PosGP01X_X->setValue(
      t4SAlignment->getGP01X()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP01X_Y->setValue(
      t4SAlignment->getGP01X()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP01X_Z->setValue(
      t4SAlignment->getGP01X()[2] / 10);
  uiMainWindow->checkBox_GP01X->setChecked(t4SAlignment->getGP01X()[3]);

  uiMainWindow->doubleSpinBox_PosGP01U_X->setValue(
      t4SAlignment->getGP01U()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP01U_Y->setValue(
      t4SAlignment->getGP01U()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP01U_Z->setValue(
      t4SAlignment->getGP01U()[2] / 10);
  uiMainWindow->checkBox_GP01U->setChecked(t4SAlignment->getGP01U()[3]);

  uiMainWindow->doubleSpinBox_PosGP02X_X->setValue(
      t4SAlignment->getGP02X()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP02X_Y->setValue(
      t4SAlignment->getGP02X()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP02X_Z->setValue(
      t4SAlignment->getGP02X()[2] / 10);
  uiMainWindow->checkBox_GP02X->setChecked(t4SAlignment->getGP02X()[3]);

  uiMainWindow->doubleSpinBox_PosGP02U_X->setValue(
      t4SAlignment->getGP02U()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP02U_Y->setValue(
      t4SAlignment->getGP02U()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP02U_Z->setValue(
      t4SAlignment->getGP02U()[2] / 10);
  uiMainWindow->checkBox_GP02U->setChecked(t4SAlignment->getGP02U()[3]);

  uiMainWindow->doubleSpinBox_PosGP03X_X->setValue(
      t4SAlignment->getGP03X()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP03X_Y->setValue(
      t4SAlignment->getGP03X()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP03X_Z->setValue(
      t4SAlignment->getGP03X()[2] / 10);
  uiMainWindow->checkBox_GP03X->setChecked(t4SAlignment->getGP03X()[3]);

  uiMainWindow->doubleSpinBox_PosGP03U_X->setValue(
      t4SAlignment->getGP03U()[0] / 10);
  uiMainWindow->doubleSpinBox_PosGP03U_Y->setValue(
      t4SAlignment->getGP03U()[1] / 10);
  uiMainWindow->doubleSpinBox_PosGP03U_Z->setValue(
      t4SAlignment->getGP03U()[2] / 10);
  uiMainWindow->checkBox_GP03U->setChecked(t4SAlignment->getGP03U()[3]);

  uiMainWindow->checkBox_PGEM->setChecked(
      t4SAlignment->getGP01X()[3] || t4SAlignment->getGP01U()[3]
          || t4SAlignment->getGP02X()[3] || t4SAlignment->getGP02U()[3]
          || t4SAlignment->getGP03X()[3] || t4SAlignment->getGP03U()[3]);

  // MWPC
  uiMainWindow->doubleSpinBox_PosPA01X_X->setValue(
      t4SAlignment->getPA(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA01X_Y->setValue(
      t4SAlignment->getPA(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA01X_Z->setValue(
      t4SAlignment->getPA(1)[2] / 10);
  uiMainWindow->checkBox_PA01X->setChecked(t4SAlignment->getPA(1)[3]);

  uiMainWindow->doubleSpinBox_PosPA02X_X->setValue(
      t4SAlignment->getPA(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA02X_Y->setValue(
      t4SAlignment->getPA(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA02X_Z->setValue(
      t4SAlignment->getPA(2)[2] / 10);
  uiMainWindow->checkBox_PA02X->setChecked(t4SAlignment->getPA(2)[3]);

  uiMainWindow->doubleSpinBox_PosPA03X_X->setValue(
      t4SAlignment->getPA(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA03X_Y->setValue(
      t4SAlignment->getPA(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA03X_Z->setValue(
      t4SAlignment->getPA(3)[2] / 10);
  uiMainWindow->checkBox_PA03X->setChecked(t4SAlignment->getPA(3)[3]);

  uiMainWindow->doubleSpinBox_PosPA04X_X->setValue(
      t4SAlignment->getPA(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA04X_Y->setValue(
      t4SAlignment->getPA(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA04X_Z->setValue(
      t4SAlignment->getPA(4)[2] / 10);
  uiMainWindow->checkBox_PA04X->setChecked(t4SAlignment->getPA(4)[3]);

  uiMainWindow->doubleSpinBox_PosPA05X_X->setValue(
      t4SAlignment->getPA(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA05X_Y->setValue(
      t4SAlignment->getPA(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA05X_Z->setValue(
      t4SAlignment->getPA(5)[2] / 10);
  uiMainWindow->checkBox_PA05X->setChecked(t4SAlignment->getPA(5)[3]);

  uiMainWindow->doubleSpinBox_PosPA06X_X->setValue(
      t4SAlignment->getPA(6)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA06X_Y->setValue(
      t4SAlignment->getPA(6)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA06X_Z->setValue(
      t4SAlignment->getPA(6)[2] / 10);
  uiMainWindow->checkBox_PA06X->setChecked(t4SAlignment->getPA(6)[3]);

  uiMainWindow->doubleSpinBox_PosPA11X_X->setValue(
      t4SAlignment->getPA(11)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPA11X_Y->setValue(
      t4SAlignment->getPA(11)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPA11X_Z->setValue(
      t4SAlignment->getPA(11)[2] / 10);
  uiMainWindow->checkBox_PA11X->setChecked(t4SAlignment->getPA(11)[3]);

  uiMainWindow->checkBox_PA->setChecked(false);
  for (int i = 1; i <= 11; i++)
    if (t4SAlignment->getPA(i)[3]) {
      uiMainWindow->checkBox_PA->setChecked(true);
      break;
    }

  uiMainWindow->doubleSpinBox_PosPB01X_X->setValue(
      t4SAlignment->getPB(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB01X_Y->setValue(
      t4SAlignment->getPB(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB01X_Z->setValue(
      t4SAlignment->getPB(1)[2] / 10);
  uiMainWindow->checkBox_PB01X->setChecked(t4SAlignment->getPB(1)[3]);

  uiMainWindow->doubleSpinBox_PosPB03X_X->setValue(
      t4SAlignment->getPB(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB03X_Y->setValue(
      t4SAlignment->getPB(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB03X_Z->setValue(
      t4SAlignment->getPB(3)[2] / 10);
  uiMainWindow->checkBox_PB03X->setChecked(t4SAlignment->getPB(3)[3]);

  uiMainWindow->doubleSpinBox_PosPB05X_X->setValue(
      t4SAlignment->getPB(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB05X_Y->setValue(
      t4SAlignment->getPB(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB05X_Z->setValue(
      t4SAlignment->getPB(5)[2] / 10);
  uiMainWindow->checkBox_PB05X->setChecked(t4SAlignment->getPB(5)[3]);

  uiMainWindow->doubleSpinBox_PosPB02V_X->setValue(
      t4SAlignment->getPB(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB02V_Y->setValue(
      t4SAlignment->getPB(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB02V_Z->setValue(
      t4SAlignment->getPB(2)[2] / 10);
  uiMainWindow->checkBox_PB02V->setChecked(t4SAlignment->getPB(2)[3]);

  uiMainWindow->doubleSpinBox_PosPB04V_X->setValue(
      t4SAlignment->getPB(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB04V_Y->setValue(
      t4SAlignment->getPB(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB04V_Z->setValue(
      t4SAlignment->getPB(4)[2] / 10);
  uiMainWindow->checkBox_PB04V->setChecked(t4SAlignment->getPB(4)[3]);

  uiMainWindow->doubleSpinBox_PosPB06V_X->setValue(
      t4SAlignment->getPB(6)[0] / 10);
  uiMainWindow->doubleSpinBox_PosPB06V_Y->setValue(
      t4SAlignment->getPB(6)[1] / 10);
  uiMainWindow->doubleSpinBox_PosPB06V_Z->setValue(
      t4SAlignment->getPB(6)[2] / 10);
  uiMainWindow->checkBox_PB06V->setChecked(t4SAlignment->getPB(6)[3]);

  uiMainWindow->checkBox_PB->setChecked(false);
  for (int i = 1; i <= 6; i++)
    if (t4SAlignment->getPB(i)[3]) {
      uiMainWindow->checkBox_PB->setChecked(true);
      break;
    }

  uiMainWindow->doubleSpinBox_PosPS01X_X->setValue(
      t4SAlignment->getPS01()[0] / 10);
  uiMainWindow->doubleSpinBox_PosPS01X_Y->setValue(
      t4SAlignment->getPS01()[1] / 10);
  uiMainWindow->doubleSpinBox_PosPS01X_Z->setValue(
      t4SAlignment->getPS01()[2] / 10);
  uiMainWindow->checkBox_PS01X->setChecked(t4SAlignment->getPS01()[3]);
  uiMainWindow->checkBox_PS->setChecked(t4SAlignment->getPS01()[3]);

  // SI
  uiMainWindow->doubleSpinBox_PosSI01X_X->setValue(
      t4SAlignment->getSIX(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI01X_Y->setValue(
      t4SAlignment->getSIX(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI01X_Z->setValue(
      t4SAlignment->getSIX(1)[2] / 10);
  uiMainWindow->checkBox_SI01X->setChecked(t4SAlignment->getSIX(1)[3]);

  uiMainWindow->doubleSpinBox_PosSI01U_X->setValue(
      t4SAlignment->getSIU(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI01U_Y->setValue(
      t4SAlignment->getSIU(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI01U_Z->setValue(
      t4SAlignment->getSIU(1)[2] / 10);
  uiMainWindow->checkBox_SI01U->setChecked(t4SAlignment->getSIU(1)[3]);

  uiMainWindow->doubleSpinBox_PosSI02X_X->setValue(
      t4SAlignment->getSIX(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI02X_Y->setValue(
      t4SAlignment->getSIX(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI02X_Z->setValue(
      t4SAlignment->getSIX(2)[2] / 10);
  uiMainWindow->checkBox_SI02X->setChecked(t4SAlignment->getSIX(2)[3]);

  uiMainWindow->doubleSpinBox_PosSI02U_X->setValue(
      t4SAlignment->getSIU(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI02U_Y->setValue(
      t4SAlignment->getSIU(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI02U_Z->setValue(
      t4SAlignment->getSIU(2)[2] / 10);
  uiMainWindow->checkBox_SI02U->setChecked(t4SAlignment->getSIU(2)[3]);

  uiMainWindow->doubleSpinBox_PosSI03X_X->setValue(
      t4SAlignment->getSIX(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI03X_Y->setValue(
      t4SAlignment->getSIX(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI03X_Z->setValue(
      t4SAlignment->getSIX(3)[2] / 10);
  uiMainWindow->checkBox_SI03X->setChecked(t4SAlignment->getSIX(3)[3]);

  uiMainWindow->doubleSpinBox_PosSI03U_X->setValue(
      t4SAlignment->getSIU(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI03U_Y->setValue(
      t4SAlignment->getSIU(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI03U_Z->setValue(
      t4SAlignment->getSIU(3)[2] / 10);
  uiMainWindow->checkBox_SI03U->setChecked(t4SAlignment->getSIU(3)[3]);

  uiMainWindow->doubleSpinBox_PosSI04X_X->setValue(
      t4SAlignment->getSIX(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI04X_Y->setValue(
      t4SAlignment->getSIX(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI04X_Z->setValue(
      t4SAlignment->getSIX(4)[2] / 10);
  uiMainWindow->checkBox_SI04X->setChecked(t4SAlignment->getSIX(4)[3]);

  uiMainWindow->doubleSpinBox_PosSI04U_X->setValue(
      t4SAlignment->getSIU(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI04U_Y->setValue(
      t4SAlignment->getSIU(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI04U_Z->setValue(
      t4SAlignment->getSIU(4)[2] / 10);
  uiMainWindow->checkBox_SI04U->setChecked(t4SAlignment->getSIU(4)[3]);

  uiMainWindow->doubleSpinBox_PosSI05X_X->setValue(
      t4SAlignment->getSIX(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI05X_Y->setValue(
      t4SAlignment->getSIX(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI05X_Z->setValue(
      t4SAlignment->getSIX(5)[2] / 10);
  uiMainWindow->checkBox_SI05X->setChecked(t4SAlignment->getSIX(5)[3]);

  uiMainWindow->doubleSpinBox_PosSI05U_X->setValue(
      t4SAlignment->getSIU(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosSI05U_Y->setValue(
      t4SAlignment->getSIU(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosSI05U_Z->setValue(
      t4SAlignment->getSIU(5)[2] / 10);
  uiMainWindow->checkBox_SI05U->setChecked(t4SAlignment->getSIU(5)[3]);

  uiMainWindow->checkBox_SI->setChecked(false);
  for (int i = 1; i <= 5; i++)
    if (t4SAlignment->getSIX(i)[3] || t4SAlignment->getSIU(i)[3]) {
      uiMainWindow->checkBox_SI->setChecked(true);
      break;
    }

  // FI
  uiMainWindow->doubleSpinBox_PosFI01X_X->setValue(
      t4SAlignment->getFI(1)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI01X_Y->setValue(
      t4SAlignment->getFI(1)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI01X_Z->setValue(
      t4SAlignment->getFI(1)[2] / 10);
  uiMainWindow->checkBox_FI01X->setChecked(t4SAlignment->getFI(1)[3]);

  uiMainWindow->doubleSpinBox_PosFI15X_X->setValue(
      t4SAlignment->getFI(15)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI15X_Y->setValue(
      t4SAlignment->getFI(15)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI15X_Z->setValue(
      t4SAlignment->getFI(15)[2] / 10);
  uiMainWindow->checkBox_FI15X->setChecked(t4SAlignment->getFI(15)[3]);
  uiMainWindow->checkBox_FI15X_useU->setChecked(t4SAlignment->getFI(15)[4]);

  uiMainWindow->doubleSpinBox_PosFI02X_X->setValue(
      t4SAlignment->getFI(2)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI02X_Y->setValue(
      t4SAlignment->getFI(2)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI02X_Z->setValue(
      t4SAlignment->getFI(2)[2] / 10);
  uiMainWindow->checkBox_FI02X->setChecked(t4SAlignment->getFI(2)[3]);

  uiMainWindow->doubleSpinBox_PosFI03X_X->setValue(
      t4SAlignment->getFI(3)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI03X_Y->setValue(
      t4SAlignment->getFI(3)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI03X_Z->setValue(
      t4SAlignment->getFI(3)[2] / 10);
  uiMainWindow->checkBox_FI03X->setChecked(t4SAlignment->getFI(3)[3]);

  uiMainWindow->doubleSpinBox_PosFI35X_X->setValue(
      t4SAlignment->getFI(35)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI35X_Y->setValue(
      t4SAlignment->getFI(35)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI35X_Z->setValue(
      t4SAlignment->getFI(35)[2] / 10);
  uiMainWindow->checkBox_FI35X->setChecked(t4SAlignment->getFI(35)[3]);

  uiMainWindow->doubleSpinBox_PosFI04X_X->setValue(
      t4SAlignment->getFI(4)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI04X_Y->setValue(
      t4SAlignment->getFI(4)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI04X_Z->setValue(
      t4SAlignment->getFI(4)[2] / 10);
  uiMainWindow->checkBox_FI04X->setChecked(t4SAlignment->getFI(4)[3]);

  uiMainWindow->doubleSpinBox_PosFI05X_X->setValue(
      t4SAlignment->getFI(5)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI05X_Y->setValue(
      t4SAlignment->getFI(5)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI05X_Z->setValue(
      t4SAlignment->getFI(5)[2] / 10);
  uiMainWindow->checkBox_FI05X->setChecked(t4SAlignment->getFI(5)[3]);

  uiMainWindow->doubleSpinBox_PosFI55U_X->setValue(
      t4SAlignment->getFI(55)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI55U_Y->setValue(
      t4SAlignment->getFI(55)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI55U_Z->setValue(
      t4SAlignment->getFI(55)[2] / 10);
  uiMainWindow->checkBox_FI55U->setChecked(t4SAlignment->getFI(55)[3]);

  uiMainWindow->doubleSpinBox_PosFI06X_X->setValue(
      t4SAlignment->getFI(6)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI06X_Y->setValue(
      t4SAlignment->getFI(6)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI06X_Z->setValue(
      t4SAlignment->getFI(6)[2] / 10);
  uiMainWindow->checkBox_FI06X->setChecked(t4SAlignment->getFI(6)[3]);

  uiMainWindow->doubleSpinBox_PosFI07X_X->setValue(
      t4SAlignment->getFI(7)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI07X_Y->setValue(
      t4SAlignment->getFI(7)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI07X_Z->setValue(
      t4SAlignment->getFI(7)[2] / 10);
  uiMainWindow->checkBox_FI07X->setChecked(t4SAlignment->getFI(7)[3]);

  uiMainWindow->doubleSpinBox_PosFI08X_X->setValue(
      t4SAlignment->getFI(8)[0] / 10);
  uiMainWindow->doubleSpinBox_PosFI08X_Y->setValue(
      t4SAlignment->getFI(8)[1] / 10);
  uiMainWindow->doubleSpinBox_PosFI08X_Z->setValue(
      t4SAlignment->getFI(8)[2] / 10);
  uiMainWindow->checkBox_FI08X->setChecked(t4SAlignment->getFI(8)[3]);

  bool isOn = false;
  for (int i = 1; i <= 55; i++)
    if (t4SAlignment->getFI(i)[3]) {
      isOn = true;
      break;
    }
  uiMainWindow->checkBox_FI->setChecked(isOn);

  // Veto
  uiMainWindow->doubleSpinBox_VI01P1_X->setValue(
      t4SAlignment->getVI01P1()[0] / 10);
  uiMainWindow->doubleSpinBox_VI01P1_Y->setValue(
      t4SAlignment->getVI01P1()[1] / 10);
  uiMainWindow->doubleSpinBox_VI01P1_Z->setValue(
      t4SAlignment->getVI01P1()[2] / 10);
  uiMainWindow->checkBox_VI01P1->setChecked(t4SAlignment->getVI01P1()[3]);

  uiMainWindow->doubleSpinBox_VO01X1_X->setValue(
      t4SAlignment->getVO01X1()[0] / 10);
  uiMainWindow->doubleSpinBox_VO01X1_Y->setValue(
      t4SAlignment->getVO01X1()[1] / 10);
  uiMainWindow->doubleSpinBox_VO01X1_Z->setValue(
      t4SAlignment->getVO01X1()[2] / 10);
  uiMainWindow->checkBox_VO01X1->setChecked(t4SAlignment->getVO01X1()[3]);

  uiMainWindow->doubleSpinBox_VI02X1_X->setValue(
      t4SAlignment->getVI02X1()[0] / 10);
  uiMainWindow->doubleSpinBox_VI02X1_Y->setValue(
      t4SAlignment->getVI02X1()[1] / 10);
  uiMainWindow->doubleSpinBox_VI02X1_Z->setValue(
      t4SAlignment->getVI02X1()[2] / 10);
  uiMainWindow->checkBox_VI02X1->setChecked(t4SAlignment->getVI02X1()[3]);

  uiMainWindow->checkBox_Veto->setChecked(
      t4SAlignment->getVI01P1()[3] || t4SAlignment->getVO01X1()[3]
          || t4SAlignment->getVI02X1()[3]);

  // Calorimeter
  uiMainWindow->doubleSpinBox_PosECAL0X->setValue(
      t4SAlignment->getECAL0()[0] / 10);
  uiMainWindow->doubleSpinBox_PosECAL0Y->setValue(
      t4SAlignment->getECAL0()[1] / 10);
  uiMainWindow->doubleSpinBox_PosECAL0Z->setValue(
      t4SAlignment->getECAL0()[2] / 10);
  uiMainWindow->checkBox_ECAL0->setChecked(t4SAlignment->getECAL0()[3]);

  uiMainWindow->doubleSpinBox_PosECAL1X->setValue(
      t4SAlignment->getECAL1()[0] / 10);
  uiMainWindow->doubleSpinBox_PosECAL1Y->setValue(
      t4SAlignment->getECAL1()[1] / 10);
  uiMainWindow->doubleSpinBox_PosECAL1Z->setValue(
      t4SAlignment->getECAL1()[2] / 10);
  uiMainWindow->checkBox_ECAL1->setChecked(t4SAlignment->getECAL1()[3]);

  uiMainWindow->doubleSpinBox_PosECAL2X->setValue(
      t4SAlignment->getECAL2()[0] / 10);
  uiMainWindow->doubleSpinBox_PosECAL2Y->setValue(
      t4SAlignment->getECAL2()[1] / 10);
  uiMainWindow->doubleSpinBox_PosECAL2Z->setValue(
      t4SAlignment->getECAL2()[2] / 10);
  uiMainWindow->checkBox_ECAL2->setChecked(t4SAlignment->getECAL2()[3]);

  uiMainWindow->doubleSpinBox_PosHCAL1X->setValue(
      t4SAlignment->getHCAL1()[0] / 10);
  uiMainWindow->doubleSpinBox_PosHCAL1Y->setValue(
      t4SAlignment->getHCAL1()[1] / 10);
  uiMainWindow->doubleSpinBox_PosHCAL1Z->setValue(
      t4SAlignment->getHCAL1()[2] / 10);
  uiMainWindow->checkBox_HCAL1->setChecked(t4SAlignment->getHCAL1()[3]);

  uiMainWindow->doubleSpinBox_PosHCAL2X->setValue(
      t4SAlignment->getHCAL2()[0] / 10);
  uiMainWindow->doubleSpinBox_PosHCAL2Y->setValue(
      t4SAlignment->getHCAL2()[1] / 10);
  uiMainWindow->doubleSpinBox_PosHCAL2Z->setValue(
      t4SAlignment->getHCAL2()[2] / 10);
  uiMainWindow->checkBox_HCAL2->setChecked(t4SAlignment->getHCAL2()[3]);

  // Magnets
  uiMainWindow->doubleSpinBox_PosSM1X->setValue(t4SAlignment->getSM1()[0] / 10);
  uiMainWindow->doubleSpinBox_PosSM1Y->setValue(t4SAlignment->getSM1()[1] / 10);
  uiMainWindow->doubleSpinBox_PosSM1Z->setValue(t4SAlignment->getSM1()[2] / 10);
  uiMainWindow->checkBox_SM1->setChecked(t4SAlignment->getSM1()[3]);
  uiMainWindow->doubleSpinBox_FieldScaleSM1->setValue(t4SAlignment->getSM1()[4]);
  
  uiMainWindow->doubleSpinBox_PosSM2X->setValue(t4SAlignment->getSM2()[0] / 10);
  uiMainWindow->doubleSpinBox_PosSM2Y->setValue(t4SAlignment->getSM2()[1] / 10);
  uiMainWindow->doubleSpinBox_PosSM2Z->setValue(t4SAlignment->getSM2()[2] / 10);
  uiMainWindow->checkBox_SM2->setChecked(t4SAlignment->getSM2()[3]);
  uiMainWindow->doubleSpinBox_FieldScaleSM2->setValue(t4SAlignment->getSM2()[4]);
  uiMainWindow->comboBox_SM2Field->setCurrentIndex(
      uiMainWindow->comboBox_SM2Field->findText(
          QString::fromStdString(intToStr(t4SAlignment->getSM2()[5]))));


  // RICH
  uiMainWindow->doubleSpinBox_PosRichX->setValue(
      t4SAlignment->getRICH()[0] / 10);
  uiMainWindow->doubleSpinBox_PosRichY->setValue(
      t4SAlignment->getRICH()[1] / 10);
  uiMainWindow->doubleSpinBox_PosRichZ->setValue(
      t4SAlignment->getRICH()[2] / 10);
  uiMainWindow->checkBox_Rich->setChecked(t4SAlignment->getRICH()[3]);

  // H3
  uiMainWindow->doubleSpinBox_HO03_X->setValue(t4SAlignment->getHO03()[0] / 10);
  uiMainWindow->doubleSpinBox_HO03_Y->setValue(t4SAlignment->getHO03()[1] / 10);
  uiMainWindow->doubleSpinBox_HO03_Z->setValue(t4SAlignment->getHO03()[2] / 10);
  uiMainWindow->checkBox_HO03->setChecked(t4SAlignment->getHO03()[3]);
  uiMainWindow->checkBox_H3O->setChecked(t4SAlignment->getHO03()[3]);

  // H4
  uiMainWindow->doubleSpinBox_HI04X1_u_X->setValue(
      t4SAlignment->getHI04X1_u()[0] / 10);
  uiMainWindow->doubleSpinBox_HI04X1_u_Y->setValue(
      t4SAlignment->getHI04X1_u()[1] / 10);
  uiMainWindow->doubleSpinBox_HI04X1_u_Z->setValue(
      t4SAlignment->getHI04X1_u()[2] / 10);
  uiMainWindow->checkBox_HI04X1_u->setChecked(t4SAlignment->getHI04X1_u()[3]);

  uiMainWindow->doubleSpinBox_HI04X1_d_X->setValue(
      t4SAlignment->getHI04X1_d()[0] / 10);
  uiMainWindow->doubleSpinBox_HI04X1_d_Y->setValue(
      t4SAlignment->getHI04X1_d()[1] / 10);
  uiMainWindow->doubleSpinBox_HI04X1_d_Z->setValue(
      t4SAlignment->getHI04X1_d()[2] / 10);
  uiMainWindow->checkBox_HI04X1_d->setChecked(t4SAlignment->getHI04X1_d()[3]);

  uiMainWindow->checkBox_H4I->setChecked(t4SAlignment->getHI04X1_u()[3] || t4SAlignment->getHI04X1_d()[3]);

  uiMainWindow->doubleSpinBox_HM04Y1_u1_X->setValue(
      t4SAlignment->getHM04Y1_u1()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_u1_Y->setValue(
      t4SAlignment->getHM04Y1_u1()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_u1_Z->setValue(
      t4SAlignment->getHM04Y1_u1()[2] / 10);
  uiMainWindow->checkBox_HM04Y1_u1->setChecked(t4SAlignment->getHM04Y1_u1()[3]);

  uiMainWindow->doubleSpinBox_HM04Y1_u2_X->setValue(
      t4SAlignment->getHM04Y1_u2()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_u2_Y->setValue(
      t4SAlignment->getHM04Y1_u2()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_u2_Z->setValue(
      t4SAlignment->getHM04Y1_u2()[2] / 10);
  uiMainWindow->checkBox_HM04Y1_u2->setChecked(t4SAlignment->getHM04Y1_u2()[3]);

  uiMainWindow->doubleSpinBox_HM04Y1_d1_X->setValue(
      t4SAlignment->getHM04Y1_d1()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_d1_Y->setValue(
      t4SAlignment->getHM04Y1_d1()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_d1_Z->setValue(
      t4SAlignment->getHM04Y1_d1()[2] / 10);
  uiMainWindow->checkBox_HM04Y1_d1->setChecked(t4SAlignment->getHM04Y1_d1()[3]);

  uiMainWindow->doubleSpinBox_HM04Y1_d2_X->setValue(
      t4SAlignment->getHM04Y1_d2()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_d2_Y->setValue(
      t4SAlignment->getHM04Y1_d2()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04Y1_d2_Z->setValue(
      t4SAlignment->getHM04Y1_d2()[2] / 10);
  uiMainWindow->checkBox_HM04Y1_d2->setChecked(t4SAlignment->getHM04Y1_d2()[3]);

  uiMainWindow->doubleSpinBox_HM04X1_u_X->setValue(
      t4SAlignment->getHM04X1_u()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04X1_u_Y->setValue(
      t4SAlignment->getHM04X1_u()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04X1_u_Z->setValue(
      t4SAlignment->getHM04X1_u()[2] / 10);
  uiMainWindow->checkBox_HM04X1_u->setChecked(t4SAlignment->getHM04X1_u()[3]);

  uiMainWindow->doubleSpinBox_HM04X1_d_X->setValue(
      t4SAlignment->getHM04X1_d()[0] / 10);
  uiMainWindow->doubleSpinBox_HM04X1_d_Y->setValue(
      t4SAlignment->getHM04X1_d()[1] / 10);
  uiMainWindow->doubleSpinBox_HM04X1_d_Z->setValue(
      t4SAlignment->getHM04X1_d()[2] / 10);
  uiMainWindow->checkBox_HM04X1_d->setChecked(t4SAlignment->getHM04X1_d()[3]);

  uiMainWindow->checkBox_H4M->setChecked(
      t4SAlignment->getHM04Y1_u1()[3] || t4SAlignment->getHM04Y1_u2()[3]
          || t4SAlignment->getHM04Y1_d1()[3] || t4SAlignment->getHM04Y1_d2()[3]
          || t4SAlignment->getHM04X1_u()[3]
          || t4SAlignment->getHM04X1_d()[3]);

  uiMainWindow->doubleSpinBox_HL04X1_1_X->setValue(
      t4SAlignment->getHL04X1_1()[0] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_1_Y->setValue(
      t4SAlignment->getHL04X1_1()[1] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_1_Z->setValue(
      t4SAlignment->getHL04X1_1()[2] / 10);
  uiMainWindow->checkBox_HL04X1_1->setChecked(t4SAlignment->getHL04X1_1()[3]);

  uiMainWindow->doubleSpinBox_HL04X1_2_X->setValue(
      t4SAlignment->getHL04X1_2()[0] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_2_Y->setValue(
      t4SAlignment->getHL04X1_2()[1] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_2_Z->setValue(
      t4SAlignment->getHL04X1_2()[2] / 10);
  uiMainWindow->checkBox_HL04X1_2->setChecked(t4SAlignment->getHL04X1_2()[3]);

  uiMainWindow->doubleSpinBox_HL04X1_3_X->setValue(
      t4SAlignment->getHL04X1_3()[0] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_3_Y->setValue(
      t4SAlignment->getHL04X1_3()[1] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_3_Z->setValue(
      t4SAlignment->getHL04X1_3()[2] / 10);
  uiMainWindow->checkBox_HL04X1_3->setChecked(t4SAlignment->getHL04X1_3()[3]);

  uiMainWindow->doubleSpinBox_HL04X1_4_X->setValue(
      t4SAlignment->getHL04X1_4()[0] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_4_Y->setValue(
      t4SAlignment->getHL04X1_4()[1] / 10);
  uiMainWindow->doubleSpinBox_HL04X1_4_Z->setValue(
      t4SAlignment->getHL04X1_4()[2] / 10);
  uiMainWindow->checkBox_HL04X1_4->setChecked(t4SAlignment->getHL04X1_4()[3]);

  uiMainWindow->checkBox_H4L->setChecked(
      t4SAlignment->getHL04X1_1()[3] || t4SAlignment->getHL04X1_2()[3]
          || t4SAlignment->getHL04X1_3()[3] || t4SAlignment->getHL04X1_4()[3]);

  uiMainWindow->doubleSpinBox_HO04_X->setValue(t4SAlignment->getHO04()[0] / 10);
  uiMainWindow->doubleSpinBox_HO04_Y->setValue(t4SAlignment->getHO04()[1] / 10);
  uiMainWindow->doubleSpinBox_HO04_Z->setValue(t4SAlignment->getHO04()[2] / 10);
  uiMainWindow->checkBox_HO04->setChecked(t4SAlignment->getHO04()[3]);
  uiMainWindow->checkBox_H4O->setChecked(t4SAlignment->getHO04()[3]);

  // H5
  uiMainWindow->doubleSpinBox_HI05X1_u_X->setValue(
      t4SAlignment->getHI05X1_u()[0] / 10);
  uiMainWindow->doubleSpinBox_HI05X1_u_Y->setValue(
      t4SAlignment->getHI05X1_u()[1] / 10);
  uiMainWindow->doubleSpinBox_HI05X1_u_Z->setValue(
      t4SAlignment->getHI05X1_u()[2] / 10);
  uiMainWindow->checkBox_HI05X1_u->setChecked(t4SAlignment->getHI05X1_u()[3]);

  uiMainWindow->doubleSpinBox_HI05X1_d_X->setValue(
      t4SAlignment->getHI05X1_d()[0] / 10);
  uiMainWindow->doubleSpinBox_HI05X1_d_Y->setValue(
      t4SAlignment->getHI05X1_d()[1] / 10);
  uiMainWindow->doubleSpinBox_HI05X1_d_Z->setValue(
      t4SAlignment->getHI05X1_d()[2] / 10);
  uiMainWindow->checkBox_HI05X1_d->setChecked(t4SAlignment->getHI05X1_d()[3]);

  uiMainWindow->checkBox_H5I->setChecked(
      t4SAlignment->getHI05X1_u()[3] || t4SAlignment->getHI05X1_d()[3]);

  uiMainWindow->doubleSpinBox_HM05Y1_u1_X->setValue(
      t4SAlignment->getHM05Y1_u1()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_u1_Y->setValue(
      t4SAlignment->getHM05Y1_u1()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_u1_Z->setValue(
      t4SAlignment->getHM05Y1_u1()[2] / 10);
  uiMainWindow->checkBox_HM05Y1_u1->setChecked(t4SAlignment->getHM05Y1_u1()[3]);

  uiMainWindow->doubleSpinBox_HM05Y1_u2_X->setValue(
      t4SAlignment->getHM05Y1_u2()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_u2_Y->setValue(
      t4SAlignment->getHM05Y1_u2()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_u2_Z->setValue(
      t4SAlignment->getHM05Y1_u2()[2] / 10);
  uiMainWindow->checkBox_HM05Y1_u2->setChecked(t4SAlignment->getHM05Y1_u2()[3]);

  uiMainWindow->doubleSpinBox_HM05Y1_d1_X->setValue(
      t4SAlignment->getHM05Y1_d1()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_d1_Y->setValue(
      t4SAlignment->getHM05Y1_d1()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_d1_Z->setValue(
      t4SAlignment->getHM05Y1_d1()[2] / 10);
  uiMainWindow->checkBox_HM05Y1_d1->setChecked(t4SAlignment->getHM05Y1_d1()[3]);

  uiMainWindow->doubleSpinBox_HM05Y1_d2_X->setValue(
      t4SAlignment->getHM05Y1_d2()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_d2_Y->setValue(
      t4SAlignment->getHM05Y1_d2()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05Y1_d2_Z->setValue(
      t4SAlignment->getHM05Y1_d2()[2] / 10);
  uiMainWindow->checkBox_HM05Y1_d2->setChecked(t4SAlignment->getHM05Y1_d2()[3]);

  uiMainWindow->doubleSpinBox_HM05X1_u_X->setValue(
      t4SAlignment->getHM05X1_u()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05X1_u_Y->setValue(
      t4SAlignment->getHM05X1_u()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05X1_u_Z->setValue(
      t4SAlignment->getHM05X1_u()[2] / 10);
  uiMainWindow->checkBox_HM05X1_u->setChecked(t4SAlignment->getHM05X1_u()[3]);

  uiMainWindow->doubleSpinBox_HM05X1_d_X->setValue(
      t4SAlignment->getHM05X1_d()[0] / 10);
  uiMainWindow->doubleSpinBox_HM05X1_d_Y->setValue(
      t4SAlignment->getHM05X1_d()[1] / 10);
  uiMainWindow->doubleSpinBox_HM05X1_d_Z->setValue(
      t4SAlignment->getHM05X1_d()[2] / 10);
  uiMainWindow->checkBox_HM05X1_d->setChecked(t4SAlignment->getHM05X1_d()[3]);

  uiMainWindow->checkBox_H5M->setChecked(
      t4SAlignment->getHM05Y1_u1()[3] || t4SAlignment->getHM05Y1_u2()[3]
          || t4SAlignment->getHM05Y1_d1()[3] || t4SAlignment->getHM05Y1_d2()[3]
          || t4SAlignment->getHM05X1_u()[3] || t4SAlignment->getHM05X1_d()[3]);

  uiMainWindow->doubleSpinBox_HL05X1_1_X->setValue(
      t4SAlignment->getHL05X1_1()[0] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_1_Y->setValue(
      t4SAlignment->getHL05X1_1()[1] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_1_Z->setValue(
      t4SAlignment->getHL05X1_1()[2] / 10);
  uiMainWindow->checkBox_HL05X1_1->setChecked(t4SAlignment->getHL05X1_1()[3]);

  uiMainWindow->doubleSpinBox_HL05X1_2_X->setValue(
      t4SAlignment->getHL05X1_2()[0] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_2_Y->setValue(
      t4SAlignment->getHL05X1_2()[1] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_2_Z->setValue(
      t4SAlignment->getHL05X1_2()[2] / 10);
  uiMainWindow->checkBox_HL05X1_2->setChecked(t4SAlignment->getHL05X1_2()[3]);

  uiMainWindow->doubleSpinBox_HL05X1_3_X->setValue(
      t4SAlignment->getHL05X1_3()[0] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_3_Y->setValue(
      t4SAlignment->getHL05X1_3()[1] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_3_Z->setValue(
      t4SAlignment->getHL05X1_3()[2] / 10);
  uiMainWindow->checkBox_HL05X1_3->setChecked(t4SAlignment->getHL05X1_3()[3]);

  uiMainWindow->doubleSpinBox_HL05X1_4_X->setValue(
      t4SAlignment->getHL05X1_4()[0] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_4_Y->setValue(
      t4SAlignment->getHL05X1_4()[1] / 10);
  uiMainWindow->doubleSpinBox_HL05X1_4_Z->setValue(
      t4SAlignment->getHL05X1_4()[2] / 10);
  uiMainWindow->checkBox_HL05X1_4->setChecked(t4SAlignment->getHL05X1_4()[3]);

  uiMainWindow->checkBox_H5L->setChecked(
      t4SAlignment->getHL05X1_1()[3] || t4SAlignment->getHL05X1_2()[3]
          || t4SAlignment->getHL05X1_3()[3] || t4SAlignment->getHL05X1_4()[3]);

  // HK
  uiMainWindow->doubleSpinBox_HK01_X->setValue(t4SAlignment->getHK01()[0] / 10);
  uiMainWindow->doubleSpinBox_HK01_Y->setValue(t4SAlignment->getHK01()[1] / 10);
  uiMainWindow->doubleSpinBox_HK01_Z->setValue(t4SAlignment->getHK01()[2] / 10);
  uiMainWindow->checkBox_HK01->setChecked(t4SAlignment->getHK01()[3]);

  uiMainWindow->doubleSpinBox_HK02_X->setValue(t4SAlignment->getHK02()[0] / 10);
  uiMainWindow->doubleSpinBox_HK02_Y->setValue(t4SAlignment->getHK02()[1] / 10);
  uiMainWindow->doubleSpinBox_HK02_Z->setValue(t4SAlignment->getHK02()[2] / 10);
  uiMainWindow->checkBox_HK02->setChecked(t4SAlignment->getHK02()[3]);

  uiMainWindow->doubleSpinBox_HK03_X->setValue(t4SAlignment->getHK03()[0] / 10);
  uiMainWindow->doubleSpinBox_HK03_Y->setValue(t4SAlignment->getHK03()[1] / 10);
  uiMainWindow->doubleSpinBox_HK03_Z->setValue(t4SAlignment->getHK03()[2] / 10);
  uiMainWindow->checkBox_HK03->setChecked(t4SAlignment->getHK03()[3]);

  uiMainWindow->checkBox_HK->setChecked(
      t4SAlignment->getHK01()[3] || t4SAlignment->getHK02()[3]
          || t4SAlignment->getHK03()[3]);

  // SandwichVeto
  uiMainWindow->doubleSpinBox_SandwichVeto_X->setValue(t4SAlignment->getSandwichVeto()[0] / 10);
  uiMainWindow->doubleSpinBox_SandwichVeto_Y->setValue(t4SAlignment->getSandwichVeto()[1] / 10);
  uiMainWindow->doubleSpinBox_SandwichVeto_Z->setValue(t4SAlignment->getSandwichVeto()[2] / 10);
  uiMainWindow->checkBox_SandwichVeto->setChecked(t4SAlignment->getSandwichVeto()[3]);

  // MultiplicityCounter
  uiMainWindow->doubleSpinBox_Multiplicity_X->setValue(t4SAlignment->getMultiplicityCounter()[0] / 10);
  uiMainWindow->doubleSpinBox_Multiplicity_Y->setValue(t4SAlignment->getMultiplicityCounter()[1] / 10);
  uiMainWindow->doubleSpinBox_Multiplicity_Z->setValue(t4SAlignment->getMultiplicityCounter()[2] / 10);
  uiMainWindow->checkBox_Multiplicity->setChecked(t4SAlignment->getMultiplicityCounter()[3]);

  // Mainz Counter
  uiMainWindow->doubleSpinBox_HH02_X->setValue(t4SAlignment->getHH02()[0] / 10);
  uiMainWindow->doubleSpinBox_HH02_Y->setValue(t4SAlignment->getHH02()[1] / 10);
  uiMainWindow->doubleSpinBox_HH02_Z->setValue(t4SAlignment->getHH02()[2] / 10);
  uiMainWindow->checkBox_HH02->setChecked(t4SAlignment->getHH02()[3]);

  uiMainWindow->doubleSpinBox_HH03_X->setValue(t4SAlignment->getHH03()[0] / 10);
  uiMainWindow->doubleSpinBox_HH03_Y->setValue(t4SAlignment->getHH03()[1] / 10);
  uiMainWindow->doubleSpinBox_HH03_Z->setValue(t4SAlignment->getHH03()[2] / 10);
  uiMainWindow->checkBox_HH03->setChecked(t4SAlignment->getHH03()[3]);

  uiMainWindow->checkBox_HH->setChecked(
      t4SAlignment->getHH02()[3] || t4SAlignment->getHH03()[3]);
}
