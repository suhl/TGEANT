#include "include/T4ITrigger.hh"

T4ITrigger::T4ITrigger(T4ISettingsFile* settingsFile_,
        Ui::MainWindow* uiMainWindow_,
        Ui::PreferencesTriggerPrimakoff* uiTriggerPrimakoff_)
{
  settingsFile = settingsFile_;

  uiMainWindow = uiMainWindow_;
  uiPrimTrig = uiTriggerPrimakoff_;


  menuPrimTrigger = new QWidget;

  uiPrimTrig->setupUi(menuPrimTrigger);

  connect(uiMainWindow->pushButton_TriggerPreferences, SIGNAL(clicked()),
      this, SLOT(slotTrigger()));

}

void T4ITrigger::slotTrigger(void)
{
  slotPrimakoff();
}

void T4ITrigger::slotPrimakoff(void)
{
  settingsFile->setInterfacePrimTrigger();
  connect(uiPrimTrig->pushButton_Accept, SIGNAL(clicked()),
      this, SLOT(seletPrimakoff()));
  connect(uiPrimTrig->pushButton_Cancel, SIGNAL(clicked()),
      this, SLOT(cancel()));
  menuPrimTrigger->show();
}

void T4ITrigger::seletPrimakoff(void)
{
  settingsFile->setStructPrimTrigger();
  menuPrimTrigger->close();
}

void T4ITrigger::cancel(void)
{
  if(!menuPrimTrigger->isHidden())
    menuPrimTrigger->close();
}
