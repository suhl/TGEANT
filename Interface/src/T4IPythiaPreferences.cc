#include "include/T4IPythiaPreferences.hh"


T4IPythiaGui::T4IPythiaGui(QWidget* parent, Qt::WindowFlags f) : QDialog(parent)
{
    setupUi(this);
    myTuning = NULL;

    std::string envTGEANT_str = std::string(getenv("TGEANT")) + "/";
    envTGEANT = QString::fromStdString(envTGEANT_str);

    //signal slot connects for the main menu
    connect(yarpButton,SIGNAL(clicked(bool)),this,SLOT(writeFile(void)));
    connect(narpButton,SIGNAL(clicked(bool)),this,SLOT(readFile(void)));
    connect(cancelButton,SIGNAL(clicked(bool)),this,SLOT(reject()));

    selRowMDME = selRowMSBD = selRowPDF = -1;

    QIcon icon;
    icon.addFile(envTGEANT + QString::fromUtf8("/resources/TGEANT-T.png"),
                 QSize(), QIcon::Normal, QIcon::On);
    buttonLogo->setIcon(icon);
    buttonLogo->setIconSize(QSize(400, 400));

    // now for each tab change buttons
    connect(buttonPDFChange, SIGNAL(clicked(bool)),this,SLOT(savePDF()));
    connect(buttonMDMEChange, SIGNAL(clicked(bool)),this,SLOT(saveMDME()));
    connect(buttonMSBDChange, SIGNAL(clicked(bool)),this,SLOT(saveMBSD()));

    // now the table oncellclick to edit* functions
    connect(pdfTable, SIGNAL(cellClicked(int,int)), this, SLOT(editPDF(int,int)));
    connect(tableMDME, SIGNAL(cellClicked(int,int)), this, SLOT(editMDME(int,int)));
    connect(tableMSBD, SIGNAL(cellClicked(int,int)), this, SLOT(editMSBD(int,int)));

    //now implement insert
    connect(buttonPDFInsert, SIGNAL(clicked(bool)),this, SLOT(insertPDF()));
    connect(buttonMDMEInsert, SIGNAL(clicked(bool)),this, SLOT(insertMDME()));
    connect(buttonMSBDInsert, SIGNAL(clicked(bool)),this, SLOT(insertMSBD()));
    

    
    //finally implement delete function
    connect(buttonPDFDelete, SIGNAL(clicked(bool)),this, SLOT(deletePDF()));
    connect(buttonMDMEDelete, SIGNAL(clicked(bool)),this, SLOT(deleteMDME()));
    connect(buttonMSBDDelete, SIGNAL(clicked(bool)),this, SLOT(deleteMSBD()));
    

}

void T4IPythiaGui::setStructMan(T4SStructManager* _structMan)
{
    myStructMan = _structMan;
    myTuning = new T4PythiaTuning(_structMan);
}

T4IPythiaGui::~T4IPythiaGui()
{
    if (myTuning != NULL)
        delete myTuning;
}


void T4IPythiaGui::readFile(void)
{
    clearStructMan();
    setStructManFromFile();
    setInterfaceFromStructMan();
}


void T4IPythiaGui::showEvent(QShowEvent* event)
{
    readFile();
    QDialog::showEvent(event);
}


void T4IPythiaGui::writeFile(void)
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Save PythiaTuning-XML"),envTGEANT+"/resources/",tr("PythiaTunings (*.xml)"));
    if (fileName == "")
      return;
    setStructManFromInterface();
    myStructMan->getExternal()->localGeneratorFile = fileName.toStdString();
    writeOutStructMan();
    done(QDialog::Accepted);
}


void T4IPythiaGui::insertMDME(void)
{
    T4SPythiaMDME newStruct;
    myStructMan->getPythia()->flagsMDME.push_back(newStruct);
    selRowMDME = tableMDME->rowCount();
    saveMDME();
}

void T4IPythiaGui::insertMSBD(void)
{
    T4SPythiaMSBD newStruct;
    myStructMan->getPythia()->flagsMSBD.push_back(newStruct);
    selRowMSBD = tableMSBD->rowCount();
    saveMBSD();
}

void T4IPythiaGui::insertPDF(void)
{
    T4SPythiaLHAPDF newStruct;
    myStructMan->getPythia()->pdfList.push_back(newStruct);
    selRowPDF = pdfTable->rowCount();
    savePDF();
}


void T4IPythiaGui::deleteMDME(void)
{
  if (selRowMDME == -1)
    return;
  if (selRowMDME > tableMDME->rowCount())
    return;
  myStructMan->getPythia()->flagsMDME.erase(myStructMan->getPythia()->flagsMDME.begin()+selRowMDME);
  
  setInterfaceFromStructMan();
}

void T4IPythiaGui::deleteMSBD(void)
{
  if (selRowMSBD == -1)
    return;
  if (selRowMSBD > tableMDME->rowCount())
    return;
  myStructMan->getPythia()->flagsMSBD.erase(myStructMan->getPythia()->flagsMSBD.begin()+selRowMSBD);
  
  setInterfaceFromStructMan();
}

void T4IPythiaGui::deletePDF(void)
{
  if (selRowPDF == -1)
    return;
  if (selRowPDF > tableMDME->rowCount())
    return;
  myStructMan->getPythia()->pdfList.erase(myStructMan->getPythia()->pdfList.begin()+selRowPDF);
  setInterfaceFromStructMan();
  
}




void T4IPythiaGui::clearStructMan(void)
{
    myStructMan->getPythia()->flagsMDME.clear();
    myStructMan->getPythia()->flagsMSBD.clear();
    myStructMan->getPythia()->pdfList.clear();
}

void T4IPythiaGui::setStructManFromFile(void)
{
    if (myTuning == NULL) {
        cout << "PLEASE SET STRUCT MANAGER FIRST! SHOULD NOT SEE THIS!" << endl;
        return;
    }
    myTuning->readXML(myStructMan->getExternal()->localGeneratorFile);
}

void T4IPythiaGui::setStructManFromInterface(void)
{
    //everything thats not handled by signals has to be done here
    myStructMan->getPythia()->target = lineTarget->text().toStdString();
    myStructMan->getPythia()->genericFlags.msel = spinMSEL->value();

}


void T4IPythiaGui::clearInterface(void)
{
    pdfTable->clearContents();
    tableMDME->clearContents();
    tableMSBD->clearContents();
}


void T4IPythiaGui::editPDF(int _row, int _cell)
{
    spinPDFNumber->setValue(myStructMan->getPythia()->pdfList.at(_row).pdfNum);
    linePDFName->setText(QString::fromStdString(myStructMan->getPythia()->pdfList.at(_row).pdfName));
    spinPDFMemset->setValue(myStructMan->getPythia()->pdfList.at(_row).pdfMemSet);

    for (int i=0; i < comboPDFSet->count(); i++) {
        comboPDFSet->setCurrentIndex(i);
        if (comboPDFSet->currentText() == QString::fromStdString(myStructMan->getPythia()->pdfList.at(_row).pdfType))
            break;
    }
    selRowPDF = _row;
}
void T4IPythiaGui::editMDME(int _row, int _cell)
{
    spinMDMENumber->setValue(myStructMan->getPythia()->flagsMDME.at(_row).number);
    spinMDMEFlag1->setValue(myStructMan->getPythia()->flagsMDME.at(_row).flag1);
    spinMDMEFlag2->setValue(myStructMan->getPythia()->flagsMDME.at(_row).flag2);
    selRowMDME = _row;
}
void T4IPythiaGui::editMSBD(int _row, int _cell)
{
    spinMSTPNumber->setValue(myStructMan->getPythia()->flagsMSBD.at(_row).number);
    spinMSTPSetTo->setValue(myStructMan->getPythia()->flagsMSBD.at(_row).flag1);
    comboMSTPType->setCurrentIndex(myStructMan->getPythia()->flagsMSBD.at(_row).type);
    selRowMSBD = _row;
}
void T4IPythiaGui::saveMBSD(void)
{
    if (selRowMSBD == -1)
        return;

    myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).number = spinMSTPNumber->value();
    myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).flag1 = spinMSTPSetTo->value();
    switch(comboMSTPType->currentIndex()) {
    case 0:
        myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).type=MSTP;
        break;
    case 1:
        myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).type=MSTU;
        break;
    case 2:
        myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).type=PARU;
        break;
    case 3:
        myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).type=PARP;
        break;
    case 4:
        myStructMan->getPythia()->flagsMSBD.at(selRowMSBD).type=CKIN;
        break;
    default:
        T4SMessenger::getInstance()->printMessage(T4SWarning,__LINE__,__FILE__,"Failed converting pythia-flagtype to enum! Please choose valid enum int!");
    };
    setInterfaceFromStructMan();
}
void T4IPythiaGui::savePDF(void)
{
    if (selRowPDF == -1)
        return;

    myStructMan->getPythia()->pdfList.at(selRowPDF).pdfNum =spinPDFNumber->value();
    myStructMan->getPythia()->pdfList.at(selRowPDF).pdfName = linePDFName->text().toStdString();
    myStructMan->getPythia()->pdfList.at(selRowPDF).pdfMemSet = spinPDFMemset->value();
    myStructMan->getPythia()->pdfList.at(selRowPDF).pdfType= comboPDFSet->currentText().toStdString();

    setInterfaceFromStructMan();
}
void T4IPythiaGui::saveMDME(void)
{
    if (selRowMDME == -1)
        return;
    myStructMan->getPythia()->flagsMDME.at(selRowMDME).number = spinMDMENumber->value();
    myStructMan->getPythia()->flagsMDME.at(selRowMDME).flag1 = spinMDMEFlag1->value();
    myStructMan->getPythia()->flagsMDME.at(selRowMDME).flag2 = spinMDMEFlag2->value();
    setInterfaceFromStructMan();
}






void T4IPythiaGui::writeOutStructMan(void)
{
    if (myTuning == NULL) {
        cout << "PLEASE SET STRUCT MANAGER FIRST! SHOULD NOT SEE THIS!" << endl;
        return;
    }
    myTuning->writeXML(myStructMan->getExternal()->localGeneratorFile);

}


void T4IPythiaGui::setInterfaceFromStructMan(void)
{

    clearInterface();
    T4SPythia* myPythia = myStructMan->getPythia();
    //main tab
    spinMSEL->setValue(myPythia->genericFlags.msel);
    lineTarget->setText(QString::fromStdString(myPythia->target));


    //PDF-Tab
    pdfTable->setRowCount(myPythia->pdfList.size());
    for (unsigned int i = 0; i < myPythia->pdfList.size(); i++) {
        pdfTable->setItem(i,0,new QTableWidgetItem(QString::number(myPythia->pdfList.at(i).pdfNum)));
        pdfTable->setItem(i,1,new QTableWidgetItem(QString::fromStdString(myPythia->pdfList.at(i).pdfName)));
        pdfTable->setItem(i,2,new QTableWidgetItem(QString::fromStdString(myPythia->pdfList.at(i).pdfType)));
        pdfTable->setItem(i,3,new QTableWidgetItem(QString::number(myPythia->pdfList.at(i).pdfMemSet)));
	
	Qt::ItemFlags flags = pdfTable->item(i,0)->flags();
	flags |= Qt::ItemIsSelectable;
	flags &= ~Qt::ItemIsEditable;
	pdfTable->item(i,0)->setFlags(flags);
	pdfTable->item(i,1)->setFlags(flags);
	pdfTable->item(i,2)->setFlags(flags);
	pdfTable->item(i,3)->setFlags(flags);
	
	
    }



    //MDME-Tab
    tableMDME->setRowCount(myPythia->flagsMDME.size());
    for (unsigned int i = 0; i < myPythia->flagsMDME.size(); i++) {
        tableMDME->setItem(i,0,new QTableWidgetItem(QString::number(myPythia->flagsMDME.at(i).number)));
        tableMDME->setItem(i,1,new QTableWidgetItem(QString::number(myPythia->flagsMDME.at(i).flag1)));
        tableMDME->setItem(i,2,new QTableWidgetItem(QString::number(myPythia->flagsMDME.at(i).flag2)));
	
	Qt::ItemFlags flags = tableMDME->item(i,0)->flags();
	flags |= Qt::ItemIsSelectable;
	flags &= ~Qt::ItemIsEditable;
	tableMDME->item(i,0)->setFlags(flags);
	tableMDME->item(i,1)->setFlags(flags);
	tableMDME->item(i,2)->setFlags(flags);
    }


    //MSBD-Tab
    tableMSBD->setRowCount(myPythia->flagsMSBD.size());
    for (unsigned int i = 0; i < myPythia->flagsMSBD.size(); i++) {
        tableMSBD->setItem(i,0,new QTableWidgetItem(QString::number(myPythia->flagsMSBD.at(i).type)));
        tableMSBD->setItem(i,1,new QTableWidgetItem(QString::number(myPythia->flagsMSBD.at(i).number)));
        tableMSBD->setItem(i,2,new QTableWidgetItem(QString::number(myPythia->flagsMSBD.at(i).flag1)));
	Qt::ItemFlags flags = tableMSBD->item(i,0)->flags();
	flags |= Qt::ItemIsSelectable;
	flags &= ~Qt::ItemIsEditable;
	tableMSBD->item(i,0)->setFlags(flags);
	tableMSBD->item(i,1)->setFlags(flags);
	tableMSBD->item(i,2)->setFlags(flags);
    }
}
