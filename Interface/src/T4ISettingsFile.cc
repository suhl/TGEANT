#include "include/T4ISettingsFile.hh"

T4ISettingsFile::T4ISettingsFile(Ui::MainWindow* uiMainWindow_,
    Ui::PreferencesBeamUser* uiUser_, Ui::PreferencesBeamCosmics* uiCosmics_,
    Ui::PreferencesBeamEcalCalib* uiEcalCalib_,
    Ui::PreferencesGeneratorHEPGEN* uiHEPGen_,
    Ui::PreferencesGeneratorPrimGen* uiPrimGen_,
    Ui::PreferencesPhysicsList* uiPhysicsList_,
    Ui::PreferencesTriggerPrimakoff* uiPrimTrig_)
{
  uiMainWindow = uiMainWindow_;
  uiUser = uiUser_;
  uiCosmics = uiCosmics_;
  uiEcalCalib = uiEcalCalib_;
  uiHEPGen = uiHEPGen_;
  uiPrimGen = uiPrimGen_;
  uiPhysicsList = uiPhysicsList_;
  uiPrimTrig = uiPrimTrig_;

  structM = new T4SStructManager();
  t4SSettingsFileXML = new T4SSettingsFileXML(structM);

  t4SAlignment = NULL;
}

T4ISettingsFile::~T4ISettingsFile(void)
{
  delete structM;
  if (t4SAlignment != NULL)
    delete t4SAlignment;
}

void T4ISettingsFile::saveFile(std::string savePath)
{
  setStructManager();
  fileName = savePath;
  t4SSettingsFileXML->save(fileName);
}

void T4ISettingsFile::saveFile(void)
{
  setStructManager();
  fileName = std::string(getenv("TGEANT")) + "/resources/settings";
  cerr << fileName << endl;
  t4SSettingsFileXML->save(fileName);
}

void T4ISettingsFile::loadFile(std::string loadPath)
{
  std::cout << "T4ISettingsFile::loadFile called." << std::endl;
  if (loadPath.length() < 4)
    return;

  structM->clearStructManager();
  t4SSettingsFileXML->load_unchecked(loadPath);

  setInterface();
}

void T4ISettingsFile::loadDetectorsDat(void)
{
  std::cout << "T4ISettingsFile::loadDetectorsDat called. Alignment of selected detectors.dat file will be loaded." << std::endl;

  string filePath = getString(uiMainWindow->lineEdit_PathToDetectorsDatFile->text());

  t4SAlignment = new T4SAlignment();
  t4SAlignment->load(filePath);
  t4SAlignment->readAlignment();

  setDetDatAlignment();
}

std::string T4ISettingsFile::getBeamPlugin(void)
{
  if (uiMainWindow->radioButton_BeamOnly->isChecked())
    return "BeamOnly";
  else if (uiMainWindow->radioButton_VisualizationMode->isChecked())
    return "VisualizationMode";
  else if (uiMainWindow->radioButton_SelectionMode->isChecked())
    return getString(
        uiMainWindow->comboBox_BeamMode->currentText());
  else if (uiMainWindow->radioButton_Pythia6->isChecked())
    return "PYTHIA";
  else if (uiMainWindow->radioButton_Pythia8->isChecked())
    return "PYTHIA8";
  else if (uiMainWindow->radioButton_Lepto->isChecked())
    return "LEPTO";
  else if (uiMainWindow->radioButton_HEPGen->isChecked())
    return "HEPGEN";
  else if (uiMainWindow->radioButton_Primakoff->isChecked())
    return "Primakoff";
  else if (uiMainWindow->radioButton_ascii->isChecked())
    return "ascii";
  else
    return "error";
}

std::string T4ISettingsFile::getString(QString input)
{
  if (input.isEmpty()) {
    return "empty";
  } else {
    return input.toStdString();
  }
}

void T4ISettingsFile::deactivateVisualization(void)
{
  structM->getGeneral()->useVisualization = false;
}
