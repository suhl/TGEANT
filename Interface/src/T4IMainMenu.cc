#include "include/T4IMainMenu.hh"

T4IMainMenu::T4IMainMenu(T4ISettingsFile* settingsFile_,
    Ui::MainWindow* uiMainWindow_, Ui::PreferencesPhysicsList* uiPhysicsList_)
{
  uiMainWindow = uiMainWindow_;
  uiMainWindow->setupUi(this);
  settingsFile = settingsFile_;

  uiPhysicsList = uiPhysicsList_;
  menuPhysicsList = new QWidget;
  uiPhysicsList->setupUi(menuPhysicsList);

  
  efficWindow = new T4IEfficiencies;
  
  
  
  std::string envTGEANT_str = std::string(getenv("TGEANT")) + "/";
  envTGEANT = QString::fromStdString(envTGEANT_str);

  // TGEANT Logo
  QIcon icon,icon2;
  icon.addFile(envTGEANT + QString::fromUtf8("/resources/TGEANT-T.png"),
      QSize(), QIcon::Normal, QIcon::On);
  icon2.addFile(envTGEANT + QString::fromUtf8("/resources/TGEANT-Logo.png"),
      QSize(), QIcon::Normal, QIcon::On);
  
  uiMainWindow->pushButton_TGEANT->setIcon(icon2);
  uiMainWindow->pushButton_TGEANT->setIconSize(QSize(600, 100));
  uiMainWindow->pushButton_start->setIcon(icon);
  uiMainWindow->pushButton_start->setIconSize(QSize(100, 100));

  // main
  connect(uiMainWindow->pushButton_save, SIGNAL(clicked()), this,
      SLOT(buttonSave()));
  connect(uiMainWindow->pushButton_load, SIGNAL(clicked()), this,
      SLOT(buttonLoad()));
  connect(uiMainWindow->pushButton_loadLast, SIGNAL(clicked()), this,
      SLOT(buttonLoadLast()));
  connect(uiMainWindow->pushButton_close, SIGNAL(clicked()), this,
      SLOT(buttonClose()));

  // file dialog
  connect(uiMainWindow->pushButton_PathToOutputData, SIGNAL(clicked()), this,
      SLOT(slotSelectOutputPath()));
  connect(uiMainWindow->pushButton_PathToDetectorsDatFile, SIGNAL(clicked()),
      this, SLOT(slotSelectDetectorsDat()));
  connect(uiMainWindow->pushButton_PathToSM1, SIGNAL(clicked()), this,
      SLOT(slotSelectSM1()));
  connect(uiMainWindow->pushButton_PathToSM2, SIGNAL(clicked()), this,
      SLOT(slotSelectSM2()));
  connect(uiMainWindow->pushButton_PathToTargetMag, SIGNAL(clicked()), this,
      SLOT(slotSelectTargetField()));
  connect(uiMainWindow->pushButton_PathToRecoilCalibrationFile, SIGNAL(clicked()), this,
      SLOT(slotSelectRecoilCalibrationFile()));
  connect(uiMainWindow->pushButton_CMTX, SIGNAL(clicked()), this,
      SLOT(slotSelectCaloCalib()));

  connect(uiMainWindow->mtxInnerXSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxInnerX()));
  connect(uiMainWindow->mtxLadderXSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxLadderX()));
  connect(uiMainWindow->mtxOuterYSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxOuterY()));
  connect(uiMainWindow->mtxLASTSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxLAST()));
  connect(uiMainWindow->mtxMiddleXSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxMiddleX()));
  connect(uiMainWindow->mtxMiddleYSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectmtxMiddleY()));
  connect(uiMainWindow->libHEPGen_pi_select, SIGNAL(clicked()), this,
      SLOT(slotSelectlibHEPGen_Pi()));
  connect(uiMainWindow->libHEPGen_pi_transv_select, SIGNAL(clicked()), this,
      SLOT(slotSelectlibHEPGen_Pi_transv()));
  connect(uiMainWindow->libHEPGen_rho_select, SIGNAL(clicked()), this,
      SLOT(slotSelectlibHEPGen_Rho()));
  connect(uiMainWindow->visualizationMacroSelect, SIGNAL(clicked()), this,
      SLOT(slotSelectVisualizationMacro()));
  connect(uiMainWindow->detectorEfficiencySelect, SIGNAL(clicked()), this,
      SLOT(slotSelectDetectorEfficiency()));

  // general
  connect(uiMainWindow->checkBox_RestoreFileFromSeed, SIGNAL(stateChanged(int)),
      this, SLOT(slotRestoreFileFromSeed()));
  connect(uiMainWindow->comboBox_triggerPlugin, SIGNAL(activated(int)),
      this, SLOT(slotSelectTriggerPlugin(int)));

  // detectors
  connect(uiMainWindow->pushButton_select_all, SIGNAL(clicked()), this,
      SLOT(buttonSelectAll()));
  connect(uiMainWindow->pushButton_select_none, SIGNAL(clicked()), this,
      SLOT(buttonSelectNone()));
  connect(uiMainWindow->pushButton_LoadDetectorsDatAlignement,
      SIGNAL(clicked()), this, SLOT(buttonLoadDetectorsDat()));
  connectGeometries();

  connect(uiMainWindow_->pushButton_Effic,SIGNAL(clicked(bool)), this, SLOT(slotShowEfficGui()));
  
  // sm2
  connect(uiMainWindow->comboBox_SM2Field,
      SIGNAL(currentIndexChanged(int)), this, SLOT(buttonSM2Field()));

  // physicsList
  settingsFile->setStructSampling();
  connect(uiMainWindow->comboBox_PhysicsList_Details,
      SIGNAL(currentIndexChanged(int)), this, SLOT(buttonPhysicsList()));
  connect(uiMainWindow->pushButton_PhysicsListPreferences, SIGNAL(clicked()),
      this, SLOT(slotPhysicsList()));
  connect(uiMainWindow->checkBox_noSecondaries, SIGNAL(clicked()), this,
      SLOT(cutsPhysicsList()));
}

T4IMainMenu::~T4IMainMenu(void)
{
  delete settingsFile;
}

void T4IMainMenu::slotShowEfficGui(void)
{
  bool wasDefault = false;
  settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency =
      uiMainWindow->detectorEfficiencyFile->text().toStdString();
  string oldString = settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency;
  
  if (oldString
      == "default" || oldString
      == "default-dvcs_2012"){
    settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency =
        std::string(getenv("TGEANT")) + "/resources/dvcs_2012/effic.xml";
    wasDefault=true;
  }
  else if ( oldString == "default-dy_2014" ){
    settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency =
        std::string(getenv("TGEANT")) + "/resources/dy_2014/effic.xml";
    wasDefault=true;
  }
  efficWindow->setStructMan(settingsFile->getT4SStructManager());
  
  int retVal = efficWindow->exec();
  if (retVal == QDialog::Accepted)
    uiMainWindow->detectorEfficiencyFile->setText(QString::fromStdString(settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency));
  else if (retVal != QDialog::Accepted && wasDefault ==true)
    settingsFile->getT4SStructManager()->getExternal()->detectorEfficiency
      = oldString;

}


void T4IMainMenu::connectGeometries(void)
{
  geometries();
  connect(uiMainWindow->checkBox_LH2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_LH2, SIGNAL(stateChanged(int)), this,
      SLOT(slotTarget()));
  connect(uiMainWindow->checkBox_LH2Hadron, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_LH2Hadron, SIGNAL(stateChanged(int)), this,
      SLOT(slotTarget()));
  connect(uiMainWindow->checkBox_TargetPrimakoff2012, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_TargetPrimakoff2012, SIGNAL(stateChanged(int)), this,
      SLOT(slotTarget()));
  connect(uiMainWindow->checkBox_DYTarget, SIGNAL(stateChanged(int)),
      this, SLOT(slotTarget()));
  connect(uiMainWindow->checkBox_DYTarget, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_PolTarget, SIGNAL(stateChanged(int)),
      this, SLOT(slotTarget()));
  connect(uiMainWindow->checkBox_PolTarget, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_DYAbsorber, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_ECAL0, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_ECAL1, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_ECAL2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_DC00, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_DC01, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_DC04, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_DC05, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_HCAL1, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_HCAL2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MW1, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MW2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MF3, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_ST02, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_ST03, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_ST05, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MM01, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MM02, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MM03, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MP00, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_MP01, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_GEM, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_PGEM, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_PA, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_PB, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_PS, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_SI, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_FI, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_CAMERA, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_CAMERA, SIGNAL(stateChanged(int)), this,
      SLOT(slotRecoil()));
  connect(uiMainWindow->checkBox_RPD, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_RPD, SIGNAL(stateChanged(int)), this,
      SLOT(slotRecoil()));
  connect(uiMainWindow->checkBox_recoil_RingA, SIGNAL(stateChanged(int)), this,
      SLOT(slotRecoil()));
  connect(uiMainWindow->checkBox_recoil_RingB, SIGNAL(stateChanged(int)), this,
      SLOT(slotRecoil()));
  connect(uiMainWindow->checkBox_W45, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_RichWall, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_DW01X, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW01Y, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW02X, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW02Y, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW03V, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW03Y, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW04Y, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW04U, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW05X, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW05V, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW06U, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));
  connect(uiMainWindow->checkBox_DW06X, SIGNAL(stateChanged(int)), this,
      SLOT(slotW45()));

  connect(uiMainWindow->checkBox_Rich, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_SM1, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_SM2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));

  connect(uiMainWindow->checkBox_PolGPD, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_TestSetupDummy, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_TestSetupTestCal, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_PolGPD_SiRPD, SIGNAL(stateChanged(int)),
      this, SLOT(geometries()));
  connect(uiMainWindow->checkBox_SCIFI_TEST, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));

  connect(uiMainWindow->checkBox_SCIFI_TEST, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));

  connect(uiMainWindow->checkBox_H1, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H2, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_Veto, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));

  connect(uiMainWindow->checkBox_H3O, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H4O, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H4I, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H4M, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H4L, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H5I, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H5M, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_H5L, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_HK, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_SandwichVeto, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_Multiplicity, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));
  connect(uiMainWindow->checkBox_HH, SIGNAL(stateChanged(int)), this,
      SLOT(geometries()));

  connect(uiMainWindow->checkBox_HO03, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HI04X1_u, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HI04X1_d, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04Y1_u1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04Y1_u2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04Y1_d1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04Y1_d2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04X1_u, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM04X1_d, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL04X1_1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL04X1_2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL04X1_3, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL04X1_4, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HO04, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HI05X1_u, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HI05X1_d, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05Y1_u1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05Y1_u2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05Y1_d1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05Y1_d2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05X1_u, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HM05X1_d, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL05X1_1, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL05X1_2, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL05X1_3, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HL05X1_4, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));

  connect(uiMainWindow->checkBox_HK01, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HK02, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HK03, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));

  connect(uiMainWindow->checkBox_HH02, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
  connect(uiMainWindow->checkBox_HH03, SIGNAL(stateChanged(int)), this,
      SLOT(slotHodoscopes()));
}

void T4IMainMenu::buttonClose(void)
{
  std::cout << "TGEANT Graphical User Interface closed." << std::endl;
  this->close();
}

void T4IMainMenu::buttonSave(void)
{
  QFileDialog dialog;
  dialog.setDirectory(QString::fromStdString(std::string(getenv("PWD"))));
  QString fileName = dialog.getSaveFileName(this, tr("Open File"), "",
      tr("Configuration files (*.xml *.conf)"));

  settingsFile->saveFile(fileName.toStdString());
}

void T4IMainMenu::buttonLoad(void)
{
  QString settingsToLoad = uiMainWindow->comboBox_loadSettings->currentText();
  string fileToLoad;

  if (settingsToLoad == "custom") {
    QFileDialog dialog;
    dialog.setDirectory(QString::fromStdString(std::string(getenv("PWD"))));
    dialog.setConfirmOverwrite(true);
    QString fileName = dialog.getOpenFileName(this, tr("Open File"), "",
        tr("Configuration files (*.xml *.conf)"));
    fileToLoad = fileName.toStdString();
  }

  else if (settingsToLoad == "2008-Hadron")
    fileToLoad = "$TGEANT/resources/hadron_2008/settings_hadron2008.xml";
  else if (settingsToLoad == "2011-SIDIS-plus")
    fileToLoad = "$TGEANT/resources/sidis_2011/mwplus/settings_mw_plus.xml";
  else if (settingsToLoad == "2011-SIDIS-minus")
    fileToLoad = "$TGEANT/resources/sidis_2011/mwminus/settings_mw_minus.xml";
  else if (settingsToLoad == "2012-DVCS")
    fileToLoad = "$TGEANT/resources/dvcs_2012/settings_dvcs2012.xml";
  else if (settingsToLoad == "2012-Primakoff")
    fileToLoad = "$TGEANT/resources/primakoff_2012/settings_primakoff2012.xml";
  else if (settingsToLoad == "2014-DY")
    fileToLoad = "$TGEANT/resources/dy_2014/settings_dy2014.xml";
  else if (settingsToLoad == "2015-DY")
    fileToLoad = "$TGEANT/resources/dy_2015/settings_dy2015.xml";
  else if (settingsToLoad == "2016-DVCS")
    fileToLoad = "$TGEANT/resources/dvcs_2016/settings_dvcs2016.xml";

  replacePathEnvTGEANT(fileToLoad);
  settingsFile->loadFile(fileToLoad);
}

void T4IMainMenu::buttonLoadLast(void)
{
  settingsFile->loadFile((envTGEANT + "/resources/settings.xml").toStdString());
}

void T4IMainMenu::buttonLoadDetectorsDat(void)
{
  settingsFile->loadDetectorsDat();
}

void T4IMainMenu::geometries(void)
{
  uiMainWindow->groupBox_ECAL0->setEnabled(
      uiMainWindow->checkBox_ECAL0->isChecked());
  uiMainWindow->groupBox_ECAL1->setEnabled(
      uiMainWindow->checkBox_ECAL1->isChecked());
  uiMainWindow->groupBox_ECAL2->setEnabled(
      uiMainWindow->checkBox_ECAL2->isChecked());
  uiMainWindow->groupBox_HCAL1->setEnabled(
      uiMainWindow->checkBox_HCAL1->isChecked());
  uiMainWindow->groupBox_HCAL2->setEnabled(
      uiMainWindow->checkBox_HCAL2->isChecked());

  uiMainWindow->groupBox_DC00->setEnabled(
      uiMainWindow->checkBox_DC00->isChecked());
  uiMainWindow->groupBox_DC01->setEnabled(
      uiMainWindow->checkBox_DC01->isChecked());
  uiMainWindow->groupBox_DC04->setEnabled(
      uiMainWindow->checkBox_DC04->isChecked());
  uiMainWindow->groupBox_DC05->setEnabled(
      uiMainWindow->checkBox_DC05->isChecked());

  uiMainWindow->groupBox_SM1->setEnabled(
      uiMainWindow->checkBox_SM1->isChecked());
  uiMainWindow->groupBox_SM2->setEnabled(
      uiMainWindow->checkBox_SM2->isChecked());

  uiMainWindow->groupBox_H1->setEnabled(uiMainWindow->checkBox_H1->isChecked());
  uiMainWindow->groupBox_H2->setEnabled(uiMainWindow->checkBox_H2->isChecked());

  uiMainWindow->groupBox_Rich->setEnabled(
      uiMainWindow->checkBox_Rich->isChecked());

  uiMainWindow->groupBox_MW1->setEnabled(
      uiMainWindow->checkBox_MW1->isChecked());
  uiMainWindow->groupBox_MW2->setEnabled(
      uiMainWindow->checkBox_MW2->isChecked());
  uiMainWindow->groupBox_MF3->setEnabled(
      uiMainWindow->checkBox_MF3->isChecked());
  uiMainWindow->groupBox_MF3_Block->setEnabled(
      uiMainWindow->checkBox_MF3->isChecked());

  uiMainWindow->groupBox_recoil->setEnabled(
      uiMainWindow->checkBox_CAMERA->isChecked()
          || uiMainWindow->checkBox_RPD->isChecked());

  uiMainWindow->groupBox_ST02->setEnabled(
      uiMainWindow->checkBox_ST02->isChecked());
  uiMainWindow->groupBox_ST03->setEnabled(
      uiMainWindow->checkBox_ST03->isChecked());
  uiMainWindow->groupBox_ST05->setEnabled(
      uiMainWindow->checkBox_ST05->isChecked());

  uiMainWindow->groupBox_MM01X->setEnabled(
      uiMainWindow->checkBox_MM01->isChecked());
  uiMainWindow->groupBox_MM01Y->setEnabled(
      uiMainWindow->checkBox_MM01->isChecked());
  uiMainWindow->groupBox_MM01U->setEnabled(
      uiMainWindow->checkBox_MM01->isChecked());
  uiMainWindow->groupBox_MM01V->setEnabled(
      uiMainWindow->checkBox_MM01->isChecked());

  uiMainWindow->groupBox_MM02X->setEnabled(
      uiMainWindow->checkBox_MM02->isChecked());
  uiMainWindow->groupBox_MM02Y->setEnabled(
      uiMainWindow->checkBox_MM02->isChecked());
  uiMainWindow->groupBox_MM02U->setEnabled(
      uiMainWindow->checkBox_MM02->isChecked());
  uiMainWindow->groupBox_MM02V->setEnabled(
      uiMainWindow->checkBox_MM02->isChecked());

  uiMainWindow->groupBox_MM03X->setEnabled(
      uiMainWindow->checkBox_MM03->isChecked());
  uiMainWindow->groupBox_MM03Y->setEnabled(
      uiMainWindow->checkBox_MM03->isChecked());
  uiMainWindow->groupBox_MM03U->setEnabled(
      uiMainWindow->checkBox_MM03->isChecked());
  uiMainWindow->groupBox_MM03V->setEnabled(
      uiMainWindow->checkBox_MM03->isChecked());

  uiMainWindow->groupBox_MP00X->setEnabled(
      uiMainWindow->checkBox_MP00->isChecked());

  uiMainWindow->groupBox_MP01X->setEnabled(
      uiMainWindow->checkBox_MP01->isChecked());
  uiMainWindow->groupBox_MP01Y->setEnabled(
      uiMainWindow->checkBox_MP01->isChecked());
  uiMainWindow->groupBox_MP01U->setEnabled(
      uiMainWindow->checkBox_MP01->isChecked());
  uiMainWindow->groupBox_MP01V->setEnabled(
      uiMainWindow->checkBox_MP01->isChecked());

  bool gemOn = uiMainWindow->checkBox_GEM->isChecked();
  uiMainWindow->groupBox_GM01X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM01U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM02X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM02U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM03X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM03U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM04X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM04U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM05X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM05U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM06X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM06U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM07X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM07U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM08X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM08U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM09X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM09U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM10X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM10U->setEnabled(gemOn);
  uiMainWindow->groupBox_GM11X->setEnabled(gemOn);
  uiMainWindow->groupBox_GM11U->setEnabled(gemOn);

  bool pgemOn = uiMainWindow->checkBox_PGEM->isChecked();
  uiMainWindow->groupBox_GP01X->setEnabled(pgemOn);
  uiMainWindow->groupBox_GP01U->setEnabled(pgemOn);
  uiMainWindow->groupBox_GP02X->setEnabled(pgemOn);
  uiMainWindow->groupBox_GP02U->setEnabled(pgemOn);
  uiMainWindow->groupBox_GP03X->setEnabled(pgemOn);
  uiMainWindow->groupBox_GP03U->setEnabled(pgemOn);

  bool paOn = uiMainWindow->checkBox_PA->isChecked();
  uiMainWindow->groupBox_PA01X->setEnabled(paOn);
  uiMainWindow->groupBox_PA02X->setEnabled(paOn);
  uiMainWindow->groupBox_PA03X->setEnabled(paOn);
  uiMainWindow->groupBox_PA04X->setEnabled(paOn);
  uiMainWindow->groupBox_PA05X->setEnabled(paOn);
  uiMainWindow->groupBox_PA06X->setEnabled(paOn);
  uiMainWindow->groupBox_PA11X->setEnabled(paOn);

  bool pbOn = uiMainWindow->checkBox_PB->isChecked();
  uiMainWindow->groupBox_PB01X->setEnabled(pbOn);
  uiMainWindow->groupBox_PB03X->setEnabled(pbOn);
  uiMainWindow->groupBox_PB05X->setEnabled(pbOn);
  uiMainWindow->groupBox_PB02V->setEnabled(pbOn);
  uiMainWindow->groupBox_PB04V->setEnabled(pbOn);
  uiMainWindow->groupBox_PB06V->setEnabled(pbOn);

  uiMainWindow->groupBox_PS01X->setEnabled(
      uiMainWindow->checkBox_PS->isChecked());

  bool siOn = uiMainWindow->checkBox_SI->isChecked();
  uiMainWindow->groupBox_SI01X->setEnabled(siOn);
  uiMainWindow->groupBox_SI01U->setEnabled(siOn);
  uiMainWindow->groupBox_SI02X->setEnabled(siOn);
  uiMainWindow->groupBox_SI02U->setEnabled(siOn);
  uiMainWindow->groupBox_SI03X->setEnabled(siOn);
  uiMainWindow->groupBox_SI03U->setEnabled(siOn);
  uiMainWindow->groupBox_SI04X->setEnabled(siOn);
  uiMainWindow->groupBox_SI04U->setEnabled(siOn);
  uiMainWindow->groupBox_SI05X->setEnabled(siOn);
  uiMainWindow->groupBox_SI05U->setEnabled(siOn);

  bool fiOn = uiMainWindow->checkBox_FI->isChecked();
  uiMainWindow->groupBox_FI01X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI15X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI02X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI03X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI35X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI04X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI05X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI55U->setEnabled(fiOn);
  uiMainWindow->groupBox_FI06X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI07X->setEnabled(fiOn);
  uiMainWindow->groupBox_FI08X->setEnabled(fiOn);

  uiMainWindow->groupBox_Target->setEnabled(
      uiMainWindow->checkBox_LH2->isChecked()
	  || uiMainWindow->checkBox_LH2Hadron->isChecked()
          || uiMainWindow->checkBox_DYTarget->isChecked()
          || uiMainWindow->checkBox_TargetPrimakoff2012->isChecked()
          || uiMainWindow->checkBox_PolTarget->isChecked());

  uiMainWindow->groupBox_DYAbsorber->setEnabled(
      uiMainWindow->checkBox_DYAbsorber->isChecked());

  uiMainWindow->groupBox_Veto_unkn->setEnabled(
      (uiMainWindow->checkBox_Veto->isChecked()));
  uiMainWindow->groupBox_VI01P1->setEnabled(
      (uiMainWindow->checkBox_Veto->isChecked()));
  uiMainWindow->groupBox_VO01X1->setEnabled(
      (uiMainWindow->checkBox_Veto->isChecked()));
  uiMainWindow->groupBox_VI02X1->setEnabled(
      (uiMainWindow->checkBox_Veto->isChecked()));

  bool useW45 = uiMainWindow->checkBox_W45->isChecked();
  uiMainWindow->groupBox_DW01X->setEnabled(useW45);
  uiMainWindow->groupBox_DW01Y->setEnabled(useW45);
  uiMainWindow->groupBox_DW02X->setEnabled(useW45);
  uiMainWindow->groupBox_DW02Y->setEnabled(useW45);
  uiMainWindow->groupBox_DW03V->setEnabled(useW45);
  uiMainWindow->groupBox_DW03Y->setEnabled(useW45);
  uiMainWindow->groupBox_DW04Y->setEnabled(useW45);
  uiMainWindow->groupBox_DW04U->setEnabled(useW45);
  uiMainWindow->groupBox_DW05X->setEnabled(useW45);
  uiMainWindow->groupBox_DW05V->setEnabled(useW45);
  uiMainWindow->groupBox_DW06U->setEnabled(useW45);
  uiMainWindow->groupBox_DW06X->setEnabled(useW45);

  bool useRichWall = uiMainWindow->checkBox_RichWall->isChecked();
  uiMainWindow->groupBox_DR01X1->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR01X2->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR01Y1->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR01Y2->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR02X1->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR02X2->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR02Y1->setEnabled(useRichWall);
  uiMainWindow->groupBox_DR02Y2->setEnabled(useRichWall);

  uiMainWindow->groupBox_PolGPD->setEnabled(
      uiMainWindow->checkBox_PolGPD->isChecked());
  uiMainWindow->groupBox_DUMMY->setEnabled(
      uiMainWindow->checkBox_TestSetupDummy->isChecked());
  uiMainWindow->groupBox_TESTCAL->setEnabled(
      uiMainWindow->checkBox_TestSetupTestCal->isChecked());
  uiMainWindow->groupBox_PolGPD_SiRPD->setEnabled(
      uiMainWindow->checkBox_PolGPD_SiRPD->isChecked());
  uiMainWindow->groupBox_SCIFI_TEST->setEnabled(
      uiMainWindow->checkBox_SCIFI_TEST->isChecked());

  // disable false settings...
  uiMainWindow->checkBox_recoil_RingA->setEnabled(
      uiMainWindow->checkBox_CAMERA->isChecked()
          || uiMainWindow->checkBox_RPD->isChecked());

  uiMainWindow->checkBox_recoil_RingA_Mech->setEnabled(
      uiMainWindow->checkBox_recoil_RingA->isChecked());
  uiMainWindow->checkBox_recoil_RingA_Optical->setEnabled(
      uiMainWindow->checkBox_recoil_RingA->isChecked());
  uiMainWindow->checkBox_recoil_RingA_Slab->setEnabled(
      uiMainWindow->checkBox_recoil_RingA->isChecked());
  uiMainWindow->checkBox_recoil_RingA_ConicalCryostat->setEnabled(
      uiMainWindow->checkBox_recoil_RingA->isChecked()
          && uiMainWindow->checkBox_RPD->isChecked());

  uiMainWindow->checkBox_recoil_RingB->setEnabled(
      uiMainWindow->checkBox_CAMERA->isChecked()
          || uiMainWindow->checkBox_RPD->isChecked());

  uiMainWindow->checkBox_recoil_RingB_Mech->setEnabled(
      uiMainWindow->checkBox_recoil_RingB->isChecked());
  uiMainWindow->checkBox_recoil_RingB_Optical->setEnabled(
      uiMainWindow->checkBox_recoil_RingB->isChecked());
  uiMainWindow->checkBox_recoil_RingB_Slab->setEnabled(
      uiMainWindow->checkBox_recoil_RingB->isChecked());


  uiMainWindow->groupBox_HO03->setEnabled(
      uiMainWindow->checkBox_H3O->isChecked());

  uiMainWindow->groupBox_HO04->setEnabled(
      uiMainWindow->checkBox_H4O->isChecked());

  uiMainWindow->groupBox_HI04X1_u->setEnabled(
      uiMainWindow->checkBox_H4I->isChecked());
  uiMainWindow->groupBox_HI04X1_d->setEnabled(
      uiMainWindow->checkBox_H4I->isChecked());

  uiMainWindow->groupBox_HM04Y1_u1->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());
  uiMainWindow->groupBox_HM04Y1_u2->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());
  uiMainWindow->groupBox_HM04Y1_d1->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());
  uiMainWindow->groupBox_HM04Y1_d2->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());
  uiMainWindow->groupBox_HM04X1_u->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());
  uiMainWindow->groupBox_HM04X1_d->setEnabled(
      uiMainWindow->checkBox_H4M->isChecked());

  uiMainWindow->groupBox_HL04X1_1->setEnabled(
      uiMainWindow->checkBox_H4L->isChecked());
  uiMainWindow->groupBox_HL04X1_2->setEnabled(
      uiMainWindow->checkBox_H4L->isChecked());
  uiMainWindow->groupBox_HL04X1_3->setEnabled(
      uiMainWindow->checkBox_H4L->isChecked());
  uiMainWindow->groupBox_HL04X1_4->setEnabled(
      uiMainWindow->checkBox_H4L->isChecked());

  uiMainWindow->groupBox_HI05X1_u->setEnabled(
      uiMainWindow->checkBox_H5I->isChecked());
  uiMainWindow->groupBox_HI05X1_d->setEnabled(
      uiMainWindow->checkBox_H5I->isChecked());

  uiMainWindow->groupBox_HM05Y1_u1->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());
  uiMainWindow->groupBox_HM05Y1_u2->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());
  uiMainWindow->groupBox_HM05Y1_d1->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());
  uiMainWindow->groupBox_HM05Y1_d2->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());
  uiMainWindow->groupBox_HM05X1_u->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());
  uiMainWindow->groupBox_HM05X1_d->setEnabled(
      uiMainWindow->checkBox_H5M->isChecked());

  uiMainWindow->groupBox_HL05X1_1->setEnabled(
      uiMainWindow->checkBox_H5L->isChecked());
  uiMainWindow->groupBox_HL05X1_2->setEnabled(
      uiMainWindow->checkBox_H5L->isChecked());
  uiMainWindow->groupBox_HL05X1_3->setEnabled(
      uiMainWindow->checkBox_H5L->isChecked());
  uiMainWindow->groupBox_HL05X1_4->setEnabled(
      uiMainWindow->checkBox_H5L->isChecked());

  uiMainWindow->groupBox_HK01->setEnabled(
      uiMainWindow->checkBox_HK->isChecked());
  uiMainWindow->groupBox_HK02->setEnabled(
      uiMainWindow->checkBox_HK->isChecked());
  uiMainWindow->groupBox_HK03->setEnabled(
      uiMainWindow->checkBox_HK->isChecked());

  uiMainWindow->groupBox_SandwichVeto->setEnabled(
      uiMainWindow->checkBox_SandwichVeto->isChecked());

  uiMainWindow->groupBox_Multiplicity->setEnabled(
      uiMainWindow->checkBox_Multiplicity->isChecked());

  uiMainWindow->groupBox_HH02->setEnabled(
      uiMainWindow->checkBox_HH->isChecked());
  uiMainWindow->groupBox_HH03->setEnabled(
      uiMainWindow->checkBox_HH->isChecked());
}

void T4IMainMenu::slotRestoreFileFromSeed(void)
{
  uiMainWindow->lineEdit_seed->setEnabled(
      uiMainWindow->checkBox_RestoreFileFromSeed->isChecked());
}

void T4IMainMenu::slotSelectTriggerPlugin(int idx)
{
  string name = uiMainWindow->comboBox_triggerPlugin->currentText().toStdString();

  if(name == "Primakoff2012")
    uiMainWindow->pushButton_TriggerPreferences->setEnabled(true);
  else
    uiMainWindow->pushButton_TriggerPreferences->setEnabled(false);

  int year = strToInt(name.substr(name.size()-4, 4));

  if (year == 2014) {
    uiMainWindow->mtxOuterY->setText("$TGEANT/resources/triggerMatrix/outer_y_2014.mtx");
    uiMainWindow->mtxLAST->setText("$TGEANT/resources/triggerMatrix/last_dy2014.mtx");
  } else if (year == 2015) {
    uiMainWindow->mtxOuterY->setText("$TGEANT/resources/triggerMatrix/outer_y_2015.mtx");
    uiMainWindow->mtxLAST->setText("$TGEANT/resources/triggerMatrix/last_dy2015.mtx");
  } else if (year == 2016) {
    uiMainWindow->mtxOuterY->setText("$TGEANT/resources/triggerMatrix/outer_y_2015.mtx");
    uiMainWindow->mtxLAST->setText("$TGEANT/resources/triggerMatrix/last_dy2015.mtx");
  } else {
    uiMainWindow->mtxOuterY->setText("$TGEANT/resources/triggerMatrix/outer_y.mtx");
    uiMainWindow->mtxLAST->setText("$TGEANT/resources/triggerMatrix/last_dvcs2012.xml");
  }

}

void T4IMainMenu::slotSelectDetectorsDat(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_PathToDetectorsDatFile->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec())
    uiMainWindow->lineEdit_PathToDetectorsDatFile->setText(
        dialog.selectedFiles().at(0));
}

void T4IMainMenu::slotSelectlibHEPGen_Pi(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->libHEPGen_pi0->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->libHEPGen_pi0->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectlibHEPGen_Pi_transv(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->libHEPGen_pi0_transv->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->libHEPGen_pi0_transv->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectlibHEPGen_Rho(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->libHEPGen_rho->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->libHEPGen_rho->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectVisualizationMacro(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->visualizationMacro->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->visualizationMacro->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectDetectorEfficiency(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->detectorEfficiencyFile->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->detectorEfficiencyFile->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxInnerX(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxInnerX->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxInnerX->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxLadderX(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxLadderX->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxLadderX->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxLAST(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxLAST->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxLAST->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxMiddleX(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxMiddleX->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxMiddleX->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxMiddleY(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxMiddleY->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxMiddleY->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectmtxOuterY(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->mtxOuterY->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->mtxOuterY->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectOutputPath(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_OutputPath->text());
  dialog.setConfirmOverwrite(true);
  QString fileName = dialog.getExistingDirectory(this, "", "");
  if (fileName != "")
    uiMainWindow->lineEdit_OutputPath->setText(fileName);
}

void T4IMainMenu::slotSelectSM1(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_PathToSM1->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->lineEdit_PathToSM1->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectSM2(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_PathToSM2->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->lineEdit_PathToSM2->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectTargetField(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_PathToTargetMag->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->lineEdit_PathToTargetMag->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectRecoilCalibrationFile(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_PathToRecoilCalibrationFile->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->lineEdit_PathToRecoilCalibrationFile->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotSelectCaloCalib(void)
{
  QFileDialog dialog;
  dialog.setDirectory(uiMainWindow->lineEdit_Path_CMTX->text());
  dialog.setConfirmOverwrite(true);
  if (dialog.exec()) {
    string fileName = dialog.selectedFiles().at(0).toStdString();
    reverseReplacePathEnvTGEANT(fileName);
    uiMainWindow->lineEdit_Path_CMTX->setText(QString::fromStdString(fileName));
  }
}

void T4IMainMenu::slotTarget(void)
{
  bool islh2 = uiMainWindow->checkBox_LH2->isChecked();
  if (islh2) {
    uiMainWindow->checkBox_LH2Hadron->setChecked(false);
    uiMainWindow->checkBox_DYTarget->setChecked(false);
    uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
    uiMainWindow->checkBox_PolTarget->setChecked(false);
    uiMainWindow->doubleSpinBox_PosTargetZ->setValue(-187.8);
    uiMainWindow->doubleSpinBox_targetStepLimit->setValue(1.);
  }
  
  bool islh2hadron = uiMainWindow->checkBox_LH2Hadron->isChecked();
  if (islh2hadron) {
    uiMainWindow->checkBox_LH2->setChecked(false);
    uiMainWindow->checkBox_DYTarget->setChecked(false);
    uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
    uiMainWindow->checkBox_PolTarget->setChecked(false);
    uiMainWindow->doubleSpinBox_PosTargetZ->setValue(-48.52);
    uiMainWindow->doubleSpinBox_targetStepLimit->setValue(0.5);
  }

  bool isprimakoff = uiMainWindow->checkBox_TargetPrimakoff2012->isChecked();
  if (isprimakoff) {
    uiMainWindow->checkBox_LH2->setChecked(false);
    uiMainWindow->checkBox_LH2Hadron->setChecked(false);
    uiMainWindow->checkBox_DYTarget->setChecked(false);
    uiMainWindow->checkBox_PolTarget->setChecked(false);
    uiMainWindow->doubleSpinBox_PosTargetZ->setValue(-67.4);
    uiMainWindow->doubleSpinBox_targetStepLimit->setValue(0.001);
  }

  bool ispoltarget = uiMainWindow->checkBox_PolTarget->isChecked();
  if (ispoltarget) {
    uiMainWindow->checkBox_LH2->setChecked(false);
    uiMainWindow->checkBox_LH2Hadron->setChecked(false);
    uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
    uiMainWindow->checkBox_DYTarget->setChecked(false);
    uiMainWindow->doubleSpinBox_PosTargetZ->setValue(0);
  }

  bool isdy = uiMainWindow->checkBox_DYTarget->isChecked();
  if (isdy) {
    uiMainWindow->checkBox_LH2->setChecked(false);
    uiMainWindow->checkBox_LH2Hadron->setChecked(false);
    uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
    uiMainWindow->checkBox_PolTarget->setChecked(false);
    uiMainWindow->doubleSpinBox_PosTargetZ->setValue(-230.);
  }

  uiMainWindow->checkBox_LH2->setEnabled(!isdy && !isprimakoff && !ispoltarget && !islh2hadron);
  uiMainWindow->checkBox_LH2Hadron->setEnabled(!isdy && !isprimakoff && !ispoltarget && !islh2);
  uiMainWindow->checkBox_TargetPrimakoff2012->setEnabled(!isdy && !islh2 && !islh2hadron && !ispoltarget);
  uiMainWindow->checkBox_DYTarget->setEnabled(!islh2 && !islh2hadron && !isprimakoff && !ispoltarget);
  uiMainWindow->checkBox_PolTarget->setEnabled(!isdy && !isprimakoff && !islh2 && !islh2hadron);

  uiMainWindow->checkBox_Target_Field->setEnabled(isdy || ispoltarget);
  uiMainWindow->doubleSpinBox_FieldScaleTarget->setEnabled(isdy || ispoltarget);
  uiMainWindow->lineEdit_PathToTargetMag->setEnabled(isdy || ispoltarget);
  uiMainWindow->pushButton_PathToTargetMag->setEnabled(isdy || ispoltarget);
}

void T4IMainMenu::slotRecoil(void)
{
  bool isRdp = uiMainWindow->checkBox_RPD->isChecked();
  if (isRdp)
    uiMainWindow->checkBox_CAMERA->setChecked(false);

  bool isCamera = uiMainWindow->checkBox_CAMERA->isChecked();
  if (isCamera)
    uiMainWindow->checkBox_RPD->setChecked(false);

  uiMainWindow->checkBox_RPD->setEnabled(!isCamera);
  uiMainWindow->checkBox_CAMERA->setEnabled(!isRdp);

  uiMainWindow->checkBox_recoil_Use_CalibrationFile->setEnabled(isCamera);
  uiMainWindow->lineEdit_PathToRecoilCalibrationFile->setEnabled(isCamera);
  uiMainWindow->pushButton_PathToRecoilCalibrationFile->setEnabled(isCamera);
}

void T4IMainMenu::slotW45(void)
{
  bool useDW01X = uiMainWindow->checkBox_DW01X->isChecked();
  uiMainWindow->doubleSpinBox_DW01X_X->setEnabled(useDW01X);
  uiMainWindow->doubleSpinBox_DW01X_Y->setEnabled(useDW01X);
  uiMainWindow->doubleSpinBox_DW01X_Z->setEnabled(useDW01X);
  uiMainWindow->checkBox_DW01X_mech->setEnabled(useDW01X);

  bool useDW01Y = uiMainWindow->checkBox_DW01Y->isChecked();
  uiMainWindow->doubleSpinBox_DW01Y_X->setEnabled(useDW01Y);
  uiMainWindow->doubleSpinBox_DW01Y_Y->setEnabled(useDW01Y);
  uiMainWindow->doubleSpinBox_DW01Y_Z->setEnabled(useDW01Y);
  uiMainWindow->checkBox_DW01Y_mech->setEnabled(useDW01Y);

  bool useDW02X = uiMainWindow->checkBox_DW02X->isChecked();
  uiMainWindow->doubleSpinBox_DW02X_X->setEnabled(useDW02X);
  uiMainWindow->doubleSpinBox_DW02X_Y->setEnabled(useDW02X);
  uiMainWindow->doubleSpinBox_DW02X_Z->setEnabled(useDW02X);
  uiMainWindow->checkBox_DW02X_mech->setEnabled(useDW02X);

  bool useDW02Y = uiMainWindow->checkBox_DW02Y->isChecked();
  uiMainWindow->doubleSpinBox_DW02Y_X->setEnabled(useDW02Y);
  uiMainWindow->doubleSpinBox_DW02Y_Y->setEnabled(useDW02Y);
  uiMainWindow->doubleSpinBox_DW02Y_Z->setEnabled(useDW02Y);
  uiMainWindow->checkBox_DW02Y_mech->setEnabled(useDW02Y);

  bool useDW03V = uiMainWindow->checkBox_DW03V->isChecked();
  uiMainWindow->doubleSpinBox_DW03V_X->setEnabled(useDW03V);
  uiMainWindow->doubleSpinBox_DW03V_Y->setEnabled(useDW03V);
  uiMainWindow->doubleSpinBox_DW03V_Z->setEnabled(useDW03V);
  uiMainWindow->checkBox_DW03V_mech->setEnabled(useDW03V);

  bool useDW03Y = uiMainWindow->checkBox_DW03Y->isChecked();
  uiMainWindow->doubleSpinBox_DW03Y_X->setEnabled(useDW03Y);
  uiMainWindow->doubleSpinBox_DW03Y_Y->setEnabled(useDW03Y);
  uiMainWindow->doubleSpinBox_DW03Y_Z->setEnabled(useDW03Y);
  uiMainWindow->checkBox_DW03Y_mech->setEnabled(useDW03Y);

  bool useDW04Y = uiMainWindow->checkBox_DW04Y->isChecked();
  uiMainWindow->doubleSpinBox_DW04Y_X->setEnabled(useDW04Y);
  uiMainWindow->doubleSpinBox_DW04Y_Y->setEnabled(useDW04Y);
  uiMainWindow->doubleSpinBox_DW04Y_Z->setEnabled(useDW04Y);
  uiMainWindow->checkBox_DW04Y_mech->setEnabled(useDW04Y);

  bool useDW04U = uiMainWindow->checkBox_DW04U->isChecked();
  uiMainWindow->doubleSpinBox_DW04U_X->setEnabled(useDW04U);
  uiMainWindow->doubleSpinBox_DW04U_Y->setEnabled(useDW04U);
  uiMainWindow->doubleSpinBox_DW04U_Z->setEnabled(useDW04U);
  uiMainWindow->checkBox_DW04U_mech->setEnabled(useDW04U);

  bool useDW05X = uiMainWindow->checkBox_DW05X->isChecked();
  uiMainWindow->doubleSpinBox_DW05X_X->setEnabled(useDW05X);
  uiMainWindow->doubleSpinBox_DW05X_Y->setEnabled(useDW05X);
  uiMainWindow->doubleSpinBox_DW05X_Z->setEnabled(useDW05X);
  uiMainWindow->checkBox_DW05X_mech->setEnabled(useDW05X);

  bool useDW05V = uiMainWindow->checkBox_DW05V->isChecked();
  uiMainWindow->doubleSpinBox_DW05V_X->setEnabled(useDW05V);
  uiMainWindow->doubleSpinBox_DW05V_Y->setEnabled(useDW05V);
  uiMainWindow->doubleSpinBox_DW05V_Z->setEnabled(useDW05V);
  uiMainWindow->checkBox_DW05V_mech->setEnabled(useDW05V);

  bool useDW06U = uiMainWindow->checkBox_DW06U->isChecked();
  uiMainWindow->doubleSpinBox_DW06U_X->setEnabled(useDW06U);
  uiMainWindow->doubleSpinBox_DW06U_Y->setEnabled(useDW06U);
  uiMainWindow->doubleSpinBox_DW06U_Z->setEnabled(useDW06U);
  uiMainWindow->checkBox_DW06U_mech->setEnabled(useDW06U);

  bool useDW06X = uiMainWindow->checkBox_DW06X->isChecked();
  uiMainWindow->doubleSpinBox_DW06X_X->setEnabled(useDW06X);
  uiMainWindow->doubleSpinBox_DW06X_Y->setEnabled(useDW06X);
  uiMainWindow->doubleSpinBox_DW06X_Z->setEnabled(useDW06X);
  uiMainWindow->checkBox_DW06X_mech->setEnabled(useDW06X);
}

void T4IMainMenu::slotHodoscopes(void)
{
  bool active;
  active = uiMainWindow->checkBox_HO03->isChecked();
  uiMainWindow->doubleSpinBox_HO03_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HO03_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HO03_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HI04X1_u->isChecked();
  uiMainWindow->doubleSpinBox_HI04X1_u_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI04X1_u_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI04X1_u_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HI04X1_d->isChecked();
  uiMainWindow->doubleSpinBox_HI04X1_d_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI04X1_d_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI04X1_d_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04Y1_u1->isChecked();
  uiMainWindow->doubleSpinBox_HM04Y1_u1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_u1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_u1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04Y1_u2->isChecked();
  uiMainWindow->doubleSpinBox_HM04Y1_u2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_u2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_u2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04Y1_d1->isChecked();
  uiMainWindow->doubleSpinBox_HM04Y1_d1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_d1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_d1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04Y1_d2->isChecked();
  uiMainWindow->doubleSpinBox_HM04Y1_d2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_d2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04Y1_d2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04X1_u->isChecked();
  uiMainWindow->doubleSpinBox_HM04X1_u_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04X1_u_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04X1_u_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM04X1_d->isChecked();
  uiMainWindow->doubleSpinBox_HM04X1_d_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04X1_d_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM04X1_d_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL04X1_1->isChecked();
  uiMainWindow->doubleSpinBox_HL04X1_1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL04X1_2->isChecked();
  uiMainWindow->doubleSpinBox_HL04X1_2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL04X1_3->isChecked();
  uiMainWindow->doubleSpinBox_HL04X1_3_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_3_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_3_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL04X1_4->isChecked();
  uiMainWindow->doubleSpinBox_HL04X1_4_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_4_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL04X1_4_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HO04->isChecked();
  uiMainWindow->doubleSpinBox_HO04_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HO04_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HO04_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HI05X1_u->isChecked();
  uiMainWindow->doubleSpinBox_HI05X1_u_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI05X1_u_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI05X1_u_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HI05X1_d->isChecked();
  uiMainWindow->doubleSpinBox_HI05X1_d_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI05X1_d_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HI05X1_d_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05Y1_u1->isChecked();
  uiMainWindow->doubleSpinBox_HM05Y1_u1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_u1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_u1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05Y1_u2->isChecked();
  uiMainWindow->doubleSpinBox_HM05Y1_u2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_u2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_u2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05Y1_d1->isChecked();
  uiMainWindow->doubleSpinBox_HM05Y1_d1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_d1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_d1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05Y1_d2->isChecked();
  uiMainWindow->doubleSpinBox_HM05Y1_d2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_d2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05Y1_d2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05X1_u->isChecked();
  uiMainWindow->doubleSpinBox_HM05X1_u_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05X1_u_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05X1_u_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HM05X1_d->isChecked();
  uiMainWindow->doubleSpinBox_HM05X1_d_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05X1_d_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HM05X1_d_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL05X1_1->isChecked();
  uiMainWindow->doubleSpinBox_HL05X1_1_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_1_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_1_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL05X1_2->isChecked();
  uiMainWindow->doubleSpinBox_HL05X1_2_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_2_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_2_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL05X1_3->isChecked();
  uiMainWindow->doubleSpinBox_HL05X1_3_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_3_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_3_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HL05X1_4->isChecked();
  uiMainWindow->doubleSpinBox_HL05X1_4_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_4_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HL05X1_4_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HK01->isChecked();
  uiMainWindow->doubleSpinBox_HK01_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK01_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK01_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HK02->isChecked();
  uiMainWindow->doubleSpinBox_HK02_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK02_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK02_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HK03->isChecked();
  uiMainWindow->doubleSpinBox_HK03_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK03_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HK03_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HH02->isChecked();
  uiMainWindow->doubleSpinBox_HH02_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HH02_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HH02_Z->setEnabled(active);

  active = uiMainWindow->checkBox_HH03->isChecked();
  uiMainWindow->doubleSpinBox_HH03_X->setEnabled(active);
  uiMainWindow->doubleSpinBox_HH03_Y->setEnabled(active);
  uiMainWindow->doubleSpinBox_HH03_Z->setEnabled(active);
}

void T4IMainMenu::setAll(bool input)
{
  uiMainWindow->checkBox_CAMERA->setChecked(input);
  uiMainWindow->checkBox_RPD->setChecked(false);
  uiMainWindow->checkBox_DC00->setChecked(input);
  uiMainWindow->checkBox_DC01->setChecked(input);
  uiMainWindow->checkBox_DC04->setChecked(input);
  uiMainWindow->checkBox_DC05->setChecked(false);
  uiMainWindow->checkBox_ECAL0->setChecked(input);
  uiMainWindow->checkBox_ECAL1->setChecked(input);
  uiMainWindow->checkBox_ECAL2->setChecked(input);
  uiMainWindow->checkBox_HCAL1->setChecked(input);
  uiMainWindow->checkBox_HCAL2->setChecked(input);
  uiMainWindow->checkBox_H1->setChecked(input);
  uiMainWindow->checkBox_H2->setChecked(input);
  uiMainWindow->checkBox_H3O->setChecked(input);
  uiMainWindow->checkBox_H4O->setChecked(input);
  uiMainWindow->checkBox_H4I->setChecked(false);
  uiMainWindow->checkBox_H4M->setChecked(input);
  uiMainWindow->checkBox_H4L->setChecked(input);
  uiMainWindow->checkBox_H5I->setChecked(input);
  uiMainWindow->checkBox_H5M->setChecked(input);
  uiMainWindow->checkBox_H5L->setChecked(input);
  uiMainWindow->checkBox_HK->setChecked(false);
  uiMainWindow->checkBox_HH->setChecked(false);
  uiMainWindow->checkBox_SandwichVeto->setChecked(false);
  uiMainWindow->checkBox_Multiplicity->setChecked(false);


  uiMainWindow->checkBox_MW1->setChecked(input);
  uiMainWindow->checkBox_MW2->setChecked(input);
  uiMainWindow->checkBox_MF3->setChecked(input);
  uiMainWindow->checkBox_LH2->setChecked(input);

  uiMainWindow->checkBox_Rich->setChecked(input);
  uiMainWindow->checkBox_SM1->setChecked(input);
  uiMainWindow->checkBox_SM2->setChecked(input);
  uiMainWindow->checkBox_ST02->setChecked(input);
  uiMainWindow->checkBox_ST03->setChecked(input);
  uiMainWindow->checkBox_ST05->setChecked(input);
  uiMainWindow->checkBox_MM01->setChecked(input);
  uiMainWindow->checkBox_MM02->setChecked(input);
  uiMainWindow->checkBox_MM03->setChecked(input);
  uiMainWindow->checkBox_MP00->setChecked(false);
  uiMainWindow->checkBox_MP01->setChecked(input);
  uiMainWindow->checkBox_MP02->setChecked(false);
  uiMainWindow->checkBox_MP03->setChecked(false);
  uiMainWindow->checkBox_GEM->setChecked(input);
  uiMainWindow->checkBox_PGEM->setChecked(input);
  uiMainWindow->checkBox_PA->setChecked(input);
  uiMainWindow->checkBox_PB->setChecked(input);
  uiMainWindow->checkBox_PS->setChecked(input);
  uiMainWindow->checkBox_SI->setChecked(input);
  uiMainWindow->checkBox_FI->setChecked(input);
  uiMainWindow->checkBox_Veto->setChecked(input);
  uiMainWindow->checkBox_W45->setChecked(input);
  uiMainWindow->checkBox_RichWall->setChecked(false);
  uiMainWindow->checkBox_DYTarget->setChecked(false);
  uiMainWindow->checkBox_DYAbsorber->setChecked(false);
  uiMainWindow->checkBox_TargetPrimakoff2012->setChecked(false);
  uiMainWindow->checkBox_PolTarget->setChecked(false);
  uiMainWindow->checkBox_LH2Hadron->setChecked(false);

  uiMainWindow->checkBox_PolGPD->setChecked(false);
  uiMainWindow->checkBox_TestSetupDummy->setChecked(false);
  uiMainWindow->checkBox_TestSetupTestCal->setChecked(false);
  uiMainWindow->checkBox_PolGPD_SiRPD->setChecked(false);
  uiMainWindow->checkBox_SCIFI_TEST->setChecked(false);
}

void T4IMainMenu::buttonPhysicsList(void)
{
  uiMainWindow->pushButton_PhysicsListPreferences->setEnabled(
      uiMainWindow->comboBox_PhysicsList_Details->currentText() == "FULL");
}

void T4IMainMenu::buttonSM2Field(void)
{
  if (uiMainWindow->comboBox_SM2Field->currentText() == "select") {
    uiMainWindow->lineEdit_PathToSM2->setEnabled(true);
    uiMainWindow->pushButton_PathToSM2->setEnabled(true);
  } else {
    uiMainWindow->lineEdit_PathToSM2->setEnabled(false);
    uiMainWindow->pushButton_PathToSM2->setEnabled(false);

    if (uiMainWindow->comboBox_SM2Field->currentText() == "4000")
      uiMainWindow->lineEdit_PathToSM2->setText("$TGEANT/resources/fieldmaps/FieldMapSM2_4000.data");
    else
      uiMainWindow->lineEdit_PathToSM2->setText("$TGEANT/resources/fieldmaps/FieldMapSM2_5000.data");
  }
}

void T4IMainMenu::cutsPhysicsList(void)
{
  uiMainWindow->doubleSpinBox_CutsGlobal->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsGams->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsRHGams->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsMainz->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsOlga->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsShashlik->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
  uiMainWindow->doubleSpinBox_CutsHcal->setDisabled(
      uiMainWindow->checkBox_noSecondaries->isChecked());
}

void T4IMainMenu::slotPhysicsList(void)
{
  settingsFile->setInterfaceSampling();

  connect(uiPhysicsList->checkBox_GANDALFbaseline, SIGNAL(stateChanged(int)),
      this, SLOT(baseLinePL()));
  connect(uiPhysicsList->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectPhysicsList()));
  connect(uiPhysicsList->pushButton_Cancel, SIGNAL(clicked()), this,
      SLOT(cancelPhysicsList()));
  menuPhysicsList->show();
  uiMainWindow->comboBox_PhysicsList_Details->setEnabled(false);
}

void T4IMainMenu::selectPhysicsList(void)
{
  settingsFile->setStructSampling();
  menuPhysicsList->close();
  uiMainWindow->comboBox_PhysicsList_Details->setEnabled(true);
}

void T4IMainMenu::cancelPhysicsList(void)
{
  if (!menuPhysicsList->isHidden())
    menuPhysicsList->close();
  uiMainWindow->comboBox_PhysicsList_Details->setEnabled(true);
}

void T4IMainMenu::baseLinePL(void)
{
  uiPhysicsList->doubleSpinBox_GANDALFbaseline->setEnabled(
      uiPhysicsList->checkBox_GANDALFbaseline->isChecked());
}
