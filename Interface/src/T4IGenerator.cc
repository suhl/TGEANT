#include "include/T4IGenerator.hh"

T4IGenerator::T4IGenerator(T4ISettingsFile* settingsFile_,
    Ui::MainWindow* uiMainWindow_, Ui::PreferencesBeamUser* uiUser_,
    Ui::PreferencesBeamCosmics* uiCosmics_,
    Ui::PreferencesBeamEcalCalib* uiEcalCalib_,
    Ui::PreferencesGeneratorHEPGEN* uiHEPGen_,
    Ui::PreferencesGeneratorPrimGen* uiPrimGen_,
    T4IPythiaGui* uiPythia_)
{
  settingsFile = settingsFile_;

  uiMainWindow = uiMainWindow_;
  uiUser = uiUser_;
  uiCosmics = uiCosmics_;
  uiEcalCalib = uiEcalCalib_;
  uiHEPGen = uiHEPGen_;
  uiPrimGen = uiPrimGen_;
  uiPythia = uiPythia_;

  menuHEPGen = new QWidget;
  menuPrimGen = new QWidget;
  menuUser = new QWidget;
  menuCosmics = new QWidget;
  menuEcalCalib = new QWidget;

  uiHEPGen->setupUi(menuHEPGen);
  uiPrimGen->setupUi(menuPrimGen);
  uiUser->setupUi(menuUser);
  uiCosmics->setupUi(menuCosmics);
  uiEcalCalib->setupUi(menuEcalCalib);

  // set default
  settingsFile->setStructHEPGen();
  settingsFile->setStructPrimGen();
  settingsFile->setStructUser();
  settingsFile->setStructCosmics();
  settingsFile->setStructEcalCalib();

  connect(uiMainWindow->pushButton_GeneratorPreferences, SIGNAL(clicked()),
      this, SLOT(slotGenerator()));

  // file dialog
  connect(uiMainWindow->pushButton_SelectLocalGeneratorFile, SIGNAL(clicked()),
      this, SLOT(slotSelectLocalGeneratorFile()));
  connect(uiMainWindow->checkBox_useBeamfile, SIGNAL(stateChanged(int)), this,
      SLOT(slotBeamfile()));
  connect(uiMainWindow->checkBox_usePileUp, SIGNAL(stateChanged(int)), this,
      SLOT(slotBeamfile()));
  connect(uiMainWindow->checkBox_usePileUp_2, SIGNAL(stateChanged(int)), this,
      SLOT(slotBeamfile()));
  connect(uiMainWindow->checkBox_useTargetExtrap, SIGNAL(stateChanged(int)), this,
      SLOT(slotBeamfile()));
  connect(uiMainWindow->comboBox_beamfileBackend, SIGNAL(currentIndexChanged(int)), this,
      SLOT(slotBeamfileType()));
  connect(uiMainWindow->pushButton_SelectPathToBeamfile, SIGNAL(clicked()),
      this, SLOT(slotSelectBeamfile()));
  connect(uiMainWindow->pushButton_SelectPathToBeamfile_2, SIGNAL(clicked()),
      this, SLOT(slotSelectBeamfile2()));

  // beam
  connect(uiMainWindow->radioButton_BeamOnly, SIGNAL(clicked()), this,
      SLOT(beamModeBeamOnly()));
  connect(uiMainWindow->radioButton_VisualizationMode, SIGNAL(clicked()), this,
      SLOT(beamModeVisualization()));
  connect(uiMainWindow->radioButton_SelectionMode, SIGNAL(clicked()), this,
      SLOT(beamModeComboBox()));
  connect(uiMainWindow->comboBox_BeamMode, SIGNAL(currentIndexChanged(int)), this,
      SLOT(beamModeComboBox()));
  connect(uiMainWindow->radioButton_Pythia6, SIGNAL(clicked()), this,
      SLOT(beamModePythia()));
  connect(uiMainWindow->radioButton_Pythia8, SIGNAL(clicked()), this,
      SLOT(beamModePythia8()));
  connect(uiMainWindow->radioButton_HEPGen, SIGNAL(clicked()), this,
      SLOT(beamModeHepgen()));
  connect(uiMainWindow->radioButton_Lepto, SIGNAL(clicked()), this,
      SLOT(beamModeLepto()));
  connect(uiMainWindow->radioButton_Primakoff, SIGNAL(clicked()), this,
      SLOT(beamModePrimakoff()));
  connect(uiMainWindow->radioButton_ascii, SIGNAL(clicked()), this,
      SLOT(beamModeASCII()));

  connect(uiPythia, SIGNAL(finished(int)), this,
      SLOT(cancel()));

  uiMainWindow->checkBox_useBeamfile->setChecked(false);
  useGeneratorFile(false);
}

void T4IGenerator::slotGenerator(void)
{
  if (uiMainWindow->radioButton_Pythia6->isChecked())
    slotPythia();
  else if (uiMainWindow->radioButton_HEPGen->isChecked())
    slotHEPGen();
  else if (uiMainWindow->radioButton_Primakoff->isChecked())
    slotPrimakoff();
  else if (uiMainWindow->radioButton_SelectionMode->isChecked()) {
    if (uiMainWindow->comboBox_BeamMode->currentText() == "User")
      slotUser();
    else if (uiMainWindow->comboBox_BeamMode->currentText() == "Cosmics")
      slotCosmics();
    else if (uiMainWindow->comboBox_BeamMode->currentText() == "EcalCalib")
      slotEcalCalib();
    else if (uiMainWindow->comboBox_BeamMode->currentText() == "EventGen")
      slotHEPGen();
  } else {
    // nothing to do
  }
}

void T4IGenerator::slotHEPGen(void)
{
  settingsFile->setInterfaceHEPGen();

  connect(uiHEPGen->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectHEPGen()));
  connect(uiHEPGen->pushButton_Cancel, SIGNAL(clicked()), this, SLOT(cancel()));
  menuHEPGen->show();
  enableSelection(false);
}

void T4IGenerator::slotPrimakoff(void)
{
  settingsFile->setInterfacePrimGen();
  connect(uiPrimGen->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectPrimGen()));
  connect(uiPrimGen->pushButton_Cancel, SIGNAL(clicked()), this, SLOT(cancel()));
  connect(uiPrimGen->doubleSpinBox_alpha_1, SIGNAL(valueChanged(double)), this, SLOT(slotPrimaUpdatePol(double)) );
  connect(uiPrimGen->doubleSpinBox_alpha_2, SIGNAL(valueChanged(double)), this, SLOT(slotPrimaUpdatePol(double)) );
  connect(uiPrimGen->doubleSpinBox_beta_1, SIGNAL(valueChanged(double)), this, SLOT(slotPrimaUpdatePol(double)) );
  connect(uiPrimGen->doubleSpinBox_beta_2, SIGNAL(valueChanged(double)), this, SLOT(slotPrimaUpdatePol(double)) );
  slotPrimaUpdatePol(1);
  menuPrimGen->show();
  enableSelection(false);
}

void T4IGenerator::slotPrimaUpdatePol(double dummy)
{
  const double factor = (0.1973269718/0.13957018)*(0.1973269718/0.13957018)*(0.1973269718/0.13957018)*1e4;
  QString value = QString("%1*10^-4 fm^3").arg(uiPrimGen->doubleSpinBox_alpha_1->value()*factor,5,'f',2,' ');
  uiPrimGen->label_alpha_1->setText(value);
  value = QString("%1*10^-4 fm^3").arg(uiPrimGen->doubleSpinBox_alpha_2->value()*factor,5,'f',2,' ');
  uiPrimGen->label_alpha_2->setText(value);
  value = QString("%1*10^-4 fm^3").arg(uiPrimGen->doubleSpinBox_beta_1->value()*factor,5,'f',2,' ');
  uiPrimGen->label_beta_1->setText(value);
  value = QString("%1*10^-4 fm^3").arg(uiPrimGen->doubleSpinBox_beta_2->value()*factor,5,'f',2,' ');
  uiPrimGen->label_beta_2->setText(value);

}

void T4IGenerator::slotPythia(void)
{
  string original = uiMainWindow->lineEdit_pathToLocalGeneratorFile->text().toStdString();
  settingsFile->getT4SStructManager()->getExternal()->localGeneratorFile = original;
  replacePathEnvTGEANT(settingsFile->getT4SStructManager()->getExternal()->localGeneratorFile);

  int retVal = uiPythia->exec();
  if (retVal == QDialog::Accepted) {
    reverseReplacePathEnvTGEANT(
        settingsFile->getT4SStructManager()->getExternal()->localGeneratorFile);
    uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText(
        QString::fromStdString(
            settingsFile->getT4SStructManager()->getExternal()
                ->localGeneratorFile));
  }
  else
    settingsFile->getT4SStructManager()->getExternal()->localGeneratorFile = original;
}

void T4IGenerator::slotUser(void)
{
  settingsFile->setInterfaceUser();
  slotUserEnergy();

  connect(uiUser->checkBox_useRandomEnergy, SIGNAL(stateChanged(int)), this,
      SLOT(slotUserEnergy()));
  connect(uiUser->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectUser()));
  connect(uiUser->pushButton_Cancel, SIGNAL(clicked()), this, SLOT(cancel()));
  menuUser->show();
  enableSelection(false);
}

void T4IGenerator::slotCosmics(void)
{
  settingsFile->setInterfaceCosmic();

  connect(uiCosmics->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectCosmics()));
  connect(uiCosmics->pushButton_Cancel, SIGNAL(clicked()), this,
      SLOT(cancel()));
  menuCosmics->show();
  enableSelection(false);
}

void T4IGenerator::slotEcalCalib(void)
{
  settingsFile->setInterfaceEcalCalib();

  connect(uiEcalCalib->pushButton_Accept, SIGNAL(clicked()), this,
      SLOT(selectEcalCalib()));
  connect(uiEcalCalib->pushButton_Cancel, SIGNAL(clicked()), this,
      SLOT(cancel()));
  menuEcalCalib->show();
  enableSelection(false);
}

void T4IGenerator::enableSelection(bool isOn)
{
  uiMainWindow->radioButton_BeamOnly->setEnabled(isOn);
  uiMainWindow->radioButton_VisualizationMode->setEnabled(isOn);
  uiMainWindow->radioButton_SelectionMode->setEnabled(isOn);
  uiMainWindow->comboBox_BeamMode->setEnabled(isOn);
  uiMainWindow->radioButton_Pythia6->setEnabled(isOn);
  uiMainWindow->radioButton_Pythia8->setEnabled(isOn);
  uiMainWindow->radioButton_HEPGen->setEnabled(isOn);
  uiMainWindow->radioButton_Lepto->setEnabled(isOn);
  uiMainWindow->radioButton_Primakoff->setEnabled(isOn);
}

void T4IGenerator::selectHEPGen(void)
{
  settingsFile->setStructHEPGen();
  menuHEPGen->close();
  enableSelection(true);
}

void T4IGenerator::selectPrimGen(void)
{
  settingsFile->setStructPrimGen();
  menuPrimGen->close();
  enableSelection(true);
}

void T4IGenerator::selectUser(void)
{
  settingsFile->setStructUser();
  menuUser->close();
  enableSelection(true);
}

void T4IGenerator::selectCosmics(void)
{
  settingsFile->setStructCosmics();
  menuCosmics->close();
  enableSelection(true);
}

void T4IGenerator::selectEcalCalib(void)
{
  settingsFile->setStructEcalCalib();
  menuEcalCalib->close();
  enableSelection(true);
}

void T4IGenerator::cancel(void)
{
  if (!menuHEPGen->isHidden())
    menuHEPGen->close();
  if (!menuPrimGen->isHidden())
    menuPrimGen->close();
  if (!menuUser->isHidden()) {
    settingsFile->setInterfaceUser();
    slotUserEnergy();
    menuUser->close();
  }
  if (!menuCosmics->isHidden())
    menuCosmics->close();
  if (!menuEcalCalib->isHidden())
    menuEcalCalib->close();

  enableSelection(true);
}

void T4IGenerator::slotSelectLocalGeneratorFile(void)
{
  QFileDialog dialog;
  dialog.setConfirmOverwrite(true);
  string fileName = dialog.getOpenFileName(this, tr("Open File"), "", tr("")).toStdString();
  reverseReplacePathEnvTGEANT(fileName);
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText(QString::fromStdString(fileName));
}

void T4IGenerator::slotBeamfile(void)
{
  bool isOn = uiMainWindow->checkBox_useBeamfile->isChecked();
  bool isOnPU = uiMainWindow->checkBox_usePileUp->isChecked();
  bool isOnPU2 = (isOnPU && uiMainWindow->checkBox_usePileUp_2->isChecked());
  bool isOnTargetExtrap = uiMainWindow->checkBox_useTargetExtrap->isChecked();

  uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(!isOn);

  uiMainWindow->lineEdit_PathToBeamfile->setEnabled(isOn);
  uiMainWindow->lineEdit_PathToBeamfile_2->setEnabled(isOnPU2);
  uiMainWindow->pushButton_SelectPathToBeamfile->setEnabled(isOn);
  uiMainWindow->pushButton_SelectPathToBeamfile_2->setEnabled(isOnPU2);
  uiMainWindow->comboBox_beamfileBackend->setEnabled(isOn);
  uiMainWindow->comboBox_beamfileType->setEnabled(isOn);
  uiMainWindow->doubleSpinBox_beamFileZConvention->setEnabled(isOn);
  uiMainWindow->doubleSpinBox_beamFileZConvention_2->setEnabled(isOnPU2);

  uiMainWindow->doubleSpinBox_targetStepLimit->setEnabled(isOnTargetExtrap);

  uiMainWindow->checkBox_usePileUp->setEnabled(isOn);
  uiMainWindow->checkBox_usePileUp_2->setEnabled(isOnPU);
  uiMainWindow->doubleSpinBox_beamFlux->setEnabled(isOn && isOnPU);
  uiMainWindow->doubleSpinBox_beamFlux_2->setEnabled(isOnPU2);
  uiMainWindow->doubleSpinBox_TimeGate->setEnabled(isOn && isOnPU);
}

void T4IGenerator::slotBeamfileType(void)
{
  QString currentText = uiMainWindow->comboBox_beamfileBackend->currentText();
  if (currentText == "Muon") {
    uiMainWindow->spinBox_beamParticleId->setValue(-13);
    uiMainWindow->doubleSpinBox_beamFileZConvention->setValue(0.);
    uiMainWindow->doubleSpinBox_beamZStart->setValue(-9000.);
    uiMainWindow->checkBox_useTargetExtrap->setChecked(true);
    uiMainWindow->checkBox_useHadronicInteractionCall->setChecked(false);
    if (uiMainWindow->lineEdit_PathToBeamfile->text() == "$TGEANT/resources/dy_2014/beamfile_dy2014.dat")
      uiMainWindow->lineEdit_PathToBeamfile->setText("$TGEANT/resources/dvcs_2012/beamfile_dvcs2012.dat");
    uiMainWindow->checkBox_usePileUp_2->setChecked(false);
  } else if (currentText == "Pion") {
    uiMainWindow->spinBox_beamParticleId->setValue(-211);
    uiMainWindow->doubleSpinBox_beamFileZConvention->setValue(-7500.);
    uiMainWindow->doubleSpinBox_beamZStart->setValue(-7500.);
    uiMainWindow->checkBox_useTargetExtrap->setChecked(false);
    uiMainWindow->checkBox_useHadronicInteractionCall->setChecked(true);
    if (uiMainWindow->lineEdit_PathToBeamfile->text() == "$TGEANT/resources/dvcs_2012/beamfile_dvcs2012.dat")
      uiMainWindow->lineEdit_PathToBeamfile->setText("$TGEANT/resources/dy_2014/beamfile_dy2014.dat");
  }
}

void T4IGenerator::slotSelectBeamfile(void)
{
  QFileDialog dialog;
  dialog.setConfirmOverwrite(true);
  string fileName = dialog.getOpenFileName(this, tr("Open File"), "", tr("")).toStdString();
  reverseReplacePathEnvTGEANT(fileName);
  uiMainWindow->lineEdit_PathToBeamfile->setText(QString::fromStdString(fileName));
}

void T4IGenerator::slotSelectBeamfile2(void)
{
  QFileDialog dialog;
  dialog.setConfirmOverwrite(true);
  string fileName = dialog.getOpenFileName(this, tr("Open File"), "", tr("")).toStdString();
  reverseReplacePathEnvTGEANT(fileName);
  uiMainWindow->lineEdit_PathToBeamfile_2->setText(QString::fromStdString(fileName));
}

void T4IGenerator::beamModeBeamOnly(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
}

void T4IGenerator::beamModeVisualization(void)
{
  useVisualizationMode(true);
  useBeamfile(false);
  useGeneratorFile(false);
  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);

  uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(false);
  uiMainWindow->spinBox_beamParticleId->setEnabled(false);
  uiMainWindow->radioButton_VisualizationEnabled->setChecked(true);
  uiMainWindow->checkBox_saveASCII->setChecked(false);
  uiMainWindow->checkBox_saveBinary->setChecked(false);
  uiMainWindow->checkBox_useTriggerLogic->setChecked(false);
  uiMainWindow->checkBox_RestoreFileFromSeed->setChecked(false);
}

void T4IGenerator::beamModeComboBox(void)
{
  useVisualizationMode(false);
  useBeamfile(false);
  useGeneratorFile(false);
  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(true);

  QString currentText = uiMainWindow->comboBox_BeamMode->currentText();
  if (currentText == "User") {
    uiMainWindow->doubleSpinBox_beamEnergy->setValue(160);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(true);
    uiMainWindow->spinBox_beamParticleId->setEnabled(true);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
  } else if (currentText == "Cosmics") {
    uiMainWindow->doubleSpinBox_beamEnergy->setValue(4);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(true);
    uiMainWindow->spinBox_beamParticleId->setValue(-13);
    uiMainWindow->spinBox_beamParticleId->setEnabled(false);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
  } else if (currentText == "ElectronBeam") {
    uiMainWindow->spinBox_beamParticleId->setValue(11);
    uiMainWindow->spinBox_beamParticleId->setEnabled(false);
    uiMainWindow->doubleSpinBox_beamEnergy->setValue(40);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(false);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
  } else if (currentText == "PhotonBeam" || currentText == "PhotonCone") {
    uiMainWindow->spinBox_beamParticleId->setValue(22);
    uiMainWindow->spinBox_beamParticleId->setEnabled(false);
    uiMainWindow->doubleSpinBox_beamEnergy->setValue(2);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(false);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
  } else if (currentText == "EcalCalib") {
    uiMainWindow->spinBox_beamParticleId->setValue(22);
    uiMainWindow->spinBox_beamParticleId->setEnabled(true);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(false);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
  } else if (currentText == "EventGen") {
    useBeamfile(true);
    uiMainWindow->spinBox_beamParticleId->setValue(-13);
    uiMainWindow->doubleSpinBox_beamEnergy->setValue(160);
    uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(true);
    uiMainWindow->spinBox_beamParticleId->setEnabled(true);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
  } else if (currentText == "DummyGen") {
    useBeamfile(true);
    uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
  }
}

void T4IGenerator::beamModeHepgen(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(false);
  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);

  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);
  uiMainWindow->radioButton_ascii->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
}

void T4IGenerator::beamModePythia(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(true);
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText("$TGEANT/resources/dy_2014/pythiaSettings_dy2014.xml");

  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_ascii->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
}

void T4IGenerator::beamModePythia8(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(true);
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText("$TGEANT/resources/dy_2014/pythia8_tuning.txt");

  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_ascii->setChecked(false);
  
  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
}

void T4IGenerator::beamModeLepto(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(true);
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setText("unset");

  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Primakoff->setChecked(false);
  uiMainWindow->radioButton_ascii->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
}

void T4IGenerator::beamModePrimakoff(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(false);
  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);
  uiMainWindow->radioButton_ascii->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(true);
}

void T4IGenerator::beamModeASCII(void)
{
  useVisualizationMode(false);
  useBeamfile(true);
  useGeneratorFile(true);
  uiMainWindow->radioButton_BeamOnly->setChecked(false);
  uiMainWindow->radioButton_VisualizationMode->setChecked(false);
  uiMainWindow->radioButton_SelectionMode->setChecked(false);
  uiMainWindow->comboBox_BeamMode->setEnabled(false);
  uiMainWindow->radioButton_Pythia6->setChecked(false);
  uiMainWindow->radioButton_Pythia8->setChecked(false);
  uiMainWindow->radioButton_HEPGen->setChecked(false);
  uiMainWindow->radioButton_Lepto->setChecked(false);

  uiMainWindow->pushButton_GeneratorPreferences->setEnabled(false);
}

void T4IGenerator::useBeamfile(bool isOn)
{
  if (!isOn)
    uiMainWindow->checkBox_useBeamfile->setChecked(false);

  uiMainWindow->lineEdit_PathToBeamfile->setEnabled(isOn);
  uiMainWindow->pushButton_SelectPathToBeamfile->setEnabled(isOn);
  uiMainWindow->checkBox_useBeamfile->setEnabled(isOn);
}

void T4IGenerator::useVisualizationMode(bool isOn)
{
  // Beam
  uiMainWindow->doubleSpinBox_beamEnergy->setDisabled(isOn);
  uiMainWindow->spinBox_numParticlesPerRun->setDisabled(isOn);
  // General
  uiMainWindow->comboBox_PhysicsList_Details->setDisabled(isOn);
  uiMainWindow->lineEdit_runName->setDisabled(isOn);
  uiMainWindow->lineEdit_OutputPath->setDisabled(isOn);
  uiMainWindow->pushButton_PathToOutputData->setDisabled(isOn);
  uiMainWindow->checkBox_RestoreFileFromSeed->setDisabled(isOn);
  uiMainWindow->radioButton_VisualizationDisabled->setDisabled(isOn);

  if (isOn)
    uiMainWindow->comboBox_PhysicsList_Details->setCurrentIndex(
        uiMainWindow->comboBox_PhysicsList_Details->findText("STANDARD"));

}

void T4IGenerator::slotUserEnergy(void)
{
  bool useRandomEnergy = uiUser->checkBox_useRandomEnergy->isChecked();
  uiUser->doubleSpinBox_RandomEnergyMax->setEnabled(useRandomEnergy);
  uiUser->doubleSpinBox_RandomEnergyMin->setEnabled(useRandomEnergy);
  uiMainWindow->doubleSpinBox_beamEnergy->setEnabled(!useRandomEnergy);
}

void T4IGenerator::useGeneratorFile(bool isLepto)
{
  uiMainWindow->lineEdit_pathToLocalGeneratorFile->setEnabled(isLepto);
  uiMainWindow->pushButton_SelectLocalGeneratorFile->setEnabled(isLepto);
}
