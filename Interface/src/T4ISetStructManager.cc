#include "include/T4ISettingsFile.hh"

void T4ISettingsFile::setStructManager(void)
{
  structM->clearStructManager();

  setStructGeneral();
  setStructBeam();
  setStructPaths();
  setStructCalorimeter();

  setStructUser();
  setStructCosmics();
  setStructEcalCalib();
  setStructHEPGen();
  setStructSampling();

  setStructMW1();
  setStructMW2();
  setStructST02();
  setStructST03();
  setStructST05();
  setStructMicromegas();
  setStructGem();
  setStructSciFi();
  setStructSilicon();
  setStructMWPC();
  setStructVeto();
  setStructDC00();
  setStructDC01();
  setStructDC04();
  setStructDC05();
  setStructW45();
  setStructRichWall();
  setStructRPD();
  setStructMagnet();
  setStructRICH();
  setStructDetector();
  setStructDetectorRes();
  setStructPolGPD();
  setStructScifiTest();
  setStructDummy();
}

void T4ISettingsFile::setStructGeneral(void)
{
  T4SGeneral* general = structM->getGeneral();
  general->physicsList = getString(
      uiMainWindow->comboBox_PhysicsList_Details->currentText());
  general->useGflash = uiMainWindow->checkBox_useGflash->isChecked();
  general->runName = getString(uiMainWindow->lineEdit_runName->text());
  general->seed = uiMainWindow->lineEdit_seed->text().toLong();
  general->useVisualization = uiMainWindow->radioButton_VisualizationEnabled
      ->isChecked();
  general->useSeed = uiMainWindow->checkBox_RestoreFileFromSeed->isChecked();

  general->outputPath = getString(uiMainWindow->lineEdit_OutputPath->text());
  general->saveASCII = uiMainWindow->checkBox_saveASCII->isChecked();
  general->saveBinary = uiMainWindow->checkBox_saveBinary->isChecked();
  general->saveDetDat = uiMainWindow->checkBox_saveDETDAT->isChecked();
  general->useTrigger = uiMainWindow->checkBox_useTriggerLogic->isChecked();
  general->triggerPlugin = uiMainWindow->comboBox_triggerPlugin->currentText().toStdString();
  general->exportGDML = uiMainWindow->checkBox_ROOTGeometry_GDML->isChecked();
  general->namingWithSeed = uiMainWindow->checkBox_namingWithSeed->isChecked();

  general->verboseLevel = uiMainWindow->spinBox_verboseLevel->value();
  general->usePerformanceMonitor = uiMainWindow->checkBox_usePerformanceMonitor
      ->isChecked();
  general->checkOverlap = uiMainWindow->checkBox_checkOverlap->isChecked();
  general->productionCutsGlobal = uiMainWindow->doubleSpinBox_CutsGlobal->value();
  general->productionCutsGams = uiMainWindow->doubleSpinBox_CutsGams->value();
  general->productionCutsRHGams = uiMainWindow->doubleSpinBox_CutsRHGams->value();
  general->productionCutsMainz = uiMainWindow->doubleSpinBox_CutsMainz->value();
  general->productionCutsOlga = uiMainWindow->doubleSpinBox_CutsOlga->value();
  general->productionCutsShashlik = uiMainWindow->doubleSpinBox_CutsShashlik->value();
  general->productionCutsHcal = uiMainWindow->doubleSpinBox_CutsHcal->value();
  general->noSecondaries = uiMainWindow->checkBox_noSecondaries->isChecked();
  general->colorTheme = uiMainWindow->comboBox_ColorTheme->currentIndex();
  general->simplifiedGeometries = uiMainWindow->checkBox_simplifiedGeometries->isChecked();
  general->noMemoryLimitation = uiMainWindow->checkBox_noMemoryLimitation->isChecked();
  general->useSplitting = uiMainWindow->checkBox_UseSplitting->isChecked();
  general->eventsPerChunk = uiMainWindow->spinBox_EventNum->value();  
  general->useSplitPiping = uiMainWindow->checkBox_PipeToFile->isChecked();
}

void T4ISettingsFile::setStructPaths(void)
{
   T4SExternalFiles* extPaths = structM->getExternal();
   extPaths->beamFile = getString(
      uiMainWindow->lineEdit_PathToBeamfile->text());
   extPaths->beamFileForAdditionalPileUp = getString(
      uiMainWindow->lineEdit_PathToBeamfile_2->text());
   extPaths->localGeneratorFile = getString(
      uiMainWindow->lineEdit_pathToLocalGeneratorFile->text());
   
   extPaths->libHEPGen_Pi0 = getString(uiMainWindow->libHEPGen_pi0->text());
   extPaths->libHEPGen_Pi0_transv = getString(uiMainWindow->libHEPGen_pi0_transv->text());
   extPaths->libHEPGen_Rho = getString(uiMainWindow->libHEPGen_rho->text());
   extPaths->visualizationMacro = getString(uiMainWindow->visualizationMacro->text());
   extPaths->detectorEfficiency = getString(uiMainWindow->detectorEfficiencyFile->text());
   extPaths->triggerMatrixInnerX = getString(uiMainWindow->mtxInnerX->text());
   extPaths->triggerMatrixLadderX = getString(uiMainWindow->mtxLadderX->text());
   extPaths->triggerMatrixLAST = getString(uiMainWindow->mtxLAST->text());
   extPaths->triggerMatrixMiddleX = getString(uiMainWindow->mtxMiddleX->text());
   extPaths->triggerMatrixMiddleY = getString(uiMainWindow->mtxMiddleY->text());
   extPaths->triggerMatrixOuterY = getString(uiMainWindow->mtxOuterY->text());
   extPaths->calorimeterInfo = getString(uiMainWindow->lineEdit_Path_CMTX->text());
}

void T4ISettingsFile::setStructBeam(void)
{
  T4SBeam* beam = structM->getBeam();
  beam->numParticles = uiMainWindow->spinBox_numParticlesPerRun->value();
  beam->beamParticle = uiMainWindow->spinBox_beamParticleId->value();
  beam->beamPlugin = getBeamPlugin();
  beam->beamEnergy = uiMainWindow->doubleSpinBox_beamEnergy->value() * 1000; // GeV => MeV
  beam->useBeamfile = uiMainWindow->checkBox_useBeamfile->isChecked();
  beam->beamFileType = getString(
      uiMainWindow->comboBox_beamfileType->currentText());
  beam->beamFileBackend = getString(
      uiMainWindow->comboBox_beamfileBackend->currentText());
  beam->beamFileZConvention = uiMainWindow->doubleSpinBox_beamFileZConvention->value();
  beam->beamFileZConventionForAdditionalPileUp = uiMainWindow->doubleSpinBox_beamFileZConvention_2->value();
  beam->beamZStart = uiMainWindow->doubleSpinBox_beamZStart->value();
  beam->usePileUp = uiMainWindow->checkBox_usePileUp->isChecked();
  beam->useAdditionalPileUp = uiMainWindow->checkBox_usePileUp_2->isChecked();
  beam->beamFlux = uiMainWindow->doubleSpinBox_beamFlux->value();
  beam->additionalPileUpFlux = uiMainWindow->doubleSpinBox_beamFlux_2->value();
  beam->timeGate = uiMainWindow->doubleSpinBox_TimeGate->value();
  beam->useTargetExtrap = uiMainWindow->checkBox_useTargetExtrap->isChecked();
  beam->targetStepLimit = uiMainWindow->doubleSpinBox_targetStepLimit->value();
  beam->useHadronicInteractionEGCall = uiMainWindow->checkBox_useHadronicInteractionCall->isChecked();
}

void T4ISettingsFile::setStructCalorimeter(void)
{
  if (uiMainWindow->checkBox_ECAL0->isChecked()) {
    T4SDetector* calo = structM->addCalorimeter("ECAL0");
    calo->useDetector = true;
    calo->position[0] = uiMainWindow->doubleSpinBox_PosECAL0X->value() * 10;
    calo->position[1] = uiMainWindow->doubleSpinBox_PosECAL0Y->value() * 10;
    calo->position[2] = uiMainWindow->doubleSpinBox_PosECAL0Z->value() * 10;
    calo->useMechanicalStructure = uiMainWindow->checkBox_ECAL0_MechStructure
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ECAL1->isChecked()) {
    T4SDetector* calo = structM->addCalorimeter("ECAL1");
    calo->useDetector = true;
    calo->position[0] = uiMainWindow->doubleSpinBox_PosECAL1X->value() * 10;
    calo->position[1] = uiMainWindow->doubleSpinBox_PosECAL1Y->value() * 10;
    calo->position[2] = uiMainWindow->doubleSpinBox_PosECAL1Z->value() * 10;
    calo->useMechanicalStructure = uiMainWindow->checkBox_ECAL1_MechStructure
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ECAL2->isChecked()) {
    T4SDetector* calo = structM->addCalorimeter("ECAL2");
    calo->useDetector = true;
    calo->position[0] = uiMainWindow->doubleSpinBox_PosECAL2X->value() * 10;
    calo->position[1] = uiMainWindow->doubleSpinBox_PosECAL2Y->value() * 10;
    calo->position[2] = uiMainWindow->doubleSpinBox_PosECAL2Z->value() * 10;
    calo->useMechanicalStructure = uiMainWindow->checkBox_ECAL2_MechStructure
        ->isChecked();
  }
  if (uiMainWindow->checkBox_HCAL1->isChecked()) {
    T4SDetector* calo = structM->addCalorimeter("HCAL1");
    calo->useDetector = true;
    calo->position[0] = uiMainWindow->doubleSpinBox_PosHCAL1X->value() * 10;
    calo->position[1] = uiMainWindow->doubleSpinBox_PosHCAL1Y->value() * 10;
    calo->position[2] = uiMainWindow->doubleSpinBox_PosHCAL1Z->value() * 10;
    calo->useMechanicalStructure = uiMainWindow->checkBox_HCAL1_MechStructure
        ->isChecked();
  }
  if (uiMainWindow->checkBox_HCAL2->isChecked()) {
    T4SDetector* calo = structM->addCalorimeter("HCAL2");
    calo->useDetector = true;
    calo->position[0] = uiMainWindow->doubleSpinBox_PosHCAL2X->value() * 10;
    calo->position[1] = uiMainWindow->doubleSpinBox_PosHCAL2Y->value() * 10;
    calo->position[2] = uiMainWindow->doubleSpinBox_PosHCAL2Z->value() * 10;
    calo->useMechanicalStructure = uiMainWindow->checkBox_HCAL2_MechStructure
        ->isChecked();
  }
}

void T4ISettingsFile::setStructMW1(void)
{
  if (!uiMainWindow->checkBox_MW1->isChecked())
    return;
  T4SMW1* mw1 = structM->getMW1();
  mw1->general.useDetector = true;
  mw1->general.position[0] = uiMainWindow->doubleSpinBox_MW1_AbsorberX->value()
      * 10;
  mw1->general.position[1] = uiMainWindow->doubleSpinBox_MW1_AbsorberY->value()
      * 10;
  mw1->general.position[2] = uiMainWindow->doubleSpinBox_MW1_AbsorberZ->value()
      * 10;
  mw1->general.useMechanicalStructure = false;

  mw1->useAbsorber = uiMainWindow->checkBox_MW1_useAbsorber->isChecked();
  mw1->useMA01X1 = uiMainWindow->checkBox_MA01X1->isChecked();
  mw1->useMA01X3 = uiMainWindow->checkBox_MA01X3->isChecked();
  mw1->useMA01Y1 = uiMainWindow->checkBox_MA01Y1->isChecked();
  mw1->useMA01Y3 = uiMainWindow->checkBox_MA01Y3->isChecked();
  mw1->useMA02X1 = uiMainWindow->checkBox_MA02X1->isChecked();
  mw1->useMA02X3 = uiMainWindow->checkBox_MA02X3->isChecked();
  mw1->useMA02Y1 = uiMainWindow->checkBox_MA02Y1->isChecked();
  mw1->useMA02Y3 = uiMainWindow->checkBox_MA02Y3->isChecked();

  mw1->posMA01X1[0] = uiMainWindow->doubleSpinBox_MA01X1_X->value() * 10;
  mw1->posMA01X1[1] = uiMainWindow->doubleSpinBox_MA01X1_Y->value() * 10;
  mw1->posMA01X1[2] = uiMainWindow->doubleSpinBox_MA01X1_Z->value() * 10;

  mw1->posMA01X3[0] = uiMainWindow->doubleSpinBox_MA01X3_X->value() * 10;
  mw1->posMA01X3[1] = uiMainWindow->doubleSpinBox_MA01X3_Y->value() * 10;
  mw1->posMA01X3[2] = uiMainWindow->doubleSpinBox_MA01X3_Z->value() * 10;

  mw1->posMA01Y1[0] = uiMainWindow->doubleSpinBox_MA01Y1_X->value() * 10;
  mw1->posMA01Y1[1] = uiMainWindow->doubleSpinBox_MA01Y1_Y->value() * 10;
  mw1->posMA01Y1[2] = uiMainWindow->doubleSpinBox_MA01Y1_Z->value() * 10;

  mw1->posMA01Y3[0] = uiMainWindow->doubleSpinBox_MA01Y3_X->value() * 10;
  mw1->posMA01Y3[1] = uiMainWindow->doubleSpinBox_MA01Y3_Y->value() * 10;
  mw1->posMA01Y3[2] = uiMainWindow->doubleSpinBox_MA01Y3_Z->value() * 10;

  mw1->posMA02X1[0] = uiMainWindow->doubleSpinBox_MA02X1_X->value() * 10;
  mw1->posMA02X1[1] = uiMainWindow->doubleSpinBox_MA02X1_Y->value() * 10;
  mw1->posMA02X1[2] = uiMainWindow->doubleSpinBox_MA02X1_Z->value() * 10;

  mw1->posMA02X3[0] = uiMainWindow->doubleSpinBox_MA02X3_X->value() * 10;
  mw1->posMA02X3[1] = uiMainWindow->doubleSpinBox_MA02X3_Y->value() * 10;
  mw1->posMA02X3[2] = uiMainWindow->doubleSpinBox_MA02X3_Z->value() * 10;

  mw1->posMA02Y1[0] = uiMainWindow->doubleSpinBox_MA02Y1_X->value() * 10;
  mw1->posMA02Y1[1] = uiMainWindow->doubleSpinBox_MA02Y1_Y->value() * 10;
  mw1->posMA02Y1[2] = uiMainWindow->doubleSpinBox_MA02Y1_Z->value() * 10;

  mw1->posMA02Y3[0] = uiMainWindow->doubleSpinBox_MA02Y3_X->value() * 10;
  mw1->posMA02Y3[1] = uiMainWindow->doubleSpinBox_MA02Y3_Y->value() * 10;
  mw1->posMA02Y3[2] = uiMainWindow->doubleSpinBox_MA02Y3_Z->value() * 10;
}

void T4ISettingsFile::setStructMW2(void)
{
  if (!uiMainWindow->checkBox_MW2->isChecked())
    return;
  T4SMW2* mw2 = structM->getMW2();
  mw2->general.useDetector = true;
  mw2->general.position[0] = uiMainWindow->doubleSpinBox_PosMW2X->value() * 10;
  mw2->general.position[1] = uiMainWindow->doubleSpinBox_PosMW2Y->value() * 10;
  mw2->general.position[2] = uiMainWindow->doubleSpinBox_PosMW2Z->value() * 10;
  mw2->general.useMechanicalStructure = false;

  mw2->useAbsorber = uiMainWindow->checkBox_MW2_Absorber->isChecked();
  mw2->MB01X = uiMainWindow->checkBox_MB01X->isChecked();
  mw2->MB01Y = uiMainWindow->checkBox_MB01Y->isChecked();
  mw2->MB01V = uiMainWindow->checkBox_MB01V->isChecked();
  mw2->MB02X = uiMainWindow->checkBox_MB02X->isChecked();
  mw2->MB02Y = uiMainWindow->checkBox_MB02Y->isChecked();
  mw2->MB02V = uiMainWindow->checkBox_MB02V->isChecked();

  mw2->positionMB01X[0] = uiMainWindow->doubleSpinBox_MB01X_X->value() * 10;
  mw2->positionMB01X[1] = uiMainWindow->doubleSpinBox_MB01X_Y->value() * 10;
  mw2->positionMB01X[2] = uiMainWindow->doubleSpinBox_MB01X_Z->value() * 10;

  mw2->positionMB01Y[0] = uiMainWindow->doubleSpinBox_MB01Y_X->value() * 10;
  mw2->positionMB01Y[1] = uiMainWindow->doubleSpinBox_MB01Y_Y->value() * 10;
  mw2->positionMB01Y[2] = uiMainWindow->doubleSpinBox_MB01Y_Z->value() * 10;

  mw2->positionMB01V[0] = uiMainWindow->doubleSpinBox_MB01V_X->value() * 10;
  mw2->positionMB01V[1] = uiMainWindow->doubleSpinBox_MB01V_Y->value() * 10;
  mw2->positionMB01V[2] = uiMainWindow->doubleSpinBox_MB01V_Z->value() * 10;

  mw2->positionMB02X[0] = uiMainWindow->doubleSpinBox_MB02X_X->value() * 10;
  mw2->positionMB02X[1] = uiMainWindow->doubleSpinBox_MB02X_Y->value() * 10;
  mw2->positionMB02X[2] = uiMainWindow->doubleSpinBox_MB02X_Z->value() * 10;

  mw2->positionMB02Y[0] = uiMainWindow->doubleSpinBox_MB02Y_X->value() * 10;
  mw2->positionMB02Y[1] = uiMainWindow->doubleSpinBox_MB02Y_Y->value() * 10;
  mw2->positionMB02Y[2] = uiMainWindow->doubleSpinBox_MB02Y_Z->value() * 10;

  mw2->positionMB02V[0] = uiMainWindow->doubleSpinBox_MB02V_X->value() * 10;
  mw2->positionMB02V[1] = uiMainWindow->doubleSpinBox_MB02V_Y->value() * 10;
  mw2->positionMB02V[2] = uiMainWindow->doubleSpinBox_MB02V_Z->value() * 10;
}

void T4ISettingsFile::setStructST02(void)
{
  if (!uiMainWindow->checkBox_ST02->isChecked())
    return;
  if (uiMainWindow->checkBox_ST02X1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02X1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02X1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02X1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02X1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02X1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST02X2->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02X2");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02X2_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02X2_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02X2_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02X2_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST02Y1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02Y1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02Y1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02Y1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02Y1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02Y1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST02Y2->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02Y2");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02Y2_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02Y2_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02Y2_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02Y2_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST02U1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02U1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02U1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02U1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02U1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02U1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST02V1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST02V1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST02V1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST02V1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST02V1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST02V1_mech
        ->isChecked();
  }
}

void T4ISettingsFile::setStructST03(void)
{
  if (!uiMainWindow->checkBox_ST03->isChecked())
    return;
  if (uiMainWindow->checkBox_ST03X1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03X1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03X1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03X1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03X1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03X1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST03X2->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03X2");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03X2_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03X2_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03X2_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03X2_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST03Y1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03Y1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03Y1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03Y1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03Y1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03Y1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST03Y2->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03Y2");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03Y2_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03Y2_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03Y2_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03Y2_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST03U1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03U1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03U1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03U1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03U1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03U1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST03V1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST03V1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST03V1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST03V1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST03V1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST03V1_mech
        ->isChecked();
  }
}

void T4ISettingsFile::setStructST05(void)
{
  if (!uiMainWindow->checkBox_ST05->isChecked())
    return;
  if (uiMainWindow->checkBox_ST05X1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST05X1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST05X1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST05X1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST05X1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST05X1_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST05Y2->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST05Y2");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST05Y2_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST05Y2_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST05Y2_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST05Y2_mech
        ->isChecked();
  }
  if (uiMainWindow->checkBox_ST05U1->isChecked()) {
    T4SDetector* straw = structM->addStraw("ST05U1");
    straw->useDetector = true;
    straw->position[0] = uiMainWindow->doubleSpinBox_ST05U1_X->value() * 10;
    straw->position[1] = uiMainWindow->doubleSpinBox_ST05U1_Y->value() * 10;
    straw->position[2] = uiMainWindow->doubleSpinBox_ST05U1_Z->value() * 10;
    straw->useMechanicalStructure = uiMainWindow->checkBox_ST05U1_mech
        ->isChecked();
  }
}

void T4ISettingsFile::setStructMicromegas(void)
{
  if (uiMainWindow->checkBox_MM01->isChecked()) {
    if (uiMainWindow->checkBox_MM01X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM01X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM01X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM01X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM01Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM01Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM01Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM01Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM01Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM01V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM01V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM01V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM01V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM01V_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM01U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM01U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM01U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM01U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM01U_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MM02->isChecked()) {
    if (uiMainWindow->checkBox_MM02X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM02X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM02X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM02X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM02Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM02Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM02Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM02Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM02Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM02V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM02V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM02V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM02V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM02V_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM02U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM02U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM02U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM02U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM02U_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MM03->isChecked()) {
    if (uiMainWindow->checkBox_MM03X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM03X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM03X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM03X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM03Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM03Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM03Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM03Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM03Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM03V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM03V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM03V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM03V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM03V_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MM03U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MM03U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMM03U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMM03U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMM03U_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MP00->isChecked()) {
    if (uiMainWindow->checkBox_MP00X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP00X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP00X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP00X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP00X_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MP01->isChecked()) {
    if (uiMainWindow->checkBox_MP01X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP01X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP01X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP01X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP01Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP01Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP01Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP01Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP01Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP01U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP01U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP01U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP01U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP01U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP01V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP01V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP01V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP01V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP01V_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MP02->isChecked()) {
    if (uiMainWindow->checkBox_MP02X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP02X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP02X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP02X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP02Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP02Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP02Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP02Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP02Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP02U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP02U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP02U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP02U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP02U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP02V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP02V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP02V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP02V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP02V_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_MP03->isChecked()) {
    if (uiMainWindow->checkBox_MP03X->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP03X1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP03X_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP03X_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP03Y->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP03Y1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP03Y_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP03Y_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP03Y_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP03U->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP03U1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP03U_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP03U_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP03U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_MP03V->isChecked()) {
      T4SDetector* mm = structM->addMicromegas("MP03V1__");
      mm->useDetector = true;
      mm->position[0] = uiMainWindow->doubleSpinBox_PosMP03V_X->value() * 10;
      mm->position[1] = uiMainWindow->doubleSpinBox_PosMP03V_Y->value() * 10;
      mm->position[2] = uiMainWindow->doubleSpinBox_PosMP03V_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructGem(void)
{
  if (uiMainWindow->checkBox_GEM->isChecked()) {
    if (uiMainWindow->checkBox_GM01X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM01X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM01X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM01X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM01U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM01U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM01U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM01U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM01U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM02X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM02X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM02X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM02X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM02U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM02U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM02U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM02U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM02U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM03X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM03X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM03X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM03X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM03U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM03U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM03U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM03U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM03U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM04X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM04X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM04X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM04X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM04X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM04U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM04U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM04U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM04U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM04U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM05X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM05X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM05X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM05X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM05X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM05U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM05U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM05U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM05U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM05U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM06X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM06X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM06X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM06X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM06X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM06U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM06U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM06U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM06U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM06U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM07X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM07X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM07X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM07X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM07X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM07U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM07U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM07U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM07U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM07U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM08X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM08X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM08X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM08X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM08X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM08U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM08U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM08U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM08U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM08U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM09X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM09X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM09X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM09X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM09X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM09U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM09U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM09U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM09U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM09U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM10X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM10X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM10X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM10X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM10X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM10U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM10U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM10U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM10U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM10U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM11X->isChecked()) {
      T4SDetector* gem = structM->addGem("GM11X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM11X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM11X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM11X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GM11U->isChecked()) {
      T4SDetector* gem = structM->addGem("GM11U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGM11U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGM11U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGM11U_Z->value() * 10;
    }
  }

  if (uiMainWindow->checkBox_PGEM->isChecked()) {
    if (uiMainWindow->checkBox_GP01X->isChecked()) {
      T4SDetector* gem = structM->addGem("GP01X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP01X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP01X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GP01U->isChecked()) {
      T4SDetector* gem = structM->addGem("GP01U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP01U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP01U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP01U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GP02X->isChecked()) {
      T4SDetector* gem = structM->addGem("GP02X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP02X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP02X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GP02U->isChecked()) {
      T4SDetector* gem = structM->addGem("GP02U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP02U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP02U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP02U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GP03X->isChecked()) {
      T4SDetector* gem = structM->addGem("GP03X1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP03X_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP03X_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_GP03U->isChecked()) {
      T4SDetector* gem = structM->addGem("GP03U1__");
      gem->useDetector = true;
      gem->position[0] = uiMainWindow->doubleSpinBox_PosGP03U_X->value() * 10;
      gem->position[1] = uiMainWindow->doubleSpinBox_PosGP03U_Y->value() * 10;
      gem->position[2] = uiMainWindow->doubleSpinBox_PosGP03U_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructSciFi(void)
{
  if (uiMainWindow->checkBox_FI->isChecked()) {
    if (uiMainWindow->checkBox_FI01X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI01X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI01X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI15X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI15X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI15X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI15X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI15X_Z->value() * 10;
      det->useMechanicalStructure = uiMainWindow->checkBox_FI15X_useU->isChecked();
    }
    if (uiMainWindow->checkBox_FI02X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI02X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI02X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI02X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI03X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI03X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI03X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI03X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI35X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI35X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI35X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI35X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI35X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI04X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI04X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI04X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI04X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI04X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI05X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI05X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI05X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI05X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI05X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI55U->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI55U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI55U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI55U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI55U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI06X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI06X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI06X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI06X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI06X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI07X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI07X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI07X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI07X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI07X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_FI08X->isChecked()) {
      T4SDetector* det = structM->addSciFi("FI08X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosFI08X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosFI08X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosFI08X_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructSilicon(void)
{
  if (uiMainWindow->checkBox_SI->isChecked()) {
    if (uiMainWindow->checkBox_SI01X->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI01X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI01X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI01U->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI01U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI01U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI01U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI01U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI02X->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI02X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI02X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI02X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI02U->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI02U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI02U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI02U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI02U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI03X->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI03X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI03X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI03X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI03U->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI03U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI03U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI03U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI03U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI04X->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI04X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI04X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI04X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI04X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI04U->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI04U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI04U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI04U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI04U_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI05X->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI05X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI05X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI05X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI05X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_SI05U->isChecked()) {
      T4SDetector* det = structM->addSilicon("SI05U1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosSI05U_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosSI05U_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosSI05U_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructMWPC(void)
{
  if (uiMainWindow->checkBox_PA->isChecked()) {
    if (uiMainWindow->checkBox_PA01X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA01X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA01X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA02X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA02X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA02X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA02X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA02X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA03X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA03X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA03X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA03X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA04X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA04X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA04X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA04X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA04X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA05X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA05X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA05X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA05X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA05X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA06X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA06X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA06X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA06X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA06X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PA11X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PA11X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPA11X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPA11X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPA11X_Z->value() * 10;
    }
  }
  if (uiMainWindow->checkBox_PB->isChecked()) {
    if (uiMainWindow->checkBox_PB01X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB01X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB01X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB01X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PB03X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB03X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB03X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB03X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB03X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PB05X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB05X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB05X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB05X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB05X_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PB02V->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB02V1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB02V_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB02V_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB02V_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PB04V->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB04V1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB04V_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB04V_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB04V_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_PB06V->isChecked()) {
      T4SDetector* det = structM->addMWPC("PB06V1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPB06V_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPB06V_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPB06V_Z->value() * 10;
    }
  }
  if (uiMainWindow->checkBox_PS->isChecked()) {
    if (uiMainWindow->checkBox_PS01X->isChecked()) {
      T4SDetector* det = structM->addMWPC("PS01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_PosPS01X_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_PosPS01X_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_PosPS01X_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructVeto(void)
{
  if (uiMainWindow->checkBox_Veto->isChecked()) {
    if (uiMainWindow->checkBox_VI01P1->isChecked()) {
      T4SDetector* det = structM->addVeto("VI01P1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_VI01P1_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_VI01P1_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_VI01P1_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_VO01X1->isChecked()) {
      T4SDetector* det = structM->addVeto("VO01X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_VO01X1_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_VO01X1_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_VO01X1_Z->value() * 10;
    }
    if (uiMainWindow->checkBox_VI02X1->isChecked()) {
      T4SDetector* det = structM->addVeto("VI02X1__");
      det->useDetector = true;
      det->position[0] = uiMainWindow->doubleSpinBox_VI02X1_X->value() * 10;
      det->position[1] = uiMainWindow->doubleSpinBox_VI02X1_Y->value() * 10;
      det->position[2] = uiMainWindow->doubleSpinBox_VI02X1_Z->value() * 10;
    }
  }
}

void T4ISettingsFile::setStructDC00(void)
{
  if (!uiMainWindow->checkBox_DC00->isChecked())
    return;
  T4SDetector* dc = structM->addDC("DC00");
  dc->useDetector = true;
  dc->position[0] = uiMainWindow->doubleSpinBox_PosDC00X->value() * 10;
  dc->position[1] = uiMainWindow->doubleSpinBox_PosDC00Y->value() * 10;
  dc->position[2] = uiMainWindow->doubleSpinBox_PosDC00Z->value() * 10;
  dc->useMechanicalStructure = uiMainWindow->checkBox_DC00useMechStructure
      ->isChecked();
}

void T4ISettingsFile::setStructDC01(void)
{
  if (!uiMainWindow->checkBox_DC01->isChecked())
    return;
  T4SDetector* dc = structM->addDC("DC01");
  dc->useDetector = true;
  dc->position[0] = uiMainWindow->doubleSpinBox_PosDC01X->value() * 10;
  dc->position[1] = uiMainWindow->doubleSpinBox_PosDC01Y->value() * 10;
  dc->position[2] = uiMainWindow->doubleSpinBox_PosDC01Z->value() * 10;
  dc->useMechanicalStructure = uiMainWindow->checkBox_DC01useMechStructure
      ->isChecked();
}

void T4ISettingsFile::setStructDC04(void)
{
  if (!uiMainWindow->checkBox_DC04->isChecked())
    return;
  T4SDetector* dc = structM->addDC("DC04");
  dc->useDetector = true;
  dc->position[0] = uiMainWindow->doubleSpinBox_PosDC04X->value() * 10;
  dc->position[1] = uiMainWindow->doubleSpinBox_PosDC04Y->value() * 10;
  dc->position[2] = uiMainWindow->doubleSpinBox_PosDC04Z->value() * 10;
  dc->useMechanicalStructure = uiMainWindow->checkBox_DC04useMechStructure
      ->isChecked();
}

void T4ISettingsFile::setStructDC05(void)
{
  if (!uiMainWindow->checkBox_DC05->isChecked())
    return;
  T4SDetector* dc = structM->addDC("DC05");
  dc->useDetector = true;
  dc->position[0] = uiMainWindow->doubleSpinBox_PosDC05X->value() * 10;
  dc->position[1] = uiMainWindow->doubleSpinBox_PosDC05Y->value() * 10;
  dc->position[2] = uiMainWindow->doubleSpinBox_PosDC05Z->value() * 10;
  dc->useMechanicalStructure = uiMainWindow->checkBox_DC05useMechStructure
      ->isChecked();
}

void T4ISettingsFile::setStructW45(void)
{
  if (!uiMainWindow->checkBox_W45->isChecked())
    return;
  if (uiMainWindow->checkBox_DW01X->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW01X");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW01X_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW01X_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW01X_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW01X_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW01Y->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW01Y");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW01Y_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW01Y_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW01Y_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW01Y_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW02X->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW02X");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW02X_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW02X_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW02X_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW02X_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW02Y->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW02Y");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW02Y_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW02Y_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW02Y_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW02Y_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW03V->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW03V");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW03V_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW03V_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW03V_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW03V_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW03Y->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW03Y");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW03Y_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW03Y_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW03Y_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW03Y_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW04Y->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW04Y");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW04Y_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW04Y_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW04Y_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW04Y_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW04U->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW04U");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW04U_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW04U_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW04U_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW04U_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW05X->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW05X");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW05X_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW05X_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW05X_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW05X_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW05V->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW05V");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW05V_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW05V_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW05V_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW05V_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW06U->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW06U");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW06U_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW06U_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW06U_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW06U_mech->isChecked();
  }
  if (uiMainWindow->checkBox_DW06X->isChecked()) {
    T4SDetector* w45 = structM->addW45("DW06X");
    w45->useDetector = true;
    w45->position[0] = uiMainWindow->doubleSpinBox_DW06X_X->value() * 10;
    w45->position[1] = uiMainWindow->doubleSpinBox_DW06X_Y->value() * 10;
    w45->position[2] = uiMainWindow->doubleSpinBox_DW06X_Z->value() * 10;
    w45->useMechanicalStructure =
        uiMainWindow->checkBox_DW06X_mech->isChecked();
  }
}

void T4ISettingsFile::setStructRichWall(void)
{
  if (!uiMainWindow->checkBox_RichWall->isChecked())
    return;
  if (uiMainWindow->checkBox_DR01X1->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR01X1");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR01X1_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR01X1_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR01X1_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR01X2->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR01X2");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR01X2_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR01X2_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR01X2_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR01Y1->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR01Y1");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR01Y1_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR01Y1_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR01Y1_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR01Y2->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR01Y2");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR01Y2_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR01Y2_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR01Y2_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR02X1->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR02X1");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR02X1_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR02X1_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR02X1_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR02X2->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR02X2");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR02X2_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR02X2_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR02X2_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR02Y1->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR02Y1");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR02Y1_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR02Y1_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR02Y1_Z->value() * 10;
  }
  if (uiMainWindow->checkBox_DR02Y2->isChecked()) {
    T4SDetector* richwall = structM->addRichWall("DR02Y2");
    richwall->useDetector = true;
    richwall->position[0] = uiMainWindow->doubleSpinBox_DR02Y2_X->value() * 10;
    richwall->position[1] = uiMainWindow->doubleSpinBox_DR02Y2_Y->value() * 10;
    richwall->position[2] = uiMainWindow->doubleSpinBox_DR02Y2_Z->value() * 10;
  }
}

void T4ISettingsFile::setStructRPD(void)
{
  for (unsigned int i = 0; i < structM->getRPD()->size(); i++) {
    T4SRPD* rpd = &structM->getRPD()->at(i);

    if (rpd->general.name == "CA01R1" || rpd->general.name == "RP01R1") {

      if (rpd->general.name == "CA01R1") {
        if (uiMainWindow->checkBox_CAMERA->isChecked())
          rpd->general.useDetector = uiMainWindow->checkBox_recoil_RingA
              ->isChecked();
        else
          rpd->general.useDetector = false;

        rpd->useCalibrationFile = uiMainWindow
            ->checkBox_recoil_Use_CalibrationFile->isChecked();
        rpd->calibrationFilePath = getString(
            uiMainWindow->lineEdit_PathToRecoilCalibrationFile->text());
      } else {
        if (uiMainWindow->checkBox_RPD->isChecked())
          rpd->general.useDetector = uiMainWindow->checkBox_recoil_RingA
              ->isChecked();
        else
          rpd->general.useDetector = false;

        rpd->useConicalCryostat = uiMainWindow
            ->checkBox_recoil_RingA_ConicalCryostat->isChecked();
      }

      rpd->general.position[0] = uiMainWindow->doubleSpinBox_recoil_RingA_X
          ->value() * 10;
      rpd->general.position[1] = uiMainWindow->doubleSpinBox_recoil_RingA_Y
          ->value() * 10;
      rpd->general.position[2] = uiMainWindow->doubleSpinBox_recoil_RingA_Z
          ->value() * 10;
      rpd->general.useMechanicalStructure = uiMainWindow
          ->checkBox_recoil_RingA_Mech->isChecked();
      rpd->useOptical =
          uiMainWindow->checkBox_recoil_RingA_Optical->isChecked();
      rpd->useSingleSlab =
          uiMainWindow->checkBox_recoil_RingA_Slab->isChecked();

    } else if (rpd->general.name == "CA01R2" || rpd->general.name == "RP01R2") {

      if (rpd->general.name == "CA01R2") {
        if (uiMainWindow->checkBox_CAMERA->isChecked())
          rpd->general.useDetector = uiMainWindow->checkBox_recoil_RingB
              ->isChecked();
        else
          rpd->general.useDetector = false;

        rpd->useCalibrationFile =
            uiMainWindow->checkBox_recoil_Use_CalibrationFile->isChecked();
        rpd->calibrationFilePath = getString(
            uiMainWindow->lineEdit_PathToRecoilCalibrationFile->text());
      } else {
        if (uiMainWindow->checkBox_RPD->isChecked())
          rpd->general.useDetector = uiMainWindow->checkBox_recoil_RingB
              ->isChecked();
        else
          rpd->general.useDetector = false;
      }

      rpd->general.position[0] = uiMainWindow->doubleSpinBox_recoil_RingB_X
          ->value() * 10;
      rpd->general.position[1] = uiMainWindow->doubleSpinBox_recoil_RingB_Y
          ->value() * 10;
      rpd->general.position[2] = uiMainWindow->doubleSpinBox_recoil_RingB_Z
          ->value() * 10;
      rpd->general.useMechanicalStructure = uiMainWindow
          ->checkBox_recoil_RingB_Mech->isChecked();
      rpd->useOptical =
          uiMainWindow->checkBox_recoil_RingB_Optical->isChecked();
      rpd->useSingleSlab =
          uiMainWindow->checkBox_recoil_RingB_Slab->isChecked();
    }
  }
}

void T4ISettingsFile::setStructMagnet(void)
{
  for (unsigned int i = 0; i < structM->getMagnet()->size(); i++) {
    T4SMagnet* magnet = &structM->getMagnet()->at(i);

    if (magnet->general.name == "SM1") {
      magnet->general.useDetector = uiMainWindow->checkBox_SM1->isChecked();
      magnet->general.position[0] = uiMainWindow->doubleSpinBox_PosSM1X->value()
          * 10;
      magnet->general.position[1] = uiMainWindow->doubleSpinBox_PosSM1Y->value()
          * 10;
      magnet->general.position[2] = uiMainWindow->doubleSpinBox_PosSM1Z->value()
          * 10;
      magnet->general.useMechanicalStructure = uiMainWindow->checkBox_SM1_Mech->isChecked();
      magnet->useField = uiMainWindow->checkBox_SM1_Field->isChecked();
      magnet->fieldmapPath = getString(
          uiMainWindow->lineEdit_PathToSM1->text());
      magnet->scaleFactor = uiMainWindow->doubleSpinBox_FieldScaleSM1->value();

    } else if (magnet->general.name == "SM2") {
      magnet->general.useDetector = uiMainWindow->checkBox_SM2->isChecked();
      magnet->general.position[0] = uiMainWindow->doubleSpinBox_PosSM2X->value()
          * 10;
      magnet->general.position[1] = uiMainWindow->doubleSpinBox_PosSM2Y->value()
          * 10;
      magnet->general.position[2] = uiMainWindow->doubleSpinBox_PosSM2Z->value()
          * 10;
      magnet->general.useMechanicalStructure = uiMainWindow->checkBox_SM2_Mech->isChecked();
      magnet->useField = uiMainWindow->checkBox_SM2_Field->isChecked();
      magnet->fieldmapPath = getString(
          uiMainWindow->lineEdit_PathToSM2->text());
      magnet->scaleFactor = uiMainWindow->doubleSpinBox_FieldScaleSM2->value();
      magnet->current = strToInt(uiMainWindow->comboBox_SM2Field->currentText().toStdString());

    } else if (magnet->general.name == "DYTARGET" || magnet->general.name == "POLTARGET") {
      if (magnet->general.name == "DYTARGET")
        magnet->general.useDetector = uiMainWindow->checkBox_DYTarget->isChecked();
      else if (magnet->general.name == "POLTARGET")
        magnet->general.useDetector = uiMainWindow->checkBox_PolTarget->isChecked();

      magnet->general.position[0] = uiMainWindow->doubleSpinBox_PosTargetX->value()
          * 10;
      magnet->general.position[1] = uiMainWindow->doubleSpinBox_PosTargetY->value()
          * 10;
      magnet->general.position[2] = uiMainWindow->doubleSpinBox_PosTargetZ->value()
          * 10;
      magnet->general.useMechanicalStructure = uiMainWindow
          ->checkBox_TargetMechStructure->isChecked();
      magnet->useField = uiMainWindow->checkBox_Target_Field->isChecked();
      magnet->fieldmapPath = getString(
          uiMainWindow->lineEdit_PathToTargetMag->text());
      magnet->scaleFactor = uiMainWindow->doubleSpinBox_FieldScaleTarget->value();
    }
  }
}

void T4ISettingsFile::setStructRICH(void)
{
  T4SRICH* rich = structM->getRICH();
  rich->general.useDetector = uiMainWindow->checkBox_Rich->isChecked();
  rich->general.position[0] = uiMainWindow->doubleSpinBox_PosRichX->value()
      * 10;
  rich->general.position[1] = uiMainWindow->doubleSpinBox_PosRichY->value()
      * 10;
  rich->general.position[2] = uiMainWindow->doubleSpinBox_PosRichZ->value()
      * 10;
  rich->general.useMechanicalStructure = false;
  rich->useOpticalPhysics = uiMainWindow->checkBox_Rich_OpticalPhysics
      ->isChecked();
  rich->visibleHousing =
      uiMainWindow->checkBox_RICH_housingVisible->isChecked();
  rich->gas = uiMainWindow->comboBox_richGas->currentText().toStdString();
}

void T4ISettingsFile::setStructDetector(void)
{
  for (unsigned int i = 0; i < structM->getDetector()->size(); i++) {
    T4SDetector* detector = &structM->getDetector()->at(i);

    // LH2
    if (detector->name == "LH2" || detector->name == "LH2Hadron" || detector->name == "PRIMAKOFFTARGET" 
      || detector->name == "WORKSHOPEXAMPLE") {
      if (detector->name == "LH2")
        detector->useDetector = uiMainWindow->checkBox_LH2->isChecked();
      else if (detector->name == "LH2Hadron")
        detector->useDetector = uiMainWindow->checkBox_LH2Hadron->isChecked();
      else if (detector->name == "PRIMAKOFFTARGET")
        detector->useDetector = uiMainWindow->checkBox_TargetPrimakoff2012->isChecked();
      else if (detector->name == "WORKSHOPEXAMPLE")
        detector->useDetector = uiMainWindow->checkBox_WorkshopExample->isChecked();

      detector->position[0] = uiMainWindow->doubleSpinBox_PosTargetX->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_PosTargetY->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_PosTargetZ->value() * 10;
      detector->useMechanicalStructure = uiMainWindow->checkBox_TargetMechStructure->isChecked();
    }

    // DY 2014 setup
    else if (detector->name == "DYABSORBER") {
      detector->useDetector = uiMainWindow->checkBox_DYAbsorber->isChecked();
      detector->position[0] =
          uiMainWindow->doubleSpinBox_PosDYAbsorberX->value() * 10;
      detector->position[1] =
          uiMainWindow->doubleSpinBox_PosDYAbsorberY->value() * 10;
      detector->position[2] =
          uiMainWindow->doubleSpinBox_PosDYAbsorberZ->value() * 10;
      detector->useMechanicalStructure = false;
    }

    // MF3
    else if (detector->name == "MF3") {
      detector->useDetector = uiMainWindow->checkBox_MF3->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_PosMF3_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_PosMF3_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_PosMF3_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    }
    else if (detector->name == "MF3_BLOCK") {
      detector->useDetector = uiMainWindow->checkBox_MF3->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_PosMF3_Block_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_PosMF3_Block_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_PosMF3_Block_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    }

    else if (detector->name == "TESTCAL") {
      detector->useDetector =
          uiMainWindow->checkBox_TestSetupTestCal->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_TESTCAL_PosX->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_TESTCAL_PosY->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_TESTCAL_PosZ->value()
          * 10;
      detector->useMechanicalStructure = uiMainWindow
          ->checkBox_TESTCAL_MechanicalStructure->isChecked();
    }
    else if (detector->name == "PolGPDSiRPD") {
      detector->useDetector =
          uiMainWindow->checkBox_PolGPD_SiRPD->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosX->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosY->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_PolGPD_SiRPD_PosZ->value()
          * 10;
      detector->useMechanicalStructure = uiMainWindow
          ->checkBox_PolGPD_SiRPD_MechanicalStructure->isChecked();
    }

    else if (detector->name == "HO03") {
      if (!uiMainWindow->checkBox_H3O->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HO03->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HO03_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HO03_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HO03_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HI04X1_u") {
      if (!uiMainWindow->checkBox_H4I->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HI04X1_u->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HI04X1_u_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HI04X1_u_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HI04X1_u_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HI04X1_d") {
      if (!uiMainWindow->checkBox_H4I->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HI04X1_d->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HI04X1_d_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HI04X1_d_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HI04X1_d_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04Y1_u1") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04Y1_u1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04Y1_u1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04Y1_u1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04Y1_u1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04Y1_u2") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04Y1_u2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04Y1_u2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04Y1_u2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04Y1_u2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04Y1_d1") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04Y1_d1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04Y1_d1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04Y1_d1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04Y1_d1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04Y1_d2") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04Y1_d2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04Y1_d2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04Y1_d2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04Y1_d2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04X1_u") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04X1_u->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04X1_u_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04X1_u_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04X1_u_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM04X1_d") {
      if (!uiMainWindow->checkBox_H4M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM04X1_d->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM04X1_d_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM04X1_d_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM04X1_d_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL04X1_1") {
      if (!uiMainWindow->checkBox_H4L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL04X1_1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL04X1_1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL04X1_1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL04X1_1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL04X1_2") {
      if (!uiMainWindow->checkBox_H4L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL04X1_2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL04X1_2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL04X1_2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL04X1_2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL04X1_3") {
      if (!uiMainWindow->checkBox_H4L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL04X1_3->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL04X1_3_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL04X1_3_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL04X1_3_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL04X1_4") {
      if (!uiMainWindow->checkBox_H4L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL04X1_4->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL04X1_4_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL04X1_4_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL04X1_4_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HO04") {
      if (!uiMainWindow->checkBox_H4O->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HO04->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HO04_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HO04_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HO04_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HI05X1_u") {
      if (!uiMainWindow->checkBox_H5I->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HI05X1_u->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HI05X1_u_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HI05X1_u_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HI05X1_u_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HI05X1_d") {
      if (!uiMainWindow->checkBox_H5I->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HI05X1_d->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HI05X1_d_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HI05X1_d_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HI05X1_d_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05Y1_u1") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05Y1_u1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05Y1_u1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05Y1_u1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05Y1_u1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05Y1_u2") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05Y1_u2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05Y1_u2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05Y1_u2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05Y1_u2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05Y1_d1") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05Y1_d1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05Y1_d1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05Y1_d1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05Y1_d1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05Y1_d2") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05Y1_d2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05Y1_d2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05Y1_d2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05Y1_d2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05X1_u") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05X1_u->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05X1_u_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05X1_u_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05X1_u_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HM05X1_d") {
      if (!uiMainWindow->checkBox_H5M->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HM05X1_d->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HM05X1_d_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HM05X1_d_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HM05X1_d_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL05X1_1") {
      if (!uiMainWindow->checkBox_H5L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL05X1_1->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL05X1_1_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL05X1_1_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL05X1_1_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL05X1_2") {
      if (!uiMainWindow->checkBox_H5L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL05X1_2->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL05X1_2_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL05X1_2_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL05X1_2_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL05X1_3") {
      if (!uiMainWindow->checkBox_H5L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL05X1_3->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL05X1_3_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL05X1_3_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL05X1_3_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HL05X1_4") {
      if (!uiMainWindow->checkBox_H5L->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HL05X1_4->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HL05X1_4_X->value()
          * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HL05X1_4_Y->value()
          * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HL05X1_4_Z->value()
          * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HK01X1__") {
      if (!uiMainWindow->checkBox_HK->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HK01->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HK01_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HK01_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HK01_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HK02X1__") {
      if (!uiMainWindow->checkBox_HK->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HK02->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HK02_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HK02_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HK02_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HK03X1__") {
      if (!uiMainWindow->checkBox_HK->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HK03->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HK03_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HK03_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HK03_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "SANDWICHVETO") {
      detector->useDetector = uiMainWindow->checkBox_SandwichVeto->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_SandwichVeto_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_SandwichVeto_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_SandwichVeto_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if(detector->name == "MultiplicityCounter") {
      if (!uiMainWindow->checkBox_Multiplicity->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_Multiplicity->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_Multiplicity_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_Multiplicity_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_Multiplicity_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HH02R1__") {
      if (!uiMainWindow->checkBox_HH02->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_Multiplicity->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HH02_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HH02_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HH02_Z->value() * 10;
      detector->useMechanicalStructure = false;
    } else if (detector->name == "HH03R1__") {
      if (!uiMainWindow->checkBox_HK->isChecked()) continue;
      detector->useDetector = uiMainWindow->checkBox_HH03->isChecked();
      detector->position[0] = uiMainWindow->doubleSpinBox_HH03_X->value() * 10;
      detector->position[1] = uiMainWindow->doubleSpinBox_HH03_Y->value() * 10;
      detector->position[2] = uiMainWindow->doubleSpinBox_HH03_Z->value() * 10;
      detector->useMechanicalStructure = false;
    }

    else {
      std::cerr
          << "Error in T4ISetStructManager::readoutSettings. Unknown DETECTOR name: "
          << detector->name << std::endl;
    }
  }
}

void T4ISettingsFile::setStructDetectorRes(void)
{
  for (unsigned int i = 0; i < structM->getDetectorRes()->size(); i++) {
    T4SDetectorRes* detectorRes = &structM->getDetectorRes()->at(i);

    // H1
    if (detectorRes->general.name == "HG01Y") {
      detectorRes->general.useDetector = uiMainWindow->checkBox_H1->isChecked();
      detectorRes->general.position[0] =
          uiMainWindow->doubleSpinBox_H1_X->value() * 10;
      detectorRes->general.position[1] =
          uiMainWindow->doubleSpinBox_H1_Y->value() * 10;
      detectorRes->general.position[2] =
          uiMainWindow->doubleSpinBox_H1_Z->value() * 10;
      detectorRes->general.useMechanicalStructure = uiMainWindow
          ->checkBox_H1_MechStructure->isChecked();
      detectorRes->useOptical =
          uiMainWindow->checkBox_H1_useOptical->isChecked();
    }

    // H2
    else if (detectorRes->general.name == "HG02Y") {
      detectorRes->general.useDetector = uiMainWindow->checkBox_H2->isChecked();
      detectorRes->general.position[0] =
          uiMainWindow->doubleSpinBox_H2_X->value() * 10;
      detectorRes->general.position[1] =
          uiMainWindow->doubleSpinBox_H2_Y->value() * 10;
      detectorRes->general.position[2] =
          uiMainWindow->doubleSpinBox_H2_Z->value() * 10;
      detectorRes->general.useMechanicalStructure = uiMainWindow
          ->checkBox_H2_MechStructure->isChecked();
      detectorRes->useOptical =
          uiMainWindow->checkBox_H2_useOptical->isChecked();
    }

    // VETO
    else if (detectorRes->general.name == "VETO") {
      if (!uiMainWindow->checkBox_Veto->isChecked()) continue;
      detectorRes->general.useDetector =
          uiMainWindow->checkBox_Veto_unkn->isChecked();
      detectorRes->general.position[0] = uiMainWindow->doubleSpinBox_Veto_unkn_X
          ->value() * 10;
      detectorRes->general.position[1] = uiMainWindow->doubleSpinBox_Veto_unkn_Y
          ->value() * 10;
      detectorRes->general.position[2] = uiMainWindow->doubleSpinBox_Veto_unkn_Z
          ->value() * 10;
      detectorRes->general.useMechanicalStructure = uiMainWindow
          ->checkBox_Veto_unkn_MechStructure->isChecked();
      detectorRes->useOptical = uiMainWindow->checkBox_Veto_unkn_useOptical
          ->isChecked();
    }
  }
}

void T4ISettingsFile::setStructPolGPD(void)
{
  T4SPolGPD* out = structM->getPolGPD();
  out->general.useDetector =
      uiMainWindow->checkBox_PolGPD->isChecked();
  out->general.position[0] = uiMainWindow->doubleSpinBox_PolGPD_PosX
      ->value() * 10;
  out->general.position[1] = uiMainWindow->doubleSpinBox_PolGPD_PosY
      ->value() * 10;
  out->general.position[2] = uiMainWindow->doubleSpinBox_PolGPD_PosZ
      ->value() * 10;
  out->general.useMechanicalStructure = false;

  // target
  out->target_cryostat_radius = uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_radius->value();
  out->target_cryostat_length = uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_length->value();
  out->target_cryostat_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_cryostat_thickness->value();

  out->target_aluminium_radius = uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_radius->value();
  out->target_aluminium_length = uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_length->value();
  out->target_aluminium_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_aluminium_thickness->value();

  out->target_coils_radius = uiMainWindow->doubleSpinBox_PolGPD_target_coils_radius->value();
  out->target_coils_length = uiMainWindow->doubleSpinBox_PolGPD_target_coils_length->value();
  out->target_coils_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_coils_thickness->value();

  out->target_mylar_window_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_mylar_window_thickness->value();

  out->target_length = uiMainWindow->doubleSpinBox_PolGPD_target_length->value();
  out->target_lhe_radius = uiMainWindow->doubleSpinBox_PolGPD_target_lhe_radius->value();
  out->target_nh3_radius = uiMainWindow->doubleSpinBox_PolGPD_target_nh3_radius->value();
  out->target_fiber_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_fiber_thickness->value();
  out->target_kevlar_thickness = uiMainWindow->doubleSpinBox_PolGPD_target_kevlar_thickness->value();

  // micromegas
  out->mm_length = uiMainWindow->doubleSpinBox_PolGPD_mm_length->value();
  out->mm_radius = uiMainWindow->doubleSpinBox_PolGPD_mm_radius->value();
  out->mm_layer_thickness = uiMainWindow->doubleSpinBox_PolGPD_mm_layer_thickness->value();
  out->mm_layer_distance = uiMainWindow->doubleSpinBox_PolGPD_mm_layer_distance->value();
  out->mm_zPosOffset = uiMainWindow->doubleSpinBox_PolGPD_mm_zPosOffset->value();

  // ring B
  out->ringB_length = uiMainWindow->doubleSpinBox_PolGPD_ringB_length->value();
  out->ringB_radius = uiMainWindow->doubleSpinBox_PolGPD_ringB_radius->value();
  out->ringB_zPosOffset = uiMainWindow->doubleSpinBox_PolGPD_ringB_zPosOffset->value();
}

void T4ISettingsFile::setStructScifiTest(void)
{
  T4SSciFiTest* scifiTest = structM->getSciFiTest();
  scifiTest->general.useDetector =
      uiMainWindow->checkBox_SCIFI_TEST->isChecked();
  scifiTest->general.position[0] = uiMainWindow->doubleSpinBox_SCIFI_TEST_PosX
      ->value() * 10;
  scifiTest->general.position[1] = uiMainWindow->doubleSpinBox_SCIFI_TEST_PosY
      ->value() * 10;
  scifiTest->general.position[2] = uiMainWindow->doubleSpinBox_SCIFI_TEST_PosZ
      ->value() * 10;
  scifiTest->general.useMechanicalStructure = false;
  scifiTest->length = uiMainWindow->doubleSpinBox_SCIFI_TEST_Length->value()
      * 10;
}

void T4ISettingsFile::setStructDummy(void)
{
  if (!uiMainWindow->checkBox_TestSetupDummy->isChecked())
    return;

  T4SDummy* dummy = structM->addDummy();
  dummy->general.useDetector = true;
  dummy->general.name = "DUMMY";
  dummy->general.position[0] = uiMainWindow->doubleSpinBox_DUMMY_PosX->value() * 10;
  dummy->general.position[1] = uiMainWindow->doubleSpinBox_DUMMY_PosY->value() * 10;
  dummy->general.position[2] = uiMainWindow->doubleSpinBox_DUMMY_PosZ->value() * 10;
  dummy->general.useMechanicalStructure = uiMainWindow->checkBox_DUMMY_MechanicalStructure;
  dummy->planeSize[0] = uiMainWindow->doubleSpinBox_DUMMY_SizeX->value() * 10;
  dummy->planeSize[1] = uiMainWindow->doubleSpinBox_DUMMY_SizeY->value() * 10;
  dummy->planeSize[2] = uiMainWindow->doubleSpinBox_DUMMY_SizeZ->value() * 10;

  for (unsigned int i = 0; i < other_dummies.size(); i++)
    structM->addDummy(other_dummies.at(i));
}

void T4ISettingsFile::setStructUser(void)
{
  structM->getUser()->position[0] = uiUser->doubleSpinBox_PosX->value() * 10;
  structM->getUser()->position[1] = uiUser->doubleSpinBox_PosY->value() * 10;
  structM->getUser()->position[2] = uiUser->doubleSpinBox_PosZ->value() * 10;
  structM->getUser()->momentum[0] = uiUser->doubleSpinBox_Px->value();
  structM->getUser()->momentum[1] = uiUser->doubleSpinBox_Py->value();
  structM->getUser()->momentum[2] = uiUser->doubleSpinBox_Pz->value();
  structM->getUser()->useRandomEnergy = uiUser->checkBox_useRandomEnergy
      ->isChecked();

  double min = uiUser->doubleSpinBox_RandomEnergyMin->value();
  double max = uiUser->doubleSpinBox_RandomEnergyMax->value();

  if (max >= min) {
    structM->getUser()->minEnergy = min;
    structM->getUser()->maxEnergy = max;
  } else {
    structM->getUser()->minEnergy = max;
    structM->getUser()->maxEnergy = min;
  }
}

void T4ISettingsFile::setStructCosmics(void)
{
  structM->getCosmic()->angleVariation = uiCosmics->doubleSpinBox_AngleVariation
      ->value();
  structM->getCosmic()->variationX = uiCosmics->doubleSpinBoxVariationX->value()
      * 10;
  structM->getCosmic()->variationZ = uiCosmics->doubleSpinBoxVariationZ->value()
      * 10;
  structM->getCosmic()->positionX = uiCosmics->doubleSpinBox_PosX->value() * 10;
  structM->getCosmic()->positionZ = uiCosmics->doubleSpinBox_PosY->value() * 10;
}

void T4ISettingsFile::setStructEcalCalib(void)
{
  structM->getEcalCalib()->positionZ = uiEcalCalib->doubleSpinBox_PosZ->value() * 10;
  structM->getEcalCalib()->positionXMin = uiEcalCalib->doubleSpinBox_VariationXMin->value() * 10;
  structM->getEcalCalib()->positionXMax = uiEcalCalib->doubleSpinBox_VariationXMax->value() * 10;
  structM->getEcalCalib()->positionYMin = uiEcalCalib->doubleSpinBox_VariationYMin->value() * 10;
  structM->getEcalCalib()->positionYMax = uiEcalCalib->doubleSpinBox_VariationYMax->value() * 10;
  structM->getEcalCalib()->energyMin = uiEcalCalib->doubleSpinBox_RandomEnergyMin->value() * 1000;
  structM->getEcalCalib()->energyMax = uiEcalCalib->doubleSpinBox_RandomEnergyMax->value() * 1000;
}


void T4ISettingsFile::setStructHEPGen(void)
{
  structM->getHEPGen()->target = uiHEPGen->comboBox_Target->currentText()
      .toStdString();

  std::string ivecm = uiHEPGen->comboBox_IVECM->currentText().toStdString();
  if (ivecm == "0: DVCS") {
    structM->getHEPGen()->IVECM = 0;
  } else if (ivecm == "1: Pi0 -> yy") {
    structM->getHEPGen()->IVECM = 1;
  } else if (ivecm == "2: Rho0 -> Pi Pi") {
    structM->getHEPGen()->IVECM = 2;
  } else if (ivecm == "3: Phi -> K K") {
    structM->getHEPGen()->IVECM = 3;
  } else if (ivecm == "4: J/Psi -> e+ e- and mu+ mu-") {
    structM->getHEPGen()->IVECM = 4;
  } else if (ivecm == "6: Omega-> Pi Pi Pi") {
    structM->getHEPGen()->IVECM = 6;
  } else if (ivecm == "7: Rho+ -> Pi yy") {
    structM->getHEPGen()->IVECM = 7;
  } else if (ivecm == "8: Omega -> y Pi0") {
    structM->getHEPGen()->IVECM = 8;
  } else {
    std::cerr
        << "Error in T4ISettingsFile::setHEPGen(void). IVECM not identified, set to 0."
        << std::endl;
    structM->getHEPGen()->IVECM = 0;
  }

  if (uiHEPGen->checkBox_diffractiveDissociation->isChecked()) {
    structM->getHEPGen()->LST = 1;
  } else {
    structM->getHEPGen()->LST = 0;
  }

  structM->getHEPGen()->CUT[0] = uiHEPGen->doubleSpinBox_x_min->value();
  structM->getHEPGen()->CUT[1] = uiHEPGen->doubleSpinBox_x_max->value();
  structM->getHEPGen()->CUT[2] = uiHEPGen->doubleSpinBox_y_min->value();
  structM->getHEPGen()->CUT[3] = uiHEPGen->doubleSpinBox_y_max->value();
  structM->getHEPGen()->CUT[4] = uiHEPGen->doubleSpinBox_Q2_min->value();
  structM->getHEPGen()->CUT[5] = uiHEPGen->doubleSpinBox_Q2_max->value();
  structM->getHEPGen()->CUT[6] = uiHEPGen->doubleSpinBox_W2_min->value();
  structM->getHEPGen()->CUT[7] = uiHEPGen->doubleSpinBox_W2_max->value();
  structM->getHEPGen()->CUT[8] = uiHEPGen->doubleSpinBox_nu_min->value();
  structM->getHEPGen()->CUT[9] = uiHEPGen->doubleSpinBox_nu_max->value();
  structM->getHEPGen()->CUT[10] = uiHEPGen->doubleSpinBox_E_min->value();
  structM->getHEPGen()->CUT[11] = uiHEPGen->doubleSpinBox_E_max->value();
  structM->getHEPGen()->CUT[12] = uiHEPGen->doubleSpinBox_phi_min->value();
  structM->getHEPGen()->CUT[13] = uiHEPGen->doubleSpinBox_phi_max->value();

  structM->getHEPGen()->THMAX = uiHEPGen->doubleSpinBox_MACC->value();
  structM->getHEPGen()->alf = uiHEPGen->doubleSpinBox_ALFA->value();
  structM->getHEPGen()->atomas = uiHEPGen->doubleSpinBox_TPAR_A->value();
  structM->getHEPGen()->probc = uiHEPGen->doubleSpinBox_TPAR_probc->value();
  structM->getHEPGen()->bcoh = uiHEPGen->doubleSpinBox_TPAR_bcoh->value();
  structM->getHEPGen()->bin = uiHEPGen->doubleSpinBox_TPAR_bin->value();
  structM->getHEPGen()->clept = uiHEPGen->doubleSpinBox_BPAR_charge->value();
  structM->getHEPGen()->slept =
      uiHEPGen->doubleSpinBox_BPAR_polarisation->value();
  structM->getHEPGen()->B0 = uiHEPGen->doubleSpinBox_DVCS_B0->value();
  structM->getHEPGen()->xbj0 = uiHEPGen->doubleSpinBox_DVCS_xbj0->value();
  structM->getHEPGen()->alphap = uiHEPGen->doubleSpinBox_DVCS_alphap->value();
  structM->getHEPGen()->TLIM[0] = uiHEPGen->doubleSpinBox_TLIM1->value();
  structM->getHEPGen()->TLIM[1] = uiHEPGen->doubleSpinBox_TLIM2->value();
  structM->getHEPGen()->usePi0_transv_table = uiHEPGen->checkBox_usePi_transv_table->isChecked();
}

void T4ISettingsFile::setStructPrimGen(void)
{
  structM->getPrimGen()->Z = uiPrimGen->doubleSpinBox_Z->value();
  structM->getPrimGen()->particle = uiPrimGen->comboBox_Particle->currentIndex() == 0 ? "pion" : "muon";
  structM->getPrimGen()->alpha_1 = uiPrimGen->doubleSpinBox_alpha_1->value();
  structM->getPrimGen()->beta_1 = uiPrimGen->doubleSpinBox_beta_1->value();
  structM->getPrimGen()->alpha_2 = uiPrimGen->doubleSpinBox_alpha_2->value();
  structM->getPrimGen()->beta_2 = uiPrimGen->doubleSpinBox_beta_2->value();
  structM->getPrimGen()->s_min = uiPrimGen->doubleSpinBox_s_min->value();
  structM->getPrimGen()->s_max = uiPrimGen->doubleSpinBox_s_max->value();
  structM->getPrimGen()->Q2_max = uiPrimGen->doubleSpinBox_Q2_max->value();
  structM->getPrimGen()->Egamma_min = uiPrimGen->doubleSpinBox_Egamma_min->value();
  structM->getPrimGen()->pbeam_min = uiPrimGen->doubleSpinBox_pbeam_min->value();
  structM->getPrimGen()->pbeam_max = uiPrimGen->doubleSpinBox_pbeam_max->value();
  structM->getPrimGen()->born = uiPrimGen->CheckboxBorn->isChecked();
  structM->getPrimGen()->rhoContrib = uiPrimGen->CheckboxRho->isChecked();
  structM->getPrimGen()->chiralLoops = uiPrimGen->CheckboxChiralLoops->isChecked();
  structM->getPrimGen()->polarizabilities = uiPrimGen->Checkboxpolarizabilities->isChecked();
  structM->getPrimGen()->radcorr = uiPrimGen->CheckboxRadCorr->isChecked();
}

void T4ISettingsFile::setStructPrimTrigger(void)
{
  structM->getPrimTrig()->threshPrim1 = uiPrimTrig->doubleSpinBox_ThresholdPrim1->value();
  structM->getPrimTrig()->threshPrim2 = uiPrimTrig->doubleSpinBox_ThresholdPrim2_Maximum->value();
  structM->getPrimTrig()->threshPrim2Veto = uiPrimTrig->doubleSpinBox_ThresholdPrim2_Veto->value();
  structM->getPrimTrig()->threshPrim3 = uiPrimTrig->doubleSpinBox_ThresholdPrim3->value();
}

void T4ISettingsFile::setStructSampling(void)
{
  T4SSampling* sampling = structM->getSampling();
  sampling->baseline = uiPhysicsList->doubleSpinBox_GANDALFbaseline->value();
  sampling->useBaseline = uiPhysicsList->checkBox_GANDALFbaseline->isChecked();
  sampling->windowSize = uiPhysicsList->spinBox_windowSizeGandalf->value();
  if (uiPhysicsList->radioButton_GANDALF_1GHZ->isChecked())
    sampling->binsPerNanoSecond = 1;
  else
    sampling->binsPerNanoSecond = 2;
}

void T4ISettingsFile::setStructFieldMapPath(std::string name, std::string path)
{
  bool success = false;
  for (unsigned int i = 0; i < structM->getMagnet()->size(); i++) {
    if (structM->getMagnet()->at(i).general.name == name) {
      structM->getMagnet()->at(i).fieldmapPath = path;
      success = true;
      break;
    }
  }

  if (!success) {
    std::cerr << "T4ISettingsFile::setFieldMapPath - Unknown magnet."
        << std::endl;
  }
}
