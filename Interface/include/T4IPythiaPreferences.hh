#ifndef T4IPYTHIAPREFS_HH_
#define T4IPYTHIAPREFS_HH_


#include "ui_Preferences-Pythia.h"


#include <QDebug>
#include <QFileDialog>

#include "T4PythiaTuning.hh"
#include "T4SStructManager.hh"
#include "T4ISettingsFile.hh"



class T4IPythiaGui :public QDialog, public Ui::pythiaPrefs 
{  
  Q_OBJECT
public:
    T4IPythiaGui(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~T4IPythiaGui();
    void setStructMan(T4SStructManager* _structMan);
    
public slots:
  void readFile(void);
  void writeFile(void); 
    
private:
  void showEvent(QShowEvent* event);
  
  
  T4SStructManager* myStructMan;
  T4PythiaTuning* myTuning;
  int selRowPDF,selRowMSBD,selRowMDME;
  QString envTGEANT;
private slots:
  void clearStructMan(void);
  void clearInterface(void);
  void setStructManFromFile(void);
  void setStructManFromInterface(void);
  void setInterfaceFromStructMan(void);
  void writeOutStructMan(void);
  
  void editPDF(int, int);
  void editMSBD(int, int);
  void editMDME(int, int);
  
  void savePDF(void);
  void saveMBSD(void);
  void saveMDME(void);
  
  void insertPDF(void);
  void insertMSBD(void);
  void insertMDME(void);
  
  void deletePDF(void);
  void deleteMSBD(void);
  void deleteMDME(void);
  
  
  
  
  
};




#endif