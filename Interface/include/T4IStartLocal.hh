#ifndef T4ISTARTLOCAL_HH_
#define T4ISTARTLOCAL_HH_

#include "qthread.h"

#include "include/T4ISettingsFile.hh"
#include "ui_MainWindow.h"

class T4IStartLocal : public QThread
{
  Q_OBJECT
  public:
    T4IStartLocal(T4ISettingsFile*, Ui::MainWindow*);
    virtual ~T4IStartLocal(void) {}

  private slots:
    void buttonStart(void);

  private:
    void run();
    Ui::MainWindow* uiMainWindow;
    T4ISettingsFile* settingsFile;
};

#endif /* T4ISTARTLOCAL_HH_ */
