#ifndef T4ITRIGGER_HH_
#define T4ITRIGGER_HH_

#include "qwidget.h"
#include "qfiledialog.h"

#include "include/T4ISettingsFile.hh"

#include "ui_MainWindow.h"
#include "ui_Preferences-Trigger-Primakoff.h"

#include "T4SGlobals.hh"

class T4ITrigger : public QWidget
{
  Q_OBJECT
  public:
    T4ITrigger(T4ISettingsFile*, Ui::MainWindow*,
        Ui::PreferencesTriggerPrimakoff*);
    virtual ~T4ITrigger() {}

  public slots:
    void slotTrigger(void);
  private slots:
    void slotPrimakoff(void);
    void seletPrimakoff(void);
    void cancel(void);


  private:
  T4ISettingsFile* settingsFile;

  Ui::MainWindow* uiMainWindow;
  Ui::PreferencesTriggerPrimakoff* uiPrimTrig;

  QWidget* menuPrimTrigger;

};
#endif
