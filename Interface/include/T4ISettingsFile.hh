#ifndef T4ISETTINGSFILE_HH_
#define T4ISETTINGSFILE_HH_

#include "qstring.h"
#include <string>
#include <fstream>
#include <iostream>
#include <time.h>

#include "ui_MainWindow.h"
#include "ui_Preferences-Generator-Hepgen.h"
#include "ui_Preferences-Generator-Primakoff.h"
#include "ui_Preferences-Trigger-Primakoff.h"
#include "ui_Preferences-User.h"
#include "ui_Preferences-Cosmics.h"
#include "ui_Preferences-EcalCalib.h"
#include "ui_Preferences-PhysicsList.h"

#include "T4SStructManager.hh"
#include "T4SFileBackEnd.hh"
#include "T4SGlobals.hh"
#include "T4SAlignment.hh"
#include "T4SSettingsFileXML.hh"

class T4ISettingsFile
{
  public:
    T4ISettingsFile(Ui::MainWindow*, Ui::PreferencesBeamUser*,
        Ui::PreferencesBeamCosmics*, Ui::PreferencesBeamEcalCalib*, Ui::PreferencesGeneratorHEPGEN*,
        Ui::PreferencesGeneratorPrimGen*, Ui::PreferencesPhysicsList*, Ui::PreferencesTriggerPrimakoff*);
    ~T4ISettingsFile(void);

    void saveFile(void);
    void saveFile(std::string savePath);

    void loadFile(std::string loadPath);
    void loadDetectorsDat(void);

    void setStructUser(void);
    void setStructCosmics(void);
    void setStructEcalCalib(void);
    void setStructHEPGen(void);
    void setStructPrimGen(void);
    void setStructPrimTrigger(void);
    void setStructSampling(void);
    void setStructPaths(void);
    
    void setStructFieldMapPath(std::string name, std::string path);
    void deactivateVisualization(void);

    void setInterfaceUser(void);
    void setInterfaceCosmic(void);
    void setInterfaceEcalCalib(void);
    void setInterfaceHEPGen(void);
    void setInterfacePrimGen(void);
    void setInterfaceSampling(void);
    void setInterfacePaths(void);
    void setInterfacePrimTrigger(void);

    T4SStructManager* getT4SStructManager(void) {return structM;};

  private:
    T4SStructManager* structM;
    T4SSettingsFileXML* t4SSettingsFileXML;
    
    T4SAlignment* t4SAlignment;

    Ui::MainWindow* uiMainWindow;
    Ui::PreferencesBeamUser* uiUser;
    Ui::PreferencesBeamCosmics* uiCosmics;
    Ui::PreferencesBeamEcalCalib* uiEcalCalib;
    Ui::PreferencesGeneratorHEPGEN* uiHEPGen;
    Ui::PreferencesGeneratorPrimGen* uiPrimGen;
    Ui::PreferencesPhysicsList* uiPhysicsList;
    Ui::PreferencesTriggerPrimakoff* uiPrimTrig;

    std::string fileName;

    std::string getBeamPlugin(void);
    std::string getString(QString);

    // read values from the GUI and fill the structManager
    void setStructManager(void);
    void setStructGeneral(void);
    void setStructBeam(void);
    void setStructCalorimeter(void);
    void setStructMW1(void);
    void setStructMW2(void);
    void setStructST02(void);
    void setStructST03(void);
    void setStructST05(void);
    void setStructMicromegas(void);
    void setStructGem(void);
    void setStructSciFi(void);
    void setStructSilicon(void);
    void setStructMWPC(void);
    void setStructVeto(void);
    void setStructDC00(void);
    void setStructDC01(void);
    void setStructDC04(void);
    void setStructDC05(void);
    void setStructW45(void);
    void setStructRichWall(void);
    void setStructRPD(void);
    void setStructMagnet(void);
    void setStructRICH(void);
    void setStructDetector(void);
    void setStructDetectorRes(void);
    void setStructPolGPD(void);
    void setStructScifiTest(void);
    void setStructDummy(void);

    // read values from settings.conf or detectors.dat file and write them to the GUI
    void setInterface(void);
    void setInterfaceGeneral(void);
    void setInterfaceBeam(void);
    void setInterfaceCalorimeter(void);
    void setInterfaceMW1(void);
    void setInterfaceMW2(void);
    void setInterfaceStraw(void);
    void setInterfaceMicromegas(void);
    void setInterfaceGem(void);
    void setInterfaceSciFi(void);
    void setInterfaceSilicon(void);
    void setInterfaceMWPC(void);
    void setInterfaceVeto(void);
    void setInterfaceDC(void);
    void setInterfaceW45(void);
    void setInterfaceRichWall(void);
    void setInterfaceRPD(void);
    void setInterfaceMagnet(void);
    void setInterfaceRICH(void);
    void setInterfaceDetector(void);
    void setInterfaceDetectorRes(void);
    void setInterfacePolGPD(void);
    void setInterfaceScifiTest(void);
    void setInterfaceDUMMY(void);
    vector<T4SDummy> other_dummies;

    void setDetDatAlignment(void);
};

#endif /* T4ISETTINGSFILE_HH_ */
