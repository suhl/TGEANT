#ifndef T4IEFFIC_HH_
#define T4IEFFIC_HH_


#include "ui_Preferences-Efficiencies.h"


#include <QDebug>
#include <QFileDialog>

#include "T4SEfficiency.hh"
#include "T4SStructManager.hh"
#include "T4ISettingsFile.hh"


class T4IEfficiencies :public QDialog, public Ui::efficiencyDialog
{
  Q_OBJECT
  
  public:
    T4IEfficiencies(QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~T4IEfficiencies();
    void setStructMan(T4SStructManager* _structMan);
    
public slots:
  void readFile(void);
  void writeFile(void); 
  void importDetDat(void);
    
private:
  void showEvent(QShowEvent* event);
  
  T4SStructManager* myStructMan;
  T4SEfficiency* myEffic;
  QString envTGEANT;
  int selDet,selMag;

private slots:
  void clearStructMan(void);
  void clearInterface(void);
  void setStructManFromInterface(void);
  void setInterfaceFromStructMan(void);
  void writeOutStructMan(void);
  
  void editDetEff(int, int);
  void saveDet(void);
  void insertDet(void);
  void deleteDet(void);
  
  void editMagEff(int, int);
  void saveMag(void);
  void insertMag(void);
  void deleteMag(void);


};




#endif