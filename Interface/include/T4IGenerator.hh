#ifndef T4IGENERATOR_HH_
#define T4IGENERATOR_HH_

#include "qwidget.h"
#include "qfiledialog.h"

#include "include/T4ISettingsFile.hh"

#include "ui_MainWindow.h"
#include "ui_Preferences-Generator-Hepgen.h"
#include "ui_Preferences-Generator-Primakoff.h"
#include "ui_Preferences-User.h"
#include "ui_Preferences-Cosmics.h"
#include "ui_Preferences-EcalCalib.h"
#include "T4IPythiaPreferences.hh"

#include "T4SGlobals.hh"

class T4IGenerator : public QWidget
{
  Q_OBJECT
  public:
    T4IGenerator(T4ISettingsFile*, Ui::MainWindow*, Ui::PreferencesBeamUser*,
        Ui::PreferencesBeamCosmics*, Ui::PreferencesBeamEcalCalib*,
        Ui::PreferencesGeneratorHEPGEN*, Ui::PreferencesGeneratorPrimGen*, T4IPythiaGui*);
    virtual ~T4IGenerator(void) {};

  public slots:
    void slotGenerator(void);

  private slots:
    void slotPythia(void);
    void slotHEPGen(void);
    void slotPrimakoff(void);
    void slotPrimaUpdatePol(double);
    void slotUser(void);
    void slotUserEnergy(void);
    void slotCosmics(void);
    void slotEcalCalib(void);

    void selectHEPGen(void);
    void selectPrimGen(void);
    void selectUser(void);
    void selectCosmics(void);
    void selectEcalCalib(void);
    void cancel(void);

    void slotSelectLocalGeneratorFile(void);
    void slotBeamfile(void);
    void slotBeamfileType(void);
    void slotSelectBeamfile(void);
    void slotSelectBeamfile2(void);

    void beamModeBeamOnly(void);
    void beamModeVisualization(void);
    void beamModeComboBox(void);
    void beamModeHepgen(void);
    void beamModePythia(void);
    void beamModePythia8(void);
    void beamModeLepto(void);
    void beamModePrimakoff(void);
    void beamModeASCII(void);

  private:
    T4ISettingsFile* settingsFile;

    Ui::MainWindow* uiMainWindow;
    Ui::PreferencesBeamUser* uiUser;
    Ui::PreferencesBeamCosmics* uiCosmics;
    Ui::PreferencesBeamEcalCalib* uiEcalCalib;
    Ui::PreferencesGeneratorHEPGEN* uiHEPGen;
    Ui::PreferencesGeneratorPrimGen* uiPrimGen;
    T4IPythiaGui* uiPythia;

    void enableSelection(bool);
    void useBeamfile(bool);
    void useVisualizationMode(bool);
    void useGeneratorFile(bool);

    QWidget* menuHEPGen;
    QWidget* menuPrimGen;
    QWidget* menuPythia;
    QWidget* menuUser;
    QWidget* menuCosmics;
    QWidget* menuEcalCalib;
};

#endif /* T4IGENERATOR_HH_ */
