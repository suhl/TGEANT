#ifndef T4IMAINMENU_HH_
#define T4IMAINMENU_HH_

#include "qwidget.h"
#include "qfiledialog.h"
#include "qtimer.h"
#include "qevent.h"
#include <QDebug>
#include <unistd.h>
#include <sys/stat.h>

#include "ui_MainWindow.h"
#include "ui_Preferences-Cosmics.h"
#include "ui_Preferences-User.h"
#include "ui_Preferences-Generator-Hepgen.h"
#include "ui_Preferences-PhysicsList.h"

#include "T4IEfficiencies.hh"

#include "include/T4ISettingsFile.hh"
#include "include/T4IGenerator.hh"
#include "include/T4ITrigger.hh"

#include <iostream>
#include <sstream>
#include "qrgb.h"
#include "qthread.h"

class T4IMainMenu : public QWidget
{
  Q_OBJECT
  public:
    T4IMainMenu(T4ISettingsFile*, Ui::MainWindow*, Ui::PreferencesPhysicsList*);
    virtual ~T4IMainMenu(void);

  private slots:
    // main
    void buttonSave(void);
    void buttonLoad(void);
    void buttonLoadLast(void);
    void buttonClose(void);

    // file dialog
    void slotSelectOutputPath(void);
    void slotSelectDetectorsDat(void);
    void slotSelectSM1(void);
    void slotSelectSM2(void);
    void slotSelectTargetField(void);
    void slotSelectRecoilCalibrationFile(void);
    void slotSelectCaloCalib(void);
    void slotSelectlibHEPGen_Pi(void);
    void slotSelectlibHEPGen_Pi_transv(void);
    void slotSelectlibHEPGen_Rho(void);
    void slotSelectVisualizationMacro(void);
    void slotSelectDetectorEfficiency(void);
    void slotSelectmtxInnerX(void);
    void slotSelectmtxLadderX(void);
    void slotSelectmtxLAST(void);
    void slotSelectmtxMiddleX(void);
    void slotSelectmtxMiddleY(void);
    void slotSelectmtxOuterY(void);
    
    void slotShowEfficGui(void);

    // general
    void slotRestoreFileFromSeed(void);
    void slotSelectTriggerPlugin(int idx);

    // detectors
    void slotTarget(void);
    void slotRecoil(void);
    void buttonSelectAll(void) {setAll(true);}
    void buttonSelectNone(void) {setAll(false);}
    void geometries(void);
    void slotW45(void);
    void slotHodoscopes(void);
    void buttonLoadDetectorsDat(void);

    // physicsList
    void slotPhysicsList(void);
    void buttonPhysicsList(void);
    void selectPhysicsList(void);
    void cancelPhysicsList(void);
    void cutsPhysicsList(void);
    void baseLinePL(void);

    // sm2
    void buttonSM2Field(void);

  private:
    QString envTGEANT;

    Ui::MainWindow* uiMainWindow;
    Ui::PreferencesPhysicsList* uiPhysicsList;
    T4IEfficiencies* efficWindow;
    QWidget* menuPhysicsList;

    T4ISettingsFile* settingsFile;

    void connectGeometries(void);
    void setAll(bool);
};

#endif /* T4IMAINMENU_HH_ */
