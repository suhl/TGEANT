
QMAKE_LFLAGS_RPATH=

TEMPLATE = app thread
CONFIG -= moc
win32: DEFINES +=  _CRT_SECURE_NO_WARNINGS

# libT4Settings.so
LIBS += -L$$(TGEANT)/lib/ -lT4Settings -lxerces-c
INCLUDEPATH += "$$(TGEANT)/include"
HEADERS += $$(TGEANT)/include/T4SFileBackEnd.hh
HEADERS += $$(TGEANT)/include/T4SGlobals.hh
HEADERS += $$(TGEANT)/include/T4SStructManager.hh

# Interface
SOURCES += src/Interface.cc
SOURCES += src/T4IMainMenu.cc
SOURCES += src/T4ISettingsFile.cc
SOURCES += src/T4ISetStructManager.cc
SOURCES += src/T4ISetInterface.cc
SOURCES += src/T4IGenerator.cc
SOURCES += src/T4IStartLocal.cc
SOURCES += src/T4IPythiaPreferences.cc
SOURCES += src/T4IEfficiencies.cc
SOURCES += src/T4ITrigger.cc

HEADERS += include/T4IMainMenu.hh
HEADERS += include/T4ISettingsFile.hh
HEADERS += include/T4IGenerator.hh
HEADERS += include/T4IStartLocal.hh
HEADERS += include/T4IPythiaPreferences.hh
HEADERS += include/T4IEfficiencies.hh
HEADERS += include/T4ITrigger.hh

FORMS += forms/MainWindow.ui
FORMS += forms/Preferences-Trigger-Primakoff.ui
FORMS += forms/Preferences-Generator-Primakoff.ui
FORMS += forms/Preferences-Cosmics.ui
FORMS += forms/Preferences-User.ui
FORMS += forms/Preferences-EcalCalib.ui
FORMS += forms/Preferences-Generator-Hepgen.ui
FORMS += forms/Preferences-Pythia.ui
FORMS += forms/Preferences-PhysicsList.ui
FORMS += forms/Preferences-Efficiencies.ui
FORMS += forms/Preferences-Trigger-Primakoff.ui

#The following line was inserted by qt3to4
QT +=  qt3support
