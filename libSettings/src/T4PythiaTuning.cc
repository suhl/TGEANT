#include "T4PythiaTuning.hh"


void T4PythiaTuning::readXML(string _inFile)
{
    myParser = new T4XMLParser();

    if (!myParser->parseFile(_inFile)) {
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
                __FILE__, "T4PythiaTuning could not parse file: '" + _inFile + "'");
        return;
    }
    T4SPythia* pythia = structMan->getPythia();
    
    
    
    
    if (myParser->getBaseNodeCount("PYTHIA6_GeneralSettings") > 0) {
        myParser->getChildValue("PYTHIA6_GeneralSettings",0,"target",structMan->getPythia()->target);
        myParser->getChildValue("PYTHIA6_GeneralSettings",0,"msel",structMan->getPythia()->genericFlags.msel);
        myParser->getChildValue("PYTHIA6_GeneralSettings",0,"mrpy",structMan->getPythia()->genericFlags.mrpy);
        myParser->getChildValue("PYTHIA6_GeneralSettings",0,"mrpy2",structMan->getPythia()->genericFlags.mrpy2);
    }

    for (int i = 0; i < myParser->getBaseNodeCount("PYTHIA6_MSBD"); i++) {
        T4SPythiaMSBD newMSBD;
        myParser->getChildValue("PYTHIA6_MSBD",i,"number",newMSBD.number);
        myParser->getChildValue("PYTHIA6_MSBD",i,"flag1",newMSBD.flag1);
	int type;
        myParser->getChildValue("PYTHIA6_MSBD",i,"type",type);
// 	newMSBD.type = type;
        switch(type){
	  case 0: newMSBD.type=MSTP;break;
	  case 1: newMSBD.type=MSTU;break;
	  case 2: newMSBD.type=PARU;break;
	  case 3: newMSBD.type=PARP;break;
	  case 4: newMSBD.type=CKIN;break;
	  default: T4SMessenger::getInstance()->printMessage(T4SWarning,__LINE__,__FILE__,"Failed converting pythia-flagtype to enum! Please choose valid enum int!");
	};
	
	pythia->flagsMSBD.push_back(newMSBD);
    }



    for (int i = 0; i < myParser->getBaseNodeCount("PYTHIA6_MDME"); i++) {
        T4SPythiaMDME newMDME;
        myParser->getChildValue("PYTHIA6_MDME",i,"number",newMDME.number);
        myParser->getChildValue("PYTHIA6_MDME",i,"flag1",newMDME.flag1);
        myParser->getChildValue("PYTHIA6_MDME",i,"flag2",newMDME.flag2);
        pythia->flagsMDME.push_back(newMDME);
    }
    
     for (int i = 0; i < myParser->getBaseNodeCount("PYTHIA6_PDF"); i++) {
	T4SPythiaLHAPDF newPDF;
	myParser->getChildValue("PYTHIA6_PDF",i,"pdfNum",newPDF.pdfNum);
        myParser->getChildValue("PYTHIA6_PDF",i,"pdfName",newPDF.pdfName);
        myParser->getChildValue("PYTHIA6_PDF",i,"pdfType",newPDF.pdfType);
        myParser->getChildValue("PYTHIA6_PDF",i,"pdfMemSet",newPDF.pdfMemSet);
	pythia->pdfList.push_back(newPDF);
     }

    delete myParser;
    myParser = NULL;

    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
            "T4PythiaTuning '" + _inFile + "' has been loaded.");

}

void T4PythiaTuning::writeXML(string _inFile)
{
    myWriter = new T4XMLWriter;
    myWriter->addNode("PYTHIA6_GeneralSettings");
    myWriter->createDataNode("PYTHIA6_GeneralSettings",0,"target",structMan->getPythia()->target);
    myWriter->createDataNode("PYTHIA6_GeneralSettings",0,"msel",structMan->getPythia()->genericFlags.msel);
    myWriter->createDataNode("PYTHIA6_GeneralSettings",0,"mrpy",structMan->getPythia()->genericFlags.mrpy);
    myWriter->createDataNode("PYTHIA6_GeneralSettings",0,"mrpy2",structMan->getPythia()->genericFlags.mrpy2);

    for (unsigned int i = 0; i < structMan->getPythia()->flagsMSBD.size(); i ++) {
        myWriter->addNode("PYTHIA6_MSBD");
        myWriter->createDataNode("PYTHIA6_MSBD",i,"number",structMan->getPythia()->flagsMSBD.at(i).number);
        myWriter->createDataNode("PYTHIA6_MSBD",i,"flag1",structMan->getPythia()->flagsMSBD.at(i).flag1);
        myWriter->createDataNode("PYTHIA6_MSBD",i,"type",structMan->getPythia()->flagsMSBD.at(i).type);
    }
    for (unsigned int i = 0; i < structMan->getPythia()->flagsMDME.size(); i ++) {
        myWriter->addNode("PYTHIA6_MDME");
        myWriter->createDataNode("PYTHIA6_MDME",i,"number",structMan->getPythia()->flagsMDME.at(i).number);
        myWriter->createDataNode("PYTHIA6_MDME",i,"flag1",structMan->getPythia()->flagsMDME.at(i).flag1);
        myWriter->createDataNode("PYTHIA6_MDME",i,"flag2",structMan->getPythia()->flagsMDME.at(i).flag2);
    }
    for (unsigned int i = 0; i < structMan->getPythia()->pdfList.size(); i++) {
	myWriter->addNode("PYTHIA6_PDF");
        myWriter->createDataNode("PYTHIA6_PDF",i,"pdfNum",structMan->getPythia()->pdfList.at(i).pdfNum);
        myWriter->createDataNode("PYTHIA6_PDF",i,"pdfName",structMan->getPythia()->pdfList.at(i).pdfName);
        myWriter->createDataNode("PYTHIA6_PDF",i,"pdfType",structMan->getPythia()->pdfList.at(i).pdfType);
        myWriter->createDataNode("PYTHIA6_PDF",i,"pdfMemSet",structMan->getPythia()->pdfList.at(i).pdfMemSet);
    }
    

    myWriter->createFile(_inFile);
    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
            "T4PythiaTuning '" + _inFile + "' has been created.");
    delete myWriter;
    myWriter = NULL;
}


T4PythiaTuning::~T4PythiaTuning()
{
    if (myParser != NULL)
        delete myParser;
    if (myWriter != NULL)
        delete myWriter;
}

