#include "T4SFileBackEnd.hh"

void T4SFileBackEnd::checkSettings(void)
{
  // check runName
  std::vector<std::string> name = explodeStringCustomDelim(
      structM->getGeneral()->runName, ".");
  if (name.size() == 0)
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "Error in T4SFileBackEnd: Run name does not exist!");
  else if (name.size() != 1) {
    if (name.back() == "root") {
      structM->getGeneral()->runName.erase(
          structM->getGeneral()->runName.size() - 5, 5);
      checkSettings();
    } else if (name.back() == "tgeant") {
      structM->getGeneral()->runName.erase(
          structM->getGeneral()->runName.size() - 7, 7);
      checkSettings();
    } else if (name.back() == "dat") {
      structM->getGeneral()->runName.erase(
          structM->getGeneral()->runName.size() - 4, 4);
      checkSettings();
    }
  }

  // check outputPath
  if (structM->getGeneral()->outputPath == "default") {
    structM->getGeneral()->outputPath = getenv("PWD");
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
    __FILE__,
        "Warning in T4SFileBackEnd: Output path set to 'default'. All output (if any) will be saved in the current work directory: '"
            + structM->getGeneral()->outputPath + "'.");
  }
  if (*structM->getGeneral()->outputPath.rbegin() != '/')
    structM->getGeneral()->outputPath.append("/");
  if (!dirExists(structM->getGeneral()->outputPath))
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "Error in T4SFileBackEnd: Output path does not exist!");

  // useTargetExtrap makes no sense for VisualizationMode and others...
  if (structM->getBeam()->beamPlugin == "VisualizationMode"
      || structM->getBeam()->beamPlugin == "BeamOnly"
      || structM->getBeam()->beamPlugin == "EcalCalib"
      || structM->getBeam()->beamPlugin == "ElectronBeam"
      || structM->getBeam()->beamPlugin == "EventGen"
      || structM->getBeam()->beamPlugin == "Cosmics"
      || structM->getBeam()->beamPlugin == "PhotonBeam"
      || structM->getBeam()->beamPlugin == "PhotonCone"
      || structM->getBeam()->beamPlugin == "User") {
    structM->getBeam()->useTargetExtrap = false;
  }

  // check simplifiedGeometries => only accepted for VisualizationMode
  if (structM->getBeam()->beamPlugin != "VisualizationMode")
    structM->getGeneral()->simplifiedGeometries = false;

  // check external files
  replacePathEnvTGEANT(structM->getExternal()->calorimeterInfo);
  replacePathEnvTGEANT(structM->getExternal()->beamFile);
  replacePathEnvTGEANT(structM->getExternal()->beamFileForAdditionalPileUp);
  replacePathEnvTGEANT(structM->getExternal()->visualizationMacro);
  replacePathEnvTGEANT(structM->getExternal()->detectorEfficiency);

  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixInnerX);
  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixOuterY);
  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixLadderX);
  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixMiddleX);
  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixMiddleY);
  replacePathEnvTGEANT(structM->getExternal()->triggerMatrixLAST);

  replacePathEnvTGEANT(structM->getExternal()->libHEPGen_Pi0);
  replacePathEnvTGEANT(structM->getExternal()->libHEPGen_Pi0_transv);
  replacePathEnvTGEANT(structM->getExternal()->libHEPGen_Rho);

  // camera calibration file
  for (unsigned int i = 0; i < structM->getRPD()->size(); i++) {
    if (structM->getRPD()->at(i).useCalibrationFile == true)
      replacePathEnvTGEANT(structM->getRPD()->at(i).calibrationFilePath);
  }

  // field maps
  for (unsigned int i = 0; i < structM->getMagnet()->size(); i++)
    replacePathEnvTGEANT(structM->getMagnet()->at(i).fieldmapPath);
  
  //load pythia settings from pythia settings xml
  replacePathEnvTGEANT(structM->getExternal()->localGeneratorFile);
  if (structM->getBeam()->beamPlugin == "PYTHIA") {
    T4PythiaTuning myTuning(structM);
    myTuning.readXML(structM->getExternal()->localGeneratorFile);
  }
}
