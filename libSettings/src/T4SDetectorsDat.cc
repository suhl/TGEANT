#include "T4SDetectorsDat.hh"
#include "T4SMessenger.hh"
#include "T4SEfficiency.hh"
#include "T4SCalorimeter.hh"

void T4SDetectorsDat::load(string fileName)
{
  if (fileName == "unset") {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Error in T4SDetectorsDat: Path to detectors.dat file not set.");
    return;
  }

  ifstream loadFile(fileName.c_str());

  string line;
  vector<string> originalFile;
  if (loadFile.is_open()) {
    while (loadFile.good()) {
      getline(loadFile, line);
      originalFile.push_back(line);
    }
    loadFile.close();
  } else
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__, "Error in T4SDetectorsDat: detectors.dat file not found.");

  map<int, vector<string> >* putIn;

  for (unsigned int i = 0; i < originalFile.size(); i++) {
    vector<string> boomList = explodeString(originalFile.at(i));
    if (boomList.size() == 0)
      continue;
    if (boomList.at(DETDAT_LINECODE) == "det")
      putIn = &detMap;
    else if (boomList.at(DETDAT_LINECODE) == "rpd")
      putIn = &rpdMap;
    else if (boomList.at(DETDAT_LINECODE) == "dead")
      putIn = &deadMap;
    else if (boomList.at(DETDAT_LINECODE) == "mag")
      putIn = &magMap;
    else if (boomList.at(DETDAT_LINECODE) == "targ")
      putIn = &targMap;
    else if (boomList.at(DETDAT_LINECODE) == "phot0") {
      richVec.push_back(boomList);
      continue;
    } else if (boomList.at(DETDAT_LINECODE) == "calo") {
      caloVec.push_back(boomList);
      continue;
    } else if (boomList.at(DETDAT_LINECODE) == "cmtx") {
      putIn = &cmtxMap;
    } else if (boomList.at(DETDAT_LINECODE) == "cath") {
      putIn = NULL;
      richCath.push_back(originalFile.at(i));
    } else if (boomList.at(DETDAT_LINECODE) == "mirr") {
      putIn = NULL;
      richMirr.push_back(originalFile.at(i));
    } else
      continue;

    if (putIn != NULL)
      (*putIn)[strToInt(boomList.at(DETDAT_ID))] = boomList;
  }

  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4SDetectorsDat: File '" + fileName + "' has been loaded.");
}

void T4SDetectorsDat::addWireInformationFromEffic(string _efficFile)
{
  myEffic= new T4SEfficiency(NULL);
  myEffic->readXML(_efficFile);
  
  for (unsigned int i = 0; i < wireDet.size(); i++) {
    T4SWireDetector* det = &wireDet.at(i);
    string tbName = det->tbName;
    string detName = det->det;
    int unit = det->unit;

    // exceptions for rpd (there exists no additional information, furthermore it's not in the detMap list)
    if (tbName == "CA01R1__" || tbName == "CA01R2__" || tbName == "RP01R1__" || tbName == "RP01R2__")
      continue;

    // all hodoscope (outer, middle, ladder, innner, last) information already set
    string substr = tbName.substr(0,2);
    if (substr == "HO" || substr == "HI" || (substr == "HM" && tbName != "HM01P2__") || substr == "HL" || substr == "HG")
      continue;
    if(!myEffic->addWireInformationToStruct(wireDet.at(i))){
      T4SMessenger::getInstance()->printfMessage(T4SWarning,__LINE__,__FILE__,"T4SDetectorsDat has a problem! Cannot find detector with tbname %s, detname %s, unit %i in the efficiency database %s !!!\n",tbName.c_str(),detName.c_str(),unit,_efficFile.c_str());
    }
  }
}

void T4SDetectorsDat::save(string fileName)
{
  std::string lastSymbol = fileName;
  lastSymbol = lastSymbol.erase(0, fileName.size() - 4); // check for .dat
  if (lastSymbol != ".dat")
    fileName += ".dat";

  saveFile.open(fileName.c_str(), std::ios::out | std::ios::ate);
  if (!saveFile.is_open()) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Error in T4SDetectorsDat: Output file has not been created!");
    return;
  }
  saveFile << std::setprecision(9);

  writeHeader();
  writeMagnets();
  writeRotation();
  writeTarget();
  writeWireHeader();
  writeWireDetectors();
  writeCaloPosition();
  writeCaloCMTX();
  writeDeadHeader();
  writeDeadZone();
  writeRich();

  saveFile.close();
}

void T4SDetectorsDat::writeHeader(void)
{
  time_t rawtime;
  time(&rawtime);
  std::string now = ctime(&rawtime);
  //first we write the header
  addNL();
  addLine(" ---   TGEANT ===> Detector data " + now);
  addLine(" ori   1  2  3  - beam, detector orientation in MRS");
  addNL();
  addLine(" ver  v007rel4");
  addLine(" mgeo  y2011v00");
  addNL();
}

void T4SDetectorsDat::writeMagnets(void)
{
  addLine("    magnet     Xcen      Ycen      Zcen    rot   field scale   flag1   flag2   current");

  if (magnetPosition == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Error in T4SDetectorsDat::writeMagnets. Magnet position not set.");
    return;
  }

  int sm1 = -1, sm2 = -1, targ = -1;
  int sm1Eff = -1, sm2Eff = -1, targEff = -1;
  for (unsigned int i = 0; i < magnetPosition->size(); i++) {
    if (magnetPosition->at(i).general.useDetector) {
      if (magnetPosition->at(i).general.name == "SM1")
        sm1 = i;
      else if (magnetPosition->at(i).general.name == "SM2")
        sm2 = i;
      else
        targ = i;
    }
  }
  
  for (unsigned int i = 0; i < myEffic->getMagList().size();i++){
    switch(myEffic->getMagList().at(i).number){
      case 3: targEff = i; break;
      case 4: sm1Eff = i; break;
      case 5: sm2Eff = i; break;
    }
  }

  if (targ == -1)
    addLine(" mag   3         0.00      0.00      0.00    0         0.00        1       0"); // we don't know why, but coral crashes without this line
  else{
    saveFile << " mag   3" << "\t"
            << magnetPosition->at(targ).general.position[2] / 10 << "\t"
            << magnetPosition->at(targ).general.position[0] / 10 << "\t"
        << magnetPosition->at(targ).general.position[1] / 10 << "\t" << " "
        << myEffic->getMagList().at(targEff).rot << " "
        << magnetPosition->at(targ).scaleFactor << " "
        << myEffic->getMagList().at(targEff).flag1 << " "
        << myEffic->getMagList().at(targEff).flag2 << endl;
  }    

  if (sm1 != -1){
    saveFile << " mag   4" << "\t"
        << magnetPosition->at(sm1).general.position[2] / 10 << "\t"
        << magnetPosition->at(sm1).general.position[0] / 10 << "\t"
        << magnetPosition->at(sm1).general.position[1] / 10 << "\t"
        << " "
        << myEffic->getMagList().at(sm1Eff).rot << " "
        << magnetPosition->at(sm1).scaleFactor << " "
        << myEffic->getMagList().at(sm1Eff).flag1 << " "
        << myEffic->getMagList().at(sm1Eff).flag2 << endl;
  }
  if (sm2 != -1){
    saveFile << " mag   5" << "\t"
        << magnetPosition->at(sm2).general.position[2] / 10 << "\t"
        << magnetPosition->at(sm2).general.position[0] / 10 << "\t"
        << magnetPosition->at(sm2).general.position[1] / 10 << "\t"
        << " "
        << myEffic->getMagList().at(sm2Eff).rot << " "
        << magnetPosition->at(sm2).scaleFactor << " "
        << myEffic->getMagList().at(sm2Eff).flag1 << " "
        << myEffic->getMagList().at(sm2Eff).flag2 << " "
        << magnetPosition->at(sm2).current << endl;
  }
  addNL();
}

void T4SDetectorsDat::writeTarget(void)
{
  addLine("            Targets: parameters of:");
  addLine(
      " ===========================================================================");
  addLine(
      "        num name  rot.matr  sh    Xsize  Ysize  Zsize   MRS: Xcen      Ycen      Zcen");
  addLine(
      " ---------------------------------------------------------------------------");

  for (unsigned int i = 0; i < targ.size(); i++) {
    saveFile << " targ" << "\t" << i+1 << "\t" << targ.at(i).name
        << "\t" << targ.at(i).rotMatrix << "\t"
        << targ.at(i).sh << "\t"
        << doubleToStrFP(targ.at(i).zSize / 10) << "\t"
        << doubleToStrFP(targ.at(i).xSize / 10) << "\t"
        << doubleToStrFP(targ.at(i).ySize / 10) << "\t"
        << doubleToStrFP(targ.at(i).zCen / 10) << "\t"
        << doubleToStrFP(targ.at(i).xCen / 10) << "\t"
        << doubleToStrFP(targ.at(i).yCen / 10) << std::endl;
  }
  addNL();
}

void T4SDetectorsDat::writeWireHeader(void)
{
  addLine("            Parameters of the wire detectors");
  addLine(
      " =============================================================================================================================");
  addLine(
      "       ID   TBname   det unit type rad.len.  Xsize   Ysize   Zsize MRS: Xcen       Ycen       Zcen  rot matr    Wire dist  angle  Nwires   pitch  effic  backgr tgate  dr.vel.  T0  res2hit space tslice");
  addLine(
      " -----------------------------------------------------------------------------------------------------------------------------");
}

void T4SDetectorsDat::writeWireDetectors(void)
{
  // values stored in T4WireDetector objects (position and size) are in TGEANT system
  // cm (detectors.dat) = mm (TGEANT) / 10
  // we rotate X, Y and Z into detectors.dat system:
  // X (detectors.dat) = Z (TGEANT)
  // Y (detectors.dat) = X (TGEANT)
  // Z (detectors.dat) = Y (TGEANT)

  std::string key, tbName;

  for (unsigned int i = 0; i < wireDet.size(); i++) {
    T4SWireDetector* det = &wireDet.at(i);
    tbName = det->tbName;
    if (tbName == "CA01R1__" || tbName == "CA01R2__" || tbName == "RP01R1__"
        || tbName == "RP01R2__")
      key = " rpd";
    else
      key = " det";

    saveFile << key << "\t" << det->id << "\t" << tbName << "\t" << det->det
        << "\t" << det->unit << "\t" << det->type << "\t"
        << doubleToStrFP(det->radLength) << "\t"
        << doubleToStrFP(det->zSize / 10) << "\t"
        << doubleToStrFP(det->xSize / 10) << "\t"
        << doubleToStrFP(det->ySize / 10) << "\t"
        << doubleToStrFP(det->zCen / 10) << "\t"
        << doubleToStrFP(det->xCen / 10) << "\t"
        << doubleToStrFP(det->yCen / 10) << "\t" << det->rotMatrix << "\t"
        << doubleToStrFP(det->wireDist / 10) << "\t"
        << doubleToStrFP(det->angle) << "\t" << det->nWires << "\t"
        << doubleToStrFP(det->pitch / 10) << "\t" << doubleToStrFP(det->effic)
        << "\t" << doubleToStrFP(det->backgr) << "\t"
        << doubleToStrFP(det->tgate) << "\t" << doubleToStrFP(det->drVel)
        << "\t" << doubleToStrFP(det->t0) << "\t" << doubleToStrFP(det->res2hit)
        << "\t" << doubleToStrFP(det->space) << "\t"
        << doubleToStrFP(det->tslice) << std::endl;
  }

  addNL(4);
}

void T4SDetectorsDat::writeDeadHeader(void)
{
  addLine("          Parameters of the dead zones");
  addLine(
      " ===========================================================================");
  addLine(
      "        ID   TBname  det unit sh  Xsize  Ysize  Zsize   MRS: Xcen       Ycen     Zcen  rot.matr ");
  addLine(
      " ---------------------------------------------------------------------------");
}

void T4SDetectorsDat::writeDeadZone(void)
{
  // values stored in T4DeadZone objects (position and size) are in TGEANT system
  // cm (detectors.dat) = mm (TGEANT) / 10
  // we rotate X, Y and Z into detectors.dat system:
  // X (detectors.dat) = Z (TGEANT)
  // Y (detectors.dat) = X (TGEANT)
  // Z (detectors.dat) = Y (TGEANT)
  for (unsigned int i = 0; i < deadZone.size(); i++) {
    T4SDeadZone* dead = &deadZone.at(i);
    saveFile << " dead" << "\t" << dead->id << "\t" << dead->tbName << "\t"
        << dead->det << "\t" << dead->unit << "\t" << dead->sh << "\t"
        << doubleToStrFP(dead->zSize / 10) << "\t"
        << doubleToStrFP(dead->xSize / 10) << "\t"
        << doubleToStrFP(dead->ySize / 10) << "\t"
        << doubleToStrFP(dead->zCen / 10) << "\t"
        << doubleToStrFP(dead->xCen / 10) << "\t"
        << doubleToStrFP(dead->yCen / 10) << "\t" << dead->rotMatrix
        << std::endl;
  }

  addNL(3);
}

void T4SDetectorsDat::writeCaloPosition(void)
{
  addLine("          Parameters of calorimeters");
  addLine(
      " ===========================================================================");
  addLine("        TBname  name  pos.MRS  X     Y     Z");
  addLine(
      " ---------------------------------------------------------------------------");

  if (caloPosition == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Error in T4SDetectorsDat::writeCaloPosition. Calorimeter position not set.");
    return;
  }

  for (unsigned int i = 0; i < caloPosition->size(); i++) {
    string tgeantName = caloPosition->at(i).name;

    saveFile << " calo" << "\t" << getCaloTBName(tgeantName) << "\t"
        << getCaloName(tgeantName) << "\t"
        << doubleToStrFP(caloPosition->at(i).position[2] / 10) << "\t"
        << doubleToStrFP(caloPosition->at(i).position[0] / 10) << "\t"
        << doubleToStrFP(caloPosition->at(i).position[1] / 10) << std::endl;
  }
  addNL(2);
  addLine("          Parameters of calorimeters matrices");
  addLine(
      " ===========================================================================");
  addLine(
      "       ID   det type  nmod  nrow  ncol  roffset coffset Xsize  Ysize  Zsize  1st cell CaloRefSys: X    Y    Z           Ystep   Zstep   tgate   Tresh  GeV->ADC   rad.len abs.len StochT ConstT reserv nholes Ecrit idtype holepars. ");
  addLine(
      " ---------------------------------------------------------------------------");
}

void T4SDetectorsDat::writeCaloCMTX(void)
{
  for (unsigned int i = 0; i < cmtxLines->size(); i++) {
    saveFile << "cmtx\t"
        << cmtxLines->at(i).detectorId << "\t"
        << cmtxLines->at(i).detName << "\t"
        << cmtxLines->at(i).type << "\t"
        << cmtxLines->at(i).nmod << "\t"
        << cmtxLines->at(i).nRow << "\t"
        << cmtxLines->at(i).nCol << "\t"
        << cmtxLines->at(i).roffset << "\t"
        << cmtxLines->at(i).coffset << "\t"
        << cmtxLines->at(i).size[2] / 10. << "\t"
        << cmtxLines->at(i).size[0] / 10. << "\t"
        << cmtxLines->at(i).size[1] / 10. << "\t"
        << cmtxLines->at(i).position[2] / 10. << "\t"
        << cmtxLines->at(i).position[0] / 10. << "\t"
        << cmtxLines->at(i).position[1] / 10. << "\t"
        << cmtxLines->at(i).xStep / 10. << "\t"
        << cmtxLines->at(i).yStep / 10. << "\t"
        << cmtxLines->at(i).tGate << "\t"
        << cmtxLines->at(i).threshold << "\t"
        << cmtxLines->at(i).gev2adc << "\t"
        << cmtxLines->at(i).radlen << "\t"
        << cmtxLines->at(i).abslen << "\t"
        << cmtxLines->at(i).stochasticTerm << "\t"
        << cmtxLines->at(i).constantTerm << "\t"
        << cmtxLines->at(i).calibration << "\t"
        << cmtxLines->at(i).nHole << "\t"
        << cmtxLines->at(i).ecrit << "\t"
        << cmtxLines->at(i).moduleName << endl;
  }
  addNL(4);
}

string T4SDetectorsDat::getCaloTBName(string name)
{
  if (name == "ECAL0")
    return "EC00P1__";
  else if (name == "ECAL1")
    return "EC01P1__";
  else if (name == "ECAL2")
    return "EC02P1__";
  else if (name == "HCAL1")
    return "HC01P1__";
  else if (name == "HCAL2")
    return "HC02P1__";
  else
    return "UNKOWN";
}

string T4SDetectorsDat::getCaloName(string name)
{
  if (name == "ECAL0")
    return "LG3V";
  else if (name == "ECAL1")
    return "LG1V";
  else if (name == "ECAL2")
    return "LG2V";
  else if (name == "HCAL1")
    return "HC1V";
  else if (name == "HCAL2")
    return "HC2V";
  else
    return "UNKOWN";
}

void T4SDetectorsDat::writeRotation(void)
{
  addLine("          Rotation matrices for detectors");
  addLine(
      " rot      1     1.0000  0.0000  0.0000  0.0000  1.0000  0.0000  0.0000  0.0000  1.0000");
  addLine(
      " rot      2    -1.0000  0.0000  0.0000  0.0000  0.0000 -1.0000  0.0000  1.0000  0.0000");
  addLine(
      " rot      3     1.0000  0.0000  0.0000  0.0000  0.9848 -0.1736  0.0000  0.1736  0.9848");
  addLine(
      " rot      4     1.0000  0.0000  0.0000  0.0000  0.9848  0.1736  0.0000 -0.1736  0.9848");
  addLine(
      " rot      5     1.0000  0.0000  0.0000  0.0000  0.0000 -1.0000  0.0000  1.0000  0.0000");
  addLine(
      " rot      6     1.0000  0.0000  0.0000  0.0000  0.7071 -0.7071  0.0000  0.7071  0.7071");
  addLine(
      " rot      7    1.0000  0.0000  0.0000  0.0000  0.8660 -0.5  0.0000  0.5  0.8660");
  addLine(
      " rot      8    -1.0000  0.0000  0.0000  0.0000  0.0000 -1.0000  0.0000 -1.0000  0.0000");
  addLine(
      " rot      9     1.0000  0.0000  0.0000  0.0000  0.9990 -0.0436  0.0000  0.0436  0.9990");
  addLine(
      " rot     10     1.0000  0.0000  0.0000  0.0000  0.7071  0.7071  0.0000 -0.7071  0.7071");
  addLine(
      " rot     11     1.0000  0.0000  0.0000  0.0000  0.9659  0.2588  0.0000 -0.2588  0.9659");
  addLine(
      " rot     12     0.0000  0.0000  1.0000  1.0000  0.0000  0.0000  0.0000  1.0000  0.0000");
  addLine(
      " rot     13     0.0000  0.0000  1.0000  0.7071 -0.7071  0.0000  0.7071  0.7071  0.0000");
  addLine(
      " rot     14     0.0000  0.0000  1.0000  0.0000 -1.0000  0.0000  1.0000  0.0000  0.0000");
  addLine(
      " rot     15     0.0000  0.0000  1.0000  0.7071  0.7071  0.0000 -0.7071  0.7071  0.0000");
  addLine(
      " rot     16     0.0000  0.0000  1.0000  1.0000  0.0000  0.0000  0.0000  1.0000  0.0000");
  addNL();
  addNL();
  addNL();
}

void T4SDetectorsDat::writeRich(void)
{
  if (richPosition == NULL) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
        __FILE__,
        "Error in T4SDetectorsDat::writeRich. RICH position not set.");
    return;
  }

  addLine("          Parameters of RICH1 :");
  addLine(" ===================================");
  addLine(" rich  900");

  double rindex, rindexVS;
  if (richPosition->gas == "C4F10") {
    rindex = 1.001515;
    rindexVS = 1.001340;
  } else if (richPosition->gas == "N2") {
    rindex = 1.000448;
    rindexVS = 1.000273;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4SDetectorsDat::writeRich: Unknown rich gas name: " + richPosition->gas + ".");
    rindex = rindexVS = 0;
  }

  saveFile << " rich1 rindex " << rindex << std::endl;
  saveFile << " rich1 rindexVS " << rindexVS << std::endl;
  addLine(
      " ===========================================================================");
  addLine(
      "          RICH1 photon detector data, up and down ( det centres and rot. matrices )");
  addLine(
      " ===========================================================================");
  addLine("          name  centre-x   -y     -z     rot-matrix");
  addNL();



  saveFile << " phot0    Up    " << richPosition->general.position[2] / 10
      << "    0.00  193.10  0.98877  0.00000 -0.14943  0.00000  1.00000  0.00000  0.14943  0.00000  0.98877"
      << std::endl;
  saveFile << " phot0    Down  " << richPosition->general.position[2] / 10
      << "    0.00  193.10  0.98877  0.00000 -0.14943  0.00000  1.00000  0.00000  0.14943  0.00000  0.98877"
      << std::endl;

  addNL(2);
  addLine(
      " ===========================================================================");
  addLine("          RICH1 cathodes data 0-7 and 8-15");
  addLine(
      " ===========================================================================");
  addLine(
      "        ID   TBname   name   offset-x offset-y offset-z npadx npady padx  pady qz-wind  gap   delta-rot-matrix");
  addNL();
  addLine(" cath   901  RA01P0_u  U01      4.50    95.25    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   902  RA01P0_d  U02     -4.50    95.25   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   903  RA01P1_u  U03      4.50    31.75    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   904  RM01P1_d  U04     -4.50    31.75   -29.76   48   48   1.20   1.20   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   905  RA01P2_u  U05      4.50   -31.75    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   906  RM01P2_d  U06     -4.50   -31.75   -29.76   48   48   1.20   1.20   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   907  RA01P3_u  U07      4.50   -95.25    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   908  RA01P3_d  U08     -4.50   -95.25   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   909  RA01P4_u  D01     -4.50    95.25    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   910  RA01P4_d  D02      4.50    95.25   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   911  RM01P5_u  D03     -4.50    31.75    29.76   48   48   1.20   1.20   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   912  RA01P5_d  D04      4.50    31.75   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   913  RM01P6_u  D05     -4.50   -31.75    29.76   48   48   1.20   1.20   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   914  RA01P6_d  D06      4.50   -31.75   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   915  RA01P7_u  D07     -4.50   -95.25    29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addLine(" cath   916  RA01P7_d  D08      4.50   -95.25   -29.76   72   72   0.80   0.80   0.50   3.40  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000  0.00000");
  addNL(3);
  addLine(
      " ===========================================================================");
  addLine("          RICH1 mirror data --- nom mirror centres");
  addLine(
      " ===========================================================================");
  addLine("          nominal mirrors");
  addLine("          name     x          y       z(MRS)    Radius ");
  addLine(
      " ===========================================================================");
  addLine(" mirr0    Up     253.500     0.000   160.000   660.000");
  addLine(" mirr0    Down   253.500     0.000  -160.000   660.000");
  addNL();
  addLine(
      " ===========================================================================");
  addLine("          mirror elements ( MUST follow nominal mirror data ) :");
  addLine(
      "         name   theta     phi   radius    d-the    d-phi   delta  qual  notusd");
  addLine(" ___________________________________________________________________________");
  addLine(" mirr    MT18   87.937   21.846    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT17   88.061   17.874    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT16   88.161   13.903    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT15   88.237    9.931    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT14   88.288    5.959    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT13   88.313    1.986    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT42   88.313   -1.986    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT43   88.288   -5.959    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT44   88.237   -9.931    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT45   88.161  -13.903    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT46   88.061  -17.874    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT47   87.937  -21.846    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT12   91.469   19.969    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT11   91.582   15.976    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT10   91.671   11.983    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT9    91.734    7.989    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT8    91.772    3.995    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT7    91.785    0.000    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT37   91.772   -3.995    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT38   91.734   -7.989    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT39   91.671  -11.983    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT40   91.582  -15.976    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT41   91.469  -19.969    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT6    94.867   22.084    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT5    94.992   18.071    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT4    95.094   14.057    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT3    95.170   10.041    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT2    95.221    6.025    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT1    95.247    2.008    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT31   95.247   -2.008    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT32   95.221   -6.025    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT33   95.170  -10.041    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT34   95.094  -14.057    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT35   94.992  -18.071    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT36   94.867  -22.084    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT24   98.396   20.187    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT23   98.510   16.153    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT22   98.599   12.116    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT21   98.663    8.078    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT20   98.702    4.039    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT19   98.715    0.000    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT48   98.702   -4.039    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT49   98.663   -8.078    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT50   98.599  -12.116    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT51   98.510  -16.153    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT52   98.396  -20.187    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT30  101.796   22.327    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT29  101.923   18.272    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT28  102.026   14.214    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT27  102.103   10.154    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT26  102.155    6.093    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT25  102.181    2.031    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT53  102.181   -2.031    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT54  102.155   -6.093    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT55  102.103  -10.154    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT56  102.026  -14.214    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT57  101.923  -18.272    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MT58  101.796  -22.327    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB30   92.063   21.846    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB29   91.939   17.874    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB28   91.839   13.903    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB27   91.763    9.931    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB26   91.712    5.959    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB25   91.687    1.986    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB53   91.687   -1.986    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB54   91.712   -5.959    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB55   91.763   -9.931    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB56   91.839  -13.903    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB57   91.939  -17.874    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB58   92.063  -21.846    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB24   88.531   19.969    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB23   88.418   15.976    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB22   88.329   11.983    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB21   88.266    7.989    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB20   88.228    3.995    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB19   88.215    0.000    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB48   88.228   -3.995    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB49   88.266   -7.989    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB50   88.329  -11.983    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB51   88.418  -15.976    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB52   88.531  -19.969    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB6    85.133   22.084    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB5    85.008   18.071    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB4    84.906   14.057    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB3    84.830   10.041    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB2    84.779    6.025    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB1    84.753    2.008    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB31   84.753   -2.008    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB32   84.779   -6.025    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB33   84.830  -10.041    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB34   84.906  -14.057    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB35   85.008  -18.071    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB36   85.133  -22.084    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB12   81.604   20.187    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB11   81.490   16.153    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB10   81.401   12.116    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB9    81.337    8.078    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB8    81.298    4.039    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB7    81.285    0.000    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB37   81.298   -4.039    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB38   81.337   -8.078    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB39   81.401  -12.116    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB40   81.490  -16.153    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB41   81.604  -20.187    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB18   78.204   22.327    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB17   78.077   18.272    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB16   77.974   14.214    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB15   77.897   10.154    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB14   77.845    6.093    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB13   77.819    2.031    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB42   77.819   -2.031    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB43   77.845   -6.093    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB44   77.897  -10.154    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB45   77.974  -14.214    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB46   78.077  -18.272    660.    0.000    0.000   0.0   0.00   1");
  addLine(" mirr    MB47   78.204  -22.327    660.    0.000    0.000   0.0   0.00   1");

  addNL();
}
