#include "T4XMLParser.hh"
#include "T4SMessenger.hh"

int T4XMLParser::getBaseNodeCount(string _nodeName)
{
  return rootNode->getElementsByTagName(XMLString::transcode(_nodeName.c_str()))
      ->getLength();
}

T4XMLParser::T4XMLParser(void)
{
  try {
    XMLPlatformUtils::Initialize();
  } catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());

    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4XMLParser::T4XMLParser - Error during initialization!");
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, message);
    XMLString::release(&message);
  }

  rootNode = 0;

  myDOMParser = new XercesDOMParser();
  myDOMParser->setValidationScheme(XercesDOMParser::Val_Always);
  myDOMParser->setDoNamespaces(false);
  ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
  myDOMParser->setErrorHandler(errHandler);
  myDOMParser->setDoSchema(false);
  myDOMParser->setValidationConstraintFatal(false);
}

string T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode)
{
  XMLCh* temp = XMLString::transcode(_baseNode.c_str());
  DOMNodeList* list = myDOMParser->getDocument()->getElementsByTagName(temp);
  XMLString::release(&temp);

  if (getBaseNodeCount(_baseNode) <= _baseNumber)
    return "";

  DOMElement* parent = dynamic_cast<DOMElement*>(list->item(_baseNumber));
  DOMElement* child = dynamic_cast<DOMElement*>(parent->getElementsByTagName(
      XMLString::transcode(_childNode.c_str()))->item(0));
  string value;
  if (child) {
    char* temp2 = XMLString::transcode(child->getTextContent());
    value = temp2;
    XMLString::release(&temp2);
  } else {
    value = "";
  }
  return value;
}

int T4XMLParser::getNodeChildCount(string _nodeName, int _nodeNumber,
    string _childName)
{
  XMLCh* temp = XMLString::transcode(_nodeName.c_str());
  DOMNodeList* list = myDOMParser->getDocument()->getElementsByTagName(temp);
  XMLString::release(&temp);
  if (list->getLength() <= (unsigned int) _nodeNumber)
    return 0;

  DOMElement* parent = dynamic_cast<DOMElement*>(list->item(_nodeNumber));
  DOMNodeList* childList = parent->getElementsByTagName(
      XMLString::transcode(_childName.c_str()));
  return (int) childList->getLength();
}

bool T4XMLParser::parseFile(string _fileName)
{
  try {
    myDOMParser->parse(XMLString::transcode(_fileName.c_str()));
  } catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        (string) "T4XMLParser::parseFile - XMLException message is: \n"
            + message);
    XMLString::release(&message);
    return false;
  } catch (const DOMException& toCatch) {
    char* message = XMLString::transcode(toCatch.msg);
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        (string) "T4XMLParser::parseFile - DOMException message is: \n"
            + message);
    XMLString::release(&message);
    return false;
  } catch (...) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__,
        (string) "T4XMLParser::parseFile - Unexpected Exception. Does '"
            + _fileName + "' exist?");
    return false;
  }
  rootNode = myDOMParser->getDocument()->getDocumentElement();
  return true;
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, string& _retVal)
{
  _retVal = getChildValue(_baseNode, _baseNumber, _childNode);
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, bool& _retVal)
{
  _retVal = strToBool(getChildValue(_baseNode, _baseNumber, _childNode));
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, double& _retVal)
{
  _retVal = strToDouble(getChildValue(_baseNode, _baseNumber, _childNode));
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, int& _retVal)
{
  _retVal = strToInt(getChildValue(_baseNode, _baseNumber, _childNode));
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, unsigned int& _retVal)
{
  _retVal = static_cast<unsigned int>(strToInt(
      getChildValue(_baseNode, _baseNumber, _childNode)));
}

void T4XMLParser::getChildValue(string _baseNode, int _baseNumber,
    string _childNode, long int& _retVal)
{
  _retVal = static_cast<long int>(strToLong(
      getChildValue(_baseNode, _baseNumber, _childNode)));
}

T4XMLParser::~T4XMLParser()
{
  if (myDOMParser != NULL)
    delete myDOMParser; 
  XMLPlatformUtils::Terminate();
}
