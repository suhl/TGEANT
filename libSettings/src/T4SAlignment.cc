#include "T4SAlignment.hh"
#include "T4SMessenger.hh"

void T4SAlignment::readAlignment(void)
{
  setDefault();
  map<int, vector<string> >::iterator iter;

  for (iter = detMap.begin(); iter != detMap.end(); ++iter) {
    string tbName = iter->second.at(DETDAT_TBNAME);

    // DC
    // we take the position of the first dc-plane in a dc-detector-system
    if (tbName == "DC00Y1__")
      setPosition(dc00, iter);
    else if (tbName == "DC01Y1__")
      setPosition(dc01, iter);
    else if (tbName == "DC04U2__")
      setPosition(dc04, iter);
    else if (tbName == "DC05U2__")
      setPosition(dc05, iter);
    else if (tbName.substr(0, 3) == "DC0"
        && (tbName[3] == '0' || tbName[3] == '1' || tbName[3] == '4'
            || tbName[3] == '5'))
      continue;

    // MW1
    // we take the positions of plane MA0??1__ and MA0??3__ (not 2 and 4)
    else if (tbName == "MA01X1__")
      setPosition(ma01x1, iter);
    else if (tbName == "MA01X3__")
      setPosition(ma01x3, iter);
    else if (tbName == "MA01Y1__")
      setPosition(ma01y1, iter);
    else if (tbName == "MA01Y3__")
      setPosition(ma01y3, iter);
    else if (tbName == "MA02X1__")
      setPosition(ma02x1, iter);
    else if (tbName == "MA02X3__")
      setPosition(ma02x3, iter);
    else if (tbName == "MA02Y1__")
      setPosition(ma02y1, iter);
    else if (tbName == "MA02Y3__")
      setPosition(ma02y3, iter);
    else if (tbName.substr(0, 2) == "MA"
        && (tbName[5] == '2' || tbName[5] == '4'))
      continue;

    // MW2
    // we take the positions of the ub (X and V) or ur (Y) plane
    else if (tbName == "MB01X1ub")
      setPosition(mb01x, iter);
    else if (tbName == "MB01Y1ur")
      setPosition(mb01y, iter);
    else if (tbName == "MB01V1ub")
      setPosition(mb01v, iter);
    else if (tbName == "MB02X2ub")
      setPosition(mb02x, iter);
    else if (tbName == "MB02Y2ur")
      setPosition(mb02y, iter);
    else if (tbName == "MB02V2ub")
      setPosition(mb02v, iter);
    else if ((tbName.substr(0, 2) == "MB" && (tbName[4] == 'X' || tbName[4] == 'V') && tbName.substr(6, 2) != "ub")
        || (tbName.substr(0, 2) == "MB" && tbName[4] == 'Y' && tbName.substr(6, 2) != "ur"))
      continue;

    // W45
    // we take the positions of the DW0??1__ plane (not 2)
    else if (tbName == "DW01X1__")
      setPosition(dw01x, iter);
    else if (tbName == "DW01Y1__")
      setPosition(dw01y, iter);
    else if (tbName == "DW02X1__")
      setPosition(dw02x, iter);
    else if (tbName == "DW02Y1__")
      setPosition(dw02y, iter);
    else if (tbName == "DW03V1__")
      setPosition(dw03v, iter);
    else if (tbName == "DW03Y1__")
      setPosition(dw03y, iter);
    else if (tbName == "DW04Y1__")
      setPosition(dw04y, iter);
    else if (tbName == "DW04U1__")
      setPosition(dw04u, iter);
    else if (tbName == "DW05X1__")
      setPosition(dw05x, iter);
    else if (tbName == "DW05V1__")
      setPosition(dw05v, iter);
    else if (tbName == "DW06U1__")
      setPosition(dw06u, iter);
    else if (tbName == "DW06X1__")
      setPosition(dw06x, iter);
    else if (tbName.substr(0, 2) == "DW" && tbName[5] == '2')
      continue;

    // RichWall
    else if (tbName == "DR01X1__")
      setPosition(dr01x1, iter);
    else if (tbName == "DR01X2__")
      setPosition(dr01x2, iter);
    else if (tbName == "DR01Y1__")
      setPosition(dr01y1, iter);
    else if (tbName == "DR01Y2__")
      setPosition(dr01y2, iter);
    else if (tbName == "DR02X1__")
      setPosition(dr02x1, iter);
    else if (tbName == "DR02X2__")
      setPosition(dr02x2, iter);
    else if (tbName == "DR02Y1__")
      setPosition(dr02y1, iter);
    else if (tbName == "DR02Y2__")
      setPosition(dr02y2, iter);

    // H1
    else if (tbName == "HG01Y1__")
      setPosition(h1, iter, &h1Counter);
    // H2
    else if (tbName == "HG02Y1__" || tbName == "HG02Y2__")
      setPosition(h2, iter, &h2Counter);

    // H3
    // the tbName of H3O exists a bunch of times, explanations in function setH3Position
    else if (tbName == "HO03Y1_m")
      setPosition(ho03, iter, &ho03Counter);

    // H4O
    // the tbName of H4O exists a bunch of times, explanations in function setH4Position
    else if (tbName == "HO04Y1_m" || tbName == "HO04Y2_m")
      setPosition(ho04, iter, &ho04Counter);

    // H4 and H5
    // inner
    else if (tbName == "HI04X1_u")
      setPosition(hi04x1_u, iter);
    else if (tbName == "HI04X1_d")
      setPosition(hi04x1_d, iter);
    else if (tbName == "HI05X1_u")
      setPosition(hi05x1_u, iter);
    else if (tbName == "HI05X1_d")
      setPosition(hi05x1_d, iter);

    // middle Y
    // explanations in source code
    else if (tbName == "HM04Y1_u")
      setHMYPosition(iter, (char*) "4u");
    else if (tbName == "HM04Y1_d")
      setHMYPosition(iter, (char*) "4d");
    else if (tbName == "HM05Y1_u")
      setHMYPosition(iter, (char*) "5u");
    else if (tbName == "HM05Y1_d")
      setHMYPosition(iter, (char*) "5d");

    // middle X
    else if (tbName == "HM04X1_u")
      setPosition(hm04x1_u, iter);
    else if (tbName == "HM04X1_d")
      setPosition(hm04x1_d, iter);
    else if (tbName == "HM05X1_u")
      setPosition(hm05x1_u, iter);
    else if (tbName == "HM05X1_d")
      setPosition(hm05x1_d, iter);

    // ladder
    // explanations in source code
    else if (tbName == "HL04X1_m")
      setHLXPosition(iter, '4');
    else if (tbName == "HL05X1_m")
      setHLXPosition(iter, '5');

    // HK
    else if (tbName == "HK01X1__")
      setPosition(hk01, iter);
    else if (tbName == "HK02X1__")
      setPosition(hk02, iter);
    else if (tbName == "HK03X1__")
      setPosition(hk03, iter);

    // SandwichVeto
    else if (tbName.substr(0,2) == "HH" && tbName.substr(4,4) == "X1__")
      setPosition(sandwichVeto, iter, &sandwichVetoCounter);
    // MultiplicityCounter
    else if (tbName.substr(0,4) == "HM01")
      setPosition(multiplicityCounter, iter, &multiplicityCounterCounter);

    else if (tbName == "HH02R1__")
      setPosition(hh02, iter);
    else if (tbName == "HH03R1__")
      setPosition(hh03, iter);

    // STRAWs
    // x-pos: from ST0???ub plane (n.b.: for Y plane this is about y-pos)
    // y-pos: should be equal for ub and db, so we take ub (n.b.: for Y plane this is about x-pos)
    // z-pos: average of ub and db plane
    // we don't read (or: we don't need) uc, dc, ua and da positions
    // ST02X1:
    else if (tbName == "ST02X1ub") {
      setStrawXYPosition(st02x1, iter);
      setStrawZPosition(st02x1, iter);
    } else if (tbName == "ST02X1db")
      setStrawZPosition(st02x1, iter);
    // ST02X2:
    else if (tbName == "ST02X2ub") {
      setStrawXYPosition(st02x2, iter);
      setStrawZPosition(st02x2, iter);
    } else if (tbName == "ST02X2db")
      setStrawZPosition(st02x2, iter);
    // ST02Y1:
    else if (tbName == "ST02Y1ub") {
      setStrawXYPosition(st02y1, iter);
      setStrawZPosition(st02y1, iter);
    } else if (tbName == "ST02Y1db")
      setStrawZPosition(st02y1, iter);
    // ST02Y2:
    else if (tbName == "ST02Y2ub") {
      setStrawXYPosition(st02y2, iter);
      setStrawZPosition(st02y2, iter);
    } else if (tbName == "ST02Y2db")
      setStrawZPosition(st02y2, iter);
    // ST02U1:
    else if (tbName == "ST02U1ub") {
      setStrawXYPosition(st02u1, iter);
      setStrawZPosition(st02u1, iter);
    } else if (tbName == "ST02U1db")
      setStrawZPosition(st02u1, iter);
    // ST02V1:
    else if (tbName == "ST02V1ub") {
      setStrawXYPosition(st02v1, iter);
      setStrawZPosition(st02v1, iter);
    } else if (tbName == "ST02V1db")
      setStrawZPosition(st02v1, iter);

    // ST03X1:
    else if (tbName == "ST03X1ub") {
      setStrawXYPosition(st03x1, iter);
      setStrawZPosition(st03x1, iter);
    } else if (tbName == "ST03X1db")
      setStrawZPosition(st03x1, iter);
    // ST03X2:
    else if (tbName == "ST03X2ub") {
      setStrawXYPosition(st03x2, iter);
      setStrawZPosition(st03x2, iter);
    } else if (tbName == "ST03X2db")
      setStrawZPosition(st03x2, iter);
    // ST03Y1:
    else if (tbName == "ST03Y1ub") {
      setStrawXYPosition(st03y1, iter);
      setStrawZPosition(st03y1, iter);
    } else if (tbName == "ST03Y1db")
      setStrawZPosition(st03y1, iter);
    // ST03Y2:
    else if (tbName == "ST03Y2ub") {
      setStrawXYPosition(st03y2, iter);
      setStrawZPosition(st03y2, iter);
    } else if (tbName == "ST03Y2db")
      setStrawZPosition(st03y2, iter);
    // ST03U1:
    else if (tbName == "ST03U1ub") {
      setStrawXYPosition(st03u1, iter);
      setStrawZPosition(st03u1, iter);
    } else if (tbName == "ST03U1db")
      setStrawZPosition(st03u1, iter);
    // ST03V1:
    else if (tbName == "ST03V1ub") {
      setStrawXYPosition(st03v1, iter);
      setStrawZPosition(st03v1, iter);
    } else if (tbName == "ST03V1db")
      setStrawZPosition(st03v1, iter);

    // ST05X1:
    else if (tbName == "ST05X1ub") {
      setStrawXYPosition(st05x1, iter);
      setStrawZPosition(st05x1, iter);
    } else if (tbName == "ST05X1db")
      setStrawZPosition(st05x1, iter);
    // ST05Y2:
    else if (tbName == "ST05Y2ub") {
      setStrawXYPosition(st05y2, iter);
      setStrawZPosition(st05y2, iter);
    } else if (tbName == "ST05Y2db")
      setStrawZPosition(st05y2, iter);
    // ST05U1:
    else if (tbName == "ST05U1ub") {
      setStrawXYPosition(st05u1, iter);
      setStrawZPosition(st05u1, iter);
    } else if (tbName == "ST05U1db")
      setStrawZPosition(st05u1, iter);
    else if (tbName.substr(0, 2) == "ST"
        && (tbName.substr(6, 2) == "uc" || tbName.substr(6, 2) == "dc"
            || tbName.substr(6, 2) == "ua" || tbName.substr(6, 2) == "da"))
      continue;

    // MM
    else if (tbName == "MM01X1__")
      setPosition(getMMX(1), iter, &mmxCounter[1]);
    else if (tbName == "MM01Y1__")
      setPosition(getMMY(1), iter, &mmyCounter[1]);
    else if (tbName == "MM01V1__")
      setPosition(getMMV(1), iter, &mmvCounter[1]);
    else if (tbName == "MM01U1__")
      setPosition(getMMU(1), iter, &mmuCounter[1]);

    else if (tbName == "MM02X1__")
      setPosition(getMMX(2), iter, &mmxCounter[2]);
    else if (tbName == "MM02Y1__")
      setPosition(getMMY(2), iter, &mmyCounter[2]);
    else if (tbName == "MM02V1__")
      setPosition(getMMV(2), iter, &mmvCounter[2]);
    else if (tbName == "MM02U1__")
      setPosition(getMMU(2), iter, &mmuCounter[2]);

    else if (tbName == "MM03X1__")
      setPosition(getMMX(3), iter, &mmxCounter[3]);
    else if (tbName == "MM03Y1__")
      setPosition(getMMY(3), iter, &mmyCounter[3]);
    else if (tbName == "MM03V1__")
      setPosition(getMMV(3), iter, &mmvCounter[3]);
    else if (tbName == "MM03U1__")
      setPosition(getMMU(3), iter, &mmuCounter[3]);

    // PixelMM
    else if (tbName == "MP00X1__")
      setPosition(getMPV(0), iter, &mpxCounter[0]);

    else if (tbName == "MP01X1__")
      setPosition(getMPX(1), iter, &mpxCounter[1]);
    else if (tbName == "MP01Y1__")
      setPosition(getMPY(1), iter, &mpyCounter[1]);
    else if (tbName == "MP01U1__")
      setPosition(getMPU(1), iter, &mpuCounter[1]);
    else if (tbName == "MP01V1__")
      setPosition(getMPV(1), iter, &mpvCounter[1]);

    else if (tbName == "MP02X1__")
      setPosition(getMPX(2), iter, &mpxCounter[2]);
    else if (tbName == "MP02Y1__")
      setPosition(getMPY(2), iter, &mpyCounter[2]);
    else if (tbName == "MP02U1__")
      setPosition(getMPU(2), iter, &mpuCounter[2]);
    else if (tbName == "MP02V1__")
      setPosition(getMPV(2), iter, &mpvCounter[2]);

    else if (tbName == "MP03X1__")
      setPosition(getMPX(3), iter, &mpxCounter[3]);
    else if (tbName == "MP03Y1__")
      setPosition(getMPY(3), iter, &mpyCounter[3]);
    else if (tbName == "MP03U1__")
      setPosition(getMPU(3), iter, &mpuCounter[3]);
    else if (tbName == "MP03V1__")
      setPosition(getMPV(3), iter, &mpvCounter[3]);

    else if (tbName.substr(0,3) == "MP0" && tbName[4] == 'M')
      continue;

    // GEM
    else if (tbName == "GM01X1__")
      setPosition(getGMX(1), iter);
    else if (tbName == "GM01U1__")
      setPosition(getGMU(1), iter);

    else if (tbName == "GM02X1__")
      setPosition(getGMX(2), iter);
    else if (tbName == "GM02U1__")
      setPosition(getGMU(2), iter);

    else if (tbName == "GM03X1__")
      setPosition(getGMX(3), iter);
    else if (tbName == "GM03U1__")
      setPosition(getGMU(3), iter);

    else if (tbName == "GM04X1__")
      setPosition(getGMX(4), iter);
    else if (tbName == "GM04U1__")
      setPosition(getGMU(4), iter);

    else if (tbName == "GM05X1__")
      setPosition(getGMX(5), iter);
    else if (tbName == "GM05U1__")
      setPosition(getGMU(5), iter);

    else if (tbName == "GM06X1__")
      setPosition(getGMX(6), iter);
    else if (tbName == "GM06U1__")
      setPosition(getGMU(6), iter);

    else if (tbName == "GM07X1__")
      setPosition(getGMX(7), iter);
    else if (tbName == "GM07U1__")
      setPosition(getGMU(7), iter);

    else if (tbName == "GM08X1__")
      setPosition(getGMX(8), iter);
    else if (tbName == "GM08U1__")
      setPosition(getGMU(8), iter);

    else if (tbName == "GM09X1__")
      setPosition(getGMX(9), iter);
    else if (tbName == "GM09U1__")
      setPosition(getGMU(9), iter);

    else if (tbName == "GM10X1__")
      setPosition(getGMX(10), iter);
    else if (tbName == "GM10U1__")
      setPosition(getGMU(10), iter);

    else if (tbName == "GM11X1__")
      setPosition(getGMX(11), iter);
    else if (tbName == "GM11U1__")
      setPosition(getGMU(11), iter);
    else if (tbName.substr(0, 2) == "GM"
        && (tbName[4] == 'Y' || tbName[4] == 'V'))
      continue;

    else if (tbName == "GP01X1__")
      setPosition(gp01x, iter);
    else if (tbName == "GP01U1__")
      setPosition(gp01u, iter);
    else if (tbName == "GP02X1__")
      setPosition(gp02x, iter);
    else if (tbName == "GP02U1__")
      setPosition(gp02u, iter);
    else if (tbName == "GP03X1__")
      setPosition(gp03x, iter);
    else if (tbName == "GP03U1__")
      setPosition(gp03u, iter);
    else if (tbName.substr(0, 2) == "GP"
        && (tbName[4] == 'Y' || tbName[4] == 'V' || tbName[4] == 'P'))
      continue;

    // MWPC
    // we only read one entry per station
    else if (tbName == "PA01X1__")
      setPosition(getPA(1), iter);
    else if (tbName == "PA02X1__")
      setPosition(getPA(2), iter);
    else if (tbName == "PA03X1__")
      setPosition(getPA(3), iter);
    else if (tbName == "PA04X1__")
      setPosition(getPA(4), iter);
    else if (tbName == "PA05X1__")
      setPosition(getPA(5), iter);
    else if (tbName == "PA06X1__")
      setPosition(getPA(6), iter);
    else if (tbName == "PA11X1__")
      setPosition(getPA(11), iter);

    else if (tbName == "PB01X1__")
      setPosition(getPB(1), iter);
    else if (tbName == "PB03X1__")
      setPosition(getPB(3), iter);
    else if (tbName == "PB05X1__")
      setPosition(getPB(5), iter);
    else if (tbName == "PB02V1__")
      setPosition(getPB(2), iter);
    else if (tbName == "PB04V1__")
      setPosition(getPB(4), iter);
    else if (tbName == "PB06V1__")
      setPosition(getPB(6), iter);

    else if (tbName == "PS01X1__")
      setPosition(ps01, iter);
    else if ((tbName.substr(0, 4) == "PA01" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA02" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA03" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA04" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA05" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA06" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PA11" && tbName[4] != 'X') ||

        (tbName.substr(0, 4) == "PB01" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PB02" && tbName[4] != 'V')
        || (tbName.substr(0, 4) == "PB03" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PB04" && tbName[4] != 'V')
        || (tbName.substr(0, 4) == "PB05" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "PB06" && tbName[4] != 'V') ||

        (tbName.substr(0, 4) == "PS01" && tbName[4] != 'X'))
      continue;

    // SI
    else if (tbName == "SI01X1__")
      setPosition(getSIX(1), iter);
    else if (tbName == "SI01U1__")
      setPosition(getSIU(1), iter);
    else if (tbName == "SI02X1__")
      setPosition(getSIX(2), iter);
    else if (tbName == "SI02U1__")
      setPosition(getSIU(2), iter);
    else if (tbName == "SI03X1__")
      setPosition(getSIX(3), iter);
    else if (tbName == "SI03U1__")
      setPosition(getSIU(3), iter);
    else if (tbName == "SI04X1__")
      setPosition(getSIX(4), iter);
    else if (tbName == "SI04U1__")
      setPosition(getSIU(4), iter);
    else if (tbName == "SI05X1__")
      setPosition(getSIX(5), iter);
    else if (tbName == "SI05U1__")
      setPosition(getSIU(5), iter);
    else if (tbName.substr(0, 2) == "SI"
        && (tbName[4] == 'Y' || tbName[4] == 'V'))
      continue;

    // FI
    // we only read one entry per station
    else if (tbName == "FI01X1__")
      setPosition(getFI(1), iter);
    else if (tbName == "FI15X1__")
      setPosition(getFI(15), iter);
    else if (tbName == "FI15U1__")
      getFI(15)[4] = true; // use third plane
    else if (tbName == "FI02X1__")
      setPosition(getFI(2), iter);
    else if (tbName == "FI03X1__")
      setPosition(getFI(3), iter);
    else if (tbName == "FI35X1__")
      setPosition(getFI(35), iter);
    else if (tbName == "FI04X1__")
      setPosition(getFI(4), iter);
    else if (tbName == "FI05X1__")
      setPosition(getFI(5), iter);
    else if (tbName == "FI55U1__")
      setPosition(getFI(55), iter);
    else if (tbName == "FI06X1__")
      setPosition(getFI(6), iter);
    else if (tbName == "FI07X1__")
      setPosition(getFI(7), iter);
    else if (tbName == "FI08X1__")
      setPosition(getFI(8), iter);
    else if ((tbName.substr(0, 4) == "FI01" && tbName[4] != 'X')
        || (tbName == "FI15Y1__")
        || (tbName.substr(0, 4) == "FI02" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI03" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI04" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI05" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI55" && tbName[4] != 'U')
        || (tbName.substr(0, 4) == "FI06" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI07" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI08" && tbName[4] != 'X')
        || (tbName.substr(0, 4) == "FI35" && (tbName[4] != 'X'||tbName[5] == '2')))
      continue;

    // Veto
    else if (tbName == "VI01P1__")
      setPosition(vi01p1, iter);
    else if (tbName == "VO01X1__")
      setPosition(vo01x1, iter);
    else if (tbName == "VI02X1__")
      setPosition(vi02x1, iter);

    // BMS not in MC only in RD
    else if (tbName.substr(0,2) == "BM")
      continue;
    else
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
          "Detector not found: " + tbName);
  }

  for (unsigned int i = 0; i < caloVec.size(); i++) {
    string tbName = caloVec.at(i).at(CALO_TBNAME);
    if (tbName == "EC00P1__")
      setCaloPosition(ecal0, caloVec.at(i));
    else if (tbName == "EC01P1__")
      setCaloPosition(ecal1, caloVec.at(i));
    else if (tbName == "EC02P1__")
      setCaloPosition(ecal2, caloVec.at(i));
    else if (tbName == "HC01P1__")
      setCaloPosition(hcal1, caloVec.at(i));
    else if (tbName == "HC02P1__")
      setCaloPosition(hcal2, caloVec.at(i));
    else
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
          "Calorimeter not found: " + tbName);
  }

  for (iter = magMap.begin(); iter != magMap.end(); ++iter) {
    if (strToInt(iter->second.at(MAG_ID)) == 4)
      setMagPosition(sm1, iter);
    else if (strToInt(iter->second.at(MAG_ID)) == 5)
      setMagPosition(sm2, iter);
//    else
//      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
//          "Magnet not found (id number): " + iter->second.at(MAG_ID));
  }

  bool first(true);
  for (iter = targMap.begin(); iter != targMap.end(); ++iter) {
    if (first) {
      first = false;
      T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
          "Found target lines in loaded detectors.dat file. Please choose the correct target and set the position in the GUI yourself!");
    }
    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
        "Name: " + iter->second.at(TARG_NAME) + ", Pos(x,y,z): "
            + iter->second.at(TARG_XPOS) + " " + iter->second.at(TARG_YPOS)
            + " " + iter->second.at(TARG_ZPOS));
  }

  for (iter = rpdMap.begin(); iter != rpdMap.end(); ++iter) {
    string tbName = iter->second.at(DETDAT_TBNAME);

    if (tbName == "CA01R1__")
      setPosition(ca01r1, iter);
    else if (tbName == "CA01R2__")
      setPosition(ca01r2, iter);
    else if (tbName == "RP01R1__")
      setPosition(rp01r1, iter);
    else if (tbName == "RP01R2__")
      setPosition(rp01r2, iter);
    else
      T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
          "RPD not found: " + tbName);
  }

  if (richVec.size() == 2)
      setRichPosition(rich, richVec);
  else
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "Warning in T4SAlignment::readAlignment: We expect two entries in richMap, but we found "
            + intToStr(richVec.size())
            + ". Position of the RICH detector is not set.");

  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "Don't forget to enable the TARGET and the MF3 (not in detectors.dat) for the full setup! Please check the RICH gas, TRIGGER and BEAM plugin!");
}

inline void T4SAlignment::setPosition(double* pos, map<int, vector<string> >::iterator iter)
{
  if (pos[3] == 1) {
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "Warning! T4SAlignment::readAlignment has found the tbName '"
            + iter->second.at(DETDAT_TBNAME)
            + "' again. The x- and y-position of this detectors is set to 0, z is unchanged.");
    pos[0] = 0;
    pos[1] = 0;
  } else {
    pos[0] = strToDouble(iter->second.at(DETDAT_XPOS)) * 10;
    pos[1] = strToDouble(iter->second.at(DETDAT_YPOS)) * 10;
    pos[2] = strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
    pos[3] = 1;
  }
}

inline void T4SAlignment::setStrawZPosition(double* pos, map<int, vector<string> >::iterator iter)
{
  if (pos[2] == 0) // we enter this function the first time
    pos[2] = strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
  else {
    pos[2] += strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
    pos[2] /= 2;
    pos[3] = 1;

    // there's an overlap for ST03 in 2012 RD detectors.dat, so let's print a warning here
    if (iter->second.at(DETDAT_TBNAME).substr(0, 6) == "ST03V1")
      T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
          "Please check the z position of ST03V1! Sometimes (e.g. in 2012) this plane is 1 cm off that will result in an overlap. The typical distance between two straw planes is 4 cm. (This is a default message.)");
  }
}

inline void T4SAlignment::setStrawXYPosition(double* pos, map<int, vector<string> >::iterator iter)
{
  pos[0] = strToDouble(iter->second.at(DETDAT_XPOS)) * 10;
  pos[1] = strToDouble(iter->second.at(DETDAT_YPOS)) * 10;
}

inline void T4SAlignment::setCaloPosition(double* pos, vector<string>& vec)
{
  pos[0] = strToDouble(vec.at(CALO_XPOS)) * 10;
  pos[1] = strToDouble(vec.at(CALO_YPOS)) * 10;
  pos[2] = strToDouble(vec.at(CALO_ZPOS)) * 10;
  pos[3] = 1;
}

inline void T4SAlignment::setMagPosition(double* pos, map<int, vector<string> >::iterator iter)
{
  pos[0] = strToDouble(iter->second.at(MAG_XPOS)) * 10;
  pos[1] = strToDouble(iter->second.at(MAG_YPOS)) * 10;
  pos[2] = strToDouble(iter->second.at(MAG_ZPOS)) * 10;
  pos[3] = 1.0;
  pos[4] = strToDouble(iter->second.at(MAG_SCALE));
  if (iter->second.size() > MAG_CURRENT)
    pos[5] = strToDouble(iter->second.at(MAG_CURRENT));
}

inline void T4SAlignment::setRichPosition(double* pos, vector<vector<string> >& richVec)
{
  pos[0] = (strToDouble(richVec.at(0).at(RICH_XPOS)) + strToDouble(richVec.at(1).at(RICH_XPOS))) * 10 / 2;
  pos[1] = (strToDouble(richVec.at(0).at(RICH_YPOS)) + strToDouble(richVec.at(1).at(RICH_YPOS))) * 10 / 2;
  pos[2] = (strToDouble(richVec.at(0).at(RICH_ZPOS)) + strToDouble(richVec.at(1).at(RICH_ZPOS))) * 10 / 2;
  pos[3] = 1;
}

inline void T4SAlignment::setPosition(double* pos, map<int, vector<string> >::iterator iter, int* counter)
{
  pos[0] = pos[0] * (*counter) + strToDouble(iter->second.at(DETDAT_XPOS)) * 10;
  pos[1] = pos[1] * (*counter) + strToDouble(iter->second.at(DETDAT_YPOS)) * 10;
  pos[2] = pos[2] * (*counter) + strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
  pos[3] = 1;

  (*counter)++;
  pos[0] /= (*counter);
  pos[1] /= (*counter);
  pos[2] /= (*counter);
}

inline void T4SAlignment::setHMYPosition(map<int, vector<string> >::iterator iter, char key[2])
{
  // this value is important for the bigger/lesser comparison in the second call:
  // for UP-plane: plane_big should be on top
  // for DOWN-plane: plane_big should be on bottom
  double p;

  // separation:
  double* plane_small;
  double* plane_big;
  if (key[0] == '4' && key[1] == 'u') {
    plane_small = hm04y1_u1;
    plane_big = hm04y1_u2;
    p = 1;
  } else if (key[0] == '4' && key[1] == 'd') {
    plane_small = hm04y1_d1;
    plane_big = hm04y1_d2;
    p = -1;
  } else if (key[0] == '5' && key[1] == 'u') {
    plane_small = hm05y1_u1;
    plane_big = hm05y1_u2;
    p = 1;
  } else if (key[0] == '5' && key[1] == 'd') {
    plane_small = hm05y1_d1;
    plane_big = hm05y1_d2;
    p = -1;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Error in T4SAlignment::setHMYPosition: Unknown keyword for the plane. ");
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Allowed is '4' and '5' with 'u' and 'd' in combination. Return.");
    return;
  }

  double tmp[4];
  tmp[0] = strToDouble(iter->second.at(DETDAT_XPOS)) * 10;
  tmp[1] = strToDouble(iter->second.at(DETDAT_YPOS)) * 10;
  tmp[2] = strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
  tmp[3] = 1;

  if (plane_small[3] == 0) // first call
    memcpy(plane_small, tmp, sizeof(double) * 4);
  else if (p * strToDouble(iter->second.at(DETDAT_YPOS)) * 10 > p * plane_small[1]) // second call, new plane on top
    memcpy(plane_big, tmp, sizeof(double) * 4);
  else { // second call, new plane on bottom
    memcpy(plane_big, plane_small, sizeof(double) * 4);
    memcpy(plane_small, tmp, sizeof(double) * 4);
  }
}

inline void T4SAlignment::setHLXPosition(map<int, vector<string> >::iterator iter, char key)
{
  double tmp[4];
  tmp[0] = strToDouble(iter->second.at(DETDAT_XPOS)) * 10;
  tmp[1] = strToDouble(iter->second.at(DETDAT_YPOS)) * 10;
  tmp[2] = strToDouble(iter->second.at(DETDAT_ZPOS)) * 10;
  tmp[3] = 1;

  // separation:
  double* plane[4];
  if (key == '4') {
    plane[0] = hl04x1_1;
    plane[1] = hl04x1_2;
    plane[2] = hl04x1_3;
    plane[3] = hl04x1_4;
  } else if (key == '5') {
    plane[0] = hl05x1_1;
    plane[1] = hl05x1_2;
    plane[2] = hl05x1_3;
    plane[3] = hl05x1_4;
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Error in T4SAlignment::setHLXPosition: Unknown keyword for the plane. Allowed is '4' and '5'. Return.");
    return;
  }

  // The following lines are for the ordering of the 4 planes.
  // We check for each call the order.

  // first call
  if (plane[0][3] == 0)
    memcpy(plane[0], tmp, sizeof(double) * 4);

  // second call
  else if (plane[1][3] == 0) {
    if (tmp[0] > plane[0][0]) // order okay
      memcpy(plane[1], tmp, sizeof(double) * 4);
    else { // switch order
      memcpy(plane[1], plane[0], sizeof(double) * 4);
      memcpy(plane[0], tmp, sizeof(double) * 4);
    }
  }

  // third call
  else if (plane[2][3] == 0) {
    if (tmp[0] > plane[1][0]) // order okay
      memcpy(plane[2], tmp, sizeof(double) * 4);
    else {
      memcpy(plane[2], plane[1], sizeof(double) * 4);
      if (tmp[0] > plane[0][0])
        memcpy(plane[1], tmp, sizeof(double) * 4);
      else {
        memcpy(plane[1], plane[0], sizeof(double) * 4);
        memcpy(plane[0], tmp, sizeof(double) * 4);
      }
    }
  }

  // last call
  else if (plane[3][3] == 0) {
    if (tmp[0] > plane[2][0]) // order okay
      memcpy(plane[3], tmp, sizeof(double) * 4);
    else {
      memcpy(plane[3], plane[2], sizeof(double) * 4);
      if (tmp[0] > plane[1][0]) // position between 2 and 1
        memcpy(plane[2], tmp, sizeof(double) * 4);
      else {
        memcpy(plane[2], plane[1], sizeof(double) * 4);
        if (tmp[0] > plane[0][0]) // position between 1 and 0
          memcpy(plane[1], tmp, sizeof(double) * 4);
        else { // position at 0
          memcpy(plane[1], plane[0], sizeof(double) * 4);
          memcpy(plane[0], tmp, sizeof(double) * 4);
        }
      }
    }
  } else {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Error in T4SAlignment::setHLXPosition: This is the 5th call of this function. We only expect 4 planes.");
  }
}

void T4SAlignment::setDefault(void)
{
  for (unsigned int i = 0; i < 4; i++) {
    // RPD
    ca01r1[i] = 0;
    ca01r2[i] = 0;
    rp01r1[i] = 0;
    rp01r2[i] = 0;
    
    // DCs
    dc00[i] = 0;
    dc01[i] = 0;
    dc04[i] = 0;
    dc05[i] = 0;
    // MW1
    ma01x1[i] = 0;
    ma01x3[i] = 0;
    ma01y1[i] = 0;
    ma01y3[i] = 0;
    ma02x1[i] = 0;
    ma02x3[i] = 0;
    ma02y1[i] = 0;
    ma02y3[i] = 0;
    // MW2
    mb01x[i] = 0;
    mb01y[i] = 0;
    mb01v[i] = 0;
    mb02x[i] = 0;
    mb02y[i] = 0;
    mb02v[i] = 0;
    // W45
    dw01x[i] = 0;
    dw01y[i] = 0;
    dw02x[i] = 0;
    dw02y[i] = 0;
    dw03v[i] = 0;
    dw03y[i] = 0;
    dw04y[i] = 0;
    dw04u[i] = 0;
    dw05x[i] = 0;
    dw05v[i] = 0;
    dw06u[i] = 0;
    dw06x[i] = 0;
    // RichWall
    dr01x1[i] = 0;
    dr01x2[i] = 0;
    dr01y1[i] = 0;
    dr01y2[i] = 0;
    dr02x1[i] = 0;
    dr02x2[i] = 0;
    dr02y1[i] = 0;
    dr02y2[i] = 0;
    // H1 & H2
    h1[i] = 0;
    h2[i] = 0;
    // ST02
    st02x1[i] = 0;
    st02x2[i] = 0;
    st02y1[i] = 0;
    st02y2[i] = 0;
    st02u1[i] = 0;
    st02v1[i] = 0;
    // ST03
    st03x1[i] = 0;
    st03x2[i] = 0;
    st03y1[i] = 0;
    st03y2[i] = 0;
    st03u1[i] = 0;
    st03v1[i] = 0;
    // ST05
    st05x1[i] = 0;
    st05y2[i] = 0;
    st05u1[i] = 0;

    // MM & MP
    for (unsigned int j = 0; j < 4; j++) {
      mmx[j][i] = 0;
      mmy[j][i] = 0;
      mmv[j][i] = 0;
      mmu[j][i] = 0;

      mpx[j][i] = 0;
      mpy[j][i] = 0;
      mpu[j][i] = 0;
      mpv[j][i] = 0;
    }

    // GEM & MWPC
    for (unsigned int j = 0; j < 11; j++) {
      gmx[j][i] = 0;
      gmu[j][i] = 0;
      pa[j][i] = 0;
    }
    gp01x[i] = 0;
    gp01u[i] = 0;
    gp02x[i] = 0;
    gp02u[i] = 0;
    gp03x[i] = 0;
    gp03u[i] = 0;

    // MWPC
    for (unsigned int j = 0; j < 6; j++)
      pb[j][i] = 0;
    ps01[i] = 0;

    // SI
    for (unsigned int j = 0; j < 5; j++) {
      six[j][i] = 0;
      siu[j][i] = 0;
    }
    
    // Veto
    vi01p1[i] = 0;
    vo01x1[i] = 0;
    vi02x1[i] = 0;

    // Calorimeter
    ecal0[i] = 0;
    ecal1[i] = 0;
    ecal2[i] = 0;
    hcal1[i] = 0;
    hcal2[i] = 0;

    // Magnets
    sm1[i] = 0;
    sm2[i] = 0;

    // Rich
    rich[i] = 0;

    // H3
    ho03[i] = 0;

    // H4
    hi04x1_u[i] = 0;
    hi04x1_d[i] = 0;
    hm04y1_u1[i] = 0;
    hm04y1_u2[i] = 0;
    hm04y1_d1[i] = 0;
    hm04y1_d2[i] = 0;
    hm04x1_u[i] = 0;
    hm04x1_d[i] = 0;
    hl04x1_1[i] = 0;
    hl04x1_2[i] = 0;
    hl04x1_3[i] = 0;
    hl04x1_4[i] = 0;
    ho04[i] = 0;

    // H5
    hi05x1_u[i] = 0;
    hi05x1_d[i] = 0;
    hm05y1_u1[i] = 0;
    hm05y1_u2[i] = 0;
    hm05y1_d1[i] = 0;
    hm05y1_d2[i] = 0;
    hm05x1_u[i] = 0;
    hm05x1_d[i] = 0;
    hl05x1_1[i] = 0;
    hl05x1_2[i] = 0;
    hl05x1_3[i] = 0;
    hl05x1_4[i] = 0;

    hk01[i] = 0;
    hk02[i] = 0;
    hk03[i] = 0;
    sandwichVeto[i] = 0;
    multiplicityCounter[i] = 0;
    hh02[i] = 0;
    hh03[i] = 0;
  }

  // FI
  for (unsigned int i = 0; i < 5; i++) {
    for (unsigned int j = 0; j < 55; j++)
      fi[j][i] = 0;
  }

  h1Counter = 0;
  h2Counter = 0;
  ho03Counter = 0;
  ho04Counter = 0;

  for (unsigned int j = 0; j < 4; j++) {
    mmxCounter[j] = 0;
    mmyCounter[j] = 0;
    mmvCounter[j] = 0;
    mmuCounter[j] = 0;

    mpxCounter[j] = 0;
    mpyCounter[j] = 0;
    mpuCounter[j] = 0;
    mpvCounter[j] = 0;
  }

  sandwichVetoCounter = 0;
  multiplicityCounterCounter = 0;
}
