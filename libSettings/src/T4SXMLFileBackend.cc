#include "T4SXMLFileBackend.hh"

T4SXMLFileBackend::~T4SXMLFileBackend()
{
  if (myParser != NULL)
    delete myParser;
  if (myWriter != NULL)
    delete myWriter;
}
