#include "T4SMessenger.hh"

bool T4SMessenger::instanceFlag = false;
T4SMessenger* T4SMessenger::single = NULL;

T4SMessenger* T4SMessenger::getInstance(void)
{
  if (!instanceFlag) {
    single = new T4SMessenger();
    instanceFlag = true;
    return single;
  } else {
    return single;
  }
}

T4SMessenger& T4SMessenger::getReference(void)
{
  return *(getInstance());
}

void T4SMessenger::printMessage(statusCode _statusCode, int _lineNumber,
    string _fileName, string _message, bool _printEndl)
{
  if (_statusCode > verboseLevel)
    return;

  string prefix = "";
  switch (_statusCode) {
    case -3:
      textcolor(CBLINK, CRED, CBLACK);
      prefix = "[FATAL] ";
      break;
    case -2:
      textcolor(CBRIGHT, CRED, CBLACK);
      prefix = "[ERROR] ";
      break;
    case -1:
      textcolor(CRESET, CYELLOW, CBLACK);
      prefix = "[WARNING] ";
      break;
    case 0:
      textcolor(CRESET, CWHITE, CBLACK);
      break;
    case 1:
      textcolor(CDIM, CGREEN, CBLACK);
      break;
    case 2:
      textcolor(CDIM, CGREEN, CBLACK);
      break;
    case 3:
      textcolor(CDIM, CGREEN, CBLACK);
      break;
  }
  cout << prefix << currentTime() << _message;
  if (_printEndl)
    cout << endl;

  if ((_statusCode < 0 || verboseLevel > 3) && _printEndl) {
    cout << "\t" << _fileName << " in line " << _lineNumber;
    cout << endl;
  }

  if (_printEndl)
    textcolor(CRESET, CWHITE, CBLACK);

  if (_statusCode == T4SFatalError && _printEndl)
    exit(0);
}

T4SMessenger::T4SMessenger(void)
{
  verboseLevel = 0;
  colorTheme = CTHEME_UNSET;

  streamCode = T4SNotice;
  streamLine = 0;
}

void T4SMessenger::textcolor(int attr, int fg, int bg)
{
  if (colorTheme == CTHEME_UNSET)
    return;
  else if (colorTheme == CTHEME_DARK)
    bg = CDEFAULT;
  else if (colorTheme == CTHEME_LIGHT) {
    bg = CDEFAULT;
    attr = CRESET;
  }

  fg = themeTranslator(fg);

  char command[13];
  /* Command is the control command to the terminal */
  sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg + 30, bg + 40);
  printf("%s", command);
}

void T4SMessenger::printCurrentTime(const char* _format)
{
  cout << currentTime(_format) << endl;
}

string T4SMessenger::currentTime(const char* _format)
{
  time_t now = time(0);
  struct tm tstruct = *localtime(&now);
  char buf[80];
  strftime(buf, sizeof(buf), _format, &tstruct);
  return (string) "[" + buf + "] ";
}

void T4SMessenger::setupStream(statusCode _statusCode, int _lineNumber,
    string _fileName)
{
  streamCode = _statusCode;
  streamLine = _lineNumber;
  streamFile = _fileName;
  printMessage(streamCode, streamLine, streamFile, "", false);
}


void T4SMessenger::printfMessage(statusCode _statusCode, int _lineNumber, string _fileName, string _message, ... )
{
  if (_statusCode > verboseLevel)
    return;
  
  //so we just start by setting everything in the console ready
  streamCode = _statusCode;
  streamLine = _lineNumber;
  streamFile = _fileName;
  printMessage(streamCode, streamLine, streamFile, "", false);
  // then we parse and print the message 
  va_list args;
  va_start(args, _message);
  
  vfprintf(stdout, _message.c_str(), args);
  va_end(args);
  
  
  

  //finish it up!
  textcolor(CRESET, CWHITE, CBLACK);
  if (streamCode == T4SFatalError)
    assert(false);
}







void T4SMessenger::endStream(void)
{
  if (streamCode > verboseLevel)
    return;

  cout << endl;
  if (streamCode < 0 || verboseLevel > 1)
    cout << "\t" << streamFile << " in line " << streamLine << endl;
  textcolor(CRESET, CWHITE, CBLACK);
  if (streamCode == T4SFatalError)
    assert(false);
}

T4SMessenger& operator<<(T4SMessenger& _out, const string& _msg)
{
  if (_out.streamCode <= _out.verboseLevel)
    cout << _msg << flush;
  return _out;
}

T4SMessenger& operator<<(T4SMessenger& _out, const int& _msg)
{
  if (_out.streamCode <= _out.verboseLevel)
    cout << _msg << flush;
  return _out;
}

T4SMessenger& operator<<(T4SMessenger& _out, const double& _msg)
{
  if (_out.streamCode <= _out.verboseLevel)
    cout << _msg << flush;
  return _out;
}

int T4SMessenger::themeTranslator(int _colour)
{
  // no change for dark theme
  if (colorTheme == CTHEME_DARK) {
    if (_colour == CBLACK)
      return CDEFAULT;
    else
      return _colour;
  } else if (colorTheme == CTHEME_LIGHT) {
    switch (_colour) {
      case CBLACK:
        return CWHITE;
        break;
      case CWHITE:
        return CBLACK;
        break;
      case CYELLOW:
        return CBLUE;
        break;
    };
  }

  return _colour;
}

