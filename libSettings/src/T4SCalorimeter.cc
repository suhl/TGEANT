#include "T4SCalorimeter.hh"

#include "T4SDetectorsDat.hh"

void T4SCalorimeter::importCalosFromDetDat(string _inDetDat)
{
  T4SDetectorsDat myDetDat;
  myDetDat.load(_inDetDat);

  map<int, vector<string> >::const_iterator myIter;
  t4SCalorimeter.clear();
  usedIDs.clear();

  const map<int, vector<string> >& myMap = myDetDat.getCmtxMap();

  for (myIter = myMap.begin(); myIter != myMap.end(); myIter++) {
    // read out all the information we need and make a new T4SCAL out of it
    if (myIter->second.size() < 27)
      continue;
    T4SCAL t4SCal;
    t4SCal.detectorId = strToInt(myIter->second.at(CMTX_ID));
    t4SCal.detName = myIter->second.at(CMTX_DET);
    t4SCal.type = strToInt(myIter->second.at(CMTX_TYPE));
    t4SCal.nmod = strToInt(myIter->second.at(CMTX_MODNUM));
    t4SCal.nRow = strToInt(myIter->second.at(CMTX_NROW));
    t4SCal.nCol = strToInt(myIter->second.at(CMTX_NCOL));
    t4SCal.roffset = strToInt(myIter->second.at(CMTX_ROFFSET));
    t4SCal.coffset = strToInt(myIter->second.at(CMTX_COFFSET));
    t4SCal.size[0] = strToDouble(myIter->second.at(CMTX_XSIZE)) * 10;
    t4SCal.size[1] = strToDouble(myIter->second.at(CMTX_YSIZE)) * 10;
    t4SCal.size[2] = strToDouble(myIter->second.at(CMTX_ZSIZE)) * 10;
    t4SCal.position[0] = strToDouble(myIter->second.at(CMTX_XPOS)) * 10;
    t4SCal.position[1] = strToDouble(myIter->second.at(CMTX_YPOS)) * 10; //to mm from cm
    t4SCal.position[2] = strToDouble(myIter->second.at(CMTX_ZPOS)) * 10;
    t4SCal.xStep = strToDouble(myIter->second.at(CMTX_XSTEP)) * 10;
    t4SCal.yStep = strToDouble(myIter->second.at(CMTX_YSTEP)) * 10;
    t4SCal.tGate = strToDouble(myIter->second.at(CMTX_TGATE));
    t4SCal.threshold = strToDouble(myIter->second.at(CMTX_TRESHOLD));
    t4SCal.gev2adc = strToDouble(myIter->second.at(CMTX_GEV2ADC));
    t4SCal.abslen = strToDouble(myIter->second.at(CMTX_ABSLEN)); //maybe here too for radiation lengths?
    t4SCal.radlen = strToDouble(myIter->second.at(CMTX_RADLEN));
    t4SCal.stochasticTerm = strToDouble(myIter->second.at(CMTX_STOCHASTIC));
    t4SCal.constantTerm = strToDouble(myIter->second.at(CMTX_CONSTTERM));
    t4SCal.calibration = -1;
    t4SCal.nHole = strToInt(myIter->second.at(CMTX_NHOLES));
    t4SCal.ecrit = strToDouble(myIter->second.at(CMTX_ECRIT)); //as crit energy maybe check gev/mev?
    t4SCal.moduleName = myIter->second.at(CMTX_MODNAME);

    checkCMTX(t4SCal);
    t4SCalorimeter.push_back(t4SCal);
  }
}

void T4SCalorimeter::readXML(string _inFile)
{
  myParser = new T4XMLParser();

  if (!myParser->parseFile(_inFile)) {
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4SCalorimeter could not parse file: '" + _inFile + "'");
    return;
  }

  t4SCalorimeter.clear();
  usedIDs.clear();
  if (myParser->getBaseNodeCount("CaloLine") > 0)
    for (int i = 0; i < myParser->getBaseNodeCount("CaloLine"); i++) {
      T4SCAL t4SCal;
      myParser->getChildValue("CaloLine", i, "detectorId", t4SCal.detectorId);
      myParser->getChildValue("CaloLine", i, "detName", t4SCal.detName);
      myParser->getChildValue("CaloLine", i, "type", t4SCal.type);
      myParser->getChildValue("CaloLine", i, "nmod", t4SCal.nmod);
      myParser->getChildValue("CaloLine", i, "nRow", t4SCal.nRow);
      myParser->getChildValue("CaloLine", i, "nCol", t4SCal.nCol);
      myParser->getChildValue("CaloLine", i, "roffset", t4SCal.roffset);
      myParser->getChildValue("CaloLine", i, "coffset", t4SCal.coffset);
      myParser->getChildValue("CaloLine", i, "sizeX", t4SCal.size[0]);
      myParser->getChildValue("CaloLine", i, "sizeY", t4SCal.size[1]);
      myParser->getChildValue("CaloLine", i, "sizeZ", t4SCal.size[2]);
      myParser->getChildValue("CaloLine", i, "positionX", t4SCal.position[0]);
      myParser->getChildValue("CaloLine", i, "positionY", t4SCal.position[1]);
      myParser->getChildValue("CaloLine", i, "positionZ", t4SCal.position[2]);
      myParser->getChildValue("CaloLine", i, "yStep", t4SCal.yStep);
      myParser->getChildValue("CaloLine", i, "xStep", t4SCal.xStep);
      myParser->getChildValue("CaloLine", i, "tGate", t4SCal.tGate);
      myParser->getChildValue("CaloLine", i, "threshold", t4SCal.threshold);
      myParser->getChildValue("CaloLine", i, "gev2adc", t4SCal.gev2adc);
      myParser->getChildValue("CaloLine", i, "abslen", t4SCal.abslen);
      myParser->getChildValue("CaloLine", i, "radlen", t4SCal.radlen);
      myParser->getChildValue("CaloLine", i, "stochasticTerm", t4SCal.stochasticTerm);
      myParser->getChildValue("CaloLine", i, "constantTerm", t4SCal.constantTerm);
      myParser->getChildValue("CaloLine", i, "nHole", t4SCal.nHole);
      myParser->getChildValue("CaloLine", i, "ecrit", t4SCal.ecrit);
      myParser->getChildValue("CaloLine", i, "moduleName", t4SCal.moduleName);

      checkCMTX(t4SCal);
      t4SCalorimeter.push_back(t4SCal);
    }

  if (myParser->getBaseNodeCount("ECAL0") == 0)
    return;

  for (int i = 0; i < 20; i++)
    calibrationData[i].caloNum = -1;

  int counter = 0;

  // -- ECAL 0
  calibrationData[counter].caloNum = CALONAME_ECAL0;
  calibrationData[counter].moduleNum = MODULE_SHASHLIK;
  myParser->getChildValue("ECAL0", 0, "shashlik",
      calibrationData[counter].calibrationValue);
  counter++;

  // -- ECAL 1
  calibrationData[counter].caloNum = CALONAME_ECAL1;
  calibrationData[counter].moduleNum = MODULE_SHASHLIK;
  myParser->getChildValue("ECAL1", 0, "shashlik",
      calibrationData[counter].calibrationValue);
  counter++;
  calibrationData[counter].caloNum = CALONAME_ECAL1;
  calibrationData[counter].moduleNum = MODULE_GAMS;
  myParser->getChildValue("ECAL1", 0, "gams",
      calibrationData[counter].calibrationValue);
  counter++;
  calibrationData[counter].caloNum = CALONAME_ECAL1;
  calibrationData[counter].moduleNum = MODULE_MAINZ;
  myParser->getChildValue("ECAL1", 0, "mainz",
      calibrationData[counter].calibrationValue);
  counter++;
  calibrationData[counter].caloNum = CALONAME_ECAL1;
  calibrationData[counter].moduleNum = MODULE_OLGA;
  myParser->getChildValue("ECAL1", 0, "olga",
      calibrationData[counter].calibrationValue);
  counter++;

  // -- ECAL 2
  calibrationData[counter].caloNum = CALONAME_ECAL2;
  calibrationData[counter].moduleNum = MODULE_SHASHLIK;
  myParser->getChildValue("ECAL2", 0, "shashlik",
      calibrationData[counter].calibrationValue);
  counter++;
  calibrationData[counter].caloNum = CALONAME_ECAL2;
  calibrationData[counter].moduleNum = MODULE_GAMS;
  myParser->getChildValue("ECAL2", 0, "gams",
      calibrationData[counter].calibrationValue);
  counter++;
  calibrationData[counter].caloNum = CALONAME_ECAL2;
  calibrationData[counter].moduleNum = MODULE_GAMSRH;
  myParser->getChildValue("ECAL2", 0, "gamsrh",
      calibrationData[counter].calibrationValue);
  counter++;

  // -- HCAL1
  calibrationData[counter].caloNum = CALONAME_HCAL1;
  calibrationData[counter].moduleNum = MODULE_SAMPLING;
  myParser->getChildValue("HCAL1", 0, "sampling",
      calibrationData[counter].calibrationValue);
  counter++;

  // -- HCAL2
  calibrationData[counter].caloNum = CALONAME_HCAL2;
  calibrationData[counter].moduleNum = MODULE_SAMPLING;
  myParser->getChildValue("HCAL2", 0, "sampling",
      calibrationData[counter].calibrationValue);
  counter++;

  // dr. seg fault ist am start
  // hier ist der ralle der commited hard
  // compile check ja, der rest ist mir egal
  // shandy ist hier der format admiral

  for (unsigned int i = 0; i < t4SCalorimeter.size(); i++) {
    int calo, module;

    switch (t4SCalorimeter.at(i).detName.at(0)) {
      case 'L':
        if (t4SCalorimeter.at(i).moduleName == "SHASHLIK")
          module = MODULE_SHASHLIK;
        else if (t4SCalorimeter.at(i).moduleName == "GAMS")
          module = MODULE_GAMS;
        else if (t4SCalorimeter.at(i).moduleName == "RHGAMS")
          module = MODULE_GAMSRH;
        else if (t4SCalorimeter.at(i).moduleName == "MAINZ")
          module = MODULE_MAINZ;
        else if (t4SCalorimeter.at(i).moduleName == "OLGA")
          module = MODULE_OLGA;

        switch (t4SCalorimeter.at(i).detName.at(2)) {
          case '3':
            calo = CALONAME_ECAL0;
            break;
          case '1':
            calo = CALONAME_ECAL1;
            break;
          case '2':
            calo = CALONAME_ECAL2;
            break;
          default:
            T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
            __FILE__,
                "T4CaloManager::construct: Unknown ECAL detector name in cmtx line. This entry will be skipped.");
            continue;
            break;
        }
        break;
      case 'H':
        module = MODULE_SAMPLING;
        switch (t4SCalorimeter.at(i).detName.at(2)) {
          case '1':
            calo = CALONAME_HCAL1;
            break;
          case '2':
            calo = CALONAME_HCAL2;
            break;
          default:
            T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
            __FILE__,
                "T4CaloManager::construct: Unknown HCAL detector name in cmtx line. This entry will be skipped.");
            continue;
            break;
        }
        break;

      default:
        T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__,
        __FILE__,
            "T4CaloManager::construct: Unknown CAL detector name in cmtx line. This entry will be skipped.");
        continue;
        break;

    }

    t4SCalorimeter.at(i).calibration = getCalib(calo, module);
  }

  delete myParser;
  myParser = NULL;
}

double T4SCalorimeter::getCalib(int _calo, int _module)
{
  for (int i = 0; i < 20; i++)
    if (calibrationData[i].caloNum == _calo
        && calibrationData[i].moduleNum == _module)
      return calibrationData[i].calibrationValue;

  return 1.0;
}

void T4SCalorimeter::writeXML(string _inFile)
{
  myWriter = new T4XMLWriter;

  //write calibration data extra for easy hand-editing
  myWriter->addNode("ECAL0");
  myWriter->addNode("ECAL1");
  myWriter->addNode("ECAL2");
  myWriter->addNode("HCAL1");
  myWriter->addNode("HCAL2");

  for (int a = 0; a < 20; a++) {
    string caloName, moduleName;
    if (calibrationData[a].caloNum == -1)
      continue;
    switch (calibrationData[a].caloNum) {
      case CALONAME_ECAL0:
        caloName = "ECAL0";
        break;
      case CALONAME_ECAL1:
        caloName = "ECAL1";
        break;
      case CALONAME_ECAL2:
        caloName = "ECAL2";
        break;
      case CALONAME_HCAL1:
        caloName = "HCAL1";
        break;
      case CALONAME_HCAL2:
        caloName = "HCAL2";
        break;
    }
    switch (calibrationData[a].moduleNum) {
      case MODULE_GAMS:
        moduleName = "gams";
        break;
      case MODULE_SHASHLIK:
        moduleName = "shashlik";
        break;
      case MODULE_MAINZ:
        moduleName = "mainz";
        break;
      case MODULE_OLGA:
        moduleName = "olga";
        break;
      case MODULE_GAMSRH:
        moduleName = "gamsrh";
        break;
      case MODULE_SAMPLING:
        moduleName = "sampling";
        break;
    }
    myWriter->createDataNode(caloName, 0, moduleName,
        calibrationData[a].calibrationValue);
  }

  for (unsigned int i = 0; i < t4SCalorimeter.size(); i++) {
    myWriter->addNode("CaloLine");
    T4SCAL t4SCal = t4SCalorimeter.at(i);
    myWriter->createDataNode("CaloLine", i, "detectorId", t4SCal.detectorId);
    myWriter->createDataNode("CaloLine", i, "detName", t4SCal.detName);
    myWriter->createDataNode("CaloLine", i, "type", t4SCal.type);
    myWriter->createDataNode("CaloLine", i, "nmod", t4SCal.nmod);
    myWriter->createDataNode("CaloLine", i, "nRow", t4SCal.nRow);
    myWriter->createDataNode("CaloLine", i, "nCol", t4SCal.nCol);
    myWriter->createDataNode("CaloLine", i, "roffset", t4SCal.roffset);
    myWriter->createDataNode("CaloLine", i, "coffset", t4SCal.coffset);
    myWriter->createDataNode("CaloLine", i, "sizeX", t4SCal.size[0]);
    myWriter->createDataNode("CaloLine", i, "sizeY", t4SCal.size[1]);
    myWriter->createDataNode("CaloLine", i, "sizeZ", t4SCal.size[2]);
    myWriter->createDataNode("CaloLine", i, "positionX", t4SCal.position[0]);
    myWriter->createDataNode("CaloLine", i, "positionY", t4SCal.position[1]);
    myWriter->createDataNode("CaloLine", i, "positionZ", t4SCal.position[2]);
    myWriter->createDataNode("CaloLine", i, "xStep", t4SCal.xStep);
    myWriter->createDataNode("CaloLine", i, "yStep", t4SCal.yStep);
    myWriter->createDataNode("CaloLine", i, "tGate", t4SCal.tGate);
    myWriter->createDataNode("CaloLine", i, "threshold", t4SCal.threshold);
    myWriter->createDataNode("CaloLine", i, "gev2adc", t4SCal.gev2adc);
    myWriter->createDataNode("CaloLine", i, "radlen", t4SCal.radlen);
    myWriter->createDataNode("CaloLine", i, "abslen", t4SCal.abslen);
    myWriter->createDataNode("CaloLine", i, "stochasticTerm",
        t4SCal.stochasticTerm);
    myWriter->createDataNode("CaloLine", i, "constantTerm",
        t4SCal.constantTerm);
    myWriter->createDataNode("CaloLine", i, "nHole", t4SCal.nHole);
    myWriter->createDataNode("CaloLine", i, "ecrit", t4SCal.ecrit);
    myWriter->createDataNode("CaloLine", i, "moduleName", t4SCal.moduleName);
  }
  myWriter->createFile(_inFile);
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "T4SCalorimeter '" + _inFile + "' has been created.");
  delete myWriter;
  myWriter = NULL;
}

T4SCalorimeter::~T4SCalorimeter()
{

}

int T4SCalorimeter::getCollision(int _startId)
{
  for (unsigned int i = 0; i < usedIDs.size(); i++) {
    if (_startId >= usedIDs.at(i).first
        && _startId < usedIDs.at(i).first + usedIDs.at(i).second)
      return i;
  }
  return -1;
}

void T4SCalorimeter::checkCMTX(T4SCAL& cmtxLine)
{
  // -- Module ID correction check
  int index = 0;
  while ((index = getCollision(cmtxLine.detectorId)) != -1) {
    cmtxLine.detectorId = usedIDs.at(index).first + usedIDs.at(index).second;
    T4SMessenger::getInstance()->printMessage(T4SWarning, __LINE__, __FILE__,
        "T4SCalorimeter::checkCMTX: Same detector id detected for " + cmtxLine.detName + " " + cmtxLine.moduleName);
  }

  usedIDs.push_back(std::pair<int, int>(cmtxLine.detectorId, cmtxLine.nmod));
}
