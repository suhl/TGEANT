#include "T4SEfficiency.hh"

void T4SEfficiency::readXML(string _inFile)
{
  myParser = new T4XMLParser();

  if (!myParser->parseFile(_inFile)) {
      T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
              __FILE__, "T4SEfficiency could not parse file: '" + _inFile + "'");
      return;
  }

  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__,
                __FILE__, "Parsing T4SEfficiency file: '" + _inFile + "'");

  detList.clear();
  if (myParser->getBaseNodeCount("DetectorEfficiency") > 0) {
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorEfficiency"); i++) {
      T4SWireDetector newWireDet;
      myParser->getChildValue("DetectorEfficiency",i,"tbName",newWireDet.tbName);
      myParser->getChildValue("DetectorEfficiency",i,"det",newWireDet.det);
      myParser->getChildValue("DetectorEfficiency",i,"unit",newWireDet.unit);

      //read values needed
      myParser->getChildValue("DetectorEfficiency",i,"radLength",newWireDet.radLength);
      myParser->getChildValue("DetectorEfficiency",i,"effic",newWireDet.effic);
      myParser->getChildValue("DetectorEfficiency",i,"backgr",newWireDet.backgr);
      myParser->getChildValue("DetectorEfficiency",i,"tgate",newWireDet.tgate);
      myParser->getChildValue("DetectorEfficiency",i,"drVel",newWireDet.drVel);
      myParser->getChildValue("DetectorEfficiency",i,"t0",newWireDet.t0);
      myParser->getChildValue("DetectorEfficiency",i,"res2hit",newWireDet.res2hit);
      myParser->getChildValue("DetectorEfficiency",i,"space",newWireDet.space);
      myParser->getChildValue("DetectorEfficiency",i,"tslice",newWireDet.tslice);
      detList.push_back(newWireDet);
    }
  }

  magList.clear();
  if (myParser->getBaseNodeCount("MagnetParams") > 0) {
    for (int i = 0; i < myParser->getBaseNodeCount("MagnetParams"); i++) {
      T4SMagnetLine newMagnet;
      myParser->getChildValue("MagnetParams", i, "number", newMagnet.number);
      myParser->getChildValue("MagnetParams", i, "flag1", newMagnet.flag1);
      myParser->getChildValue("MagnetParams", i, "flag2", newMagnet.flag2);
      myParser->getChildValue("MagnetParams", i, "rot", newMagnet.rot);

      magList.push_back(newMagnet);
    }
  }
}

void T4SEfficiency::writeXML(string _inFile)
{
  myWriter = new T4XMLWriter;
  for (unsigned int i = 0; i < detList.size(); i++) {
    myWriter->addNode("DetectorEfficiency");
    //for identification, save these also
    myWriter->createDataNode("DetectorEfficiency",i,"tbName",detList.at(i).tbName);
    myWriter->createDataNode("DetectorEfficiency",i,"det",detList.at(i).det);
    myWriter->createDataNode("DetectorEfficiency",i,"unit",detList.at(i).unit);

    //now save the values needed
    myWriter->createDataNode("DetectorEfficiency",i,"radLength",detList.at(i).radLength);
    myWriter->createDataNode("DetectorEfficiency",i,"effic",detList.at(i).effic);
    myWriter->createDataNode("DetectorEfficiency",i,"backgr",detList.at(i).backgr);
    myWriter->createDataNode("DetectorEfficiency",i,"tgate",detList.at(i).tgate);
    myWriter->createDataNode("DetectorEfficiency",i,"drVel",detList.at(i).drVel);
    myWriter->createDataNode("DetectorEfficiency",i,"t0",detList.at(i).t0);
    myWriter->createDataNode("DetectorEfficiency",i,"res2hit",detList.at(i).res2hit);
    myWriter->createDataNode("DetectorEfficiency",i,"space",detList.at(i).space);
    myWriter->createDataNode("DetectorEfficiency",i,"tslice",detList.at(i).tslice);
  }

  for (unsigned int i = 0; i < magList.size(); i++){
    myWriter->addNode("MagnetParams");
    myWriter->createDataNode("MagnetParams",i,"number",magList.at(i).number);
    myWriter->createDataNode("MagnetParams",i,"flag1",magList.at(i).flag1);
    myWriter->createDataNode("MagnetParams",i,"flag2",magList.at(i).flag2);
    myWriter->createDataNode("MagnetParams",i,"rot",magList.at(i).rot);
  }

  myWriter->createFile(_inFile);
  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
          "T4SEfficiency '" + _inFile + "' has been created.");
  delete myWriter;
  myWriter = NULL;
}

T4SEfficiency::~T4SEfficiency()
{

}

void T4SEfficiency::addWireInfoToExistingStruct(T4SWireDetector& _source, T4SWireDetector& destination)
{
  destination.backgr = _source.backgr;
  destination.radLength = _source.radLength;
  destination.effic = _source.effic;
  destination.tgate = _source.tgate;
  destination.drVel = _source.drVel;
  destination.t0 = _source.t0;
  destination.res2hit = _source.res2hit;
  destination.space = _source.space;
  destination.tslice = _source.tslice;
}

bool T4SEfficiency::addWireInformationToStruct(T4SWireDetector& writeTo)
{
  vector<T4SWireDetector>::iterator position = detList.begin();
  vector<T4SWireDetector> results;

  //this checks ONLY for tbname!
  for (unsigned i = 0; i < detList.size(); i++)
    if (detList.at(i).tbName == writeTo.tbName)
      results.push_back(detList.at(i));

  if (results.size() == 1) {
    addWireInfoToExistingStruct(results.at(0), writeTo);
    return true;
  }

  vector<T4SWireDetector> resultsDet;
  if (results.size() > 1) {
    for (unsigned int i = 0; i < results.size(); i++) {
      if (results.at(i).det == writeTo.det) {
        resultsDet.push_back(results.at(i));
      }
    }
  }

  if (resultsDet.size() == 1) {
    addWireInfoToExistingStruct(resultsDet.at(0), writeTo);
    return true;
  }

  vector<T4SWireDetector> resultsDetUnit;
  if (resultsDet.size() > 1) {
    for (unsigned int i = 0; i < resultsDet.size(); i++) {
      if (resultsDet.at(i).unit == writeTo.unit) {
        resultsDetUnit.push_back(resultsDet.at(i));
      }
    }
  }

  if (resultsDetUnit.size() == 1) {
    addWireInfoToExistingStruct(resultsDetUnit.at(0), writeTo);
    return true;
  }

  if (resultsDetUnit.size() > 1) {
    T4SMessenger::getInstance()->printfMessage(T4SWarning, __LINE__, __FILE__,
        "T4SEfficiency multiple results for combination tbname %s, det %s, unit %i !! \n",
        writeTo.tbName.c_str(), writeTo.det.c_str(), writeTo.unit);
    return false;
  }

  T4SMessenger::getInstance()->printfMessage(T4SWarning, __LINE__, __FILE__,
      "T4SEfficiency no results for tbname %s, det %s, unit %i !! \n",
      writeTo.tbName.c_str(), writeTo.det.c_str(), writeTo.unit);
  return false;
}

void T4SEfficiency::importEfficiencyFromDetDat(string _inDetDat)
{
  T4SDetectorsDat myDetDat;
  myDetDat.load(_inDetDat);
  map<int, vector<string> >::const_iterator myIter;
  detList.clear();
  magList.clear();
  for (myIter = myDetDat.getDetMap().begin();
      myIter != myDetDat.getDetMap().end(); myIter++) {
    T4SWireDetector myNewDet;
    if (myIter->second.size() < 25) {
      T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal, __LINE__,
      __FILE__,
          "Error in T4SEfficiency detectors.dat import! Too few entries (%u should be > 25) in this det-line! Maybe you tried a real  data detectors.dat? Please use a comgeant detectors.dat!\n",
          myIter->second.size());
    }
    myNewDet.tbName = myIter->second.at(DETDAT_TBNAME); //.at(DETDAT_TBNAME);
    myNewDet.det = myIter->second.at(DETDAT_DET);
    myNewDet.unit = strToInt(myIter->second.at(DETDAT_UNIT));

    myNewDet.radLength = strToDouble(myIter->second.at(DETDAT_RADLEN));
    myNewDet.effic = strToDouble(myIter->second.at(DETDAT_EFFIC));
    myNewDet.backgr = strToDouble(myIter->second.at(DETDAT_BACKGR));
    myNewDet.tgate = strToDouble(myIter->second.at(DETDAT_TGATE));
    myNewDet.drVel = strToDouble(myIter->second.at(DETDAT_DRVEL));
    myNewDet.t0 = strToDouble(myIter->second.at(DETDAT_T0));
    myNewDet.res2hit = strToDouble(myIter->second.at(DETDAT_RES2HIT));
    myNewDet.space = strToDouble(myIter->second.at(DETDAT_SPACE));
    myNewDet.tslice = strToDouble(myIter->second.at(DETDAT_TSLICE));
    detList.push_back(myNewDet);
  }
  for (myIter = myDetDat.getMagMap().begin();
      myIter != myDetDat.getMagMap().end(); myIter++) {
    T4SMagnetLine myNewMag;
    if (myIter->second.size() < 9) {
      T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal, __LINE__,
      __FILE__,
          "Error in T4SEfficiency detectors.dat import! Too few entries (%u should be > 10) in this magnet-line! \n",
          myIter->second.size());

    }
    myNewMag.number = strToInt(myIter->second.at(MAG_ID));
    myNewMag.flag1 = strToDouble(myIter->second.at(MAG_FLAG1));
    myNewMag.flag2 = strToDouble(myIter->second.at(MAG_FLAG2));
    myNewMag.rot = strToInt(myIter->second.at(MAG_ROT));
    magList.push_back(myNewMag);
  }
}
