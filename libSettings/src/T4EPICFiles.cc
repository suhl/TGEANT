#include "T4EPICFiles.hh"
#include "T4SMessenger.hh"
bool T4EPICFiles::instanceFlag = false;
T4EPICFiles* T4EPICFiles::single = NULL;


T4EPICFiles* T4EPICFiles::getInstance ( void ) {
  if ( !instanceFlag ) {
    single = new T4EPICFiles();
    instanceFlag = true;
    return single;
  }
  else {
    return single;
  }
}

double T4EPICFiles::getCalibForModule ( int _caloNum, int _moduleNum ) {
  if (!isActive)
    return 1.;

  if (_caloNum < CALONAME_ECAL0 || _caloNum > CALONAME_ECAL2){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Only can use pi0 calibration with ECALs, you wanted calo number: %i\n",_caloNum);
    return 1.;
  }
  map<int,caloCalib>::iterator targetCalib = getCalo(_caloNum)->find(_moduleNum);
  if (targetCalib == getCalo(_caloNum)->end()){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Cannot find module number %i in calo %i\n",_moduleNum,_caloNum);
    return 1.;
  }
  return targetCalib->second.calibrationValue;
}


T4EPICFiles::T4EPICFiles ( void ) {
  isActive = false;
}

T4EPICFiles::~T4EPICFiles ( void ) {

}

void T4EPICFiles::loadFileName ( string _fileName, int _ecalNum ) {
  map<int,caloCalib>* mapToWrite = getCalo(_ecalNum);
  if (mapToWrite == NULL){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Cannot load file %s into NULLpointer\n",_fileName.c_str());
    return;
  }
  ifstream myFile;
  myFile.open(_fileName.c_str(),ios::in);
  if (!myFile.is_open()){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Cannot open file %s \n",_fileName.c_str());
    return;
  }
  if (!myFile.good()){
    T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"File %s is not good! \n",_fileName.c_str());
    return;
  }
  string line;
  while (getline(myFile,line)) {
    vector<string> boomList = explodeString(line);
    if (boomList.size() < 4){
      T4SMessenger::getInstance()->printfMessage(T4SWarning,__LINE__,__FILE__,"Could not parse the following line '%s' -- continuing anyway!\n",line.c_str());
      continue;
    }
    caloCalib newStruct;
    newStruct.caloNum = _ecalNum;
    newStruct.moduleNum = strToInt(boomList.at(0));
    newStruct.readName = boomList.at(1);
    newStruct.aux1 = strToInt(boomList.at(2));
    newStruct.aux2 = strToInt(boomList.at(3));
    newStruct.calibrationValue = strToDouble(boomList.at(4));
    (*mapToWrite)[newStruct.moduleNum] = newStruct;
  }
  isActive = true;
}


std::map< int, caloCalib >* T4EPICFiles::getCalo ( int _caloNum ) {
  switch (_caloNum){
    case CALONAME_ECAL0: return &ECAL0; break;
    case CALONAME_ECAL1: return &ECAL1; break;
    case CALONAME_ECAL2: return &ECAL2; break;
    default: T4SMessenger::getInstance()->printfMessage(T4SErrorNonFatal,__LINE__,__FILE__,"Wrong calo number chosen: %i \n",_caloNum); return NULL;
  };
}

pair< int, int > T4EPICFiles::getXYMod ( caloCalib _in ) {
  vector<string> boomList = explodeStringCustomDelim(_in.readName,"_");
  if (boomList.size() < 4){
    T4SMessenger::getInstance()->printfMessage(T4SWarning,__LINE__,__FILE__,"Could not parse the following name '%s' -- continuing anyway!\n",_in.readName.c_str());
    return make_pair(-1,-1);
  }
  int x = strToInt(boomList.at(1));
  int y = strToInt(boomList.at(3));
  return make_pair(x,y);
}

