#include "T4SSettingsFileXML.hh"
#include "T4SMessenger.hh"

T4SSettingsFileXML::T4SSettingsFileXML(T4SStructManager *_structM) :
    T4SFileBackEnd()
{
    structM = _structM;

    myParser = NULL;
    myWriter = NULL;
}

T4SSettingsFileXML::~T4SSettingsFileXML(void)
{
    if (myParser != NULL)
        delete myParser;
    if (myWriter != NULL)
        delete myWriter;
}

void T4SSettingsFileXML::save(string fileName)
{
    string lastSymbol = fileName;
    if (fileName.size() > 4) {
        lastSymbol = lastSymbol.erase(0, fileName.size() - 4); // check for .xml
        if (lastSymbol != ".xml")
            fileName += ".xml";
    } else {
        fileName += ".xml";
    }

    myWriter = new T4XMLWriter;

    //write the general settings struct
    myWriter->addNode("GeneralSettings");
    myWriter->createDataNode("GeneralSettings", 0, "physicsList",
                             structM->getGeneral()->physicsList);
    myWriter->createDataNode("GeneralSettings", 0, "useGflash",
                             structM->getGeneral()->useGflash);
    myWriter->createDataNode("GeneralSettings", 0, "outputPath",
                             structM->getGeneral()->outputPath);
    myWriter->createDataNode("GeneralSettings", 0, "useVisualization",
                             structM->getGeneral()->useVisualization);
    myWriter->createDataNode("GeneralSettings", 0, "useSeed",
                             structM->getGeneral()->useSeed);
    myWriter->createDataNode("GeneralSettings", 0, "seed",
                             structM->getGeneral()->seed);
    myWriter->createDataNode("GeneralSettings", 0, "runName",
                             structM->getGeneral()->runName);
    myWriter->createDataNode("GeneralSettings", 0, "saveASCII",
                             structM->getGeneral()->saveASCII);
    myWriter->createDataNode("GeneralSettings", 0, "saveBinary",
                             structM->getGeneral()->saveBinary);
    myWriter->createDataNode("GeneralSettings", 0, "saveDetDat",
                             structM->getGeneral()->saveDetDat);
    myWriter->createDataNode("GeneralSettings", 0, "useTrigger",
                             structM->getGeneral()->useTrigger);
    myWriter->createDataNode("GeneralSettings", 0, "exportGDML",
                             structM->getGeneral()->exportGDML);
    myWriter->createDataNode("GeneralSettings", 0, "namingWithSeed",
                             structM->getGeneral()->namingWithSeed);
    myWriter->createDataNode("GeneralSettings", 0, "verboseLevel",
                             structM->getGeneral()->verboseLevel);
    myWriter->createDataNode("GeneralSettings", 0, "usePerformanceMonitor",
                             structM->getGeneral()->usePerformanceMonitor);
    myWriter->createDataNode("GeneralSettings", 0, "checkOverlap",
                             structM->getGeneral()->checkOverlap);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsGlobal",
                             structM->getGeneral()->productionCutsGlobal);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsGams",
                             structM->getGeneral()->productionCutsGams);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsRHGams",
                             structM->getGeneral()->productionCutsRHGams);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsMainz",
                             structM->getGeneral()->productionCutsMainz);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsOlga",
                             structM->getGeneral()->productionCutsOlga);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsShashlik",
                             structM->getGeneral()->productionCutsShashlik);
    myWriter->createDataNode("GeneralSettings", 0, "productionCutsHcal",
                             structM->getGeneral()->productionCutsHcal);
    myWriter->createDataNode("GeneralSettings", 0, "noSecondaries",
                             structM->getGeneral()->noSecondaries);
    myWriter->createDataNode("GeneralSettings", 0, "colorTheme",
                             structM->getGeneral()->colorTheme);
    myWriter->createDataNode("GeneralSettings", 0, "simplifiedGeometries",
                             structM->getGeneral()->simplifiedGeometries);
    myWriter->createDataNode("GeneralSettings", 0, "noMemoryLimitation",
                             structM->getGeneral()->noMemoryLimitation);
    myWriter->createDataNode("GeneralSettings", 0, "useSplitting",
                             structM->getGeneral()->useSplitting);
    myWriter->createDataNode("GeneralSettings", 0, "eventsPerChunk",
                             structM->getGeneral()->eventsPerChunk);
    myWriter->createDataNode("GeneralSettings", 0, "useSplitPiping",
                             structM->getGeneral()->useSplitPiping);
    myWriter->createDataNode("GeneralSettings", 0, "triggerPlugin",
                             structM->getGeneral()->triggerPlugin);
    

    //write the paths struct
    myWriter->addNode("ExternalPaths");
    myWriter->createDataNode("ExternalPaths", 0, "localGeneratorFile",
                             structM->getExternal()->localGeneratorFile);
    myWriter->createDataNode("ExternalPaths", 0, "beamFile",
                             structM->getExternal()->beamFile);
    myWriter->createDataNode("ExternalPaths", 0, "beamFileForAdditionalPileUp",
                             structM->getExternal()->beamFileForAdditionalPileUp);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixInnerX",
                             structM->getExternal()->triggerMatrixInnerX);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixOuterY",
                             structM->getExternal()->triggerMatrixOuterY);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixLadderX",
                             structM->getExternal()->triggerMatrixLadderX);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixMiddleX",
                             structM->getExternal()->triggerMatrixMiddleX);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixMiddleY",
                             structM->getExternal()->triggerMatrixMiddleY);
    myWriter->createDataNode("ExternalPaths", 0, "triggerMatrixLAST",
                             structM->getExternal()->triggerMatrixLAST);
    myWriter->createDataNode("ExternalPaths", 0, "libHEPGen_Pi0",
                             structM->getExternal()->libHEPGen_Pi0);
    myWriter->createDataNode("ExternalPaths", 0, "libHEPGen_Pi0_transv",
                             structM->getExternal()->libHEPGen_Pi0_transv);
    myWriter->createDataNode("ExternalPaths", 0, "libHEPGen_Rho",
                             structM->getExternal()->libHEPGen_Rho);
    myWriter->createDataNode("ExternalPaths", 0, "visualizationMacro",
                             structM->getExternal()->visualizationMacro);
    myWriter->createDataNode("ExternalPaths", 0, "detectorEfficiency",
                             structM->getExternal()->detectorEfficiency);
    myWriter->createDataNode("ExternalPaths", 0, "calorimeterInfo",
                             structM->getExternal()->calorimeterInfo);

    //write beam struct
    myWriter->addNode("BeamSettings");
    myWriter->createDataNode("BeamSettings", 0, "numParticles",
                             (int) structM->getBeam()->numParticles);
    myWriter->createDataNode("BeamSettings", 0, "beamParticle",
                             structM->getBeam()->beamParticle);
    myWriter->createDataNode("BeamSettings", 0, "beamPlugin",
                             structM->getBeam()->beamPlugin);
    myWriter->createDataNode("BeamSettings", 0, "beamEnergy",
                             structM->getBeam()->beamEnergy);
    myWriter->createDataNode("BeamSettings", 0, "useBeamfile",
                             structM->getBeam()->useBeamfile);
    myWriter->createDataNode("BeamSettings", 0, "beamFileType",
                             structM->getBeam()->beamFileType);
    myWriter->createDataNode("BeamSettings", 0, "beamFileZConvention",
                             structM->getBeam()->beamFileZConvention);
    myWriter->createDataNode("BeamSettings", 0, "beamZStart",
                             structM->getBeam()->beamZStart);
    myWriter->createDataNode("BeamSettings", 0, "beamFileBackend",
                             structM->getBeam()->beamFileBackend);
    myWriter->createDataNode("BeamSettings", 0, "usePileUp",
                             structM->getBeam()->usePileUp);
    myWriter->createDataNode("BeamSettings", 0, "beamFlux",
                             structM->getBeam()->beamFlux);
    myWriter->createDataNode("BeamSettings", 0, "timeGate",
                             structM->getBeam()->timeGate);
    myWriter->createDataNode("BeamSettings", 0, "useAdditionalPileUp",
                             structM->getBeam()->useAdditionalPileUp);
    myWriter->createDataNode("BeamSettings", 0, "additionalPileUpFlux",
                             structM->getBeam()->additionalPileUpFlux);
    myWriter->createDataNode("BeamSettings", 0, "beamFileZConventionForAdditionalPileUp",
                             structM->getBeam()->beamFileZConventionForAdditionalPileUp);
    myWriter->createDataNode("BeamSettings", 0, "useTargetExtrap",
                             structM->getBeam()->useTargetExtrap);
    myWriter->createDataNode("BeamSettings", 0, "targetStepLimit",
                             structM->getBeam()->targetStepLimit);
    myWriter->createDataNode("BeamSettings", 0, "useHadronicInteractionEGCall",
                             structM->getBeam()->useHadronicInteractionEGCall);
    


    //check for generator-settings and write usermode-structs
    if (structM->getBeam()->beamPlugin == "User") {
        myWriter->addNode("UserModeSettings");
        myWriter->createDataNode("UserModeSettings", 0, "xPos",
                                 structM->getUser()->position[0]);
        myWriter->createDataNode("UserModeSettings", 0, "yPos",
                                 structM->getUser()->position[1]);
        myWriter->createDataNode("UserModeSettings", 0, "zPos",
                                 structM->getUser()->position[2]);
        myWriter->createDataNode("UserModeSettings", 0, "xMom",
                                 structM->getUser()->momentum[0]);
        myWriter->createDataNode("UserModeSettings", 0, "yMom",
                                 structM->getUser()->momentum[1]);
        myWriter->createDataNode("UserModeSettings", 0, "zMom",
                                 structM->getUser()->momentum[2]);
        myWriter->createDataNode("UserModeSettings", 0, "useRandomEnergy",
                                 structM->getUser()->useRandomEnergy);
        myWriter->createDataNode("UserModeSettings", 0, "minEnergy",
                                 structM->getUser()->minEnergy);
        myWriter->createDataNode("UserModeSettings", 0, "maxEnergy",
                                 structM->getUser()->maxEnergy);
    }

    //do the cosmics struct
    if (structM->getBeam()->beamPlugin == "Cosmics") {
        myWriter->addNode("CosmicsSettings");
        myWriter->createDataNode("CosmicsSettings", 0, "angleVariation",
                                 structM->getCosmic()->angleVariation);
        myWriter->createDataNode("CosmicsSettings", 0, "xPos",
                                 structM->getCosmic()->positionX);
        myWriter->createDataNode("CosmicsSettings", 0, "zPos",
                                 structM->getCosmic()->positionZ);
        myWriter->createDataNode("CosmicsSettings", 0, "xVariation",
                                 structM->getCosmic()->variationX);
        myWriter->createDataNode("CosmicsSettings", 0, "zVariation",
                                 structM->getCosmic()->variationZ);
    }

    //do the ecalcalib struct
    if (structM->getBeam()->beamPlugin == "EcalCalib") {
        myWriter->addNode("EcalCalibSettings");
        myWriter->createDataNode("EcalCalibSettings", 0, "positionZ",
                                 structM->getEcalCalib()->positionZ);
        myWriter->createDataNode("EcalCalibSettings", 0, "positionXMin",
                                 structM->getEcalCalib()->positionXMin);
        myWriter->createDataNode("EcalCalibSettings", 0, "positionXMax",
                                 structM->getEcalCalib()->positionXMax);
        myWriter->createDataNode("EcalCalibSettings", 0, "positionYMin",
                                 structM->getEcalCalib()->positionYMin);
        myWriter->createDataNode("EcalCalibSettings", 0, "positionYMax",
                                 structM->getEcalCalib()->positionYMax);
        myWriter->createDataNode("EcalCalibSettings", 0, "energyMin",
                                 structM->getEcalCalib()->energyMin);
        myWriter->createDataNode("EcalCalibSettings", 0, "energyMax",
                                 structM->getEcalCalib()->energyMax);
    }

    //write the pythia struct -- not anymore needed, handled in T4PythiaTuning
    if (structM->getBeam()->beamPlugin == "PYTHIA") {
    }
    //write the hepgen struct
    if (structM->getBeam()->beamPlugin == "HEPGEN" || structM->getBeam()->beamPlugin == "EventGen") {
        myWriter->addNode("HEPGenSettings");
        myWriter->createDataNode("HEPGenSettings", 0, "target",
                                 structM->getHEPGen()->target);
        myWriter->createDataNode("HEPGenSettings", 0, "IVECM",
                                 structM->getHEPGen()->IVECM);
        myWriter->createDataNode("HEPGenSettings", 0, "LST",
                                 structM->getHEPGen()->LST);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT0",
                                 structM->getHEPGen()->CUT[0]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT1",
                                 structM->getHEPGen()->CUT[1]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT2",
                                 structM->getHEPGen()->CUT[2]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT3",
                                 structM->getHEPGen()->CUT[3]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT4",
                                 structM->getHEPGen()->CUT[4]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT5",
                                 structM->getHEPGen()->CUT[5]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT6",
                                 structM->getHEPGen()->CUT[6]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT7",
                                 structM->getHEPGen()->CUT[7]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT8",
                                 structM->getHEPGen()->CUT[8]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT9",
                                 structM->getHEPGen()->CUT[9]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT10",
                                 structM->getHEPGen()->CUT[10]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT11",
                                 structM->getHEPGen()->CUT[11]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT12",
                                 structM->getHEPGen()->CUT[12]);
        myWriter->createDataNode("HEPGenSettings", 0, "CUT13",
                                 structM->getHEPGen()->CUT[13]);

        myWriter->createDataNode("HEPGenSettings", 0, "THMAX",
                                 structM->getHEPGen()->THMAX);
        myWriter->createDataNode("HEPGenSettings", 0, "alf",
                                 structM->getHEPGen()->alf);
        myWriter->createDataNode("HEPGenSettings", 0, "probc",
                                 structM->getHEPGen()->probc);
        myWriter->createDataNode("HEPGenSettings", 0, "bcoh",
                                 structM->getHEPGen()->bcoh);
        myWriter->createDataNode("HEPGenSettings", 0, "bin",
                                 structM->getHEPGen()->bin);
        myWriter->createDataNode("HEPGenSettings", 0, "clept",
                                 structM->getHEPGen()->clept);
        myWriter->createDataNode("HEPGenSettings", 0, "slept",
                                 structM->getHEPGen()->slept);
        myWriter->createDataNode("HEPGenSettings", 0, "B0",
                                 structM->getHEPGen()->B0);
        myWriter->createDataNode("HEPGenSettings", 0, "xbj0",
                                 structM->getHEPGen()->xbj0);
        myWriter->createDataNode("HEPGenSettings", 0, "alphap",
                                 structM->getHEPGen()->alphap);
        myWriter->createDataNode("HEPGenSettings", 0, "TLIM0",
                                 structM->getHEPGen()->TLIM[0]);
        myWriter->createDataNode("HEPGenSettings", 0, "TLIM1",
                                 structM->getHEPGen()->TLIM[1]);
    }
    //write the primgen struct
    if (structM->getBeam()->beamPlugin == "Primakoff") {
        myWriter->addNode("PrimGenSettings");
        myWriter->createDataNode("PrimGenSettings", 0, "targetZ",
                                 structM->getPrimGen()->Z);
        myWriter->createDataNode("PrimGenSettings", 0, "particle",
                                 structM->getPrimGen()->particle);
        myWriter->createDataNode("PrimGenSettings", 0, "alpha_1",
                                 structM->getPrimGen()->alpha_1);
        myWriter->createDataNode("PrimGenSettings", 0, "beta_1",
                                 structM->getPrimGen()->beta_1);
        myWriter->createDataNode("PrimGenSettings", 0, "alpha_2",
                                 structM->getPrimGen()->alpha_2);
        myWriter->createDataNode("PrimGenSettings", 0, "beta_2",
                                 structM->getPrimGen()->beta_2);
        myWriter->createDataNode("PrimGenSettings", 0, "s_min",
                                 structM->getPrimGen()->s_min);
        myWriter->createDataNode("PrimGenSettings", 0, "s_max",
                                 structM->getPrimGen()->s_max);
        myWriter->createDataNode("PrimGenSettings", 0, "Q2_max",
                                 structM->getPrimGen()->Q2_max);
        myWriter->createDataNode("PrimGenSettings", 0, "Egamma_min",
                                 structM->getPrimGen()->Egamma_min);
        myWriter->createDataNode("PrimGenSettings", 0, "pbeam_min",
                                 structM->getPrimGen()->pbeam_min);
        myWriter->createDataNode("PrimGenSettings", 0, "pbeam_max",
                                 structM->getPrimGen()->pbeam_max);
        myWriter->createDataNode("PrimGenSettings", 0, "born",
                                 structM->getPrimGen()->born);
        myWriter->createDataNode("PrimGenSettings", 0, "RhoContrib",
                                 structM->getPrimGen()->rhoContrib);
        myWriter->createDataNode("PrimGenSettings", 0, "ChiralLoops",
                                 structM->getPrimGen()->chiralLoops);
        myWriter->createDataNode("PrimGenSettings", 0, "Polarizabilities",
                                 structM->getPrimGen()->polarizabilities);
        myWriter->createDataNode("PrimGenSettings", 0, "radcorr",
                                 structM->getPrimGen()->radcorr);
    }
    //write the primTrig struct
    if (structM->getGeneral()->triggerPlugin == "Primakoff2012") {
        myWriter->addNode("PrimTriggerSettings");
        myWriter->createDataNode("PrimTriggerSettings", 0, "thresholdPrim1",
                                 structM->getPrimTrig()->threshPrim1);
        myWriter->createDataNode("PrimTriggerSettings", 0, "thresholdPrim2",
                                 structM->getPrimTrig()->threshPrim2);
        myWriter->createDataNode("PrimTriggerSettings", 0, "thresholdPrim3",
                                 structM->getPrimTrig()->threshPrim3);
        myWriter->createDataNode("PrimTriggerSettings", 0, "thresholdPrim2Veto",
                                 structM->getPrimTrig()->threshPrim2Veto);
    }
    if (structM->getGeneral()->physicsList == "FULL") {
        myWriter->addNode("PhysicsListSampling");
        myWriter->createDataNode("PhysicsListSampling", 0, "windowSize",
                                 (int) structM->getSampling()->windowSize);
        myWriter->createDataNode("PhysicsListSampling", 0, "binsPerNanoSecond",
                                 (int) structM->getSampling()->binsPerNanoSecond);
        myWriter->createDataNode("PhysicsListSampling", 0, "useBaseline",
                                 structM->getSampling()->useBaseline);
        myWriter->createDataNode("PhysicsListSampling", 0, "baseline",
                                 structM->getSampling()->baseline);
    }

    for (unsigned int i = 0; i < structM->getRPD()->size(); i++)
        if (structM->getRPD()->at(i).general.useDetector == true)
            saveRPD(i);

    for (unsigned int i = 0; i < structM->getMagnet()->size(); i++)
        if (structM->getMagnet()->at(i).general.useDetector == true)
            saveMagnet(i);

    if (structM->getRICH()->general.useDetector == true) {
        myWriter->addNode("DetectorRICH");
        myWriter->createDataNode("DetectorRICH", 0, "name",
                                 structM->getRICH()->general.name);
        myWriter->createDataNode("DetectorRICH", 0, "xPos",
                                 structM->getRICH()->general.position[0]);
        myWriter->createDataNode("DetectorRICH", 0, "yPos",
                                 structM->getRICH()->general.position[1]);
        myWriter->createDataNode("DetectorRICH", 0, "zPos",
                                 structM->getRICH()->general.position[2]);
        myWriter->createDataNode("DetectorRICH", 0, "useMechanicalStructure",
                                 structM->getRICH()->general.useMechanicalStructure);
        myWriter->createDataNode("DetectorRICH", 0, "useOpticalPhysics",
                                 structM->getRICH()->useOpticalPhysics);
        myWriter->createDataNode("DetectorRICH", 0, "visibleHousing",
                                 structM->getRICH()->visibleHousing);
        myWriter->createDataNode("DetectorRICH", 0, "gas",
                                 structM->getRICH()->gas);
    }

    if (structM->getMW1()->general.useDetector == true) {
        myWriter->addNode("DetectorMW1");
        myWriter->createDataNode("DetectorMW1", 0, "name",
                                 structM->getMW1()->general.name);
        myWriter->createDataNode("DetectorMW1", 0, "xPos",
                                 structM->getMW1()->general.position[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yPos",
                                 structM->getMW1()->general.position[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zPos",
                                 structM->getMW1()->general.position[2]);
        myWriter->createDataNode("DetectorMW1", 0, "useMechanicalStructure",
                                 structM->getMW1()->general.useMechanicalStructure);
        myWriter->createDataNode("DetectorMW1", 0, "useAbsorber",
                                 structM->getMW1()->useAbsorber);
        myWriter->createDataNode("DetectorMW1", 0, "useMA01X1",
                                 structM->getMW1()->useMA01X1);
        myWriter->createDataNode("DetectorMW1", 0, "useMA01X3",
                                 structM->getMW1()->useMA01X3);
        myWriter->createDataNode("DetectorMW1", 0, "useMA01Y1",
                                 structM->getMW1()->useMA01Y1);
        myWriter->createDataNode("DetectorMW1", 0, "useMA01Y3",
                                 structM->getMW1()->useMA01Y3);
        myWriter->createDataNode("DetectorMW1", 0, "useMA02X3",
                                 structM->getMW1()->useMA02X3);
        myWriter->createDataNode("DetectorMW1", 0, "useMA02X1",
                                 structM->getMW1()->useMA02X1);
        myWriter->createDataNode("DetectorMW1", 0, "useMA02Y1",
                                 structM->getMW1()->useMA02Y1);
        myWriter->createDataNode("DetectorMW1", 0, "useMA02Y3",
                                 structM->getMW1()->useMA02Y3);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA01X1",
                                 structM->getMW1()->posMA01X1[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA01X1",
                                 structM->getMW1()->posMA01X1[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA01X1",
                                 structM->getMW1()->posMA01X1[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA01X3",
                                 structM->getMW1()->posMA01X3[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA01X3",
                                 structM->getMW1()->posMA01X3[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA01X3",
                                 structM->getMW1()->posMA01X3[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA01Y1",
                                 structM->getMW1()->posMA01Y1[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA01Y1",
                                 structM->getMW1()->posMA01Y1[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA01Y1",
                                 structM->getMW1()->posMA01Y1[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA01Y3",
                                 structM->getMW1()->posMA01Y3[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA01Y3",
                                 structM->getMW1()->posMA01Y3[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA01Y3",
                                 structM->getMW1()->posMA01Y3[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA02X1",
                                 structM->getMW1()->posMA02X1[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA02X1",
                                 structM->getMW1()->posMA02X1[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA02X1",
                                 structM->getMW1()->posMA02X1[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA02X3",
                                 structM->getMW1()->posMA02X3[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA02X3",
                                 structM->getMW1()->posMA02X3[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA02X3",
                                 structM->getMW1()->posMA02X3[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA02Y1",
                                 structM->getMW1()->posMA02Y1[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA02Y1",
                                 structM->getMW1()->posMA02Y1[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA02Y1",
                                 structM->getMW1()->posMA02Y1[2]);

        myWriter->createDataNode("DetectorMW1", 0, "xposMA02Y3",
                                 structM->getMW1()->posMA02Y3[0]);
        myWriter->createDataNode("DetectorMW1", 0, "yposMA02Y3",
                                 structM->getMW1()->posMA02Y3[1]);
        myWriter->createDataNode("DetectorMW1", 0, "zposMA02Y3",
                                 structM->getMW1()->posMA02Y3[2]);

    }

    if (structM->getMW2()->general.useDetector == true) {

        myWriter->addNode("DetectorMW2");
        myWriter->createDataNode("DetectorMW2", 0, "name",
                                 structM->getMW2()->general.name);
        myWriter->createDataNode("DetectorMW2", 0, "xPos",
                                 structM->getMW2()->general.position[0]);
        myWriter->createDataNode("DetectorMW2", 0, "yPos",
                                 structM->getMW2()->general.position[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zPos",
                                 structM->getMW2()->general.position[2]);
        myWriter->createDataNode("DetectorMW2", 0, "useMechanicalStructure",
                                 structM->getMW2()->general.useMechanicalStructure);
        myWriter->createDataNode("DetectorMW2", 0, "useAbsorber",
                                 structM->getMW2()->useAbsorber);
        myWriter->createDataNode("DetectorMW2", 0, "MB01X",
                                 structM->getMW2()->MB01X);
        myWriter->createDataNode("DetectorMW2", 0, "MB01Y",
                                 structM->getMW2()->MB01Y);
        myWriter->createDataNode("DetectorMW2", 0, "MB01V",
                                 structM->getMW2()->MB01V);
        myWriter->createDataNode("DetectorMW2", 0, "MB02X",
                                 structM->getMW2()->MB02X);
        myWriter->createDataNode("DetectorMW2", 0, "MB02Y",
                                 structM->getMW2()->MB02Y);
        myWriter->createDataNode("DetectorMW2", 0, "MB02V",
                                 structM->getMW2()->MB02V);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB01X",
                                 structM->getMW2()->positionMB01X[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB01X",
                                 structM->getMW2()->positionMB01X[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB01X",
                                 structM->getMW2()->positionMB01X[2]);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB01Y",
                                 structM->getMW2()->positionMB01Y[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB01Y",
                                 structM->getMW2()->positionMB01Y[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB01Y",
                                 structM->getMW2()->positionMB01Y[2]);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB01V",
                                 structM->getMW2()->positionMB01V[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB01V",
                                 structM->getMW2()->positionMB01V[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB01V",
                                 structM->getMW2()->positionMB01V[2]);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB02X",
                                 structM->getMW2()->positionMB02X[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB02X",
                                 structM->getMW2()->positionMB02X[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB02X",
                                 structM->getMW2()->positionMB02X[2]);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB02Y",
                                 structM->getMW2()->positionMB02Y[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB02Y",
                                 structM->getMW2()->positionMB02Y[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB02Y",
                                 structM->getMW2()->positionMB02Y[2]);

        myWriter->createDataNode("DetectorMW2", 0, "xpositionMB02V",
                                 structM->getMW2()->positionMB02V[0]);
        myWriter->createDataNode("DetectorMW2", 0, "ypositionMB02V",
                                 structM->getMW2()->positionMB02V[1]);
        myWriter->createDataNode("DetectorMW2", 0, "zpositionMB02V",
                                 structM->getMW2()->positionMB02V[2]);
    }

    for (unsigned int i = 0; i < structM->getCalorimeter()->size(); i++)
        saveDetector("DetectorCalorimeter", &structM->getCalorimeter()->at(i));
    for (unsigned int i = 0; i < structM->getStraw()->size(); i++)
        saveDetector("DetectorStraw", &structM->getStraw()->at(i));
    for (unsigned int i = 0; i < structM->getMicromegas()->size(); i++)
        saveDetector("DetectorMicromegas", &structM->getMicromegas()->at(i));
    for (unsigned int i = 0; i < structM->getGem()->size(); i++)
        saveDetector("DetectorGem", &structM->getGem()->at(i));
    for (unsigned int i = 0; i < structM->getSciFi()->size(); i++)
        saveDetector("DetectorSciFi", &structM->getSciFi()->at(i));
    for (unsigned int i = 0; i < structM->getSilicon()->size(); i++)
        saveDetector("DetectorSilicon", &structM->getSilicon()->at(i));
    for (unsigned int i = 0; i < structM->getMWPC()->size(); i++)
        saveDetector("DetectorMWPC", &structM->getMWPC()->at(i));
    for (unsigned int i = 0; i < structM->getVeto()->size(); i++)
        saveDetector("DetectorVeto", &structM->getVeto()->at(i));
    for (unsigned int i = 0; i < structM->getDC()->size(); i++)
        saveDetector("DetectorDC", &structM->getDC()->at(i));
    for (unsigned int i = 0; i < structM->getW45()->size(); i++)
        saveDetector("DetectorW45", &structM->getW45()->at(i));
    for (unsigned int i = 0; i < structM->getRichWall()->size(); i++)
        saveDetector("DetectorRichWall", &structM->getRichWall()->at(i));

    for (unsigned int i = 0; i < structM->getDetector()->size(); i++)
        if (structM->getDetector()->at(i).useDetector == true)
            saveDetector("DetectorLowRes", &structM->getDetector()->at(i));

    for (unsigned int i = 0; i < structM->getDetectorRes()->size(); i++)
        if (structM->getDetectorRes()->at(i).general.useDetector == true)
            saveDetectorRes(i);

    if (structM->getPolGPD()->general.useDetector == true) {
        myWriter->addNode("DetectorPolGPD");
        myWriter->createDataNode("DetectorPolGPD", 0, "name",
                                 structM->getPolGPD()->general.name);
        myWriter->createDataNode("DetectorPolGPD", 0, "xPos",
                                 structM->getPolGPD()->general.position[0]);
        myWriter->createDataNode("DetectorPolGPD", 0, "yPos",
                                 structM->getPolGPD()->general.position[1]);
        myWriter->createDataNode("DetectorPolGPD", 0, "zPos",
                                 structM->getPolGPD()->general.position[2]);
        myWriter->createDataNode("DetectorPolGPD", 0, "useMechanicalStructure",
                                 structM->getPolGPD()->general.useMechanicalStructure);

        // target
        myWriter->createDataNode("DetectorPolGPD", 0, "target_cryostat_radius", structM->getPolGPD()->target_cryostat_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_cryostat_length", structM->getPolGPD()->target_cryostat_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_cryostat_thickness", structM->getPolGPD()->target_cryostat_thickness);

        myWriter->createDataNode("DetectorPolGPD", 0, "target_aluminium_radius", structM->getPolGPD()->target_aluminium_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_aluminium_length", structM->getPolGPD()->target_aluminium_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_aluminium_thickness", structM->getPolGPD()->target_aluminium_thickness);

        myWriter->createDataNode("DetectorPolGPD", 0, "target_coils_radius", structM->getPolGPD()->target_coils_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_coils_length", structM->getPolGPD()->target_coils_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_coils_thickness", structM->getPolGPD()->target_coils_thickness);

        myWriter->createDataNode("DetectorPolGPD", 0, "target_mylar_window_thickness", structM->getPolGPD()->target_mylar_window_thickness);

        myWriter->createDataNode("DetectorPolGPD", 0, "target_length", structM->getPolGPD()->target_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_lhe_radius", structM->getPolGPD()->target_lhe_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_nh3_radius", structM->getPolGPD()->target_nh3_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_fiber_thickness", structM->getPolGPD()->target_fiber_thickness);
        myWriter->createDataNode("DetectorPolGPD", 0, "target_kevlar_thickness", structM->getPolGPD()->target_kevlar_thickness);

        // micromegas
        myWriter->createDataNode("DetectorPolGPD", 0, "mm_length", structM->getPolGPD()->mm_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "mm_radius", structM->getPolGPD()->mm_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "mm_layer_thickness", structM->getPolGPD()->mm_layer_thickness);
        myWriter->createDataNode("DetectorPolGPD", 0, "mm_layer_distance", structM->getPolGPD()->mm_layer_distance);
        myWriter->createDataNode("DetectorPolGPD", 0, "mm_zPosOffset", structM->getPolGPD()->mm_zPosOffset);

        // ring B
        myWriter->createDataNode("DetectorPolGPD", 0, "ringB_length", structM->getPolGPD()->ringB_length);
        myWriter->createDataNode("DetectorPolGPD", 0, "ringB_radius", structM->getPolGPD()->ringB_radius);
        myWriter->createDataNode("DetectorPolGPD", 0, "ringB_zPosOffset", structM->getPolGPD()->ringB_zPosOffset);
    }

    if (structM->getSciFiTest()->general.useDetector == true) {
        myWriter->addNode("DetectorSciFiTest");
        myWriter->createDataNode("DetectorSciFiTest", 0, "name",
                                 structM->getSciFiTest()->general.name);
        myWriter->createDataNode("DetectorSciFiTest", 0, "xPos",
                                 structM->getSciFiTest()->general.position[0]);
        myWriter->createDataNode("DetectorSciFiTest", 0, "yPos",
                                 structM->getSciFiTest()->general.position[1]);
        myWriter->createDataNode("DetectorSciFiTest", 0, "zPos",
                                 structM->getSciFiTest()->general.position[2]);
        myWriter->createDataNode("DetectorSciFiTest", 0, "useMechanicalStructure",
                                 structM->getSciFiTest()->general.useMechanicalStructure);
        myWriter->createDataNode("DetectorSciFiTest", 0, "length",
                                 structM->getSciFiTest()->length);
    }

    for (unsigned int i = 0; i < structM->getDummies()->size(); i++)
        saveDummies(&structM->getDummies()->at(i));

    myWriter->createFile(fileName);

    delete myWriter;
}

void T4SSettingsFileXML::saveMagnet(unsigned int i)
{
    const T4SMagnet *magnet = &structM->getMagnet()->at(i);
    myWriter->addNode("magnet");
    int index = myWriter->getNodeCount("magnet") - 1;

    myWriter->createDataNode("magnet", index, "name", magnet->general.name);
    myWriter->createDataNode("magnet", index, "xPos",
                             magnet->general.position[0]);
    myWriter->createDataNode("magnet", index, "yPos",
                             magnet->general.position[1]);
    myWriter->createDataNode("magnet", index, "zPos",
                             magnet->general.position[2]);
    myWriter->createDataNode("magnet", index, "useMechanicalStructure",
                             magnet->general.useMechanicalStructure);
    myWriter->createDataNode("magnet", index, "useField", magnet->useField);
    myWriter->createDataNode("magnet", index, "fieldmapPath",
                             magnet->fieldmapPath);
    myWriter->createDataNode("magnet", index, "scaleFactor",magnet->scaleFactor);
    myWriter->createDataNode("magnet", index, "current",magnet->current);
}

void T4SSettingsFileXML::saveDetector(std::string keyWord,
                                      const T4SDetector *detector)
{

    myWriter->addNode(keyWord);
    int index = myWriter->getNodeCount(keyWord) - 1;

    myWriter->createDataNode(keyWord, index, "name", detector->name);
    myWriter->createDataNode(keyWord, index, "xPos", detector->position[0]);
    myWriter->createDataNode(keyWord, index, "yPos", detector->position[1]);
    myWriter->createDataNode(keyWord, index, "zPos", detector->position[2]);
    myWriter->createDataNode(keyWord, index, "useMechanicalStructure",
                             detector->useMechanicalStructure);
}

void T4SSettingsFileXML::saveDetectorRes(unsigned int i)
{
    const T4SDetectorRes *detector = &structM->getDetectorRes()->at(i);
    myWriter->addNode("DetectorOptical");

    int index = myWriter->getNodeCount("DetectorOptical") - 1;

    myWriter->createDataNode("DetectorOptical", index, "name",
                             detector->general.name);
    myWriter->createDataNode("DetectorOptical", index, "xPos",
                             detector->general.position[0]);
    myWriter->createDataNode("DetectorOptical", index, "yPos",
                             detector->general.position[1]);
    myWriter->createDataNode("DetectorOptical", index, "zPos",
                             detector->general.position[2]);
    myWriter->createDataNode("DetectorOptical", index, "useMechanicalStructure",
                             detector->general.useMechanicalStructure);
    myWriter->createDataNode("DetectorOptical", index, "useOptical",
                             detector->useOptical);
}

void T4SSettingsFileXML::saveDummies(const T4SDummy *detector)
{
    myWriter->addNode("DetectorDummy");
    int index = myWriter->getNodeCount("DetectorDummy") - 1;

    myWriter->createDataNode("DetectorDummy", index, "name", detector->general.name);
    myWriter->createDataNode("DetectorDummy", index, "xPos", detector->general.position[0]);
    myWriter->createDataNode("DetectorDummy", index, "yPos", detector->general.position[1]);
    myWriter->createDataNode("DetectorDummy", index, "zPos", detector->general.position[2]);
    myWriter->createDataNode("DetectorDummy", index, "useMechanicalStructure", detector->general.useMechanicalStructure);
    myWriter->createDataNode("DetectorDummy", index, "xSize", detector->planeSize[0]);
    myWriter->createDataNode("DetectorDummy", index, "ySize", detector->planeSize[1]);
    myWriter->createDataNode("DetectorDummy", index, "zSize", detector->planeSize[2]);
}

void T4SSettingsFileXML::saveRPD(unsigned int i)
{
    const T4SRPD *rpd = &structM->getRPD()->at(i);
    myWriter->addNode("RPD");
    int index = myWriter->getNodeCount("RPD") - 1;

    myWriter->createDataNode("RPD", index, "name", rpd->general.name);
    myWriter->createDataNode("RPD", index, "xPos", rpd->general.position[0]);
    myWriter->createDataNode("RPD", index, "yPos", rpd->general.position[1]);
    myWriter->createDataNode("RPD", index, "zPos", rpd->general.position[2]);
    myWriter->createDataNode("RPD", index, "useMechanicalStructure",
                             rpd->general.useMechanicalStructure);
    myWriter->createDataNode("RPD", index, "useOptical", rpd->useOptical);
    myWriter->createDataNode("RPD", index, "useSingleSlab", rpd->useSingleSlab);
    myWriter->createDataNode("RPD", index, "useCalibrationFile", rpd->useCalibrationFile);
    myWriter->createDataNode("RPD", index, "calibrationFilePath", rpd->calibrationFilePath);
    if(rpd->general.name == "RP01R1")
      myWriter->createDataNode("RPD", index, "useConicalCryostat", rpd->useConicalCryostat);
}

void T4SSettingsFileXML::load_unchecked(string fileName)
{
    //this makes it safe for self-calling because of includes
    T4XMLParser *oldReader = myParser;
    myParser = new T4XMLParser();

    if (!myParser->parseFile(fileName)) {
        T4SMessenger::getInstance()->printMessage(T4SFatalError, __LINE__,
                __FILE__, "T4SSettingsFileXML could not parse file: '" + fileName + "'");
        return;
    }

    if (myParser->getBaseNodeCount("GeneralSettings") > 0) {

        //read general settings
        myParser->getChildValue("GeneralSettings", 0, "physicsList",
                                structM->getGeneral()->physicsList);
        myParser->getChildValue("GeneralSettings", 0, "useGflash",
                                structM->getGeneral()->useGflash);
        myParser->getChildValue("GeneralSettings", 0, "useVisualization",
                                structM->getGeneral()->useVisualization);
        myParser->getChildValue("GeneralSettings", 0, "useSeed",
                                structM->getGeneral()->useSeed);
        myParser->getChildValue("GeneralSettings", 0, "runName",
                                structM->getGeneral()->runName);
        myParser->getChildValue("GeneralSettings", 0, "seed",
                                structM->getGeneral()->seed);
        myParser->getChildValue("GeneralSettings", 0, "outputPath",
                                structM->getGeneral()->outputPath);
        myParser->getChildValue("GeneralSettings", 0, "saveASCII",
                                structM->getGeneral()->saveASCII);
	//this came later, so too keep compat, we check if it exists.
	//if yes, we read the flag, if no we disable it.
	if (myParser->getNodeChildCount("GeneralSettings",0,"saveBinary") != 0)
	  myParser->getChildValue("GeneralSettings", 0, "saveBinary",
                                structM->getGeneral()->saveBinary);
	else
	  structM->getGeneral()->saveBinary = false;
	
	myParser->getChildValue("GeneralSettings", 0, "saveDetDat",
                                structM->getGeneral()->saveDetDat);
        myParser->getChildValue("GeneralSettings", 0, "useTrigger",
                                structM->getGeneral()->useTrigger);
        myParser->getChildValue("GeneralSettings", 0, "exportGDML",
                                structM->getGeneral()->exportGDML);
        myParser->getChildValue("GeneralSettings", 0, "namingWithSeed",
                                structM->getGeneral()->namingWithSeed);
        myParser->getChildValue("GeneralSettings", 0, "verboseLevel",
                                structM->getGeneral()->verboseLevel);
        myParser->getChildValue("GeneralSettings", 0, "usePerformanceMonitor",
                                structM->getGeneral()->usePerformanceMonitor);
        myParser->getChildValue("GeneralSettings", 0, "checkOverlap",
                                structM->getGeneral()->checkOverlap);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsGlobal",
                                structM->getGeneral()->productionCutsGlobal);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsGams",
                                structM->getGeneral()->productionCutsGams);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsRHGams",
                                structM->getGeneral()->productionCutsRHGams);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsMainz",
                                structM->getGeneral()->productionCutsMainz);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsOlga",
                                structM->getGeneral()->productionCutsOlga);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsShashlik",
                                structM->getGeneral()->productionCutsShashlik);
        myParser->getChildValue("GeneralSettings", 0, "productionCutsHcal",
                                structM->getGeneral()->productionCutsHcal);
        myParser->getChildValue("GeneralSettings", 0, "noSecondaries",
                                structM->getGeneral()->noSecondaries);
        myParser->getChildValue("GeneralSettings", 0, "colorTheme",
                                structM->getGeneral()->colorTheme);
        myParser->getChildValue("GeneralSettings", 0, "simplifiedGeometries",
                                structM->getGeneral()->simplifiedGeometries);
        myParser->getChildValue("GeneralSettings", 0, "noMemoryLimitation",
                                structM->getGeneral()->noMemoryLimitation);
        myParser->getChildValue("GeneralSettings", 0, "useSplitting",
                                structM->getGeneral()->useSplitting);
        myParser->getChildValue("GeneralSettings", 0, "eventsPerChunk",
                                structM->getGeneral()->eventsPerChunk);
        myParser->getChildValue("GeneralSettings", 0, "useSplitPiping",
                                structM->getGeneral()->useSplitPiping);
        myParser->getChildValue("GeneralSettings", 0, "triggerPlugin",
                                structM->getGeneral()->triggerPlugin);
    }

    if (myParser->getBaseNodeCount("ExternalPaths") > 0) {
        //fill external paths struct
        myParser->getChildValue("ExternalPaths", 0, "localGeneratorFile",
                                structM->getExternal()->localGeneratorFile);
        myParser->getChildValue("ExternalPaths", 0, "beamFile",
                                structM->getExternal()->beamFile);
        myParser->getChildValue("ExternalPaths", 0, "beamFileForAdditionalPileUp",
                                structM->getExternal()->beamFileForAdditionalPileUp);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixInnerX",
                                structM->getExternal()->triggerMatrixInnerX);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixOuterY",
                                structM->getExternal()->triggerMatrixOuterY);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixLadderX",
                                structM->getExternal()->triggerMatrixLadderX);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixMiddleX",
                                structM->getExternal()->triggerMatrixMiddleX);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixMiddleY",
                                structM->getExternal()->triggerMatrixMiddleY);
        myParser->getChildValue("ExternalPaths", 0, "triggerMatrixLAST",
                                structM->getExternal()->triggerMatrixLAST);
        myParser->getChildValue("ExternalPaths", 0, "libHEPGen_Pi0",
                                structM->getExternal()->libHEPGen_Pi0);
        myParser->getChildValue("ExternalPaths", 0, "libHEPGen_Pi0_transv",
                                structM->getExternal()->libHEPGen_Pi0_transv);
        myParser->getChildValue("ExternalPaths", 0, "libHEPGen_Rho",
                                structM->getExternal()->libHEPGen_Rho);
        myParser->getChildValue("ExternalPaths", 0, "visualizationMacro",
                                structM->getExternal()->visualizationMacro);
        myParser->getChildValue("ExternalPaths", 0, "detectorEfficiency",
                                structM->getExternal()->detectorEfficiency);
        myParser->getChildValue("ExternalPaths", 0, "calorimeterInfo",
                                structM->getExternal()->calorimeterInfo);
    }
    if (myParser->getBaseNodeCount("BeamSettings") > 0) {
        //read beamsettings
        myParser->getChildValue("BeamSettings", 0, "numParticles",
                                structM->getBeam()->numParticles);
        myParser->getChildValue("BeamSettings", 0, "beamParticle",
                                structM->getBeam()->beamParticle);
        myParser->getChildValue("BeamSettings", 0, "beamPlugin",
                                structM->getBeam()->beamPlugin);
        myParser->getChildValue("BeamSettings", 0, "beamEnergy",
                                structM->getBeam()->beamEnergy);
        myParser->getChildValue("BeamSettings", 0, "useBeamfile",
                                structM->getBeam()->useBeamfile);
        myParser->getChildValue("BeamSettings", 0, "beamFileType",
                                structM->getBeam()->beamFileType);
        myParser->getChildValue("BeamSettings", 0, "beamFileZConvention",
                                structM->getBeam()->beamFileZConvention);
        myParser->getChildValue("BeamSettings", 0, "beamZStart",
                                structM->getBeam()->beamZStart);
        myParser->getChildValue("BeamSettings", 0, "beamFileBackend",
                                structM->getBeam()->beamFileBackend);
        myParser->getChildValue("BeamSettings", 0, "usePileUp",
                                structM->getBeam()->usePileUp);
        myParser->getChildValue("BeamSettings", 0, "beamFlux",
                                structM->getBeam()->beamFlux);
        myParser->getChildValue("BeamSettings", 0, "timeGate",
                                structM->getBeam()->timeGate);
        myParser->getChildValue("BeamSettings", 0, "useAdditionalPileUp",
                                structM->getBeam()->useAdditionalPileUp);
        myParser->getChildValue("BeamSettings", 0, "additionalPileUpFlux",
                                structM->getBeam()->additionalPileUpFlux);
        myParser->getChildValue("BeamSettings", 0, "beamFileZConventionForAdditionalPileUp",
                                structM->getBeam()->beamFileZConventionForAdditionalPileUp);
        myParser->getChildValue("BeamSettings", 0, "useTargetExtrap",
                                structM->getBeam()->useTargetExtrap);
        myParser->getChildValue("BeamSettings", 0, "targetStepLimit",
                                structM->getBeam()->targetStepLimit);
        myParser->getChildValue("BeamSettings", 0, "useHadronicInteractionEGCall",
                                structM->getBeam()->useHadronicInteractionEGCall);
    }

    //check for generator-settings and read usermode-structs
    if (myParser->getBaseNodeCount("BeamSettings") > 0)
        if (structM->getBeam()->beamPlugin == "User") {
            myParser->getChildValue("UserModeSettings", 0, "xPos",
                                    structM->getUser()->position[0]);
            myParser->getChildValue("UserModeSettings", 0, "yPos",
                                    structM->getUser()->position[1]);
            myParser->getChildValue("UserModeSettings", 0, "zPos",
                                    structM->getUser()->position[2]);
            myParser->getChildValue("UserModeSettings", 0, "xMom",
                                    structM->getUser()->momentum[0]);
            myParser->getChildValue("UserModeSettings", 0, "yMom",
                                    structM->getUser()->momentum[1]);
            myParser->getChildValue("UserModeSettings", 0, "zMom",
                                    structM->getUser()->momentum[2]);
            myParser->getChildValue("UserModeSettings", 0, "useRandomEnergy",
                                    structM->getUser()->useRandomEnergy);
            myParser->getChildValue("UserModeSettings", 0, "minEnergy",
                                    structM->getUser()->minEnergy);
            myParser->getChildValue("UserModeSettings", 0, "maxEnergy",
                                    structM->getUser()->maxEnergy);
        }

    //do the cosmics struct
    if (myParser->getBaseNodeCount("BeamSettings") > 0)
        if (structM->getBeam()->beamPlugin == "Cosmics") {
            myParser->getChildValue("CosmicsSettings", 0, "angleVariation",
                                    structM->getCosmic()->angleVariation);
            myParser->getChildValue("CosmicsSettings", 0, "xPos",
                                    structM->getCosmic()->positionX);
            myParser->getChildValue("CosmicsSettings", 0, "zPos",
                                    structM->getCosmic()->positionZ);
            myParser->getChildValue("CosmicsSettings", 0, "xVariation",
                                    structM->getCosmic()->variationX);
            myParser->getChildValue("CosmicsSettings", 0, "zVariation",
                                    structM->getCosmic()->variationZ);
        }

    //do the ecalcalib struct
    if (myParser->getBaseNodeCount("BeamSettings") > 0)
        if (structM->getBeam()->beamPlugin == "EcalCalib") {
            myParser->getChildValue("EcalCalibSettings", 0, "positionZ",
                                    structM->getEcalCalib()->positionZ);
            myParser->getChildValue("EcalCalibSettings", 0, "positionXMin",
                                        structM->getEcalCalib()->positionXMin);
            myParser->getChildValue("EcalCalibSettings", 0, "positionXMax",
                                        structM->getEcalCalib()->positionXMax);
            myParser->getChildValue("EcalCalibSettings", 0, "positionYMin",
                                        structM->getEcalCalib()->positionYMin);
            myParser->getChildValue("EcalCalibSettings", 0, "positionYMax",
                                        structM->getEcalCalib()->positionYMax);
            myParser->getChildValue("EcalCalibSettings", 0, "energyMin",
                                        structM->getEcalCalib()->energyMin);
            myParser->getChildValue("EcalCalibSettings", 0, "energyMax",
                                        structM->getEcalCalib()->energyMax);
        }
    //read the hepgen struct
    if (myParser->getBaseNodeCount("BeamSettings") > 0)
        if (structM->getBeam()->beamPlugin == "HEPGEN" || structM->getBeam()->beamPlugin == "EventGen") {
            myParser->getChildValue("HEPGenSettings", 0, "target",
                                    structM->getHEPGen()->target);
            myParser->getChildValue("HEPGenSettings", 0, "IVECM",
                                    structM->getHEPGen()->IVECM);
            myParser->getChildValue("HEPGenSettings", 0, "LST",
                                    structM->getHEPGen()->LST);
            myParser->getChildValue("HEPGenSettings", 0, "CUT0",
                                    structM->getHEPGen()->CUT[0]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT1",
                                    structM->getHEPGen()->CUT[1]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT2",
                                    structM->getHEPGen()->CUT[2]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT3",
                                    structM->getHEPGen()->CUT[3]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT4",
                                    structM->getHEPGen()->CUT[4]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT5",
                                    structM->getHEPGen()->CUT[5]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT6",
                                    structM->getHEPGen()->CUT[6]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT7",
                                    structM->getHEPGen()->CUT[7]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT8",
                                    structM->getHEPGen()->CUT[8]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT9",
                                    structM->getHEPGen()->CUT[9]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT10",
                                    structM->getHEPGen()->CUT[10]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT11",
                                    structM->getHEPGen()->CUT[11]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT12",
                                    structM->getHEPGen()->CUT[12]);
            myParser->getChildValue("HEPGenSettings", 0, "CUT13",
                                    structM->getHEPGen()->CUT[13]);
            myParser->getChildValue("HEPGenSettings", 0, "THMAX",
                                    structM->getHEPGen()->THMAX);
            myParser->getChildValue("HEPGenSettings", 0, "alf",
                                    structM->getHEPGen()->alf);
            myParser->getChildValue("HEPGenSettings", 0, "probc",
                                    structM->getHEPGen()->probc);
            myParser->getChildValue("HEPGenSettings", 0, "bcoh",
                                    structM->getHEPGen()->bcoh);
            myParser->getChildValue("HEPGenSettings", 0, "bin",
                                    structM->getHEPGen()->bin);
            myParser->getChildValue("HEPGenSettings", 0, "clept",
                                    structM->getHEPGen()->clept);
            myParser->getChildValue("HEPGenSettings", 0, "slept",
                                    structM->getHEPGen()->slept);
            myParser->getChildValue("HEPGenSettings", 0, "B0",
                                    structM->getHEPGen()->B0);
            myParser->getChildValue("HEPGenSettings", 0, "xbj0",
                                    structM->getHEPGen()->xbj0);
            myParser->getChildValue("HEPGenSettings", 0, "alphap",
                                    structM->getHEPGen()->alphap);
            myParser->getChildValue("HEPGenSettings", 0, "TLIM0",
                                    structM->getHEPGen()->TLIM[0]);
            myParser->getChildValue("HEPGenSettings", 0, "TLIM1",
                                    structM->getHEPGen()->TLIM[1]);
        }

    //read the primgen struct
    if (myParser->getBaseNodeCount("BeamSettings") > 0) {
        if (structM->getBeam()->beamPlugin == "Primakoff") {
            myParser->getChildValue("PrimGenSettings", 0, "targetZ",
                                      structM->getPrimGen()->Z);
            myParser->getChildValue("PrimGenSettings", 0, "particle",
                                    structM->getPrimGen()->particle);
            myParser->getChildValue("PrimGenSettings", 0, "alpha_1",
                                    structM->getPrimGen()->alpha_1);
            myParser->getChildValue("PrimGenSettings", 0, "beta_1",
                                    structM->getPrimGen()->beta_1);
            myParser->getChildValue("PrimGenSettings", 0, "alpha_2",
                                    structM->getPrimGen()->alpha_2);
            myParser->getChildValue("PrimGenSettings", 0, "beta_2",
                                    structM->getPrimGen()->beta_2);
            myParser->getChildValue("PrimGenSettings", 0, "s_min",
                                     structM->getPrimGen()->s_min);
            myParser->getChildValue("PrimGenSettings", 0, "s_max",
                                     structM->getPrimGen()->s_max);
            myParser->getChildValue("PrimGenSettings", 0, "Q2_max",
                                     structM->getPrimGen()->Q2_max);
            myParser->getChildValue("PrimGenSettings", 0, "Egamma_min",
                                     structM->getPrimGen()->Egamma_min);
            myParser->getChildValue("PrimGenSettings", 0, "pbeam_min",
                                     structM->getPrimGen()->pbeam_min);
            myParser->getChildValue("PrimGenSettings", 0, "pbeam_max",
                                     structM->getPrimGen()->pbeam_max);
            myParser->getChildValue("PrimGenSettings", 0, "born",
                                     structM->getPrimGen()->born);
            myParser->getChildValue("PrimGenSettings", 0, "RhoContrib",
                                     structM->getPrimGen()->rhoContrib);
            myParser->getChildValue("PrimGenSettings", 0, "ChiralLoops",
                                     structM->getPrimGen()->chiralLoops);
            myParser->getChildValue("PrimGenSettings", 0, "Polarizabilities",
                                     structM->getPrimGen()->polarizabilities);
            myParser->getChildValue("PrimGenSettings", 0, "radcorr",
                                     structM->getPrimGen()->radcorr);

        }
    }
    //read the primTrig struct
    if (structM->getGeneral()->triggerPlugin == "Primakoff2012") {
        if (myParser->getBaseNodeCount("PrimTriggerSettings") > 0) {
            myParser->getChildValue("PrimTriggerSettings", 0, "thresholdPrim1",
                                     structM->getPrimTrig()->threshPrim1);
            myParser->getChildValue("PrimTriggerSettings", 0, "thresholdPrim2",
                                     structM->getPrimTrig()->threshPrim2);
            myParser->getChildValue("PrimTriggerSettings", 0, "thresholdPrim3",
                                     structM->getPrimTrig()->threshPrim3);
            myParser->getChildValue("PrimTriggerSettings", 0, "thresholdPrim2Veto",
                                     structM->getPrimTrig()->threshPrim2Veto);
        }
    }
    if (structM->getGeneral()->physicsList == "FULL")
        if (myParser->getBaseNodeCount("PhysicsListSampling") > 0) {
            myParser->getChildValue("PhysicsListSampling", 0, "windowSize",
                                    structM->getSampling()->windowSize);
            myParser->getChildValue("PhysicsListSampling", 0, "binsPerNanoSecond",
                                    structM->getSampling()->binsPerNanoSecond);
            myParser->getChildValue("PhysicsListSampling", 0, "useBaseline",
                                    structM->getSampling()->useBaseline);
            myParser->getChildValue("PhysicsListSampling", 0, "baseline",
                                    structM->getSampling()->baseline);
        }

    for (int i = 0; i < myParser->getBaseNodeCount("RPD"); i++)
        loadRPD(i);

    for (int i = 0; i < myParser->getBaseNodeCount("magnet"); i++)
        loadMagnet(i);

    if (myParser->getBaseNodeCount("DetectorRICH") > 0) {
        structM->getRICH()->general.useDetector = true;
        myParser->getChildValue("DetectorRICH", 0, "name",
                                structM->getRICH()->general.name);
        myParser->getChildValue("DetectorRICH", 0, "xPos",
                                structM->getRICH()->general.position[0]);
        myParser->getChildValue("DetectorRICH", 0, "yPos",
                                structM->getRICH()->general.position[1]);
        myParser->getChildValue("DetectorRICH", 0, "zPos",
                                structM->getRICH()->general.position[2]);
        myParser->getChildValue("DetectorRICH", 0, "useMechanicalStructure",
                                structM->getRICH()->general.useMechanicalStructure);
        myParser->getChildValue("DetectorRICH", 0, "useOpticalPhysics",
                                structM->getRICH()->useOpticalPhysics);
        myParser->getChildValue("DetectorRICH", 0, "visibleHousing",
                                structM->getRICH()->visibleHousing);
        myParser->getChildValue("DetectorRICH", 0, "gas",
                                structM->getRICH()->gas);
    }

    if (myParser->getBaseNodeCount("DetectorMW1") > 0) {
        structM->getMW1()->general.useDetector = true;
        myParser->getChildValue("DetectorMW1", 0, "name",
                                structM->getMW1()->general.name);
        myParser->getChildValue("DetectorMW1", 0, "xPos",
                                structM->getMW1()->general.position[0]);
        myParser->getChildValue("DetectorMW1", 0, "yPos",
                                structM->getMW1()->general.position[1]);
        myParser->getChildValue("DetectorMW1", 0, "zPos",
                                structM->getMW1()->general.position[2]);
        myParser->getChildValue("DetectorMW1", 0, "useMechanicalStructure",
                                structM->getMW1()->general.useMechanicalStructure);
        myParser->getChildValue("DetectorMW1", 0, "useAbsorber",
                                structM->getMW1()->useAbsorber);
        myParser->getChildValue("DetectorMW1", 0, "useMA01X1",
                                structM->getMW1()->useMA01X1);
        myParser->getChildValue("DetectorMW1", 0, "useMA01X3",
                                structM->getMW1()->useMA01X3);
        myParser->getChildValue("DetectorMW1", 0, "useMA01Y1",
                                structM->getMW1()->useMA01Y1);
        myParser->getChildValue("DetectorMW1", 0, "useMA01Y3",
                                structM->getMW1()->useMA01Y3);
        myParser->getChildValue("DetectorMW1", 0, "useMA02X3",
                                structM->getMW1()->useMA02X3);
        myParser->getChildValue("DetectorMW1", 0, "useMA02X1",
                                structM->getMW1()->useMA02X1);
        myParser->getChildValue("DetectorMW1", 0, "useMA02Y1",
                                structM->getMW1()->useMA02Y1);
        myParser->getChildValue("DetectorMW1", 0, "useMA02Y3",
                                structM->getMW1()->useMA02Y3);

        myParser->getChildValue("DetectorMW1", 0, "xposMA01X1",
                                structM->getMW1()->posMA01X1[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA01X1",
                                structM->getMW1()->posMA01X1[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA01X1",
                                structM->getMW1()->posMA01X1[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA01X3",
                                structM->getMW1()->posMA01X3[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA01X3",
                                structM->getMW1()->posMA01X3[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA01X3",
                                structM->getMW1()->posMA01X3[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA01Y1",
                                structM->getMW1()->posMA01Y1[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA01Y1",
                                structM->getMW1()->posMA01Y1[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA01Y1",
                                structM->getMW1()->posMA01Y1[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA01Y3",
                                structM->getMW1()->posMA01Y3[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA01Y3",
                                structM->getMW1()->posMA01Y3[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA01Y3",
                                structM->getMW1()->posMA01Y3[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA02X1",
                                structM->getMW1()->posMA02X1[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA02X1",
                                structM->getMW1()->posMA02X1[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA02X1",
                                structM->getMW1()->posMA02X1[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA02X3",
                                structM->getMW1()->posMA02X3[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA02X3",
                                structM->getMW1()->posMA02X3[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA02X3",
                                structM->getMW1()->posMA02X3[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA02Y1",
                                structM->getMW1()->posMA02Y1[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA02Y1",
                                structM->getMW1()->posMA02Y1[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA02Y1",
                                structM->getMW1()->posMA02Y1[2]);

        myParser->getChildValue("DetectorMW1", 0, "xposMA02Y3",
                                structM->getMW1()->posMA02Y3[0]);
        myParser->getChildValue("DetectorMW1", 0, "yposMA02Y3",
                                structM->getMW1()->posMA02Y3[1]);
        myParser->getChildValue("DetectorMW1", 0, "zposMA02Y3",
                                structM->getMW1()->posMA02Y3[2]);

    }

    if (myParser->getBaseNodeCount("DetectorMW2") > 0) {
        structM->getMW2()->general.useDetector = true;

        myParser->getChildValue("DetectorMW2", 0, "name",
                                structM->getMW2()->general.name);
        myParser->getChildValue("DetectorMW2", 0, "xPos",
                                structM->getMW2()->general.position[0]);
        myParser->getChildValue("DetectorMW2", 0, "yPos",
                                structM->getMW2()->general.position[1]);
        myParser->getChildValue("DetectorMW2", 0, "zPos",
                                structM->getMW2()->general.position[2]);
        myParser->getChildValue("DetectorMW2", 0, "useMechanicalStructure",
                                structM->getMW2()->general.useMechanicalStructure);
        myParser->getChildValue("DetectorMW2", 0, "useAbsorber",
                                structM->getMW2()->useAbsorber);
        myParser->getChildValue("DetectorMW2", 0, "MB01X",
                                structM->getMW2()->MB01X);
        myParser->getChildValue("DetectorMW2", 0, "MB01Y",
                                structM->getMW2()->MB01Y);
        myParser->getChildValue("DetectorMW2", 0, "MB01V",
                                structM->getMW2()->MB01V);
        myParser->getChildValue("DetectorMW2", 0, "MB02X",
                                structM->getMW2()->MB02X);
        myParser->getChildValue("DetectorMW2", 0, "MB02Y",
                                structM->getMW2()->MB02Y);
        myParser->getChildValue("DetectorMW2", 0, "MB02V",
                                structM->getMW2()->MB02V);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB01X",
                                structM->getMW2()->positionMB01X[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB01X",
                                structM->getMW2()->positionMB01X[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB01X",
                                structM->getMW2()->positionMB01X[2]);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB01Y",
                                structM->getMW2()->positionMB01Y[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB01Y",
                                structM->getMW2()->positionMB01Y[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB01Y",
                                structM->getMW2()->positionMB01Y[2]);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB01V",
                                structM->getMW2()->positionMB01V[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB01V",
                                structM->getMW2()->positionMB01V[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB01V",
                                structM->getMW2()->positionMB01V[2]);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB02X",
                                structM->getMW2()->positionMB02X[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB02X",
                                structM->getMW2()->positionMB02X[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB02X",
                                structM->getMW2()->positionMB02X[2]);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB02Y",
                                structM->getMW2()->positionMB02Y[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB02Y",
                                structM->getMW2()->positionMB02Y[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB02Y",
                                structM->getMW2()->positionMB02Y[2]);

        myParser->getChildValue("DetectorMW2", 0, "xpositionMB02V",
                                structM->getMW2()->positionMB02V[0]);
        myParser->getChildValue("DetectorMW2", 0, "ypositionMB02V",
                                structM->getMW2()->positionMB02V[1]);
        myParser->getChildValue("DetectorMW2", 0, "zpositionMB02V",
                                structM->getMW2()->positionMB02V[2]);
    }

    for (int i = 0; i < myParser->getBaseNodeCount("DetectorLowRes"); i++)
        loadDetector("DetectorLowRes", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorDC"); i++)
        loadDetector("DetectorDC", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorW45"); i++)
        loadDetector("DetectorW45", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorStraw"); i++)
        loadDetector("DetectorStraw", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorMicromegas"); i++)
        loadDetector("DetectorMicromegas", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorGem"); i++)
        loadDetector("DetectorGem", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorSciFi"); i++)
        loadDetector("DetectorSciFi", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorSilicon"); i++)
        loadDetector("DetectorSilicon", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorMWPC"); i++)
        loadDetector("DetectorMWPC", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorVeto"); i++)
        loadDetector("DetectorVeto", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorCalorimeter"); i++)
        loadDetector("DetectorCalorimeter", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorRichWall"); i++)
        loadDetector("DetectorRichWall", i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorOptical"); i++)
        loadDetectorRes(i);
    for (int i = 0; i < myParser->getBaseNodeCount("DetectorDummy"); i++)
        loadDummies(i);

    if (myParser->getBaseNodeCount("DetectorPolGPD") > 0) {
        structM->getPolGPD()->general.useDetector = true;
        myParser->getChildValue("DetectorPolGPD", 0, "name",
                                structM->getPolGPD()->general.name);
        myParser->getChildValue("DetectorPolGPD", 0, "xPos",
                                structM->getPolGPD()->general.position[0]);
        myParser->getChildValue("DetectorPolGPD", 0, "yPos",
                                structM->getPolGPD()->general.position[1]);
        myParser->getChildValue("DetectorPolGPD", 0, "zPos",
                                structM->getPolGPD()->general.position[2]);
        myParser->getChildValue("DetectorPolGPD", 0, "useMechanicalStructure",
                                structM->getPolGPD()->general.useMechanicalStructure);

        // target
        myParser->getChildValue("DetectorPolGPD", 0, "target_cryostat_radius", structM->getPolGPD()->target_cryostat_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "target_cryostat_length", structM->getPolGPD()->target_cryostat_length);
        myParser->getChildValue("DetectorPolGPD", 0, "target_cryostat_thickness", structM->getPolGPD()->target_cryostat_thickness);

        myParser->getChildValue("DetectorPolGPD", 0, "target_aluminium_radius", structM->getPolGPD()->target_aluminium_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "target_aluminium_length", structM->getPolGPD()->target_aluminium_length);
        myParser->getChildValue("DetectorPolGPD", 0, "target_aluminium_thickness", structM->getPolGPD()->target_aluminium_thickness);

        myParser->getChildValue("DetectorPolGPD", 0, "target_coils_radius", structM->getPolGPD()->target_coils_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "target_coils_length", structM->getPolGPD()->target_coils_length);
        myParser->getChildValue("DetectorPolGPD", 0, "target_coils_thickness", structM->getPolGPD()->target_coils_thickness);

        myParser->getChildValue("DetectorPolGPD", 0, "target_mylar_window_thickness", structM->getPolGPD()->target_mylar_window_thickness);

        myParser->getChildValue("DetectorPolGPD", 0, "target_length", structM->getPolGPD()->target_length);
        myParser->getChildValue("DetectorPolGPD", 0, "target_lhe_radius", structM->getPolGPD()->target_lhe_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "target_nh3_radius", structM->getPolGPD()->target_nh3_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "target_fiber_thickness", structM->getPolGPD()->target_fiber_thickness);
        myParser->getChildValue("DetectorPolGPD", 0, "target_kevlar_thickness", structM->getPolGPD()->target_kevlar_thickness);

        // micromegas
        myParser->getChildValue("DetectorPolGPD", 0, "mm_length", structM->getPolGPD()->mm_length);
        myParser->getChildValue("DetectorPolGPD", 0, "mm_radius", structM->getPolGPD()->mm_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "mm_layer_thickness", structM->getPolGPD()->mm_layer_thickness);
        myParser->getChildValue("DetectorPolGPD", 0, "mm_layer_distance", structM->getPolGPD()->mm_layer_distance);
        myParser->getChildValue("DetectorPolGPD", 0, "mm_zPosOffset", structM->getPolGPD()->mm_zPosOffset);

        // ring B
        myParser->getChildValue("DetectorPolGPD", 0, "ringB_length", structM->getPolGPD()->ringB_length);
        myParser->getChildValue("DetectorPolGPD", 0, "ringB_radius", structM->getPolGPD()->ringB_radius);
        myParser->getChildValue("DetectorPolGPD", 0, "ringB_zPosOffset", structM->getPolGPD()->ringB_zPosOffset);
    }

    if (myParser->getBaseNodeCount("DetectorSciFiTest") > 0) {
        structM->getSciFiTest()->general.useDetector = true;
        myParser->getChildValue("DetectorSciFiTest", 0, "name",
                                structM->getSciFiTest()->general.name);
        myParser->getChildValue("DetectorSciFiTest", 0, "xPos",
                                structM->getSciFiTest()->general.position[0]);
        myParser->getChildValue("DetectorSciFiTest", 0, "yPos",
                                structM->getSciFiTest()->general.position[1]);
        myParser->getChildValue("DetectorSciFiTest", 0, "zPos",
                                structM->getSciFiTest()->general.position[2]);
        myParser->getChildValue("DetectorSciFiTest", 0, "useMechanicalStructure",
                                structM->getSciFiTest()->general.useMechanicalStructure);
        myParser->getChildValue("DetectorSciFiTest", 0, "length",
                                structM->getSciFiTest()->length);
    }

    if (myParser->getBaseNodeCount("IncludeXMLFile") > 0) {
        for (int i = 0; i < myParser->getBaseNodeCount("IncludeXMLFile"); i++) {
            string fileName = "";
            bool isInTGEANT = true;
            if (myParser->getNodeChildCount("IncludeXMLFile", i, "isInTGEANT") > 0)
                myParser->getChildValue("IncludeXMLFile", i, "isInTGEANT", isInTGEANT);

            if (isInTGEANT) {
                fileName = string(getenv("TGEANT"));
                if (fileName.at(fileName.size() - 1) != '/')
                    fileName.append("/");
            }


            string fileNameFromXML;
            myParser->getChildValue("IncludeXMLFile", i, "FileName", fileNameFromXML);
            fileName += fileNameFromXML;
            T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
                    "Loading embedded XML-File: " + fileName);
            load_unchecked(fileName);
        }
    }

    //clean up and reset for the last call;
    delete myParser;
    myParser = oldReader;

    T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
            "SettingsFileXML '" + fileName + "' has been loaded.");
}

void T4SSettingsFileXML::loadDetector(std::string keyWord, int i)
{
    std::string detName;
    myParser->getChildValue(keyWord, i, "name", detName);
    T4SDetector *detectorPointer = NULL;
    if (keyWord == "DetectorW45")
        detectorPointer = structM->addW45(detName);
    else if (keyWord == "DetectorDC")
        detectorPointer = structM->addDC(detName);
    else if (keyWord == "DetectorCalorimeter")
        detectorPointer = structM->addCalorimeter(detName);
    else if (keyWord == "DetectorStraw")
        detectorPointer = structM->addStraw(detName);
    else if (keyWord == "DetectorMicromegas")
        detectorPointer = structM->addMicromegas(detName);
    else if (keyWord == "DetectorGem")
        detectorPointer = structM->addGem(detName);
    else if (keyWord == "DetectorSciFi")
        detectorPointer = structM->addSciFi(detName);
    else if (keyWord == "DetectorSilicon")
        detectorPointer = structM->addSilicon(detName);
    else if (keyWord == "DetectorMWPC")
        detectorPointer = structM->addMWPC(detName);
    else if (keyWord == "DetectorRichWall")
        detectorPointer = structM->addRichWall(detName);
    else if (keyWord == "DetectorVeto")
        detectorPointer = structM->addVeto(detName);
    else if (keyWord == "DetectorLowRes")
        detectorPointer = new T4SDetector;
    else {
        T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
                __FILE__, "T4SSettingsFileXML::loadDetector: Unknown keyword.");
        return;
    }

    detectorPointer->useDetector = true;

    myParser->getChildValue(keyWord, i, "name", detectorPointer->name);
    myParser->getChildValue(keyWord, i, "xPos", detectorPointer->position[0]);
    myParser->getChildValue(keyWord, i, "yPos", detectorPointer->position[1]);
    myParser->getChildValue(keyWord, i, "zPos", detectorPointer->position[2]);
    myParser->getChildValue(keyWord, i, "useMechanicalStructure",
                            detectorPointer->useMechanicalStructure);

    if (keyWord == "DetectorLowRes") {
        for (unsigned int i = 0; i < structM->getDetector()->size(); i++)
            if (structM->getDetector()->at(i).name == detectorPointer->name)
                structM->getDetector()->at(i) = (*detectorPointer);
        delete detectorPointer;
    }

}

void T4SSettingsFileXML::loadMagnet(int i)
{
    T4SMagnet magnet;
    magnet.general.useDetector = true;
    myParser->getChildValue("magnet", i, "name", magnet.general.name);
    myParser->getChildValue("magnet", i, "xPos", magnet.general.position[0]);
    myParser->getChildValue("magnet", i, "yPos", magnet.general.position[1]);
    myParser->getChildValue("magnet", i, "zPos", magnet.general.position[2]);
    myParser->getChildValue("magnet", i, "useMechanicalStructure",
                            magnet.general.useMechanicalStructure);
    myParser->getChildValue("magnet", i, "useField", magnet.useField);
    myParser->getChildValue("magnet", i, "fieldmapPath", magnet.fieldmapPath);
    myParser->getChildValue("magnet", i, "scaleFactor", magnet.scaleFactor);
    myParser->getChildValue("magnet", i, "current", magnet.current);

    for (unsigned int i = 0; i < structM->getMagnet()->size(); i++) {
        if (structM->getMagnet()->at(i).general.name == magnet.general.name) {
            structM->getMagnet()->at(i) = magnet;
        }
    }
}

void T4SSettingsFileXML::loadRPD(int i)
{
    T4SRPD rpd;

    rpd.general.useDetector = true;
    myParser->getChildValue("RPD", i, "name", rpd.general.name);
    myParser->getChildValue("RPD", i, "xPos", rpd.general.position[0]);
    myParser->getChildValue("RPD", i, "yPos", rpd.general.position[1]);
    myParser->getChildValue("RPD", i, "zPos", rpd.general.position[2]);
    myParser->getChildValue("RPD", i, "useMechanicalStructure",
                            rpd.general.useMechanicalStructure);
    myParser->getChildValue("RPD", i, "useOptical", rpd.useOptical);
    myParser->getChildValue("RPD", i, "useSingleSlab", rpd.useSingleSlab);
    myParser->getChildValue("RPD", i, "useCalibrationFile", rpd.useCalibrationFile);
    myParser->getChildValue("RPD", i, "calibrationFilePath", rpd.calibrationFilePath);
    if(rpd.general.name == "RP01R1")
      myParser->getChildValue("RPD", i, "useConicalCryostat", rpd.useConicalCryostat);
    for (unsigned int i = 0; i < structM->getRPD()->size(); i++)
        if (structM->getRPD()->at(i).general.name == rpd.general.name)
            structM->getRPD()->at(i) = rpd;

}

void T4SSettingsFileXML::loadDetectorRes(int i)
{
    T4SDetectorRes newDetOpt;
    newDetOpt.general.useDetector = true;
    myParser->getChildValue("DetectorOptical", i, "name", newDetOpt.general.name);
    myParser->getChildValue("DetectorOptical", i, "xPos",
                            newDetOpt.general.position[0]);
    myParser->getChildValue("DetectorOptical", i, "yPos",
                            newDetOpt.general.position[1]);
    myParser->getChildValue("DetectorOptical", i, "zPos",
                            newDetOpt.general.position[2]);
    myParser->getChildValue("DetectorOptical", i, "useMechanicalStructure",
                            newDetOpt.general.useMechanicalStructure);
    myParser->getChildValue("DetectorOptical", i, "useOptical",
                            newDetOpt.useOptical);
    for (unsigned int i = 0; i < structM->getDetectorRes()->size(); i++) {
        if (structM->getDetectorRes()->at(i).general.name
                == newDetOpt.general.name) {
            structM->getDetectorRes()->at(i) = newDetOpt;
        }
    }
}

void T4SSettingsFileXML::loadDummies(int i)
{
  T4SDummy *detectorPointer = structM->addDummy();
  detectorPointer->general.useDetector = true;

  myParser->getChildValue("DetectorDummy", i, "name",
      detectorPointer->general.name);
  myParser->getChildValue("DetectorDummy", i, "name",
      detectorPointer->general.name);
  myParser->getChildValue("DetectorDummy", i, "xPos",
      detectorPointer->general.position[0]);
  myParser->getChildValue("DetectorDummy", i, "yPos",
      detectorPointer->general.position[1]);
  myParser->getChildValue("DetectorDummy", i, "zPos",
      detectorPointer->general.position[2]);
  myParser->getChildValue("DetectorDummy", i, "useMechanicalStructure",
      detectorPointer->general.useMechanicalStructure);
  myParser->getChildValue("DetectorDummy", i, "xSize",
      detectorPointer->planeSize[0]);
  myParser->getChildValue("DetectorDummy", i, "ySize",
      detectorPointer->planeSize[1]);
  myParser->getChildValue("DetectorDummy", i, "zSize",
      detectorPointer->planeSize[2]);
}
