#include "T4SGlobals.hh"

string intToStr(int arg)
{
  stringstream sstr;
  sstr << arg;
  return sstr.str();
}

int strToInt(string arg)
{
  stringstream sstr;
  int i;
  sstr << arg;
  sstr >> i;
  return i;
}

string uintToStr(unsigned int arg)
{
  stringstream sstr;
  sstr << arg;
  return sstr.str();
}

string longintToStr(long int arg)
{
  stringstream sstr;
  sstr << arg;
  return sstr.str();
}

long int strToLong(std::string arg)
{
  std::stringstream sstr;
  long int i;
  sstr << arg;
  sstr >> i;
  return i;
}

double strToDouble(std::string arg)
{
  std::stringstream sstr;
  double i;
  sstr << arg;
  sstr >> i;
  return i;
}

std::string doubleToStrFP(double arg)
{
  std::stringstream sstr;
  sstr << std::setprecision(8);
  sstr << arg;
  return sstr.str();
}

bool strToBool(std::string arg)
{
  return arg != "false";
}

bool fileExists(const std::string& name)
{
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

bool dirExists(const std::string& name)
{
  struct stat buffer;
  return ((stat(name.c_str(), &buffer) == 0)
      && (((buffer.st_mode) & S_IFMT) == S_IFDIR));
}


string translatePathEnvTGEANT(string _in)
{
  std::size_t found = _in.find("$TGEANT");
  string retString = _in;
  if (found != string::npos){
    retString.replace(found,7,std::string(getenv("TGEANT")));
  }
  return retString;
}

void replacePathEnvTGEANT(string& toChange)
{
  string tmp = translatePathEnvTGEANT(toChange);
  toChange = tmp;
}

void reverseReplacePathEnvTGEANT(string& toChange)
{
  string tmp = reverseTranslatePathEnvTGEANT(toChange);
  toChange = tmp;
}

string reverseTranslatePathEnvTGEANT(string _in)
{
  string envTGEANT = std::string(getenv("TGEANT"));
  std::size_t found = _in.find(envTGEANT);
  string retString = _in;
  if (found != string::npos){
    retString.replace(found,envTGEANT.size(),"$TGEANT/");
  }
  return retString;
}






std::vector<std::string> explodeString(const std::string& input)
{
  std::vector<std::string> array;

  int inputlength = input.length();
  int whitespaceLength = 1;
  int i = 0;
  int k = 0;
  while (i < inputlength) {
    int j = 0;
    while (i + j < inputlength && j < whitespaceLength
        && (input[i + j] == ' ' || input[i + j] == '\t'))
      j++;
    if (j == whitespaceLength) { //found whitespace
      if (input.substr(k, i - k) != "")
        array.push_back(input.substr(k, i - k));
      i += whitespaceLength;
      k = i;
    } else {
      i++;
    }
  }
  if (input.substr(k, i - k) != "")
    array.push_back(input.substr(k, i - k));
  return array;
}

std::vector<std::string> explodeStringCustomDelim(const std::string& input,
    const std::string& delim)
{
  std::vector<std::string> arr;

  int _inputleng = input.length();
  int delleng = delim.length();
  if (delleng == 0)
    return arr; //no change

  int i = 0;
  int k = 0;
  while (i < _inputleng) {
    int j = 0;
    while (i + j < _inputleng && j < delleng && input[i + j] == delim[j])
      j++;
    if (j == delleng) { //found _delim
      if (input.substr(k, i - k) != "")
        arr.push_back(input.substr(k, i - k));
      i += delleng;
      k = i;
    } else {
      i++;
    }
  }
  if (input.substr(k, i - k) != "" && input.substr(k, i - k) != " "
      && input.substr(k, i - k) != "\t")
    arr.push_back(input.substr(k, i - k));
  return arr;
}
