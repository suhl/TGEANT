#include "T4SStructManager.hh"

T4SStructManager::T4SStructManager(void)
{
  addT4SRPD("CA01R1");
  addT4SRPD("CA01R2");
  addT4SRPD("RP01R1");
  addT4SRPD("RP01R2");

  addT4SMagnet("SM1");
  addT4SMagnet("SM2");
  addT4SMagnet("DYTARGET");
  addT4SMagnet("POLTARGET");

  t4SRICH.general.name = "RICH";
  t4SMW1.general.name = "MW1";
  t4SMW2.general.name = "MW2";

  // T4SDetector
  addT4SDetector("LH2");
  addT4SDetector("LH2Hadron");
  addT4SDetector("PRIMAKOFFTARGET"); // 2012 primakoff target
  addT4SDetector("MF3");
  addT4SDetector("MF3_BLOCK");
  addT4SDetector("DYABSORBER");
  addT4SDetector("HO03");

  addT4SDetector("TESTCAL");
  addT4SDetector("PolGPDSiRPD");
  addT4SDetector("WORKSHOPEXAMPLE");

  addT4SDetector("HI04X1_u");
  addT4SDetector("HI04X1_d");
  addT4SDetector("HM04Y1_u1");
  addT4SDetector("HM04Y1_u2");
  addT4SDetector("HM04Y1_d1");
  addT4SDetector("HM04Y1_d2");
  addT4SDetector("HM04X1_u");
  addT4SDetector("HM04X1_d");
  addT4SDetector("HL04X1_1");
  addT4SDetector("HL04X1_2");
  addT4SDetector("HL04X1_3");
  addT4SDetector("HL04X1_4");
  addT4SDetector("HO04");

  addT4SDetector("HI05X1_u");
  addT4SDetector("HI05X1_d");
  addT4SDetector("HM05Y1_u1");
  addT4SDetector("HM05Y1_u2");
  addT4SDetector("HM05Y1_d1");
  addT4SDetector("HM05Y1_d2");
  addT4SDetector("HM05X1_u");
  addT4SDetector("HM05X1_d");
  addT4SDetector("HL05X1_1");
  addT4SDetector("HL05X1_2");
  addT4SDetector("HL05X1_3");
  addT4SDetector("HL05X1_4");

  addT4SDetector("HK01X1__");
  addT4SDetector("HK02X1__");
  addT4SDetector("HK03X1__");
  addT4SDetector("SANDWICHVETO");
  addT4SDetector("MultiplicityCounter");
  addT4SDetector("HH02R1__");
  addT4SDetector("HH03R1__");

  // T4SDetectorRes
  addT4SDetectorRes("HG01Y");
  addT4SDetectorRes("HG02Y");
  addT4SDetectorRes("VETO");

  t4SPolGPD.general.name = "POLGPD";
  t4SSciFiTest.general.name = "SCIFITEST";

  clearStructManager();
}

inline void T4SStructManager::addT4SMagnet(std::string name)
{
  t4SMagnet.push_back(T4SMagnet());
  t4SMagnet.back().general.name = name;
}

inline void T4SStructManager::addT4SRPD(std::string name)
{
  t4SRPD.push_back(T4SRPD());
  t4SRPD.back().general.name = name;
}

inline void T4SStructManager::addT4SDetector(std::string name)
{
  t4SDetector.push_back(T4SDetector());
  t4SDetector.back().name = name;
}

inline void T4SStructManager::addT4SDetectorRes(std::string name)
{
  t4SDetectorRes.push_back(T4SDetectorRes());
  t4SDetectorRes.back().general.name = name;
}

T4SDetector* T4SStructManager::addStraw(std::string name)
{
  t4SStraw.push_back(T4SDetector());
  t4SStraw.back().name = name;
  return &t4SStraw.back();
}

T4SDetector* T4SStructManager::addMicromegas(std::string name)
{
  t4SMM.push_back(T4SDetector());
  t4SMM.back().name = name;
  return &t4SMM.back();
}

T4SDetector* T4SStructManager::addGem(std::string name)
{
  t4SGEM.push_back(T4SDetector());
  t4SGEM.back().name = name;
  return &t4SGEM.back();
}

T4SDetector* T4SStructManager::addSciFi(std::string name)
{
  t4SFI.push_back(T4SDetector());
  t4SFI.back().name = name;
  return &t4SFI.back();
}

T4SDetector* T4SStructManager::addSilicon(std::string name)
{
  t4SSI.push_back(T4SDetector());
  t4SSI.back().name = name;
  return &t4SSI.back();
}

T4SDetector* T4SStructManager::addMWPC(std::string name)
{
  t4SMWPC.push_back(T4SDetector());
  t4SMWPC.back().name = name;
  return &t4SMWPC.back();
}

T4SDetector* T4SStructManager::addVeto(std::string name)
{
  t4SVeto.push_back(T4SDetector());
  t4SVeto.back().name = name;
  return &t4SVeto.back();
}

T4SDetector* T4SStructManager::addDC(std::string name)
{
  t4SDC.push_back(T4SDetector());
  t4SDC.back().name = name;
  return &t4SDC.back();
}

T4SDetector* T4SStructManager::addW45(std::string name)
{
  t4SW45.push_back(T4SDetector());
  t4SW45.back().name = name;
  return &t4SW45.back();
}

T4SDetector* T4SStructManager::addCalorimeter(std::string name)
{
  t4SCalorimeter.push_back(T4SDetector());
  t4SCalorimeter.back().name = name;
  return &t4SCalorimeter.back();
}

T4SDetector* T4SStructManager::addRichWall(std::string name)
{
  t4SRichWall.push_back(T4SDetector());
  t4SRichWall.back().name = name;
  return &t4SRichWall.back();
}

T4SDummy* T4SStructManager::addDummy(void)
{
  t4SDummy.push_back(T4SDummy());
  t4SDummy.back().general.name = "DUMMY";
  return &t4SDummy.back();
}

void T4SStructManager::clearStructManager(void)
{
  t4SStraw.clear();
  t4SMM.clear();
  t4SGEM.clear();
  t4SPGEM.clear();
  t4SFI.clear();
  t4SSI.clear();
  t4SMWPC.clear();
  t4SVeto.clear();
  t4SDC.clear();
  t4SW45.clear();
  t4SCalorimeter.clear();
  t4SRichWall.clear();
  t4SDummy.clear();

  general.physicsList = "unset";
  general.runName = "unset";
  general.outputPath = "unset";
  general.verboseLevel = 0;
  general.noMemoryLimitation = false;
  general.simplifiedGeometries = false;

  beam.beamPlugin = "unset";
  beam.useAdditionalPileUp = false;
  t4SRICH.gas = "unset";

  pythia.target = "unset";
  hepgen.target = "unset";
  hepgen.usePi0_transv_table = true;

  external.beamFile = "unset";
  external.beamFileForAdditionalPileUp = "unset";
  external.libHEPGen_Pi0 = "unset";
  external.libHEPGen_Pi0_transv = "unset";
  external.libHEPGen_Rho = "unset";
  external.localGeneratorFile = "unset";
  external.triggerMatrixInnerX = "unset";
  external.triggerMatrixLadderX = "unset";
  external.triggerMatrixLAST = "unset";
  external.triggerMatrixMiddleX = "unset";
  external.triggerMatrixMiddleY = "unset";
  external.triggerMatrixOuterY = "unset";
  external.visualizationMacro = "unset";
  external.detectorEfficiency = "unset";
  external.calorimeterInfo = "unset";

  sampling.baseline = 0;
  sampling.binsPerNanoSecond = 1;
  sampling.useBaseline = false;
  sampling.windowSize = 0;

  for (unsigned int i = 0; i < t4SRPD.size(); i++) {
    t4SRPD.at(i).general.useDetector = false;
    t4SRPD.at(i).general.useMechanicalStructure = false;
    t4SRPD.at(i).useOptical = false;
    t4SRPD.at(i).useSingleSlab = false;
    t4SRPD.at(i).useCalibrationFile = false;
    t4SRPD.at(i).calibrationFilePath = "unset";
    if(t4SRPD.at(i).general.name == "RP01R1")
      t4SRPD.at(i).useConicalCryostat = true;
    else
      t4SRPD.at(i).useConicalCryostat = false;

  }

  for (unsigned int i = 0; i < t4SMagnet.size(); i++) {
    t4SMagnet.at(i).general.useDetector = false;
    t4SMagnet.at(i).general.useMechanicalStructure = false;
    t4SMagnet.at(i).useField = false;
    t4SMagnet.at(i).fieldmapPath = "unset";
    t4SMagnet.at(i).current = -1;
  }

  t4SRICH.general.useDetector = false;
  t4SRICH.general.useMechanicalStructure = false;

  t4SMW1.general.useDetector = false;
  t4SMW1.general.useMechanicalStructure = false;

  t4SMW2.general.useDetector = false;
  t4SMW2.general.useMechanicalStructure = false;

  for (unsigned int i = 0; i < t4SDetector.size(); i++) {
    t4SDetector.at(i).useDetector = false;
    t4SDetector.at(i).useMechanicalStructure = false;
  }

  for (unsigned int i = 0; i < t4SDetectorRes.size(); i++) {
    t4SDetectorRes.at(i).general.useDetector = false;
    t4SDetectorRes.at(i).general.useMechanicalStructure = false;
    t4SDetectorRes.at(i).useOptical = false;
  }

  t4SPolGPD.general.useDetector = false;
  t4SPolGPD.general.useMechanicalStructure = false;

  t4SSciFiTest.general.useDetector = false;
  t4SSciFiTest.general.useMechanicalStructure = false;
}
