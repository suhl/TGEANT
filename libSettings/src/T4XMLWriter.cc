#include "T4XMLWriter.hh"
#include "T4SMessenger.hh"

void T4XMLWriter::addNode(string _nodeName)
{
  DOMElement* newNode = domDOC->createElement(
      XMLString::transcode(_nodeName.c_str()));
  myROOTNode->appendChild(newNode);
}

void T4XMLWriter::createDataNode(string _nodeName, int _nodeNumber,
    string _dataNodeName, string _dataNodeContent)
{
  DOMElement* parent = dynamic_cast<DOMElement*>(myROOTNode
      ->getElementsByTagName(XMLString::transcode(_nodeName.c_str()))->item(
      _nodeNumber));
  DOMElement* newNode = domDOC->createElement(
      XMLString::transcode(_dataNodeName.c_str()));
  newNode->setTextContent(XMLString::transcode(_dataNodeContent.c_str()));
  parent->appendChild(newNode);
}

int T4XMLWriter::getNodeCount(string _nodeName)
{
  return myROOTNode->getElementsByTagName(
      XMLString::transcode(_nodeName.c_str()))->getLength();
}

void T4XMLWriter::createFile(string _fileName)
{
  DOMImplementation *implementation =
      DOMImplementationRegistry::getDOMImplementation(
          XMLString::transcode("LS"));
  DOMLSSerializer *serializer = ((DOMImplementationLS*) implementation)
      ->createLSSerializer();
  if (serializer->getDomConfig()->canSetParameter(
      XMLUni::fgDOMWRTFormatPrettyPrint, true))
    serializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint,
        true);
  serializer->setNewLine(XMLString::transcode("\r\n"));
  XMLCh *tempFilePath = XMLString::transcode(_fileName.c_str());
  XMLFormatTarget *formatTarget = new LocalFileFormatTarget(tempFilePath);
  DOMLSOutput *output =
      ((DOMImplementationLS*) implementation)->createLSOutput();
  output->setByteStream(formatTarget);
  serializer->write(domDOC, output);
  serializer->release();
  XMLString::release(&tempFilePath);
  delete formatTarget;
  output->release();

  T4SMessenger::getInstance()->printMessage(T4SNotice, __LINE__, __FILE__,
      "XMLSettingsFile '" + _fileName + "' has been created.");
}

T4XMLWriter::T4XMLWriter(void)
{
  try {
    XMLPlatformUtils::Initialize();
  } catch (const XMLException& toCatch) {
    char* message = XMLString::transcode(toCatch.getMessage());
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, "T4XMLWriter::T4XMLWriter - Error during initialization!");
    T4SMessenger::getInstance()->printMessage(T4SErrorNonFatal, __LINE__,
    __FILE__, message);
    XMLString::release(&message);
  }
  XMLCh tempStr[100];

  XMLString::transcode("Range", tempStr, 99);
  domIMP = DOMImplementationRegistry::getDOMImplementation(tempStr);
  XMLString::transcode("root", tempStr, 99);
  domDOC = domIMP->createDocument(0, tempStr, 0);

  myROOTNode = domDOC->getDocumentElement();
}

void T4XMLWriter::createDataNode(string _nodeName, int _nodeNumber,
    string _dataNodeName, bool _boolInput)
{
  if (_boolInput)
    createDataNode(_nodeName, _nodeNumber, _dataNodeName, (string) "true");
  else
    createDataNode(_nodeName, _nodeNumber, _dataNodeName, (string) "false");
}

void T4XMLWriter::createDataNode(string _nodeName, int _nodeNumber,
    string _dataNodeName, double _doubleInput)
{
  string value = doubleToStrFP(_doubleInput);
  createDataNode(_nodeName, _nodeNumber, _dataNodeName, value);
}

void T4XMLWriter::createDataNode(string _nodeName, int _nodeNumber,
    string _dataNodeName, int _intInput)
{
  string value = intToStr(_intInput);
  createDataNode(_nodeName, _nodeNumber, _dataNodeName, value);
}

void T4XMLWriter::createDataNode(string _nodeName, int _nodeNumber,
    string _dataNodeName, long int _intInput)
{
  string value = longintToStr(_intInput);
  createDataNode(_nodeName, _nodeNumber, _dataNodeName, value);
}
