#include "T4CaloNoise.hh"
#include "T4SMessenger.hh"
bool T4CaloNoise::instanceFlag = false;
T4CaloNoise* T4CaloNoise::single = NULL;


T4CaloNoise* T4CaloNoise::getInstance ( void )
{
    if ( !instanceFlag ) {
        single = new T4CaloNoise();
        instanceFlag = true;
        return single;
    } else {
        return single;
    }
}

void T4CaloNoise::generateEventEntries()
{
  if (!isActive())
    return;
  
  eventNoise0.clear();
  eventNoise1.clear();
  eventNoise2.clear();
  
  int nNoise = numberOfNoiseCells();
//   printf("Wanting %i noise events, poissonian mean: %f!\n",nNoise,rate);
  caloNoiseEntry myEntry;
  for (int i = 0; i < nNoise; i++){
    
    myEntry.energy = -1.0;
    while (myEntry.energy == -1.0)
      makeANoise(myEntry.cellId,myEntry.caloNum,myEntry.energy,myEntry.time);
  }
  myEntry.used=false;
  switch(myEntry.caloNum){
    case 0: eventNoise0[myEntry.cellId] = myEntry;break;
    case 1: eventNoise1[myEntry.cellId] = myEntry;break;
    case 2: eventNoise2[myEntry.cellId] = myEntry;break;
  }
}


void T4CaloNoise::makeANoise ( int& cellNumber, int& ecalNumber, double& energy, double& time )
{
    if (allCDF == NULL){
      T4SMessenger::getInstance()->printfMessage ( T4SFatalError,__LINE__,__FILE__,"Wanted to make noise but database not initialized!!!!\n");
    }
    
    //throw for time
    double random = myRandom->Uniform(-50,50);
    
    time = random;
    
    //throw for calorimeter module
    random = myRandom->Rndm();
    
    double uniqueNumber = cdfInvert ( random,allCDF,false );

    ecalNumber = static_cast<int> ( uniqueNumber ) / 10000;
    cellNumber = static_cast<int> ( uniqueNumber ) % 10000;

    //throw for energy
    random = myRandom->Rndm();

    //find CDF for chosen module
    map<int,TH1D*>::iterator myIter = allCDFs.find ( static_cast<int> ( uniqueNumber ) );

    energy =-1.0;
    if ( myIter != allCDFs.end() ) {
        energy = cdfInvert ( random,myIter->second, true );
    } else {
        T4SMessenger::getInstance()->printfMessage ( T4SFatalError,__LINE__,__FILE__,"Module number %i not found!! Database inconsistant!\n",static_cast<int> ( uniqueNumber ) );
    }
    //smear the energies out a bit from the mean values
    random =  myRandom->Uniform(-0.050,0.050);
    energy += random;
//     printf("Noise generated, energy: %.3e, time %.2f, cell %i\n",energy,time,static_cast<int> ( uniqueNumber ) );
}

double T4CaloNoise::cdfInvert ( double _random, TH1D* _histo, bool _center )
{
    for ( int i = 1; i < _histo->GetNbinsX(); i++ )
        if ( _histo->GetBinContent ( i ) > _random )
            if ( _center ) {
                return _histo->GetXaxis()->GetBinCenter ( i );
            } else {
                return _histo->GetXaxis()->GetBinLowEdge ( i );
            }
}



int T4CaloNoise::numberOfNoiseCells ( double _rate )
{
    if ( _rate != -1 ) {
        setRate ( _rate );
    }
    int nNoise = myRandom->Poisson (rate);
    return nNoise;
}



void T4CaloNoise::loadCDFs ( string _fileName )
{
    for ( std::map<int,TH1D*>::iterator it=allCDFs.begin(); it!=allCDFs.end(); ++it ) {
        delete it->second;
    }
    allCDFs.clear();
    TFile* myFile = new TFile ( _fileName.c_str(),"read" );
    vector<string> histVec;
    if ( myFile->IsOpen() ) {
        TList* keyListHist = gDirectory->GetListOfKeys();
        TIterator* histList = keyListHist->MakeIterator();
        TObjString *histname_obj;
        while ( histname_obj = ( TObjString * ) histList->Next() ) {
            string histoname = ( histname_obj->String() ).Data();
            histVec.push_back ( histoname );
        }
    } else {
        T4SMessenger::getInstance()->printfMessage ( T4SFatalError,__LINE__,__FILE__,"Could not open ecal noise cdf file %s!!!!\n",_fileName.c_str() );
    }

    for ( unsigned int i =0; i < histVec.size(); i++ ) {
        string actHist = histVec.at ( i );
        vector<string> boomResult = explodeStringCustomDelim ( actHist,"_" );
        if ( boomResult.size() > 2 ) {
            TH1D* myHistNew = ( TH1D* ) myFile->Get ( actHist.c_str() );
            TH1D* cloneHist = ( TH1D* ) myHistNew->Clone();
            int uniqueNumber =strToInt ( boomResult.back() );
            allCDFs[uniqueNumber] = cloneHist;
        } else if ( actHist=="CellCDF" ) {
            allCDF = ( TH1D* ) myFile->Get ( actHist.c_str() )->Clone();
        } else if ( actHist=="CellInt" ) {
            allInt = ( TH1D* ) myFile->Get ( actHist.c_str() )->Clone();
        } else {
            T4SMessenger::getInstance()->printfMessage ( T4SErrorNonFatal,__LINE__,__FILE__,"Unhandled Histogramm name '%s' in file '%s' found!\n",actHist.c_str(),_fileName.c_str() );
        }
    }
}


T4CaloNoise::T4CaloNoise ( void )
{
    allCDF = NULL;
    allInt = NULL;
    myRandom=new TRandom3(0);
    amIActive = false;
}


T4CaloNoise::~T4CaloNoise ( void )
{
    for ( std::map<int,TH1D*>::iterator it=allCDFs.begin(); it!=allCDFs.end(); ++it ) {
        delete it->second;
    }
    allCDFs.clear();
}


