#ifndef T4SCALORIMETER_HH_
#define T4SCALORIMETER_HH_

#include "T4SXMLFileBackend.hh"

/*! \class T4SCalorimeter
 *  \brief This class inherits from class T4SDetectorsDat and reads
 *  the calorimeter information from a detectors.dat file.
 *
 *  This class and its functions are called in T4CaloManager. It converts
 *  the cmtx from a detectors.dat file to T4SCAL objects which are
 *  used in TGEANT to build up the calorimeter with its modules.
 */

class T4SCalorimeter : public T4SXMLFileBackend
{
  public:
    T4SCalorimeter(T4SStructManager* _structMan) :
        T4SXMLFileBackend(_structMan) {}
    
    /*! \brief reads an xml file */
    void readXML(string _inFile);
    
    
    /*! \brief writes an xml file */
    void writeXML(string _inFile);
    
    /*! \brief reads the detList from a given detectors.dat
    */
    void importCalosFromDetDat(string _inDetDat);
   
    /*! \brief Standard destructor.*/
    virtual ~T4SCalorimeter(void);

    /*! \brief Get all the calorimeter information read from the detectors.dat.
     *
     *  Make sure that you have loaded a detectors.dat first and called the addCaloInformation()
     *  function after before calling this function. Otherwise the size of the returning vector
     *  will be zero.
     *
     *  \return All the calorimeter information from the detectors.dat file.
     */
    const std::vector<T4SCAL>& getCalorimeter(void) const {return t4SCalorimeter;}

    /*! \brief Returns the calibration data for a single calo/module combination */
    double getCalib(int _calo, int _module);
    
  private:
    /*! \brief Container for the calorimeter information.*/
    std::vector<T4SCAL> t4SCalorimeter;
    
    /*! \brief Saves the calibration data read by the class from the xml file */
    caloCalib calibrationData[20];

    /*! \brief Check the detector ids of a cmtx line.
     *
     * This function also checks the detectors id to avoid same detector ids for different calorimeter modules.
     * Often seen for the last two line for HCAL2.
     * \param cmtxLine Content of one cmtx line.
     * */
    void checkCMTX(T4SCAL& cmtxLine);

    /*! \brief returns the index of usedIDs where the module blocks collide given the start id as parameter. Returns -1 if no collision) */
    int getCollision(int _startId);

    /*! \brief Calo-moduleId savevector - can be used to check for overlaps, stores information like pair<int firstModule, int blockSize> */
    vector< pair<int, int> > usedIDs;
};

#endif /* T4SCALORIMETER_HH_ */
