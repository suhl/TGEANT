#ifndef T4PYTHIATUNING_HH_
#define T4PYTHIATUNING_HH_

#include "T4SXMLFileBackend.hh"

class T4PythiaTuning : public T4SXMLFileBackend
{
public:
   /*! \brief basic constructor with struct manager, cannot work without struct manager
   */
    T4PythiaTuning(T4SStructManager* _structMan) : T4SXMLFileBackend(_structMan) {
        
    };
    ~T4PythiaTuning();
    
    /*! \brief reads an xml file */
    void readXML(string _inFile);
    /*! \brief writes an xml file */
    void writeXML(string _inFile);
    
    string MSBDTypeToString(T4SPythiaMSBDType _in);
    
    
    T4SPythiaMSBDType stringToMSBDType(string _in);
};




#endif



