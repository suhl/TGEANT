#ifndef T4SALIGNMENT_HH_
#define T4SALIGNMENT_HH_

#include "T4SDetectorsDat.hh"
#include <cstring>

/*! \class T4SAlignment
 *  \brief This class inherits from class T4SDetectorsDat and reads
 *         an alignment from a detectors.dat file.
 *
 *  For each detector or structure a function can be called to return
 *  its position. For some detectors several detector.dat entries are
 *  combined to one global position. This class is used in the GUI.
 */
class T4SAlignment : public T4SDetectorsDat
{
  public:
    /*! \brief Standard constructor. All positions are set to 0.*/
    T4SAlignment(void) {setDefault();}
    /*! \brief Standard destructor.*/
    virtual ~T4SAlignment(void) {}

    /*! \brief Read an alignment from a loaded detectors.dat file.
     *
     *  This function must be called after a detectors.dat file has been loaded.
     *  It reads the detectors.dat file and calculates the positions for all detectors.
     *  The different positions can be accessed with the different get-functions.
     */
    void readAlignment(void);

    // RPD
    double* getCA01R1(void) {return ca01r1;}
    double* getCA01R2(void) {return ca01r2;}
    double* getRP01R1(void) {return rp01r1;}
    double* getRP01R2(void) {return rp01r2;}

    // DCs
    double* getDC00(void) {return dc00;}
    double* getDC01(void) {return dc01;}
    double* getDC04(void) {return dc04;}
    double* getDC05(void) {return dc05;}

    // MW1
    double* getMA01X1(void) {return ma01x1;}
    double* getMA01X3(void) {return ma01x3;}
    double* getMA01Y1(void) {return ma01y1;}
    double* getMA01Y3(void) {return ma01y3;}
    double* getMA02X1(void) {return ma02x1;}
    double* getMA02X3(void) {return ma02x3;}
    double* getMA02Y1(void) {return ma02y1;}
    double* getMA02Y3(void) {return ma02y3;}

    // MW2
    double* getMB01X(void) {return mb01x;}
    double* getMB01Y(void) {return mb01y;}
    double* getMB01V(void) {return mb01v;}
    double* getMB02X(void) {return mb02x;}
    double* getMB02Y(void) {return mb02y;}
    double* getMB02V(void) {return mb02v;}

    // W45
    double* getDW01X(void) {return dw01x;}
    double* getDW01Y(void) {return dw01y;}
    double* getDW02X(void) {return dw02x;}
    double* getDW02Y(void) {return dw02y;}
    double* getDW03V(void) {return dw03v;}
    double* getDW03Y(void) {return dw03y;}
    double* getDW04Y(void) {return dw04y;}
    double* getDW04U(void) {return dw04u;}
    double* getDW05X(void) {return dw05x;}
    double* getDW05V(void) {return dw05v;}
    double* getDW06U(void) {return dw06u;}
    double* getDW06X(void) {return dw06x;}

    // RichWall
    double* getDR01X1(void) {return dr01x1;}
    double* getDR01X2(void) {return dr01x2;}
    double* getDR01Y1(void) {return dr01y1;}
    double* getDR01Y2(void) {return dr01y2;}
    double* getDR02X1(void) {return dr02x1;}
    double* getDR02X2(void) {return dr02x2;}
    double* getDR02Y1(void) {return dr02y1;}
    double* getDR02Y2(void) {return dr02y2;}

    // H1 & H2
    double* getH1(void) {return h1;}
    double* getH2(void) {return h2;}

    // ST02
    double* getST02X1(void) {return st02x1;}
    double* getST02X2(void) {return st02x2;}
    double* getST02Y1(void) {return st02y1;}
    double* getST02Y2(void) {return st02y2;}
    double* getST02U1(void) {return st02u1;}
    double* getST02V1(void) {return st02v1;}

    // ST03
    double* getST03X1(void) {return st03x1;}
    double* getST03X2(void) {return st03x2;}
    double* getST03Y1(void) {return st03y1;}
    double* getST03Y2(void) {return st03y2;}
    double* getST03U1(void) {return st03u1;}
    double* getST03V1(void) {return st03v1;}

    // ST05
    double* getST05X1(void) {return st05x1;}
    double* getST05Y2(void) {return st05y2;}
    double* getST05U1(void) {return st05u1;}

    // MM
    double* getMMX(int mmNo) {return mmx[mmNo];}
    double* getMMY(int mmNo) {return mmy[mmNo];}
    double* getMMV(int mmNo) {return mmv[mmNo];}
    double* getMMU(int mmNo) {return mmu[mmNo];}

    // PixelMM
    double* getMPX(int mpNo) {return mpx[mpNo];}
    double* getMPY(int mpNo) {return mpy[mpNo];}
    double* getMPU(int mpNo) {return mpu[mpNo];}
    double* getMPV(int mpNo) {return mpv[mpNo];}

    // GEM
    double* getGMX(int gemNo) {return gmx[gemNo-1];}
    double* getGMU(int gemNo) {return gmu[gemNo-1];}
    double* getGP01X(void) {return gp01x;}
    double* getGP01U(void) {return gp01u;}
    double* getGP02X(void) {return gp02x;}
    double* getGP02U(void) {return gp02u;}
    double* getGP03X(void) {return gp03x;}
    double* getGP03U(void) {return gp03u;}

    // MWPC
    double* getPA(int paNo) {return pa[paNo-1];}
    double* getPB(int pbNo) {return pb[pbNo-1];}
    double* getPS01(void) {return ps01;}

    // SI
    double* getSIX(int siNo) {return six[siNo-1];}
    double* getSIU(int siNo) {return siu[siNo-1];}

    // FI
    double* getFI(int fiNo) {return fi[fiNo-1];}

    // Veto
    double* getVI01P1(void) {return vi01p1;}
    double* getVO01X1(void) {return vo01x1;}
    double* getVI02X1(void) {return vi02x1;}

    // Calorimeter
    double* getECAL0(void) {return ecal0;}
    double* getECAL1(void) {return ecal1;}
    double* getECAL2(void) {return ecal2;}
    double* getHCAL1(void) {return hcal1;}
    double* getHCAL2(void) {return hcal2;}

    // Magnets
    double* getSM1(void) {return sm1;}
    double* getSM2(void) {return sm2;}

    // RICH
    double* getRICH(void) {return rich;}

    // H3
    double* getHO03(void) {return ho03;}

    // H4
    double* getHI04X1_u(void) {return hi04x1_u;}
    double* getHI04X1_d(void) {return hi04x1_d;}
    double* getHM04Y1_u1(void) {return hm04y1_u1;}
    double* getHM04Y1_u2(void) {return hm04y1_u2;}
    double* getHM04Y1_d1(void) {return hm04y1_d1;}
    double* getHM04Y1_d2(void) {return hm04y1_d2;}
    double* getHM04X1_u(void) {return hm04x1_u;}
    double* getHM04X1_d(void) {return hm04x1_d;}
    double* getHL04X1_1(void) {return hl04x1_1;}
    double* getHL04X1_2(void) {return hl04x1_2;}
    double* getHL04X1_3(void) {return hl04x1_3;}
    double* getHL04X1_4(void) {return hl04x1_4;}
    double* getHO04(void) {return ho04;}

    // H5
    double* getHI05X1_u(void) {return hi05x1_u;}
    double* getHI05X1_d(void) {return hi05x1_d;}
    double* getHM05Y1_u1(void) {return hm05y1_u1;}
    double* getHM05Y1_u2(void) {return hm05y1_u2;}
    double* getHM05Y1_d1(void) {return hm05y1_d1;}
    double* getHM05Y1_d2(void) {return hm05y1_d2;}
    double* getHM05X1_u(void) {return hm05x1_u;}
    double* getHM05X1_d(void) {return hm05x1_d;}
    double* getHL05X1_1(void) {return hl05x1_1;}
    double* getHL05X1_2(void) {return hl05x1_2;}
    double* getHL05X1_3(void) {return hl05x1_3;}
    double* getHL05X1_4(void) {return hl05x1_4;}

    double* getHK01(void) {return hk01;}
    double* getHK02(void) {return hk02;}
    double* getHK03(void) {return hk03;}
    double* getSandwichVeto(void) {return sandwichVeto;}
    double* getMultiplicityCounter(void) {return multiplicityCounter;}
    double* getHH02(void) {return hh02;}
    double* getHH03(void) {return hh03;}

  private:
    /*! \brief This function sets all positions to 0.*/
    void setDefault(void);

    /*! \brief Set the position of a detector.
     *
     *  If this position is already set what means that this function is
     *  called twice for the given detector (e.g. unexpected behavior of
     *  the detectors.dat file), the function throws a warning.
     *  \param position A pointer position array which should be set.
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     */
    inline void setPosition(double* position, map<int, vector<string> >::iterator iter);

    /*! \brief This function sets the z position of Straw planes.
     *
     *  The z position is calculated as the average of the ub and db plane.
     *  \param position A pointer position array which should be set.
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     * */
    inline void setStrawZPosition(double* position, map<int, vector<string> >::iterator iter);

    /*! \brief This function sets the x and y position of Straw planes.
     *
     *  The x position is taken from ST0???ub plane (n.b.: for Y plane this is about the y position)
     *  The y position should be equal for ub and db, so we take ub (n.b.: for Y plane this is about the x position)
     *  \param position A pointer position array which should be set.
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     */
    inline void setStrawXYPosition(double* position, map<int, vector<string> >::iterator iter);

    /*! \brief This function sets the position of the calorimeters.
     *
     *  \param position A pointer position array which should be set.
     *  \param vec An vector which contains the position read from the detectors.dat.
     * */
    inline void setCaloPosition(double* position, vector<string>& vec);

    /*! \brief This function sets the position of the magnets.
     *
     *  \param position A pointer position array which should be set.
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     * */
    inline void setMagPosition(double* position, map<int, vector<string> >::iterator iter);

    /*! \brief This function sets the position of the RICH.
     *
     *  \param position A pointer position array which should be set.
     *  \param vec An vector which contains the position read from the detectors.dat.
     * */
    inline void setRichPosition(double* position, vector<vector<string> >& vec);

    /*! \brief This function sets the position of a detector using a counter.
     *
     *  If we find the same tbName in detectors.dat for one detector, we can enter this
     *  function several times and it always calculates the mean value of the position.
     *
     *  \param position A pointer position array which should be set.
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     *  \param counter A pointer to the corresponding detector counter.
     */
    inline void setPosition(double* position, map<int, vector<string> >::iterator iter, int* counter);

    /*! \brief This function sets the position of the middle trigger Y plane.
     *
     *  There exists two planes with the same tbName. The difference is the
     *  y-position and the y-size of the plane. The plane with the bigger size is
     *  on top for UP-plane (on bottom for DOWN-plane). If there is for some
     *  reasons only one of the two planes in the detectors.dat file,
     *  the _u1 plane will be constructed.
     *
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     *  \param key Two char values, the first for the number of the system ('4' or '5')
     *         and the second for the plane type ('u' or 'd').
     */
    inline void setHMYPosition(map<int, vector<string> >::iterator iter, char key[2]);

    /*! \brief This function sets the position of the ladder trigger X plane.
     *
     *  There exists 4 planes with the same tbName. The difference is the
     *  x position and the x size of the planes. The bigger the size, the bigger
     *  the x value because this part of the plane stands more outside of the beam.
     *  Each call of this function will add one plane. The existing planes
     *  will be ordered looking on the x-position.
     *
     *  \param iter An iterator which contains the position as vector<string> read from the detectors.dat.
     *  \param key A char identifying the number of the system ('4' or '5').
     */
    inline void setHLXPosition(map<int, vector<string> >::iterator iter, char key);

    /*! \brief The first three values of the array are for the position.
     *         The last value is used as a boolean and describes if the plane
     *         is in use (0 = false, 1 = true).
     */
    // RPD
    double ca01r1[4];
    double ca01r2[4];
    double rp01r1[4];
    double rp01r2[4];

    // DCs
    double dc00[4];
    double dc01[4];
    double dc04[4];
    double dc05[4];

    // MW1
    double ma01x1[4];
    double ma01x3[4];
    double ma01y1[4];
    double ma01y3[4];
    double ma02x1[4];
    double ma02x3[4];
    double ma02y1[4];
    double ma02y3[4];

    // MW2
    double mb01x[4];
    double mb01y[4];
    double mb01v[4];
    double mb02x[4];
    double mb02y[4];
    double mb02v[4];

    // W45
    double dw01x[4];
    double dw01y[4];
    double dw02x[4];
    double dw02y[4];
    double dw03v[4];
    double dw03y[4];
    double dw04y[4];
    double dw04u[4];
    double dw05x[4];
    double dw05v[4];
    double dw06u[4];
    double dw06x[4];

    // RichWall
    double dr01x1[4];
    double dr01x2[4];
    double dr01y1[4];
    double dr01y2[4];
    double dr02x1[4];
    double dr02x2[4];
    double dr02y1[4];
    double dr02y2[4];

    // H1 & H2
    double h1[4];
    double h2[4];
    int h1Counter;
    int h2Counter;

    // ST02
    double st02x1[4];
    double st02x2[4];
    double st02y1[4];
    double st02y2[4];
    double st02u1[4];
    double st02v1[4];

    // ST03
    double st03x1[4];
    double st03x2[4];
    double st03y1[4];
    double st03y2[4];
    double st03u1[4];
    double st03v1[4];

    // ST05
    double st05x1[4];
    double st05y2[4];
    double st05u1[4];

    // MM
    // entry [0] not in use
    double mmx[4][4];
    double mmy[4][4];
    double mmv[4][4];
    double mmu[4][4];
    int mmxCounter[4];
    int mmyCounter[4];
    int mmvCounter[4];
    int mmuCounter[4];

    // PixelMM
    // [stationNumber][position=4]
    double mpx[4][4];
    double mpy[4][4];
    double mpu[4][4];
    double mpv[4][4];
    int mpxCounter[4];
    int mpyCounter[4];
    int mpuCounter[4];
    int mpvCounter[4];

    // GEM
    double gmx[11][4];
    double gmu[11][4];
    double gp01x[4];
    double gp01u[4];
    double gp02x[4];
    double gp02u[4];
    double gp03x[4];
    double gp03u[4];

    // MWPC
    double pa[11][4];
    double pb[6][4];
    double ps01[4];

    // SI
    double six[5][4];
    double siu[5][4];

    // FI
    double fi[55][5];

    // Veto
    double vi01p1[4];
    double vo01x1[4];
    double vi02x1[4];

    // Calorimeter
    double ecal0[4];
    double ecal1[4];
    double ecal2[4];
    double hcal1[4];
    double hcal2[4];

    // Magnets
    double sm1[6];
    double sm2[6];

    // RICH
    double rich[4];

    // H3
    double ho03[4];
    int ho03Counter;

    // H4
    double hi04x1_u[4];
    double hi04x1_d[4];
    double hm04y1_u1[4];
    double hm04y1_u2[4];
    double hm04y1_d1[4];
    double hm04y1_d2[4];
    double hm04x1_u[4];
    double hm04x1_d[4];
    double hl04x1_1[4];
    double hl04x1_2[4];
    double hl04x1_3[4];
    double hl04x1_4[4];
    double ho04[4];
    int ho04Counter;

    // H5
    double hi05x1_u[4];
    double hi05x1_d[4];
    double hm05y1_u1[4];
    double hm05y1_u2[4];
    double hm05y1_d1[4];
    double hm05y1_d2[4];
    double hm05x1_u[4];
    double hm05x1_d[4];
    double hl05x1_1[4];
    double hl05x1_2[4];
    double hl05x1_3[4];
    double hl05x1_4[4];

    double hk01[4];
    double hk02[4];
    double hk03[4];
    double sandwichVeto[4];
    int sandwichVetoCounter;
    double multiplicityCounter[4];
    int multiplicityCounterCounter;
    double hh02[4];
    double hh03[4];
};

#endif /* T4SALIGNMENT_HH_ */
