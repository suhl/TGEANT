#ifndef T4SFILEBACKEND_HH_
#define T4SFILEBACKEND_HH_

#include "T4SStructManager.hh"
#include "T4SMessenger.hh"
#include "T4PythiaTuning.hh"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <sys/stat.h>

/*! \class T4SFileBackEnd 
 *  \brief The parent class of all configurations file class implementation.
 * 
 *  This class is the parent of all implementations of config outputs. Valid implementations are currently only 
 *  T4SSettingsFile and T4SSettingsFileXML of which the ladder is the new standard and the first is the old standard.
 *  In principle one can add new implementations as a derivative of this class and save settings in MySQL or wherever needed.
 */
class T4SFileBackEnd
{
  public:
    /*! \brief The constructor  */
    T4SFileBackEnd(void) {structM = NULL;}
    /*! \brief The virtual destructor */
    virtual ~T4SFileBackEnd(void) {}

    /*! \brief Virtual function to just load the settings from the file
     *  
     *  This function does not check the values of the loaded strings for keystrings as "default" or "unset"!
     *  It is important for the GUI, as it will be called to load the strings into the interface to keep "default" as "default" and
     *  not to replace it with the actual path on that system, to keep the settings file portable.
     *  \param fileName string with path and filename where to load
     */
    virtual void load_unchecked(std::string) {}
    
    /*! \brief Virtual function to save the contents of the structM to a file 
     * 
     * This function saves the contents of the T4SStructManager structM to a file that is given via the parameter.
     * \param fileName string with path and filename where to save
     */
    virtual void save(std::string) {}
    
    
    /*! \brief Virtual function to load a file and replace keystrings with actual paths
     * 
     *  This function is the main one used in TGEANT and combines load_unchecked() and checkSettings().
     */
    void load(std::string file) {load_unchecked(file); checkSettings();}

  protected:
    /*! \brief the T4SStructManager that is in use */
    T4SStructManager* structM;
    /*! \brief the actual outstream in use*/
    std::ofstream saveFile;
    /*! \brief This function shall replace the keystrings "default" or "unset" with the actual paths from $TGEANT*/
    void checkSettings (void);
};

#endif /* T4SFILEBACKEND_HH_ */
