#ifndef T4SDETECTORSDAT_HH_
#define T4SDETECTORSDAT_HH_

#include "T4SFileBackEnd.hh"
#include "T4SGlobals.hh"
#include "T4XMLParser.hh"

#include <map>
#include <vector>
#include <iomanip>

#define DETDAT_LINECODE 0
#define DETDAT_ID 1
#define DETDAT_TBNAME 2
#define DETDAT_DET 3
#define DETDAT_UNIT 4
#define DETDAT_TYPE 5
#define DETDAT_RADLEN 6
#define DETDAT_ZSIZE 7
#define DETDAT_XSIZE 8
#define DETDAT_YSIZE 9
#define DETDAT_ZPOS 10
#define DETDAT_XPOS 11
#define DETDAT_YPOS 12
#define DETDAT_ROT 13
#define DETDAT_WIREDIST 14
#define DETDAT_ANGLE 15
#define DETDAT_NWIRES 16
#define DETDAT_PITCH 17
#define DETDAT_EFFIC 18
#define DETDAT_BACKGR 19
#define DETDAT_TGATE 20
#define DETDAT_DRVEL 21
#define DETDAT_T0 22
#define DETDAT_RES2HIT 23
#define DETDAT_SPACE 24
#define DETDAT_TSLICE 25

#define DEADDAT_LINECODE 0
#define DEADDAT_ID 1
#define DEADDAT_TBNAME 2
#define DEADDAT_DET 3
#define DEADDAT_UNIT 4
#define DEADDAT_SH 5
#define DEADDAT_ZSIZE 6
#define DEADDAT_XSIZE 7
#define DEADDAT_YSIZE 8
#define DEADDAT_ZPOS 9
#define DEADDAT_XPOS 10
#define DEADDAT_YPOS 11
#define DEADDAT_ROT 12

#define CALO_LINECODE 0
#define CALO_TBNAME 1
#define CALO_NAME 2
#define CALO_ZPOS 3
#define CALO_XPOS 4
#define CALO_YPOS 5

#define MAG_LINECODE 0
#define MAG_ID 1
#define MAG_ZPOS 2
#define MAG_XPOS 3
#define MAG_YPOS 4
#define MAG_ROT 5
#define MAG_SCALE 6
#define MAG_FLAG1 7
#define MAG_FLAG2 8
#define MAG_CURRENT 9

#define RICH_LINECODE 0
#define RICH_NAME 1
#define RICH_ZPOS 2
#define RICH_XPOS 3
#define RICH_YPOS 4

#define TARG_LINECODE 0
#define TARG_NUM 1
#define TARG_NAME 2
#define TARG_ZPOS 8
#define TARG_XPOS 9
#define TARG_YPOS 10

#define CMTX_ID 1
#define CMTX_DET 2
#define CMTX_TYPE 3
#define CMTX_MODNUM 4
#define CMTX_NROW 5
#define CMTX_NCOL 6
#define CMTX_ROFFSET 7
#define CMTX_COFFSET 8
#define CMTX_ZSIZE 9
#define CMTX_XSIZE 10
#define CMTX_YSIZE 11
#define CMTX_ZPOS 12
#define CMTX_XPOS 13
#define CMTX_YPOS 14
#define CMTX_XSTEP 15
#define CMTX_YSTEP 16
#define CMTX_TGATE 17
#define CMTX_TRESHOLD 18
#define CMTX_GEV2ADC 19
#define CMTX_RADLEN 20
#define CMTX_ABSLEN 21
#define CMTX_STOCHASTIC 22
#define CMTX_CONSTTERM 23
#define CMTX_CALIB 24
#define CMTX_NHOLES 25
#define CMTX_ECRIT 26
#define CMTX_MODNAME 27

using namespace std;

class T4SEfficiency;
class T4SCalorimeter;

/*! \class T4SDetectorsDat
 *  \brief This class inherits from class T4SFileBackEnd and can read
 *         or save a detectors.dat file.
 *
 *  This class can be used either to write a new detectors.dat or to
 *  read an existing one. For the latter, this class acts as base class
 *  for class T4SAlignment to read the detector positions or class
 *  T4SCalorimeter to read the calorimeter information.
 */

class T4SDetectorsDat : public T4SFileBackEnd
{
  public:
    /*! \brief Standard constructor.*/
    T4SDetectorsDat(void) {
      caloPosition = NULL;
      magnetPosition = NULL;
      richPosition = NULL;
      myEffic = NULL;
      myCalo = NULL;
      cmtxLines = NULL;
    }

    /*! \brief Standard destructor.
     *
     *  Note: We know about the memory leak (no delete used for lowResIDsEntries),
     *  but we want to avoid a segmentation fault.
     * */
    virtual ~T4SDetectorsDat(void) {}

    /*! \brief Load a detectors.dat file.
     *
     *  \param fileName Path to the detectors.dat file.
     * */
    void load(string fileName);

    /*! \brief Save a detectors.dat file.
     *
     *  \param fileName File name for the new created detectors.dat file.
     * */
    void save(string fileName);

    /*! \brief Get the wire detector container to add a new T4SWireDetector object.
     *
     *  This function has to be called for each sensitive wire detector in TGEANT
     *  to add the detectors wire information to the container.
     *  \return wireDet
     * */
    vector<T4SWireDetector>& getWireDetector(void) {return wireDet;}

    /*! \brief Get the dead zone container to add a new T4SDeadZone object.
     *
     *  This function has to be called for each sensitive wire detector in TGEANT
     *  which has a dead zone to add this to the container.
     *  \return deadZone
     * */
    vector<T4SDeadZone>& getDeadZone(void) {return deadZone;}

    /*! \brief Get the target container to add a new T4STargetInformation object.
     *
     *  This function has to be called for each target in TGEANT..
     *  \return targ
     * */
    vector<T4STargetInformation>& getTargetInformation(void) {return targ;}

    /*! \brief Add the missing wire information from the loaded detectors.dat to
     *         wire detector container.
     *
     *  This function has to be called before saving a new detectors.dat file.
     *  Not all the information for a wire detector line is set in TGEANT. The
     *  missing values are: radLength, effic, backgr, tgate, drVel, t0,
     *  res2hit, space and tslice. These values are read from the loaded
     *  effic.xml file and added to the wire detector container for each
     *  wire detector.
     * */
    void addWireInformationFromEffic(string _efficFile);

    /*! \brief Set the calorimeter positions.
     *
     *  This function has to be called before saving a new detectors.dat file.
     *  \param cal Reference to the vector<T4SDetector> for calorimeters used in TGEANT.
     * */
    void addCaloPosition(const vector<T4SDetector>& cal) {caloPosition = &cal;}

    /*! \brief Set the calorimeter positions.
     *
     *  This function has to be called before saving a new detectors.dat file.
     *  \param _cmtxLines Reference to the vector<T4SCAL> for calorimeters used in TGEANT.
     * */
    void addCaloCMTXLines(const vector<T4SCAL>* _cmtxLines) {cmtxLines = _cmtxLines;}

    /*! \brief Set the magnet positions.
     *
     *  This function has to be called before saving a new detectors.dat file.
     *  \param mag Reference to the vector<T4SMagnet> for magnets used in TGEANT.
     * */
    void addMagnetPosition(const std::vector<T4SMagnet>& mag) {magnetPosition = &mag;}

    /*! \brief Set the RICH position.
     *
     *  This function has to be called before saving a new detectors.dat file.
     *  \param rich Reference to the T4SRICH object used in TGEANT.
     * */
    void addRichPosition(const T4SRICH& rich) {richPosition = &rich;}

    /*! \brief Get all det entries of loaded detectors.dat file.
     *
     * \return detMap
     * */
    const map<int, vector<string> >& getDetMap(void) const {return detMap;}
    
    /*! \brief Get all mag entries of loaded detectors.dat file.
     *
     * \return magMap
     * */
    const map<int, vector<string> >& getMagMap(void) const {return magMap;}
    

    /*! \brief Get all dead entries of loaded detectors.dat file.
     *
     * \return deadMap
     * */
    const map<int, vector<string> >& getDeadMap(void) const {return deadMap;}

    /*! \brief Get all cmtx entries of loaded detectors.dat file.
     *
     * \return cmtxMap
     * */
    const map<int, vector<string> >& getCmtxMap(void) const {return cmtxMap;}

  protected:
    /*! \brief All detectors.dat entries with "det" line code.*/
    map<int, vector<string> > detMap;

    /*! \brief All detectors.dat entries with "rpd" line code.*/
    map<int, vector<string> > rpdMap;

    /*! \brief All detectors.dat entries with "dead" line code.*/
    map<int, vector<string> > deadMap;

    /*! \brief All detectors.dat entries with "cmtx" line code.*/
    map<int, vector<string> > cmtxMap;

    /*! \brief All detectors.dat entries with "calo" line code.*/
    vector<vector<string> > caloVec;

    /*! \brief All detectors.dat entries with "mag" line code.*/
    map<int, vector<string> > magMap;

    /*! \brief All detectors.dat entries with "targ" line code.*/
    map<int, vector<string> > targMap;

    /*! \brief All detectors.dat entries with "phot0" line code.*/
    vector<vector<string> > richVec;

  private:
    
    /*! \brief The efficiency and magnet flag database */
    T4SEfficiency* myEffic;
    
    /*! \brief The calorimeter-lines from the calo xml file */
    T4SCalorimeter* myCalo;
    
    /*! \brief The wire detector container used for writing a detectors.dat file.*/
    vector<T4SWireDetector> wireDet;

    /*! \brief The dead zone container used for writing a detectors.dat file.*/
    vector<T4SDeadZone> deadZone;

    /*! \brief The target container used for writing a detectors.dat file.*/
    vector<T4STargetInformation> targ;

    /*! \brief All detectors.dat entries with "cath" line code.*/
    vector<string> richCath;

    /*! \brief All detectors.dat entries with "mirr" line code.*/
    vector<string> richMirr;

    /*! \brief Add a line to saveFile.
     *
     * \param newLine A new line for saveFile.
     * */
    void addLine(string newLine) {saveFile << newLine << endl;}

    /*! \brief Add a new line to saveFile.*/
    void addNL(void) {saveFile << endl;}
    /*! \brief Add a new lines to saveFile.
     *
     * \param i Number of added new lines to saveFile.
     * */
    void addNL(int i) {for (int j = 0; j < i; j++) addNL();}

    /*! \brief Write the detectors.dat header.*/
    void writeHeader(void);

    /*! \brief Write the magnets block.*/
    void writeMagnets(void);

    /*! \brief Write the rotations block.*/
    void writeRotation(void);

    /*! \brief Write the target block.*/
    void writeTarget(void);

    /*! \brief Write the wire header block.*/
    void writeWireHeader(void);

    /*! \brief Write the wire detectors block.*/
    void writeWireDetectors(void);

    /*! \brief Write the dead zone header block.*/
    void writeDeadHeader(void);

    /*! \brief Write the dead zone block.*/
    void writeDeadZone(void);

    /*! \brief Write the calorimeter positions block.*/
    void writeCaloPosition(void);

    /*! \brief Write the cmtx block.*/
    void writeCaloCMTX(void);

    /*! \brief Write the RICH block.*/
    void writeRich(void);

    /*! \brief T4SDetector container of the calorimeters.*/
    const vector<T4SDetector>* caloPosition;
    /*! \brief T4SDetector container of the cmtx lines.*/
    const vector<T4SCAL>* cmtxLines;

    /*! \brief Convert TGEANT calorimeter name to the detectors.dat TBname.
     *
     * \param name TGEANT calorimeter name.
     * \return TBName of the calorimeter.
     * */
    string getCaloTBName(string name);

    /*! \brief Convert TGEANT calorimeter name to the detectors.dat name.
     *
     * \param name TGEANT calorimeter name.
     * \return name of the calorimeter.
     * */
    string getCaloName(string name);

    /*! \brief T4SMagnet container of the magnets.*/
    const std::vector<T4SMagnet>* magnetPosition;

    /*! \brief T4SRICH object for the RICH position.*/
    const T4SRICH* richPosition;
};

#endif /* T4SDETECTORSDAT_HH_ */
