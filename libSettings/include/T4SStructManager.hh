#ifndef T4SSTRUCTMANAGER_HH_
#define T4SSTRUCTMANAGER_HH_

#include <vector>

#include "T4SGlobals.hh"

/*! \class T4SStructManager
 *  \brief This class contains all structs and objects that TGEANT, GUI and a
 *         settings file needs.
 *
 *  This class is used in class T4SFileBackEnd and all derived classes
 *  as well as in class T4SettingsFile in TGEANT and class T4ISettingsFile
 *  in the GUI. Several get-functions give access to the private structs
 *  and objects in this class. The get functions are mostly not-const for
 *  writing via GUI.
 */
class T4SStructManager
{
  public:
    /*! \brief Standard constructor.*/
    T4SStructManager(void);
    /*! \brief Standard destructor.*/
    virtual ~T4SStructManager(void) {}

  public:
    /*! \brief This function clears all vectors and sets all structs to
     *         default values.
     */
    void clearStructManager(void);

    /*! \brief Get a pointer to the T4SGeneral struct.
     *  \return general
     */
    T4SGeneral* getGeneral(void) {return &general;}

    /*! \brief Get a pointer to the T4SBeam struct.
     *  \return beam
     */
    T4SBeam* getBeam(void) {return &beam;}

    /*! \brief Get a pointer to the T4SUser struct.
     *  \return user
     */
    T4SUser* getUser(void) {return &user;}

    /*! \brief Get a pointer to the T4SCosmic struct.
     *  \return cosmics
     */
    T4SCosmic* getCosmic(void) {return &cosmics;}

    /*! \brief Get a pointer to the T4SEcalCalib struct.
     *  \return ecalCalib
     */
    T4SEcalCalib* getEcalCalib(void) {return &ecalCalib;}

    /*! \brief Get a pointer to the T4SPythia struct.
     *  \return pythia
     */
    T4SPythia* getPythia(void) {return &pythia;}

    /*! \brief Get a pointer to the T4SHEPGen struct.
     *  \return hepgen
     */
    T4SHEPGen* getHEPGen(void) {return &hepgen;}

    /*! \brief Get a pointer to the T4SPrimGen struct.
     *  \return primgen
     */
    T4SPrimGen* getPrimGen(void) {return &primgen;}

    /*! \brief Get a pointer to the T4SPrimTrig struct.
     *  \return primtrig
     */
    T4SPrimTrig* getPrimTrig(void) {return &primtrig;}

    /*! \brief Get a pointer to the T4SSampling struct.
     *  \return sampling
     */
    T4SSampling* getSampling(void) {return &sampling;}

    /*! \brief Get a pointer to the T4SExternalFiles struct.
     *  \return external
     */
    T4SExternalFiles* getExternal(void) {return &external;}

    /*! \brief Get a pointer to the T4SRPD container.
     *  \return t4SRPD
     */
    std::vector<T4SRPD>* getRPD(void) {return &t4SRPD;}

    /*! \brief Get a pointer to the T4SMagnet container.
     *  \return t4SMagnet
     */
    std::vector<T4SMagnet>* getMagnet(void) {return &t4SMagnet;}

    /*! \brief Get a pointer to the T4SRICH struct.
     *  \return t4SRICH
     */
    T4SRICH* getRICH(void) {return &t4SRICH;}

    /*! \brief Get a pointer to the T4SMW1 struct.
     *  \return t4SMW1
     */
    T4SMW1* getMW1(void) {return &t4SMW1;}

    /*! \brief Get a pointer to the T4SMW2 struct.
     *  \return t4SMW2
     */
    T4SMW2* getMW2(void) {return &t4SMW2;}

    /*! \brief Get a pointer to the T4SPolGPD struct.
     *  \return t4SPolGPD
     */
    T4SPolGPD* getPolGPD(void) {return &t4SPolGPD;}

    /*! \brief Get a pointer to the T4SSciFiTest struct.
     *  \return t4SSciFiTest
     */
    T4SSciFiTest* getSciFiTest(void) {return &t4SSciFiTest;}

    /*! \brief Get a pointer to the T4SDetector container.
     *  \return t4SDetector
     */
    std::vector<T4SDetector>* getDetector(void) {return &t4SDetector;}

    /*! \brief Get a pointer to the T4SDetectorRes container.
     *  \return t4SDetectorRes
     */
    std::vector<T4SDetectorRes>* getDetectorRes(void) {return &t4SDetectorRes;}

    /*! \brief Get a pointer to the const T4SDetector container for the Straws.
     *  \return t4SStraw
     */
    const std::vector<T4SDetector>* getStraw(void) const {return &t4SStraw;}

    /*! \brief Add a new Straw detector with given name to t4SStraw.
     *
     *  \param name Name of the new Straw detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addStraw(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the Micromegas.
     *  \return t4SMM
     */
    const std::vector<T4SDetector>* getMicromegas(void) const {return &t4SMM;}

    /*! \brief Add a new Micromegas detector with given name to t4SMM.
     *
     *  \param name Name of the new Micromegas detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addMicromegas(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the GEMs.
     *  \return t4SGEM
     */
    const std::vector<T4SDetector>* getGem(void) const {return &t4SGEM;}

    /*! \brief Add a new GEM detector with given name to t4SGEM.
     *
     *  \param name Name of the new GEM detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addGem(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the SciFis.
     *  \return t4SFI
     */
    const std::vector<T4SDetector>* getSciFi(void) const {return &t4SFI;}

    /*! \brief Add a new SciFi detector with given name to t4SFI.
     *
     *  \param name Name of the new SciFi detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addSciFi(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the Silicons.
     *  \return t4SSI
     */
    const std::vector<T4SDetector>* getSilicon(void) const {return &t4SSI;}

    /*! \brief Add a new Silicons detector with given name to t4SSI.
     *
     *  \param name Name of the new Silicons detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addSilicon(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the MWPCs.
     *  \return t4SMWPC
     */
    const std::vector<T4SDetector>* getMWPC(void) const {return &t4SMWPC;}

    /*! \brief Add a new MWPC detector with given name to t4SMWPC.
     *
     *  \param name Name of the new MWPC detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addMWPC(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the Vetos.
     *  \return t4SVeto
     */
    const std::vector<T4SDetector>* getVeto(void) const {return &t4SVeto;}

    /*! \brief Add a new Veto detector with given name to t4SVeto.
     *
     *  \param name Name of the new Veto detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addVeto(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the DCs.
     *  \return t4SDC
     */
    const std::vector<T4SDetector>* getDC(void) const {return &t4SDC;}

    /*! \brief Add a new DC detector with given name to t4SDC.
     *
     *  \param name Name of the new DC detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addDC(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the W45.
     *  \return t4SW45
     */
    const std::vector<T4SDetector>* getW45(void) const {return &t4SW45;}

    /*! \brief Add a new W45 detector with given name to t4SW45.
     *
     *  \param name Name of the new W45 detector.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addW45(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the Calorimeters.
     *  \return t4SCalorimeter
     */
    const std::vector<T4SDetector>* getCalorimeter(void) const {return &t4SCalorimeter;}

    /*! \brief Add a new Calorimeter with given name to t4SCalorimeter.
     *
     *  \param name Name of the new Calorimeter.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addCalorimeter(std::string name);

    /*! \brief Get a pointer to the const T4SDetector container for the RichWall planes.
     *  \return t4SRichWall
     */
    const std::vector<T4SDetector>* getRichWall(void) const {return &t4SRichWall;}

    /*! \brief Add a new RichWall plane with given name to t4SRichWall.
     *
     *  \param name Name of the new RichWall plane.
     *  \return Pointer to the new created T4SDetector object.
     */
    T4SDetector* addRichWall(std::string name);

    /*! \brief Get a pointer to the const T4SDummy container for the DUMMY planes.
     *  \return t4SDummy
     */
    const std::vector<T4SDummy>* getDummies(void) const {return &t4SDummy;}

    /*! \brief Add a new DUMMY detector.
     *
     *  \return Pointer to the new created T4SDummy object.
     */
    T4SDummy* addDummy(void);

    /*! \brief Add a new DUMMY detector.
     */
    void addDummy(T4SDummy dummy) {t4SDummy.push_back(dummy);}

  private:
    /*! \brief T4SGeneral struct.*/
    T4SGeneral general;
    /*! \brief T4SBeam struct.*/
    T4SBeam beam;
    /*! \brief T4SUser struct.*/
    T4SUser user;
    /*! \brief T4SCosmic struct.*/
    T4SCosmic cosmics;
    /*! \brief T4SEcalCalib struct.*/
    T4SEcalCalib ecalCalib;
    /*! \brief T4SPythia struct.*/
    T4SPythia pythia;
    /*! \brief T4SHEPGen struct.*/
    T4SHEPGen hepgen;
    /*! \brief T4SPrimGen struct.*/
    T4SPrimGen primgen;
    /*! \brief T4SPrimTrig struct.*/
    T4SPrimTrig primtrig;
    /*! \brief T4SSampling struct.*/
    T4SSampling sampling;
    /*! \brief T4SExternalFiles struct.*/
    T4SExternalFiles external;
    /*! \brief T4SRPD Container for all RPD.*/
    std::vector<T4SRPD> t4SRPD;
    /*! \brief T4SMagnet Container for all magnets.*/
    std::vector<T4SMagnet> t4SMagnet;
    /*! \brief T4SRICH struct.*/
    T4SRICH t4SRICH;
    /*! \brief T4SMW1 struct.*/
    T4SMW1 t4SMW1;
    /*! \brief T4SMW2 struct.*/
    T4SMW2 t4SMW2;
    /*! \brief T4SPolGPD struct.*/
    T4SPolGPD t4SPolGPD;
    /*! \brief T4SSciFiTest struct.*/
    T4SSciFiTest t4SSciFiTest;

    /*! \brief Container for all T4SDetector w/o special treatment.*/
    std::vector<T4SDetector> t4SDetector;
    /*! \brief Container for all T4SDetectorRes.*/
    std::vector<T4SDetectorRes> t4SDetectorRes;
    /*! \brief T4SDetector Container for all Straws.*/
    std::vector<T4SDetector> t4SStraw;
    /*! \brief T4SDetector Container for all Micromegas.*/
    std::vector<T4SDetector> t4SMM;
    /*! \brief T4SDetector Container for all GEMs.*/
    std::vector<T4SDetector> t4SGEM;
    /*! \brief T4SDetector Container for all PGEMs.*/
    std::vector<T4SDetector> t4SPGEM;
    /*! \brief T4SDetector Container for all SciFis.*/
    std::vector<T4SDetector> t4SFI;
    /*! \brief T4SDetector Container for all Silicons.*/
    std::vector<T4SDetector> t4SSI;
    /*! \brief T4SDetector Container for all MWPCs.*/
    std::vector<T4SDetector> t4SMWPC;
    /*! \brief T4SDetector Container for all Vetos.*/
    std::vector<T4SDetector> t4SVeto;
    /*! \brief T4SDetector Container for all DCs.*/
    std::vector<T4SDetector> t4SDC;
    /*! \brief T4SDetector Container for all W45.*/
    std::vector<T4SDetector> t4SW45;
    /*! \brief T4SDetector Container for all Calorimeters.*/
    std::vector<T4SDetector> t4SCalorimeter;
    /*! \brief T4SDetector Container for all RichWall planes.*/
    std::vector<T4SDetector> t4SRichWall;
    /*! \brief T4SDummy Container for all DUMMY planes.*/
    std::vector<T4SDummy> t4SDummy;

    /*! \brief Adds a new magnet with given name to t4SMagnet.
     *  \param name Name of the new magnet.
     */
    inline void addT4SMagnet(std::string name);

    /*! \brief Adds a new RPD with given name to t4SRPD.
     *  \param name Name of the new RPD.
     */
    inline void addT4SRPD(std::string name);

    /*! \brief Adds a new T4SDetector with given name to t4SDetector.
     *  \param name Name of the new T4SDetector.
     */
    inline void addT4SDetector(std::string name);

    /*! \brief Adds a new T4SDetectorRes with given name to t4SDetectorRes.
     *  \param name Name of the new T4SDetectorRes.
     */
    inline void addT4SDetectorRes(std::string name);
};

#endif /* T4SSTRUCTMANAGER_HH_ */
