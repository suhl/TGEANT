#ifndef T4SMESSENGER_HH
#define T4SMESSENGER_HH

#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <string>
#include <ostream>
#include <cassert>
#include <ctime>
#include <sstream>
#include <cstdlib>

#define CRESET           0
#define CBRIGHT          1
#define CDIM             2
#define CUNDERLINE       3
#define CBLINK           4
#define CREVERSE         7
#define CHIDDEN          8

#define CBLACK           0
#define CRED             1
#define CGREEN           2
#define CYELLOW          3
#define CBLUE            4
#define CMAGENTA         5
#define CCYAN            6
#define CWHITE           7
#define CDEFAULT         9

#define CTHEME_UNSET     0
#define CTHEME_DARK      1
#define CTHEME_LIGHT     2

using namespace std;

enum statusCode
{
  T4SVerboseMoreMore = 3, T4SVerboseMore = 2, T4SVerbose = 1, T4SNotice = 0,
  T4SWarning = -1, T4SErrorNonFatal = -2, T4SFatalError = -3
};
/*! \class T4SMessenger
 *  \brief A unified implementation for printing messages to the screen
 *  
 *  This class prints messages to the stdout stream. Depending on verboseLevel and message-status
 *  aka if it is an error or a warning, it changes the colour of the cout and checks if the needed verboseLevel is activated.
 *  The implementation is a singleton-design-type, so one can use it anywhere.
 */
class T4SMessenger
{
  public:
    /*! \brief The destructor method */
    ~T4SMessenger(void) {instanceFlag = false; single = NULL;}
    /*! \brief The getInstance method for getting the active instance*/
    static T4SMessenger* getInstance(void);
    /*! \brief This method returns a reference instead of a pointer used at the getInstance() method - useful for piping */
    static T4SMessenger& getReference(void);

    /*! \brief The core method for printing messages! 
     *  \param _statusCode The severity level of the message you have to say - of type statusCode
     *  \param _lineNumber Should just be called with __LINE__ so that a lineNumber in the source can be saved
     *  \param _fileName  Should just be called with __FILE__ so that a fileName in the source can be saved
     *  \param _message The message you want to print.
     *  \param _printEndl A bool that determines if a endline shall be put to stdout after your message. It is optional, default is true.
     */
    void printMessage(statusCode _statusCode, int _lineNumber, string _fileName,
        string _message, bool _printEndl = true);
    
    
    /*! \brief The c-style method for printing with standard printf replacers - this does NOT print endlines! Add them yourself!
     *  \param _statusCode The severity level of the message you have to say - of type statusCode
     *  \param _lineNumber Should just be called with __LINE__ so that a lineNumber in the source can be saved
     *  \param _fileName  Should just be called with __FILE__ so that a fileName in the source can be saved
     *  \param _message The message you want to print - printf formating allowed
     *  \param ... the parameters to fill in
     */
    void printfMessage(statusCode _statusCode, int _lineNumber, string _fileName, string _message, ...);
    

    /*! \brief This method sets the active verboseLevel
     *  If the verboseLevel is too low, some messages get ignored by this class.
     */
    void setVerboseLevel(int _newLevel) {verboseLevel = _newLevel;}
    /*! \brief Gets the active verbose level
     */
    int getVerboseLevel(void) {return verboseLevel;}

    /*! \brief This prints the current Time to stdout, given a specific _format
     *  \param _format a char* containing the desired timeformat
     */
    void printCurrentTime(const char* _format);
    /*! \brief This function sets the colorTheme of the T4SMessenger
     *  \param _newTheme The index of the newTheme that should be used
     * 
     *  This class can print warnings/errors/messages in different colors.
     *  The problem with different console-backgrounds is, that not all combinations are well-readable.
     *  So this function can change the set of colors used for printing. Allowed values are:
     *  _newTheme=0 -- PRINT STANDARD COLORS (UNSET)
     *  _newTheme=1 -- PRINT OPTIMIZED FOR DARK COLORS (DARK)
     *  _newTheme=2 -- PRINT OPTIMIZED FOR LIGHT COLORS (LIGHT)
     */
    void setColorTheme(int _newTheme) {colorTheme = _newTheme;}

    /*! \brief this function prints time and sets statuscode for streaming messages
     *  \param _statusCode The severity level of the message you have to say - of type statusCode
     *  \param _lineNumber Should just be called with __LINE__ so that a lineNumber in the source can be saved
     *  \param _fileName  Should just be called with __FILE__ so that a fileName in the source can be saved
     *  \param _message The message you want to print.
     *  \param _printEndl A bool that determines if a endline shall be put to stdout after your message. It is optional, default is true.
     * This function makes this class usable as a pseudo-cout. You can just: getReference() << myVar << " YO! " << myVar2;
     * It is especially interesting if you do not want to sprintf your message before handing it to this class, because it would be too complex.
     * Make sure to call endStream() once you finished printing your complex message. 
     */ 
    void setupStream(statusCode _statusCode, int _lineNumber, string _fileName);
    /*! \brief ends the streamMode enabled by setupStream()
     */
    void endStream(void);

    friend T4SMessenger& operator<<(T4SMessenger& _out, const string& _msg);
    friend T4SMessenger& operator<<(T4SMessenger& _out, const int& _msg);
    friend T4SMessenger& operator<<(T4SMessenger& _out, const double& _msg);

  private:
    int colorTheme;
    void printStream(void);
    string currentTime(const char* _format = "%X");

    int themeTranslator(int _colour);

    stringstream streamBuffer;
    statusCode streamCode;
    int streamLine;
    string streamFile;

    void textcolor(int attr, int fg, int bg);
    int verboseLevel;
    static bool instanceFlag;
    static T4SMessenger* single;
    T4SMessenger(void);
};

#endif
