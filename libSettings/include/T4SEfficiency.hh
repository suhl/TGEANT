#ifndef T4SEFFIC_HH_
#define T4SEFFIC_HH_

#include "T4SXMLFileBackend.hh"
#include "T4SDetectorsDat.hh"


class T4SEfficiency : T4SXMLFileBackend
{
public:
  /*! \brief basic constructor with struct manager, cannot work without struct manager
   */
    T4SEfficiency(T4SStructManager* _structMan) : T4SXMLFileBackend(_structMan) {
        
    };
    ~T4SEfficiency();
    
    /*! \brief reads an xml file */
    void readXML(string _inFile);
    /*! \brief writes an xml file */
    void writeXML(string _inFile);
    
    /*! \brief reads the detList from a given detectors.dat
     */
    void importEfficiencyFromDetDat(string _inDetDat);
    
    
    /*! \brief adds wire information to a given wireDetector struct. Returns true if success!
     */
    bool addWireInformationToStruct(T4SWireDetector& writeTo);
    
    /*! \brief returns the whole detector List
     */
    vector<T4SWireDetector>& getDetList(void) {return detList;}
    vector<T4SMagnetLine>& getMagList(void) {return magList;}
    
    
private:
  
  void addWireInfoToExistingStruct(T4SWireDetector& _source, T4SWireDetector& destination);
  
  vector<T4SWireDetector> detList;
  vector<T4SMagnetLine> magList;
};





#endif
