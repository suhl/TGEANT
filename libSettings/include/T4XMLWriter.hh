#ifndef T4XMLWRITER_HH
#define T4XMLWRITER_HH

#include <xercesc/framework/XMLFormatter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include <iostream>
#include <string>
#include <vector>

#include "T4SGlobals.hh"

using namespace std;
using namespace xercesc;
/*! \class T4XMLWriter
 *  \brief Generic XML-Writer class
 * 
 * This class is abstract generic interface to the xercesc functions. It features creating dataNodes with content from almost any data type.
 * 
 */
class T4XMLWriter
{
  public:
    /*! \brief constructor */
    T4XMLWriter(void);
    /*! \brief destructor */
    ~T4XMLWriter(void) {}

    /*! \brief Routine to create a new XML document from scratch
     *  \param _fileName The filename to use for the new document
     *  This creates a new XML structure in the given file.
     *  The new file will contain the proper headers and a root node.
     */
    void createFile(string _fileName);
    
    /*! \brief Routine to a new node to the document
     *  \param _nodeName the name of the new node to be created;
     * 
     * This routine will create a new <_nodeName></_nodeName> block without any content.
     * It is used to create the base-structure of the document.
     */
    void addNode(string _nodeName);
    
    /*! \brief creates a new node with data in between
     *  \param _nodeName The name of the node in which you want to create the new node - this must have already been created by addNode(string _nodeName)!!
     *  \param _nodeNumber the index number of the _nodeName above.
     *  \param _dataNodeName the name of the new data-containing node that is to be created
     *  \param _dataNodeContent data that is to be placed in between
     * 
     * Nota bene: This function exists overloaded with almost every data type! This will be the only one commented in detail.
     * 
     * Okay this is a bit difficult now:
     * Assume we start with <root></root> 
     * then we call addNode(string _nodeName) twice with the same parameter "detector".
     * Our document will look like:
     * <root>
     *   <detector>
     *   </detector>
     *   <detector>
     *   </detector>
     * </root>
     * 
     * To add data in between we can call this function!
     * createDataNode("detector",0,"name","RPD")
     * will give us:
     * <root>
     *   <detector>
     *     <name>RPD</name>
     *   </detector>
     *   <detector>
     *   </detector>
     * </root>
     * 
     * createDataNode("detector",1,"name","RPD")
     * will give us:
     * <root>
     *   <detector>
     *   </detector>
     *   <detector>
     *     <name>RPD</name>
     *   </detector>
     * </root>
     */
    void createDataNode(string _nodeName, int _nodeNumber, string _dataNodeName,
        string _dataNodeContent);
    void createDataNode(string _nodeName, int _nodeNumber, string _dataNodeName,
        bool _boolInput);
    void createDataNode(string _nodeName, int _nodeNumber, string _dataNodeName,
        double _doubleInput);
    void createDataNode(string _nodeName, int _nodeNumber, string _dataNodeName,
        int _intInput);
    void createDataNode(string _nodeName, int _nodeNumber, string _dataNodeName,
        long int _intInput);

    
    /*! \brief This function returns the count of the nodes with the given name
     *  \param _nodeName The name of the node to count
     * 
     * Return to our example:
     * Assuming we have the file:
     * <root>
     *   <detector>
     *   </detector>
     *   <detector>
     *   </detector>
     * </root>
     * 
     * getNodeCount("detector") will return 2
     * 
     */ 
    int getNodeCount(string _nodeName);

  private:
    DOMImplementation* domIMP;
    DOMDocument* domDOC;
    DOMElement* myROOTNode;
};

#endif
