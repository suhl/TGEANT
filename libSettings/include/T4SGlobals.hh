#ifndef T4SGLOBALS_HH_
#define T4SGLOBALS_HH_

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <sys/stat.h>
#include <cstdlib>



#define CALONAME_ECAL0 0
#define CALONAME_ECAL1 1
#define CALONAME_ECAL2 2
#define CALONAME_HCAL1 3
#define CALONAME_HCAL2 4

#define MODULE_SHASHLIK 0
#define MODULE_GAMS 1
#define MODULE_MAINZ 2
#define MODULE_OLGA 3
#define MODULE_GAMSRH 4
#define MODULE_SAMPLING 5



using namespace std;

/*! \struct T4SGeneral
 *  \brief General options used in the settings file.
 */
struct T4SGeneral
{
    /*! \brief String of the selected physics list: "EM_ONLY", "STANDARD" or "FULL".*/
    std::string physicsList;
    bool useGflash;
    bool useVisualization;
    /*! \brief Bool to activate a user defined seed. Otherwise a random seed is taken.*/
    bool useSeed;
    /*! \brief User defined seed (for activated useSeed only).*/
    long int seed;

    std::string runName;
    std::string outputPath;
    bool saveASCII;
    bool saveBinary;
    bool saveDetDat;
    bool useTrigger;
    bool exportGDML;
    bool namingWithSeed;

    /*! \brief Specify the amout of console output.
     *
     * Level 0: No output at all. (Except the default output from Geant4.)
     * Level 1: Additional output of the current event number.
     * Level 2: Medium output.
     * Level 3: Full output.
     * Level 4: Full output with line numbers.
     */
    int verboseLevel;
    /*! \brief Bool to activate the measuring of the cpu time per event.*/
    bool usePerformanceMonitor;
    /*! \brief Bool to check the overlap between the different geometries.
     *
     * Note: For developers only. This option needs a lot of cpu time.
     * */
    bool checkOverlap;
    /*! \brief Global production cuts for secondaries.*/
    double productionCutsGlobal;
    /*! \brief Production cuts for secondaries in Gams modules.*/
    double productionCutsGams;
    /*! \brief Production cuts for secondaries in RHGams modules.*/
    double productionCutsRHGams;
    /*! \brief Production cuts for secondaries in Mainz modules.*/
    double productionCutsMainz;
    /*! \brief Production cuts for secondaries in Olga modules.*/
    double productionCutsOlga;
    /*! \brief Production cuts for secondaries in Shashlik modules.*/
    double productionCutsShashlik;
    /*! \brief Production cuts for secondaries in the HCAL region.*/
    double productionCutsHcal;
    /*! \brief Bool to deactivate the production of secondaries at all
     *         (maximal production cuts, respectively).*/
    bool noSecondaries;

    /*! \brief Color theme of your console (0 = unset, 1 = black, 2 = white).*/
    int colorTheme;
    /*! \brief Simplified geometries used for visualization only. (E.g. remove internals in tubs or absorber plates in a shashlik module.) */
    bool simplifiedGeometries;
    /*! \brief Enable this flag to disable the memory monitor that stops TGEANT if the memory exceeds 2GB. */
    bool noMemoryLimitation;
    
    /*! \brief Bool to activate the output splitting.
     * The output file (saveASCII only) is splitted with eventsPerChunk events per file.
     *
     * This tool is useful if you are running TGEANT on a batch system w/o enough disc
     * space for the output. Splitting the files allows to run long TGEANT jobs with
     * constantly moving away finished output files.
     * */
    bool useSplitting;
    /*! \brief Number of events per output file (for activated useSplitting only).*/
    int eventsPerChunk;
    /*! \brief Bool to activate the writing of a file list containing all finished chunks
     *         (for activated useSplitting only).
     *
     * This file list gets the name of the first output file with a .lst suffix.
     * When TGEANT has finished the current output file its name is added in this list.
     * This option is useful if you want to easily monitor which files are finished
     * and ready to move.
     * */
    bool useSplitPiping;
    
    /*! \brief This selects the active TriggerPlugin for the production 
     * Can be DVCS2012, SIDIS2011, Primakoff2012, DY2014, DY2015 or DVCS2016 for now
     */
    string triggerPlugin;
};

/*! \struct T4SSampling
 *  \brief Options used for PMT readout with optical physics activated for physicsList "FULL".
 */
struct T4SSampling
{
    unsigned int windowSize;
    unsigned int binsPerNanoSecond;
    bool useBaseline;
    double baseline;
};


/*! \struct T4SPythiaMDME
 *  \brief Options for pythia called like pythia->SetMDME(number,flag1,flag2);
 */
struct T4SPythiaMDME{
    int number;
    double flag1;
    double flag2;
};

enum T4SPythiaMSBDType{
  MSTP=0,MSTU=1,PARU=2,PARP=3,CKIN=4
};

/*! \struct T4SPythiaMSBD
 *  \brief Options for pythia called like pythia->SetXXXX(number,flag1);
 */
struct T4SPythiaMSBD{
    int number;
    double flag1;
    T4SPythiaMSBDType type;
};

/*! \struct T4SPythiaLHAPDF
 *  \brief a PDF to initialize
 */

struct T4SPythiaLHAPDF{
   /*! \brief initPDFByName options will result in:  initPDFByName(pdfNum,pdfName,pdfType,pdfMemSet); */
  int pdfNum; string pdfName; string pdfType; int pdfMemSet;
  
};


/*! \struct T4SPythiaGeneric
 *  \brief Options for pythia, called like documented below
 */
struct T4SPythiaGeneric{
  /*! \brief processlist pythia->SetMSEL(msel); */
  int msel;
  
  /*! \brief random number generator pythia->SetMRPY(mrpy,mrpy2); */
  int mrpy,mrpy2; 
  
 
};


/*! \struct T4SPythia
 *  \brief Additional options if beamMode "BeamMode" and beamGenerator "PYTHIA" is activated.
 */
struct T4SPythia
{
    std::string target;
    vector<T4SPythiaMSBD> flagsMSBD;
    vector<T4SPythiaMDME> flagsMDME;
    vector<T4SPythiaLHAPDF> pdfList;
    
    T4SPythiaGeneric genericFlags;
};


/*! \struct T4SBeam
 *  \brief Beam options used in the settings file.
 */
struct T4SBeam
{
    /*! \brief Number of events to simulate.*/
    unsigned int numParticles;
    /*! \brief PDG particle Id for the incoming beam particle.*/
    int beamParticle;
    /*! \brief String of the used beam plugin: "HEPGEN", "LEPTO, "PYTHIA, "BeamMode",
     * "VisualizationMode", etc...*/
    std::string beamPlugin;
    /*! \brief Total energy of incoming beam particle (for deactivated useBeamfile).*/
    double beamEnergy;
    /*! \brief Bool to activate the beam file (for beamMode "BeamMode" only).*/
    bool useBeamfile;
    /*! \brief String of the allowed beam file particle for primary vertex: "Beam", "Halo" or "Both".*/
    std::string beamFileType;
    /*! \brief String variable of the beamFileBackend to use for reading this file: "Muon", "Pion" */
    std::string beamFileBackend;
    /*! \brief Z position where the beamfile is normalized to (e.g. z=0 in case of muon beam, z=-750cm in case of pion beam) */
    double beamFileZConvention;
    /*! \brief Z position where the primary beam should start (e.g. z=-9m for muon or z=-7.5m for pion). */
    double beamZStart;
    /*! \brief Bool to activate pileup (for activated useBeamfile only).*/
    bool usePileUp;
    /*! \brief Pileup beam flux. Unit: 1/ns*/
    double beamFlux;
    /*! \brief Pileup time gate. Unit: ns*/
    double timeGate;
    /*! \brief Bool to activate an additional pileup (muon pile for pion beam only).*/
    bool useAdditionalPileUp;
    /*! \brief Additional pileup beam flux. Unit: 1/ns*/
    double additionalPileUpFlux;
    /*! \brief Z position where the beamfile for additional pile up is normalized to (e.g. z=0 in case of muon beam, z=-750cm in case of pion beam) */
    double beamFileZConventionForAdditionalPileUp;
    /*! \brief Bool to activate the target extrapolation.*/
    bool useTargetExtrap;
    /*! \brief Double to set the step limit in target region.*/
    double targetStepLimit;
    /*! \brief If this bool is activated the event generator will be called instead of the first hadronic interaction
     *  that will kill the primary beam particle. This can happen outside of the target as well or not at all.*/
    bool useHadronicInteractionEGCall;
    /*! \brief Bool value in order to reuse one beam particle again.*/
    bool useNewBeamParticle;
};

/*! \struct T4SUser
 *  \brief Additional options if beamMode "UserMode" is activated.
 */
struct T4SUser
{
	/*! \brief Start position of the primary particle.*/
    double position[3];
    /*! \brief Momentum direction of the primary particle.*/
    double momentum[3];
    /*! \brief Bool to activate a random energy selection.*/
    bool useRandomEnergy;
    /*! \brief Minimal total energy of the primary particle (for activated useRandomEnergy only).*/
    double minEnergy;
    /*! \brief Maximum total energy of the primary particle (for activated useRandomEnergy only).*/
    double maxEnergy;
};

/*! \struct T4SCosmic
 *  \brief Additional options if beamMode "CosmicsMode" is activated.
 */
struct T4SCosmic
{
	/*! \brief Variation of the cosmics momentum direction.*/
    double angleVariation;
	/*! \brief Mean x position of cosmics.*/
    double positionX;
	/*! \brief Mean z position of cosmics.*/
    double positionZ;
	/*! \brief Variation of x position (Sigma of gaussian).*/
    double variationX;
	/*! \brief Variation of z position (Sigma of gaussian).*/
    double variationZ;
};

/*! \struct T4SEcalCalib
 *  \brief Additional options if beamMode "EcalCalib" is activated.
 */
struct T4SEcalCalib
{
  /*! \brief Z Position.*/
    double positionZ;
    /*! \brief Minimum x position.*/
    double positionXMin;
    /*! \brief Maximum x position.*/
    double positionXMax;
    /*! \brief Minimum y position.*/
    double positionYMin;
    /*! \brief Maximum y position.*/
    double positionYMax;
    /*! \brief Minimum kinetic energy.*/
    double energyMin;
    /*! \brief Maximum kinetic energy.*/
    double energyMax;
};

/*! \struct T4SPrimGen
 *  \brief Additional options if beamMode "Primakoff" is activated.
 */
struct T4SPrimGen
{
  /*! \brief Z of Target Material.*/
  double Z;
  /*! \brief particle type*/
  std::string particle;
  /*! \brief electric dipol polarisaility*/
  double alpha_1;
  /*! \brief magnetic dipol polarisaility*/
  double beta_1;
  /*! \brief electric quadrupol polarisaility*/
  double alpha_2;
  /*! \brief magnetic quadrupol polarisaility*/
  double beta_2;
  /*! \brief minimal s in pion masses*/
  double s_min;
  /*! \brief maximal s in pion masses*/
  double s_max;
  /*! \brief maximal Q2*/
  double Q2_max;
  /*! \brief minimal photon energy*/
  double Egamma_min;
  /*! \brief minimal beam energy*/
  double pbeam_min;
  /*! \brief maximal beam energy*/
  double pbeam_max;
  /*! \brief born contribution*/
  bool born;
  /*! \brief rho contribution*/
  bool rhoContrib;
  /*! \brief chiral loop contribution*/
  bool chiralLoops;
  /*! \brief polarisability contribution*/
  bool polarizabilities;
  /*! \brief radiative corrections*/
  bool radcorr;
};

/*! \struct T4SHEPGen
 *  \brief Additional options if beamMode "BeamMode" and beamGenerator "HEPGEN" is activated.
 */
struct T4SHEPGen
{
	/*! \brief String of the target nucleon ("p" only).*/
    std::string target;
	/*! \brief Physics program (0 = DVCS, 1 = Pi0->yy, 2 = Rho0->PiPi, 3 = Phi->KK).*/
    int IVECM;
	/*! \brief Lepto-switches (0 = w/o diffractive dissociation, 1 = w/ DD).*/
    int LST;
	/*! \brief Lepto soft cuts.*/
    double CUT[14]; //lepto-soft-cuts

	/*! \brief Scattered muon acceptance theta_max.*/
    double THMAX;
	/*! \brief Value of the parameter for A dependence.*/
    double alf;
	/*! \brief Atomic mass (average) for the target.*/
    double atomas;
	/*! \brief Fraction of coherent events.*/
    double probc;
	/*! \brief Slope of the nuclear form factor.*/
    double bcoh;
	/*! \brief Slope for the production on a nucleon.*/
    double bin;

	/*! \brief Lepton beam charge (default: electron).*/
    double clept;
	/*! \brief Lepton beam polarisation (default: no polarisation).*/
    double slept;
	/*! \brief Parameter for x, t correlation (for DVCS only).*/
    double B0;
	/*! \brief Parameter for x, t correlation (for DVCS only).*/
    double xbj0;
	/*! \brief Parameter for x, t correlation (for DVCS only).*/
    double alphap;
	/*! \brief Generation ranges for the t value.*/
    double TLIM[2];
	/*! \brief Bool to switch between old and new pi0 table (for IVECM=1 only).*/
    bool usePi0_transv_table;
};

/*! \struct T4SPrimTrig
 *  \brief Additional options if triggerMode "Primakoff" is activated.
 */
struct T4SPrimTrig
{
  double threshPrim1;
  double threshPrim2;
  double threshPrim2Veto;
  double threshPrim3;
};

/*! \struct T4SDetector
 *  \brief Standard information for detectors.
 */
struct T4SDetector
{
    bool useDetector;
    std::string name;
    double position[3];
    bool useMechanicalStructure;
};

/*! \struct T4SDetectorRes
 *  \brief T4SDetector with useOptical bool in addition.
 */
struct T4SDetectorRes
{
    T4SDetector general;
    bool useOptical;
};

/*! \struct T4SDummy
 *  \brief T4SDetector with plane dimensions in addition.
 */
struct T4SDummy
{
    T4SDetector general;
    double planeSize[3];
};

/*! \struct caloCalib 
 *  \brief holds the calibration for a module type
 *  It is dual use for either caloCalib in detectors dat or EPIC usage from T4EPICFiles
 */
typedef struct{
  /*! \brief caloNum as defined in this header T4SGlobals.hh */
  int caloNum;
  /*! \brief module number, either absolute from det.dat or relative from EPIC */
  int moduleNum;
  double calibrationValue;
  /*! \brief additions for EPIC bei Sergei */
  int aux1, aux2;
  /*! \brief the human readable name for this module from EPIC */
  string readName;
}caloCalib;

/*! \struct T4SCAL
 *  \brief Holds the data from a single cmtx line of the detectors.dat.
 */
struct T4SCAL
{
    /*! \brief The start id field.*/
    int detectorId;
    /*! \brief The det field.*/
    std::string detName;
    /*! \brief The module type name, i.e. GAMS.*/
    std::string moduleName;
    /*! \brief The number of rows.*/
    int nRow;
    /*! \brief The number of columns.*/
    int nCol;
    /*! \brief The position 3-vector.*/
    double position[3];
    //additional information not used by TGEANT but needed for detectors.dat writing
    
    int type,nmod,roffset,coffset,nHole;
    double size[3],xStep,yStep,tGate,threshold,gev2adc,radlen,abslen,stochasticTerm,constantTerm,calibration,ecrit;
};

/*! \struct T4SMagnetLine
 * \brief holds the information from the detectors.dat for the magnets like flags and currents
 * will be needed to write the magnet block of the detectors.dat
 */
struct T4SMagnetLine{
  int number;
  double flag1,flag2;
  int rot;
};

/*! \struct T4SMagnet
 *  \brief T4SDetector with useField bool and fieldmapPath in addition.
 *  The scale is used for choosing mu minus or mu plus -- mu plus beeing one, mu minus beeing -1
 */
struct T4SMagnet
{
    T4SDetector general;
    double scaleFactor;
    bool useField;
    std::string fieldmapPath;
    int current;
};

/*! \struct T4SRPD
 *  \brief T4SDetector with information used for a recoil detector in addition.
 */
struct T4SRPD
{
    T4SDetector general;
    bool useOptical;
    bool useSingleSlab;
    bool useCalibrationFile;
    bool useConicalCryostat;
    std::string calibrationFilePath;
};

/*! \struct T4SMW1
 *  \brief T4SDetector with information used for the MW1 detector in addition.
 */
struct T4SMW1
{
    T4SDetector general;
    bool useAbsorber;
    bool useMA01X1;
    bool useMA01X3;
    bool useMA01Y1;
    bool useMA01Y3;
    bool useMA02X1;
    bool useMA02X3;
    bool useMA02Y1;
    bool useMA02Y3;
    double posMA01X1[3];
    double posMA01X3[3];
    double posMA01Y1[3];
    double posMA01Y3[3];
    double posMA02X1[3];
    double posMA02X3[3];
    double posMA02Y1[3];
    double posMA02Y3[3];
};

/*! \struct T4SMW2
 *  \brief T4SDetector with information used for the MW2 detector in addition.
 */
struct T4SMW2
{
    T4SDetector general;
    bool useAbsorber;
    bool MB01X;
    bool MB01Y;
    bool MB01V;
    bool MB02X;
    bool MB02Y;
    bool MB02V;
    double positionMB01X[3];
    double positionMB01Y[3];
    double positionMB01V[3];
    double positionMB02X[3];
    double positionMB02Y[3];
    double positionMB02V[3];
};

/*! \struct T4SRICH
 *  \brief T4SDetector with information used for the RICH detector in addition.
 */
struct T4SRICH
{
    T4SDetector general;
    bool useOpticalPhysics;
    bool visibleHousing;
    std::string gas;
};

/*! \struct T4SPolGPD
 *  \brief T4SPolGPD with information used for the future CAMERA with polarized target.
 */
struct T4SPolGPD
{
    T4SDetector general;

    // target
    double target_cryostat_radius;
    double target_cryostat_length;
    double target_cryostat_thickness;

    double target_aluminium_radius;
    double target_aluminium_length;
    double target_aluminium_thickness;

    double target_coils_radius;
    double target_coils_length;
    double target_coils_thickness;

    double target_mylar_window_thickness;

    double target_length;
    double target_lhe_radius;
    double target_nh3_radius;
    double target_fiber_thickness;
    double target_kevlar_thickness;

    // micromegas
    double mm_length;
    double mm_radius;
    double mm_layer_thickness;
    double mm_layer_distance;
    double mm_zPosOffset;

    // ring B
    double ringB_length;
    double ringB_radius;
    double ringB_zPosOffset;
};

/*! \struct T4SSciFiTest
 *  \brief T4SDetector with length value for SciFi test setup in addition.
 */
struct T4SSciFiTest
{
    T4SDetector general;
    double length;
};

/*! \struct T4SWireDetector
 *  \brief Holds the data from a single det line of the detectors.dat.
 */
struct T4SWireDetector
{
  
    bool operator==(const std::string& r) const
    {
      return (tbName == r);
    }
    
    /*! \brief Detector identification number.*/
    int id;
    /*! \brief Technical board detector name (8 chars).*/
    std::string tbName;
    /*! \brief Another detector name (4 chars).*/
    std::string det;
    /*! \brief Detector number in station.*/
    int unit;
    /*! \brief Detector type, see comgeant.*/
    int type;
    /*! \brief Full x size of detector (in mm instead of the original cm).*/
    double xSize;
    /*! \brief Full y size of detector (in mm instead of the original cm).*/
    double ySize;
    /*! \brief Full z size of detector (in mm instead of the original cm).*/
    double zSize;
    /*! \brief x position of detector (in mm instead of the original cm).*/
    double xCen;
    /*! \brief y position of detector (in mm instead of the original cm).*/
    double yCen;
    /*! \brief z position of detector (in mm instead of the original cm).*/
    double zCen;
    /*! \brief Index of rotation matrix.*/
    int rotMatrix;
    /*! \brief Offset to the first wire (w.r.t. DRS).*/
    double wireDist;
    /*! \brief Rotation angle of WRS to MRS.*/
    double angle;
    /*! \brief Number of wires.*/
    int nWires;
    /*! \brief Distance between two wires.*/
    double pitch;
    /*! \brief Radiation length.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double radLength;
    /*! \brief Detector efficiency.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double effic;
    /*! \brief Detector background.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double backgr;
    /*! \brief Detector time gate.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double tgate;
    /*! \brief Drift velocity.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double drVel;
    /*! \brief Time zero.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double t0;
    /*! \brief 2 hit resolution.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double res2hit;
    /*! \brief Space resolution.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double space;
    /*! \brief Time slice.
     *
     * Note: This value is from the original detectors.dat file. TGEANT will not change this value.
     * */
    double tslice;
};

/*! \struct T4SDeadZone
 *  \brief Holds the data from a single dead line of the detectors.dat.
 */
struct T4SDeadZone
{
    /*! \brief Detector identification number.*/
    int id;
    /*! \brief Technical board detector name (8 chars).*/
    std::string tbName;
    /*! \brief Another detector name (4 chars).*/
    std::string det;
    /*! \brief Detector number in station.*/
    int unit;
    /*! \brief Shape of the dead zone (1 = cube, 5 = cylinder).*/
    int sh;
    /*! \brief Full x size of dead zone (in mm instead of the original cm).*/
    double xSize;
    /*! \brief Full y size of dead zone (in mm instead of the original cm).*/
    double ySize;
    /*! \brief Full z size of dead zone (in mm instead of the original cm).*/
    double zSize;
    /*! \brief x position of dead zone (in mm instead of the original cm).*/
    double xCen;
    /*! \brief y position of dead zone (in mm instead of the original cm).*/
    double yCen;
    /*! \brief z position of dead zone (in mm instead of the original cm).*/
    double zCen;
    /*! \brief Index of rotation matrix.*/
    int rotMatrix;
};



/*! \struct T4STargetInformation
 *  \brief Target information needed to write the target block to the detectors.dat file.
 */
struct T4STargetInformation
{
    string name;
    int rotMatrix;
    int sh;
    double xSize;
    double ySize;
    double zSize;
    double xCen;
    double yCen;
    double zCen;
};

/*! \struct T4SExternalFiles
 *  \brief Paths for all external files needed in TGEANT.
 *
 *  Which of these are exactly used depends on the setup.
 *  If "default" or "default-dvcs_2012" respectively option is set, the file will
 *  be loaded from the $TGEANT/resources/ folder.
 */
struct T4SExternalFiles
{
    std::string beamFile;
    std::string beamFileForAdditionalPileUp;
    std::string localGeneratorFile;
    std::string triggerMatrixInnerX;
    std::string triggerMatrixOuterY;
    std::string triggerMatrixLadderX;
    std::string triggerMatrixMiddleX;
    std::string triggerMatrixMiddleY;
    std::string triggerMatrixLAST;
    std::string libHEPGen_Pi0;
    std::string libHEPGen_Pi0_transv;
    std::string libHEPGen_Rho;
    std::string visualizationMacro;
    std::string detectorEfficiency;
    std::string calorimeterInfo;
};



/*! \brief translates a string _in from $TGEANT to the value of getenv(TGEANT)
 */

std::string translatePathEnvTGEANT(std::string _in);
/*! \brief replaces a string _in from $TGEANT to the value of getenv(TGEANT)
 */
void replacePathEnvTGEANT(string& toChange);

/*! \brief translates a string _in from the value of getenv($TGEANT) to $TGEANT
 */
std::string reverseTranslatePathEnvTGEANT(std::string _in);

/*! \brief replaces a string _in from the value of getenv($TGEANT) to $TGEANT
 */
void reverseReplacePathEnvTGEANT(string& toChange);






/*! \brief Converts int to string.
 *
 * \param _arg int value.
 * \return int value as a string.
 * */
std::string intToStr(int arg);

/*! \brief Converts string to int.
 *
 * \param arg Number as a string.
 * \return string value as a int.
 * */
int strToInt(std::string arg);

/*! \brief Converts unsinged int to string.
 *
 * \param arg unsigned int value.
 * \return unsigned int value as a string.
 * */
std::string uintToStr(unsigned int arg);

/*! \brief Converts long int to string.
 *
 * \param arg long int value.
 * \return long int value as a string.
 * */
std::string longintToStr(long int arg);

/*! \brief Converts string to long int.
 *
 * \param arg Number as a string.
 * \return string value as a long int.
 * */
long int strToLong(std::string arg);

/*! \brief Converts double to string.
 *
 * \param arg double value.
 * \return double value as a string with full precision.
 * */
std::string doubleToStrFP(double arg);

/*! \brief Converts string to double.
 *
 * \param arg Number as a string.
 * \return string value as a double.
 * */
double strToDouble(std::string arg);

/*! \brief Converts string to bool.
 *
 * \param arg Bool as a string.
 * \return string value as a bool.
 * */
bool strToBool(std::string arg);

/*! \brief Check if a file exists.
 *
 * \param name Path of the file to check.
 * \return Returns true if the file exists.
 * */
bool fileExists(const std::string& name);

/*! \brief Check if a directory exists.
 *
 * \param name Path of the directory to check.
 * \return Returns true if the directory exists.
 * */
bool dirExists(const std::string& name);

/*! \brief Explodes a string into a vector of strings, separated by the original whitespaces.
 *
 * \param input Full string to explode.
 * \return Vector of strings, separated by the original whitespaces.
 * */
std::vector<std::string> explodeString(const std::string& input);

/*! \brief Explodes a string into a vector of strings, separated by a custom delimiter.
 *
 * \param input Full string to explode.
 * \param delim Custom delimiter.
 * \return Vector of strings, separated by the custom delimiter.
 * */
std::vector<std::string> explodeStringCustomDelim(const std::string& input,
    const std::string& delim);

template <typename T> void setThreeVector(T* array,T _val1, T _val2, T _val3){
  array[0] = _val1;
  array[1] = _val2;
  array[2] = _val3;
}









#endif /* T4SGLOBALS_HH_ */
