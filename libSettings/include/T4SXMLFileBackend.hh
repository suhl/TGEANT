#ifndef T4SXMLFILEBACKEND_HH_
#define T4SXMLFILEBACKEND_HH_

#include "T4SStructManager.hh"
#include "T4SMessenger.hh"
#include "T4XMLParser.hh"
#include "T4XMLWriter.hh"

class T4SXMLFileBackend{
public:
  
   /*! \brief basic constructor with struct manager, cannot work without struct manager
   */
    T4SXMLFileBackend(T4SStructManager* _structMan) {
        structMan = _structMan;
	myWriter = NULL;
	myParser = NULL;
    };
    virtual  ~T4SXMLFileBackend();
    
    /*! \brief reads an xml file */
    virtual void readXML(string _inFile) = 0;
    /*! \brief writes an xml file */
    virtual void writeXML(string _inFile) = 0;
    
protected:
    T4SStructManager* structMan;
    /*! \brief the T4XMLParser that is used to parse an XML file
    */

    T4XMLParser* myParser;
    /*! \brief the T4XMLWriter that is used to create an XML file
     */
    T4XMLWriter* myWriter;
  
};




#endif
