#ifndef T4EPICFiles_HH_
#define T4EPICFiles_HH_



#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "T4SGlobals.hh"



/*! \class T4EPICFiles
 *  \brief This class reads in Sergeis epic-calibration and makes them available.
 *  It is a singleton because we do not want to move pointers to this global object around in CORAL too much
 */
class T4EPICFiles {
public:
  /*! \brief standard destructor */
  ~T4EPICFiles ( void );

  static T4EPICFiles* getInstance ( void );
  
  inline void loadECAL0(string _fileName){loadFileName(_fileName,CALONAME_ECAL0);};
  inline void loadECAL1(string _fileName){loadFileName(_fileName,CALONAME_ECAL1);};
  inline void loadECAL2(string _fileName){loadFileName(_fileName,CALONAME_ECAL2);};
  
  
  void loadFileName(string _fileName, int _ecalNum);
  
  map<int,caloCalib>* getCalo(int _caloNum);

  static pair<int,int> getXYMod(caloCalib _in);
  double getCalibForModule(int _caloNum, int _moduleNum);
  

private:
  /*! \brief do we have an instance already? */
  static bool instanceFlag;
  /*! \brief our instance */
  static T4EPICFiles* single;
  /*! \brief standard constructor */
  T4EPICFiles ( void );
  
  map<int,caloCalib> ECAL0;
  map<int,caloCalib> ECAL1;
  map<int,caloCalib> ECAL2;
  
  bool isActive;
};









#endif

