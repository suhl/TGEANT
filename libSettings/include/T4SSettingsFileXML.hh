#ifndef T4SSETTINGSFILEXML_HH_
#define T4SSETTINGSFILEXML_HH_

#include "T4SFileBackEnd.hh"
#include "T4XMLParser.hh"
#include "T4XMLWriter.hh"
#include "T4PythiaTuning.hh"

using namespace std;


/*! \class T4SSettingsFileXML
 * 
 *  \brief Implementation of XML container file for the settings
 * 
 *  This class implements reading and writing of the TGEANT structM in an XML file.
 *  This is the new standard settingsfile output
 */
class T4SSettingsFileXML : public T4SFileBackEnd
{
  public:
    /*! \brief The standard constructor */
    T4SSettingsFileXML(T4SStructManager* _structM);
    
    /*! \brief The standard destructor */
    virtual ~T4SSettingsFileXML(void);
    
   /*! \brief This function implements the actual unchecked loading of the ASCII formatted file 
    *  \param fileName the file to load from
    */
    void load_unchecked(std::string fileName);
   
    /*! \brief This implements the writing of the T4SStructManager structM to the given file
    *
    *  \param fileName the file to save to
    */
    void save(std::string fileName);

  private:
    
    /*! \brief This function writes an RPD line to the file
     *
     *  \param i The number of the RPD-Entry to write to the file
     *  Note that a single call to this function generates a single entry. So if more than one RPD is selected,
     *  it has to be called multiple times with different i. 
     */
    void saveRPD(unsigned int i);
    
     /*! \brief This function writes a single magnet entry to the file
     * 
     * \param i The number of the magnet to write
     *  Note that a single call to this function generates a single entry. So if more than one magnet is selected,
     *  it has to be called multiple times with different i. 
     */
    void saveMagnet(unsigned int i);
    
     /*! \brief This function writes a single detector line to the file
     * 
     * \param keyWord the keyword for the detector type to write
     * \param detector the pointer to the detector that is to be written
     * This function writes a single detector to the settingsfile. The information about positioning is taken from the second parameter
     * The keyword unter which section it is to be written, is taken from the first parameter. 
     */
    void saveDetector(std::string keyWord, const T4SDetector* detector);

    
    /*! \brief This function writes a generic detector with optical support to the file
     * 
     * \param i The number of the detector to write
     * This function saves a generic detector with optical support (keyword: #DETECTORRES) 
     * to the file. The parameter is the index which detector to write.
     */
    void saveDetectorRes(unsigned int i);

    /*! \brief This function writes a dummy detector with given size to the file
     */
    void saveDummies(const T4SDummy* detector);

    
    /*! \brief This function loads the RPD to be used
     *  \param i The index of the RPD to load 
     */
    void loadRPD(int i);
    
    /*! \brief This function loads the magnets to be used
     *  \param i the number of the magnet to load
     */
    void loadMagnet(int i);

    
    /*! \brief This function loads a detector
     * \param keyWord the keyWord for the detector type
     * \param i the index of the detector to load
     * This function loads the detector number i of the given keyWord from the file.
     */
    void loadDetector(std::string keyWord, int i);
    
    
    /*! \brief This function loads a detector with optical support
     * \param i the index of the detector to load
     * This function loads detector number i of the list of detectors with optical support
     */
    void loadDetectorRes(int i);

    /*! \brief This function loads a dummy detector with given size
     */
    void loadDummies(int i);

    
    /*! \brief the T4XMLParser that is used to parse an XML file
     */
    T4XMLParser* myParser;
    
    /*! \brief the T4XMLWriter that is used to create an XML file
     */
    T4XMLWriter* myWriter;
};

#endif
